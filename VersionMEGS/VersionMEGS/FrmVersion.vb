﻿Public Class FrmVersion
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Button1.Text = "Actualizar" Then
            Try
                Dim cDirVersion As String = "C:\CpceMEGS"
                Dim cDirTestVersion As String = "C:\CpceMEGStest"
                Dim cFileVersion As String = "cpceMEGS.exe"
                Dim cFileActualizarVersion As String = "actualizar.ini"
                Dim cOldVersion As String = Nothing
                Dim cNewVersion As String = Nothing
                Dim cOldFileActualizarVersion As String
                Dim cNewFileActualizarVersion As String
                Dim cUrlVersion As String = "http://actualizar.cpcechaco.org.ar/cpcesistemas/actualizacion"
                If Application.StartupPath = cDirTestVersion Then
                    'C:\CpceMEGStest version test. Reemplazo Dir por DirTest
                    cDirVersion = cDirTestVersion
                    cOldVersion = cDirVersion & "\" & cFileVersion
                    cNewVersion = cDirVersion & "\Temp\" & cFileVersion
                    cUrlVersion = cUrlVersion & "/test/" & cFileVersion
                    'actualizar.ini
                    cOldFileActualizarVersion = cDirVersion & "\" & cFileActualizarVersion
                    cNewFileActualizarVersion = cDirVersion & "\Temp\" & cFileActualizarVersion
                Else
                    'C:\CpceMEGS version prod
                    cOldVersion = cDirVersion & "\" & cFileVersion
                    cNewVersion = cDirVersion & "\Temp\" & cFileVersion
                    cUrlVersion = cUrlVersion & "/" & cFileVersion
                    'actualizar.ini
                    cOldFileActualizarVersion = cDirVersion & "\" & cFileActualizarVersion
                    cNewFileActualizarVersion = cDirVersion & "\Temp\" & cFileActualizarVersion
                End If
                Dim processes() As Process = Process.GetProcessesByName(Name)
                If processes IsNot Nothing Then
                    'If MsgBox("Todas las instancias abiertas se cerrarán. ¿Desea continuar?", MsgBoxStyle.YesNo, "Atención") = MsgBoxResult.No Then
                    '    Me.Close()
                    '    Exit Sub
                    'End If
                    For Each proc In processes
                        proc.Kill()
                        proc.WaitForExit()
                    Next
                    Label1.Text = "Se esta actualizando una nueva versión del sistema..."
                    Try
                        My.Computer.Network.DownloadFile(cUrlVersion, cNewVersion, "", "", True, 1000000, True, FileIO.UICancelOption.DoNothing)
                        My.Computer.FileSystem.CopyFile(cNewVersion, cOldVersion, True)
                        My.Computer.FileSystem.CopyFile(cNewFileActualizarVersion, cOldFileActualizarVersion, True)
                        Label1.Text = "El programa se ha actualizado satisfactoriamente a la última versión"
                        Button1.Text = "Ingresar"
                    Catch ex As Exception
                        MessageBox.Show("El sistema no pudo actualizarse. Error de conexión (01002). Contacte con el administrador.", "Error Version", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Else
            Try
                If Application.StartupPath = "C:\CpceMEGStest" Then
                    Process.Start("C:\CpceMEGStest\cpceMEGS.exe")
                Else
                    Process.Start("C:\CpceMEGS\cpceMEGS.exe")
                End If
                End
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
