﻿Imports System.Windows.Forms
Imports System.Drawing.Drawing2D

Public Class MDIMain
    Shadows menu As ToolStripMenuItem
    Dim ctlMDI As MdiClient
    Dim enlargenum As Integer = 28
    Dim nNroInstVentas As Integer
    Dim cnn As ConsultaBD


#Region "Degradado de fondo para un formulario MDI"


    ''' <summary>
    '''  Contendrá el control que representa al área cliente MDI
    ''' </summary>
    ''' <remarks>Se emplea por el evento Form Resize
    '''  Si se declara [WithEvents] hay que escirbir 
    '''  el evento [_ctlMdiClient.Paint]
    '''  si no hay que asignar el manejador 
    '''  [AddHandler _ctlMdiClient.Paint, AddressOf PintarFondo]
    '''</remarks>
    Private WithEvents _ctlMdiClient As MdiClient = Nothing

    ''' <summary>
    '''   Esta función tiene que ser llamada 
    '''   por el evento FormLoad del Form
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub EstablecerFondoDegradadoParaFormLoad()

        '-------------------------------------------------
        ' ATENCION
        ' Para que todo esto funcione, esta función tiene que 
        ' ser llamada desde el evento Load del formulario
        '-------------------------------------------------

        '-------------------------------------------------
        ' Estamos buscando en control que representa
        ' el área cliente MDI 
        '  La función [GetMdiContainer] comprobará si existe 
        '  un control MdiClient en el formulario indicado, 
        '  devolviendo la referencia al citado control.
        '  o el valor Nothing si no existe
        _ctlMdiClient = GetMdiContainer(Me)

    End Sub

    ''' <summary>
    '''   Esta función tiene que ser llamada 
    '''   por el evento FormLoad del Form
    ''' </summary>
    Private Sub EstablecerImagenFondoParaFormLoad()
        Try
            ' Asignamos una imagen al fondo del formulario.
            Me.BackgroundImage = _
            Image.FromFile("C:\FarmaMEGS\Imagenes\iconos\logoDelMilagro.ico")

            ' Ajustamos la imagen dentro del rectángulo
            ' cliente del control.
            ' Por ejemplo.: 
            ' Si la imagen es muy 'grande' y ponemos *center* 
            ' la imagen no se ve entera, solo se ve la parte 
            ' superior izquierda que *entra* en el formulario
            Me.BackgroundImageLayout = ImageLayout.Stretch

        Finally
            ' No hago nada
            ' Si no se carga la imagen se muestra 
            ' el color de fondo por defecto
        End Try
    End Sub

    ''' <summary>
    '''  La función comprobará si existe un control MdiClient
    '''  en el formulario indicado, devolviendo la referencia
    '''  al citado control. o el valor Nothing si no existe
    ''' </summary>
    ''' <param name="frm">El formulario en el que se busca</param>
    ''' <returns> 
    '''    La referncia al objeto 
    '''    <see cref="MdiClient">[MdiClient]</see> si existe 
    '''    o un valor Nothing si no existe en el formulario 
    ''' </returns>
    Private Shared Function GetMdiContainer( _
                     ByVal frm As Form) _
                 As MdiClient

        '--------------------------------------------------------
        ' La función comprobará si existe un control MdiClient
        ' en el formulario indicado, devolviendo la referencia
        ' al citado control. O el valor Nothing si no existe
        '--------------------------------------------------------
        ' Estamos buscando el control que representa 
        ' el área cliente MDI 
        Dim resultado As MdiClient = Nothing
        Dim ctl As Control
        For Each ctl In frm.Controls
            If ctl.GetType.Name = GetType(MdiClient).Name Then
                ' ¿Encontrado!
                resultado = CType(ctl, MdiClient)
                ' no hace falta seguir buscando mas
                Exit For
            End If
        Next
        '
        Return resultado
    End Function


    ''' <summary>
    ''' Función que pinta el fondo del área cliente
    ''' </summary>
    Private Shared Sub PintarFondo( _
                ByVal sender As Object, _
                ByVal e As System.Windows.Forms.PaintEventArgs)

        Try
            ' Colores para el degradado
            Dim colorSuperior As Color = Color.Blue 'Color.Blue
            Dim colorInferior As Color = Color.OldLace ' Color.Black
            ' el control MdiClient
            Dim panelMdiForm As MdiClient = CType(sender, MdiClient)
            ' el degradado
            Dim GradientePanel As New Drawing2D.LinearGradientBrush( _
               New RectangleF(0, 0, panelMdiForm.Width, panelMdiForm.Height), _
                   colorSuperior, _
                   colorInferior, _
                   Drawing2D.LinearGradientMode.Vertical)
            ' dibujarlo
            e.Graphics.FillRectangle( _
                GradientePanel, _
                New RectangleF(0, 0, panelMdiForm.Width, panelMdiForm.Height))

        Catch ex As Exception
            '--------------------------------------------------
            ' Ignoro la regla FxCop
            ' CA1031: No capturar los tipos de excepción general
            ' http://msdn.microsoft.com/es-es/library/ms182137.aspx
            ' Info: You should not catch Exception or SystemException. 
            '       Catching generic exception types can hide run-time 
            '       problems from the library user, and can complicate 
            '       debugging. You should catch only those exceptions 
            '       that you can handle gracefully.
            '--------------------------------------------------
            ' Registro el problema en el log pero 
            ' dejo que continue el proceso
            ' no se cambiara el color de fondo del formulario
            My.Application.Log.WriteEntry( _
             "Problemas en la funcion *PintarFondo* " & ex.Message, _
              System.Diagnostics.TraceEventType.Error)
        End Try
    End Sub



    ''' <summary>
    '''  Capturar el evento Paint del control [MdiClient].
    '''  Evento Paint de la variable de clase que tiene la instancia
    '''  del control MdiClient y que está definida como [WithEvents]
    ''' </summary>
    Private Sub _ctlMdiClient_Paint( _
            ByVal sender As Object, _
            ByVal e As System.Windows.Forms.PaintEventArgs) _
        Handles _ctlMdiClient.Paint
        '---------
        ' Llamar a la función que hace el 
        ' trabajo de  dibujar el degradado
        Call PintarFondo(sender, e)
    End Sub



    ''' <summary>
    '''  Capturar el evento Resize del control [MdiClient].
    '''  Evento Paint de la variable de clase que tiene la instancia
    '''  del control MdiClient y que esta definida como [WithEvents]
    ''' </summary>
    ''' <remarks>  
    '''  <para>
    '''    Resuelve el problema de redibujar la imagen 
    '''    cuando se cambia el tamaño del formulario
    ''' </para> 
    '''  <para>
    '''    Cuando se cambia el tamaño del formulario, la imagen 
    '''    tiene problemas para dibujarse y se queda como a "capas".
    '''    Con este evento forzamos a que se dibuje totalmente 
    '''    y se resuelve el problema
    ''' </para> 
    ''' </remarks>
    Private Sub _ctlMdiClient_Resize( _
                ByVal sender As Object, ByVal e As System.EventArgs) _
            Handles _ctlMdiClient.Resize

        ' Llamar a la función que hace el 
        ' trabajo de  dibujar el degradado
        Call PintarFondo(sender, New PaintEventArgs( _
                    Me._ctlMdiClient.CreateGraphics, _
                    New Rectangle(Me._ctlMdiClient.Location, Me._ctlMdiClient.Size)))
    End Sub



    ' ''' <summary>
    ' '''   Capturar el evento Resize del formulario.
    ' ''' </summary>
    ' ''' <remarks>  
    ' '''  <para>
    ' '''    Resuelve el problema de redibujar la imagen 
    ' '''    cuando se cambia el tamaño del formulario
    ' ''' </para> 
    ' '''  <para>
    ' '''    Cuando se cambia el tamaño del formulario, la imagen 
    ' '''    tiene problemas para dibujarse y se queda como a "capas".
    ' '''    Con este evento forzamos a que se dibuje totalmente 
    ' '''    y se resuelve el problema
    ' ''' </para> 
    ' ''' </remarks>
    'Private Sub Evento_Resize( _
    '            ByVal sender As Object, _
    '            ByVal e As System.EventArgs) _
    '        Handles MyBase.Resize
    '
    '    '--------------------------------------------------------
    '    ' Esta pregunta evita errores en la función [PintarFondo]
    '    ' cuando _ctlMdiClient Is Nothing, hecho que ocurre
    '    ' cuando no se llama la función[TrabajoParaFormLoad] 
    '    ' desde el evento "Load del formulario
    '    '--------------------------------------------------------
    '
    '    If Not (Me._ctlMdiClient Is Nothing) Then
    '        ' llamar a la función que pinta el 
    '        ' degradado del fondo del form
    '        '------------------
    '        ' Observa la *trampa* al llamar
    '        ' a la función [PintarFondo]  
    '        ' El objeto [Sender] no es el formulario sino que se  
    '        ' cambia al control [MdiClient].  
    '        ' De la misma forma el evento [EventArgs] se cambia  
    '        ' al evento [PaintEventArgs] y se vuelve a usar para  
    '        ' ello al control [MdiClient] en lugar del formulario.
    '        '------------------
    '        Call PintarFondo( _
    '            Me._ctlMdiClient, _
    '            New PaintEventArgs( _
    '                Me._ctlMdiClient.CreateGraphics, _
    '                New Rectangle(Me._ctlMdiClient.Location, _
    '                              Me._ctlMdiClient.Size)))
    '    End If
    'End Sub


#End Region


    Private Sub PictureBox1_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseEnter
        PictureBox1.BringToFront()
        PictureBox1.BackColor = Color.Blue
        PictureBox1.Width = (PictureBox1.Width + enlargenum)
        PictureBox1.Height = (PictureBox1.Height + enlargenum)
        PictureBox1.Location = New Point(PictureBox1.Location.X - enlargenum / 2, PictureBox1.Location.Y - enlargenum / 2)

    End Sub

    Private Sub PictureBox1_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseLeave
        PictureBox1.BackColor = Color.White
        PictureBox1.Width = (PictureBox1.Width - enlargenum)
        PictureBox1.Height = (PictureBox1.Height - enlargenum)
        PictureBox1.Location = New Point(PictureBox1.Location.X + enlargenum / 2, PictureBox1.Location.Y + enlargenum / 2)

    End Sub

    Private Sub PictureBox2_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseEnter
        '  Label3.Visible = True
        PictureBox2.BringToFront()
        PictureBox2.BackColor = Color.Blue
        PictureBox2.Width = (PictureBox2.Width + enlargenum)
        PictureBox2.Height = (PictureBox2.Height + enlargenum)
        PictureBox2.Location = New Point(PictureBox2.Location.X - enlargenum / 2, PictureBox2.Location.Y - enlargenum / 2)

    End Sub

    Private Sub PictureBox2_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseLeave
        '  Label3.Visible = False
        PictureBox2.BackColor = Color.White
        PictureBox2.Width = (PictureBox2.Width - enlargenum)
        PictureBox2.Height = (PictureBox2.Height - enlargenum)
        PictureBox2.Location = New Point(PictureBox2.Location.X + enlargenum / 2, PictureBox2.Location.Y + enlargenum / 2)

    End Sub

    Private Sub PictureBox3_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseEnter
        PictureBox3.BringToFront()
        PictureBox3.BackColor = Color.Blue
        PictureBox3.Width = (PictureBox3.Width + enlargenum)
        PictureBox3.Height = (PictureBox3.Height + enlargenum)
        PictureBox3.Location = New Point(PictureBox3.Location.X - enlargenum / 2, PictureBox3.Location.Y - enlargenum / 2)

    End Sub

    Private Sub PictureBox3_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseLeave
        '  Label7.Visible = False
        PictureBox3.BackColor = Color.White
        PictureBox3.Width = (PictureBox3.Width - enlargenum)
        PictureBox3.Height = (PictureBox3.Height - enlargenum)
        PictureBox3.Location = New Point(PictureBox3.Location.X + enlargenum / 2, PictureBox3.Location.Y + enlargenum / 2)
    End Sub

    Private Sub PictureBox4_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.MouseEnter
        PictureBox4.BringToFront()
        PictureBox4.BackColor = Color.Blue
        PictureBox4.Width = (PictureBox4.Width + enlargenum)
        PictureBox4.Height = (PictureBox4.Height + enlargenum)
        PictureBox4.Location = New Point(PictureBox4.Location.X - enlargenum / 2, PictureBox4.Location.Y - enlargenum / 2)

    End Sub
    Private Sub PictureBox4_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.MouseLeave
        PictureBox4.BackColor = Color.White
        PictureBox4.Width = (PictureBox4.Width - enlargenum)
        PictureBox4.Height = (PictureBox4.Height - enlargenum)
        PictureBox4.Location = New Point(PictureBox4.Location.X + enlargenum / 2, PictureBox4.Location.Y + enlargenum / 2)
    End Sub
    Private Sub PictureBox6_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.MouseEnter

        PictureBox6.BringToFront()
        PictureBox6.BackColor = Color.Blue
        PictureBox6.Width = (PictureBox6.Width + enlargenum)
        PictureBox6.Height = (PictureBox6.Height + enlargenum)
        PictureBox6.Location = New Point(PictureBox6.Location.X - enlargenum / 2, PictureBox6.Location.Y - enlargenum / 2)

    End Sub
    Private Sub PictureBox6_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.MouseLeave
        '  Label4.Visible = False
        PictureBox6.BackColor = Color.White
        PictureBox6.Width = (PictureBox6.Width - enlargenum)
        PictureBox6.Height = (PictureBox6.Height - enlargenum)
        PictureBox6.Location = New Point(PictureBox6.Location.X + enlargenum / 2, PictureBox6.Location.Y + enlargenum / 2)
    End Sub


    'Private Sub PictureBox5_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
    '    PictureBox5.BringToFront()
    '    PictureBox5.BackColor = Color.Blue
    '    PictureBox5.Width = (PictureBox5.Width + enlargenum)
    '    PictureBox5.Height = (PictureBox5.Height + enlargenum)
    '    PictureBox5.Location = New Point(PictureBox5.Location.X - enlargenum / 2, PictureBox5.Location.Y - enlargenum / 2)

    'End Sub

    'Private Sub PictureBox5_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
    '    PictureBox5.BackColor = Color.White
    '    PictureBox5.Width = (PictureBox5.Width - enlargenum)
    '    PictureBox5.Height = (PictureBox5.Height - enlargenum)
    '    PictureBox5.Location = New Point(PictureBox5.Location.X + enlargenum / 2, PictureBox5.Location.Y + enlargenum / 2)

    'End Sub

    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frmProf As New FrmProfesionales


        frmProf.MdiParent = Me
        frmProf.Show()
    End Sub


    Private Sub MDIMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        ' FORMULARIO TRANSPARENTE
        '  Me.ControlBox = False
        '  Me.BackColor = Color.White
        '  Me.TransparencyKey = Color.White
        ' POSICIONAR EN LA PANTALLA
        Me.Top = 5
        Me.Left = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width - Me.Width - 5)
        MenuStrip1.DefaultDropDownDirection = ToolStripDropDownDirection.BelowLeft
        InformesToolStripMenuItem.Owner.DefaultDropDownDirection = ToolStripDropDownDirection.BelowLeft

        '   Dim s As String

        '   s = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
        '   MessageBox.Show("El separador decimal es: '" & s & "'")

        '        s = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator
        '        MessageBox.Show("El separador de miles es: '" & s & "'")
    
        'Call EstablecerFondoDegradadoParaFormLoad()
        'Call EstablecerImagenFondoParaFormLoad()


        cnn = New ConsultaBD
   
        cPubNombrePC = My.Computer.Name
        cPubIpLocal = TraeIpHost(cPubNombrePC)
    
        cnn = New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
      

    End Sub
    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Dim frmProf As New FrmProfesionales
        ' If My.Forms.frmLogin.ShowDialog() = Windows.Forms.DialogResult.Cancel Then
        ' Exit Sub
        ' End If
        frmProf.MdiParent = Me.MdiParent
        frmProf.Show()
    End Sub


    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Dim frmMante As New FrmMantenimiento
        frmMante.MdiParent = Me.MdiParent
        frmMante.Show()
    End Sub


    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        Dim frmConta As New FrmAsiento
        frmConta.MdiParent = Me.MdiParent
        frmConta.Show()
    End Sub

    Private Sub PictureBox6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.Click
        Dim frmCaj As New FrmCaja
        frmCaj.MdiParent = Me.MdiParent
        frmCaj.Show()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Dim frmComproba As New FrmComprobantes
        frmComproba.MdiParent = Me.MdiParent
        frmComproba.Show()
    End Sub


 
    Private Sub BalanceSumaYSaldoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceSumaYSaldoToolStripMenuItem.Click
        Dim frmBalance As New FrmBalanceSumaSaldo
        frmBalance.Show()
    End Sub

    Private Sub LibroMayorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LibroMayorToolStripMenuItem.Click
        Dim frmLibro As New FrmLibroMayor
        frmLibro.Show()
    End Sub

    Private Sub CajaGeneralToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajaGeneralToolStripMenuItem.Click
        Dim frmCajaGen As New frmCajaGeneral
        frmCajaGen.Show()
    End Sub

    Private Sub ExportarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarToolStripMenuItem.Click
        Dim frmExportar As New frmExportarArchivo
        frmExportar.Show()
    End Sub
End Class
   