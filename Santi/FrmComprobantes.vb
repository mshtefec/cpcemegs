﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Shared
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class FrmComprobantes
    Private cnn As New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
    Private DAComproba As MySqlDataAdapter
    Private DATotales As MySqlDataAdapter

    Private CMDComproba As MySqlCommandBuilder
    Private DSComproba As DataSet
    Private BSComproba As BindingSource

    'Imprimir Recibo
    Private DTImprimir As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long

    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter    


    Private Sub MuestraComprobantes(ByVal cCondicion As String)
        Try
            cnn.AbrirConexion()
            DAComproba = New MySqlDataAdapter
            DAComproba = cnn.consultaBDadapter("(comproba left join procesos on pro_codigo=com_proceso) left join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc", "com_fecha,com_nroasi,concat(trim(com_proceso),'-',trim(pro_nombre)) as proceso,com_unegos,com_nrocli,com_proceso,com_nrocom,com_total,afi_nombre as Destinatario", cCondicion)
            CMDComproba = New MySqlCommandBuilder(DAComproba)
            DSComproba = New DataSet
            DAComproba.Fill(DSComproba, "comproba")
            BSComproba = New BindingSource
            BSComproba.DataSource = DSComproba.Tables("comproba")
            DAComproba = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "tot_item,tot_nrocuo,tot_nropla,pla_nombre,tot_debe,tot_haber,tot_fecven,tot_nroasi", "tot_fecha>='" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' Order by tot_item,tot_nrocuo")

            DAComproba.Fill(DSComproba, "totales")
            Dim DRComprobantes As New DataRelation("MaestroDetalle", DSComproba.Tables("comproba").Columns("com_nroasi"), DSComproba.Tables("totales").Columns("tot_nroasi"), False)
            '   DSComproba.Container
            DSComproba.Relations.Add(DRComprobantes)
            UGComproba.DataSource = DSComproba
            With UGComproba.DisplayLayout

                With .Bands(0)
                    .Override.FilterUIType = FilterUIType.Default
                    .Columns(0).Header.Caption = "Fecha"
                    .Columns(0).Width = 130
                    '.Columns(0).Hidden = False
                    .Columns(1).Header.Caption = "Asiento"
                    .Columns(1).Width = 70
                    .Columns(1).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    '.Columns(1).Hidden = False
                    .Columns(2).Header.Caption = "Proceso"
                    .Columns(2).Width = 300
                    .Columns(3).Header.Caption = "Institucion"
                    .Columns(3).Width = 40
                    .Columns(3).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center
                    .Columns(4).Header.Caption = "Delegacion"
                    .Columns(4).Width = 40
                    .Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center
                    .Columns(5).Hidden = True
                    .Columns(6).Header.Caption = "Comprobante"
                    .Columns(6).Width = 70
                    .Columns(6).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    .Columns(7).Header.Caption = "Importe"
                    .Columns(7).Width = 90
                    .Columns(7).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    .Columns(7).Format = "c"
                    .Columns(7).PromptChar = ""
                    .Columns(7).Style = ColumnStyle.Double
                    .Columns(7).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                    .Columns(8).Width = 250
                End With
                With .Bands(1)
                    .Override.FilterUIType = FilterUIType.Default
                    .Columns(0).Header.Caption = "Item"
                    .Columns(0).Width = 30
                    .Columns(1).Header.Caption = "Cuota"
                    .Columns(1).Width = 30
                    .Columns(2).Header.Caption = "Cuenta"
                    .Columns(2).Width = 80
                    .Columns(3).Header.Caption = "Descripción"
                    .Columns(3).Width = 220
                    .Columns(4).Header.Caption = "Debe"
                    .Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    .Columns(4).Width = 90
                    .Columns(4).Format = "c"
                    .Columns(4).PromptChar = ""
                    .Columns(4).Style = ColumnStyle.Double
                    .Columns(4).FormatInfo = System.Globalization.CultureInfo.CurrentCulture

                    .Columns(5).Header.Caption = "Haber"
                    .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    .Columns(5).Width = 90
                    .Columns(5).Format = "c"
                    .Columns(5).PromptChar = ""
                    .Columns(5).Style = ColumnStyle.Double
                    .Columns(5).FormatInfo = System.Globalization.CultureInfo.CurrentCulture

                    .Columns(6).Header.Caption = "Vencimiento"
                    .Columns(6).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center
                    .Columns(6).Width = 80
                    .Columns(6).Format = "d"
                    .Columns(6).PromptChar = ""
                    .Columns(6).Style = ColumnStyle.Date

                    .Columns(7).Hidden = True

                    .Columns(0).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(0).Header.Appearance.ForeColor = Color.Black
                    .Columns(0).Header.Appearance.BackColor = Color.Red
                    .Columns(0).Header.Appearance.BackColor2 = Color.White
                    .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(1).Header.Appearance.ForeColor = Color.Black
                    .Columns(1).Header.Appearance.BackColor = Color.Red
                    .Columns(1).Header.Appearance.BackColor2 = Color.White
                    .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(2).Header.Appearance.ForeColor = Color.Black
                    .Columns(2).Header.Appearance.BackColor = Color.Red
                    .Columns(2).Header.Appearance.BackColor2 = Color.White
                    .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(3).Header.Appearance.ForeColor = Color.Black
                    .Columns(3).Header.Appearance.BackColor = Color.Red
                    .Columns(3).Header.Appearance.BackColor2 = Color.White
                    .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(4).Header.Appearance.ForeColor = Color.Black
                    .Columns(4).Header.Appearance.BackColor = Color.Red
                    .Columns(4).Header.Appearance.BackColor2 = Color.White
                    .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(5).Header.Appearance.ForeColor = Color.Black
                    .Columns(5).Header.Appearance.BackColor = Color.Red
                    .Columns(5).Header.Appearance.BackColor2 = Color.White
                    .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(6).Header.Appearance.ForeColor = Color.Black
                    .Columns(6).Header.Appearance.BackColor = Color.Red
                    .Columns(6).Header.Appearance.BackColor2 = Color.White
                End With

            End With
            cnn.CerrarConexion()
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub FrmComprobantes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UDTHasta.Value = DateTime.Today
        UDTDesde.Value = DateTime.Today.AddDays(-7)
        MuestraComprobantes("com_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:00' order by com_fecha DESC")
    End Sub

    Private Sub UGComproba_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UGComproba.InitializeLayout
        e.Layout.Bands(1).Columns(1).FilterOperatorLocation = FilterOperatorLocation.Hidden
    End Sub

    Private Sub UGComproba_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGComproba.InitializeRow
        UGComproba.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            UGComproba.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
        Else
            UGComproba.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.Default
        End If
    End Sub

    Private Sub CButton1_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton1.ClickButtonArea

        Dim imprimir As New Impresiones
        imprimir.ImprimirAsiento(DSComproba.Tables(0).Rows(UGComproba.ActiveRow.Index).Item("com_nroasi"), DSComproba.Tables(0).Rows(UGComproba.ActiveRow.Index).Item("com_proceso"))
  
    End Sub

    Private Sub CButton2_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton2.ClickButtonArea
        MuestraComprobantes("mid(com_fecha,1,10) between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & "' order by com_fecha DESC")
    End Sub

    Private Sub ImpresionPrint_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles ImpresionPrint.PrintPage

        Dim NL As New NumLetra

        Dim NroReciboX, NroReciboY, FechaX, FechaY, AfiliadoX, AfiliadoY, MatriculaX, MatriculaY As Double
        Dim NroCliX, NroCliY, MontoX, MontoY, MontoLetrasX, MontoLetrasY As Double
        Dim Conceptos1X, Conceptos2X, Conceptos3X, Conceptos1Y, Conceptos2Y, Conceptos3Y As Double
        Dim Monto1X, Monto1Y As Double
        Dim LeyendaX, LeyendaY As Double
        Dim ProcesoX, ProcesoY As Double
        Dim ChequeX, ChequeY, BancoX, BancoY, SucX, SucY, MontoCHX, MontoCHY As Double
        Dim OperadorX, OperadorY As Double
        Dim InstitucionX, InstitucionY As Double
        Dim PiePaginaX, PiePaginaY As Double

        Dim Banco, Cheque, Suc, Importe As String

        Dim Monto, Letra As String
        Banco = ""

        'Inicializo las Variables
        'a ojo saque estos valores

        NroReciboX = 500
        NroReciboY = 14
        InstitucionX = 580
        InstitucionY = 14
        FechaX = 580
        FechaY = 50
        AfiliadoX = 55
        AfiliadoY = 138
        MatriculaX = 300
        MatriculaY = 138
        NroCliX = 55
        NroCliY = 165
        MontoX = 70
        MontoY = 217
        MontoLetrasX = 180
        MontoLetrasY = 217
        Conceptos1X = 70
        Conceptos1Y = 260
        Conceptos2X = 70
        Conceptos2Y = 273
        Conceptos3X = 70
        Conceptos3Y = 286
        Monto1X = 20
        Monto1Y = 310
        LeyendaX = 20
        LeyendaY = 330
        ProcesoX = 70
        ProcesoY = 242

        ChequeX = 20
        ChequeY = 350

        BancoX = 123
        BancoY = 350
        SucX = 309
        SucY = 350
        MontoCHX = 469
        MontoCHY = 350

        PiePaginaX = 20
        PiePaginaY = 505
        OperadorX = 500
        OperadorY = 100

        ' La fuente a usar
        Dim prFont As New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

        Try
            DAImprimir = cnn.consultaBDadapter("(((((comproba inner join procesos on pro_codigo=com_proceso) inner join " & _
                                               "farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join  " & _
                                               "operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) " & _
                                               "inner join plancuen on pla_nropla=tot_nropla) left join afiliado on " & _
                                               "afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "far_nombre as institucion, " & _
                                               "far_nrocli as delegacion, com_proceso as proceso,com_nrocom as comprobante, " & _
                                               "com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip, " & _
                                               "DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha, " & _
                                               "DATE_FORMAT(CAST(tot_fecven AS char),'%d/%m/%Y') as vencimiento, " & _
                                               "com_concepto1 as conceptos1, com_concepto2 as conceptos2,com_concepto3 as conceptos3, " & _
                                               "tot_item as item, tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip, " & _
                                               "tot_nrocuo as itemsubcta,CONCAT(tot_subpla,' ',afi_titulo, CAST(afi_matricula AS char)) as subcta, " & _
                                               "afi_nombre as subctadescrip, tot_debe as debe,tot_haber as haber,pro_reporte, " & _
                                               "afi_matricula as matricula,tot_tipdoc,tot_nrodoc,tot_titulo, " & _
                                               "tot_matricula", "tot_haber > 0  and com_nroasi=" & n_NroAsiento)
            DTImprimir = New DataTable
            DAImprimir.Fill(DTImprimir)

            If DTImprimir.Rows.Count > 0 Then

                DACheque = cnn.consultaBDadapter("totales", "tot_nrocheque as cheque, tot_debe-tot_haber as impche,tot_concepto " & _
                                                 "as desticheque, tot_estche,tot_concepto as Banco", "tot_nrocheque > 0 and tot_nroasi = " & n_NroAsiento)
                DTCheque = New DataTable
                DACheque.Fill(DTCheque)

                If DTCheque.Rows.Count > 0 Then
                    For Each rowCheque In DTCheque.Rows

                        If rowCheque("cheque") > 0 Then
                            Banco = rowCheque("banco")
                            Cheque = rowCheque("cheque")
                            Suc = "RCIA"
                            Importe = rowCheque("impche")

                            'imprimimo el nro del cheque
                            e.Graphics.DrawString(rowCheque("cheque"), prFont, Brushes.Black, ChequeX, ChequeY)
                            'imprimimo el banco del cheque
                            e.Graphics.DrawString(Banco, prFont, Brushes.Black, BancoX, BancoY)
                            'imprimimo la sucursal cheque
                            e.Graphics.DrawString(Suc, prFont, Brushes.Black, SucX, SucY)
                            'imprimimo el monto del cheque
                            e.Graphics.DrawString(Importe, prFont, Brushes.Black, MontoCHX, MontoCHY)

                            ChequeY += 15
                            BancoY += 15
                            SucY += 15
                            MontoCHY += 15

                        End If
                    Next
                End If

                For Each row In DTImprimir.Rows

                    Monto = "$ " & row("haber")

                    'imprimimo el comprobante
                    e.Graphics.DrawString(row("comprobante"), prFont, Brushes.Black, NroReciboX, NroReciboY)
                    'imprimo la institucion
                    If row("institucion") = "SIPRES" Then
                        e.Graphics.DrawString("(SIPRES)", prFont, Brushes.Black, InstitucionX, InstitucionY)
                    Else
                        e.Graphics.DrawString(" ", prFont, Brushes.Black, InstitucionX, InstitucionY)
                    End If
                    'imprimimo la fecha         
                    e.Graphics.DrawString(row("fecha"), prFont, Brushes.Black, FechaX, FechaY)
                    'imprimimo el OEPRADOR
                    e.Graphics.DrawString("Operador: " & row("operador"), prFont, Brushes.Black, OperadorX, OperadorY)
                    'imprimimo el Afiliado 
                    e.Graphics.DrawString(row("subctadescrip"), prFont, Brushes.Black, AfiliadoX, AfiliadoY)
                    'imprimimo la matricula
                    e.Graphics.DrawString("Matricula N°: " & row("matricula"), prFont, Brushes.Black, MatriculaX, MatriculaY)
                    'imprimimo la delegacion
                    'e.Graphics.DrawString(row("delegacion"), prFont, Brushes.Black, NroCliX, NroCliY)
                    e.Graphics.DrawString(ceros1(row("delegacion"), 5), prFont, Brushes.Black, NroCliX, NroCliY)
                    'imprimimo el monto
                    e.Graphics.DrawString(Monto, prFont, Brushes.Black, MontoX, MontoY)
                    'imprimimo el monto en letras
                    Letra = NL.Convertir(row("haber") & " ", True)
                    MontoLetrasX = (MontoX + Len(Monto) + 60)
                    e.Graphics.DrawString(" (SON PESOS: " & Letra & " )", prFont, Brushes.Black, MontoLetrasX, MontoLetrasY)
                    'imprimimo el detalle DEL PROCESO
                    e.Graphics.DrawString(row("procedescrip"), prFont, Brushes.Black, ProcesoX, ProcesoY)

                    'imprimo la descripcion
                    prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
                    e.Graphics.DrawString(row("conceptos1"), prFont, Brushes.Black, Conceptos1X, Conceptos1Y)
                    e.Graphics.DrawString(row("conceptos2"), prFont, Brushes.Black, Conceptos2X, Conceptos2Y)
                    e.Graphics.DrawString(row("conceptos3"), prFont, Brushes.Black, Conceptos3X, Conceptos3Y)

                    prFont = New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

                    ''imprimimo el detalle del cheque
                    e.Graphics.DrawString("Cheque               Banco                                       Sucursal                            Importe ", prFont, Brushes.Black, LeyendaX, LeyendaY)

                    'imprimimo la forma de pago
                    If Banco <> "" Then
                        e.Graphics.DrawString("Efectivo : ............................................................................ 0 ", prFont, Brushes.Black, Monto1X, Monto1Y)
                    Else
                        e.Graphics.DrawString("Efectivo : ............................................................................  " & Monto, prFont, Brushes.Black, Monto1X, Monto1Y)
                    End If

                    prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)

                    'imprimimo el pie de pagina
                    e.Graphics.DrawString("LOS CHEQUES RECIBIDOS SERAN ACREDITADOS A V/CUENTA PERSONAL UNA VEZ HECHOS EFECTIVOS", prFont, Brushes.Black, PiePaginaX, PiePaginaY)
                Next

                'indicamos que hemos llegado al final de la pagina
                e.HasMorePages = False
                n_NroAsiento = 0
            End If
        Catch ex As Exception
            e.Cancel = True
            MessageBox.Show("ERROR : " & ex.Message, "IMPRESION DE RECIBOS ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cbtRecibos_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cbtRecibos.ClickButtonArea

        n_NroAsiento = DSComproba.Tables(0).Rows(UGComproba.ActiveRow.Index).Item("com_nroasi")
        If ImpresionDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            ImpresionPrint.PrinterSettings.Copies = ImpresionDialog.PrinterSettings.Copies
            ImpresionPrint.Print()
        Else
            Exit Sub
        End If
    End Sub

End Class