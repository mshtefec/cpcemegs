﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComprobantes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim DesignerRectTracker1 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmComprobantes))
        Dim DesignerRectTracker2 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker3 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker4 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker5 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker6 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Me.UGComproba = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CButton1 = New CButtonLib.CButton
        Me.UDTDesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
        Me.UDTHasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.CButton2 = New CButtonLib.CButton
        Me.ImpresionPrint = New System.Drawing.Printing.PrintDocument
        Me.ImpresionDialog = New System.Windows.Forms.PrintDialog
        Me.cbtRecibos = New CButtonLib.CButton
        CType(Me.UGComproba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGComproba
        '
        Me.UGComproba.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
        Me.UGComproba.DisplayLayout.Appearance = Appearance1
        Me.UGComproba.DisplayLayout.InterBandSpacing = 10
        Me.UGComproba.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGComproba.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGComproba.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGComproba.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.TextHAlignAsString = "Left"
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGComproba.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGComproba.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGComproba.DisplayLayout.Override.RowAppearance = Appearance4
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGComproba.DisplayLayout.Override.RowSelectorAppearance = Appearance6
        Me.UGComproba.DisplayLayout.Override.RowSelectorWidth = 12
        Me.UGComproba.DisplayLayout.Override.RowSpacingBefore = 2
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance5.ForeColor = System.Drawing.Color.Black
        Me.UGComproba.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGComproba.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGComproba.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
        Me.UGComproba.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGComproba.Location = New System.Drawing.Point(0, 40)
        Me.UGComproba.Name = "UGComproba"
        Me.UGComproba.Size = New System.Drawing.Size(984, 348)
        Me.UGComproba.TabIndex = 1
        Me.UGComproba.Text = "Comprobantes"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(28, 12)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(59, 19)
        Me.CheckBox1.TabIndex = 2
        Me.CheckBox1.Text = "Filtros"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CButton1
        '
        Me.CButton1.BorderShow = False
        DesignerRectTracker1.IsActive = False
        DesignerRectTracker1.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker1.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CButton1.CenterPtTracker = DesignerRectTracker1
        DesignerRectTracker2.IsActive = False
        DesignerRectTracker2.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker2.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CButton1.FocusPtTracker = DesignerRectTracker2
        Me.CButton1.Image = CType(resources.GetObject("CButton1.Image"), System.Drawing.Image)
        Me.CButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton1.ImageIndex = 0
        Me.CButton1.ImageSize = New System.Drawing.Size(32, 32)
        Me.CButton1.Location = New System.Drawing.Point(50, 411)
        Me.CButton1.Name = "CButton1"
        Me.CButton1.Size = New System.Drawing.Size(132, 39)
        Me.CButton1.TabIndex = 3
        Me.CButton1.Text = "Imprimir"
        '
        'UDTDesde
        '
        Me.UDTDesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTDesde.Location = New System.Drawing.Point(187, 12)
        Me.UDTDesde.Name = "UDTDesde"
        Me.UDTDesde.Size = New System.Drawing.Size(96, 21)
        Me.UDTDesde.TabIndex = 4
        '
        'UDTHasta
        '
        Me.UDTHasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTHasta.Location = New System.Drawing.Point(336, 12)
        Me.UDTHasta.Name = "UDTHasta"
        Me.UDTHasta.Size = New System.Drawing.Size(96, 21)
        Me.UDTHasta.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(140, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 15)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Desde:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(291, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 15)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Hasta:"
        '
        'CButton2
        '
        Me.CButton2.BorderShow = False
        DesignerRectTracker3.IsActive = False
        DesignerRectTracker3.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker3.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CButton2.CenterPtTracker = DesignerRectTracker3
        Me.CButton2.FocalPoints.CenterPtX = 0.6610169!
        Me.CButton2.FocalPoints.CenterPtY = 0.34375!
        Me.CButton2.FocalPoints.FocusPtX = 0.0!
        Me.CButton2.FocalPoints.FocusPtY = 0.0!
        DesignerRectTracker4.IsActive = False
        DesignerRectTracker4.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker4.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CButton2.FocusPtTracker = DesignerRectTracker4
        Me.CButton2.Image = CType(resources.GetObject("CButton2.Image"), System.Drawing.Image)
        Me.CButton2.ImageIndex = 0
        Me.CButton2.ImageSize = New System.Drawing.Size(24, 24)
        Me.CButton2.Location = New System.Drawing.Point(438, 5)
        Me.CButton2.Name = "CButton2"
        Me.CButton2.Size = New System.Drawing.Size(59, 32)
        Me.CButton2.TabIndex = 8
        Me.CButton2.Text = ""
        '
        'ImpresionPrint
        '
        '
        'ImpresionDialog
        '
        Me.ImpresionDialog.UseEXDialog = True
        '
        'cbtRecibos
        '
        Me.cbtRecibos.BorderShow = False
        DesignerRectTracker5.IsActive = False
        DesignerRectTracker5.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker5.TrackerRectangle"), System.Drawing.RectangleF)
        Me.cbtRecibos.CenterPtTracker = DesignerRectTracker5
        DesignerRectTracker6.IsActive = False
        DesignerRectTracker6.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker6.TrackerRectangle"), System.Drawing.RectangleF)
        Me.cbtRecibos.FocusPtTracker = DesignerRectTracker6
        Me.cbtRecibos.Image = CType(resources.GetObject("cbtRecibos.Image"), System.Drawing.Image)
        Me.cbtRecibos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbtRecibos.ImageIndex = 0
        Me.cbtRecibos.ImageSize = New System.Drawing.Size(32, 32)
        Me.cbtRecibos.Location = New System.Drawing.Point(323, 411)
        Me.cbtRecibos.Name = "cbtRecibos"
        Me.cbtRecibos.Size = New System.Drawing.Size(132, 39)
        Me.cbtRecibos.TabIndex = 9
        Me.cbtRecibos.Text = "Recibos"
        '
        'FrmComprobantes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.cbtRecibos)
        Me.Controls.Add(Me.CButton2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UDTHasta)
        Me.Controls.Add(Me.UDTDesde)
        Me.Controls.Add(Me.CButton1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.UGComproba)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmComprobantes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comprobantes"
        CType(Me.UGComproba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGComproba As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CButton1 As CButtonLib.CButton
    Friend WithEvents UDTDesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTHasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CButton2 As CButtonLib.CButton
    Friend WithEvents ImpresionPrint As System.Drawing.Printing.PrintDocument
    Friend WithEvents ImpresionDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents cbtRecibos As CButtonLib.CButton
End Class
