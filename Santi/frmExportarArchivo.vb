﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports System.Data.OleDb
Imports MySql.Data.Types
Imports System.IO

Public Class frmExportarArchivo

    Private cnn As New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
    Private cmdExportar As MySqlCommandBuilder
    Private mysqlFecha As MySqlDateTime

    Private DTExportar As New DataTable
    Private DAExportar As New MySqlDataAdapter

    Private DATotales As MySqlDataAdapter
    Private DTTotales As DataTable

    Private DTPlancue As New DataTable
    Private DAPlancue As New MySqlDataAdapter

    Private DAFarcat As MySqlDataAdapter
    Private DTFarcat As DataTable

    Private NOrden As Integer

    Private Sub frmExportarArchivo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Cargo el farcat en la grilla
        Try
            DAFarcat = cnn.consultaBDadapter("farcat", , )
            Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
            DTFarcat = New DataTable
            DAFarcat.Fill(DTFarcat)

            If DTFarcat.Rows.Count > 0 Then
                For Each rowFarcat As DataRow In DTFarcat.Rows
                    UltraTree1.Nodes.Add("" & rowFarcat.Item("far_nombre").ToString & " " & rowFarcat.Item("far_sucursal").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
                Next
            End If

            For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
                Nodo.CheckedState = CheckState.Checked
            Next

            UltraTree1.Focus()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar las Sucursales", "SUCURSALES", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End Try
    End Sub

    Private Sub frmExportarArchivo_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint
        FondoDegrade(Color.Blue, Color.White, Me.Width, Me.Height, e)
    End Sub

    Private Sub UltraTree1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraTree1.KeyPress, UDThasta.KeyPress, UDTdesde.KeyPress, CBtExportar.KeyPress, TxOrden.KeyPress
        Tabular(e)
    End Sub

    Private Sub CBtExportar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.ClickButtonArea
        UltraProgressBar1.Maximum = 100
        UltraProgressBar1.Value = 0
        UltraProgressBar1.Refresh()

        ExportarRetenciones()
    End Sub

    Private Sub ExportarRetenciones()
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty

        'Para el archivo
        Dim strStreamW As Stream = Nothing
        Dim strStreamWriter As StreamWriter = Nothing
        Dim PathArchivo As String

        Try

            DAExportar = New MySqlDataAdapter
            DTExportar = New DataTable

            'Borro y creo el archivo
            If Directory.Exists("C:\CpceMEGS") = False Then ' si no existe la carpeta se crea
                Directory.CreateDirectory("C:\CpceMEGS")
            End If

            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            PathArchivo = "C:\CpceMEGS\RETdgr.txt"

            If File.Exists(PathArchivo) Then

                My.Computer.FileSystem.DeleteFile(PathArchivo)
                strStreamW = File.Create(PathArchivo) ' lo creamos

            Else
                strStreamW = File.Create(PathArchivo) ' lo creamos
            End If


            'Cargo las unidades de negocio el y nro de cliente
            For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
                If Nodo.CheckedState = CheckState.Checked Then
                    If cInstituciones.Length = 0 Then
                        cUnegos = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                        cInstituciones = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                    Else
                        cUnegos += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                        cInstituciones += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                    End If
                End If
            Next

            If TxOrden.TextLength = 0 Then
                MessageBox.Show("DEBE SELECCIONAR UN NUMERO DE ORDEN PARA INICIAR LA EXPORTACION", "INICIAR EXPORTACION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.TxOrden.Focus()
                Exit Sub
            End If

            'No se marco ninguna entidad
            If cUnegos <> String.Empty Then

                DAExportar = cnn.consultaBDadapter("totales inner join afiliado on tot_tipdoc = afi_tipdoc and " & _
                                                   "tot_nrodoc = afi_nrodoc and afi_tipo ='A'", "tot_nroasi,sum(tot_debe) as tot_debe,tot_nrolegali, " & _
                                                   "sum(tot_haber) as tot_haber,afi_nombre,afi_direccion,afi_cuit,tot_sobre", "tot_unegos in (" & cUnegos & ") " & _
                                                   "and tot_nrocli in (" & cInstituciones & ") and tot_fecha >='" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' " & _
                                                   "and tot_fecha <='" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' " & _
                                                   "and tot_nropla = '21030200' and tot_proceso = '02LIHN' GROUP BY tot_nroasi ORDER BY tot_nroasi,tot_item")

                DAExportar.Fill(DTExportar)

                If DTExportar.Rows.Count > 0 Then

                    UltraProgressBar1.Maximum = DTExportar.Rows.Count

                    strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)

                    Dim cadena As String = ""
                    Dim Profesional As String = ""
                    Dim Direccion As String = ""
                    Dim Cuit As String = ""
                    Dim Dia As String = ""
                    Dim Mes As String = ""
                    Dim Año As String = ""
                    Dim Wret, WMonto, c142monto As String
                    Dim Fecha As String = Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd")

                    NOrden = Cero(Convert.ToString(Me.TxOrden.Text), 6)

                    For Each rowExportar As DataRow In DTExportar.Rows

                        WMonto = rowExportar.Item("tot_sobre")
                        Wret = Math.Round(rowExportar.Item("tot_haber") * 1.1, 2)
                        c142monto = Math.Round(rowExportar.Item("tot_haber"), 2)

                        Profesional = Espacio(rowExportar.Item("afi_nombre"), 30)

                        If Profesional.Length > 30 Then
                            Profesional = Profesional.Substring(0, 30)
                        End If

                        Direccion = Espacio(rowExportar.Item("afi_direccion"), 50)

                        If Direccion.Length > 50 Then
                            Direccion = Direccion.Substring(0, 50)
                        End If

                        Cuit = Trim(rowExportar.Item("afi_cuit"))

                        If Cuit.Length > 0 Then
                            If Cuit = "  -        - " Then
                                Cuit = "00000000000"
                            Else
                                Cuit = Replace(Cuit, "-", "")

                                If Cuit.Length <= 11 Then
                                    Cuit = EspacioCero(Cuit, 11)
                                Else
                                    Cuit = Cuit.Substring(0, 11)
                                End If

                            End If
                        Else
                            Cuit = "00000000000"
                        End If

                        'Extraigo la fecha
                        Dia = Mid(Fecha.ToString, 9, 2)
                        Mes = Mid(Fecha.ToString, 6, 2)
                        Año = Mid(Fecha.ToString, 1, 4)

                        'Aplico formulas
                        Wret = Math.Round(IIf(c142monto <> 0, Math.Round(Math.Round(WMonto * 0.025, 2) * 1.1, 2), 0), 2)
                        c142monto = IIf(c142monto <> 0, 2.5, 0) * 100

                        'Completo con ceros
                        Wret = Cero(Wret * 100, 11)
                        WMonto = Cero(WMonto * 100, 11)
                        c142monto = Cero(c142monto, 11)

                        'Escribo
                        strStreamWriter.WriteLine("30553526996" & "01" & NOrden & Cuit & Profesional & Direccion & Dia & Mes & Año & Wret & "10" & WMonto & c142monto & "000")

                        NOrden += 1
                    Next

                    strStreamWriter.Close() ' cerramos

                Else

                    MessageBox.Show("No se encontraron datos para el periodo y las entidades seleccionadas", "SIN DATOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.UltraTree1.Focus()
                    Exit Sub

                End If
            Else
                MessageBox.Show("Debe Seleccionar al menos una Entidad", "SELECCIONE ENTIDAD", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.UltraTree1.Focus()
                Exit Sub
            End If

            MessageBox.Show("Archivo Generado Exitosamente", "ARCHIVO EXPORTADO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Catch ex As Exception
            MessageBox.Show("No se pudo exportar el archivo. Se produjo el Sig. Error " & ex.Message.ToString, "ERROR AL EXPORTAR ARCHIVO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            strStreamWriter.Close() ' cerramos
        Finally

        End Try

    End Sub



    Private Function Cero(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "0"
        For i = 1 To Cantidad
            cuantos = cuantos & "0"
        Next i
        Cero = Mid(cuantos, 1, Cantidad - Len(numero)) & numero

    End Function

    Private Function Espacio(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = " "

        For i = 1 To Cantidad
            cuantos = " " & cuantos
        Next i
        Espacio = numero & Mid(cuantos, 1, Cantidad - Len(numero))

    End Function

    Private Function EspacioCero(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "0"

        For i = 1 To Cantidad
            cuantos = "0" & cuantos
        Next i
        EspacioCero = numero & Mid(cuantos, 1, Cantidad - Len(numero))

    End Function


End Class