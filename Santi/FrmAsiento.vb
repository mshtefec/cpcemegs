﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports Infragistics.Shared
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Specialized
Imports MSScriptControl

Public Class FrmAsiento
    Private cnn As New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
    Private DSProcesos As DataSet
    Private DAProcesos As MySqlDataAdapter
    Private BSProcesos As BindingSource
    Private DSDetalles As DataSet
    Private DADetalles As MySqlDataAdapter
    Private BSDetalles As BindingSource
    Private DTSubCuenta As DataTable
    Private DTCheques As DataTable
    Private DTCuotasServicio As New DataTable
    Private nCaja As Integer
    Private nZeta As Integer
    Private Prestamo As Double = 0

    Dim Reporte As String = ""

    'Imprimir el Recibo
    Private DTImprimir As DataTable
    Private DTImpDeta As DataTable
    Private DTAsiento As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long

    'Para guardar los cheques aparte
    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter
    Private CantCheques As Integer = 0


    Private Sub CargaProcesos()
        DAProcesos = cnn.consultaBDadapter("procesos", "pro_codigo as proceso,pro_nombre as Descripcion,pro_buscaf,pro_bloqfech,pro_cajachica,pro_instit,pro_cpto1,pro_cpto2,pro_cpto3,pro_leyenda1,pro_leyenda2,pro_leyenda3,pro_leyenda4,pro_reporte", "1 order by pro_nombre")
        DSProcesos = New DataSet
        DAProcesos.Fill(DSProcesos, "procesos")
        BSProcesos = New BindingSource
        BSProcesos.DataSource = Me.DSProcesos.Tables(0)
        UltraDateTimeEditor1.DateTime = Now        
    End Sub

    Private Sub SeleccionaProceso()
        Dim row As DataRowView = BSProcesos.Current
        Me.UltraTextEditor1.Text = row.Item("proceso")
        Me.UltraLabel1.Text = row.Item("Descripcion")
        Me.TxtConcepto1.Text = row.Item("pro_cpto1")
        Me.TxtConcepto2.Text = row.Item("pro_cpto2")
        Me.TxtConcepto3.Text = row.Item("pro_cpto3") & ""

        Reporte = row.Item("pro_reporte")

        Me.UltraLabel1.Visible = True
        Me.UGProcesos.Visible = False
        Me.TxtDebe.Text = ""
        Me.TxtHaber.Text = ""
        If row.Item("pro_bloqfech").ToString = "Si" Then
            UltraDateTimeEditor1.ReadOnly = True
            UltraComboEditor1.ReadOnly = True
        Else
            UltraDateTimeEditor1.ReadOnly = False
            UltraComboEditor1.ReadOnly = False
        End If
        If row.Item("pro_cajachica") = "Si" Then
            If cnn.CajaActiva Then
                nZeta = cnn.Zeta
                nCaja = cnn.Caja
            Else
                nZeta = 0
                nCaja = 0
                CButton2.Enabled = False
            End If
        Else
            nZeta = 0
            nCaja = 0
        End If
    End Sub

    Private Sub LimpiarProceso()
        Me.UltraTextEditor1.Text = ""
        Me.UltraLabel1.Visible = False
        Me.UGProcesos.Visible = False
        Me.TxtDebe.Text = ""
        Me.TxtHaber.Text = ""
        Me.TxtConcepto1.Text = ""
        Me.TxtConcepto2.Text = ""
        Me.TxtConcepto3.Text = ""
        Me.UltraDateTimeEditor1.DateTime = Now
        Me.UltraComboEditor1.Value = Nothing
        Me.UGDetalles.DataSource = Nothing
        nZeta = 0
        nCaja = 0
        Prestamo = 0
        Me.CButton2.Enabled = True
        CantCheques = 0
    End Sub

    Private Sub MuestraProcesos()
        If Me.UGProcesos.Visible Then
            SeleccionaProceso()
        Else

            Dim fila As Integer = BSProcesos.Find("proceso", UltraTextEditor1.Text)

            If fila > 0 Then
                BSProcesos.Position = fila
                SeleccionaProceso()
            Else
                Me.UltraLabel1.Visible = False
                Me.UGProcesos.DataSource = Me.BSProcesos

                With UGProcesos.DisplayLayout.Bands(0)
                    .Columns(1).Width = 420
                    .Columns(2).Hidden = True
                    .Columns(3).Hidden = True
                    .Columns(4).Hidden = True
                    .Columns(5).Hidden = True
                    .Columns(6).Hidden = True
                    .Columns(7).Hidden = True
                    .Columns(8).Hidden = True
                    .Columns(9).Hidden = True
                    .Columns(10).Hidden = True
                    .Columns(11).Hidden = True
                    .Columns(12).Hidden = True
                End With
                Me.UGProcesos.Top = 37
                Me.UGProcesos.Left = 87
                Me.UGProcesos.Visible = True
                Me.UGProcesos.Height = 252
                Me.UGProcesos.Width = 560
            End If
        End If
    End Sub

    Private Sub MuestraDestinatario(ByVal lLimpia As Boolean)
        Dim rowDesti As UltraGridRow = Me.UGDetalles.ActiveRow
        If lLimpia Then
            Me.UGProcesos.Visible = False
            GrabarDestinatario(rowDesti, lLimpia)
        Else
            Dim frmDesti As New FrmDestinatario
            If Me.UGProcesos.Visible Then
                Me.UGProcesos.Visible = False
                GrabarDestinatario(rowDesti, lLimpia)
            Else
                If Trim(rowDesti.Cells(3).Text) <> "" Then
                    frmDesti.Condicion = "afi_titulo='" & Mid(rowDesti.Cells(3).Text, 1, 2) & "' and afi_matricula=" & CInt(Mid(rowDesti.Cells(3).Text, 3))
                Else
                    frmDesti.Condicion = "afi_nombre like '" & Trim(rowDesti.Cells(2).Text) & "%'"
                End If
                frmDesti.CargaDestinatario()
                If frmDesti.MatriculaDestinatario <> "" Then
                    rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                    rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                    rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                    rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario
                    GrabarDestinatario(rowDesti, lLimpia)
                Else
                    frmDesti.ShowDialog()
                    If frmDesti.DialogResult = Windows.Forms.DialogResult.OK Then
                        rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                        rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                        rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                        rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario

                        GrabarDestinatario(rowDesti, lLimpia)
                    End If
                End If
            End If
            frmDesti.Dispose()
        End If

    End Sub

    Private Sub GrabarDestinatario(ByRef rowDesti As UltraGridRow, ByVal lLimpia As Boolean)
        If lLimpia Then
            rowDesti.Cells("Destinatario").Value = ""
            rowDesti.Cells("matricula").Value = ""
            rowDesti.Cells("TipoDoc").Value = ""
            rowDesti.Cells("NroDoc").Value = 0
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("subcuenta") = ""
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = ""
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = 0
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = False
        Else
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Subcuenta") = rowDesti.Cells("TipoDoc").Value & Format(rowDesti.Cells("NroDoc").Value, "00000000")
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = rowDesti.Cells("Matricula").Value
            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = True
            Select Case UltraTextEditor1.Text.Substring(2, 1)

                Case "R" ' si es recibo , busco cuotas a cancelar
                    Dim frmCuotas As New FrmCuotasImpagas(cnn, Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta"), Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_tipmov"), rowDesti.Cells("TipoDoc").Value, rowDesti.Cells("nrodoc").Value)
                    frmCuotas.Text = "Cuotas impagas de " & rowDesti.Cells(2).Value
                    frmCuotas.ShowDialog()
                    If frmCuotas.DialogResult = Windows.Forms.DialogResult.OK Then
                        Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = frmCuotas.GetMontoImpagas
                        DTCuotasServicio = frmCuotas.GetCuotasCanceladas
                        ' agrego columna de item
                        Dim newCol As DataColumn
                        newCol = New DataColumn("item", Type.GetType("System.Int32"))
                        newCol.DefaultValue = rowDesti.Cells("item").Value - 1
                        DTCuotasServicio.Columns.Add(newCol)

                    End If
                Case "P" ' si es orden da pago
                    If Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pla_cuotas") = "Si" Then
                        FrmCuotasPrestamo.ShowDialog()
                        If FrmCuotasPrestamo.DialogResult = Windows.Forms.DialogResult.OK Then
                            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("debe") = FrmCuotasPrestamo.ImportePrestamo
                            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuotas") = FrmCuotasPrestamo.CuotasPrestamo
                            Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("fecven") = FrmCuotasPrestamo.Vencimiento
                            Me.Prestamo = FrmCuotasPrestamo.Prestamo
                        End If
                    End If
            End Select
        End If
        SumoTotales()
    End Sub

    Private Sub FrmAsiento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargaProcesos()
    End Sub

    Private Sub UltraTextEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UltraTextEditor1.EditorButtonClick
        If e.Button.Key = "Right" Then
            MuestraProcesos()
            If Me.UltraLabel1.Visible Then
                Me.UltraComboEditor1.SelectedIndex = DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_instit") - 1 ' Convert.ToInt16(Mid(Me.UltraTextEditor1.Text, 1, 2)) - 1
                MuestraDetalles()
                SumoTotales()
            End If
        Else
            LimpiarProceso()
        End If
    End Sub

    Private Sub AgregarDetalleVacio()
        Dim nR As Integer = DSDetalles.Tables(0).Rows.Count
        DSDetalles.Tables(0).Rows.Add()
        DSDetalles.Tables(0).Rows(nR).Item("pto_item") = nR + 1
        DSDetalles.Tables(0).Rows(nR).Item("Cuenta") = ""
        DSDetalles.Tables(0).Rows(nR).Item("SubCuenta") = ""
        DSDetalles.Tables(0).Rows(nR).Item("Descripcion") = ""
        DSDetalles.Tables(0).Rows(nR).Item("pla_servicio") = "No"
        DSDetalles.Tables(0).Rows(nR).Item("Matricula") = ""
        DSDetalles.Tables(0).Rows(nR).Item("pto_tipmov") = "I"
        DSDetalles.Tables(0).Rows(nR).Item("pto_valor") = 0
        DSDetalles.Tables(0).Rows(nR).Item("pto_formula") = ""
        DSDetalles.Tables(0).Rows(nR).Item("Haber") = 0
        DSDetalles.Tables(0).Rows(nR).Item("Debe") = 0
    End Sub

    Private Sub MuestraDetalles()
        DADetalles = cnn.consultaBDadapter("(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)", "pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio,pla_cuotas", "pto_codpro='" & Me.UltraTextEditor1.Text & "' order by pto_item")
        DSDetalles = New DataSet
        DADetalles.Fill(DSDetalles, "detalles")
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        newCol.DefaultValue = UltraTextEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))
        newCol.DefaultValue = UltraComboEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = False
        DSDetalles.Tables("detalles").Columns.Add(newCol)

        ' si es manual agrego fila
        If DSDetalles.Tables(0).Rows.Count = 0 Then
            AgregarDetalleVacio()
            AgregarDetalleVacio()
        End If

        DTSubCuenta = New DataTable("subcuenta")
        With DTSubCuenta
            .Columns.Add("proceso", Type.GetType("System.String"))
            .Columns.Add("item", Type.GetType("System.Int32"))
            .Columns.Add("Destinatario", Type.GetType("System.String"))
            .Columns.Add("Matricula", Type.GetType("System.String"))
            .Columns.Add("Buscar", Type.GetType("System.String"))
            .Columns.Add("Eliminar", Type.GetType("System.String"))
            .Columns.Add("TipoDoc", Type.GetType("System.String"))
            .Columns.Add("NroDoc", Type.GetType("System.Int32"))

        End With

        For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
            If rowItem.Item("pla_servicio") = "Si" Then
                AgregarSubcuenta(rowItem)
            End If
        Next
        DSDetalles.Tables.Add(DTSubCuenta)


        DTCheques = New DataTable("cheques")
        With DTCheques
            .Columns.Add("proceso", Type.GetType("System.String"))
            .Columns.Add("item", Type.GetType("System.Int32"))
            .Columns.Add("Banco", Type.GetType("System.String"))
            .Columns.Add("Cheque", Type.GetType("System.String"))
            .Columns.Add("FechaEmision", Type.GetType("System.String"))
            .Columns.Add("FechaDiferida", Type.GetType("System.String"))
            .Columns.Add("Monto", Type.GetType("System.Double"))
            .Columns.Add("Agregar", Type.GetType("System.String"))
            .Columns.Add("Eliminar", Type.GetType("System.String"))
        End With

        ' los cheques por ahora no los cargo aca, se cargan en el proceso de asignacion

        For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
            If Mid(rowItem.Item("Subcuenta"), 1, 3) = "BCO" And rowItem.Item("pto_tipmov") = "D" Then
                AgregarCheques(rowItem)
            End If
        Next
        DSDetalles.Tables.Add(DTCheques)



        Dim DRDetalles As New DataRelation("RelacionCheques", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("cheques").Columns("item"))
        DSDetalles.Relations.Add(DRDetalles)
        DRDetalles = New DataRelation("RelacionSubcuenta", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("subcuenta").Columns("item"))
        DSDetalles.Relations.Add(DRDetalles)

        BSDetalles = New BindingSource
        BSDetalles.DataSource = DSDetalles
        Me.UGDetalles.DataSource = BSDetalles


        With Me.UGDetalles.DisplayLayout

            With .Bands(0)

                .Columns(0).Width = 90
                .Columns(0).CellActivation = Activation.NoEdit
                .Columns(1).Width = 400
                .Columns(1).CellActivation = Activation.NoEdit
                .Columns(2).Width = 130
                .Columns(2).CellAppearance.TextHAlign = HAlign.Right
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(2).Format = "c"
                .Columns(2).PromptChar = ""
                .Columns(2).Style = ColumnStyle.Double
                .Columns(2).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                .Columns(3).Width = 130
                .Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .Columns(3).Format = "c"
                .Columns(3).PromptChar = ""
                .Columns(3).Style = ColumnStyle.Double
                .Columns(3).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                .Columns(3).CellActivation = Activation.NoEdit
                .Columns(4).Hidden = True
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
                .Columns(9).Hidden = True
                .Columns(10).Hidden = True
                .Columns(11).Hidden = True
                .Columns(12).Hidden = True
                .Columns(13).Hidden = True
                .Columns(14).Hidden = True
                .Columns(15).Hidden = True
                .Columns(16).Hidden = True
                .Columns(17).Hidden = True
            End With
            With .Bands(1)
                '   With .Header.Appearance
                '.BackColor = Color.Aqua
                'End With
                .Columns(0).Hidden = True
                .Columns(1).Hidden = True
                .Columns(2).Width = 200
                .Columns(3).Width = 100
                .Columns(4).Width = 100
                .Columns(4).PromptChar = ""
                .Columns(4).Style = ColumnStyle.Date
                .Columns(4).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                .Columns(4).CellActivation = Activation.NoEdit
                .Columns(5).Width = 100
                .Columns(5).PromptChar = ""
                .Columns(5).Style = ColumnStyle.Date
                .Columns(5).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                .Columns(5).CellActivation = Activation.NoEdit
                .Columns(6).Width = 100
                .Columns(6).CellAppearance.TextHAlign = HAlign.Right
                .Columns(6).Format = "c"
                .Columns(6).PromptChar = ""
                .Columns(6).Style = ColumnStyle.Double
                .Columns(6).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
                .Columns(6).CellActivation = Activation.NoEdit
                Dim column As UltraGridColumn = Me.UGDetalles.DisplayLayout.Bands(1).Columns("Agregar")

                column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50

                column = Me.UGDetalles.DisplayLayout.Bands(1).Columns("Eliminar")
                column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Agregar").Header.Caption = ""
                .Columns("Agregar").Style = UltraWinGrid.ColumnStyle.Button
                .Columns("Agregar").CellButtonAppearance.Image = Me.ImageList1.Images(0)
                .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center
                .Columns("Eliminar").Header.Caption = ""
                .Columns("Eliminar").Style = UltraWinGrid.ColumnStyle.Button
                .Columns("Eliminar").CellButtonAppearance.Image = Me.ImageList1.Images(2)
                .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center
                '.HeaderVisible = True
                '.Header.Caption = ""
                '.Header.Appearance.BackColor = Color.Blue
                '.Header.Appearance.BackColor2 = Color.SkyBlue
                '.Header.Appearance.ForeColor = Color.White
                '.Header.Appearance.BackGradientStyle = GradientStyle.Horizontal

                .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(1).Header.Appearance.ForeColor = Color.Black
                .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(1).Header.Appearance.BackColor2 = Color.White
                .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(2).Header.Appearance.ForeColor = Color.Black
                .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(2).Header.Appearance.BackColor2 = Color.White
                .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(3).Header.Appearance.ForeColor = Color.Black
                .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(3).Header.Appearance.BackColor2 = Color.White
                .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(4).Header.Appearance.ForeColor = Color.Black
                .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(4).Header.Appearance.BackColor2 = Color.White
                .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(5).Header.Appearance.ForeColor = Color.Black
                .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(5).Header.Appearance.BackColor2 = Color.White
                .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(6).Header.Appearance.ForeColor = Color.Black
                .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(6).Header.Appearance.BackColor2 = Color.White
                .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(7).Header.Appearance.ForeColor = Color.Black
                .Columns(7).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(7).Header.Appearance.BackColor2 = Color.White
                .Columns(8).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(8).Header.Appearance.ForeColor = Color.Black
                .Columns(8).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(8).Header.Appearance.BackColor2 = Color.White
            End With

            With .Bands(2)
                .Columns(0).Hidden = True
                .Columns(1).Hidden = True
                .Columns(2).Width = 300
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(3).Width = 100
                .Columns(3).CellActivation = Activation.NoEdit
                .Columns(3).Header.Caption = "Matricula"
                Dim column As UltraGridColumn = Me.UGDetalles.DisplayLayout.Bands(2).Columns("Buscar")
                column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                column = Me.UGDetalles.DisplayLayout.Bands(2).Columns("Eliminar")
                column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Buscar").Header.Caption = ""
                .Columns("Buscar").Style = UltraWinGrid.ColumnStyle.Button
                .Columns("Buscar").CellButtonAppearance.Image = Me.ImageList1.Images(1)
                .Columns("Buscar").CellButtonAppearance.ImageHAlign = HAlign.Center
                .Columns("Eliminar").Header.Caption = ""
                .Columns("Eliminar").Style = UltraWinGrid.ColumnStyle.Button
                .Columns("Eliminar").CellButtonAppearance.Image = Me.ImageList1.Images(2)
                .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

                .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(1).Header.Appearance.ForeColor = Color.Black
                .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(1).Header.Appearance.BackColor2 = Color.White
                .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(2).Header.Appearance.ForeColor = Color.Black
                .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(2).Header.Appearance.BackColor2 = Color.White
                .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(3).Header.Appearance.ForeColor = Color.Black
                .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(3).Header.Appearance.BackColor2 = Color.White
                .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(4).Header.Appearance.ForeColor = Color.Black
                .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(4).Header.Appearance.BackColor2 = Color.White
                .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(5).Header.Appearance.ForeColor = Color.Black
                .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(5).Header.Appearance.BackColor2 = Color.White
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True

            End With

        End With
    End Sub

    Private Sub UGDetalles_AfterCellUpdate(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGDetalles.AfterCellUpdate
        Select Case e.Cell.Band.Index
            Case 0
                UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.NoEdit
                UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode)
            Case 1
                UGDetalles.DisplayLayout.Bands(1).Columns("Banco").CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.NoEdit
                UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode)
            Case 2

        End Select

    End Sub

    Private Sub UGDetalles_BeforeRowsDeleted(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs) Handles UGDetalles.BeforeRowsDeleted
        ' para que no pregunte cuando borro una fila
        e.DisplayPromptMsg = False
    End Sub

    Private Sub UGDetalles_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGDetalles.ClickCellButton
        If e.Cell.Band.Index = 1 Then ' carga cheques
            If e.Cell.Column.Index = 7 Then 'Agregar cheque
                If MessageBox.Show("    Confirmar ", "Agregar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    If Reporte = "CRRecibo" Then
                        If CantCheques <= 5 Then
                            AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))
                            CantCheques += 1
                        End If
                    Else
                        AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))
                    End If


                End If
            Else ' eliminar cheque
                If MessageBox.Show("    Confirmar ", "Eliminar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    If Reporte = "CRRecibo" Then
                        If CantCheques > 0 Then                            
                            CantCheques -= 1
                        End If                                            
                    End If

                    EliminarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))

                End If

                End If
                Else ' buscar destinatario

                If e.Cell.Column.Index = 4 Then
                    MuestraDestinatario(False)
                Else ' limpia destinatario
                    MuestraDestinatario(True)
                End If
            End If
    End Sub

    Private Sub AgregarCheques(ByRef RowSubItem As DataRow)
        Dim mydr As DataRow
        mydr = DTCheques.NewRow()
        mydr("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("Agregar") = ""
        DTCheques.Rows.Add(mydr)
    End Sub

    Private Sub EliminarCheques(ByRef RowSubItem As DataRow)
        With UGDetalles.ActiveRow
            .Delete()
            If DTCheques.Rows.Count = 0 Then
                AgregarCheques(RowSubItem)                                
            End If
        End With
    End Sub

    Private Sub AgregarSubcuenta(ByRef RowSubItem As DataRow)
        Dim mydr As DataRow
        mydr = DTSubCuenta.NewRow()
        mydr.Item("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("nrodoc") = 0
        DTSubCuenta.Rows.Add(mydr)
    End Sub

    Private Sub EliminarSubcuenta(ByRef item As Integer)
        Try
            DTSubCuenta.Rows(item).Delete() ' si da error porque no hay nada en la posicion que se quiere borrar
        Catch ex As Exception
        End Try

    End Sub

    Private Sub UGDetalles_DoubleClickCell(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs) Handles UGDetalles.DoubleClickCell
        If UGDetalles.ActiveCell.IsInEditMode Then
            DesActivarEdision()
        Else
            ActivarEdision()
        End If
    End Sub

    Private Sub UGDetalles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGDetalles.InitializeRow
        Dim cell As UltraGridCell

        ' UGDetalles.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns
        UGDetalles.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free

        If e.Row.Band.Index = 0 Then
            e.Row.ExpandAll()
            If IsDBNull(e.Row.Cells("pto_tipmov").Value) Then

            Else
                If e.Row.Cells("pto_tipmov").Value = "I" Then
                    e.Row.Cells("Haber").Appearance.BackColor = Color.Red
                    e.Row.Cells("Haber").Appearance.ForeColor = Color.Black
                    e.Row.Cells("Debe").Appearance.BackColor = Color.Red
                    e.Row.Cells("Debe").Appearance.ForeColor = Color.Black
                Else
                    If e.Row.Cells("pto_tipmov").Value = "H" Then
                        cell = e.Row.Cells("Haber")
                    Else
                        cell = e.Row.Cells("Debe")
                    End If
                    cell.Appearance.BackColor = Color.Red
                    cell.Appearance.ForeColor = Color.Black
                End If
            End If
        End If
    End Sub

    Private Sub UGDetalles_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UGDetalles.KeyDown
        If e.KeyData = Keys.Insert Then
            AgregarDetalleVacio()
        End If
    End Sub

    Private Sub UGDetalles_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UGDetalles.KeyPress
        Try
            Select Case e.KeyChar
                Case ChrW(Keys.Enter)
               
                    If UGDetalles.ActiveCell.IsInEditMode Then

                        DesActivarEdision()
                        ' si estoy cargando la cuenta
                        CargarCuentaManual()

                        SumoTotales()
                    Else
                        ActivarEdision()
                    End If

            End Select


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CargarCuentaManual()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Dim rowItem As DataRow = DSDetalles.Tables(0).Rows(UGDetalles.ActiveRow.Index)
        If cell.Column.ToString = "Cuenta" Then
            Dim FrmCuenta As New FrmPlanCuentas
            If IsDBNull(cell.Value) Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
            ElseIf cell.Value = "" Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
            Else
                FrmCuenta.Condicion = "pla_nropla='" & cell.Value & "' and pla_imputa='Si'"
            End If
            FrmCuenta.CargaPlanCuenta()
            If FrmCuenta.NumeroCuenta = "" Then
                FrmCuenta.ShowDialog()
            End If
            row.Cells("Cuenta").Value = FrmCuenta.NumeroCuenta
            row.Cells("Descripcion").Value = FrmCuenta.DescripcionCuenta
            row.Cells("Matricula").Value = FrmCuenta.NumeroSubCuenta
            row.Cells("pla_servicio").Value = FrmCuenta.Servicio
            If row.Cells("pla_servicio").Value = "Si" Then
                AgregarSubcuenta(rowItem)
            Else
                EliminarSubcuenta(UGDetalles.ActiveRow.Index)
            End If
            '   If Mid(row.Cells("Matricula").Value, 1, 3) = "BCO" Then
            ' AgregarCheques(rowItem)
            'End If
            FrmCuenta.Dispose()
        End If

    End Sub

    Private Sub ActivarEdision()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Try
            Select Case cell.Band.Index
                Case 0
                    If cell.Column.ToString = "Haber" And row.Cells("pto_tipmov").Text = "H" And row.Cells("pto_formula").Text.Trim = "" Then
                        UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                        UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                    End If
                    If cell.Column.ToString = "Debe" And row.Cells("pto_tipmov").Text = "D" And row.Cells("pto_formula").Text.Trim = "" Then
                        UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                        UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                    End If
                    If row.Cells("pto_tipmov").Text = "I" Then
                        Select Case cell.Column.ToString
                            Case "Debe"
                                If row.Cells("haber").Value = 0 Then
                                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                                    UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                                End If
                            Case "Haber"
                                If row.Cells("Debe").Value = 0 Then
                                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                                    UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                                End If
                            Case "Cuenta"
                                UGDetalles.DisplayLayout.Bands(0).Columns(0).CellActivation = Activation.AllowEdit
                                UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                        End Select
                    End If
                Case 1
                    UGDetalles.DisplayLayout.Bands(1).Columns("banco").CellActivation = Activation.AllowEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.AllowEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.AllowEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.AllowEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.AllowEdit
                    UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                Case 2
                    UGDetalles.DisplayLayout.Bands(2).Columns(cell.Column.Index).CellActivation = Activation.AllowEdit
                    UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)

            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        End Try
    End Sub

    Private Sub DesActivarEdision()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Try
            Select Case cell.Band.Index
                Case 0
                    UGDetalles.DisplayLayout.Bands(0).Columns(0).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.NoEdit
                Case 1
                    UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.NoEdit
                    If cell.Column.Index = 6 Then
                        If DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("pto_tipmov") = "H" Then

                            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") = 0
                            For Each rowche As DataRow In DTCheques.Rows
                                DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") += rowche.Item("Monto")
                            Next

                        Else

                            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") = 0
                            For Each rowche As DataRow In DTCheques.Rows
                                DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") += rowche.Item("Monto")
                            Next

                        End If
                    End If
                Case 2
                    UGDetalles.DisplayLayout.Bands(2).Columns(2).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(2).Columns(3).CellActivation = Activation.NoEdit
                    MuestraDestinatario(False)
            End Select
            UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode)

        Catch ex As Exception
            MessageBox.Show("Controle los datos ingresados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SumoTotales()
        Dim nTotHaber As Double = 0
        Dim nTotDeber As Double = 0
        Dim nMontoFormula As Double = 0
        Dim Exprecion As String
        Dim nItem As Integer = 0
        Dim item(DSDetalles.Tables("Detalles").Rows.Count + 1) As Double
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            item(nItem) = RowTot.Item("debe") + RowTot.Item("haber")
        Next
        nItem = 0
        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            ' item(nItem) = RowTot.Item("debe") + RowTot.Item("haber")
            If RowTot.Item("pto_valor") <> 0 Then
                Exprecion = RowTot.Item("pto_valor")
                nMontoFormula = oSC.Eval(Exprecion)
                If RowTot.Item("pto_tipmov") = "D" Then
                    RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                Else
                    RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                End If
                item(nItem) = nMontoFormula
            Else
                If RowTot.Item("pto_formula").ToString.Trim <> "" Then
                    Exprecion = RowTot.Item("pto_formula")
                    For xItem = 1 To item.Length - 1
                        '  If item(xItem) > 0 Then
                        Exprecion = Exprecion.ToString.Replace("item" & Convert.ToString(Format(xItem, "00")), item(xItem))
                        Exprecion = Exprecion.ToString.Replace("Prestamo", Prestamo)

                        ' End If
                    Next
                    '                    Exprecion = Exprecion.ToString.Replace(Mid(Exprecion, 1, 6), item(CInt(Mid(Exprecion, 5, 2))))
                    Exprecion = Exprecion.ToString.Replace(",", ".")
                    nMontoFormula = oSC.Eval(Exprecion)
                    If RowTot.Item("pto_tipmov") = "D" Then
                        RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                    Else
                        RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                    End If
                    item(nItem) = nMontoFormula
                End If
            End If
            nTotDeber += Math.Round(RowTot.Item("debe"), 2)
            nTotHaber += Math.Round(RowTot.Item("haber"), 2)

        Next

        Dim nDifeRedondeo As Double = Math.Round(nTotDeber - nTotHaber, 2)
        ' repaso para colocar la diferencia de redondeo
        If System.Math.Abs(nDifeRedondeo) Then
            nTotDeber = 0
            nTotHaber = 0

            For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or _
                   RowTot.Item("pto_formula").ToString.IndexOf("/") > 0 Or _
                   RowTot.Item("pto_formula").ToString.IndexOf("+") > 0 Or _
                   RowTot.Item("pto_formula").ToString.IndexOf("-") > 0 Then
                    If nDifeRedondeo <> 0 Then

                        If nDifeRedondeo < 0 Then
                            If RowTot.Item("pto_tipmov") = "H" Then
                                RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                                nDifeRedondeo = 0
                            End If

                        Else
                            If RowTot.Item("pto_tipmov") = "D" Then
                                RowTot.Item("debe") = RowTot.Item("debe") - nDifeRedondeo
                                nDifeRedondeo = 0
                            End If

                        End If
                    End If
                End If
                nTotDeber += RowTot.Item("debe")
                nTotHaber += RowTot.Item("haber")

            Next

        End If

        TxtDebe.Text = Format(nTotDeber, "$ ###,##0.00")
        TxtHaber.Text = Format(nTotHaber, "$ ###,##0.00")
    End Sub

    Private Sub CButton2_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton2.ClickButtonArea
        If CDbl(TxtDebe.Text) > 0 AndAlso CDbl(TxtDebe.Text) = CDbl(TxtHaber.Text) AndAlso UltraComboEditor1.Value <> Nothing Then
            Dim nNroAsiImprime As Long
            Dim cProcesoImprime As String = UltraTextEditor1.Text
            nNroAsiImprime = cnn.GraboAsiento(DSDetalles.Tables("detalles"), _
                             DSDetalles.Tables("cheques"), _
                             DTCuotasServicio, _
                             TxtConcepto1.Text, _
                             TxtConcepto2.Text, _
                             TxtConcepto3.Text, _
                             nCaja, _
                             nZeta, False)
            LimpiarProceso()


            If Reporte = "CRRecibo" Then
                If ImpresionDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    'Cargo el Nro de Asiento
                    n_NroAsiento = nNroAsiImprime
                    ImpresionPrint.PrinterSettings.Copies = ImpresionDialog.PrinterSettings.Copies
                    ImpresionPrint.Print()
                Else
                    Exit Sub
                End If
            Else

                Dim imprimir As New Impresiones
                imprimir.ImprimirAsiento(nNroAsiImprime, cProcesoImprime)

            End If

            Reporte = String.Empty

        Else
            MessageBox.Show("Compruebe los datos ingresados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If
    End Sub

    'Private Sub CierraAsiento()
    '    If cnn.AbrirConexion Then
    '        Dim miTrans As MySqlTransaction
    '        miTrans = cnn.InicioTransaccion
    '        Try

    '            Dim FechaHora As String

    '            FechaHora = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)


    '            ' Dim nNroAsiento As Long = cnn.TomaCobte(UltraComboEditor1.Value, nPubNroCli, "ASIENT")
    '            Dim nNroAsiento As Long = cnn.TomaCobte(nPubNroIns, nPubNroCli, "ASIENT")
    '            Dim nNrocomprobante As Long = cnn.TomaCobte(UltraComboEditor1.Value, nPubNroCli, UltraTextEditor1.Text)
    '            Dim cTipDocDestinatario As String = ""
    '            Dim nNroDocDestinatario As Integer = 0
    '            Dim cTitMtrDestinatario As String = ""
    '            Dim nMatMtrDestinataria As Integer = 0
    '            Dim cTipDoc As String = ""
    '            Dim nNroDoc As Integer = 0
    '            Dim cTitMtr As String = ""
    '            Dim nMatMtr As Integer = 0
    '            Dim nItem As Integer = 0
    '            Dim ltieneCheques As Boolean = False



    '            For Each RowAsi As DataRow In DSDetalles.Tables("detalles").Rows
    '                If RowAsi.Item("Debe") > 0 Or RowAsi.Item("Haber") > 0 Then
    '                    If RowAsi.Item("Subcuenta") <> "" Then
    '                        cTipDocDestinatario = Mid(RowAsi.Item("subcuenta"), 1, 3)
    '                        nNroDocDestinatario = CInt(Mid(RowAsi.Item("subcuenta"), 4))
    '                        cTitMtrDestinatario = Mid(RowAsi.Item("Matricula"), 1, 2)
    '                        nMatMtrDestinataria = CInt(Mid(RowAsi.Item("Matricula"), 3))
    '                        cTipDoc = Mid(RowAsi.Item("subcuenta"), 1, 3)
    '                        nNroDoc = CInt(Mid(RowAsi.Item("subcuenta"), 4))
    '                        cTitMtr = Mid(RowAsi.Item("Matricula"), 1, 2)
    '                        nMatMtr = CInt(Mid(RowAsi.Item("Matricula"), 3))

    '                    Else
    '                        cTipDoc = ""
    '                        nNroDoc = 0
    '                        cTitMtr = ""
    '                        nMatMtr = 0

    '                    End If

    '                    ltieneCheques = False
    '                    For Each RowChe As DataRow In DSDetalles.Tables("cheques").Rows
    '                        If RowAsi.Item("pto_item") = RowChe.Item("item") Then
    '                            ltieneCheques = True
    '                            nItem += 1
    '                            cnn.ReplaceBD("insert into cheques set che_nrocli='" & nPubNroCli & "'," & _
    '                                                                  "che_origen='T'," & _
    '                                                                  "che_estado='C'," & _
    '                                                                  "che_nrochequera='" & 0 & "'," & _
    '                                                                  "che_letser=''," & _
    '                                                                  "che_nroser='" & RowChe.Item("cheque") & "'," & _
    '                                                                  "che_tipdoc='ENT'" & _
    '                                                                  "che_nrodoc='" & nPubNroCli & "'" & _
    '                                                                  "che_nroasi='" & nNroAsiento & "'," & _
    '                                                                  "che_item='" & nItem & "'," & _
    '                                                                  "che_fecha='" & FechaHora & "'," & _
    '                                                                  "che_fecche='" & Format(CDate(RowChe.Item("fechaEmision")), "yyyy-MM-dd") & "'," & _
    '                                                                  "che_fecdif='" & Format(CDate(RowChe.Item("fechaDiferida")), "yyyy-MM-dd") & "'," & _
    '                                                                  "che_tipbco='" & cTipDoc & "'," & _
    '                                                                  "che_nrobco='" & nNroDoc & "'," & _
    '                                                                  "che_cuebco='" & RowAsi.Item("cuenta") & "'," & _
    '                                                                  "che_impche='" & RowChe.Item("monto").ToString.Replace(",", ".") & "'")


    '                            cnn.ReplaceBD("insert into totales set tot_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                         "tot_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                         "tot_nrocli='" & nPubNroCli & "'," & _
    '                                         "tot_nrocom='" & nNrocomprobante & "'," & _
    '                                         "tot_item='" & nItem & "'," & _
    '                                         "tot_nropla='" & RowAsi.Item("cuenta") & "'," & _
    '                                         "tot_subpla='" & RowAsi.Item("subcuenta") & "'," & _
    '                                         "tot_titulo='" & cTitMtr & "'," & _
    '                                         "tot_matricula='" & nMatMtr & "'," & _
    '                                         "tot_tipdoc='" & cTipDoc & "'," & _
    '                                         "tot_nrodoc='" & nNroDoc & "'," & _
    '                                         "tot_nrocuo='" & 0 & "'," & _
    '                                         "tot_fecha='" & FechaHora & "'," & _
    '                                         "tot_nroser='" & RowChe.Item("cheque") & "'," & _
    '                                         "tot_fecche='" & Format(CDate(RowChe.Item("fechaEmision")), "yyyy-MM-dd") & "'," & _
    '                                         "tot_fecdif='" & Format(CDate(RowChe.Item("fechaDiferida")), "yyyy-MM-dd") & "'," & _
    '                                         "tot_debe='" & IIf(RowAsi.Item("debe") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," & _
    '                                         "tot_haber='" & IIf(RowAsi.Item("haber") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," & _
    '                                         "tot_tipdes='ENT'" & _
    '                                         "tot_nrodes='" & nPubNroCli & "'" & _
    '                                         "tot_nroasi='" & nNroAsiento & "'," & _
    '                                         "tot_caja='" & nCaja & "'," & _
    '                                         "tot_zeta='" & nZeta & "'," & _
    '                                         "tot_concepto='" & TxtConcepto.Text & "'")

    '                        End If
    '                    Next
    '                    If Not ltieneCheques Then
    '                        nItem += 1
    '                        cnn.ReplaceBD("insert into totales set tot_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                                      "tot_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                                      "tot_nrocli='" & nPubNroCli & "'," & _
    '                                                      "tot_nrocom='" & nNrocomprobante & "'," & _
    '                                                      "tot_item='" & nItem & "'," & _
    '                                                      "tot_nropla='" & RowAsi.Item("cuenta") & "'," & _
    '                                                      "tot_subpla='" & RowAsi.Item("subcuenta") & "'," & _
    '                                                      "tot_titulo='" & cTitMtr & "'," & _
    '                                                      "tot_matricula='" & nMatMtr & "'," & _
    '                                                      "tot_tipdoc='" & cTipDoc & "'," & _
    '                                                      "tot_nrodoc='" & nNroDoc & "'," & _
    '                                                      "tot_nrocuo='" & 0 & "'," & _
    '                                                      "tot_fecha='" & FechaHora & "'," & _
    '                                                      "tot_debe='" & RowAsi.Item("debe").ToString.Replace(",", ".") & "'," & _
    '                                                      "tot_haber='" & RowAsi.Item("haber").ToString.Replace(",", ".") & "'," & _
    '                                                      "tot_nroasi='" & nNroAsiento & "'," & _
    '                                                      "tot_caja='" & nCaja & "'," & _
    '                                                      "tot_zeta='" & nZeta & "'," & _
    '                                                      "tot_concepto='" & TxtConcepto.Text & "'")

    '                    End If

    '                End If
    '            Next


    '            cnn.ReplaceBD("insert into comproba set com_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                              "com_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                              "com_nrocli='" & nPubNroCli & "'," & _
    '                                              "com_nrocom='" & nNrocomprobante & "'," & _
    '                                              "com_nroasi='" & nNroAsiento & "'," & _
    '                                              "com_fecha='" & FechaHora & "'," & _
    '                                              "com_total='" & CDbl(TxtDebe.Text.Replace("$", "")) & "'," & _
    '                                              "com_tipdoc='" & cTipDocDestinatario & "'," & _
    '                                              "com_nrodoc='" & nNroDocDestinatario & "'," & _
    '                                              "com_titulo='" & cTitMtrDestinatario & "'," & _
    '                                              "com_matricula='" & nMatMtrDestinataria & "'," & _
    '                                              "com_caja='" & nCaja & "'," & _
    '                                              "com_zeta='" & nZeta & "'," & _
    '                                              "com_concepto1='" & TxtConcepto.Text & "'")


    '            ' cancelos las cuotas de los servicio
    '            If DTCuotasServicio.IsInitialized Then
    '                Dim nMontoCuota As Double
    '                For Each rowCuo As DataRow In DTCuotasServicio.Rows
    '                    If Not IsDBNull(rowCuo.Item("select")) AndAlso rowCuo.Item("select") Then
    '                        nMontoCuota = rowCuo.Item("debe") + rowCuo.Item("haber")
    '                        cnn.ReplaceBD("update totales set tot_imppag='" & nMontoCuota.ToString.Replace(",", ".") & "'," & _
    '                                                 "tot_asicancel='" & nNroAsiento & "'" & _
    '                                                 " where tot_nroasi=" & rowCuo.Item("tot_nroasi") & " and tot_item=" & rowCuo.Item("tot_item") & " and tot_nrocuo=" & rowCuo.Item("tot_nrocuo"))
    '                    End If
    '                Next
    '            End If
    '            miTrans.Commit()
    '        Catch ex As Exception
    '            miTrans.Rollback()
    '            MessageBox.Show(ex.Message)
    '        End Try
    '        cnn.CerrarConexion()
    '    End If
    'End Sub

    Private Sub ActualizoFechaInstitucion()
        If Not DSDetalles Is Nothing Then
            For Each RowDet As DataRow In DSDetalles.Tables("detalles").Rows
                RowDet.Item("fecha") = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
                If Not UltraComboEditor1.Value Is Nothing Then
                    RowDet.Item("institucion") = UltraComboEditor1.Value
                End If
            Next
        End If
    End Sub

    Private Sub UltraDateTimeEditor1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UltraDateTimeEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub

    Private Sub UltraComboEditor1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UltraComboEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub


    '/**********************************************************************************************************

    Private Sub ImpresionPrint_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles ImpresionPrint.PrintPage
        Dim NL As New NumLetra

        Dim NroReciboX, NroReciboY, FechaX, FechaY, AfiliadoX, AfiliadoY, MatriculaX, MatriculaY As Double
        Dim NroCliX, NroCliY, MontoX, MontoY, MontoLetrasX, MontoLetrasY As Double
        Dim Conceptos1X, Conceptos2X, Conceptos3X, Conceptos1Y, Conceptos2Y, Conceptos3Y As Double
        Dim Monto1X, Monto1Y As Double
        Dim LeyendaX, LeyendaY As Double
        Dim ProcesoX, ProcesoY As Double
        Dim ChequeX, ChequeY, BancoX, BancoY, SucX, SucY, MontoCHX, MontoCHY As Double
        Dim OperadorX, OperadorY As Double
        Dim InstitucionX, InstitucionY As Double
        Dim PiePaginaX, PiePaginaY As Double

        Dim Banco, Cheque, Suc, Importe As String

        Dim Monto, Letra As String
        Banco = ""

        'Inicializo las Variables
        'a ojo saque estos valores

        NroReciboX = 500
        NroReciboY = 14
        InstitucionX = 580
        InstitucionY = 14
        FechaX = 580
        FechaY = 50
        AfiliadoX = 55
        AfiliadoY = 138
        MatriculaX = 300
        MatriculaY = 138
        NroCliX = 55
        NroCliY = 165
        MontoX = 70
        MontoY = 217
        MontoLetrasX = 180
        MontoLetrasY = 217
        Conceptos1X = 70
        Conceptos1Y = 260
        Conceptos2X = 70
        Conceptos2Y = 273
        Conceptos3X = 70
        Conceptos3Y = 286
        Monto1X = 20
        Monto1Y = 310
        LeyendaX = 20
        LeyendaY = 330
        ProcesoX = 70
        ProcesoY = 242

        ChequeX = 20
        ChequeY = 350

        BancoX = 123
        BancoY = 350
        SucX = 309
        SucY = 350
        MontoCHX = 469
        MontoCHY = 350

        PiePaginaX = 20
        PiePaginaY = 505
        OperadorX = 500
        OperadorY = 100

        ' La fuente a usar
        Dim prFont As New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

        Try
            DAImprimir = cnn.consultaBDadapter("(((((comproba inner join procesos on pro_codigo=com_proceso) inner join " & _
                                               "farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join  " & _
                                               "operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) " & _
                                               "inner join plancuen on pla_nropla=tot_nropla) left join afiliado on " & _
                                               "afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "far_nombre as institucion, " & _
                                               "far_nrocli as delegacion, com_proceso as proceso,com_nrocom as comprobante, " & _
                                               "com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip, " & _
                                               "DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha, " & _
                                               "DATE_FORMAT(CAST(tot_fecven AS char),'%d/%m/%Y') as vencimiento, " & _
                                               "com_concepto1 as conceptos1, com_concepto2 as conceptos2,com_concepto3 as conceptos3, " & _
                                               "tot_item as item, tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip, " & _
                                               "tot_nrocuo as itemsubcta,CONCAT(tot_subpla,' ',afi_titulo, CAST(afi_matricula AS char)) as subcta, " & _
                                               "afi_nombre as subctadescrip, tot_debe as debe,tot_haber as haber,pro_reporte, " & _
                                               "afi_matricula as matricula,tot_tipdoc,tot_nrodoc,tot_titulo, " & _
                                               "tot_matricula", "tot_haber > 0  and com_nroasi=" & n_NroAsiento)
            DTImprimir = New DataTable
            DAImprimir.Fill(DTImprimir)

            If DTImprimir.Rows.Count > 0 Then

                DACheque = cnn.consultaBDadapter("totales", "tot_nrocheque as cheque, tot_debe-tot_haber as impche,tot_concepto " & _
                                                 "as desticheque, tot_estche,tot_concepto as Banco", "tot_nrocheque > 0 and tot_nroasi = " & n_NroAsiento)
                DTCheque = New DataTable
                DACheque.Fill(DTCheque)

                If DTCheque.Rows.Count > 0 Then
                    For Each rowCheque In DTCheque.Rows

                        If rowCheque("cheque") > 0 Then
                            Banco = rowCheque("banco")
                            Cheque = rowCheque("cheque")
                            Suc = "RCIA"
                            Importe = rowCheque("impche")

                            'imprimimo el nro del cheque
                            e.Graphics.DrawString(rowCheque("cheque"), prFont, Brushes.Black, ChequeX, ChequeY)
                            'imprimimo el banco del cheque
                            e.Graphics.DrawString(Banco, prFont, Brushes.Black, BancoX, BancoY)
                            'imprimimo la sucursal cheque
                            e.Graphics.DrawString(Suc, prFont, Brushes.Black, SucX, SucY)
                            'imprimimo el monto del cheque
                            e.Graphics.DrawString(Importe, prFont, Brushes.Black, MontoCHX, MontoCHY)

                            ChequeY += 15
                            BancoY += 15
                            SucY += 15
                            MontoCHY += 15

                        End If
                    Next
                End If

                For Each row In DTImprimir.Rows

                    Monto = "$ " & row("haber")

                    'imprimimo el comprobante
                    e.Graphics.DrawString(row("comprobante"), prFont, Brushes.Black, NroReciboX, NroReciboY)
                    'imprimo la institucion
                    If row("institucion") = "SIPRES" Then
                        e.Graphics.DrawString("(SIPRES)", prFont, Brushes.Black, InstitucionX, InstitucionY)
                    Else
                        e.Graphics.DrawString(" ", prFont, Brushes.Black, InstitucionX, InstitucionY)
                    End If
                    'imprimimo la fecha         
                    e.Graphics.DrawString(row("fecha"), prFont, Brushes.Black, FechaX, FechaY)
                    'imprimimo el OEPRADOR
                    e.Graphics.DrawString("Operador: " & row("operador"), prFont, Brushes.Black, OperadorX, OperadorY)
                    'imprimimo el Afiliado 
                    e.Graphics.DrawString(row("subctadescrip"), prFont, Brushes.Black, AfiliadoX, AfiliadoY)
                    'imprimimo la matricula
                    e.Graphics.DrawString("Matricula N°: " & row("matricula"), prFont, Brushes.Black, MatriculaX, MatriculaY)
                    'imprimimo la delegacion
                    'e.Graphics.DrawString(row("delegacion"), prFont, Brushes.Black, NroCliX, NroCliY)
                    e.Graphics.DrawString(ceros1(row("delegacion"), 5), prFont, Brushes.Black, NroCliX, NroCliY)
                    'imprimimo el monto
                    e.Graphics.DrawString(Monto, prFont, Brushes.Black, MontoX, MontoY)
                    'imprimimo el monto en letras
                    Letra = NL.Convertir(row("haber") & " ", True)
                    MontoLetrasX = (MontoX + Len(Monto) + 60)
                    e.Graphics.DrawString(" (SON PESOS: " & Letra & " )", prFont, Brushes.Black, MontoLetrasX, MontoLetrasY)
                    'imprimimo el detalle DEL PROCESO
                    e.Graphics.DrawString(row("procedescrip"), prFont, Brushes.Black, ProcesoX, ProcesoY)

                    'imprimo la descripcion
                    prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
                    e.Graphics.DrawString(row("conceptos1"), prFont, Brushes.Black, Conceptos1X, Conceptos1Y)
                    e.Graphics.DrawString(row("conceptos2"), prFont, Brushes.Black, Conceptos2X, Conceptos2Y)
                    e.Graphics.DrawString(row("conceptos3"), prFont, Brushes.Black, Conceptos3X, Conceptos3Y)

                    prFont = New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

                    ''imprimimo el detalle del cheque
                    e.Graphics.DrawString("Cheque               Banco                                       Sucursal                            Importe ", prFont, Brushes.Black, LeyendaX, LeyendaY)

                    'imprimimo la forma de pago
                    If Banco <> "" Then
                        e.Graphics.DrawString("Efectivo : ............................................................................ 0 ", prFont, Brushes.Black, Monto1X, Monto1Y)
                    Else
                        e.Graphics.DrawString("Efectivo : ............................................................................  " & Monto, prFont, Brushes.Black, Monto1X, Monto1Y)
                    End If

                    prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)

                    'imprimimo el pie de pagina
                    e.Graphics.DrawString("LOS CHEQUES RECIBIDOS SERAN ACREDITADOS A V/CUENTA PERSONAL UNA VEZ HECHOS EFECTIVOS", prFont, Brushes.Black, PiePaginaX, PiePaginaY)
                Next

                'indicamos que hemos llegado al final de la pagina
                e.HasMorePages = False
                n_NroAsiento = 0
            End If
        Catch ex As Exception
            e.Cancel = True
            MessageBox.Show("ERROR : " & ex.Message, "IMPRESION DE RECIBOS ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub UltraTextEditor1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UltraTextEditor1.KeyDown
        If e.KeyCode = Keys.Return Then
            MuestraProcesos()
            If Me.UltraLabel1.Visible Then
                Me.UltraComboEditor1.SelectedIndex = DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_instit") - 1 ' Convert.ToInt16(Mid(Me.UltraTextEditor1.Text, 1, 2)) - 1
                MuestraDetalles()
                SumoTotales()
            End If
        ElseIf e.KeyCode = Keys.Escape Then
            LimpiarProceso()
        End If
    End Sub

    Private Sub FrmAsiento_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        UltraTextEditor1.Focus()
    End Sub

    Private Sub UGDetalles_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UGDetalles.InitializeLayout

    End Sub
End Class