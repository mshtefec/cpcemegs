﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSipres
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Btn_new_recibo = New System.Windows.Forms.Button()
        Me.DGVRAjubilados = New System.Windows.Forms.DataGridView()
        Me.MatriculaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JubilacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CapitalizacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JubiladosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSSipres = New cpceMEGS.DSSipres()
        Me.Btn_generate_recibos = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.DGVRAjubilados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JubiladosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Btn_generate_recibos)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Btn_new_recibo)
        Me.Panel1.Controls.Add(Me.DGVRAjubilados)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(776, 426)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(270, 21)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Recibos Automáticos de Jubilados"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(532, 395)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(95, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Ver Generados"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Btn_new_recibo
        '
        Me.Btn_new_recibo.Location = New System.Drawing.Point(633, 395)
        Me.Btn_new_recibo.Name = "Btn_new_recibo"
        Me.Btn_new_recibo.Size = New System.Drawing.Size(140, 23)
        Me.Btn_new_recibo.TabIndex = 1
        Me.Btn_new_recibo.Text = "Crear Recibo"
        Me.Btn_new_recibo.UseVisualStyleBackColor = True
        '
        'DGVRAjubilados
        '
        Me.DGVRAjubilados.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVRAjubilados.AutoGenerateColumns = False
        Me.DGVRAjubilados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVRAjubilados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVRAjubilados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MatriculaDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.DniDataGridViewTextBoxColumn, Me.JubilacionDataGridViewTextBoxColumn, Me.CapitalizacionDataGridViewTextBoxColumn})
        Me.DGVRAjubilados.DataSource = Me.JubiladosBindingSource
        Me.DGVRAjubilados.Location = New System.Drawing.Point(3, 31)
        Me.DGVRAjubilados.Name = "DGVRAjubilados"
        Me.DGVRAjubilados.Size = New System.Drawing.Size(770, 357)
        Me.DGVRAjubilados.TabIndex = 0
        '
        'MatriculaDataGridViewTextBoxColumn
        '
        Me.MatriculaDataGridViewTextBoxColumn.DataPropertyName = "matricula"
        Me.MatriculaDataGridViewTextBoxColumn.HeaderText = "matricula"
        Me.MatriculaDataGridViewTextBoxColumn.Name = "MatriculaDataGridViewTextBoxColumn"
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        '
        'DniDataGridViewTextBoxColumn
        '
        Me.DniDataGridViewTextBoxColumn.DataPropertyName = "dni"
        Me.DniDataGridViewTextBoxColumn.HeaderText = "dni"
        Me.DniDataGridViewTextBoxColumn.Name = "DniDataGridViewTextBoxColumn"
        '
        'JubilacionDataGridViewTextBoxColumn
        '
        Me.JubilacionDataGridViewTextBoxColumn.DataPropertyName = "jubilacion"
        Me.JubilacionDataGridViewTextBoxColumn.HeaderText = "jubilacion"
        Me.JubilacionDataGridViewTextBoxColumn.Name = "JubilacionDataGridViewTextBoxColumn"
        '
        'CapitalizacionDataGridViewTextBoxColumn
        '
        Me.CapitalizacionDataGridViewTextBoxColumn.DataPropertyName = "capitalizacion"
        Me.CapitalizacionDataGridViewTextBoxColumn.HeaderText = "capitalizacion"
        Me.CapitalizacionDataGridViewTextBoxColumn.Name = "CapitalizacionDataGridViewTextBoxColumn"
        '
        'JubiladosBindingSource
        '
        Me.JubiladosBindingSource.DataMember = "jubilados"
        Me.JubiladosBindingSource.DataSource = Me.DSSipres
        '
        'DSSipres
        '
        Me.DSSipres.DataSetName = "DSSipres"
        Me.DSSipres.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Btn_generate_recibos
        '
        Me.Btn_generate_recibos.Location = New System.Drawing.Point(597, 5)
        Me.Btn_generate_recibos.Name = "Btn_generate_recibos"
        Me.Btn_generate_recibos.Size = New System.Drawing.Size(176, 23)
        Me.Btn_generate_recibos.TabIndex = 4
        Me.Btn_generate_recibos.Text = "Generar Masivamente"
        Me.Btn_generate_recibos.UseVisualStyleBackColor = True
        '
        'FrmSipres
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FrmSipres"
        Me.Opacity = 0.95R
        Me.Text = "SIPRES MENU"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DGVRAjubilados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JubiladosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button2 As Button
    Friend WithEvents Btn_new_recibo As Button
    Friend WithEvents DGVRAjubilados As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents MatriculaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DniDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents JubilacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CapitalizacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents JubiladosBindingSource As BindingSource
    Friend WithEvents DSSipres As DSSipres
    Friend WithEvents Btn_generate_recibos As Button
End Class
