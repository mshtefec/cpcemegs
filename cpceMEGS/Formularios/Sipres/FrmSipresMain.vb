﻿Public Class FrmSipresMain

    Private Sub FrmSipresMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Guna.UI.Lib.GraphicsHelper.ShadowForm(Me)
        Guna.UI.Lib.GraphicsHelper.DrawLineShadow(GunaPanel4, Color.Black, 20, 5, Guna.UI.WinForms.VerHorAlign.HoriziontalTop)

        Dim uc As New UCListados
        SwitchPanel(uc)
    End Sub

    Sub SwitchPanel(ByVal panel As UserControl)
        pn_container.Controls.Clear()
        pn_container.Controls.Add(panel)
        panel.Show()
        panel.Dock = DockStyle.Fill
    End Sub

    Private Sub btn_listado_Click(sender As Object, e As EventArgs) Handles btn_listado.Click
        Dim uc As New UCListados
        SwitchPanel(uc)
    End Sub

    Private Sub btn_debitos_Click(sender As Object, e As EventArgs) Handles btn_debitos.Click
        Dim uc As New UCDebitos
        SwitchPanel(uc)
    End Sub

    Private Sub btn_proximos_Click(sender As Object, e As EventArgs) Handles btn_proximos.Click
        Dim uc As New UCProximos
        SwitchPanel(uc)
    End Sub

    Private Sub btn_cumplen_Click(sender As Object, e As EventArgs) Handles btn_cumplen.Click
        Dim uc As New UCCumplen
        SwitchPanel(uc)
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub
End Class