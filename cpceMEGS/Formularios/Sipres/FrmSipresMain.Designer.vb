﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSipresMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSipresMain))
        Me.GunaPanel1 = New Guna.UI.WinForms.GunaPanel()
        Me.btn_cumplen = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_proximos = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_debitos = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_listado = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.GunaLinePanel1 = New Guna.UI.WinForms.GunaLinePanel()
        Me.GunaLabel1 = New Guna.UI.WinForms.GunaLabel()
        Me.GunaPanel2 = New Guna.UI.WinForms.GunaPanel()
        Me.btn_close = New Guna.UI.WinForms.GunaImageButton()
        Me.GunaPanel4 = New Guna.UI.WinForms.GunaPanel()
        Me.pn_container = New Guna.UI.WinForms.GunaPanel()
        Me.GunaDragControl1 = New Guna.UI.WinForms.GunaDragControl(Me.components)
        Me.GunaPanel1.SuspendLayout()
        Me.GunaLinePanel1.SuspendLayout()
        Me.GunaPanel2.SuspendLayout()
        Me.GunaPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GunaPanel1
        '
        Me.GunaPanel1.BackColor = System.Drawing.Color.White
        Me.GunaPanel1.Controls.Add(Me.btn_cumplen)
        Me.GunaPanel1.Controls.Add(Me.btn_proximos)
        Me.GunaPanel1.Controls.Add(Me.btn_debitos)
        Me.GunaPanel1.Controls.Add(Me.btn_listado)
        Me.GunaPanel1.Controls.Add(Me.GunaLinePanel1)
        Me.GunaPanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.GunaPanel1.Location = New System.Drawing.Point(0, 0)
        Me.GunaPanel1.Name = "GunaPanel1"
        Me.GunaPanel1.Size = New System.Drawing.Size(180, 620)
        Me.GunaPanel1.TabIndex = 0
        '
        'btn_cumplen
        '
        Me.btn_cumplen.Animated = True
        Me.btn_cumplen.AnimationHoverSpeed = 0.07!
        Me.btn_cumplen.AnimationSpeed = 0.03!
        Me.btn_cumplen.BackColor = System.Drawing.Color.Transparent
        Me.btn_cumplen.BaseColor = System.Drawing.Color.White
        Me.btn_cumplen.BorderColor = System.Drawing.Color.Black
        Me.btn_cumplen.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_cumplen.CheckedBaseColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btn_cumplen.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_cumplen.CheckedForeColor = System.Drawing.Color.White
        Me.btn_cumplen.CheckedImage = CType(resources.GetObject("btn_cumplen.CheckedImage"), System.Drawing.Image)
        Me.btn_cumplen.CheckedLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_cumplen.Dock = System.Windows.Forms.DockStyle.Top
        Me.btn_cumplen.FocusedColor = System.Drawing.Color.Empty
        Me.btn_cumplen.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.btn_cumplen.ForeColor = System.Drawing.Color.Black
        Me.btn_cumplen.Image = CType(resources.GetObject("btn_cumplen.Image"), System.Drawing.Image)
        Me.btn_cumplen.ImageOffsetX = 8
        Me.btn_cumplen.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_cumplen.LineColor = System.Drawing.Color.White
        Me.btn_cumplen.LineLeft = 5
        Me.btn_cumplen.Location = New System.Drawing.Point(0, 155)
        Me.btn_cumplen.Name = "btn_cumplen"
        Me.btn_cumplen.OnHoverBaseColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_cumplen.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_cumplen.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_cumplen.OnHoverImage = CType(resources.GetObject("btn_cumplen.OnHoverImage"), System.Drawing.Image)
        Me.btn_cumplen.OnHoverLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_cumplen.OnPressedColor = System.Drawing.Color.Black
        Me.btn_cumplen.Radius = 10
        Me.btn_cumplen.Size = New System.Drawing.Size(180, 35)
        Me.btn_cumplen.TabIndex = 4
        Me.btn_cumplen.Text = "CUMPLEN HOY"
        '
        'btn_proximos
        '
        Me.btn_proximos.Animated = True
        Me.btn_proximos.AnimationHoverSpeed = 0.07!
        Me.btn_proximos.AnimationSpeed = 0.03!
        Me.btn_proximos.BackColor = System.Drawing.Color.Transparent
        Me.btn_proximos.BaseColor = System.Drawing.Color.White
        Me.btn_proximos.BorderColor = System.Drawing.Color.Black
        Me.btn_proximos.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_proximos.CheckedBaseColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btn_proximos.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_proximos.CheckedForeColor = System.Drawing.Color.White
        Me.btn_proximos.CheckedImage = CType(resources.GetObject("btn_proximos.CheckedImage"), System.Drawing.Image)
        Me.btn_proximos.CheckedLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_proximos.Dock = System.Windows.Forms.DockStyle.Top
        Me.btn_proximos.FocusedColor = System.Drawing.Color.Empty
        Me.btn_proximos.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.btn_proximos.ForeColor = System.Drawing.Color.Black
        Me.btn_proximos.Image = CType(resources.GetObject("btn_proximos.Image"), System.Drawing.Image)
        Me.btn_proximos.ImageOffsetX = 8
        Me.btn_proximos.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_proximos.LineColor = System.Drawing.Color.White
        Me.btn_proximos.LineLeft = 5
        Me.btn_proximos.Location = New System.Drawing.Point(0, 120)
        Me.btn_proximos.Name = "btn_proximos"
        Me.btn_proximos.OnHoverBaseColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_proximos.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_proximos.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_proximos.OnHoverImage = CType(resources.GetObject("btn_proximos.OnHoverImage"), System.Drawing.Image)
        Me.btn_proximos.OnHoverLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_proximos.OnPressedColor = System.Drawing.Color.Black
        Me.btn_proximos.Radius = 10
        Me.btn_proximos.Size = New System.Drawing.Size(180, 35)
        Me.btn_proximos.TabIndex = 3
        Me.btn_proximos.Text = "PRÓXIMOS"
        '
        'btn_debitos
        '
        Me.btn_debitos.Animated = True
        Me.btn_debitos.AnimationHoverSpeed = 0.07!
        Me.btn_debitos.AnimationSpeed = 0.03!
        Me.btn_debitos.BackColor = System.Drawing.Color.Transparent
        Me.btn_debitos.BaseColor = System.Drawing.Color.White
        Me.btn_debitos.BorderColor = System.Drawing.Color.Black
        Me.btn_debitos.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_debitos.CheckedBaseColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btn_debitos.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_debitos.CheckedForeColor = System.Drawing.Color.White
        Me.btn_debitos.CheckedImage = CType(resources.GetObject("btn_debitos.CheckedImage"), System.Drawing.Image)
        Me.btn_debitos.CheckedLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_debitos.Dock = System.Windows.Forms.DockStyle.Top
        Me.btn_debitos.FocusedColor = System.Drawing.Color.Empty
        Me.btn_debitos.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.btn_debitos.ForeColor = System.Drawing.Color.Black
        Me.btn_debitos.Image = CType(resources.GetObject("btn_debitos.Image"), System.Drawing.Image)
        Me.btn_debitos.ImageOffsetX = 8
        Me.btn_debitos.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_debitos.LineColor = System.Drawing.Color.White
        Me.btn_debitos.LineLeft = 5
        Me.btn_debitos.Location = New System.Drawing.Point(0, 85)
        Me.btn_debitos.Name = "btn_debitos"
        Me.btn_debitos.OnHoverBaseColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_debitos.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_debitos.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_debitos.OnHoverImage = CType(resources.GetObject("btn_debitos.OnHoverImage"), System.Drawing.Image)
        Me.btn_debitos.OnHoverLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_debitos.OnPressedColor = System.Drawing.Color.Black
        Me.btn_debitos.Radius = 10
        Me.btn_debitos.Size = New System.Drawing.Size(180, 35)
        Me.btn_debitos.TabIndex = 2
        Me.btn_debitos.Text = "DEBITOS"
        '
        'btn_listado
        '
        Me.btn_listado.Animated = True
        Me.btn_listado.AnimationHoverSpeed = 0.07!
        Me.btn_listado.AnimationSpeed = 0.03!
        Me.btn_listado.BackColor = System.Drawing.Color.Transparent
        Me.btn_listado.BaseColor = System.Drawing.Color.White
        Me.btn_listado.BorderColor = System.Drawing.Color.Black
        Me.btn_listado.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_listado.Checked = True
        Me.btn_listado.CheckedBaseColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(53, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btn_listado.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_listado.CheckedForeColor = System.Drawing.Color.White
        Me.btn_listado.CheckedImage = CType(resources.GetObject("btn_listado.CheckedImage"), System.Drawing.Image)
        Me.btn_listado.CheckedLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_listado.Dock = System.Windows.Forms.DockStyle.Top
        Me.btn_listado.FocusedColor = System.Drawing.Color.Empty
        Me.btn_listado.Font = New System.Drawing.Font("Segoe UI Semibold", 10.0!)
        Me.btn_listado.ForeColor = System.Drawing.Color.Black
        Me.btn_listado.Image = CType(resources.GetObject("btn_listado.Image"), System.Drawing.Image)
        Me.btn_listado.ImageOffsetX = 8
        Me.btn_listado.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_listado.LineColor = System.Drawing.Color.White
        Me.btn_listado.LineLeft = 5
        Me.btn_listado.Location = New System.Drawing.Point(0, 50)
        Me.btn_listado.Name = "btn_listado"
        Me.btn_listado.OnHoverBaseColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_listado.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_listado.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_listado.OnHoverImage = CType(resources.GetObject("btn_listado.OnHoverImage"), System.Drawing.Image)
        Me.btn_listado.OnHoverLineColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.btn_listado.OnPressedColor = System.Drawing.Color.Black
        Me.btn_listado.Radius = 10
        Me.btn_listado.Size = New System.Drawing.Size(180, 35)
        Me.btn_listado.TabIndex = 1
        Me.btn_listado.Text = "LISTADO"
        '
        'GunaLinePanel1
        '
        Me.GunaLinePanel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(207, Byte), Integer))
        Me.GunaLinePanel1.Controls.Add(Me.GunaLabel1)
        Me.GunaLinePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GunaLinePanel1.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.GunaLinePanel1.LineColor = System.Drawing.Color.Black
        Me.GunaLinePanel1.LineStyle = System.Windows.Forms.BorderStyle.None
        Me.GunaLinePanel1.Location = New System.Drawing.Point(0, 0)
        Me.GunaLinePanel1.Name = "GunaLinePanel1"
        Me.GunaLinePanel1.Size = New System.Drawing.Size(180, 50)
        Me.GunaLinePanel1.TabIndex = 1
        '
        'GunaLabel1
        '
        Me.GunaLabel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GunaLabel1.AutoSize = True
        Me.GunaLabel1.Font = New System.Drawing.Font("Segoe UI", 16.0!, System.Drawing.FontStyle.Bold)
        Me.GunaLabel1.ForeColor = System.Drawing.Color.White
        Me.GunaLabel1.Location = New System.Drawing.Point(42, 9)
        Me.GunaLabel1.Name = "GunaLabel1"
        Me.GunaLabel1.Size = New System.Drawing.Size(84, 30)
        Me.GunaLabel1.TabIndex = 1
        Me.GunaLabel1.Text = "SIPRES"
        '
        'GunaPanel2
        '
        Me.GunaPanel2.Controls.Add(Me.btn_close)
        Me.GunaPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GunaPanel2.Location = New System.Drawing.Point(180, 0)
        Me.GunaPanel2.Name = "GunaPanel2"
        Me.GunaPanel2.Size = New System.Drawing.Size(790, 50)
        Me.GunaPanel2.TabIndex = 1
        '
        'btn_close
        '
        Me.btn_close.BackColor = System.Drawing.Color.Transparent
        Me.btn_close.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btn_close.Cursor = System.Windows.Forms.Cursors.Default
        Me.btn_close.Image = CType(resources.GetObject("btn_close.Image"), System.Drawing.Image)
        Me.btn_close.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_close.Location = New System.Drawing.Point(746, 9)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.OnHoverImage = CType(resources.GetObject("btn_close.OnHoverImage"), System.Drawing.Image)
        Me.btn_close.OnHoverImageOffset = New System.Drawing.Point(1, -1)
        Me.btn_close.Size = New System.Drawing.Size(32, 32)
        Me.btn_close.TabIndex = 0
        '
        'GunaPanel4
        '
        Me.GunaPanel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GunaPanel4.Controls.Add(Me.pn_container)
        Me.GunaPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GunaPanel4.Location = New System.Drawing.Point(180, 50)
        Me.GunaPanel4.Name = "GunaPanel4"
        Me.GunaPanel4.Padding = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.GunaPanel4.Size = New System.Drawing.Size(790, 570)
        Me.GunaPanel4.TabIndex = 2
        '
        'pn_container
        '
        Me.pn_container.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pn_container.Location = New System.Drawing.Point(0, 6)
        Me.pn_container.Name = "pn_container"
        Me.pn_container.Size = New System.Drawing.Size(790, 564)
        Me.pn_container.TabIndex = 0
        '
        'GunaDragControl1
        '
        Me.GunaDragControl1.TargetControl = Me.GunaPanel2
        '
        'FrmSipresMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(970, 620)
        Me.Controls.Add(Me.GunaPanel4)
        Me.Controls.Add(Me.GunaPanel2)
        Me.Controls.Add(Me.GunaPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSipresMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " "
        Me.GunaPanel1.ResumeLayout(False)
        Me.GunaLinePanel1.ResumeLayout(False)
        Me.GunaLinePanel1.PerformLayout()
        Me.GunaPanel2.ResumeLayout(False)
        Me.GunaPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GunaPanel1 As Guna.UI.WinForms.GunaPanel
    Friend WithEvents GunaLinePanel1 As Guna.UI.WinForms.GunaLinePanel
    Friend WithEvents GunaLabel1 As Guna.UI.WinForms.GunaLabel
    Friend WithEvents btn_listado As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_cumplen As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_proximos As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_debitos As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents GunaPanel2 As Guna.UI.WinForms.GunaPanel
    Friend WithEvents GunaPanel4 As Guna.UI.WinForms.GunaPanel
    Friend WithEvents GunaDragControl1 As Guna.UI.WinForms.GunaDragControl
    Friend WithEvents pn_container As Guna.UI.WinForms.GunaPanel
    Friend WithEvents btn_close As Guna.UI.WinForms.GunaImageButton
End Class
