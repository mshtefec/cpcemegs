﻿Imports MySql.Data.MySqlClient

Public Class FrmSipres

    'BD
    Private consultaBD As New ConsultaBD(True)
    Private myda As MySqlDataAdapter

    'UTIL
    Private util As New Util()

    Private Enum Selection
        matricula
        nombre
        dni
        jubilacion
        capitalizacion
    End Enum

    Private Enum Procesos
        DEJUBI
    End Enum

    Private Sub FrmSipres_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            consultaBD.AbrirConexion()

            myda = consultaBD.consultaBDadapter(
                "afiliado as afi",
                "CONCAT(afi.afi_titulo, afi.afi_matricula) AS MATRICULA, 
                 afi.afi_nombre AS NOMBRE, 
                 afi.afi_nrodoc AS DNI, 
                 afi.afi_fecha_jubilacion AS JUBILACION, 
                 afi.afi_jubi_capitalizacion AS CAPITALIZACION",
                "MID(afi.afi_categoria,1,2)='17' AND afi.afi_titulo <> 'EC' AND afi.afi_titulo <> 'DE' AND afi.afi_tipo = 'A' 
                 ORDER BY nombre"
            )

            myda.Fill(DSSipres, "jubilados")

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub

    Private Sub Btn_new_recibo_Click(sender As Object, e As EventArgs) Handles Btn_new_recibo.Click
        Try

            consultaBD.AbrirConexion()

            'Obtengo los todos los datos del afiliado
            Dim jubiladoInfo As DataRow = util.GetInfoAfiliado(DGVRAjubilados.CurrentRow.Cells(Selection.dni).Value)

            'Intento crear el Comprobante y cargarlo con todos los datos y generar los asientos correspondiente.
            Dim dsAsiento As DataSet = util.GeneroComprobante(Procesos.DEJUBI.ToString(), jubiladoInfo)

            'Si no hay problemas persisto el Comprobante y los asientos.
            util.PersisteComprobante(dsAsiento)

            'Si el Comprobante y los asientos se persistieron correctamente, actualizo númerales.
            util.SetNroComprobanteSiguiente(Procesos.DEJUBI.ToString())
            util.SetNroAsientoSiguiente()

            MsgBox("Fila seleccionada con dni: " + DGVRAjubilados.CurrentRow.Cells(Selection.dni).Value + " generado recibo exitosamente", MsgBoxStyle.Exclamation, "RECIBO GENERADO")

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try

    End Sub

    Private Sub Btn_generate_recibos_Click(sender As Object, e As EventArgs) Handles Btn_generate_recibos.Click
        Try

            consultaBD.AbrirConexion()

            For Each rowAsiento As DataGridViewRow In DGVRAjubilados.Rows

                'Obtengo los todos los datos del afiliado
                Dim jubiladoInfo As DataRow = util.GetInfoAfiliado(rowAsiento.Cells.Item(Selection.dni).Value)

                'Intento crear el Comprobante y cargarlo con todos los datos y generar los asientos correspondiente.
                Dim dsAsiento As DataSet = util.GeneroComprobante(Procesos.DEJUBI.ToString(), jubiladoInfo)

                'Si no hay problemas persisto el Comprobante y los asientos.
                util.PersisteComprobante(dsAsiento)

                'Si el Comprobante y los asientos se persistieron correctamente, actualizo númerales.
                util.SetNroComprobanteSiguiente(Procesos.DEJUBI.ToString())
                util.SetNroAsientoSiguiente()
            Next

            MsgBox("Todos los Recibos Generados Exitosamente!", MsgBoxStyle.Exclamation, "RECIBO GENERADO")

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub
End Class