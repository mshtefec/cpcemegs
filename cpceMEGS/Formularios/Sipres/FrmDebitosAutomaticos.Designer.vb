﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmDebitosAutomaticos
	Inherits System.Windows.Forms.Form

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.TabControlDebitos = New System.Windows.Forms.TabControl()
		Me.TabListado = New System.Windows.Forms.TabPage()
		Me.dgv_listado = New System.Windows.Forms.DataGridView()
		Me.gb_tools = New System.Windows.Forms.GroupBox()
		Me.lb_file_estado = New System.Windows.Forms.Label()
		Me.lb_estado = New System.Windows.Forms.Label()
		Me.lb_file_title = New System.Windows.Forms.Label()
		Me.lb_archivo = New System.Windows.Forms.Label()
		Me.btn_importar = New System.Windows.Forms.Button()
		Me.gb_details = New System.Windows.Forms.GroupBox()
		Me.cbox_fecha_concepto = New System.Windows.Forms.CheckBox()
		Me.dtime_concepto = New System.Windows.Forms.DateTimePicker()
		Me.cbox_fecha_comprobante = New System.Windows.Forms.CheckBox()
		Me.dtime_comprobante = New System.Windows.Forms.DateTimePicker()
		Me.lb_fecha = New System.Windows.Forms.Label()
		Me.lb_file_fecha = New System.Windows.Forms.Label()
		Me.TabControlDebitos.SuspendLayout()
		Me.TabListado.SuspendLayout()
		CType(Me.dgv_listado, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.gb_tools.SuspendLayout()
		Me.gb_details.SuspendLayout()
		Me.SuspendLayout()
		'
		'TabControlDebitos
		'
		Me.TabControlDebitos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TabControlDebitos.Controls.Add(Me.TabListado)
		Me.TabControlDebitos.Location = New System.Drawing.Point(12, 71)
		Me.TabControlDebitos.Name = "TabControlDebitos"
		Me.TabControlDebitos.SelectedIndex = 0
		Me.TabControlDebitos.Size = New System.Drawing.Size(906, 402)
		Me.TabControlDebitos.TabIndex = 0
		'
		'TabListado
		'
		Me.TabListado.Controls.Add(Me.dgv_listado)
		Me.TabListado.Location = New System.Drawing.Point(4, 22)
		Me.TabListado.Name = "TabListado"
		Me.TabListado.Padding = New System.Windows.Forms.Padding(3)
		Me.TabListado.Size = New System.Drawing.Size(898, 376)
		Me.TabListado.TabIndex = 1
		Me.TabListado.Text = "Listado"
		Me.TabListado.UseVisualStyleBackColor = True
		'
		'dgv_listado
		'
		Me.dgv_listado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.dgv_listado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.dgv_listado.Location = New System.Drawing.Point(3, 3)
		Me.dgv_listado.Name = "dgv_listado"
		Me.dgv_listado.Size = New System.Drawing.Size(892, 370)
		Me.dgv_listado.TabIndex = 0
		'
		'gb_tools
		'
		Me.gb_tools.Controls.Add(Me.lb_file_fecha)
		Me.gb_tools.Controls.Add(Me.lb_fecha)
		Me.gb_tools.Controls.Add(Me.lb_file_estado)
		Me.gb_tools.Controls.Add(Me.lb_estado)
		Me.gb_tools.Controls.Add(Me.lb_file_title)
		Me.gb_tools.Controls.Add(Me.lb_archivo)
		Me.gb_tools.Location = New System.Drawing.Point(16, 12)
		Me.gb_tools.Name = "gb_tools"
		Me.gb_tools.Size = New System.Drawing.Size(440, 49)
		Me.gb_tools.TabIndex = 1
		Me.gb_tools.TabStop = False
		Me.gb_tools.Text = "Informe"
		'
		'lb_file_estado
		'
		Me.lb_file_estado.AutoSize = True
		Me.lb_file_estado.Location = New System.Drawing.Point(359, 20)
		Me.lb_file_estado.Name = "lb_file_estado"
		Me.lb_file_estado.Size = New System.Drawing.Size(45, 13)
		Me.lb_file_estado.TabIndex = 4
		Me.lb_file_estado.Text = "ninguno"
		'
		'lb_estado
		'
		Me.lb_estado.AutoSize = True
		Me.lb_estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lb_estado.Location = New System.Drawing.Point(307, 20)
		Me.lb_estado.Name = "lb_estado"
		Me.lb_estado.Size = New System.Drawing.Size(49, 13)
		Me.lb_estado.TabIndex = 3
		Me.lb_estado.Text = "estado:"
		'
		'lb_file_title
		'
		Me.lb_file_title.Location = New System.Drawing.Point(59, 20)
		Me.lb_file_title.Name = "lb_file_title"
		Me.lb_file_title.Size = New System.Drawing.Size(130, 17)
		Me.lb_file_title.TabIndex = 2
		Me.lb_file_title.Text = "ninguno"
		'
		'lb_archivo
		'
		Me.lb_archivo.AutoSize = True
		Me.lb_archivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lb_archivo.Location = New System.Drawing.Point(7, 20)
		Me.lb_archivo.Name = "lb_archivo"
		Me.lb_archivo.Size = New System.Drawing.Size(53, 13)
		Me.lb_archivo.TabIndex = 1
		Me.lb_archivo.Text = "archivo:"
		'
		'btn_importar
		'
		Me.btn_importar.Location = New System.Drawing.Point(825, 38)
		Me.btn_importar.Name = "btn_importar"
		Me.btn_importar.Size = New System.Drawing.Size(75, 23)
		Me.btn_importar.TabIndex = 0
		Me.btn_importar.Text = "Importar"
		Me.btn_importar.UseVisualStyleBackColor = True
		'
		'gb_details
		'
		Me.gb_details.Controls.Add(Me.cbox_fecha_concepto)
		Me.gb_details.Controls.Add(Me.dtime_concepto)
		Me.gb_details.Controls.Add(Me.cbox_fecha_comprobante)
		Me.gb_details.Controls.Add(Me.dtime_comprobante)
		Me.gb_details.Location = New System.Drawing.Point(462, 12)
		Me.gb_details.Name = "gb_details"
		Me.gb_details.Size = New System.Drawing.Size(349, 75)
		Me.gb_details.TabIndex = 2
		Me.gb_details.TabStop = False
		Me.gb_details.Text = "Detalles"
		'
		'cbox_fecha_concepto
		'
		Me.cbox_fecha_concepto.AutoSize = True
		Me.cbox_fecha_concepto.Location = New System.Drawing.Point(176, 26)
		Me.cbox_fecha_concepto.Name = "cbox_fecha_concepto"
		Me.cbox_fecha_concepto.Size = New System.Drawing.Size(101, 17)
		Me.cbox_fecha_concepto.TabIndex = 3
		Me.cbox_fecha_concepto.Text = "fecha concepto"
		Me.cbox_fecha_concepto.UseVisualStyleBackColor = True
		'
		'dtime_concepto
		'
		Me.dtime_concepto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtime_concepto.Location = New System.Drawing.Point(176, 49)
		Me.dtime_concepto.Name = "dtime_concepto"
		Me.dtime_concepto.Size = New System.Drawing.Size(165, 20)
		Me.dtime_concepto.TabIndex = 2
		'
		'cbox_fecha_comprobante
		'
		Me.cbox_fecha_comprobante.AutoSize = True
		Me.cbox_fecha_comprobante.Location = New System.Drawing.Point(6, 26)
		Me.cbox_fecha_comprobante.Name = "cbox_fecha_comprobante"
		Me.cbox_fecha_comprobante.Size = New System.Drawing.Size(118, 17)
		Me.cbox_fecha_comprobante.TabIndex = 1
		Me.cbox_fecha_comprobante.Text = "fecha comprobante"
		Me.cbox_fecha_comprobante.UseVisualStyleBackColor = True
		'
		'dtime_comprobante
		'
		Me.dtime_comprobante.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
		Me.dtime_comprobante.Location = New System.Drawing.Point(6, 49)
		Me.dtime_comprobante.Name = "dtime_comprobante"
		Me.dtime_comprobante.Size = New System.Drawing.Size(165, 20)
		Me.dtime_comprobante.TabIndex = 0
		'
		'lb_fecha
		'
		Me.lb_fecha.AutoSize = True
		Me.lb_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lb_fecha.Location = New System.Drawing.Point(195, 20)
		Me.lb_fecha.Name = "lb_fecha"
		Me.lb_fecha.Size = New System.Drawing.Size(43, 13)
		Me.lb_fecha.TabIndex = 5
		Me.lb_fecha.Text = "fecha:"
		'
		'lb_file_fecha
		'
		Me.lb_file_fecha.AutoSize = True
		Me.lb_file_fecha.Location = New System.Drawing.Point(238, 20)
		Me.lb_file_fecha.Name = "lb_file_fecha"
		Me.lb_file_fecha.Size = New System.Drawing.Size(45, 13)
		Me.lb_file_fecha.TabIndex = 6
		Me.lb_file_fecha.Text = "ninguno"
		'
		'FrmDebitosAutomaticos
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(930, 485)
		Me.Controls.Add(Me.gb_details)
		Me.Controls.Add(Me.gb_tools)
		Me.Controls.Add(Me.TabControlDebitos)
		Me.Controls.Add(Me.btn_importar)
		Me.Name = "FrmDebitosAutomaticos"
		Me.Text = "Sipres Debitos Automaticos"
		Me.TabControlDebitos.ResumeLayout(False)
		Me.TabListado.ResumeLayout(False)
		CType(Me.dgv_listado, System.ComponentModel.ISupportInitialize).EndInit()
		Me.gb_tools.ResumeLayout(False)
		Me.gb_tools.PerformLayout()
		Me.gb_details.ResumeLayout(False)
		Me.gb_details.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents TabControlDebitos As TabControl
	Friend WithEvents TabListado As TabPage
	Friend WithEvents dgv_listado As DataGridView
	Friend WithEvents gb_tools As GroupBox
	Friend WithEvents btn_importar As Button
	Friend WithEvents lb_file_title As Label
	Friend WithEvents lb_archivo As Label
	Friend WithEvents lb_file_estado As Label
	Friend WithEvents lb_estado As Label
	Friend WithEvents gb_details As GroupBox
	Friend WithEvents dtime_comprobante As DateTimePicker
	Friend WithEvents cbox_fecha_comprobante As CheckBox
	Friend WithEvents cbox_fecha_concepto As CheckBox
	Friend WithEvents dtime_concepto As DateTimePicker
	Friend WithEvents lb_fecha As Label
	Friend WithEvents lb_file_fecha As Label
End Class
