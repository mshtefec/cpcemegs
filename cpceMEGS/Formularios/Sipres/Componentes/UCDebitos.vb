﻿Imports System.ComponentModel
Imports MySql.Data.MySqlClient

Public Class UCDebitos

    'BD
    Private consultaBD As New ConsultaBD(True)
    Private myda As MySqlDataAdapter

    Private Enum Selection
        matricula
        nombre
        dni
        jubilacion
        capitalizacion
    End Enum

    Private Sub UCDebitos_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            consultaBD.AbrirConexion()

            myda = consultaBD.consultaBDadapter(
                "afiliado as afi",
                "CONCAT(afi.afi_titulo, afi.afi_matricula) AS MATRICULA, 
                    afi.afi_nombre AS NOMBRE, 
                    afi.afi_nrodoc AS DNI, 
                    afi.afi_fecha_jubilacion AS JUBILACION, 
                    afi.afi_jubi_capitalizacion AS CAPITALIZACION",
                "MID(afi.afi_categoria,1,2)='17' AND afi.afi_titulo <> 'EC' AND afi.afi_titulo <> 'DE' AND afi.afi_tipo = 'A' 
                 ORDER BY nombre"
            )

            myda.Fill(DSSipres, "jubilados")

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub

    Private Sub btn_generar_recibo_Click(sender As Object, e As EventArgs) Handles btn_generar_recibo.Click
        Try
            Dim dniJubilado As String = bdgv_debito.CurrentRow.Cells(Selection.dni).Value

            consultaBD.AbrirConexion()

            myda = consultaBD.consultaBDadapter(
                "afiliado",
                "*",
                "afi_tipdoc = 'DNI' AND afi_nrodoc = " + dniJubilado + ";"
            )

            Dim dtJubiladoInfo As New DataTable()

            myda.Fill(dtJubiladoInfo)

            Dim jubiladoInfo As DataRow = dtJubiladoInfo.Rows(0)
            Dim dsAsiento As DataSet = CreaAsiento("DEJUBI", jubiladoInfo)

            GrabaAsiento(dsAsiento)

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try

        MsgBox("Fila seleccionada con dni: " + bdgv_debito.CurrentRow.Cells(Selection.dni).Value, MsgBoxStyle.Exclamation, "FILA")
    End Sub

    Private Function CreaAsiento(ByVal proceso As String, ByRef jubiladoInfo As DataRow) As DataSet

        Dim zeta = consultaBD.Zeta
        Dim caja = consultaBD.Caja
        Dim dsAsiento As New DataSet("Asiento")

        Dim dtComprobante As New DSComprobante.comprobanteDataTable()
        Dim dtTotales As DSComprobante.totalesDataTable
        Dim dtProcesos As New DSComprobante.procesosDataTable()
        Dim rowProceso As DSComprobante.procesosRow
        Dim dtProcetote As New DSComprobante.procetoteDataTable()

        myda = consultaBD.consultaBDadapter(
            "procesos",
            "*",
            "pro_codigo = '" + proceso + "';"
        )

        myda.Fill(dtProcesos)

        rowProceso = dtProcesos.Rows(0)

        myda = consultaBD.consultaBDadapter(
            "procetote",
            "*",
            "pto_codpro = '" + proceso + "';"
        )

        myda.Fill(dtProcetote)

        Dim row As DSComprobante.comprobanteRow = dtComprobante.NewRow()

        row = CreoComprobante(row, jubiladoInfo, rowProceso)

        dtComprobante.AddcomprobanteRow(row)

        dtTotales = CreoAsiento(dtProcetote, jubiladoInfo, rowProceso)

        dsAsiento.Tables.Add(dtComprobante)
        dsAsiento.Tables.Add(dtTotales)
        dsAsiento.Tables.Add(dtProcesos)
        dsAsiento.Tables.Add(dtProcetote)

        Return dsAsiento

    End Function

    Private Sub GrabaAsiento(ByRef dsAsiento As DataSet)

        Dim campos As String = ""

        'For Each rowComprobante As DSComprobante.comprobanteRow In dsAsiento.Tables("comprobante").Rows
        'campos = String.Join(",", rowComprobante.ItemArray)
        'Next

        If dsAsiento IsNot Nothing Then

            If dsAsiento.Tables("comprobante").Rows.Count > 0 Then
                For Each rowComprobante As DSComprobante.comprobanteRow In dsAsiento.Tables("comprobante").Rows
                    consultaBD.ReplaceBD("insert into comproba set com_unegos='" & rowComprobante.com_unegos & "'," &
                          "com_nrodeleg='" & rowComprobante.com_nrodeleg & "'," &
                          "com_proceso='" & rowComprobante.com_proceso & "'," &
                          "com_nrocli='" & rowComprobante.com_nrodeleg & "'," &
                          "com_nrocom='" & rowComprobante.com_nrocom & "'," &
                          "com_nroasi='" & rowComprobante.com_nroasi & "'," &
                          "com_asigrupal='" & rowComprobante.com_asigrupal & "'," &
                          "com_fecha='" & rowComprobante.com_fecha & "'," &
                          "com_total='" & rowComprobante.com_total.ToString.Replace(",", ".") & "'," &
                          "com_tipdoc='" & rowComprobante.com_tipdoc & "'," &
                          "com_nrodoc='" & rowComprobante.com_nrodoc & "'," &
                          "com_titulo='" & rowComprobante.com_titulo & "'," &
                          "com_matricula='" & rowComprobante.com_matricula & "'," &
                          "com_nroope='" & rowComprobante.com_nroope & "'," &
                          "com_caja='" & rowComprobante.com_caja & "'," &
                          "com_zeta='" & rowComprobante.com_zeta & "'," &
                          "com_concepto1='" & rowComprobante.com_concepto1 & "'," &
                          "com_concepto2='" & rowComprobante.com_concepto2 & "'," &
                          "com_concepto3='" & rowComprobante.com_concepto3 & "'," &
                          "com_destinatario='" & rowComprobante.com_destinatario & "'," &
                          "com_fecalt='" & rowComprobante.com_fecalt & "'")
                Next
            End If

            If dsAsiento.Tables("totales").Rows.Count > 0 Then
                For Each rowAsiento As DSComprobante.totalesRow In dsAsiento.Tables("totales").Rows
                    consultaBD.ReplaceBD("insert into totales set tot_unegos='" & rowAsiento.tot_unegos & "'," &
                        "tot_proceso='" & rowAsiento.tot_proceso & "'," &
                        "tot_nrocli='" & rowAsiento.tot_nrocli & "'," &
                        "tot_nrocom='" & rowAsiento.tot_nrocom & "'," &
                        "tot_item='" & rowAsiento.tot_item & "'," &
                        "tot_nropla='" & rowAsiento.tot_nropla & "'," &
                        "tot_subpla='" & rowAsiento.tot_subpla & "'," &
                        "tot_titulo='" & rowAsiento.tot_titulo & "'," &
                        "tot_matricula='" & rowAsiento.tot_matricula & "'," &
                        "tot_tipdoc='" & rowAsiento.tot_tipdoc & "'," &
                        "tot_nrodoc='" & rowAsiento.tot_nrodoc & "'," &
                        "tot_nroope='" & rowAsiento.tot_nroope & "'," &
                        "tot_nrocuo='" & rowAsiento.tot_nrocuo & "'," &
                        "tot_fecha='" & rowAsiento.tot_fecha & "'," &
                        "tot_fecven='" & Format(CDate(rowAsiento.tot_fecven.ToString), "yyyy-MM-dd") & "'," &
                        "tot_fecalt='" & rowAsiento.tot_fecalt & "'," &
                        "tot_nrocheque='" & rowAsiento.tot_nrocheque & "'," &
                        "tot_fecche='" & rowAsiento.tot_fecche & "'," &
                        "tot_debe='" & rowAsiento.tot_debe & "'," &
                        "tot_haber='" & rowAsiento.tot_haber & "'," &
                        "tot_imppag='" & rowAsiento.tot_imppag & "'," &
                        "tot_tipdes='" & rowAsiento.tot_tipdes & "'," &
                        "tot_nrodes='" & rowAsiento.tot_nrodes & "'," &
                        "tot_nroasi='" & rowAsiento.tot_nroasi & "'," &
                        "tot_asigrupal='" & rowAsiento.tot_asigrupal & "'," &
                        "tot_caja='" & rowAsiento.tot_caja & "'," &
                        "tot_zeta='" & rowAsiento.tot_zeta & "'," &
                        "tot_concepto='" & rowAsiento.tot_concepto & "'")
                Next
            End If

        End If

        'consultaBD.insertarBD("comproba", campos)

    End Sub

    Public Function CreoComprobante(ByRef comprobante As DSComprobante.comprobanteRow, ByRef matriculado As DataRow, ByVal proceso As DSComprobante.procesosRow) As DSComprobante.comprobanteRow

        Dim nroComprobante As Integer = GetNroComprobante(proceso.pro_codigo)
        Dim nroAsiento As Integer = GetNroAsiento()
        Dim nroAsientoReal As String = Convert.ToString(nroAsiento)
        nroAsientoReal = String.Concat(nroAsiento, "0201")

        Dim capitalizacion As Long = matriculado.Item("afi_jubi_capitalizacion")
        Dim periodos As Integer = matriculado.Item("afi_jubi_periodo_opto")

        Dim importeCuota As Double = Convert.ToDouble(matriculado.Item("afi_jubi_importe_cuota"))

        Dim importe As Double = capitalizacion / periodos

        comprobante.com_unegos = proceso.pro_instit
        comprobante.com_nrodeleg = 0
        comprobante.com_proceso = proceso.pro_codigo
        comprobante.com_nrocli = 0
        comprobante.com_nrocom = nroComprobante
        comprobante.com_preimpreso = "-"
        comprobante.com_nroope = 0
        comprobante.com_fecha = consultaBD.FechaHoraActual()
        comprobante.com_titulo = matriculado.Item("afi_titulo")
        comprobante.com_matricula = matriculado.Item("afi_matricula")
        comprobante.com_subcuenta = "-"
        comprobante.com_tipdoc = matriculado.Item("afi_tipdoc")
        comprobante.com_nrodoc = matriculado.Item("afi_nrodoc")
        comprobante.com_tipcmt = "-"
        comprobante.com_nrocmt = 0
        comprobante.com_nrocuo = 0
        comprobante.com_nropre = 0
        comprobante.com_tipo = "-"
        comprobante.com_lote = 1
        comprobante.com_caja = 0
        comprobante.com_nroreg = 0
        comprobante.com_zeta = 0
        comprobante.com_total = importeCuota
        comprobante.com_tipanu = "-"
        comprobante.com_punanu = 0
        comprobante.com_nroanu = 0
        comprobante.com_fecalt = Now.ToString("yyyy-MM-dd")
        comprobante.com_estado = "-"
        comprobante.com_fecven = "0000-00-00"
        comprobante.com_fecpag = "0000-00-00"
        comprobante.com_imppag = 0.00
        comprobante.com_tipmov = "-"
        comprobante.com_coeficiente = 0
        comprobante.com_nroasi = nroAsientoReal
        comprobante.com_concepto1 = proceso.pro_cpto1
        comprobante.com_concepto2 = proceso.pro_cpto2
        comprobante.com_concepto3 = "-"
        comprobante.com_leyenda = proceso.LEYENDA
        comprobante.com_refano = 0
        comprobante.com_refmes = 0
        comprobante.com_asiant = 0
        comprobante.com_tarea = 0
        comprobante.com_nrotrabajo = "-"
        comprobante.com_asigrupal = 0
        comprobante.com_nrolegali = 0
        comprobante.com_destinatario = "-"

        Return comprobante
    End Function

    Public Function CreoAsiento(ByRef dtProcetote As DSComprobante.procetoteDataTable, ByRef matriculado As DataRow, ByVal proceso As DSComprobante.procesosRow) As DSComprobante.totalesDataTable

        Dim dtTotales As New DSComprobante.totalesDataTable()

        Dim nroComprobante As Integer = GetNroComprobante(proceso.pro_codigo)
        Dim nroAsiento As Integer = GetNroAsiento()
        Dim nroAsientoReal As String = Convert.ToString(nroAsiento)
        nroAsientoReal = String.Concat(nroAsiento, "0201")

        Dim importeCuota As Double = Convert.ToDouble(matriculado.Item("afi_jubi_importe_cuota"))
        Dim importeDeudor As Double = 100
        Dim importeResto As Double = importeCuota - importeDeudor


        For Each rowProcetote As DSComprobante.procetoteRow In dtProcetote.Rows

            Dim rowT As DSComprobante.totalesRow = dtTotales.NewRow()
            rowT.tot_unegos = 1
            rowT.tot_proceso = rowProcetote.pto_codpro
            rowT.tot_nrocli = 1
            rowT.tot_nrocom = nroComprobante
            rowT.tot_item = rowProcetote.pto_item
            rowT.tot_nrocuo = 1
            rowT.tot_fecha = Now.ToString("yyyy-MM-dd")
            rowT.tot_fecven = Now.AddDays(1).ToString()
            rowT.tot_forpag = ""
            If rowProcetote.pto_tipmov = "D" Then
                'rowT.tot_debe = GetMontoCalculadoCuenta(rowProcetote.pto_formula)
                If rowProcetote.pto_nropla = 22010100 Then
                    rowT.tot_debe = importeCuota
                Else
                    rowT.tot_debe = "0"
                End If
                rowT.tot_haber = "0"
            Else
                rowT.tot_debe = "0"
                If rowProcetote.pto_nropla = 11031000 Then
                    rowT.tot_haber = importeResto
                ElseIf rowProcetote.pto_nropla = 13040300 Then
                    rowT.tot_haber = importeDeudor
                Else
                    rowT.tot_haber = "0"
                End If
            End If
            rowT.tot_nropla = rowProcetote.pto_nropla
            rowT.tot_subpla = String.Concat(matriculado.Item("afi_tipdoc"), matriculado.Item("afi_nrodoc"))
            rowT.tot_tipdoc = matriculado.Item("afi_tipdoc")
            rowT.tot_nrodoc = matriculado.Item("afi_nrodoc")
            rowT.tot_titulo = matriculado.Item("afi_titulo")
            rowT.tot_matricula = matriculado.Item("afi_matricula")
            rowT.tot_nroope = 0
            rowT.tot_nroreg = 0
            rowT.tot_caja = 0
            rowT.tot_zeta = 0
            rowT.tot_nroasi = nroAsientoReal
            rowT.tot_nrolegali = 0
            rowT.tot_concepto = ""
            rowT.tot_lote = 1
            rowT.tot_asiant = nroAsientoReal
            rowT.tot_estado = ""
            rowT.tot_fecconcilia = "0000-00-00 00:00:00"
            rowT.tot_opeconcilia = 0
            rowT.tot_fecvta = "0000-00-00"
            rowT.tot_asicancel = 0
            rowT.tot_imppag = 0.00
            rowT.tot_codretencion = 0
            rowT.tot_bonifica = 0.00
            rowT.tot_porcentaje = 0.00
            rowT.tot_sobre = 0.00
            rowT.tot_iva = 0.00
            rowT.tot_nrochequera = 0
            rowT.tot_letcheque = ""
            rowT.tot_nrocheque = 0
            rowT.tot_fecche = "0000-00-00"
            rowT.tot_fecdif = "0000-00-00"
            rowT.tot_tipdes = ""
            rowT.tot_nrodes = 0
            rowT.tot_estche = ""
            rowT.tot_asigrupal = 0
            rowT.tot_fecalt = "0000-00-00"

            dtTotales.AddtotalesRow(rowT)
        Next

        Return dtTotales
    End Function

    Public Function GetNroComprobante(ByVal codigo As String) As Integer

        Dim nroComprobante As String = consultaBD.consultaBDscalar(
            "numeros AS n, procesos AS p",
            "n.num_nrocom",
            "n.num_numerador = p.pro_numerador AND n.num_unegos = 1 AND n.num_nrocli = 1 AND p.pro_codigo = '" + codigo + "';"
        )
        nroComprobante = Convert.ToInt32(nroComprobante) + 1

        Return nroComprobante
    End Function

    Public Function GetNroAsiento() As Integer

        Dim codigo As String = "ASIENT"
        Dim nroAsiento As String = consultaBD.consultaBDscalar(
            "numeros AS n, procesos AS p",
            "n.num_nrocom",
            "n.num_numerador = p.pro_numerador AND n.num_unegos = 2 AND n.num_nrocli = 1 AND p.pro_codigo = '" + codigo + "';"
        )
        nroAsiento = Convert.ToInt64(nroAsiento) + 1

        Return nroAsiento
    End Function

    Public Function GetMontoCalculadoCuenta(ByVal porcentaje As String) As Double

    End Function

End Class
