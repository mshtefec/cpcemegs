﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCDebitos
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UCDebitos))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GunaShadowPanel1 = New Guna.UI.WinForms.GunaShadowPanel()
        Me.btn_generar_recibo = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_ver = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.bdgv_debito = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.MatriculaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JubilacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CapitalizacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.JubiladosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSSipres = New cpceMEGS.DSSipres()
        Me.lb_matricula = New Guna.UI.WinForms.GunaLabel()
        Me.GunaShadowPanel1.SuspendLayout()
        CType(Me.bdgv_debito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JubiladosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GunaShadowPanel1
        '
        Me.GunaShadowPanel1.BackColor = System.Drawing.Color.Transparent
        Me.GunaShadowPanel1.BaseColor = System.Drawing.Color.White
        Me.GunaShadowPanel1.Controls.Add(Me.btn_generar_recibo)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_ver)
        Me.GunaShadowPanel1.Controls.Add(Me.bdgv_debito)
        Me.GunaShadowPanel1.Controls.Add(Me.lb_matricula)
        Me.GunaShadowPanel1.Location = New System.Drawing.Point(21, 23)
        Me.GunaShadowPanel1.Name = "GunaShadowPanel1"
        Me.GunaShadowPanel1.ShadowColor = System.Drawing.Color.Black
        Me.GunaShadowPanel1.ShadowDepth = 180
        Me.GunaShadowPanel1.ShadowShift = 8
        Me.GunaShadowPanel1.ShadowStyle = Guna.UI.WinForms.ShadowMode.ForwardDiagonal
        Me.GunaShadowPanel1.Size = New System.Drawing.Size(746, 508)
        Me.GunaShadowPanel1.TabIndex = 0
        '
        'btn_generar_recibo
        '
        Me.btn_generar_recibo.Animated = True
        Me.btn_generar_recibo.AnimationHoverSpeed = 0.07!
        Me.btn_generar_recibo.AnimationSpeed = 0.03!
        Me.btn_generar_recibo.BackColor = System.Drawing.Color.Transparent
        Me.btn_generar_recibo.BaseColor = System.Drawing.SystemColors.ActiveCaption
        Me.btn_generar_recibo.BorderColor = System.Drawing.Color.Black
        Me.btn_generar_recibo.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_generar_recibo.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_generar_recibo.CheckedForeColor = System.Drawing.Color.White
        Me.btn_generar_recibo.CheckedImage = CType(resources.GetObject("btn_generar_recibo.CheckedImage"), System.Drawing.Image)
        Me.btn_generar_recibo.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_generar_recibo.FocusedColor = System.Drawing.Color.Empty
        Me.btn_generar_recibo.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_generar_recibo.ForeColor = System.Drawing.Color.White
        Me.btn_generar_recibo.Image = CType(resources.GetObject("btn_generar_recibo.Image"), System.Drawing.Image)
        Me.btn_generar_recibo.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_generar_recibo.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_generar_recibo.Location = New System.Drawing.Point(145, 87)
        Me.btn_generar_recibo.Name = "btn_generar_recibo"
        Me.btn_generar_recibo.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_generar_recibo.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_generar_recibo.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_generar_recibo.OnHoverImage = CType(resources.GetObject("btn_generar_recibo.OnHoverImage"), System.Drawing.Image)
        Me.btn_generar_recibo.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_generar_recibo.OnPressedColor = System.Drawing.Color.Black
        Me.btn_generar_recibo.Radius = 12
        Me.btn_generar_recibo.Size = New System.Drawing.Size(143, 32)
        Me.btn_generar_recibo.TabIndex = 6
        Me.btn_generar_recibo.Text = "GENERAR RECIBO"
        '
        'btn_ver
        '
        Me.btn_ver.Animated = True
        Me.btn_ver.AnimationHoverSpeed = 0.07!
        Me.btn_ver.AnimationSpeed = 0.03!
        Me.btn_ver.BackColor = System.Drawing.Color.Transparent
        Me.btn_ver.BaseColor = System.Drawing.SystemColors.ActiveCaption
        Me.btn_ver.BorderColor = System.Drawing.Color.Black
        Me.btn_ver.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_ver.CheckedForeColor = System.Drawing.Color.White
        Me.btn_ver.CheckedImage = CType(resources.GetObject("btn_ver.CheckedImage"), System.Drawing.Image)
        Me.btn_ver.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_ver.FocusedColor = System.Drawing.Color.Empty
        Me.btn_ver.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_ver.ForeColor = System.Drawing.Color.White
        Me.btn_ver.Image = CType(resources.GetObject("btn_ver.Image"), System.Drawing.Image)
        Me.btn_ver.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_ver.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_ver.Location = New System.Drawing.Point(16, 87)
        Me.btn_ver.Name = "btn_ver"
        Me.btn_ver.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_ver.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_ver.OnHoverImage = CType(resources.GetObject("btn_ver.OnHoverImage"), System.Drawing.Image)
        Me.btn_ver.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.OnPressedColor = System.Drawing.Color.Black
        Me.btn_ver.Radius = 12
        Me.btn_ver.Size = New System.Drawing.Size(123, 32)
        Me.btn_ver.TabIndex = 5
        Me.btn_ver.Text = "VER DETALLES"
        '
        'bdgv_debito
        '
        Me.bdgv_debito.AllowUserToAddRows = False
        Me.bdgv_debito.AllowUserToDeleteRows = False
        Me.bdgv_debito.AllowUserToResizeColumns = False
        Me.bdgv_debito.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.bdgv_debito.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.bdgv_debito.AutoGenerateColumns = False
        Me.bdgv_debito.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.bdgv_debito.BackgroundColor = System.Drawing.Color.White
        Me.bdgv_debito.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.bdgv_debito.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.bdgv_debito.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bdgv_debito.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.bdgv_debito.ColumnHeadersHeight = 40
        Me.bdgv_debito.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MatriculaDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.DniDataGridViewTextBoxColumn, Me.JubilacionDataGridViewTextBoxColumn, Me.CapitalizacionDataGridViewTextBoxColumn})
        Me.bdgv_debito.DataSource = Me.JubiladosBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.bdgv_debito.DefaultCellStyle = DataGridViewCellStyle3
        Me.bdgv_debito.DoubleBuffered = True
        Me.bdgv_debito.EnableHeadersVisualStyles = False
        Me.bdgv_debito.HeaderBgColor = System.Drawing.SystemColors.ActiveCaption
        Me.bdgv_debito.HeaderForeColor = System.Drawing.Color.White
        Me.bdgv_debito.Location = New System.Drawing.Point(16, 125)
        Me.bdgv_debito.Name = "bdgv_debito"
        Me.bdgv_debito.ReadOnly = True
        Me.bdgv_debito.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.bdgv_debito.RowHeadersVisible = False
        Me.bdgv_debito.RowHeadersWidth = 40
        Me.bdgv_debito.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgv_debito.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.DimGray
        Me.bdgv_debito.RowTemplate.DefaultCellStyle.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.bdgv_debito.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Gainsboro
        Me.bdgv_debito.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
        Me.bdgv_debito.RowTemplate.Height = 40
        Me.bdgv_debito.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.bdgv_debito.Size = New System.Drawing.Size(708, 357)
        Me.bdgv_debito.TabIndex = 4
        '
        'MatriculaDataGridViewTextBoxColumn
        '
        Me.MatriculaDataGridViewTextBoxColumn.DataPropertyName = "matricula"
        Me.MatriculaDataGridViewTextBoxColumn.FillWeight = 20.0!
        Me.MatriculaDataGridViewTextBoxColumn.HeaderText = "MATRICULA"
        Me.MatriculaDataGridViewTextBoxColumn.Name = "MatriculaDataGridViewTextBoxColumn"
        Me.MatriculaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.FillWeight = 40.0!
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "NOMBRE"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DniDataGridViewTextBoxColumn
        '
        Me.DniDataGridViewTextBoxColumn.DataPropertyName = "dni"
        Me.DniDataGridViewTextBoxColumn.FillWeight = 20.0!
        Me.DniDataGridViewTextBoxColumn.HeaderText = "DNI"
        Me.DniDataGridViewTextBoxColumn.Name = "DniDataGridViewTextBoxColumn"
        Me.DniDataGridViewTextBoxColumn.ReadOnly = True
        '
        'JubilacionDataGridViewTextBoxColumn
        '
        Me.JubilacionDataGridViewTextBoxColumn.DataPropertyName = "jubilacion"
        Me.JubilacionDataGridViewTextBoxColumn.FillWeight = 30.0!
        Me.JubilacionDataGridViewTextBoxColumn.HeaderText = "JUBILACION"
        Me.JubilacionDataGridViewTextBoxColumn.Name = "JubilacionDataGridViewTextBoxColumn"
        Me.JubilacionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CapitalizacionDataGridViewTextBoxColumn
        '
        Me.CapitalizacionDataGridViewTextBoxColumn.DataPropertyName = "capitalizacion"
        Me.CapitalizacionDataGridViewTextBoxColumn.FillWeight = 30.0!
        Me.CapitalizacionDataGridViewTextBoxColumn.HeaderText = "CAPITALIZACION"
        Me.CapitalizacionDataGridViewTextBoxColumn.Name = "CapitalizacionDataGridViewTextBoxColumn"
        Me.CapitalizacionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'JubiladosBindingSource
        '
        Me.JubiladosBindingSource.DataMember = "jubilados"
        Me.JubiladosBindingSource.DataSource = Me.DSSipres
        '
        'DSSipres
        '
        Me.DSSipres.DataSetName = "DSSipres"
        Me.DSSipres.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lb_matricula
        '
        Me.lb_matricula.AutoSize = True
        Me.lb_matricula.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!)
        Me.lb_matricula.ForeColor = System.Drawing.Color.DimGray
        Me.lb_matricula.Location = New System.Drawing.Point(12, 20)
        Me.lb_matricula.Name = "lb_matricula"
        Me.lb_matricula.Size = New System.Drawing.Size(276, 21)
        Me.lb_matricula.TabIndex = 3
        Me.lb_matricula.Text = "DEBITOS AUTOMATICOS JUBILADOS"
        '
        'UCDebitos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Controls.Add(Me.GunaShadowPanel1)
        Me.Name = "UCDebitos"
        Me.Size = New System.Drawing.Size(790, 564)
        Me.GunaShadowPanel1.ResumeLayout(False)
        Me.GunaShadowPanel1.PerformLayout()
        CType(Me.bdgv_debito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JubiladosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GunaShadowPanel1 As Guna.UI.WinForms.GunaShadowPanel
    Friend WithEvents lb_matricula As Guna.UI.WinForms.GunaLabel
    Friend WithEvents bdgv_debito As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents btn_ver As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_generar_recibo As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents JubiladosBindingSource As BindingSource
    Friend WithEvents DSSipres As DSSipres
    Friend WithEvents MatriculaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DniDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents JubilacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CapitalizacionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
