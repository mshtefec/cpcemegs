﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UCListados
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UCListados))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GunaLinePanel1 = New Guna.UI.WinForms.GunaLinePanel()
        Me.GunaShadowPanel1 = New Guna.UI.WinForms.GunaShadowPanel()
        Me.btn_filtro_clear = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_filtro_mtr = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_filtro_nom = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_filtro_dni = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.btn_filtro_cat = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.tbox_filtro = New Guna.UI.WinForms.GunaLineTextBox()
        Me.btn_ver = New Guna.UI.WinForms.GunaAdvenceButton()
        Me.bdgv_listado = New Bunifu.Framework.UI.BunifuCustomDataGrid()
        Me.lb_matricula = New Guna.UI.WinForms.GunaLabel()
        Me.DSSipres = New cpceMEGS.DSSipres()
        Me.ListadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MatriculaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CategoriaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GunaLinePanel1.SuspendLayout()
        Me.GunaShadowPanel1.SuspendLayout()
        CType(Me.bdgv_listado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ListadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GunaLinePanel1
        '
        Me.GunaLinePanel1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.GunaLinePanel1.Controls.Add(Me.GunaShadowPanel1)
        Me.GunaLinePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GunaLinePanel1.LineColor = System.Drawing.Color.Black
        Me.GunaLinePanel1.LineStyle = System.Windows.Forms.BorderStyle.None
        Me.GunaLinePanel1.Location = New System.Drawing.Point(0, 0)
        Me.GunaLinePanel1.Name = "GunaLinePanel1"
        Me.GunaLinePanel1.Size = New System.Drawing.Size(790, 564)
        Me.GunaLinePanel1.TabIndex = 0
        '
        'GunaShadowPanel1
        '
        Me.GunaShadowPanel1.BackColor = System.Drawing.Color.Transparent
        Me.GunaShadowPanel1.BaseColor = System.Drawing.Color.White
        Me.GunaShadowPanel1.Controls.Add(Me.btn_filtro_clear)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_filtro_mtr)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_filtro_nom)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_filtro_dni)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_filtro_cat)
        Me.GunaShadowPanel1.Controls.Add(Me.tbox_filtro)
        Me.GunaShadowPanel1.Controls.Add(Me.btn_ver)
        Me.GunaShadowPanel1.Controls.Add(Me.bdgv_listado)
        Me.GunaShadowPanel1.Controls.Add(Me.lb_matricula)
        Me.GunaShadowPanel1.Location = New System.Drawing.Point(21, 23)
        Me.GunaShadowPanel1.Name = "GunaShadowPanel1"
        Me.GunaShadowPanel1.ShadowColor = System.Drawing.Color.Black
        Me.GunaShadowPanel1.ShadowDepth = 180
        Me.GunaShadowPanel1.ShadowShift = 8
        Me.GunaShadowPanel1.ShadowStyle = Guna.UI.WinForms.ShadowMode.ForwardDiagonal
        Me.GunaShadowPanel1.Size = New System.Drawing.Size(746, 508)
        Me.GunaShadowPanel1.TabIndex = 3
        '
        'btn_filtro_clear
        '
        Me.btn_filtro_clear.Animated = True
        Me.btn_filtro_clear.AnimationHoverSpeed = 0.07!
        Me.btn_filtro_clear.AnimationSpeed = 0.03!
        Me.btn_filtro_clear.BackColor = System.Drawing.Color.Transparent
        Me.btn_filtro_clear.BaseColor = System.Drawing.SystemColors.ActiveCaption
        Me.btn_filtro_clear.BorderColor = System.Drawing.Color.Black
        Me.btn_filtro_clear.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_clear.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_clear.CheckedForeColor = System.Drawing.Color.White
        Me.btn_filtro_clear.CheckedImage = CType(resources.GetObject("btn_filtro_clear.CheckedImage"), System.Drawing.Image)
        Me.btn_filtro_clear.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_filtro_clear.FocusedColor = System.Drawing.Color.Empty
        Me.btn_filtro_clear.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_filtro_clear.ForeColor = System.Drawing.Color.White
        Me.btn_filtro_clear.Image = CType(resources.GetObject("btn_filtro_clear.Image"), System.Drawing.Image)
        Me.btn_filtro_clear.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_filtro_clear.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_filtro_clear.Location = New System.Drawing.Point(414, 87)
        Me.btn_filtro_clear.Name = "btn_filtro_clear"
        Me.btn_filtro_clear.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_clear.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_clear.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_filtro_clear.OnHoverImage = CType(resources.GetObject("btn_filtro_clear.OnHoverImage"), System.Drawing.Image)
        Me.btn_filtro_clear.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_clear.OnPressedColor = System.Drawing.Color.Black
        Me.btn_filtro_clear.Radius = 12
        Me.btn_filtro_clear.Size = New System.Drawing.Size(92, 32)
        Me.btn_filtro_clear.TabIndex = 12
        Me.btn_filtro_clear.Text = "LIMPIAR"
        '
        'btn_filtro_mtr
        '
        Me.btn_filtro_mtr.Animated = True
        Me.btn_filtro_mtr.AnimationHoverSpeed = 0.07!
        Me.btn_filtro_mtr.AnimationSpeed = 0.03!
        Me.btn_filtro_mtr.BackColor = System.Drawing.Color.Transparent
        Me.btn_filtro_mtr.BaseColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn_filtro_mtr.BorderColor = System.Drawing.Color.Black
        Me.btn_filtro_mtr.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_filtro_mtr.Checked = True
        Me.btn_filtro_mtr.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_mtr.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_mtr.CheckedForeColor = System.Drawing.Color.White
        Me.btn_filtro_mtr.CheckedImage = Nothing
        Me.btn_filtro_mtr.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_filtro_mtr.FocusedColor = System.Drawing.Color.Empty
        Me.btn_filtro_mtr.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_filtro_mtr.ForeColor = System.Drawing.Color.White
        Me.btn_filtro_mtr.Image = Nothing
        Me.btn_filtro_mtr.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_filtro_mtr.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_filtro_mtr.Location = New System.Drawing.Point(480, 60)
        Me.btn_filtro_mtr.Name = "btn_filtro_mtr"
        Me.btn_filtro_mtr.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_mtr.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_mtr.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_filtro_mtr.OnHoverImage = Nothing
        Me.btn_filtro_mtr.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_mtr.OnPressedColor = System.Drawing.Color.Black
        Me.btn_filtro_mtr.Radius = 12
        Me.btn_filtro_mtr.Size = New System.Drawing.Size(67, 25)
        Me.btn_filtro_mtr.TabIndex = 11
        Me.btn_filtro_mtr.Text = "matricula"
        '
        'btn_filtro_nom
        '
        Me.btn_filtro_nom.Animated = True
        Me.btn_filtro_nom.AnimationHoverSpeed = 0.07!
        Me.btn_filtro_nom.AnimationSpeed = 0.03!
        Me.btn_filtro_nom.BackColor = System.Drawing.Color.Transparent
        Me.btn_filtro_nom.BaseColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn_filtro_nom.BorderColor = System.Drawing.Color.Black
        Me.btn_filtro_nom.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_filtro_nom.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_nom.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_nom.CheckedForeColor = System.Drawing.Color.White
        Me.btn_filtro_nom.CheckedImage = Nothing
        Me.btn_filtro_nom.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_filtro_nom.FocusedColor = System.Drawing.Color.Empty
        Me.btn_filtro_nom.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_filtro_nom.ForeColor = System.Drawing.Color.White
        Me.btn_filtro_nom.Image = Nothing
        Me.btn_filtro_nom.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_filtro_nom.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_filtro_nom.Location = New System.Drawing.Point(553, 60)
        Me.btn_filtro_nom.Name = "btn_filtro_nom"
        Me.btn_filtro_nom.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_nom.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_nom.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_filtro_nom.OnHoverImage = Nothing
        Me.btn_filtro_nom.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_nom.OnPressedColor = System.Drawing.Color.Black
        Me.btn_filtro_nom.Radius = 12
        Me.btn_filtro_nom.Size = New System.Drawing.Size(58, 25)
        Me.btn_filtro_nom.TabIndex = 10
        Me.btn_filtro_nom.Text = "nombre"
        '
        'btn_filtro_dni
        '
        Me.btn_filtro_dni.Animated = True
        Me.btn_filtro_dni.AnimationHoverSpeed = 0.07!
        Me.btn_filtro_dni.AnimationSpeed = 0.03!
        Me.btn_filtro_dni.BackColor = System.Drawing.Color.Transparent
        Me.btn_filtro_dni.BaseColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn_filtro_dni.BorderColor = System.Drawing.Color.Black
        Me.btn_filtro_dni.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_filtro_dni.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_dni.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_dni.CheckedForeColor = System.Drawing.Color.White
        Me.btn_filtro_dni.CheckedImage = Nothing
        Me.btn_filtro_dni.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_filtro_dni.FocusedColor = System.Drawing.Color.Empty
        Me.btn_filtro_dni.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_filtro_dni.ForeColor = System.Drawing.Color.White
        Me.btn_filtro_dni.Image = Nothing
        Me.btn_filtro_dni.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_filtro_dni.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_filtro_dni.Location = New System.Drawing.Point(617, 60)
        Me.btn_filtro_dni.Name = "btn_filtro_dni"
        Me.btn_filtro_dni.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_dni.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_dni.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_filtro_dni.OnHoverImage = Nothing
        Me.btn_filtro_dni.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_dni.OnPressedColor = System.Drawing.Color.Black
        Me.btn_filtro_dni.Radius = 12
        Me.btn_filtro_dni.Size = New System.Drawing.Size(35, 25)
        Me.btn_filtro_dni.TabIndex = 9
        Me.btn_filtro_dni.Text = "dni"
        '
        'btn_filtro_cat
        '
        Me.btn_filtro_cat.Animated = True
        Me.btn_filtro_cat.AnimationHoverSpeed = 0.07!
        Me.btn_filtro_cat.AnimationSpeed = 0.03!
        Me.btn_filtro_cat.BackColor = System.Drawing.Color.Transparent
        Me.btn_filtro_cat.BaseColor = System.Drawing.SystemColors.AppWorkspace
        Me.btn_filtro_cat.BorderColor = System.Drawing.Color.Black
        Me.btn_filtro_cat.ButtonType = Guna.UI.WinForms.AdvenceButtonType.RadioButton
        Me.btn_filtro_cat.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_cat.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_cat.CheckedForeColor = System.Drawing.Color.White
        Me.btn_filtro_cat.CheckedImage = Nothing
        Me.btn_filtro_cat.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_filtro_cat.FocusedColor = System.Drawing.Color.Empty
        Me.btn_filtro_cat.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_filtro_cat.ForeColor = System.Drawing.Color.White
        Me.btn_filtro_cat.Image = Nothing
        Me.btn_filtro_cat.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_filtro_cat.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_filtro_cat.Location = New System.Drawing.Point(658, 60)
        Me.btn_filtro_cat.Name = "btn_filtro_cat"
        Me.btn_filtro_cat.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_cat.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_filtro_cat.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_filtro_cat.OnHoverImage = Nothing
        Me.btn_filtro_cat.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_filtro_cat.OnPressedColor = System.Drawing.Color.Black
        Me.btn_filtro_cat.Radius = 12
        Me.btn_filtro_cat.Size = New System.Drawing.Size(66, 25)
        Me.btn_filtro_cat.TabIndex = 8
        Me.btn_filtro_cat.Text = "categoria"
        '
        'tbox_filtro
        '
        Me.tbox_filtro.BackColor = System.Drawing.Color.White
        Me.tbox_filtro.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.tbox_filtro.FocusedLineColor = System.Drawing.SystemColors.ActiveCaption
        Me.tbox_filtro.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.tbox_filtro.ForeColor = System.Drawing.Color.DimGray
        Me.tbox_filtro.LineColor = System.Drawing.Color.Gainsboro
        Me.tbox_filtro.Location = New System.Drawing.Point(512, 91)
        Me.tbox_filtro.Name = "tbox_filtro"
        Me.tbox_filtro.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.tbox_filtro.Size = New System.Drawing.Size(212, 28)
        Me.tbox_filtro.TabIndex = 4
        Me.tbox_filtro.Text = "Buscar por"
        '
        'btn_ver
        '
        Me.btn_ver.Animated = True
        Me.btn_ver.AnimationHoverSpeed = 0.07!
        Me.btn_ver.AnimationSpeed = 0.03!
        Me.btn_ver.BackColor = System.Drawing.Color.Transparent
        Me.btn_ver.BaseColor = System.Drawing.SystemColors.ActiveCaption
        Me.btn_ver.BorderColor = System.Drawing.Color.Black
        Me.btn_ver.CheckedBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.CheckedBorderColor = System.Drawing.Color.Black
        Me.btn_ver.CheckedForeColor = System.Drawing.Color.White
        Me.btn_ver.CheckedImage = CType(resources.GetObject("btn_ver.CheckedImage"), System.Drawing.Image)
        Me.btn_ver.CheckedLineColor = System.Drawing.Color.DimGray
        Me.btn_ver.FocusedColor = System.Drawing.Color.Empty
        Me.btn_ver.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!)
        Me.btn_ver.ForeColor = System.Drawing.Color.White
        Me.btn_ver.Image = CType(resources.GetObject("btn_ver.Image"), System.Drawing.Image)
        Me.btn_ver.ImageSize = New System.Drawing.Size(16, 16)
        Me.btn_ver.LineColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.btn_ver.Location = New System.Drawing.Point(16, 87)
        Me.btn_ver.Name = "btn_ver"
        Me.btn_ver.OnHoverBaseColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.OnHoverBorderColor = System.Drawing.Color.Black
        Me.btn_ver.OnHoverForeColor = System.Drawing.Color.White
        Me.btn_ver.OnHoverImage = CType(resources.GetObject("btn_ver.OnHoverImage"), System.Drawing.Image)
        Me.btn_ver.OnHoverLineColor = System.Drawing.SystemColors.Highlight
        Me.btn_ver.OnPressedColor = System.Drawing.Color.Black
        Me.btn_ver.Radius = 12
        Me.btn_ver.Size = New System.Drawing.Size(123, 32)
        Me.btn_ver.TabIndex = 3
        Me.btn_ver.Text = "VER DETALLES"
        '
        'bdgv_listado
        '
        Me.bdgv_listado.AllowUserToAddRows = False
        Me.bdgv_listado.AllowUserToDeleteRows = False
        Me.bdgv_listado.AllowUserToResizeColumns = False
        Me.bdgv_listado.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.bdgv_listado.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.bdgv_listado.AutoGenerateColumns = False
        Me.bdgv_listado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.bdgv_listado.BackgroundColor = System.Drawing.Color.White
        Me.bdgv_listado.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.bdgv_listado.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.bdgv_listado.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bdgv_listado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.bdgv_listado.ColumnHeadersHeight = 40
        Me.bdgv_listado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MatriculaDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.DniDataGridViewTextBoxColumn, Me.CategoriaDataGridViewTextBoxColumn})
        Me.bdgv_listado.DataSource = Me.ListadoBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.bdgv_listado.DefaultCellStyle = DataGridViewCellStyle3
        Me.bdgv_listado.DoubleBuffered = True
        Me.bdgv_listado.EnableHeadersVisualStyles = False
        Me.bdgv_listado.HeaderBgColor = System.Drawing.SystemColors.ActiveCaption
        Me.bdgv_listado.HeaderForeColor = System.Drawing.Color.White
        Me.bdgv_listado.Location = New System.Drawing.Point(16, 125)
        Me.bdgv_listado.Name = "bdgv_listado"
        Me.bdgv_listado.ReadOnly = True
        Me.bdgv_listado.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.bdgv_listado.RowHeadersVisible = False
        Me.bdgv_listado.RowHeadersWidth = 40
        Me.bdgv_listado.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgv_listado.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.DimGray
        Me.bdgv_listado.RowTemplate.DefaultCellStyle.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.bdgv_listado.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Gainsboro
        Me.bdgv_listado.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
        Me.bdgv_listado.RowTemplate.Height = 40
        Me.bdgv_listado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.bdgv_listado.Size = New System.Drawing.Size(708, 357)
        Me.bdgv_listado.TabIndex = 0
        '
        'lb_matricula
        '
        Me.lb_matricula.AutoSize = True
        Me.lb_matricula.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!)
        Me.lb_matricula.ForeColor = System.Drawing.Color.DimGray
        Me.lb_matricula.Location = New System.Drawing.Point(12, 20)
        Me.lb_matricula.Name = "lb_matricula"
        Me.lb_matricula.Size = New System.Drawing.Size(222, 21)
        Me.lb_matricula.TabIndex = 2
        Me.lb_matricula.Text = "LISTADO DE MATRICULADOS"
        '
        'DSSipres
        '
        Me.DSSipres.DataSetName = "DSSipres"
        Me.DSSipres.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListadoBindingSource
        '
        Me.ListadoBindingSource.DataMember = "listado"
        Me.ListadoBindingSource.DataSource = Me.DSSipres
        '
        'MatriculaDataGridViewTextBoxColumn
        '
        Me.MatriculaDataGridViewTextBoxColumn.DataPropertyName = "matricula"
        Me.MatriculaDataGridViewTextBoxColumn.FillWeight = 20.0!
        Me.MatriculaDataGridViewTextBoxColumn.HeaderText = "MATRICULA"
        Me.MatriculaDataGridViewTextBoxColumn.Name = "MatriculaDataGridViewTextBoxColumn"
        Me.MatriculaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "nombre"
        Me.NombreDataGridViewTextBoxColumn.FillWeight = 40.0!
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "NOMBRE"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DniDataGridViewTextBoxColumn
        '
        Me.DniDataGridViewTextBoxColumn.DataPropertyName = "dni"
        Me.DniDataGridViewTextBoxColumn.FillWeight = 20.0!
        Me.DniDataGridViewTextBoxColumn.HeaderText = "DNI"
        Me.DniDataGridViewTextBoxColumn.Name = "DniDataGridViewTextBoxColumn"
        Me.DniDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CategoriaDataGridViewTextBoxColumn
        '
        Me.CategoriaDataGridViewTextBoxColumn.DataPropertyName = "categoria"
        Me.CategoriaDataGridViewTextBoxColumn.FillWeight = 40.0!
        Me.CategoriaDataGridViewTextBoxColumn.HeaderText = "CATEGORIA"
        Me.CategoriaDataGridViewTextBoxColumn.Name = "CategoriaDataGridViewTextBoxColumn"
        Me.CategoriaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UCListados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GunaLinePanel1)
        Me.Name = "UCListados"
        Me.Size = New System.Drawing.Size(790, 564)
        Me.GunaLinePanel1.ResumeLayout(False)
        Me.GunaShadowPanel1.ResumeLayout(False)
        Me.GunaShadowPanel1.PerformLayout()
        CType(Me.bdgv_listado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSSipres, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ListadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GunaLinePanel1 As Guna.UI.WinForms.GunaLinePanel
    Friend WithEvents lb_matricula As Guna.UI.WinForms.GunaLabel
    Friend WithEvents GunaShadowPanel1 As Guna.UI.WinForms.GunaShadowPanel
    Friend WithEvents bdgv_listado As Bunifu.Framework.UI.BunifuCustomDataGrid
    Friend WithEvents btn_ver As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents tbox_filtro As Guna.UI.WinForms.GunaLineTextBox
    Friend WithEvents btn_filtro_mtr As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_filtro_nom As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_filtro_dni As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_filtro_cat As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents btn_filtro_clear As Guna.UI.WinForms.GunaAdvenceButton
    Friend WithEvents DSSipres As DSSipres
    Friend WithEvents ListadoBindingSource As BindingSource
    Friend WithEvents MatriculaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DniDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CategoriaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
