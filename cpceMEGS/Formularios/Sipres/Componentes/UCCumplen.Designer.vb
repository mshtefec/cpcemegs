﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCCumplen
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GunaLinePanel1 = New Guna.UI.WinForms.GunaLinePanel()
        Me.lb_matricula = New Guna.UI.WinForms.GunaLabel()
        Me.GunaLinePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GunaLinePanel1
        '
        Me.GunaLinePanel1.Controls.Add(Me.lb_matricula)
        Me.GunaLinePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GunaLinePanel1.LineColor = System.Drawing.Color.Black
        Me.GunaLinePanel1.LineStyle = System.Windows.Forms.BorderStyle.None
        Me.GunaLinePanel1.Location = New System.Drawing.Point(0, 0)
        Me.GunaLinePanel1.Name = "GunaLinePanel1"
        Me.GunaLinePanel1.Size = New System.Drawing.Size(790, 564)
        Me.GunaLinePanel1.TabIndex = 0
        '
        'lb_matricula
        '
        Me.lb_matricula.AutoSize = True
        Me.lb_matricula.Font = New System.Drawing.Font("Segoe UI Semibold", 12.0!)
        Me.lb_matricula.ForeColor = System.Drawing.Color.DimGray
        Me.lb_matricula.Location = New System.Drawing.Point(31, 19)
        Me.lb_matricula.Name = "lb_matricula"
        Me.lb_matricula.Size = New System.Drawing.Size(168, 21)
        Me.lb_matricula.TabIndex = 2
        Me.lb_matricula.Text = "CUMPLEN AÑOS HOY"
        '
        'UCCumplen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GunaLinePanel1)
        Me.Name = "UCCumplen"
        Me.Size = New System.Drawing.Size(790, 564)
        Me.GunaLinePanel1.ResumeLayout(False)
        Me.GunaLinePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GunaLinePanel1 As Guna.UI.WinForms.GunaLinePanel
    Friend WithEvents lb_matricula As Guna.UI.WinForms.GunaLabel
End Class
