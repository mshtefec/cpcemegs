﻿Imports System.ComponentModel
Imports MySql.Data.MySqlClient

Public Class UCListados

    'BD
    Private consultaBD As New ConsultaBD(True)
    Private myda As MySqlDataAdapter

    Private Enum Selection
        matricula
        nombre
        dni
        categoria
    End Enum

    Dim seleccion As String = [Enum].GetName(GetType(Selection), 0) 'valor por defecto, matricula

    Private Sub UCListados_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            consultaBD.AbrirConexion()

            myda = consultaBD.consultaBDadapter(
                "afiliado as afi, categorias as cat",
                "CONCAT(afi.afi_titulo, afi.afi_matricula) AS MATRICULA, afi.afi_nombre AS NOMBRE, afi.afi_nrodoc AS DNI, cat.cat_descrip AS CATEGORIA",
                "MID(afi.afi_categoria,1,1)='1' AND afi.afi_titulo <> 'EC' AND afi.afi_titulo <> 'DE' AND afi.afi_tipo = 'A' AND afi.afi_categoria = cat.cat_codigo 
                 ORDER BY nombre"
            )

            myda.Fill(DSSipres, "listado")

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub

    Private Sub btn_filtro_mtr_Click(sender As Object, e As EventArgs) Handles btn_filtro_mtr.Click
        seleccion = [Enum].GetName(GetType(Selection), 0)
    End Sub

    Private Sub btn_filtro_nom_Click(sender As Object, e As EventArgs) Handles btn_filtro_nom.Click
        seleccion = [Enum].GetName(GetType(Selection), 1)
    End Sub

    Private Sub btn_filtro_dni_Click(sender As Object, e As EventArgs) Handles btn_filtro_dni.Click
        seleccion = [Enum].GetName(GetType(Selection), 2)
    End Sub

    Private Sub btn_filtro_cat_Click(sender As Object, e As EventArgs) Handles btn_filtro_cat.Click
        seleccion = [Enum].GetName(GetType(Selection), 3)
    End Sub

    Private Sub tbox_filtro_TextChanged(sender As Object, e As EventArgs) Handles tbox_filtro.TextChanged
        Dim custDV As DataView = DSSipres.Tables("listado").DefaultView
        custDV.RowFilter = String.Format("{0} like '%{1}%'", seleccion, tbox_filtro.Text)
        bdgv_listado.DataSource = custDV
    End Sub

    Private Sub btn_filtro_clear_Click(sender As Object, e As EventArgs) Handles btn_filtro_clear.Click
        Dim custDV As DataView = DSSipres.Tables("listado").DefaultView
        custDV.RowFilter = ""
        tbox_filtro.Text = ""
        bdgv_listado.DataSource = custDV
    End Sub
End Class
