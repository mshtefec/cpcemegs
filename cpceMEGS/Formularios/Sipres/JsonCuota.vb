﻿Imports Newtonsoft.Json
Public Class JsonCuota

	<JsonProperty("servicio")>
	Public Property servicio As String

	<JsonProperty("num_dni")>
	Public Property num_dni As Integer

	<JsonProperty("denominac")>
	Public Property denominac As String

	<JsonProperty("imp_comp")>
	Public Property imp_comp As Double

	<JsonProperty("matricula")>
	Public Property matricula As Integer

	<JsonProperty("description1")>
	Public Property description1 As String

	<JsonProperty("description2")>
	Public Property description2 As String

	<JsonProperty("description3")>
	Public Property description3 As String

	<JsonProperty("proceso")>
	Public Property proceso As String

	<JsonProperty("cuota")>
	Public Property cuota As Integer

	<JsonProperty("total")>
	Public Property total As Double

	<JsonProperty("cuentas")>
	Public Property cuentas() As New Dictionary(Of Integer, String)

End Class
