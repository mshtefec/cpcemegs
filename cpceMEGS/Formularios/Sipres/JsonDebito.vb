﻿Imports Newtonsoft.Json
Public Class JsonDebito

	<JsonProperty("tipo")>
	Public Property tipo As String

	<JsonProperty("servicio")>
	Public Property servicio As String

	<JsonProperty("operador")>
	Public Property operador As Integer

	<JsonProperty("caja")>
	Public Property caja As Integer

	<JsonProperty("zeta")>
	Public Property zeta As Integer

	<JsonProperty("fecha_comp")>
	Public Property fecha_comp As String

	<JsonProperty("cuotas")>
	Public Property cuotas() As New Dictionary(Of Integer, JsonCuota)

End Class
