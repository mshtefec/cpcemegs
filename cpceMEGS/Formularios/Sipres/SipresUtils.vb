﻿Imports MySql.Data.MySqlClient
Imports System.Data.OleDb

Public Class SipresUtils
	'BD
	Private consultaBD						As New ConsultaBD(True)
	Private myda							As MySqlDataAdapter
	'OLE BD
	Private Const provider					As String	 = "Provider=Microsoft.ACE.OLEDB.12.0;"
	Private Const properties				As String	 = "Extended Properties='Excel 12.0 Xml;HDR=Yes'"
	Private oleDb_conn						As New OleDbConnection()
	'GLOBALES
	Private dt_ley_sipres					As DataTable = New DataTable()
    'CONSTANTES
    Private Const resolucion_sipres As String = "23_2014"  'Resolucion Sipres #23 del 2014 vigente a 2018
    Private Const cuenta_capitalizacion		As Integer	 = 22010100       'cuenta aportes capitalizacion individual
	Private Const periodos					As Integer   = 144            'Son 144 periodos (12 años) que es el tiempo de retiro para ciclo completo


	'************************* JUBILACIONES *************************
	'****************************************************************

	'Calcula el aporte de capitalización individual según lo que el matriculado ha aportado durante los años de actividad.
	''' <summary>
	''' <para>Agrega una columna (capitalizacion) con el monto según lo que el matriculado ha aportado durante los años de actividad. True si es jubilado y utiliza la fecha de jubilacion para calcular el aporte hasta esa fecha.</para>
	''' <para>["si no tiene aportes elimina al matriculado de la lista"].</para>
	''' </summary>
	Public Sub CargaAporteCapitalizacion(ByVal dt_model As DataTable, Optional ByVal isJubilado As Boolean = False)

        Dim capitalizacion As Decimal = 0
        Dim capitalizacionJubilado As Decimal = 0
        Dim fechaNow As Date = Now
        Dim fechaJubilado As Date

        If isJubilado Then
            dt_model.Columns.Add("capitalizacionJubilado", GetType(Decimal))
            dt_model.Columns("capitalizacionJubilado").ReadOnly = False
        End If

        dt_model.Columns.Add("Capitalizacion", GetType(Decimal))
        dt_model.Columns("Capitalizacion").ReadOnly = False

        Try
			consultaBD.AbrirConexion()

			For Each row As DataRow In dt_model.Rows

				If row.RowState = DataRowState.Deleted Then
					Continue For
				End If

				'Si es jubilado seteo la fecha busco aportes por la fecha jubilación.
				If isJubilado Then
                    If row.Item("FechadeJubilacion").ToString() <> "00/00/0000" Then
                        fechaJubilado = Format(Convert.ToDateTime(row.Item("FechadeJubilacion")), "yyyy-MM-dd 23:59:59")
                    Else
                        fechaJubilado = fechaNow
                    End If
				End If

                'Si matricula se cargo algo en cuenta 22010100
                Try
                    capitalizacion = consultaBD.GetSaldo(row.Item("Dni"), cuenta_capitalizacion, fechaNow)
                    If isJubilado Then
                        capitalizacionJubilado = consultaBD.GetSaldo(row.Item("Dni"), cuenta_capitalizacion, fechaJubilado)
                    End If

                    'Si tiene aportes pongo el valor en positivo, o borro
                    If capitalizacion = 0 Then
                        row.Delete()
                    Else
                        If isJubilado Then
                            If fechaJubilado <> fechaNow Then
                                row.Item("capitalizacionJubilado") = capitalizacionJubilado * -1
                            Else
                                row.Item("capitalizacionJubilado") = 0
                            End If
                        End If
                        row.Item("Capitalizacion") = capitalizacion * -1
                    End If
                    'Si matricula no tiene asociado cuenta 22010100 borro fila
                Catch ex As Exception
                    row.Delete()
                End Try
            Next
		Catch
			MsgBox("No se puede realizar el calculo de capitalización individual en uno o mas matriculados.", MsgBoxStyle.Critical, "Error")
		Finally
			consultaBD.CerrarConexion()
		End Try
	End Sub

	'Determina los valores de aportes de capitalización según resolucion de sipres.
	''' <summary>
	''' <para>Agrega columnas (Periodos, Ley, Resolucion y montos mensuales de c/u) con el monto según el calculo en base a la resolucion de sipres.</para>
	''' </summary>
	Public Sub DeterminaCuotas(ByVal dt_model As DataTable)

		dt_model.Columns.Add("Periodos", GetType(String))
		dt_model.Columns("Periodos").ReadOnly = False
        dt_model.Columns.Add("PerMensual", GetType(Decimal))
        dt_model.Columns("PerMensual").ReadOnly = False
		'jubilados.DataSource.Columns("PerMensual").Text = "Mensual"
		dt_model.Columns.Add("Ley", GetType(String))
		dt_model.Columns("Ley").ReadOnly = False
        dt_model.Columns.Add("LeyMensual", GetType(Decimal))
        dt_model.Columns("LeyMensual").ReadOnly = False
		'jubilados.DataSource.Columns("LeyMensual").Text = "Mensual"
		dt_model.Columns.Add("Resolucion", GetType(String))
		dt_model.Columns("Resolucion").ReadOnly = False
        dt_model.Columns.Add("ResMensual", GetType(Decimal))
        dt_model.Columns("ResMensual").ReadOnly = False
		'jubilados.DataSource.Columns("ResMensual").Text = "Mensual"

		For Each row As DataRow In dt_model.Rows

			If row.RowState = DataRowState.Deleted Then
				Continue For
			End If

			row.Item("Periodos")	= periodos
			row.Item("PerMensual")	= Math.Abs(Math.Round(row.Item("Capitalizacion") / periodos, 0))

            row.Item("Ley") = ObtieneValorLey()

            If row.Item("Ley") = 0 Then
                row.Item("LeyMensual") = 0
            Else
                row.Item("LeyMensual") = Math.Abs(Math.Round(row.Item("Capitalizacion") / row.Item("Ley"), 0))
            End If

            row.Item("Resolucion")	= ObtieneValorLey(row.Item("Edad"))

            If row.Item("Resolucion") = 0 Then
                row.Item("ResMensual") = 0
            Else
                row.Item("ResMensual") = Math.Abs(Math.Round(row.Item("Capitalizacion") / row.Item("Resolucion"), 0))
            End If

        Next
	End Sub

	'Hace una consulta de la tabla ley sipres y devuelve el mes de retiro.
	''' <summary>
	''' <para>Devuelve de la tabla a consultar el mes de retiro.</para>
	''' </summary>
	Public Function ObtieneValorLey(Optional ByVal edad As Integer = Nothing) As Integer

		Dim sipres_a_vigencia As Integer = Now.Year 'año de vigencia por defecto el actual
		Dim sipres_m_retiro As Integer = 0      'meses de retiro

		Try
			consultaBD.AbrirConexion()

			myda = consultaBD.GetTablaResolucionSipres(resolucion_sipres)
			myda.Fill(dt_ley_sipres)

            If edad <> Nothing Then
                'Dim anioNacimiento As Integer = sipres_a_vigencia - edad
                'sipres_a_vigencia = (sipres_a_vigencia - edad) - 65
                'Obtengo el anio en que debia jubilarse
                sipres_a_vigencia = sipres_a_vigencia - (edad - 65)
            End If

            For Each row As DataRow In dt_ley_sipres.Rows
				If row.Item("sipres_anio") = sipres_a_vigencia Then
					sipres_m_retiro = row.Item("sipres_meses")
				End If
			Next

			Return sipres_m_retiro

		Catch
			MsgBox("No se puede obtener los valores de la resolución.", MsgBoxStyle.Critical, "Error")
		Finally
			consultaBD.CerrarConexion()
		End Try

	End Function

    '*********************** FIN JUBILACIONES ***********************


    '********************* DEBITOS AUTOMATICOS **********************
    '****************************************************************

    ''' <summary>
    ''' Carga excel de debitos a un datatable y dejo solo campos utiles.
    ''' </summary>
    Public Function CargarDebitos(ByVal ruta As String, Optional ByVal hoja As String = "Hoja1") As DataTable
        Dim dt As DataTable = New DataTable()
        Dim query As String = "SELECT * FROM  [" & hoja & "$]"
        Dim dataSource As String = "data source=" & ruta & ";"
        oleDb_conn.ConnectionString = provider & dataSource & properties

        Try
            oleDb_conn.Open()
            'genera una consulta que devuelve todos los campos de la hoja
            Dim da As OleDbDataAdapter = New OleDbDataAdapter(query, oleDb_conn)

            da.Fill(dt)

            'Elimino columnas que no me sirven
            dt.Columns.Remove("CUIT")
            dt.Columns.Remove("EMPRESA")
            dt.Columns.Remove("CODIGO")
            dt.Columns.Remove("TIPO_DNI")
            dt.Columns.Remove("CUIT_DNI")
            dt.Columns.Remove("CBU")
            dt.Columns.Remove("IDCLIENTE")
            dt.Columns.Remove("RF_DEBIT")
            dt.Columns.Remove("FE_PRES")
            dt.Columns.Remove("FE_COMP")
            dt.Columns.Remove("FE_RECH")
            dt.Columns.Remove("FE_REVE")
            dt.Columns.Remove("IMP_REVE")
            dt.Columns.Remove("TRCNUMBER")

            dt.Columns.Add("MATRICULA", GetType(String))
            dt.Columns.Add("DESCRIP_1", GetType(String))
            dt.Columns.Add("DESCRIP_2", GetType(String))
            dt.Columns.Add("DESCRIP_3", GetType(String))
            dt.Columns.Add("PROCESO", GetType(String))
            dt.Columns.Add("CUOTA", GetType(Integer))
            dt.Columns.Add("TOTAL", GetType(Double))

            dt.Columns("MATRICULA").ReadOnly = False
            dt.Columns("DESCRIP_1").ReadOnly = False
            dt.Columns("DESCRIP_2").ReadOnly = False
            dt.Columns("DESCRIP_3").ReadOnly = False
            dt.Columns("PROCESO").ReadOnly = False
            dt.Columns("CUOTA").ReadOnly = False
            dt.Columns("TOTAL").ReadOnly = False

            dt.AcceptChanges()
        Catch
            MsgBox("Hubo un problema al procesar el archivo excel", MsgBoxStyle.Critical, "Error")
        Finally
            oleDb_conn.Close()
        End Try

        Return dt
    End Function

    ''' <summary>
    ''' False si no se reconoce el archivo a procesar o la caja se encuentra innoperable.
    ''' </summary>
    Public Function CheckArchivo(ByVal servicio As String) As Boolean
		Dim isValid As Boolean = False
		isValid = consultaBD.CajaActiva
		If servicio = "desconocido" Or servicio = "ninguno" Then
			MsgBox("No se puede procesar un archivo desconocido o nulo", MsgBoxStyle.Critical, "Error")
			isValid = False
		End If
		Return isValid
	End Function

	''' <summary>
	''' Devuelve titulo correcto del archivo de debitos automaticos posterior a procesar
	''' </summary>
	Public Sub ProsArchivo(ByVal lb_titulo As Label, ByVal servicio As String, ByVal dt_global As DataTable, ByRef tabControl As TabControl, ByRef fechaConcepto As DateTime, ByRef fechaComprobante As DateTime)
		Dim debito As New Debito(dt_global, tabControl, fechaConcepto, fechaComprobante)
		Select Case servicio
			Case "REFINASOCI"
				lb_titulo.Text = "refinanciación asociados"
				debito.REFINASOCI()
			Case "REFAFILIAD"
				lb_titulo.Text = "refinanciación afiliados"
				debito.REFAFILIAD()
			Case "DEUDA ASOC"
				lb_titulo.Text = "deudores asociados"
				debito.DEUDAASOC()
			Case "DEUDA APOR"
				lb_titulo.Text = "deudores aportes"
				debito.DEUDAAPOR()
			Case "DEUDA FARM"
				lb_titulo.Text = "deudores farmacia"
				debito.DEUDAFARM()
			Case "DEUDA JUBI"
				lb_titulo.Text = "deudores jubilados"
				debito.DEUDAJUBI()
			Case "DERECHO PR"
				lb_titulo.Text = "derecho de ejercicio prof."
				debito.DERECHOPR()
			Case "MORASIPRES"
				lb_titulo.Text = "moratorias"
				debito.MORATORIA()
			Case "AYUDA PERS"
				lb_titulo.Text = "ayudas personales"
				debito.AYUDAPERS(tabControl)
			Case Else
				lb_titulo.Text = "desconocido"
		End Select
	End Sub



	'********************* FIN DEBITOS AUTOMATICOS **********************

End Class
