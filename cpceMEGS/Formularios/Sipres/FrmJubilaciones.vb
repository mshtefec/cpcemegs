﻿Imports System.ComponentModel
Imports MySql.Data.MySqlClient

Public Class FrmJubilaciones
    'BD
    Private consultaBD As New ConsultaBD(True)
    Private myda As MySqlDataAdapter
    'UTIL
    Private sipresUtils As New SipresUtils
    'GLOBALES
    Private dt_global As DataTable = New DataTable()
    Private dt_sipres_res As DataTable = New DataTable()

    Private Sub FrmJubilaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Carga()
        OnEnterJubilados()
    End Sub

    'Carga datatables
    Private Sub Carga()
        Try
            consultaBD.AbrirConexion()

            myda = consultaBD.GetAfiliados()
            myda.Fill(dt_global)

            myda = consultaBD.GetNombresResolucionSipres()
            myda.Fill(dt_sipres_res)

            sl_resolucionSipres.DataSource = dt_sipres_res
            sl_resolucionSipres.DisplayMember = "sipres_reso"
            sl_resolucionSipres.ValueMember = "sipres_reso"

        Catch
            consultaBD.CerrarConexion()
            MsgBox("Consulte con el administrador de sistemas", MsgBoxStyle.Critical, "Error")
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub

    'Control de los tabs mientras va haciendo click
    Private Sub TabControlSipres_Selected(sender As Object, e As TabControlEventArgs) Handles TabControlSipres.Selected
        Select Case True
            Case TabControlSipres.SelectedTab Is TabJubilados
                OnEnterJubilados()
            Case TabControlSipres.SelectedTab Is TabProximosJub
                OnEnterProximosJub()
            Case TabControlSipres.SelectedTab Is TabMayoresJub
                OnEnterMayores()
            Case TabControlSipres.SelectedTab Is TabBaja
                OnEnterBaja()
        End Select
    End Sub

    'Tab Jubilados
    Private Sub OnEnterJubilados()
        Try
            Dim dt_jubilados As DataTable
            dt_jubilados = dt_global.Copy()

            For Each fila As DataRow In dt_jubilados.Rows
                'dejo solo categoria jubilados
                If Mid(fila.Item("Categoria"), 1, 2) <> "17" Then
                    fila.Delete()
                End If
            Next

            sipresUtils.CargaAporteCapitalizacion(dt_jubilados, True)

            dt_jubilados.Columns.Remove("DNI")
            dt_jubilados.Columns.Remove("FechadeNacimiento")
            dt_jubilados.Columns.Remove("Edad")
            dt_jubilados.Columns.Remove("Categoria")
            dt_jubilados.Columns.Remove("Telefono")
            dt_jubilados.Columns.Remove("Email")

            dt_jubilados.Columns.Add("Cuota", GetType(Double), "0")

            dgv_jubilados.DataSource = dt_jubilados
            dgv_jubilados.AutoResizeColumns()

            dgv_jubilados.Columns("capitalizacionJubilado").DefaultCellStyle.Format = "c"
            dgv_jubilados.Columns("Capitalizacion").DefaultCellStyle.Format = "c"
            dgv_jubilados.Columns("Cuota").DefaultCellStyle.Format = "c"

            'Agrego boton
            Dim btn As New DataGridViewButtonColumn()
            dgv_jubilados.Columns.Add(btn)
            btn.HeaderText = "Acciones"
            btn.Text = "Actualizar"
            btn.Name = "btn_update"
            btn.UseColumnTextForButtonValue = True

            'Ordenamiento
            dgv_jubilados.Sort(dgv_jubilados.Columns(2), ListSortDirection.Descending)
        Catch
            MsgBox("La pestaña jubilados no se cargo correctamente", MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    'Tab Proximos Jubilados
    Private Sub OnEnterProximosJub()
        Try
            Dim dt_prox_jubilados As DataTable
            dt_prox_jubilados = dt_global.Copy()

            For Each row As DataRow In dt_prox_jubilados.Rows
                'dejo todos excepto jubilados y dados de baja
                If Mid(row.Item("Categoria"), 1, 2) = "17" Or Mid(row.Item("Categoria"), 1, 2) = "22" Or
                    Convert.ToInt16(row.Item("Edad")) < 65 Or Convert.ToInt16(row.Item("Edad")) > 69 Then
                    row.Delete()
                End If
            Next

            sipresUtils.CargaAporteCapitalizacion(dt_prox_jubilados)
            sipresUtils.DeterminaCuotas(dt_prox_jubilados)

            dgv_proxjub.DataSource = dt_prox_jubilados
            dgv_proxjub.AutoResizeColumns()

            dgv_proxjub.Columns("Capitalizacion").DefaultCellStyle.Format = "c"
            dgv_proxjub.Columns("PerMensual").DefaultCellStyle.Format = "c"
            dgv_proxjub.Columns("LeyMensual").DefaultCellStyle.Format = "c"
            dgv_proxjub.Columns("ResMensual").DefaultCellStyle.Format = "c"

        Catch
            MsgBox("La pestaña proximos jubilados no se cargo correctamente", MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    'Tab Mayores de 69
    Private Sub OnEnterMayores()
        Try
            Dim dt_mayores As DataTable
            dt_mayores = dt_global.Copy()

            For Each row As DataRow In dt_mayores.Rows
                'dejo todos excepto jubilados y dados de baja
                If Mid(row.Item("Categoria"), 1, 2) = "17" Or Mid(row.Item("Categoria"), 1, 2) = "22" Or
                    Convert.ToInt16(row.Item("Edad")) < 70 Then
                    row.Delete()
                End If
            Next

            sipresUtils.CargaAporteCapitalizacion(dt_mayores)
            sipresUtils.DeterminaCuotas(dt_mayores)

            dgv_mayoresjub.DataSource = dt_mayores
            dgv_mayoresjub.AutoResizeColumns()

            dgv_mayoresjub.Columns("Capitalizacion").DefaultCellStyle.Format = "c"
            dgv_mayoresjub.Columns("PerMensual").DefaultCellStyle.Format = "c"
            dgv_mayoresjub.Columns("LeyMensual").DefaultCellStyle.Format = "c"
            dgv_mayoresjub.Columns("ResMensual").DefaultCellStyle.Format = "c"
        Catch
            MsgBox("La pestaña mayores de 70 años no se cargo correctamente", MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    'Tab Mayores de 69
    Private Sub OnEnterBaja()
        Try
            Dim dt_mayores As DataTable
            dt_mayores = dt_global.Copy()

            For Each row As DataRow In dt_mayores.Rows
                'dejo solo dados de baja, excepto fallecidos
                If Mid(row.Item("Categoria"), 1, 2) <> "22" Or row.Item("Categoria") = "220100" Or
                    Convert.ToInt16(row.Item("Edad")) < 65 Then
                    row.Delete()
                End If
            Next

            dgv_baja.DataSource = dt_mayores
            dgv_baja.AutoResizeColumns()
        Catch
            MsgBox("La pestaña dados de baja no se cargo correctamente", MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Private Sub dgv_jubilados_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgv_jubilados.CellFormatting
        If Me.dgv_jubilados.Columns(e.ColumnIndex).Name = "FechadeJubilacion" Then
            If e.Value <> Nothing Then
                e.CellStyle.BackColor = Color.Aqua
            End If
        End If
    End Sub

    Private Sub btn_procesarReso_Click(sender As Object, e As EventArgs) Handles btn_procesarReso.Click
        MsgBox("Función en desarrollo", MsgBoxStyle.Critical, "Atención")
    End Sub
End Class