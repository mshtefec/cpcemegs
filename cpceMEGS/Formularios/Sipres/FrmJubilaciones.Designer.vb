﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmJubilaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmJubilaciones))
        Me.Lb_header = New System.Windows.Forms.Label()
        Me.dgv_jubilados = New System.Windows.Forms.DataGridView()
        Me.TabControlSipres = New System.Windows.Forms.TabControl()
        Me.TabJubilados = New System.Windows.Forms.TabPage()
        Me.TabProximosJub = New System.Windows.Forms.TabPage()
        Me.dgv_proxjub = New System.Windows.Forms.DataGridView()
        Me.TabMayoresJub = New System.Windows.Forms.TabPage()
        Me.dgv_mayoresjub = New System.Windows.Forms.DataGridView()
        Me.TabBaja = New System.Windows.Forms.TabPage()
        Me.dgv_baja = New System.Windows.Forms.DataGridView()
        Me.sl_resolucionSipres = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_procesarReso = New System.Windows.Forms.Button()
        Me.lb_selectRes = New System.Windows.Forms.Label()
        CType(Me.dgv_jubilados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlSipres.SuspendLayout()
        Me.TabJubilados.SuspendLayout()
        Me.TabProximosJub.SuspendLayout()
        CType(Me.dgv_proxjub, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabMayoresJub.SuspendLayout()
        CType(Me.dgv_mayoresjub, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabBaja.SuspendLayout()
        CType(Me.dgv_baja, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Lb_header
        '
        Me.Lb_header.AutoSize = True
        Me.Lb_header.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_header.Location = New System.Drawing.Point(12, 9)
        Me.Lb_header.Name = "Lb_header"
        Me.Lb_header.Size = New System.Drawing.Size(205, 24)
        Me.Lb_header.TabIndex = 1
        Me.Lb_header.Text = "SIPRES Jubilaciones"
        '
        'dgv_jubilados
        '
        Me.dgv_jubilados.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_jubilados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_jubilados.Location = New System.Drawing.Point(3, 3)
        Me.dgv_jubilados.Name = "dgv_jubilados"
        Me.dgv_jubilados.Size = New System.Drawing.Size(918, 447)
        Me.dgv_jubilados.TabIndex = 2
        '
        'TabControlSipres
        '
        Me.TabControlSipres.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControlSipres.Controls.Add(Me.TabJubilados)
        Me.TabControlSipres.Controls.Add(Me.TabProximosJub)
        Me.TabControlSipres.Controls.Add(Me.TabMayoresJub)
        Me.TabControlSipres.Controls.Add(Me.TabBaja)
        Me.TabControlSipres.Location = New System.Drawing.Point(17, 53)
        Me.TabControlSipres.Name = "TabControlSipres"
        Me.TabControlSipres.SelectedIndex = 0
        Me.TabControlSipres.Size = New System.Drawing.Size(932, 479)
        Me.TabControlSipres.TabIndex = 3
        '
        'TabJubilados
        '
        Me.TabJubilados.Controls.Add(Me.dgv_jubilados)
        Me.TabJubilados.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TabJubilados.Location = New System.Drawing.Point(4, 22)
        Me.TabJubilados.Name = "TabJubilados"
        Me.TabJubilados.Padding = New System.Windows.Forms.Padding(3)
        Me.TabJubilados.Size = New System.Drawing.Size(924, 453)
        Me.TabJubilados.TabIndex = 0
        Me.TabJubilados.Text = "Jubilados"
        Me.TabJubilados.UseVisualStyleBackColor = True
        '
        'TabProximosJub
        '
        Me.TabProximosJub.Controls.Add(Me.dgv_proxjub)
        Me.TabProximosJub.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TabProximosJub.Location = New System.Drawing.Point(4, 22)
        Me.TabProximosJub.Name = "TabProximosJub"
        Me.TabProximosJub.Padding = New System.Windows.Forms.Padding(3)
        Me.TabProximosJub.Size = New System.Drawing.Size(924, 453)
        Me.TabProximosJub.TabIndex = 1
        Me.TabProximosJub.Text = "Proximos Jubilados"
        Me.TabProximosJub.UseVisualStyleBackColor = True
        '
        'dgv_proxjub
        '
        Me.dgv_proxjub.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_proxjub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_proxjub.Location = New System.Drawing.Point(3, 3)
        Me.dgv_proxjub.Name = "dgv_proxjub"
        Me.dgv_proxjub.Size = New System.Drawing.Size(918, 447)
        Me.dgv_proxjub.TabIndex = 0
        '
        'TabMayoresJub
        '
        Me.TabMayoresJub.Controls.Add(Me.dgv_mayoresjub)
        Me.TabMayoresJub.Location = New System.Drawing.Point(4, 22)
        Me.TabMayoresJub.Name = "TabMayoresJub"
        Me.TabMayoresJub.Size = New System.Drawing.Size(924, 453)
        Me.TabMayoresJub.TabIndex = 2
        Me.TabMayoresJub.Text = "Mayores de 70"
        Me.TabMayoresJub.UseVisualStyleBackColor = True
        '
        'dgv_mayoresjub
        '
        Me.dgv_mayoresjub.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_mayoresjub.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_mayoresjub.Location = New System.Drawing.Point(3, 3)
        Me.dgv_mayoresjub.Name = "dgv_mayoresjub"
        Me.dgv_mayoresjub.Size = New System.Drawing.Size(918, 447)
        Me.dgv_mayoresjub.TabIndex = 0
        '
        'TabBaja
        '
        Me.TabBaja.Controls.Add(Me.dgv_baja)
        Me.TabBaja.Location = New System.Drawing.Point(4, 22)
        Me.TabBaja.Name = "TabBaja"
        Me.TabBaja.Size = New System.Drawing.Size(924, 453)
        Me.TabBaja.TabIndex = 3
        Me.TabBaja.Text = "Dados de Baja"
        Me.TabBaja.UseVisualStyleBackColor = True
        '
        'dgv_baja
        '
        Me.dgv_baja.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_baja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_baja.Location = New System.Drawing.Point(3, 3)
        Me.dgv_baja.Name = "dgv_baja"
        Me.dgv_baja.Size = New System.Drawing.Size(918, 447)
        Me.dgv_baja.TabIndex = 0
        '
        'sl_resolucionSipres
        '
        Me.sl_resolucionSipres.FormattingEnabled = True
        Me.sl_resolucionSipres.Location = New System.Drawing.Point(358, 28)
        Me.sl_resolucionSipres.Name = "sl_resolucionSipres"
        Me.sl_resolucionSipres.Size = New System.Drawing.Size(158, 21)
        Me.sl_resolucionSipres.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_procesarReso)
        Me.GroupBox1.Controls.Add(Me.lb_selectRes)
        Me.GroupBox1.Controls.Add(Me.sl_resolucionSipres)
        Me.GroupBox1.Location = New System.Drawing.Point(311, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(634, 56)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Herramientas"
        '
        'btn_procesarReso
        '
        Me.btn_procesarReso.Location = New System.Drawing.Point(537, 28)
        Me.btn_procesarReso.Name = "btn_procesarReso"
        Me.btn_procesarReso.Size = New System.Drawing.Size(91, 23)
        Me.btn_procesarReso.TabIndex = 6
        Me.btn_procesarReso.Text = "Procesar"
        Me.btn_procesarReso.UseVisualStyleBackColor = True
        '
        'lb_selectRes
        '
        Me.lb_selectRes.AutoSize = True
        Me.lb_selectRes.Location = New System.Drawing.Point(355, 12)
        Me.lb_selectRes.Name = "lb_selectRes"
        Me.lb_selectRes.Size = New System.Drawing.Size(119, 13)
        Me.lb_selectRes.TabIndex = 5
        Me.lb_selectRes.Text = "Seleccione Resolución:"
        '
        'FrmJubilaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(961, 544)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TabControlSipres)
        Me.Controls.Add(Me.Lb_header)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmJubilaciones"
        Me.Text = "SIPRES Jubilaciones"
        CType(Me.dgv_jubilados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlSipres.ResumeLayout(False)
        Me.TabJubilados.ResumeLayout(False)
        Me.TabProximosJub.ResumeLayout(False)
        CType(Me.dgv_proxjub, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabMayoresJub.ResumeLayout(False)
        CType(Me.dgv_mayoresjub, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabBaja.ResumeLayout(False)
        CType(Me.dgv_baja, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lb_header As Label
    Friend WithEvents dgv_jubilados As DataGridView
    Friend WithEvents TabControlSipres As TabControl
    Friend WithEvents TabJubilados As TabPage
    Friend WithEvents TabProximosJub As TabPage
    Friend WithEvents dgv_proxjub As DataGridView
    Friend WithEvents TabMayoresJub As TabPage
    Friend WithEvents dgv_mayoresjub As DataGridView
    Friend WithEvents sl_resolucionSipres As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lb_selectRes As Label
    Friend WithEvents btn_procesarReso As Button
    Friend WithEvents TabBaja As TabPage
    Friend WithEvents dgv_baja As DataGridView
End Class
