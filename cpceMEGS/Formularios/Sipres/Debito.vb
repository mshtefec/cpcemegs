﻿Imports MySql.Data.MySqlClient

Public Class Debito

	Private consultaBD As New ConsultaBD(True)
	Private importar As New Importar
	Private rest As New Rest
	Private calculos As New Calculos
	Private dt_global As DataTable
	Private dt_cuotas_impagas As DataTable
	Private tabControl As TabControl
	Private cuentaDebito As String
	Private procesoDebito As String
	Private count As Integer
	Private count_rows As Integer
	Private siguiente As DataRow
	Private fecha_concepto As DateTime
	Private fecha_comprobante As DateTime

	Private cuentas_ref_asociados() As String = {"02D067", "11030801", "13010900"} 'ok
	Private cuentas_ref_afiliados() As String = {"02D068", "11030801", "13010800"} 'ok
	Private cuentas_derecho_ejercicio() As String = {"02D001", "11030801", "13010200"} 'ok
	Private cuentas_deuda_asociados() As String = {"02D005", "11030801", "13010600"} 'ok
	Private cuentas_deuda_aportes() As String = {"01D153", "11031001", "41022000", "22050200", "13040100", "31020700", "22040100", "22010100", "22010200", "31021300", "31021400", "31021100"} 'ok antes 01D148
	Private cuentas_deuda_farmacia() As String = {"01D026", "11031001", "22050300", "13050900"} 'ok
	Private cuentas_deuda_jubilados() As String = {"01D029", "11031001", "13040300"} 'ok
	Private cuentas_ayudas_personales() As String = {"-", "11031001", "22050300", "C3"}
	Private cuentas_ayudas_personales_afiliados() As String = {"01D022", "11031001", "22050300", "C3"} 'ver 13051000
	Private cuentas_ayudas_personales_asociados() As String = {"01D023", "11031001", "22050300", "C3"} 'ver 13051100
	Private cuentas_moratoria_sipres() As String = {"01DCMO", "11031001", "13040400", "22040100", "22040700", "22040800", "22010100", "22020100", "22010200", "22020200", "31021400", "31021300", "31021800", "22030406"} 'ok

	'Constructor
	Public Sub New(dt_global As DataTable, tabControl As TabControl, fechaConcepto As DateTime, fechaComprobante As DateTime)
		Me.dt_global = dt_global
		Me.tabControl = tabControl
		Me.fecha_concepto = fechaConcepto
		Me.fecha_comprobante = fechaComprobante
	End Sub

	Private Sub Enviar()

		Dim answer As Integer
		answer = MsgBox("Seguro desea generar los Comprobantes y Asientos?", vbYesNo + vbQuestion, "Generando")

		If answer = vbYes Then

			'Chequeo si toda la columna procesos esta cargada si no, se va a detener el envio...
			For Each row As DataRow In dt_global.Rows
				Dim proceso = row.Item("PROCESO")
				If (proceso Is DBNull.Value) Or (proceso Is Nothing) Or (proceso = "") Then
					MsgBox("Faltan cargar procesos, o el archivo ya se proceso", MsgBoxStyle.Critical, "Atención")
					Exit Sub
				End If
			Next

			Dim zeta As Double
			Dim caja As Double

			If consultaBD.CajaActiva Then
				zeta = consultaBD.Zeta
				caja = consultaBD.Caja

				Try
					Dim json As String = importar.generaJson(dt_global, "debitos_json", "-", fecha_comprobante)
					rest.sendDebito(json)
					Catch ex As Exception
						MsgBox("Ups!, algo no salio como se esperaba", MsgBoxStyle.Critical, "Error")
					End Try
				Else
					MsgBox("Verifique si la caja es actual y se encuentra operable.", MsgBoxStyle.Critical, "Error")
			End If

		End If

	End Sub

    Public Function cambiaCodigo(codigo As String) As String
        Dim message As String = ""

        Select Case codigo
            Case "R02"
                message = "Cta Cerrada p/Orden Judicial"
            Case "R03"
            Case "R08"
                message = "Orden de no pagar"
            Case "R10"
                message = "Falta de Fondos"
            Case "R15"
                message = "Baja de Servicios"
        End Select

        Return message
    End Function

    Public Function checkDeuda(dni As Integer) As Dictionary(Of Integer, Double)
		Dim cuota As New Dictionary(Of Integer, Double)
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", Integer.Parse(dni), True, Now.Date)
		dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("Select") = "True"
		cuota.Add(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("nroCuota"), dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("total"))
		Return cuota
	End Function

	Public Sub RemoveRechazados()
		Dim dgvRechazados As New DataGridView
		Dim newTabRechazados As New TabPage
		Dim dt_rechazados As DataTable = dt_global.Clone()
		newTabRechazados.Text = "Rechazados"

		For Each row As DataRow In dt_global.Rows
			If row.Item("IMP_RECH") > 0 And row IsNot Nothing Then
				dt_rechazados.ImportRow(row)
				row.Delete()
			End If
		Next

		dt_global.Columns.Remove("MOTIVO_RE")
		dt_global.Columns.Remove("IMP_RECH")

		dt_global.AcceptChanges()

		dt_rechazados.Columns.Remove("IMP_COMP")
		dt_rechazados.Columns.Remove("DESCRIP_1")
		dt_rechazados.Columns.Remove("DESCRIP_2")
		dt_rechazados.Columns.Remove("DESCRIP_3")
		dt_rechazados.Columns.Remove("PROCESO")
		dt_rechazados.Columns.Remove("CUOTA")
		dt_rechazados.Columns.Remove("TOTAL")
		dt_rechazados.Columns.Remove("MATRICULA")
		dt_rechazados.Columns("IMP_RECH").ColumnName = "MONTO"
		dt_rechazados.AcceptChanges()

		For Each row As DataRow In dt_rechazados.Rows
			row.Item("MOTIVO_RE") = cambiaCodigo(row.Item("MOTIVO_RE"))
		Next

		dgvRechazados.DataSource = dt_rechazados
		dgvRechazados.AutoResizeColumns()
		dgvRechazados.Dock = DockStyle.Fill
		dgvRechazados.ColumnHeadersDefaultCellStyle.ForeColor = Color.White 'Letras
		dgvRechazados.ColumnHeadersDefaultCellStyle.Font = New Font("Roboto", 8, FontStyle.Bold)
		dgvRechazados.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#323232") 'Fondo de Header
		dgvRechazados.RowHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#7f7f7f") 'Selector
		dgvRechazados.RowsDefaultCellStyle.Font = New Font("Roboto", 8)

		tabControl.TabPages.Add(newTabRechazados)
		tabControl.TabPages(1).Controls.Add(dgvRechazados)
	End Sub

	Public Sub REFINASOCI()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_ref_asociados(2)
		procesoDebito = cuentas_ref_asociados(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_ref_asociados.Count() - 1
			dt_global.Columns.Add(cuentas_ref_asociados(index), GetType(String))
			dt_global.Columns(cuentas_ref_asociados(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Refinasoci(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Refinasoci(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

	End Sub

	Public Sub REFAFILIAD()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_ref_afiliados(2)
		procesoDebito = cuentas_ref_afiliados(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_ref_afiliados.Count() - 1
			dt_global.Columns.Add(cuentas_ref_afiliados(index), GetType(String))
			dt_global.Columns(cuentas_ref_afiliados(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Refafiliad(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Refafiliad(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

	End Sub

	Public Sub DERECHOPR()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_derecho_ejercicio(2)
		procesoDebito = cuentas_derecho_ejercicio(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_derecho_ejercicio.Count() - 1
			dt_global.Columns.Add(cuentas_derecho_ejercicio(index), GetType(String))
			dt_global.Columns(cuentas_derecho_ejercicio(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_DerechoEjercicio(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_DerechoEjercicio(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

	End Sub

	Public Sub DEUDAASOC()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_deuda_asociados(2)
		procesoDebito = cuentas_deuda_asociados(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_deuda_asociados.Count() - 1
			dt_global.Columns.Add(cuentas_deuda_asociados(index), GetType(String))
			dt_global.Columns(cuentas_deuda_asociados(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Deudaasoc(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Deudaasoc(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

	End Sub

	Public Sub DEUDAAPOR()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_deuda_aportes(4)
		procesoDebito = cuentas_deuda_aportes(0)

		Me.RemoveRechazados()

		dt_global.Columns.Add("INTERES", GetType(String))
		dt_global.Columns("INTERES").ReadOnly = False

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_deuda_aportes.Count() - 1
			dt_global.Columns.Add(cuentas_deuda_aportes(index), GetType(String))
			dt_global.Columns(cuentas_deuda_aportes(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				anterior.Item("PROCESO") = consultaBD.DeterminaProcesoDeudoresAportes("DNI", anterior.Item("NUM_DNI"))
				procesoDebito = anterior.Item("PROCESO")

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Deudaapor(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			anterior.Item("PROCESO") = consultaBD.DeterminaProcesoDeudoresAportes("DNI", anterior.Item("NUM_DNI"))
			procesoDebito = anterior.Item("PROCESO")

			Pro_Deudaapor(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

		Me.QuitarInteres(dt_global, cuentas_deuda_aportes(3))
		dt_global.Columns.Remove("INTERES")
		dt_global.AcceptChanges()

	End Sub

	Public Sub DEUDAFARM()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_deuda_farmacia(3)
		procesoDebito = cuentas_deuda_farmacia(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_deuda_farmacia.Count() - 1
			dt_global.Columns.Add(cuentas_deuda_farmacia(index), GetType(String))
			dt_global.Columns(cuentas_deuda_farmacia(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Deudafarm(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Deudafarm(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

		Me.QuitarInteres(dt_global, cuentas_deuda_farmacia(2))
		dt_global.AcceptChanges()

	End Sub

	Public Sub DEUDAJUBI()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_deuda_jubilados(2)
		procesoDebito = cuentas_deuda_jubilados(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_deuda_jubilados.Count() - 1
			dt_global.Columns.Add(cuentas_deuda_jubilados(index), GetType(String))
			dt_global.Columns(cuentas_deuda_jubilados(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Deudajubi(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Deudajubi(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If

	End Sub

	Public Sub AYUDAPERS(ByRef tabControlDebitos As TabControl)

		Me.RemoveRechazados()

		dt_global.Columns.Add("CATEGORIA", GetType(String))
		dt_global.Columns("CATEGORIA").ReadOnly = False

		For index = 1 To cuentas_ayudas_personales.Count() - 1
			dt_global.Columns.Add(cuentas_ayudas_personales(index), GetType(String))
			dt_global.Columns(cuentas_ayudas_personales(index)).ReadOnly = False
		Next

		Dim dgvAfiliados As New DataGridView
		Dim dgvAsociados As New DataGridView
		Dim newTabAfiliados As New TabPage
		Dim newTabAsociados As New TabPage
		'copy() copia la estructura y todo el contenido.
		'clone() copia solo la estructura pero no el contenido.
		Dim dt_afiliados As DataTable = dt_global.Clone()
		Dim dt_asociados As DataTable = dt_global.Clone()

		newTabAfiliados.Text = "Afiliados"
		dgvAfiliados.Name = "dgv_afiliados"
		newTabAsociados.Text = "Asociados"
		dgvAsociados.Name = "dgv_asociados"

		For Each row As DataRow In dt_global.Rows
			row.Item("CATEGORIA") = consultaBD.buscaCategoriaScalar("DNI", row.Item("NUM_DNI"))
			If row.Item("CATEGORIA").Substring(0, 1) = "1" Then
				dt_afiliados.ImportRow(row)
			Else
				dt_asociados.ImportRow(row)
			End If
		Next

		If dt_afiliados.Rows.Count <> 0 Then
			Me.AYUDAPERS_AFILIADOS(dt_afiliados)
			dgvAfiliados.DataSource = dt_afiliados
		End If

		If dt_asociados.Rows.Count <> 0 Then
			Me.AYUDAPERS_ASOCIADOS(dt_asociados)
			dgvAsociados.DataSource = dt_asociados
		End If

		dgvAfiliados.AutoResizeColumns()
		dgvAsociados.AutoResizeColumns()

		dgvAfiliados.Dock = DockStyle.Fill
		dgvAsociados.Dock = DockStyle.Fill

		dgvAfiliados.ColumnHeadersDefaultCellStyle.ForeColor = Color.White 'Letras
		dgvAfiliados.ColumnHeadersDefaultCellStyle.Font = New Font("Roboto", 8, FontStyle.Bold)
		dgvAfiliados.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#323232") 'Fondo de Header
		dgvAfiliados.RowHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#7f7f7f") 'Selector
		dgvAfiliados.RowsDefaultCellStyle.Font = New Font("Roboto", 8)

		dgvAsociados.ColumnHeadersDefaultCellStyle.ForeColor = Color.White 'Letras
		dgvAsociados.ColumnHeadersDefaultCellStyle.Font = New Font("Roboto", 8, FontStyle.Bold)
		dgvAsociados.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#323232") 'Fondo de Header
		dgvAsociados.RowHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#7f7f7f") 'Selector
		dgvAsociados.RowsDefaultCellStyle.Font = New Font("Roboto", 8)

		tabControlDebitos.TabPages.Add(newTabAfiliados)
		tabControlDebitos.TabPages.Add(newTabAsociados)

		tabControlDebitos.TabPages(2).Controls.Add(dgvAfiliados)
		tabControlDebitos.TabPages(3).Controls.Add(dgvAsociados)

		For Each row As DataRow In dt_global.Rows
			row.Delete()
		Next
		dt_global.AcceptChanges()

		For Each row As DataRow In dt_afiliados.Rows
			dt_global.ImportRow(row)
		Next
		For Each row As DataRow In dt_asociados.Rows
			dt_global.ImportRow(row)
		Next

		Me.QuitarInteres(dt_global, cuentas_ayudas_personales(2))
		dt_global.Columns.Remove("CATEGORIA")
		dt_global.AcceptChanges()

	End Sub

	Public Sub AYUDAPERS_AFILIADOS(ByRef dt_afiliados As DataTable)
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_afiliados.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_ayudas_personales_afiliados(2)
		procesoDebito = cuentas_ayudas_personales_afiliados(0)

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		'For index = 1 To cuentas_ayudas_personales_afiliados.Count() - 1
		'dt_afiliados.Columns.Add(cuentas_ayudas_personales_afiliados(index), GetType(String))
		'dt_afiliados.Columns(cuentas_ayudas_personales_afiliados(index)).ReadOnly = False
		'Next

		For Each row As DataRow In dt_afiliados.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_AyudaPers_Afiliados(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice, dt_afiliados)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_AyudaPers_Afiliados(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice, dt_afiliados)
		End If

		Me.QuitarInteres(dt_afiliados, cuentas_ayudas_personales(2))
	End Sub

	Public Sub AYUDAPERS_ASOCIADOS(ByRef dt_asociados As DataTable)
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_asociados.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_ayudas_personales_asociados(2)
		procesoDebito = cuentas_ayudas_personales_asociados(0)

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		'For index = 1 To cuentas_ayudas_personales_asociados.Count() - 1
		'dt_asociados.Columns.Add(cuentas_ayudas_personales_asociados(index), GetType(String))
		'dt_asociados.Columns(cuentas_ayudas_personales_asociados(index)).ReadOnly = False
		'Next

		For Each row As DataRow In dt_asociados.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_AyudaPers_Asociados(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice, dt_asociados)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_AyudaPers_Asociados(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice, dt_asociados)
		End If

		Me.QuitarInteres(dt_asociados, cuentas_ayudas_personales(2))
	End Sub

	Public Sub MORATORIA()
		Dim primero As Boolean = True
		Dim cuotas_a_cobrar As Integer = 1
		Dim anterior As DataRow = dt_global.Rows(0) 'lo seteo con el primer elemento
		Dim indice As Integer = 0
		'Intereses = 0
		cuentaDebito = cuentas_moratoria_sipres(2)
		procesoDebito = cuentas_moratoria_sipres(0)

		Me.RemoveRechazados()

		'count - 1 porque cuenta de 1 el cont no tiene en cuenta la primera posicion
		For index = 1 To cuentas_moratoria_sipres.Count() - 1
			dt_global.Columns.Add(cuentas_moratoria_sipres(index), GetType(String))
			dt_global.Columns(cuentas_moratoria_sipres(index)).ReadOnly = False
		Next

		For Each row As DataRow In dt_global.Rows

			If primero Then
				primero = False
			Else

				If row.Item("NUM_DNI") = anterior.Item("NUM_DNI") Then
					cuotas_a_cobrar += 1
				Else
					Pro_Moratoria(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)

					cuotas_a_cobrar = 1
				End If

			End If
			anterior = row
			indice += 1
		Next

		'procesa el ultimo
		If primero = False Then
			Pro_Moratoria(Integer.Parse(anterior.Item("NUM_DNI")), cuotas_a_cobrar, indice)
		End If
	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Refinasoci(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 1 To cuentas_ref_asociados.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_ref_asociados(index)) = dt_global.Rows(indice - 1)("IMP_COMP")
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_ref_asociados.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_ref_asociados(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Refafiliad(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 1 To cuentas_ref_afiliados.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_ref_afiliados(index)) = dt_global.Rows(indice - 1)("IMP_COMP")
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_ref_afiliados.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_ref_afiliados(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_DerechoEjercicio(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 1 To cuentas_derecho_ejercicio.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_derecho_ejercicio(index)) = dt_global.Rows(indice - 1)("IMP_COMP")
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_derecho_ejercicio.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_derecho_ejercicio(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Deudaasoc(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			While nroCuotas > 0
				Dim fechaCuota = DateAdd("m", (nroCuotas - 1), Now)

				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + fechaCuota.ToString("MM") + " MES: " + fechaCuota.ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = fechaCuota.ToString("MM")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_global.Rows(indice - nroCuotas)("IMP_COMP")

				For index = 1 To cuentas_deuda_asociados.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_deuda_asociados(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_deuda_asociados.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_deuda_asociados(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Deudaapor(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)
		Dim porcentajes As MySqlDataAdapter
		Dim DSPorcentajes As DataSet = New DataSet()

		If dt_cuotas_impagas.Rows.Count = 0 Then
			porcentajes = consultaBD.buscaProcetotePorcentaje(procesoDebito)
			porcentajes.Fill(DSPorcentajes, "porcentajes")
			'Esta al dia...
			While nroCuotas > 0
				Dim fechaCuota = DateAdd("m", (nroCuotas - 1), Now)

				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + fechaCuota.ToString("MM") + " MES: " + fechaCuota.ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = fechaCuota.ToString("MM")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				dt_global.Rows(indice - nroCuotas)("INTERES") = "0"

				procesaAportes(DSPorcentajes, dt_global, indice - nroCuotas, procesoDebito)

				nroCuotas -= 1
			End While
		Else

			Try
				While nroCuotas > 0

					procesoDebito = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("ProCancel")
					procesoDebito = procesoDebito.Replace("R", "D")
					porcentajes = consultaBD.buscaProcetotePorcentaje(procesoDebito)
					porcentajes.Fill(DSPorcentajes, "porcentajes")

					dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
					dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
					dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
					dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
					dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
					dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
					dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
					dt_global.Rows(indice - nroCuotas)("INTERES") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("interes")

					procesaAportes(DSPorcentajes, dt_global, indice - nroCuotas, procesoDebito)

					nroCuotas -= 1
				End While
			Catch ex As Exception
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				dt_global.Rows(indice - nroCuotas)("INTERES") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - 1)("interes")
				porcentajes = consultaBD.buscaProcetotePorcentaje(procesoDebito)
				porcentajes.Fill(DSPorcentajes, "porcentajes")

				procesaAportes(DSPorcentajes, dt_global, indice - nroCuotas, procesoDebito)

				MsgBox("El Matriculado " + dt_global.Rows(indice - nroCuotas)("DENOMINAC") + ", CP" + dt_global.Rows(indice - nroCuotas)("MATRICULA") + " solo posee 1 cuota y se debito 2.")

				nroCuotas = 0
				Exit Sub
			End Try

		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Deudafarm(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 1 To cuentas_deuda_farmacia.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_deuda_farmacia(index)) = dt_global.Rows(indice - 1)("IMP_COMP")
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_deuda_farmacia.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_deuda_farmacia(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Deudajubi(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 1 To cuentas_deuda_jubilados.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_deuda_jubilados(index)) = dt_global.Rows(indice - 1)("IMP_COMP")
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_deuda_jubilados.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_deuda_jubilados(index)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_AyudaPers_Afiliados(nroDni As Integer, nroCuotas As Integer, indice As Integer, dt_afiliados As DataTable)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			While nroCuotas > 0
				Dim fechaCuota = DateAdd("m", (nroCuotas - 1), Now)

				dt_afiliados.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + fechaCuota.ToString("MM") + " MES: " + fechaCuota.ToString("MM/yyyy")
				dt_afiliados.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_afiliados.Rows(indice - nroCuotas)("CUOTA") = fechaCuota.ToString("MM")
				dt_afiliados.Rows(indice - nroCuotas)("TOTAL") = dt_afiliados.Rows(indice - nroCuotas)("IMP_COMP")

				For index = 1 To cuentas_ayudas_personales_afiliados.Count() - 1
					dt_afiliados.Rows(indice - nroCuotas)(cuentas_ayudas_personales_afiliados(index)) = dt_afiliados.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1

			End While
		Else
			While nroCuotas > 0
				dt_afiliados.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_afiliados.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_afiliados.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_afiliados.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_afiliados.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_ayudas_personales_afiliados.Count() - 1
					dt_afiliados.Rows(indice - nroCuotas)(cuentas_ayudas_personales_afiliados(index)) = dt_afiliados.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_AyudaPers_Asociados(nroDni As Integer, nroCuotas As Integer, indice As Integer, dt_asociados As DataTable)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			While nroCuotas > 0
				Dim fechaCuota = DateAdd("m", (nroCuotas - 1), Now)

				dt_asociados.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + fechaCuota.ToString("MM") + " MES: " + fechaCuota.ToString("MM/yyyy")
				dt_asociados.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_asociados.Rows(indice - nroCuotas)("CUOTA") = fechaCuota.ToString("MM")
				dt_asociados.Rows(indice - nroCuotas)("TOTAL") = dt_asociados.Rows(indice - nroCuotas)("IMP_COMP")

				For index = 1 To cuentas_ayudas_personales_asociados.Count() - 1
					dt_asociados.Rows(indice - nroCuotas)(cuentas_ayudas_personales_asociados(index)) = dt_asociados.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1

			End While
		Else
			While nroCuotas > 0
				dt_asociados.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_asociados.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_asociados.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_asociados.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				dt_asociados.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")

				For index = 1 To cuentas_ayudas_personales_asociados.Count() - 1
					dt_asociados.Rows(indice - nroCuotas)(cuentas_ayudas_personales_asociados(index)) = dt_asociados.Rows(indice - nroCuotas)("IMP_COMP")
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub
	'por cada fila que se repita el matriculado va a acumular las cuotas y setear todos los montos.
	Public Sub Pro_Moratoria(nroDni As Integer, nroCuotas As Integer, indice As Integer)
		'busca las cuotas que debe
		dt_cuotas_impagas = calculos.CuotasServicioImpagas(cuentaDebito, "DNI", nroDni, True, Now.Date)

		If dt_cuotas_impagas.Rows.Count = 0 Then
			'Esta al dia...
			dt_global.Rows(indice - 1)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
			dt_global.Rows(indice - 1)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
			dt_global.Rows(indice - 1)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
			dt_global.Rows(indice - 1)("DESCRIP_3") = "CUOTA Nro: " + Now.ToString("MM") + " MES: " + Now.ToString("MM/yyyy")
			dt_global.Rows(indice - 1)("PROCESO") = procesoDebito
			dt_global.Rows(indice - 1)("CUOTA") = Now.ToString("MM")
			dt_global.Rows(indice - 1)("TOTAL") = dt_global.Rows(indice - 1)("IMP_COMP")

			dt_global.Rows(indice - 1)(cuentas_moratoria_sipres(1)) = dt_global.Rows(indice - 1)("IMP_COMP")
			dt_global.Rows(indice - 1)(cuentas_moratoria_sipres(2)) = dt_global.Rows(indice - 1)("IMP_COMP")

			For index = 3 To cuentas_moratoria_sipres.Count() - 1
				dt_global.Rows(indice - 1)(cuentas_moratoria_sipres(index)) = Math.Round(dt_global.Rows(indice - 1)("TOTAL") * montosPorcentajesMoratoria(index - 3), 2)
			Next

		Else
			While nroCuotas > 0
				dt_global.Rows(indice - nroCuotas)("MATRICULA") = consultaBD.buscaMatriculaScalar(nroDni)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_1") = consultaBD.buscaProcesoDescripcionScalar(procesoDebito)
				dt_global.Rows(indice - nroCuotas)("DESCRIP_2") = "DEBITO AUTOMATICO CON FECHA: " + fecha_concepto.ToString("dd/MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("DESCRIP_3") = "CUOTA Nro: " + dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota").ToString + " MES: " + Convert.ToDateTime(dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("Vencimiento")).ToString("MM/yyyy")
				dt_global.Rows(indice - nroCuotas)("PROCESO") = procesoDebito
				dt_global.Rows(indice - nroCuotas)("CUOTA") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("nroCuota")
				If dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total") = "0" Then
					dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
					dt_global.Rows(indice - nroCuotas)(cuentas_moratoria_sipres(1)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
					dt_global.Rows(indice - nroCuotas)(cuentas_moratoria_sipres(2)) = dt_global.Rows(indice - nroCuotas)("IMP_COMP")
				Else
					dt_global.Rows(indice - nroCuotas)("TOTAL") = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")
					dt_global.Rows(indice - nroCuotas)(cuentas_moratoria_sipres(1)) = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")
					dt_global.Rows(indice - nroCuotas)(cuentas_moratoria_sipres(2)) = dt_cuotas_impagas.Rows(dt_cuotas_impagas.Rows.Count - nroCuotas)("total")
				End If

				For index = 3 To cuentas_moratoria_sipres.Count() - 1
					dt_global.Rows(indice - nroCuotas)(cuentas_moratoria_sipres(index)) = Math.Round(dt_global.Rows(indice - nroCuotas)("TOTAL") * montosPorcentajesMoratoria(index - 3), 2)
				Next

				nroCuotas -= 1
			End While
		End If

	End Sub

	Private Sub procesaAportes(DSPorcentajes As DataSet, dt As DataTable, i As Integer, proceso As String)
		Try
			If DSPorcentajes.Tables("porcentajes").Columns.Count < 1 Then
				Exit Sub
			Else
                If proceso = "01D131" Or proceso = "01D132" Or proceso = "01D133" Or proceso = "01D134" Then
                    Dim cper As String

                    dt.Rows(i)(cuentas_deuda_aportes(1)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(2)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(3)) = dt.Rows(i)("INTERES")
                    dt.Rows(i)(cuentas_deuda_aportes(4)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(5)) = dt.Rows(i)("IMP_COMP")
                    cper = DSPorcentajes.Tables("porcentajes").Rows(5).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(6)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(5)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(6).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(7)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(5)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(7).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(8)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(5)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(8).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(9)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(5)) * cper, 2)
                    dt.Rows(i)(cuentas_deuda_aportes(10)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(11)) = "0"
                ElseIf proceso = "01D148" Or proceso = "01D149" Or proceso = "01D150" Or proceso = "01D151" Or proceso = "01D152" Then
                    Dim cper As String

                    dt.Rows(i)(cuentas_deuda_aportes(1)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(2)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(3)) = dt.Rows(i)("INTERES")
                    dt.Rows(i)(cuentas_deuda_aportes(4)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(5)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(6)) = dt.Rows(i)(cuentas_deuda_aportes(4))
                    cper = DSPorcentajes.Tables("porcentajes").Rows(6).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(7)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(7).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(8)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(8).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(9)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(9).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(10)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    dt.Rows(i)(cuentas_deuda_aportes(11)) = "0"
                ElseIf proceso = "01D153" Or proceso = "01D154" Or proceso = "01D155" Or proceso = "01D156" Or proceso = "01D157" Then
                    Dim cper As String

                    dt.Rows(i)(cuentas_deuda_aportes(1)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(2)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(3)) = dt.Rows(i)("INTERES")
                    dt.Rows(i)(cuentas_deuda_aportes(4)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(5)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(6)) = dt.Rows(i)(cuentas_deuda_aportes(4))
                    cper = DSPorcentajes.Tables("porcentajes").Rows(6).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(7)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(7).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(8)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(8).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(9)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(9).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(10)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    dt.Rows(i)(cuentas_deuda_aportes(11)) = "0"
                ElseIf proceso = "01D158" Or proceso = "01D159" Or proceso = "01D160" Or proceso = "01D161" Or proceso = "01D162" Then
                    Dim cper As String

                    dt.Rows(i)(cuentas_deuda_aportes(1)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(2)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(3)) = dt.Rows(i)("INTERES")
                    dt.Rows(i)(cuentas_deuda_aportes(4)) = dt.Rows(i)("IMP_COMP")
                    dt.Rows(i)(cuentas_deuda_aportes(5)) = "0"
                    dt.Rows(i)(cuentas_deuda_aportes(6)) = dt.Rows(i)(cuentas_deuda_aportes(4))
                    cper = DSPorcentajes.Tables("porcentajes").Rows(6).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(7)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(7).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(8)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(8).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(9)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    cper = DSPorcentajes.Tables("porcentajes").Rows(9).Item("pto_formula").Remove(0, 7)
                    cper = cper.Replace(".", ",")
                    dt.Rows(i)(cuentas_deuda_aportes(10)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
                    dt.Rows(i)(cuentas_deuda_aportes(11)) = "0"
                Else
                    Dim cper As String

					dt.Rows(i)(cuentas_deuda_aportes(1)) = dt.Rows(i)("IMP_COMP")
					dt.Rows(i)(cuentas_deuda_aportes(2)) = "0"
					dt.Rows(i)(cuentas_deuda_aportes(3)) = dt.Rows(i)("INTERES")
					dt.Rows(i)(cuentas_deuda_aportes(5)) = "0"
					dt.Rows(i)(cuentas_deuda_aportes(4)) = dt.Rows(i)("IMP_COMP")
					dt.Rows(i)(cuentas_deuda_aportes(6)) = dt.Rows(i)("IMP_COMP")
					cper = DSPorcentajes.Tables("porcentajes").Rows(6).Item("pto_formula").Remove(0, 7)
					cper = cper.Replace(".", ",")
					dt.Rows(i)(cuentas_deuda_aportes(7)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
					cper = DSPorcentajes.Tables("porcentajes").Rows(7).Item("pto_formula").Remove(0, 7)
					cper = cper.Replace(".", ",")
					dt.Rows(i)(cuentas_deuda_aportes(8)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
					cper = DSPorcentajes.Tables("porcentajes").Rows(8).Item("pto_formula").Remove(0, 7)
					cper = cper.Replace(".", ",")
					dt.Rows(i)(cuentas_deuda_aportes(9)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
					cper = DSPorcentajes.Tables("porcentajes").Rows(9).Item("pto_formula").Remove(0, 7)
					cper = cper.Replace(".", ",")
					dt.Rows(i)(cuentas_deuda_aportes(10)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
					cper = DSPorcentajes.Tables("porcentajes").Rows(10).Item("pto_formula").Remove(0, 7)
					cper = cper.Replace(".", ",")
					dt.Rows(i)(cuentas_deuda_aportes(11)) = Math.Round(dt.Rows(i)(cuentas_deuda_aportes(6)) * cper, 2)
				End If
			End If
		Catch ex As Exception
			MsgBox("El Matriculado " + dt.Rows(i)("DENOMINAC") + ", CP" + dt.Rows(i)("MATRICULA") + " no pertenece a una categoria activa para débitos automáticos.")
			Exit Sub
		End Try
	End Sub

	Public Sub QuitarInteres(ByRef dt As DataTable, cuenta As String)
		For Each row As DataRow In dt.Rows
			row.Item(cuenta) = 0
		Next
	End Sub

End Class
