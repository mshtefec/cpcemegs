﻿Public Class FrmDebitosAutomaticos
	'BD
	Private consultaBD As New ConsultaBD(True)
	'UTIL
	Private sipresUtils As New SipresUtils
	Private importar As New Importar
	Private apiRest As New Rest
	'GLOBALES
	Private dt_global As New DataTable

	Private Sub FrmDebitosAutomaticos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

	End Sub

	Private Sub ImportarClick(sender As Object, e As EventArgs) Handles btn_importar.Click
		Select Case btn_importar.Text
			Case "Importar"
				Carga()
			Case "Enviar"
				Enviar()
		End Select
	End Sub

	Private Sub Carga()
		Dim openFile As New OpenFileDialog()
		Dim ruta As String
		Dim hoja As String
		Dim fechaConcepto As DateTime
		Dim fechaComprobante As DateTime

		openFile.Filter = "Excel files (*.xlsx)|*.xlsx|CSV Files (*.csv)|*.csv|XLS Files (*.xls)|*xls"
		openFile.FilterIndex = 1
		openFile.Multiselect = False

		Dim clickedOK As Boolean = openFile.ShowDialog()

		If (clickedOK = True) Then
			ruta = openFile.FileName.ToString()

			Dim startindex As Integer = ruta.LastIndexOf("\")
			Dim nombre As String = ruta.Substring(startindex + 1)
			startindex = nombre.LastIndexOf(".")
			nombre = nombre.Substring(0, startindex)

			hoja = nombre

			Try
				dt_global = sipresUtils.CargarDebitos(ruta, hoja)

				lb_file_fecha.Text = Now.ToString("dd/MM/yyyy")
				lb_file_estado.Text = "cargado"
				'Set titulo de archivo y estado
				If cbox_fecha_concepto.Checked Then
					fechaConcepto = Convert.ToDateTime(dtime_concepto.Value)
				Else
					fechaConcepto = Now
				End If

				If cbox_fecha_comprobante.Checked Then
					fechaComprobante = Convert.ToDateTime(dtime_comprobante.Value)
				Else
					fechaComprobante = Now
				End If

				sipresUtils.ProsArchivo(lb_file_title, dt_global.Rows(0)("SERVICIO"), dt_global, TabControlDebitos, fechaConcepto, fechaComprobante)
				lb_file_estado.Text = "procesado"

				dgv_listado.DataSource = dt_global
				dgv_listado.AutoResizeColumns()
				dgv_listado.Dock = DockStyle.Fill
				dgv_listado.ColumnHeadersDefaultCellStyle.ForeColor = Color.White 'Letras
				dgv_listado.ColumnHeadersDefaultCellStyle.Font = New Font("Roboto", 8, FontStyle.Bold)
				dgv_listado.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#323232") 'Fondo de Header
				dgv_listado.RowHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#7f7f7f") 'Selector
				dgv_listado.RowsDefaultCellStyle.Font = New Font("Roboto", 8)

				btn_importar.Text = "Enviar"

				Me.DisableControlsFechas()
			Catch

			End Try
		End If
	End Sub

	Private Sub DisableControlsFechas()
		For Each control In gb_details.Controls
			If TypeOf control Is CheckBox Or TypeOf control Is DateTimePicker Then
				control.Enabled = False
			End If
		Next
	End Sub

	Private Sub Enviar()

		If Not sipresUtils.CheckArchivo(lb_file_title.Text) Then Exit Sub

		Dim answer As Integer
		answer = MsgBox("Seguro desea generar los Comprobantes y Asientos?", vbYesNo + vbQuestion, "Generando")

		If answer = vbYes Then
			Dim zeta As Double
			Dim caja As Double
			Dim fechaComp As String

			If consultaBD.CajaActiva Then
				zeta = consultaBD.Zeta
				caja = consultaBD.Caja

				If cbox_fecha_comprobante.Checked Then
					fechaComp = dtime_comprobante.Value.ToString("yyyy/MM/dd")
					fechaComp = fechaComp.Replace("/", "")
				Else
					fechaComp = "-"
				End If

				Try
					Dim json As String = importar.generaJson(dt_global, "debitos_json", lb_file_title.Text, fechaComp)
					'Interaction.InputBox("JSON!, :D", "Copie el contenido", json)
					apiRest.sendDebito(json)

					btn_importar.Enabled = False
					lb_file_estado.Text = "enviado"

				Catch ex As Exception
					MsgBox("Ups!, algo no salio como se esperaba", MsgBoxStyle.Critical, "Error")
				End Try
			Else
				MsgBox("Verifique si la caja es actual y se encuentra operable.", MsgBoxStyle.Critical, "Error")
			End If
		End If
	End Sub

End Class