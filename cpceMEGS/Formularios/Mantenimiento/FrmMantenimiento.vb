﻿Imports System.IO
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MySql.Data.MySqlClient

Public Class FrmMantenimiento
    Private cnnMante As New ConsultaBD(True)
    Private DAFarcat As MySqlDataAdapter
    Private BSFarcat As BindingSource
    Private DTFarcat As DataTable
    Private DSCategoria As DataSet
    Private DACategoria As MySqlDataAdapter
    Private bindingCate As BindingSource
    Private DSTitulos As DataSet
    Private DATitulos As MySqlDataAdapter
    Private bindingTitu As BindingSource
    Private DTTrabajo As DataTable
    Private BSTrabajo As BindingSource
    Private DTProfesionales As DataTable
    Private BSProfesionales As BindingSource
    Private DTComitente As DataTable
    Private BSComitente As BindingSource
    Private lAltaObleas As Boolean
    Private DTObleas As DataTable
    Private DAObleas As MySqlDataAdapter
    Private cmdObleas As MySqlCommandBuilder
    Private WithEvents BSObleas As BindingSource
    Private DSCuentas As DataSet
    Private DAcuentas As MySqlDataAdapter
    Private cmdCuentas As MySqlCommandBuilder
    Private BSCuentas As BindingSource
    Private DSProceso As DataSet
    Private DAProceso As MySqlDataAdapter
    Private cmdProceso As MySqlCommandBuilder
    Private BSProceso As BindingSource
    Private DAProcTot As MySqlDataAdapter
    Private cmdProcTot As MySqlCommandBuilder
    Private BSProcTot As BindingSource
    'Operador
    Private DSOperador As DataSet
    Private DAOperador As MySqlDataAdapter
    Private BSOperador As BindingSource
    'ObraSocial
    Private DAObraSocial As MySqlDataAdapter
    Private DTObraSocial As DataTable
    Private DSObraSocial As DataSet
    Private BSObraSocial As BindingSource
    'Asientos
    Private DTAsientosDesbalanceados As DataTable
    '---
    Private DSHonorarios As DataSet
    Private DAHonorarios As MySqlDataAdapter
    Private cmdHonorarios As MySqlCommandBuilder
    Private DTRecibos As DataTable
    Private DARecibos As MySqlDataAdapter
    Private BSRecibos As BindingSource
    Private cmdRecibos As MySqlCommandBuilder
    Private DT As DataTable
    Private DTDevenga As DataTable
    Private DADevenga As MySqlDataAdapter
    Dim DTDevAsi As DataTable
    Dim xC As Integer
    Private CCopias As Integer = 0
    Private Sub MuestraCategoria()
        DACategoria = cnnMante.consultaBDadapter("categorias")
        DSCategoria = New DataSet
        DACategoria.Fill(DSCategoria, "categoria")
        bindingCate = New BindingSource
        bindingCate.DataSource = DSCategoria.Tables(0)
        UGCategorias.DataSource = bindingCate
        With UGCategorias.DisplayLayout.Bands(0)
            .Columns(0).Width = 60
            .Columns(0).Header.Caption = "Codigo"
            .Columns(1).Width = 300
            .Columns(1).Header.Caption = "Descripcion"
        End With

        txcodigo.DataBindings.Clear()
        txcodigo.DataBindings.Add(New Binding("Text", bindingCate, "cat_codigo", True))
        txdescripcion.DataBindings.Clear()
        txdescripcion.DataBindings.Add(New Binding("Text", bindingCate, "cat_descrip", True))
        TxDevenga1.DataBindings.Clear()
        TxDevenga1.DataBindings.Add(New Binding("Text", bindingCate, "cat_devenga1", True))
        TxDevenga2.DataBindings.Clear()
        TxDevenga2.DataBindings.Add(New Binding("Text", bindingCate, "cat_devenga2", True))
        TxRecibo1.DataBindings.Clear()
        TxRecibo1.DataBindings.Add(New Binding("Text", bindingCate, "cat_recibo1", True))
        TxRecibo2.DataBindings.Clear()
        TxRecibo2.DataBindings.Add(New Binding("Text", bindingCate, "cat_recibo2", True))
    End Sub

    Private Sub MuestraPlancuenta()
        Dim val As New ValueList
        val.ValueListItems.Add("Si", "Si")
        val.ValueListItems.Add("No", "No")

        EditarCuenta(True)
        DAcuentas = cnnMante.consultaBDadapter(
            "plancuen",
            "pla_nropla,pla_nombre,pla_subcta,pla_madre,pla_imputa,pla_servicio,pla_caja,pla_cajachica,pla_cuotas,pla_debito,pla_busca_matricula"
        )
        cmdCuentas = New MySqlCommandBuilder(DAcuentas)
        DSCuentas = New DataSet
        DAcuentas.Fill(DSCuentas, "cuentas")
        BSCuentas = New BindingSource
        BSCuentas.DataSource = DSCuentas.Tables(0)
        UGplancuen.DataSource = BSCuentas
        With UGplancuen.DisplayLayout.Bands(0)
            .Columns(0).Header.Caption = "Cuenta"
            .Columns(0).Width = 100
            .Columns(0).CellAppearance.TextHAlign = HAlign.Center
            .Columns(1).Header.Caption = "Descripcion"
            .Columns(1).Width = 300
            .Columns(2).Header.Caption = "Subcuenta"
            .Columns(3).Header.Caption = "Cuenta Madre"
            .Columns(4).Header.Caption = "Imputa"
            .Columns(5).Header.Caption = "Servicio"
            .Columns(6).Header.Caption = "Caja"
            .Columns(7).Header.Caption = "Caja Chica"
            .Columns(8).Header.Caption = "Cuotas"
            .Columns(9).Header.Caption = "Debito"
            .Columns(9).Header.Caption = "Busca Matricula"
        End With
        txcuenta.DataBindings.Clear()
        txcuenta.DataBindings.Add(New Binding("Text", BSCuentas, "pla_nropla", True))
        txDescripCuenta.DataBindings.Clear()
        txDescripCuenta.DataBindings.Add(New Binding("Text", BSCuentas, "pla_nombre", True))
        txSubcuenta.DataBindings.Clear()
        txSubcuenta.DataBindings.Add(New Binding("Text", BSCuentas, "pla_subcta", True))
        txmadre.DataBindings.Clear()
        txmadre.DataBindings.Add(New Binding("Text", BSCuentas, "pla_madre", True))
        cboImputa.DataBindings.Clear()
        cboImputa.DataBindings.Add(New Binding("Text", BSCuentas, "pla_imputa", True))
        cboServicio.DataBindings.Clear()
        cboServicio.DataBindings.Add(New Binding("Text", BSCuentas, "pla_servicio", True))
        CboCajaGeneral.DataBindings.Clear()
        CboCajaGeneral.DataBindings.Add(New Binding("Text", BSCuentas, "pla_caja", True))
        CboCajaChica.DataBindings.Clear()
        CboCajaChica.DataBindings.Add(New Binding("Text", BSCuentas, "pla_cajachica", True))
        cboCuotas.DataBindings.Clear()
        cboCuotas.DataBindings.Add(New Binding("Text", BSCuentas, "pla_cuotas", True))
        CBDebito.DataBindings.Clear()
        CBDebito.DataBindings.Add(New Binding("Text", BSCuentas, "pla_debito", True))
        CBBuscaMatricula.DataBindings.Clear()
        CBBuscaMatricula.DataBindings.Add(New Binding("Text", BSCuentas, "pla_busca_matricula", True))
    End Sub

    Private Sub BuscaProfecional()
        DAObleas = cnnMante.consultaBDadapter("afiliado", "afi_nombre,afi_tipdoc,afi_nrodoc,afi_titulo,afi_matricula", "afi_titulo='" & UTEMtra.Text.Substring(0, 2) & "' and afi_matricula=" & UTEMtra.Text.Substring(2) & " order by afi_nombre")
        DTProfesionales = New DataTable
        DAObleas.Fill(DTProfesionales)
        BSProfesionales.DataSource = DTProfesionales
    End Sub

    Private Sub MuestraOperadores()
        DAOperador = cnnMante.consultaBDadapter("operador", "ope_nroope as Nro,ope_nrodoc as Documento,ope_client as Personal,ope_nombre as Username,ope_clave as Clave,ope_nivel as Nivel")
        Dim cmdOperador As New MySqlCommandBuilder(DAOperador)

        DSOperador = New DataSet
        DAOperador.Fill(DSOperador, "operador")
        BSOperador = New BindingSource
        BSOperador.DataSource = DSOperador.Tables(0)

        UGOperador.DataSource = BSOperador
        With UGOperador.DisplayLayout.Bands(0)
            .Override.FilterUIType = FilterUIType.FilterRow
        End With
    End Sub

    Private Sub MuestraPermisos()
        'TODO: esta línea de código carga datos en la tabla 'DSPermiso.permiso_tipo' Puede moverla o quitarla según sea necesario.
        Permiso_tipoTableAdapter.Fill(DSPermiso.permiso_tipo)
        'TODO: esta línea de código carga datos en la tabla 'DSPermiso.accion' Puede moverla o quitarla según sea necesario.
        AccionTableAdapter.Fill(DSPermiso.accion)
        'TODO: esta línea de código carga datos en la tabla 'DSPermiso.modulo' Puede moverla o quitarla según sea necesario.
        ModuloTableAdapter.Fill(DSPermiso.modulo)
        'TODO: esta línea de código carga datos en la tabla 'DSPermiso.operador' Puede moverla o quitarla según sea necesario.
        OperadorTableAdapter.Fill(DSPermiso.operador)
        'TODO: esta línea de código carga datos en la tabla 'DSPermiso.permiso' Puede moverla o quitarla según sea necesario.
        PermisoTableAdapter.Fill(DSPermiso.permiso)
    End Sub

    Private Sub MuestraObraSociales()
        DAObraSocial = cnnMante.consultaBDadapter("obrasocial", "obr_nombre as Nombre, obr_activo as Activo")
        DTObraSocial = New DataTable
        DAObraSocial.Fill(DTObraSocial)

        DGVObraSocial.DataSource = DTObraSocial
    End Sub

    Private Sub MuestraProcesos()
        Dim val As New ValueList
        Dim valCol As New ValueList

        val.ValueListItems.Add("Si", "Si")
        val.ValueListItems.Add("No", "No")
        val.ValueListItems.Add("SN", "Ambos")

        valCol.ValueListItems.Add("H", "Haber")
        valCol.ValueListItems.Add("D", "Debe")

        DAProceso = cnnMante.consultaBDadapter("procesos", "pro_codigo,pro_nombre,pro_buscaf,pro_bloqfech,pro_cajachica,pro_cpto1,pro_cpto2,pro_cpto3,pro_procancela,pro_reporte1,pro_reporte2,pro_reporte3,pro_reporte4,pro_numerador,pro_instit,pro_catexcl,pro_destinatario")
        cmdProceso = New MySqlCommandBuilder(DAProceso)
        DSProceso = New DataSet
        DAProceso.Fill(DSProceso, "procesos")
        DSProceso.Tables("procesos").Columns.Add("Agregar", Type.GetType("System.String"))

        BSProceso = New BindingSource
        BSProcTot = New BindingSource

        DAProcTot = cnnMante.consultaBDadapter("procetote inner join plancuen on pla_nropla=pto_nropla", "pto_codpro,pto_item,pto_nropla,pla_nombre,pto_tipmov,pto_valor,pto_formula")
        '   cmdProcTot = New MySqlCommandBuilder(DAProcTot)

        DAProcTot.Fill(DSProceso, "proceplan")
        DSProceso.Tables("proceplan").Columns.Add("Eliminar", Type.GetType("System.String"))
        '    BSProcTot.DataSource = DSProceso.Tables("proceplan")

        DAProcTot = cnnMante.consultaBDadapter("procetote")
        cmdProcTot = New MySqlCommandBuilder(DAProcTot)

        DAProcTot.Fill(DSProceso, "procetote")
        Dim columns(1) As DataColumn
        columns(0) = DSProceso.Tables("procetote").Columns("pto_codpro")
        columns(1) = DSProceso.Tables("procetote").Columns("pto_item")
        DSProceso.Tables("procetote").PrimaryKey = columns

        DSProceso.Relations.Add("proceso_detalles", DSProceso.Tables("procesos").Columns("pro_codigo"), DSProceso.Tables("proceplan").Columns("pto_codpro"), False)

        BSProceso.DataSource = DSProceso
        BSProceso.DataMember = "procesos"

        BSProcTot.DataSource = BSProceso
        BSProcTot.DataMember = "proceso_detalles"

        UGProcesos.DataSource = BSProceso
        With UGProcesos.DisplayLayout.Bands(0)
            .Override.FilterUIType = FilterUIType.FilterRow
            .Columns(0).Width = 70
            .Columns(0).Header.Caption = "Codigo"
            .Columns(0).CellActivation = Activation.NoEdit
            .Columns(1).Width = 300
            .Columns(1).Header.Caption = "Descripcion"
            '   .Columns(1).CellActivation = Activation.NoEdit
            .Columns(2).Header.Caption = "BuscAf"
            .Columns(2).Width = 70
            .Columns(2).Style = ColumnStyle.DropDownList
            .Columns(2).ValueList = val
            '   .Columns(2).CellActivation = Activation.NoEdit
            .Columns(3).Header.Caption = "BloqFech"
            .Columns(3).Width = 70
            .Columns(3).Style = ColumnStyle.DropDownList
            .Columns(3).ValueList = val
            .Columns(4).Header.Caption = "CajaChica"
            .Columns(4).Width = 70
            .Columns(4).Style = ColumnStyle.DropDownList
            .Columns(4).ValueList = val
            .Columns(5).Header.Caption = "ProCancela"
            .Columns(9).Header.Caption = "Reporte Rcia"
            .Columns(10).Header.Caption = "Reporte SP"
            .Columns(11).Header.Caption = "Reporte VA"
            .Columns(12).Header.Caption = "Reporte LB"
            .Columns(16).Header.Caption = "PermiteDestinatario"
            .Columns(16).Style = ColumnStyle.DropDownList
            .Columns(16).ValueList = val
            Dim column As UltraGridColumn = UGProcesos.DisplayLayout.Bands(0).Columns("Agregar")

            column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
            column.Width = 50
            .Columns("Agregar").Header.Caption = ""
            .Columns("Agregar").Style = ColumnStyle.Button
            .Columns("Agregar").CellButtonAppearance.Image = ImageList1.Images(0)
            .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center

        End With

        With UGProcesos.DisplayLayout.Bands(1)
            .Override.FilterUIType = FilterUIType.Default
            .Columns(0).Hidden = True
            .Columns(1).Width = 20
            .Columns(1).Header.Caption = "Item"
            .Columns(1).CellActivation = Activation.NoEdit
            .Columns(2).Width = 90
            .Columns(2).Header.Caption = "Cuenta"
            .Columns(2).CellActivation = Activation.NoEdit
            .Columns(3).Width = 300
            .Columns(3).Header.Caption = "Descripcion"
            .Columns(3).CellActivation = Activation.NoEdit
            .Columns(4).Width = 70
            .Columns(4).Header.Caption = "Col"
            '.Columns(4).CellActivation = Activation.NoEdit
            .Columns(4).Style = ColumnStyle.DropDownList
            .Columns(4).ValueList = valCol
            '.Columns(5).Width = 20
            .Columns(5).Header.Caption = "Valor"
            '.Columns(5).CellActivation = Activation.NoEdit
            .Columns(6).Width = 150
            .Columns(6).Header.Caption = "Formula"
            .Columns(6).CellActivation = Activation.AllowEdit

            Dim column As UltraGridColumn = UGProcesos.DisplayLayout.Bands(1).Columns("Eliminar")

            column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
            column.Width = 50
            .Columns("Eliminar").Header.Caption = ""
            .Columns("Eliminar").Style = ColumnStyle.Button
            .Columns("Eliminar").CellButtonAppearance.Image = ImageList1.Images(2)
            .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

            .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(1).Header.Appearance.ForeColor = Color.Black
            .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(1).Header.Appearance.BackColor2 = Color.White
            .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(2).Header.Appearance.ForeColor = Color.Black
            .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(2).Header.Appearance.BackColor2 = Color.White
            .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(3).Header.Appearance.ForeColor = Color.Black
            .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(3).Header.Appearance.BackColor2 = Color.White
            .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(4).Header.Appearance.ForeColor = Color.Black
            .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(4).Header.Appearance.BackColor2 = Color.White
            .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(5).Header.Appearance.ForeColor = Color.Black
            .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(5).Header.Appearance.BackColor2 = Color.White
            .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(6).Header.Appearance.ForeColor = Color.Black
            .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(6).Header.Appearance.BackColor2 = Color.White
            .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            .Columns(7).Header.Appearance.ForeColor = Color.Black
            .Columns(7).Header.Appearance.BackColor = Color.BlueViolet
            .Columns(7).Header.Appearance.BackColor2 = Color.White
        End With
    End Sub

    Private Sub MuestraTitulo()
        DATitulos = cnnMante.consultaBDadapter("titulos", "tit_idtitulo,tit_matricula as NroMatricula,tit_descripcion as Descripcion")
        DSTitulos = New DataSet
        DATitulos.Fill(DSTitulos, "titulos")
        bindingTitu = New BindingSource
        bindingTitu.DataSource = DSTitulos.Tables(0)
        With dgvTitulos
            .DataSource = bindingTitu
            .Columns(0).Width = 60
            .Columns(0).HeaderText = "Codigo"
            .Columns(2).Width = 300
            .Columns(2).HeaderText = "Descripcion"
        End With
        ' txcodigo.DataBindings.Clear()
        ' txdescripcion.DataBindings.Clear()
        ' txcodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", bindingCate, "cat_codigo", True))
        'txdescripcion.DataBindings.Add(New System.Windows.Forms.Binding("Text", bindingCate, "cat_descrip", True))
    End Sub

    Private Sub UltraTabControl1_SelectedTabChanged(ByVal sender As Object, ByVal e As UltraWinTabControl.SelectedTabChangedEventArgs) Handles UltraTabControl1.SelectedTabChanged
        Select Case e.Tab.Index
            Case 0
                UDTHasta.Value = Date.Today
                UDTDesde.Value = Date.Today.AddDays(-7)
            Case 1
                MuestraCategoria()
            Case 2
                MuestraPlancuenta()
            Case 3
                MuestraProcesos()
            Case 4
                MuestroHonararios()
            Case 5
                MuestraTitulo()
            Case 7
                CargaDelegacion()
            Case 11
                MuestraObraSociales()
        End Select
    End Sub

    Private Sub EditarCuenta(ByVal lEditar As Boolean, Optional ByVal lAlta As Boolean = False)
        If lAlta Then
            txcuenta.Enabled = lAlta
        Else
            txcuenta.Enabled = lAlta
        End If
        txDescripCuenta.Enabled = lEditar
        txSubcuenta.Enabled = lEditar
        txmadre.Enabled = lEditar
    End Sub

    Private Sub txcuenta_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txcuenta.KeyPress
        Tabular(e)
    End Sub

    Private Sub txmadre_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txmadre.KeyPress
        Tabular(e)
    End Sub

    Private Sub txNomcorto_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txSubcuenta.KeyPress
        Tabular(e)
    End Sub

    Private Sub txDescrip_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txDescripCuenta.KeyPress
        Tabular(e)
    End Sub

    Private Sub CButton2_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton2.Click
        If CButton2.Text = "Alta" Then
            CButton2.Text = "Cancelar"
            BSCuentas.AddNew()
            BSCuentas.MoveLast()
            EditarCuenta(True, True)
            txcuenta.Focus()
        Else
            CButton2.Text = "Alta"
            BSCuentas.CancelEdit()
        End If
    End Sub

    Private Sub CButton3_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton3.Click
        Try
            If IsNumeric(txcuenta.Text) Then
                BSCuentas.EndEdit()
                DAcuentas.Update(DSCuentas, "cuentas")
                CButton2.Text = "Alta"
            Else
                MessageBox.Show("La cuenta tiene formato incorrecto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txcuenta.Focus()
            End If
        Catch ex As MySqlException

            If ex.Number = 1062 Then ' clave duplicada
                MessageBox.Show("Clave duplicada ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub MuestroHonararios()
        DAHonorarios = cnnMante.consultaBDadapter("thcalcac")
        cmdHonorarios = New MySqlCommandBuilder(DAHonorarios)
        DSHonorarios = New DataSet
        DAHonorarios.Fill(DSHonorarios, "thcalcac")
        With dgvTHAC
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "N2"
            .DataSource = DSHonorarios.Tables("thcalcac")
            .Columns(0).Width = 130
            .Columns(0).ReadOnly = True
            .Columns(0).HeaderText = "MontoDesde"
            .Columns(1).Width = 130
            .Columns(1).ReadOnly = True
            .Columns(1).HeaderText = "MontoHasta"
            .Columns(2).HeaderText = "Fijo"
            .Columns(3).DefaultCellStyle.Format = "N3"
            .Columns(3).HeaderText = "Porcentaje"
            .Columns(4).HeaderText = "Excedente"
        End With

        DAHonorarios = cnnMante.consultaBDadapter("thcaljud")

        DAHonorarios.Fill(DSHonorarios, "thcaljud")
        With dgvTHJUD
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "N2"
            .DataSource = DSHonorarios.Tables("thcaljud")
            .Columns(0).Width = 130
            .Columns(0).HeaderText = "MontoDesde"
            .Columns(1).Width = 130
            .Columns(1).HeaderText = "MontoHasta"
            .Columns(2).HeaderText = "Fijo"
            .Columns(3).DefaultCellStyle.Format = "N3"
            .Columns(3).HeaderText = "Porcentaje"
            .Columns(4).HeaderText = "Excedente"
        End With

        DAHonorarios = cnnMante.consultaBDadapter("thcalact")

        DAHonorarios.Fill(DSHonorarios, "thcalact")
        With dgvTHACT
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .DefaultCellStyle.Format = "N2"
            .DataSource = DSHonorarios.Tables("thcalact")
            .Columns(0).Width = 130
            .Columns(0).HeaderText = "MontoDesde"
            .Columns(1).Width = 130
            .Columns(1).HeaderText = "MontoHasta"
            .Columns(2).HeaderText = "Fijo"
            .Columns(3).DefaultCellStyle.Format = "N3"
            .Columns(3).HeaderText = "Porcentaje"
            .Columns(4).HeaderText = "Excedente"
        End With

        DAHonorarios = cnnMante.consultaBDadapter("tareas")

        DAHonorarios.Fill(DSHonorarios, "tareas")

        With dgvTareas
            .DataSource = DSHonorarios.Tables("tareas")
            .Columns(0).Width = 30
            .Columns(0).HeaderText = "Codigo"
            .Columns(1).Width = 30
            .Columns(1).HeaderText = "Orden"
            .Columns(2).Width = 290
            .Columns(2).HeaderText = "Descripcion"
            .Columns(3).Visible = False
            .Columns(4).Visible = False
            .Columns(5).HeaderText = "Imputa"
            .Columns(5).Width = 70
            .Columns(6).HeaderText = "Minima"
            .Columns(6).DefaultCellStyle.Format = "N2"
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(6).Width = 80
            .Columns(7).HeaderText = "Porcentaje"
            .Columns(7).DefaultCellStyle.Format = "N2"
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(7).Width = 80
        End With
    End Sub

    Private Sub CBtAltaProceso_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtAltaProceso.Click
        Try
            FrmAltaProcesos.ShowDialog()
            If FrmAltaProcesos.DialogResult = DialogResult.OK Then
                If FrmAltaProcesos.GetCodigo <> "" And FrmAltaProcesos.GetDescripcion <> "" Then
                    Dim rowAlta As DataRow = DSProceso.Tables("procesos").NewRow()
                    rowAlta.Item("pro_codigo") = FrmAltaProcesos.GetCodigo
                    rowAlta.Item("pro_nombre") = FrmAltaProcesos.GetDescripcion
                    DSProceso.Tables("procesos").Rows.Add(rowAlta)
                    BSProceso.Position = DSProceso.Tables("procesos").Rows.Count
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'ImportoProceso()
    End Sub

    'Private Sub ImportoProceso()
    '    Dim cmdPro As MySqlCommand
    '    Dim nValor As Double
    '    Dim cFormula As String
    '    If cnnMante.AbrirConexion Then
    '        For Each dwPro As DataRow In DSProceso.Tables(0).Rows
    '            Try
    '                For item As Integer = 1 To 20
    '                    '    If dwPro.Item("CTA" & item) = "" Then
    '                    '   Else
    '                    If item = 4 Then
    '                        If IsNumeric(dwPro.Item("GOR" & item)) Then
    '                            nValor = CDbl(dwPro.Item("GOR" & item))
    '                            cFormula = ""
    '                        Else
    '                            nValor = 0
    '                            cFormula = dwPro.Item("GOR" & item)
    '                        End If
    '                        cmdPro = New MySqlCommand("INSERT INTO procetote ( pto_codpro, pto_item , pto_nropla , pto_tipmov,pto_valor,pto_formula ) " &
    '                                    "VALUES ('" & dwPro.Item("pro_codigo") & "','" & item & "','" & dwPro.Item("CTA" & item) & "','" & IIf(dwPro.Item("COL" & item) = "1", "H", "D") & "','" & nValor & "','" & cFormula & "')", cnnMante.conexion)
    '                        cmdPro.ExecuteNonQuery()
    '                        cmdPro.Dispose()
    '                    Else
    '                        If IsNumeric(dwPro.Item("FOR" & item)) Then
    '                            nValor = CDbl(dwPro.Item("FOR" & item))
    '                            cFormula = ""
    '                        Else
    '                            nValor = 0
    '                            cFormula = dwPro.Item("FOR" & item)
    '                        End If

    '                        cmdPro = New MySqlCommand("INSERT INTO procetote ( pto_codpro, pto_item , pto_nropla , pto_tipmov,pto_valor,pto_formula ) " &
    '                                     "VALUES ('" & dwPro.Item("pro_codigo") & "','" & item & "','" & dwPro.Item("CTA" & item) & "','" & IIf(dwPro.Item("COL" & item) = "1", "H", "D") & "','" & nValor & "','" & cFormula & "')", cnnMante.conexion)
    '                        cmdPro.ExecuteNonQuery()
    '                        cmdPro.Dispose()
    '                    End If
    '                    '    End If
    '                Next
    '            Catch ex As Exception

    '            End Try
    '        Next
    '        cnnMante.CerrarConexion()
    '    End If
    'End Sub

    Private Sub UGProcesos_BeforeCellUpdate(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventArgs) Handles UGProcesos.BeforeCellUpdate
        If e.Cell.Band.Index = 1 Then
            Dim rowGrid As UltraGridRow = UGProcesos.ActiveRow
            Dim Keys(1) As String
            Keys(0) = rowGrid.Cells.Item(0).Text
            Keys(1) = rowGrid.Cells.Item(1).Text
            Dim RowMod As DataRow = DSProceso.Tables("procetote").Rows.Find(Keys)

            RowMod.Item(e.Cell.Column.ToString) = e.NewValue
        End If
    End Sub

    Private Sub UGProcesos_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGProcesos.ClickCellButton
        If e.Cell.Band.Index = 0 Then
            'If e.Cell.Column.Index = 6 Then (comento nose donde entraba con el 6)
            'El 14 es el button + del listado de procesos
            If e.Cell.Column.Index = 14 Then
                FrmAltaCuentaProceso.ShowDialog()
                If FrmAltaCuentaProceso.DialogResult = DialogResult.OK Then

                    Dim rowGrid As UltraGridRow = UGProcesos.ActiveRow
                    Dim dwItem() As DataRow = DSProceso.Tables("proceplan").Select("pto_codpro='" & rowGrid.Cells.Item(0).Text & "'")
                    Dim rowCtaPro As DataRow = DSProceso.Tables("proceplan").NewRow
                    rowCtaPro.Item("pto_codpro") = rowGrid.Cells.Item(0).Text
                    rowCtaPro.Item("pto_item") = dwItem.Length + 1
                    rowCtaPro.Item("pto_nropla") = FrmAltaCuentaProceso.UltraTextEditor1.Text
                    rowCtaPro.Item("pla_nombre") = FrmAltaCuentaProceso.UltraTextEditor2.Text
                    rowCtaPro.Item("pto_tipmov") = "H"
                    DSProceso.Tables("proceplan").Rows.Add(rowCtaPro)

                    rowCtaPro = DSProceso.Tables("procetote").NewRow
                    rowCtaPro.Item("pto_codpro") = rowGrid.Cells.Item(0).Text
                    rowCtaPro.Item("pto_item") = dwItem.Length + 1
                    rowCtaPro.Item("pto_nropla") = FrmAltaCuentaProceso.UltraTextEditor1.Text
                    rowCtaPro.Item("pto_tipmov") = "H"
                    DSProceso.Tables("procetote").Rows.Add(rowCtaPro)
                End If
            End If
        End If
    End Sub

    Private Sub UGProcesos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGProcesos.InitializeRow
        UGProcesos.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
    End Sub

    Private Sub CBtActualizar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtActualizar.Click
        Try
            If MessageBox.Show("Confirmar la actualizacion", "Procesos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                DAProceso.Update(DSProceso, "procesos")
                DAProcTot.Update(DSProceso, "procetote")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmMantenimiento_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        '1 Profesionales 2 Comitentes 3 Proveedores 4 Bancos 5 Empleados
        'Controlo permisos
        If controlAcceso(6, 1, , False) = False Then
            UTPCCategorias.Tab.Enabled = False
            UTPCCategorias.Tab.Visible = False
        End If
        If controlAcceso(6, 2, , False) = False Then
            UTPCPlanDeCuentas.Tab.Enabled = False
            UTPCPlanDeCuentas.Tab.Visible = False
        End If
        If controlAcceso(6, 3, , False) = False Then
            UTPCProcesosContables.Tab.Enabled = False
            UTPCProcesosContables.Tab.Visible = False
        End If
        If controlAcceso(6, 4, , False) = False Then
            UTPCTablasDeHonorarios.Tab.Enabled = False
            UTPCTablasDeHonorarios.Tab.Visible = False
        End If
        If controlAcceso(6, 5, , False) = False Then
            UTPCTitulos.Tab.Enabled = False
            UTPCTitulos.Tab.Visible = False
        End If
        If controlAcceso(6, 6, , False) = False Then
            UTPCGeneraciones.Tab.Enabled = False
            UTPCGeneraciones.Tab.Visible = False
        End If
        If controlAcceso(6, 7, , False) = False Then
            UTPCTransmisiones.Tab.Enabled = False
            UTPCTransmisiones.Tab.Visible = False
        End If
        If controlAcceso(6, 8, , False) = False Then
            UTPCDebitosAutomaticos.Tab.Enabled = False
            UTPCDebitosAutomaticos.Tab.Visible = False
        End If
        If controlAcceso(6, 9, , False) = False Then
            UTPCSistema.Tab.Enabled = False
            UTPCSistema.Tab.Visible = False
        End If
        If controlAcceso(6, 10, , False) = False Then
            UTPCAsientos.Tab.Enabled = False
            UTPCAsientos.Tab.Visible = False
        End If
        'Fin controlo permisos
        Show()
    End Sub

    Private Sub CBtDevCuoAsoc_Click(sender As Object, e As EventArgs) Handles CBtDevCuoAsoc.Click
        DevengamientoCuotaAsociados()
    End Sub

    Private Sub DevengamientoCuotaAsociados()
        Dim DTAsociado As New DataTable
        Dim DAAsociado As MySqlDataAdapter
        'Dim nEdad As Integer
        Dim nAnioGraduacion As Integer
        Dim dFecGraduacion As Date
        Dim cProcesoDevCuo As String
        Dim cCategoria As String
        Dim nItem As Integer = 0
        Dim mesesDesdeQueSeMatriculo As Long

        DAAsociado = cnnMante.consultaBDadapter(
            "afiliado INNER JOIN cuentas ON cue_tipdoc=afi_tipdoc AND cue_nrodoc=afi_nrodoc AND cue_titulo=afi_titulo", ,
            "afi_tipo = 'A' AND afi_titulo <> 'EC' AND afi_titulo <> 'ET' AND afi_matricula > 0 AND MID(afi_categoria,1,2) = '21' ORDER BY afi_matricula"
        )
        DAAsociado.Fill(DTAsociado)
        UltraProgressBar1.Maximum = DTAsociado.Rows.Count
        UltraProgressBar1.Value = 0
        UltraProgressBar1.Refresh()
        cnnMante.AbrirConexion()
        For Each rowAsoc As DataRow In DTAsociado.Rows
            Application.DoEvents()
            'nEdad = cnnMante.Edad(CDate(rowAsoc.Item("afi_fecnac").ToString))
            'nAnioGraduacion = DTPCuotasDevenga.Value.Year - dFecGraduacion.Year + 1
            'Si el afiliado NO tiene la categoria 210300 Asociado Incompatible. Entra y calcula el año de graduacion
            If rowAsoc.Item("afi_categoria") <> "210300" Then
                dFecGraduacion = CDate(rowAsoc.Item("cue_fecgraduacion").ToString)
                If IsDBNull(dFecGraduacion) Then
                    nAnioGraduacion = DTPCuotasDevenga.Value.Year
                Else
                    nAnioGraduacion = DTPCuotasDevenga.Value.Year - dFecGraduacion.Year + 1
                End If
                If nAnioGraduacion <= 3 Then
                    cCategoria = "210200"
                Else
                    cCategoria = "210100"
                End If

                If nAnioGraduacion <= 3 Then
                    cProcesoDevCuo = "02DA02"
                Else
                    cProcesoDevCuo = "02DA01"
                End If
            Else
                cCategoria = "210300"
                cProcesoDevCuo = "02DA03"
            End If

            If rowAsoc.Item("afi_categoria") <> cCategoria Then
                'Aca hace la magia y actualiza las cuentas subcuenta y categoria
                actualizoCategoriaCuentaSubcuenta(cCategoria, rowAsoc)
            End If
            UltraProgressBar1.Value += 1
            UltraProgressBar1.Refresh()
        Next

        DAAsociado = cnnMante.consultaBDadapter(
            "(afiliado INNER JOIN cuentas ON cue_tipdoc=afi_tipdoc AND cue_nrodoc=afi_nrodoc AND cue_titulo=afi_titulo) INNER JOIN categorias ON cat_codigo=afi_categoria", ,
            "afi_tipo = 'A' AND afi_titulo <> 'EC' AND afi_titulo <> 'ET' AND afi_matricula > 0 AND MID(afi_categoria,1,2) = '21' ORDER BY afi_categoria,afi_matricula"
        )
        DTAsociado = New DataTable
        DAAsociado.Fill(DTAsociado)
        UltraProgressBar1.Maximum = DTAsociado.Rows.Count
        UltraProgressBar1.Value = 0

        cProcesoDevCuo = ""
        DTDevAsi = New DataTable
        Dim nTotProceso As Double = 0
        Dim RowTotHaber As DataRow = Nothing
        Dim lCuotaPrimerAnio As Boolean = False
        For Each rowAsoc As DataRow In DTAsociado.Rows
            Application.DoEvents()
            If cProcesoDevCuo <> rowAsoc.Item("cat_devenga1") Then
                If cProcesoDevCuo <> "" Then
                    DTDevAsi.ImportRow(RowTotHaber)
                    RowTotHaber = Nothing
                End If
                ArmoAsientoDevengamiento(rowAsoc.Item("cat_devenga1"), DTPCuotasDevenga.Value, 3)
                cProcesoDevCuo = rowAsoc.Item("cat_devenga1")
            End If

            If DTDevAsi.Rows.Count = 0 Then
                DTDevAsi = DTDevenga.Clone
                'Dim columns(1) As DataColumn
                'columns(0) = DTDevAsi.Columns("proceso")
                'columns(1) = DTDevAsi.Columns("matricula")
                'DTDevAsi.PrimaryKey = columns
            End If

            'obtengo los meses que lleva de matriculado para no devengar hasta pasar los 12 meses
            mesesDesdeQueSeMatriculo = DateDiff(DateInterval.Month, CDate(rowAsoc.Item("cue_fecmatricula").ToString), DTPCuotasDevenga.Value)
            'If CDate(rowAsoc.Item("cue_fecmatricula").ToString).Year = DTPCuotasDevenga.Value.Year Then
            If mesesDesdeQueSeMatriculo < 13 Then
                lCuotaPrimerAnio = True
            Else
                lCuotaPrimerAnio = False
            End If

            For Each rowP As DataRow In DTDevenga.Rows
                nItem += 1
                If rowP.Item("pto_tipmov") = "D" Then
                    rowP.Item("pto_item") = nItem
                    If lCuotaPrimerAnio Then
                        rowP.Item("debe") = 5
                    Else
                        rowP.Item("debe") = rowP.Item("pto_valor")
                    End If
                    rowP.Item("subcuenta") = rowAsoc.Item("afi_tipdoc") & rowAsoc.Item("afi_nrodoc")
                    rowP.Item("matricula") = rowAsoc.Item("afi_titulo") & rowAsoc.Item("afi_matricula")
                    DTDevAsi.ImportRow(rowP)
                Else
                    If RowTotHaber Is Nothing Then
                        RowTotHaber = rowP
                        rowP.Item("pto_item") = nItem
                        If lCuotaPrimerAnio Then
                            rowP.Item("haber") = 5
                        Else
                            rowP.Item("haber") = rowP.Item("pto_valor")
                        End If
                    Else
                        RowTotHaber.Item("pto_item") = nItem
                        If lCuotaPrimerAnio Then
                            RowTotHaber.Item("haber") += 5
                        Else
                            RowTotHaber.Item("haber") += rowP.Item("pto_valor")
                        End If
                    End If
                End If
            Next
            UltraProgressBar1.Value += 1
            UltraProgressBar1.Refresh()
        Next
        If cProcesoDevCuo <> "" Then
            DTDevAsi.ImportRow(RowTotHaber)
            RowTotHaber = Nothing
        End If
        dgvCuoAsoc.DataSource = DTDevAsi

        CBtGraCueAsoc.Enabled = True
        cnnMante.CerrarConexion()
    End Sub

    Private Sub DevengamientoCuotaJubilados()
        Dim DTJubilado As New DataTable
        Dim DAJubilado As MySqlDataAdapter
        'Dim nEdad As Integer
        'Dim nAnioGraduacion As Integer
        'Dim dFecGraduacion As Date
        Dim cProcesoDevCuo As String
        Dim cCategoria As String = ""
        'Dim miTrans As MySqlTransaction
        Dim nItem As Integer = 0
        'UltraProgressBar1.Maximum = DTAsociado.Rows.Count
        'UltraProgressBar1.Value = 0
        'UltraProgressBar1.Refresh()
        'En el ORDER BY agrego cat_devenga1 porque de esa manera ordena correctamente
        'Segun montos de las categorias quedaria asi: 1: 170102 2y3: 01y03
        DAJubilado = cnnMante.consultaBDadapter(
            "afiliado INNER JOIN categorias ON cat_codigo=afi_categoria INNER JOIN cuentas ON cue_tipdoc=afi_tipdoc AND cue_nrodoc=afi_nrodoc AND cue_titulo=afi_titulo", ,
            "afi_tipo = 'A' 
            AND afi_titulo <> 'EC' 
            AND afi_titulo <> 'ET' 
            AND afi_matricula > 0 
            AND (afi_categoria='170101' 
            OR afi_categoria='170102' 
            OR afi_categoria='170103') 
            ORDER BY cat_devenga1,afi_categoria,afi_matricula"
        )

        DTJubilado = New DataTable
        DAJubilado.Fill(DTJubilado)
        UltraProgressBar1.Maximum = DTJubilado.Rows.Count
        UltraProgressBar1.Value = 0

        cProcesoDevCuo = ""
        DTDevAsi = New DataTable
        Dim nTotProceso As Double = 0
        Dim RowTotHaber As DataRow = Nothing
        For Each rowAsoc As DataRow In DTJubilado.Rows
            Application.DoEvents()
            If cProcesoDevCuo <> rowAsoc.Item("cat_devenga1") Then
                If cProcesoDevCuo <> "" Then
                    DTDevAsi.ImportRow(RowTotHaber)
                    RowTotHaber = Nothing
                End If
                ArmoAsientoDevengamiento(rowAsoc.Item("cat_devenga1"), DTPCuotasDevenga.Value, 4)
                cProcesoDevCuo = rowAsoc.Item("cat_devenga1")
            End If

            If DTDevAsi.Rows.Count = 0 Then
                DTDevAsi = DTDevenga.Clone
                'Dim columns(1) As DataColumn
                'columns(0) = DTDevAsi.Columns("proceso")
                'columns(1) = DTDevAsi.Columns("matricula")
                'DTDevAsi.PrimaryKey = columns
            End If

            For Each rowP As DataRow In DTDevenga.Rows
                nItem += 1
                If rowP.Item("pto_tipmov") = "D" Then
                    rowP.Item("pto_item") = nItem
                    rowP.Item("debe") = rowP.Item("pto_valor")
                    rowP.Item("subcuenta") = rowAsoc.Item("afi_tipdoc") & rowAsoc.Item("afi_nrodoc")
                    rowP.Item("matricula") = rowAsoc.Item("afi_titulo") & rowAsoc.Item("afi_matricula")
                    DTDevAsi.ImportRow(rowP)
                Else
                    If RowTotHaber Is Nothing Then
                        RowTotHaber = rowP

                        rowP.Item("pto_item") = nItem
                        rowP.Item("haber") = rowP.Item("pto_valor")
                    Else
                        RowTotHaber.Item("pto_item") = nItem
                        RowTotHaber.Item("haber") += rowP.Item("pto_valor")
                    End If
                End If
            Next
            UltraProgressBar1.Value += 1
            UltraProgressBar1.Refresh()
        Next
        If cProcesoDevCuo <> "" Then
            DTDevAsi.ImportRow(RowTotHaber)
            RowTotHaber = Nothing
        End If
        dgvCuoJub.DataSource = DTDevAsi

        CBtGraCuoJub.Enabled = True
    End Sub

    Private Sub DevengamientoCuotasAfiliado(ByVal nDevenga As Integer, ByVal dFecDevenga As Date, ByRef ProgressBar As UltraWinProgressBar.UltraProgressBar)
        Dim cCategoria As String
        Dim DTAfiliado As New DataTable
        Dim DAAfiliado As MySqlDataAdapter
        Dim nEdad As Integer
        Dim nAnioGraduacion As Integer
        Dim dFecGraduacion As Date
        Dim dFecMatricula As Date
        Dim dFecNacimiento As Date
        Dim nMesMatricula As Long 'guarda lo mismo que la variable de abajo
        Dim mesesDesdeQueSeMatriculo As Long 'se usan en diferentes foreach
        Dim categoriaNoCambia As String() = {"110105", "110106"} 'Categorias que no se actualizan
        ' filtro todos las categoria que el campo cat_recat es igual a S
        DAAfiliado = cnnMante.consultaBDadapter(
            "afiliado INNER JOIN cuentas ON cue_tipdoc=afi_tipdoc AND cue_nrodoc=afi_nrodoc AND cue_titulo=afi_titulo", ,
            "afi_tipo = 'A' AND afi_titulo <> 'EC' AND afi_titulo <> 'ET' AND afi_matricula > 0 AND MID(afi_categoria,1,2) IN ('11','12') ORDER BY afi_matricula"
        )
        DAAfiliado.Fill(DTAfiliado)
        ProgressBar.Maximum = DTAfiliado.Rows.Count
        ProgressBar.Value = 0
        ProgressBar.Refresh()
        cnnMante.AbrirConexion()
        For Each rowAsoc As DataRow In DTAfiliado.Rows
            ' contolo CPCE
            Application.DoEvents()
            Try
                dFecGraduacion = CDate(rowAsoc.Item("cue_fecgraduacion").ToString)
                dFecMatricula = CDate(rowAsoc.Item("cue_fecmatricula").ToString)
                dFecNacimiento = CDate(rowAsoc.Item("afi_fecnac").ToString)
            Catch ex As Exception
                MessageBox.Show("Controlar las fechas, matricula: " & rowAsoc.Item("afi_titulo") & rowAsoc.Item("afi_matricula"))
                cnnMante.CerrarConexion()
                Exit Sub
            End Try
            If IsDBNull(dFecGraduacion) Then
                nAnioGraduacion = DTPCuotasDevenga.Value.Year
            Else
                nAnioGraduacion = DTPCuotasDevenga.Value.Year - dFecGraduacion.Year + 1
            End If
            If nAnioGraduacion <= 3 Then
                cCategoria = "12"
            Else
                cCategoria = "11"
            End If
            ' controlo SIPRES
            nEdad = cnnMante.Edad(dFecNacimiento)
            nMesMatricula = DateDiff(DateInterval.Month, dFecMatricula, DTPCuotasDevenga.Value)
            If nEdad < 31 Then
                cCategoria += "0102"
            ElseIf nMesMatricula <= 36 Then
                cCategoria += "0101"
            ElseIf nMesMatricula > 36 And nMesMatricula < 120 Then
                cCategoria += "0103"
            Else
                cCategoria += "0104"
            End If

            If rowAsoc.Item("afi_categoria") <> cCategoria Then
                If categoriaNoCambia.Contains(rowAsoc.Item("afi_categoria")) Then
                    'Si la categoria es 110105 o 110106 no la actualiza.
                Else
                    'Aca hace la magia y actualiza las cuentas subcuenta y categoria
                    actualizoCategoriaCuentaSubcuenta(cCategoria, rowAsoc)
                End If
            End If
            ProgressBar.Value += 1
            ProgressBar.Refresh()
        Next

        DAAfiliado = cnnMante.consultaBDadapter(
            "(afiliado INNER JOIN cuentas ON cue_tipdoc=afi_tipdoc AND cue_nrodoc=afi_nrodoc AND cue_titulo=afi_titulo) INNER JOIN categorias ON cat_codigo=afi_categoria", ,
            "afi_tipo = 'A' AND afi_titulo <> 'EC' AND afi_titulo <> 'ET' AND afi_matricula > 0 AND MID(afi_categoria,1,2) IN ('11','12') AND cat_devenga" & Str(nDevenga).Trim & "<>'' ORDER BY cat_devenga" & Str(nDevenga).Trim & ",afi_categoria,afi_matricula"
        )

        DTAfiliado = New DataTable
        DAAfiliado.Fill(DTAfiliado)
        ProgressBar.Maximum = DTAfiliado.Rows.Count
        ProgressBar.Value = 0
        ProgressBar.Refresh()

        Dim cProcesoDevCuo As String = ""
        Dim nItem As Integer = 0
        DTDevAsi = New DataTable
        Dim nTotProceso As Double = 0
        Dim RowTotHaber As DataRow = Nothing
        Dim lListaMatriculado As Boolean = False
        Dim lMontoCuotaPrimerAnio As Double = 0
        For Each rowAsoc As DataRow In DTAfiliado.Rows
            Application.DoEvents()
            'Seteo en false para no devengar si no pasa los siguientes controles
            lListaMatriculado = False
            lMontoCuotaPrimerAnio = 0
            'obtengo los meses que lleva de matriculado
            mesesDesdeQueSeMatriculo = DateDiff(DateInterval.Month, CDate(rowAsoc.Item("cue_fecmatricula").ToString), DTPCuotasDevenga.Value.Date)
            ' si es devengamiento derecho ejercicio controlo la año de matricula 
            If nDevenga = 1 Then
                'el mes desde que se matriculo debe ser mayor o = 0 para devengar
                If mesesDesdeQueSeMatriculo = 0 Then
                    lListaMatriculado = True
                    lMontoCuotaPrimerAnio = 5
                ElseIf mesesDesdeQueSeMatriculo = 1 Then
                    lListaMatriculado = True
                    lMontoCuotaPrimerAnio = 15
                ElseIf mesesDesdeQueSeMatriculo > 1 Then
                    lListaMatriculado = True
                    'pregunto para no devengar hasta pasar los 12 meses. En este caso monto minimo.
                    If mesesDesdeQueSeMatriculo = 13 Then
                        lMontoCuotaPrimerAnio = 0.5
                    ElseIf mesesDesdeQueSeMatriculo < 13 Then
                        lMontoCuotaPrimerAnio = 10
                    End If
                End If
            Else
                'el mes desde que se matriculo debe ser mayor a 0 para devengar. Si es 0 significa mes de matriculacion
                If mesesDesdeQueSeMatriculo > 0 Then
                    lListaMatriculado = True
                End If
            End If
            If lListaMatriculado Then
                If cProcesoDevCuo <> rowAsoc.Item("cat_devenga" & Str(nDevenga).Trim) Then
                    If cProcesoDevCuo <> "" Then
                        DTDevAsi.ImportRow(RowTotHaber)
                        RowTotHaber = Nothing
                    End If
                    ArmoAsientoDevengamiento(rowAsoc.Item("cat_devenga" & Str(nDevenga).Trim), dFecDevenga, nDevenga)
                    cProcesoDevCuo = rowAsoc.Item("cat_devenga" & Str(nDevenga).Trim)
                End If
                If DTDevAsi.Rows.Count = 0 Then
                    DTDevAsi = DTDevenga.Clone
                End If
                For Each rowP As DataRow In DTDevenga.Rows
                    nItem += 1
                    If rowP.Item("pto_tipmov") = "D" Then
                        rowP.Item("pto_item") = nItem
                        If lMontoCuotaPrimerAnio = 0.5 Then
                            rowP.Item("debe") = rowP.Item("pto_valor") * lMontoCuotaPrimerAnio
                        ElseIf lMontoCuotaPrimerAnio <> 0 Then
                            rowP.Item("debe") = lMontoCuotaPrimerAnio
                        Else
                            rowP.Item("debe") = rowP.Item("pto_valor")
                        End If
                        rowP.Item("subcuenta") = rowAsoc.Item("afi_tipdoc") & rowAsoc.Item("afi_nrodoc")
                        rowP.Item("matricula") = rowAsoc.Item("afi_titulo") & rowAsoc.Item("afi_matricula")
                        DTDevAsi.ImportRow(rowP)
                    Else
                        If RowTotHaber Is Nothing Then
                            RowTotHaber = rowP
                            rowP.Item("pto_item") = nItem
                            If lMontoCuotaPrimerAnio = 0.5 Then
                                rowP.Item("haber") = rowP.Item("pto_valor") * lMontoCuotaPrimerAnio
                            ElseIf lMontoCuotaPrimerAnio <> 0 Then
                                rowP.Item("haber") = lMontoCuotaPrimerAnio
                            Else
                                rowP.Item("haber") = rowP.Item("pto_valor")
                            End If
                        Else
                            RowTotHaber.Item("pto_item") = nItem
                            If lMontoCuotaPrimerAnio = 0.5 Then
                                RowTotHaber.Item("haber") += rowP.Item("pto_valor") * lMontoCuotaPrimerAnio
                            ElseIf lMontoCuotaPrimerAnio <> 0 Then
                                RowTotHaber.Item("haber") += lMontoCuotaPrimerAnio
                            Else
                                RowTotHaber.Item("haber") += rowP.Item("pto_valor")
                            End If
                        End If
                    End If
                Next
            End If
            ProgressBar.Value += 1
            ProgressBar.Refresh()
        Next
        If cProcesoDevCuo <> "" Then
            DTDevAsi.ImportRow(RowTotHaber)
            RowTotHaber = Nothing
        End If
        cnnMante.CerrarConexion()
    End Sub

    Private Sub ArmoAsientoDevengamiento(ByVal cProceso As String, ByVal dFechaDev As Date, ByVal nDevenga As Integer)
        Dim numeroDeCuota As Integer
        Dim fechaVencimiento As String
        '1-CPCE 2-SIPRES 3-ASOCIADO 4-JUBILADO
        If nDevenga = 1 Then
            'si es CPCE entra
            Select Case Month(dFechaDev)
                Case 1, 2
                    numeroDeCuota = 1
                Case 3, 4
                    numeroDeCuota = 2
                Case 5, 6
                    numeroDeCuota = 3
                Case 7, 8
                    numeroDeCuota = 4
                Case 9, 10
                    numeroDeCuota = 5
                Case 11, 12
                    numeroDeCuota = 6
                Case Else
                    'Aca no deberia entrar nunca
                    numeroDeCuota = 1
            End Select
            'FECHA VENCIMIENTO
            fechaVencimiento = Format(dFechaDev.AddDays(9), "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
            'FIN FECHA VENCIMIENTO
        ElseIf nDevenga = 2 Then
            'si es SIPRES entra
            'FECHA VENCIMIENTO
            fechaVencimiento = Format(dFechaDev.AddDays(10), "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
            'FIN FECHA VENCIMIENTO
            'setea el numero de mes como numero de cuota
            numeroDeCuota = Month(dFechaDev)
        ElseIf nDevenga = 4 Then
            'si es JUBILADOS entra
            'FECHA VENCIMIENTO
            fechaVencimiento = Format(dFechaDev.AddDays(10), "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
            'FIN FECHA VENCIMIENTO
            'setea el numero de mes como numero de cuota
            numeroDeCuota = Month(dFechaDev)
        Else
            'FECHA VENCIMIENTO
            fechaVencimiento = Format(dFechaDev.AddDays(9), "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
            'FIN FECHA VENCIMIENTO
            'si es otro entra y setea el numero de mes como numero de cuota
            numeroDeCuota = Month(dFechaDev)
        End If
        DADevenga = cnnMante.consultaBDadapter("(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)", "pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio,pla_cuotas", "pto_codpro='" & cProceso & "' order by pto_codpro,pto_item")
        DTDevenga = New DataTable
        DADevenga.Fill(DTDevenga)
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        newCol.DefaultValue = cProceso
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))
        newCol.DefaultValue = CInt(Mid(cProceso, 1, 2))
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("delegacion", Type.GetType("System.Int32"))
        newCol.DefaultValue = nPubNroCli
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(dFechaDev, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = fechaVencimiento
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("numerodecuota", Type.GetType("System.Int32"))
        newCol.DefaultValue = numeroDeCuota
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = False
        DTDevenga.Columns.Add(newCol)
    End Sub
    Private Sub actualizoCategoriaCuentaSubcuenta(ByVal cCategoria As String, ByVal rowAsoc As DataRow)
        Dim miTrans As MySqlTransaction = cnnMante.InicioTransaccion
        Try
            cnnMante.actualizarBD(
                "UPDATE afiliado SET afi_categoria='" & cCategoria &
                "' WHERE afi_tipdoc='" & rowAsoc.Item("afi_tipdoc") & "' AND afi_nrodoc=" & rowAsoc.Item("afi_nrodoc")
            )
            cnnMante.actualizarBD(
                "UPDATE cuentas SET cue_categoria='" & cCategoria &
                "' WHERE cue_tipdoc='" & rowAsoc.Item("afi_tipdoc") & "' AND cue_nrodoc=" & rowAsoc.Item("afi_nrodoc") &
                " AND cue_titulo='" & rowAsoc.Item("afi_titulo") & "'"
            )
            cnnMante.actualizarBD(
                "INSERT INTO subcuenta (scu_tipdoc,scu_nrodoc,scu_titulo,scu_matricula,scu_catant,scu_categoria,scu_vigencia,scu_fecha,scu_nota1) " &
                "VALUE ('" & rowAsoc.Item("afi_tipdoc") & "','" & rowAsoc.Item("afi_nrodoc") & "','" & rowAsoc.Item("afi_titulo") & "','" &
                rowAsoc.Item("afi_matricula") & "','" & rowAsoc.Item("afi_categoria") & "','" & cCategoria & "','" &
                Format(DTPCuotasDevenga.Value, "yyyy-MM-dd") & "','" & Format(Now.Date, "yyyy-MM-dd") & "','Actualizacion automatica por devengamiento')"
            )
            miTrans.Commit()
        Catch ex As Exception
            miTrans.Rollback()
            MessageBox.Show(ex.Message & vbCrLf & " No se puedo actualizar la categoria de la Matricula : " & rowAsoc.Item("afi_matricula"))
        End Try
    End Sub
    Private Sub CBtGraCueAsoc_Click(sender As Object, e As EventArgs) Handles CBtGraCueAsoc.Click
        CBtGraCueAsoc.Enabled = False
        Dim dtChe As New DataTable
        Dim nAsiento As Long
        nAsiento = cnnMante.GraboAsiento(DTDevAsi, dtChe, dtChe, dtChe, "", "", "", "", 0, 0, False)
        MessageBox.Show("Proceso finalizado exitosamente, Ultimo N° Asiento: " & nAsiento)
    End Sub

    Private Sub CBtDevCuoSIPRES_Click(sender As Object, e As EventArgs) Handles CBtDevCuoSIPRES.Click
        DevengamientoCuotasAfiliado(2, DTPCuotasDevenga.Value, UltraProgressBar4)
        dgvCuoSipres.DataSource = DTDevAsi
        CBtGrabCuoSipres.Enabled = True
    End Sub

    Private Sub CBtGrabCuoSipres_Click(sender As Object, e As EventArgs) Handles CBtGrabCuoSipres.Click
        CBtGrabCuoSipres.Enabled = False
        Dim dtChe As New DataTable
        Dim nAsiento As Long
        nAsiento = cnnMante.GraboAsiento(DTDevAsi, dtChe, dtChe, dtChe, "", "", "", "", 0, 0, False)
        MessageBox.Show("Proceso finalizado exitosamente, Ultimo N° Asiento: " & nAsiento)
    End Sub
    Private Sub CBtGrabCuoCPCE_Click(sender As Object, e As EventArgs) Handles CBtGrabCuoCPCE.Click
        CBtGrabCuoCPCE.Enabled = False
        Dim dtChe As New DataTable
        Dim nAsiento As Long
        nAsiento = cnnMante.GraboAsiento(DTDevAsi, dtChe, dtChe, dtChe, "", "", "", "", 0, 0, False)
        MessageBox.Show("Proceso finalizado exitosamente, Ultimo N° Asiento: " & nAsiento)
    End Sub

    Private Sub CBtDevCuoCPCE_Click(sender As Object, e As EventArgs) Handles CBtDevCuoCPCE.Click
        DevengamientoCuotasAfiliado(1, DTPCuotasDevenga.Value, UltraProgressBar3)
        dgvCuoCPCE.DataSource = DTDevAsi
        CBtGrabCuoCPCE.Enabled = True
    End Sub

    Private Sub CargaDelegacion()
        DAFarcat = cnnMante.consultaBDadapter("farcat", "far_nrocli as NroDeleg,far_sucursal as Delegacion,far_server as Servidor", "far_unegos=2")
        'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
        DTFarcat = New DataTable
        DAFarcat.Fill(DTFarcat)
        DTFarcat.Columns.Add("Select", GetType(Boolean))
        For Each rowFar As DataRow In DTFarcat.Rows
            rowFar.Item("Select") = False
        Next
        BSFarcat = New BindingSource
        BSFarcat.DataSource = DTFarcat
        dgvDelegaciones.DataSource = BSFarcat
        'If DTFarcat.Rows.Count > 0 Then
        '    For Each rowFarcat As DataRow In DTFarcat.Rows
        '        UltraTree1.Nodes.Add("" & rowFarcat.Item("far_nombre").ToString & " " & rowFarcat.Item("far_sucursal").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
        '    Next
        'End If

        'For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
        '    Nodo.CheckedState = CheckState.Checked
        'Next
        'UltraTree1.Focus()
    End Sub

    'Private Sub SincronizarConWEB()
    '    Dim cnnWeb As New ConsultaBD("200.58.122.192", "ty000319_cpce", "ty000319_meg", "Cpce_2014", 3306)
    '    Dim DAWeb As MySqlDataAdapter
    '    Dim DTSincronizar As New DataTable
    '    Dim miTrans As MySqlTransaction

    '    If cnnWeb.AbrirConexion() And cnnMante.AbrirConexion() Then
    '        miTrans = cnnWeb.InicioTransaccion
    '        Try
    '            DAWeb = cnnMante.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "tot_titulo,tot_matricula,tot_nropla,pla_nombre,max(tot_fecha) as ultmov,sum(tot_debe-tot_haber) as saldo", "tot_matricula>0 and pla_servicio='Si' and (mid(tot_nropla,1,2)='13' or tot_nropla='22010100') and tot_estado<>'9' group by tot_titulo,tot_matricula,tot_nropla")
    '            DAWeb.Fill(DTSincronizar)
    '            UltraProgressBar2.Value = 0
    '            UltraProgressBar2.Text = "Servicios"
    '            UltraProgressBar2.Maximum = DTSincronizar.Rows.Count
    '            cnnWeb.actualizarBD("DELETE FROM servicios")

    '            For Each rowSinc As DataRow In DTSincronizar.Rows
    '                Application.DoEvents()
    '                cnnWeb.actualizarBD("INSERT INTO servicios (ser_tit,ser_mat,ser_cue,ser_desc,ser_ultmov,ser_sald) " & _
    '                                    "VALUES ('" & rowSinc.Item("tot_titulo") & "','" & rowSinc.Item("tot_matricula") & "','" & _
    '                                    rowSinc.Item("tot_nropla") & "','" & rowSinc.Item("pla_nombre").ToString & "','" & Format(CDate(rowSinc.Item("ultmov").ToString), "yyyy-MM-dd") & "','" & rowSinc.Item("Saldo").ToString.Replace(",", ".") & "')")
    '                UltraProgressBar2.Value += 1
    '            Next

    '            DAWeb = cnnWeb.consultaBDadapter("afiliado", , "afi_mail<>''")
    '            DTSincronizar = New DataTable
    '            DAWeb.Fill(DTSincronizar)
    '            UltraProgressBar2.Value = 0
    '            UltraProgressBar2.Text = "Afiliados"
    '            UltraProgressBar2.Maximum = DTSincronizar.Rows.Count

    '            For Each rowSinc As DataRow In DTSincronizar.Rows
    '                Application.DoEvents()
    '                cnnMante.actualizarBD("update afiliado set afi_mail='" & rowSinc.Item("afi_mail") & "' where afi_tipdoc='" & rowSinc.Item("afi_tipdoc") & "' and afi_nrodoc=" & rowSinc.Item("afi_nrodoc"))

    '                UltraProgressBar2.Value += 1
    '            Next

    '            DAWeb = cnnMante.consultaBDadapter("afiliado", , "afi_tipo='A'")
    '            DTSincronizar = New DataTable
    '            DAWeb.Fill(DTSincronizar)
    '            UltraProgressBar2.Value = 0
    '            UltraProgressBar2.Text = "Afiliados"
    '            UltraProgressBar2.Maximum = DTSincronizar.Rows.Count
    '            cnnWeb.actualizarBD("DELETE FROM afiliado")

    '            For Each rowSinc As DataRow In DTSincronizar.Rows
    '                Application.DoEvents()
    '                cnnWeb.actualizarBD("INSERT INTO afiliado (afi_nombre,afi_fecnac,afi_tipo,afi_tipdoc,afi_nrodoc,afi_titulo,afi_matricula," & _
    '                                    "afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_civil," & _
    '                                    "afi_categoria) " & _
    '                                    "VALUES ('" & rowSinc.Item("afi_nombre").ToString.Replace("'", " ") & "','" & Format(CDate(rowSinc.Item("afi_fecnac").ToString), "yyyy-MM-dd") & "','" & _
    '                                    rowSinc.Item("afi_tipo") & "','" & rowSinc.Item("afi_tipdoc") & "','" & rowSinc.Item("afi_nrodoc") & "','" & _
    '                                    rowSinc.Item("afi_titulo") & "','" & rowSinc.Item("afi_matricula") & "','" & rowSinc.Item("afi_direccion") & "','" & _
    '                                    rowSinc.Item("afi_localidad") & "','" & rowSinc.Item("afi_provincia") & "','" & rowSinc.Item("afi_zona") & "','" & _
    '                                    rowSinc.Item("afi_codpos") & "','" & rowSinc.Item("afi_telefono1") & "','" & rowSinc.Item("afi_telefono2") & "','" & _
    '                                    rowSinc.Item("afi_mail") & "','" & rowSinc.Item("afi_civil") & "','" & rowSinc.Item("afi_categoria") & "')")
    '                UltraProgressBar2.Value += 1
    '            Next

    '            miTrans.Commit()

    '            MessageBox.Show("Sincroniacion finalizada exitosamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Catch ex As Exception
    '            miTrans.Rollback()
    '            MessageBox.Show(ex.Message, "Se cancelo la sincronizacion por el siguiente error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        End Try
    '        cnnMante.CerrarConexion()
    '        cnnWeb.CerrarConexion()
    '    End If
    'End Sub

    'Private Sub InicioTransmision()
    '    ' Dim cnnSaenz As New ConsultaBD("192.168.100.3", cPubUsuario, cPubClave)
    '    Dim cnnDelegacion As ConsultaBD
    '    Dim nUltimoLote As Integer
    '    ' Dim DTDelega As New DataTable
    '    Dim DATrans As MySqlDataAdapter
    '    Dim cmdTrans As MySqlCommandBuilder
    '    Dim DTCatalogo As DataTable
    '    Dim DTEnviar As DataTable
    '    Dim DTRecibir As DataTable
    '    Dim DTLoteHis As DataTable
    '    Dim MItrans As MySqlTransaction
    '    'DATrans = cnnMante.consultaBDadapter("farcat", , "far_unegos=2 and far_nrocli=3")
    '    'DATrans.Fill(DTDelega)
    '    UltraProgressBar2.Value = 0
    '    UltraProgressBar2.Text = ""
    '    UltraProgressBar2.Refresh()
    '    DATrans = cnnMante.consultaBDadapter("catalogo")
    '    DTCatalogo = New DataTable
    '    DATrans.Fill(DTCatalogo)
    '    ' busco que hay en central para transmitir a delegaciones
    '    cnnMante.AbrirConexion()
    '    MItrans = cnnMante.InicioTransaccion
    '    For Each rowCata As DataRow In DTCatalogo.Rows
    '        If rowCata.Item("cat_vaiven") = "E" Or rowCata.Item("cat_vaiven") = "S" Then

    '            nUltimoLote = cnnMante.ActualizoLote(rowCata.Item("cat_tabla"))
    '            cnnMante.actualizarBD("update " & rowCata.Item("cat_tabla") & " set " & rowCata.Item("cat_indice") & "=" & nUltimoLote & " where " & rowCata.Item("cat_indice") & "=0")

    '            DATrans = cnnMante.consultaBDadapter(rowCata.Item("cat_tabla"), , rowCata.Item("cat_indice") & "=" & nUltimoLote)
    '            DTEnviar = New DataTable
    '            DATrans.Fill(DTEnviar)
    '            If DTEnviar.Rows.Count > 0 Then
    '                For Each rowDelegacion As DataRow In DTFarcat.Rows
    '                    cnnMante.actualizarBD("INSERT INTO `lotehistorico` (`lhi_nrocli`,`lhi_tabla`,`lhi_fecha`,`lhi_hora`,`lhi_lote`,`lhi_origen`,`lhi_estado`,`lhi_cantreg`,`lhi_ultreg`) " + _
    '                                        "VALUES (" + CStr(rowDelegacion.Item("NroDeleg")) + ",'" + rowCata.Item("cat_tabla") + "','" + Convert.ToString(Format(Date.Today, "yyyy-MM-dd")) + "','" + CStr(TimeOfDay) + "'," + CStr(nUltimoLote) + "," + _
    '                                        CStr(nPubNroCli) + ",''," + CStr(DTEnviar.Rows.Count) + ",0);")
    '                Next
    '            End If
    '        End If
    '    Next
    '    MItrans.Commit()
    '    cnnMante.CerrarConexion()
    '    For Each rowDelegacion As DataRow In DTFarcat.Rows
    '        If rowDelegacion.Item("Select") Then
    '            cnnDelegacion = New ConsultaBD(rowDelegacion.Item("servidor"), "cpce", "root", "cpce_2012", 3306)
    '            If cnnDelegacion.AbrirConexion Then
    '                Try
    '                    For Each rowCata As DataRow In DTCatalogo.Rows
    '                        If rowCata.Item("cat_vaiven") <> "E" Then
    '                            If rowCata.Item("cat_filtro").ToString.Trim = "" Then
    '                                DATrans = cnnDelegacion.consultaBDadapter(rowCata.Item("cat_tabla"), , rowCata.Item("cat_indice") & "=0")
    '                            Else
    '                                DATrans = cnnDelegacion.consultaBDadapter(rowCata.Item("cat_tabla"), , rowCata.Item("cat_indice") & "=0 and " & rowCata.Item("cat_filtro"))
    '                            End If
    '                            cmdTrans = New MySqlCommandBuilder(DATrans)
    '                            DTRecibir = New DataTable
    '                            DATrans.Fill(DTRecibir)
    '                            nUltimoLote = cnnMante.ActualizoLote(rowCata.Item("cat_tabla"))
    '                            UltraProgressBar2.Value = 0
    '                            UltraProgressBar2.Maximum = DTRecibir.Rows.Count
    '                            UltraProgressBar2.Text = "Recibiendo " & rowCata.Item("cat_tabla")
    '                            cnnMante.AbrirConexion()
    '                            For Each rowRecibir As DataRow In DTRecibir.Rows
    '                                Application.DoEvents()
    '                                If cnnMante.ReplaceBD(cnnDelegacion.ReplaceTabla(rowCata.Item("cat_tabla"), rowRecibir, DTRecibir.Columns, rowCata.Item("cat_indice"), nUltimoLote)) Then
    '                                    rowRecibir.Item(rowCata.Item("cat_indice")) = nUltimoLote
    '                                End If
    '                                UltraProgressBar2.Value += 1
    '                                UltraProgressBar2.Refresh()
    '                            Next

    '                            DATrans.Update(DTRecibir)
    '                            DTRecibir.Dispose()
    '                            DATrans.Dispose()
    '                            cnnMante.CerrarConexion()
    '                        End If

    '                        DTLoteHis = New DataTable
    '                        DATrans = cnnMante.consultaBDadapter("lotehistorico", , "lhi_nrocli=" & rowDelegacion.Item("NroDeleg") & " and lhi_tabla='" & rowCata.Item("cat_tabla") & "' and lhi_estado=''")
    '                        DATrans.Fill(DTLoteHis)
    '                        For Each rowLotHis As DataRow In DTLoteHis.Rows
    '                            DTEnviar = New DataTable
    '                            DATrans = cnnMante.consultaBDadapter(rowLotHis.Item("lhi_tabla"), , rowCata.Item("cat_indice") & "=" & rowLotHis.Item("lhi_lote"))
    '                            DATrans.Fill(DTEnviar)
    '                            UltraProgressBar2.Value = 0
    '                            UltraProgressBar2.Maximum = DTEnviar.Rows.Count
    '                            UltraProgressBar2.Text = "Enviando " & rowCata.Item("cat_tabla")
    '                            cnnMante.AbrirConexion()
    '                            For Each rowEnviar As DataRow In DTEnviar.Rows
    '                                Application.DoEvents()
    '                                cnnMante.ReplaceTabla(rowCata.Item("cat_tabla"), rowEnviar, DTEnviar.Columns, rowCata.Item("cat_indice"), rowLotHis.Item("lhi_lote"))
    '                                UltraProgressBar2.Value += 1
    '                                UltraProgressBar2.Refresh()
    '                            Next
    '                            cnnMante.actualizarBD("DELETE FROM lotehistorico WHERE lhi_nrocli=" & rowLotHis.Item("lhi_nrocli") & " and lhi_tabla='" & rowLotHis.Item("lhi_tabla") & "' and lhi_lote=" & rowLotHis.Item("lhi_lote"))
    '                            DTEnviar.Dispose()
    '                            DATrans.Dispose()
    '                            cnnMante.CerrarConexion()
    '                        Next
    '                    NextCbtActualizarObleas_Click
    '                    MessageBox.Show("Transmision finalizada exitosamente delegación " & rowDelegacion.Item("Delegacion"))
    '                Catch ex As Exception
    '                    MessageBox.Show(ex.Message)
    '                End Try
    '            Else
    '                MessageBox.Show("No se pudo conectar con la delegación " & rowDelegacion.Item("Delegacion"))
    '            End If
    '        End If
    '    Next

    '    MessageBox.Show("Fin de las transmiciones")
    'End Sub

    'Private Sub CBtTransmitir_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtTransmitir.Click
    '    InicioTransmision()
    'End Sub

    'Private Sub CbtActualizarObleas_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CbtActualizarObleas.Click

    'End Sub

    Private Sub CButton4_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton4.Click
        If cnnMante.AbrirConexion Then
            ' DSHonorarios.Tables("thcalcac").AcceptChanges()
            For Each row As DataRow In DSHonorarios.Tables("thcalcac").Rows
                cnnMante.actualizarBD("update thcalcac set thc_fijo=" & row.Item("thc_fijo").ToString.Replace(",", ".") & ",thc_adicporc=" & row.Item("thc_adicporc").ToString.Replace(",", ".") & ",thc_excedente=" & row.Item("thc_excedente").ToString.Replace(",", ".") & " where thc_mondes=" & row.Item("thc_mondes").ToString.Replace(",", ".") & " and thc_monhas=" & row.Item("thc_monhas").ToString.Replace(",", "."))
            Next
            cnnMante.CerrarConexion()
        End If
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtExportar.Click
        Dim clsExportarExcel As New ExportarExcel
        Dim cListado(0) As String
        cListado(0) = "Plan de Cuentas"
        clsExportarExcel.ExportarDatosExcel(UGplancuen, cListado)
    End Sub

    Private Sub CBtExportarProceso_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtExportarProceso.Click
        Dim clsExportarExcel As New ExportarExcel
        Dim cListado(0) As String
        cListado(0) = "Procesos Contables"
        clsExportarExcel.ExportarDatosExcel(UGProcesos, cListado)
    End Sub

    'Private Sub CBtsincronizar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtsincronizar.Click
    '    SincronizarConWEB()
    'End Sub
    Private Sub CBtDevengaJub_Click(sender As Object, e As EventArgs) Handles CBtDevengaJub.Click
        DevengamientoCuotaJubilados()
    End Sub
    Private Sub CBtGraCuoJub_Click(sender As Object, e As EventArgs) Handles CBtGraCuoJub.Click
        CBtGraCuoJub.Enabled = False
        Dim dtChe As New DataTable
        Dim nAsiento As Long
        nAsiento = cnnMante.GraboAsiento(DTDevAsi, dtChe, dtChe, dtChe, "", "", "", "", 0, 0, False)
        MessageBox.Show("Proceso finalizado exitosamente, Ultimo N° Asiento: " & nAsiento)
    End Sub

    Private Sub GenerarDebitos(ByVal cServicio() As String, ByVal institucion As String, ByVal tipo_debito As String)
        If tipo_debito = "Tarjeta Tuya" Then
            tipo_debito = 1
        Else
            tipo_debito = 0
        End If
        Dim Cuotas As New Calculos
        Dim nNroComp As String
        Dim nTotal As Double
        'Dim contador As Integer = 0
        Dim contadorSub As Integer = 0
        'Dim contadorString As String
        Dim servicioIN As String = ""
        For i As Integer = 0 To cServicio.Length - 1
            If i = 0 Then
                servicioIN = cServicio(i)
            Else
                servicioIN = servicioIN & "," & cServicio(i)
            End If
        Next
        'Fecha seteo dia 15 del mes corriente para hacer la consulta de totales
        Dim fechaToday As Date = Today()
        Dim fechaTotales As New Date(fechaToday.Year, fechaToday.Month, 15)
        ' SI ES PROCESO DE RECIBO DE MORATORIA BUSCO EN LA TABLA AUXILIAR
        'If c_Proceso = "01RCMO" Or c_Proceso = "MORMAN" Then
        '    DTCuotasImpagas = Cuotas.CuotasMoratoriasImpagas(c_NroPlan, c_Tipdoc, n_NroDoc)
        '    With DataGridView1
        '        .DataSource = DTCuotasImpagas
        '    End With
        'Else
        '    DTCuotasImpagas = Cuotas.CuotasServicioImpagas(c_NroPlan, c_Tipdoc, n_NroDoc, False, Now.Date)
        'End If
        '        DADevenga = cnnMante.consultaBDadapter("(totales inner join plancuen on pla_nropla=tot_nropla) inner join afiliado on afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "afi_cbu as CBU,afi_nombre as Nombre,afi_titulo as Titulo,afi_matricula as Matricula,afi_nrodoc,afi_cuit as CUIT,tot_nrocom as NroComp,sum(tot_debe-tot_haber) as total,pla_nropla,pla_nombre,pla_coddebito as CodEmpresa", "tot_nropla in (" & cServicio & ") and tot_fecha<='" & Format(UDTCuota.Value, "yyyy-MM-dd") & "' and tot_estado<>'9' and afi_cbu<>'' and pla_servicio='Si' group by tot_tipdoc,tot_nrodoc,tot_nropla ORDER BY tot_nropla")
        Dim DTcuotasImpagas As DataTable
        Dim DTDevengaClon As New DataTable
        Dim DTTotal As DataTable
        DTDevenga = New DataTable

        'tipo_debito = 0 Banco, 1 Tarjeta Tuya;
        DADevenga = cnnMante.consultaBDadapter(
            "afiliado INNER JOIN debito ON afi_tipdoc = deb_tipdoc AND afi_nrodoc = deb_nrodoc",
            "afi_cbu as CBU, afi_nombre as Nombre, afi_titulo as Titulo, afi_matricula as Matricula, afi_tipdoc, afi_nrodoc, afi_cuit as CUIT, afi_lote as NroComp, afi_monto as total, '' as pla_nropla, '' as pla_nombre, 0 as CodEmpresa, deb_nropla",
            "afi_cbu <> '' AND deb_nropla IN (" & servicioIN & ") AND deb_tipo = " & tipo_debito & " ORDER BY deb_nropla"
        )

        DADevenga.Fill(DTDevengaClon)
        DTDevenga = DTDevengaClon.Clone
        '        For D1 As Integer = 0 To cServicio.Length - 1
        'contador = contador + 1
        'contadorString = CStr(contador)
        UProBarDebito.Maximum = DTDevengaClon.Rows.Count
        UProBarDebito.Value = 0
        UProBarDebito.Refresh()
        For Each rowAfiClon As DataRow In DTDevengaClon.Rows
            DTTotal = New DataTable
            'DADevenga = cnnMante.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "concat(tot_nroasi,tot_item) as NroAsiento,tot_nrocom as NroComp,sum(tot_debe-tot_haber) as total,pla_nropla,pla_nombre,pla_coddebito as CodEmpresa", "tot_nropla ='" & cServicio(D1) & "' and tot_fecha<='" & Format(UDTCuota.Value, "yyyy-MM-dd") & "' and tot_estado<>'9' and pla_servicio='Si' and tot_titulo='" & rowAfiClon.Item("titulo") & "' and tot_matricula=" & rowAfiClon.Item("matricula") & " group by tot_nropla")
            'DADevenga = cnnMante.consultaBDadapter(
            '   "totales INNER JOIN plancuen ON pla_nropla=tot_nropla",
            '  "tot_nroasi,tot_item,tot_nrocom as NroComp,sum(tot_debe-tot_haber) as total,pla_nropla,pla_nombre,pla_coddebito as CodEmpresa",
            ' "tot_nropla ='" & cServicio(D1) & "' AND tot_fecha<='" & Format(UDTCuota.Value, "yyyy-MM-dd") & "' AND tot_estado<>'9' AND pla_servicio='Si' AND tot_titulo='" & rowAfiClon.Item("titulo") & "' AND tot_matricula=" & rowAfiClon.Item("matricula") & " GROUP BY tot_nropla"
            ')
            DADevenga = cnnMante.consultaBDadapter(
                "totales INNER JOIN plancuen ON pla_nropla=tot_nropla",
                "tot_nroasi,tot_item,tot_nrocom as NroComp,sum(tot_debe-tot_haber) as total,pla_nropla,pla_nombre,pla_coddebito as CodEmpresa",
                "tot_nropla ='" & rowAfiClon.Item("deb_nropla") & "' AND tot_fecven <= '" & Format(fechaTotales, "yyyy-MM-dd") & "' AND tot_estado<>'9' AND pla_servicio='Si' AND tot_titulo='" & rowAfiClon.Item("titulo") & "' AND tot_matricula=" & rowAfiClon.Item("matricula") & " GROUP BY tot_nropla"
            )
            DADevenga.Fill(DTTotal)
            'If DTTotal.Rows.Count = 1 Then
            '    If rowAfiClon.Item("deb_nropla") = "13040100" Then 'DEUDORES APORTES BUSCO LA MAS VIEJA IMPAGA CON INTERESES
            nNroComp = 0
            nTotal = 0
            DTcuotasImpagas = Cuotas.CuotasServicioImpagas(rowAfiClon.Item("deb_nropla"), rowAfiClon.Item("afi_tipdoc"), rowAfiClon.Item("afi_nrodoc"), True, fechaTotales, False, True)
            For Each dtCuoImp As DataRow In DTcuotasImpagas.Rows
                nTotal = 0
                If dtCuoImp.Item("imppagado") = 0 Or dtCuoImp.Item("imppagado") < dtCuoImp.Item("debe") Then
                    contadorSub = contadorSub + 1
                    nNroComp = CInt(institucion + CStr(contadorSub))
                    'nNroComp = Format(resContador, "0000000")
                    'nNroComp = dtCuoImp.Item("tot_nroasi") & dtCuoImp.Item("tot_item")
                    'nNroComp = contadorCeros
                    nTotal = dtCuoImp.Item("debe") - dtCuoImp.Item("imppagado")
                    'nTotal = dtCuoImp.Item("debe") + dtCuoImp.Item("interes")
                End If
                If nTotal > 0 Then
                    Dim newRowAfiClon As DataRow = DTDevenga.NewRow
                    newRowAfiClon.ItemArray = rowAfiClon.ItemArray
                    newRowAfiClon.Item("NroComp") = nNroComp
                    newRowAfiClon.Item("total") = nTotal
                    newRowAfiClon.Item("pla_nropla") = DTTotal.Rows(0).Item("pla_nropla")
                    newRowAfiClon.Item("pla_nombre") = DTTotal.Rows(0).Item("pla_nombre")
                    newRowAfiClon.Item("CodEmpresa") = DTTotal.Rows(0).Item("CodEmpresa")
                    DTDevenga.Rows.Add(newRowAfiClon)
                End If
            Next
            'Else
            '    contadorSub = contadorSub + 1
            '    nNroComp = CInt(institucion + CStr(contadorSub))
            '    'nNroComp = Format(resContador, "0000000")
            '    'nNroComp = DTTotal.Rows(0).Item("NroAsiento")
            '    'nNroComp = DTTotal.Rows(0).Item("tot_nroasi") & DTTotal.Rows(0).Item("tot_item")
            '    'nNroComp = contadorCeros
            '    nTotal = DTTotal.Rows(0).Item("total")
            'End If

            'If nTotal > 0 Then
            '    rowAfiClon.Item("NroComp") = nNroComp
            '    rowAfiClon.Item("total") = nTotal
            '    rowAfiClon.Item("pla_nropla") = DTTotal.Rows(0).Item("pla_nropla")
            '    rowAfiClon.Item("pla_nombre") = DTTotal.Rows(0).Item("pla_nombre")
            '    rowAfiClon.Item("CodEmpresa") = DTTotal.Rows(0).Item("CodEmpresa")
            '    DTDevenga.ImportRow(rowAfiClon)
            'End If
            'End If
            UProBarDebito.Value += 1
            UProBarDebito.Refresh()
        Next
        '        Next
        Dim dc As DataColumn
        dc = New DataColumn("Select", GetType(Boolean))
        dc.DefaultValue = True
        DTDevenga.Columns.Add(dc)
        DTDevenga.Columns("Select").SetOrdinal(0)

        UGdebitos.DataSource = DTDevenga

        With UGdebitos.DisplayLayout.Bands(0)
            .Columns(0).Width = 50
            .Columns(1).Width = 170
            .Columns(2).Width = 200
            .Columns(3).Width = 50
            .Columns(4).Width = 50
            .Columns(5).Width = 60
            .Columns(6).Width = 70
            .Columns(9).CellAppearance.TextHAlign = HAlign.Right
            .Columns(9).Format = "N2"
            .Columns(10).Width = 70
            .Columns(11).Width = 150
            .Columns(12).Width = 60
        End With
    End Sub

    Private Sub CBtGeneraDebito_Click(sender As Object, e As EventArgs) Handles CBtGeneraDebito.Click
        If RadioButtonDebitosSIPRES.Checked Then
            Dim cServicios(6) As String
            cServicios(0) = "13040100"
            cServicios(1) = "13040400"
            cServicios(2) = "13051600"
            cServicios(3) = "13051000"
            cServicios(4) = "13051100"
            cServicios(5) = "13050900"
            cServicios(6) = "13040300"

            GenerarDebitos(cServicios, "1", CBoxTipo.Text)
        End If
        If RadioButtonDebitosCPCE.Checked Then
            Dim cServicios(3) As String
            cServicios(0) = "13010200"
            cServicios(1) = "13010600"
            cServicios(2) = "13010800"
            cServicios(3) = "13010900"

            GenerarDebitos(cServicios, "2", CBoxTipo.Text)
        End If
    End Sub
    Private Sub ExportarDebitos()
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty
        Dim cMtrError As String = ""
        'Para el archivo
        Dim strStreamW As Stream = Nothing
        Dim strStreamWriter As StreamWriter = Nothing
        Dim PathArchivo As String

        Try
            'Borro y creo el archivo
            If Directory.Exists("C:\CpceMEGS\Temp") = False Then ' si no existe la carpeta se crea
                Directory.CreateDirectory("C:\CpceMEGS\Temp")
            End If

            Cursor.Current = Cursors.WaitCursor

            'No se marco ninguna entidad
            If DTDevenga.Rows.Count > 0 Then

                UltraProgressBar1.Maximum = DTDevenga.Rows.Count

                Dim cadena As String = ""
                Dim Profesional As String = ""
                Dim Direccion As String = ""
                Dim TotalDebito As Double = 0
                Dim CodServ As Integer = 0
                Dim cDescriServ As String = ""
                Dim Fecha As String = Format(Convert.ToDateTime(UDTDesde.Text), "yyyy-MM-dd")
                Dim nAsiExp As Long = 0
                Dim nImpComprobante As Double = 0
                Dim xReg As Integer = 0
                Dim xRegProceso As Integer = 0

                UltraProgressBar1.Minimum = 0
                UltraProgressBar1.Maximum = DTDevenga.Rows.Count

                For Each rowExportar As DataRow In DTDevenga.Rows
                    Application.DoEvents()

                    If CodServ <> rowExportar.Item("codempresa") Then
                        If CodServ <> 0 Then
                            strStreamWriter.WriteLine("3" & Format(xRegProceso, "000000") & Format(TotalDebito * 100, "000000000000") & Format(TotalDebito * 100, "000000000000") & Space(169))
                            strStreamWriter.Close() ' cerramos
                        End If
                        CodServ = rowExportar.Item("codempresa")
                        TotalDebito = 0
                        Select Case CodServ
                            Case "10639"
                                cDescriServ = "DERECHO PR"
                            Case "10641"
                                cDescriServ = "DEUDA ASOC"
                            Case "10638"
                                cDescriServ = "COESPECIAL"
                            Case "10637"
                                cDescriServ = "AYUDA PERS"
                            Case "10640"
                                cDescriServ = "DEUDA APOR"
                            Case "10642"
                                cDescriServ = "DEUDA FARM"
                            Case "10643"
                                cDescriServ = "DEUDA JUBI"
                            Case "10644"
                                cDescriServ = "MORASIPRES"
                            Case "10876"
                                cDescriServ = "REFAFILIAD"
                            Case "10877"
                                cDescriServ = "REFINASOCI"
                        End Select

                        Dim titulo As String = "archivo_"
                        Dim nroComercio As String = "130553526996" ' Banco por defecto
                        Dim nroEstablecimiento As String

                        Select Case CBoxTipo.Text
                            Case "Tarjeta Tuya"
                                titulo = "tuya_"
                            Case "Tarjeta Visa"
                                titulo = "tuya_"
                                nroComercio = "0030865411"
                                nroEstablecimiento = "0043923549"
                            Case "Tarjeta Mastercard"
                                titulo = "mastercard_"
                                nroComercio = "0030865411"
                                nroEstablecimiento = "0043923556"
                            Case "Tarjeta Naranja"
                                titulo = "naranja_"
                            Case Else
                                titulo = "banco_"

                        End Select

                        PathArchivo = "C:\CpceMEGS\Temp\DP" & titulo & Format(UDTCuota.Value, "yyMM") & Format(UNEnroInicial.Value, "00") & ".txt"
                        UNEnroInicial.Value += 1
                        If File.Exists(PathArchivo) Then

                            My.Computer.FileSystem.DeleteFile(PathArchivo)
                            strStreamW = File.Create(PathArchivo) ' lo creamos

                        Else
                            strStreamW = File.Create(PathArchivo) ' lo creamos
                        End If
                        strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default)

                        strStreamWriter.WriteLine(nroComercio & "CPCE CHACO      " & Format(UDTCuota.Value, "yyyyMM") & "01" & "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
                        xRegProceso = 0
                    End If

                    'Escribo
                    If rowExportar.Item("Total") > 0 And rowExportar.Item("Select") Then
                        xRegProceso += 1

                        TotalDebito += rowExportar.Item("Total")
                        xReg += 1
                        If rowExportar.Item("CUIT").ToString.Replace("-", "").Trim = "" Then
                            MessageBox.Show(rowExportar.Item("Nombre").ToString & " no tiene cargado el CUIT")
                        Else
                            'Espacio(rowExportar.Item("Nombre"), 30) & rowExportar.Item("CBU") & Format(rowExportar.Item("Matricula"), "0000000000000000000000") & "A" & Format(rowExportar.Item("nrocom"), "000000000000000") & _
                            Dim nroReferencia As String = Format(UDTCuota.Value, "yyyyMMdd") + Format(rowExportar.Item("nroComp"), "0000000")
                            'Dim nroReferencia As String = Format(Format(UDTCuota.Value, "yyyyMMdd") + Format(rowExportar.Item("nroComp"), "0000000"), "000000000000000")
                            strStreamWriter.WriteLine("2" & Format(xReg, "000000") & cDescriServ & Format(rowExportar.Item("CodEmpresa"), "000000000000000") & "96" & Format(rowExportar.Item("afi_nrodoc"), "000000000000000") & rowExportar.Item("cuit").ToString.Replace("-", "") &
                                     Espacio(rowExportar.Item("Nombre"), 30) & rowExportar.Item("CBU") & Format(rowExportar.Item("Matricula"), "0000000000000000000000") & "A" & nroReferencia &
                                     Format(UDTCuota.Value, "yyyyMMdd") & Format(rowExportar.Item("Total") * 100, "0000000000") &
                                     Format(UDTvence.Value, "yyyyMMdd") & Format(rowExportar.Item("Total") * 100, "0000000000") & "00000000000000")
                        End If
                    End If
                    UltraProgressBar1.Value = xReg
                    UltraProgressBar1.Refresh()

                Next
                If CodServ <> 0 Then
                    'strStreamWriter.WriteLine("3" & Format(DTDevenga.Rows.Count, "000000") & Format(TotalDebito * 100, "000000000000") & Format(TotalDebito * 100, "000000000000") & Space(169)) 'Antes hacia un count del total de cada archivo
                    strStreamWriter.WriteLine("3" & Format(xRegProceso, "000000") & Format(TotalDebito * 100, "000000000000") & Format(TotalDebito * 100, "000000000000") & Space(169))
                    strStreamWriter.Close() ' cerramos
                End If
            Else
                MessageBox.Show("No se encontraron datos para el periodo y las entidades seleccionadas", "SIN DATOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                Exit Sub
            End If

            MessageBox.Show("Archivo Generado Exitosamente", "ARCHIVO EXPORTADO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Catch ex As Exception
            MessageBox.Show("No se pudo exportar el archivo. Se produjo el Siguiente Error con matricula " & cMtrError & " =" & ex.Message.ToString, "ERROR AL EXPORTAR ARCHIVO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            If strStreamWriter IsNot Nothing Then
                strStreamWriter.Close() ' cerramos
            End If
        End Try
    End Sub

    Private Function Cero(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "0"
        For i = 1 To Cantidad
            cuantos = cuantos & "0"
        Next i
        Cero = Mid(cuantos, 1, Cantidad - Len(numero)) & numero
    End Function

    Private Function Espacio(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = " "

        For i = 1 To Cantidad
            cuantos = " " & cuantos
        Next i
        If Cantidad > Len(numero) Then
            Espacio = numero & Mid(cuantos, 1, Cantidad - Len(numero))
        Else
            Espacio = Microsoft.VisualBasic.Left(numero, Cantidad)
        End If
    End Function

    Private Function EspacioCero(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "0"

        For i = 1 To Cantidad
            cuantos = "0" & cuantos
        Next i
        EspacioCero = numero & Mid(cuantos, 1, Cantidad - Len(numero))
    End Function

    Private Sub CButton5_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton5.Click
        ExportarDebitos()
    End Sub

    Private Sub CButton6_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton6.Click
        Dim cListado(0) As String
        If RadioButtonDebitosSIPRES.Checked Then
            cListado(0) = "Informacion de SIPRES generada para el Banco al " & UDTCuota.Value.ToString.Substring(0, 10)
        Else
            cListado(0) = "Informacion de CPCE generada para el Banco al " & UDTCuota.Value.ToString.Substring(0, 10)

        End If

        Dim clsExportarExcel As New ExportarExcel
        clsExportarExcel.ExportarDatosExcel(UGdebitos, cListado)
    End Sub

    Private Sub UltraTabControlSistema_SelectedTabChanged(ByVal sender As Object, ByVal e As UltraWinTabControl.SelectedTabChangedEventArgs) Handles UltraTabControlSistema.SelectedTabChanged
        Select Case e.Tab.Index
            Case 0
                'Sucursal
                CargaSucursales()
            Case 1
                'Operadores
                MuestraOperadores()
            Case 2
                'Permisos
                MuestraPermisos()
        End Select
    End Sub

    Private Sub CargaSucursales()
        DAFarcat = cnnMante.consultaBDadapter("farcat", "far_nrocli as NroDeleg,far_sucursal as Delegacion,far_server as Servidor", "far_unegos=2")
        'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
        DTFarcat = New DataTable
        DAFarcat.Fill(DTFarcat)
        DTFarcat.Columns.Add("Select", GetType(Boolean))
        For Each rowFar As DataRow In DTFarcat.Rows
            rowFar.Item("Select") = False
        Next
        BSFarcat = New BindingSource
        BSFarcat.DataSource = DTFarcat
        dgvSucursal.DataSource = BSFarcat
    End Sub

    Private Sub ButtonOperadorActualizar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles ButtonOperadorActualizar.Click
        Try
            If MessageBox.Show("Confirmar la actualizacion", "Operador", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                DAOperador.Update(DSOperador, "operador")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ObraSocialBindingNavigatorSaveItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Validate()
        DGVObraSocial.EndEdit()
        Try
            If MessageBox.Show("Confirmar la actualizacion", "Obra Social", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                'DAObraSocial.Adapter.Update(DSObraSocial)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PermisoBindingNavigatorSaveItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PermisoBindingNavigatorSaveItem.Click
        Validate()
        PermisoBindingSource.EndEdit()
        Try
            If MessageBox.Show("Confirmar la actualizacion", "Permiso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                PermisoTableAdapter.Adapter.Update(DSPermiso)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PermisoDataGridView_CellBeginEdit(ByVal sender As Object, ByVal e As DataGridViewCellCancelEventArgs) Handles PermisoDataGridView.CellBeginEdit
        'Si entro a editar el select de Accion entra
        If PermisoDataGridView.Columns(e.ColumnIndex).Name = "Accion" Then
            updateComboBoxCell()
        End If
    End Sub

    Private Sub PermisoDataGridView_CellValueChanged(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles PermisoDataGridView.CellValueChanged
        'Si cambio select de Modulo entra
        If PermisoDataGridView.Columns(e.ColumnIndex).Name = "Modulo" Then
            updateComboBoxCell()
        End If
    End Sub

    Private Sub updateComboBoxCell()
        Dim DTAccion As New DataTable()
        Dim Celda As DataGridViewComboBoxCell = DirectCast(PermisoDataGridView.CurrentRow.Cells("Accion"), DataGridViewComboBoxCell)
        Dim idCelda = Celda.Value()
        If Not IsDBNull(PermisoDataGridView.CurrentRow.Cells("Modulo").Value) Then
            DTAccion = AccionTableAdapter.GetAccionByModulo(PermisoDataGridView.CurrentRow.Cells("Modulo").Value)
        End If
        'Si es <= 0 entra y trae el accion Inicio para setear por defecto
        If DTAccion.DefaultView.Count <= 0 Then
            DTAccion = AccionTableAdapter.GetAccionByModulo(1)
        End If
        'seteo el primer id de las acciones que trae la consulta
        Dim newIdCelda As Integer = DTAccion.Rows.Item(0).Item("Id")
        'recorro para ver si existe el id que tiene seleccionado en las acciones que trajo
        If Not IsDBNull(idCelda) Then
            For Each idRow As DataRow In DTAccion.Rows
                'si entra cambio el id al que tenia seleccionado
                If idRow.Item("Id") = idCelda Then
                    newIdCelda = idCelda
                    Exit For
                End If
            Next
        End If
        Celda.DataSource = DTAccion.DefaultView
        'Seteo por defecto el primer valor de las acciones que trae en la consulta
        Celda.Value = newIdCelda
    End Sub

    Private Sub PermisoDataGridView_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles PermisoDataGridView.DataError
        MessageBox.Show("Error. Debe completar todos los campos.")
    End Sub
    '=======================================================================================ASIENTOS
    Private Sub UTPCAsientos_SelectedTabChanged(ByVal sender As Object, ByVal e As UltraWinTabControl.SelectedTabChangedEventArgs) Handles UltraTabControlSistema.SelectedTabChanged
        Select Case e.Tab.Index
            Case 0
                'Asientos desbalanceados
                'CargaAsientosDesbalanceados()
        End Select
    End Sub
    'ACCIONES
    Private Sub CargaAsientosDesbalanceados(ByVal desdeFechaBusca As Date, ByVal hastaFechaBusca As Date, ByVal procesoBusca As String)
        Dim tot_fecha_desde As String = Format(Convert.ToDateTime(desdeFechaBusca), "yyyy-MM-dd") & " 00:00:00"
        Dim tot_fecha_hasta As String = Format(Convert.ToDateTime(hastaFechaBusca), "yyyy-MM-dd") & " 23:59:59"
        Dim tot_proceso As String = procesoBusca
        Dim DAAsientosDesbalanceados As MySqlDataAdapter
        Dim BSAsientosDesbalanceados As BindingSource
        DTAsientosDesbalanceados = New DataTable

        Dim tabla As String = "totales"
        Dim campos As String = "tot_proceso AS Proceso, tot_nrocom AS Comprobante, tot_nroasi AS Asiento, tot_fecha AS Fecha, tot_matricula AS Matricula, SUM(tot_debe) AS Debe, SUM(tot_haber) AS Haber, IF((SUM(tot_haber) - SUM(tot_debe)) <> 0, SUM(tot_haber) - SUM(tot_debe), null) AS Diferencia"
        Dim condiciones As String = "tot_estado <> 9 AND tot_fecha >= '" & tot_fecha_desde & "' AND tot_fecha <= '" & tot_fecha_hasta & "'"
        If tot_proceso <> "" Then
            condiciones &= " AND tot_proceso = '" & tot_proceso & "'"
        End If
        condiciones &= " GROUP BY tot_nroasi ORDER BY diferencia DESC"

        DAAsientosDesbalanceados = cnnMante.consultaBDadapter(tabla, campos, condiciones)

        DAAsientosDesbalanceados.Fill(DTAsientosDesbalanceados)
        BSAsientosDesbalanceados = New BindingSource
        BSAsientosDesbalanceados.DataSource = DTAsientosDesbalanceados
        DGVAsientos.DataSource = BSAsientosDesbalanceados
    End Sub

    Private Sub BalancearAsientos()
        'Dim cmbAsientosDesbalanceados As New MySqlCommandBuilder(DAAsientosDesbalanceados)
        UPBAsientosDesbalanceados.Maximum = DTAsientosDesbalanceados.Rows.Count
        UPBAsientosDesbalanceados.Value = 0
        UPBAsientosDesbalanceados.Refresh()
        For Each asiDesbal As DataRow In DTAsientosDesbalanceados.Rows
            'si entra balanceo el asiento
            If Not IsDBNull(asiDesbal.Item("Diferencia")) Then
                'Dim asiento As Integer = asiDesbal.Item("Asiento")
                'Dim diferencia As Double = asiDesbal.Item("Diferencia")
                cnnMante.BalanceoAsiento(asiDesbal.Item("Asiento"), asiDesbal.Item("Diferencia"))
            End If
            UPBAsientosDesbalanceados.Value += 1
            UPBAsientosDesbalanceados.Refresh()
        Next
    End Sub
    'BOTONES
    Private Sub CBAsientosBuscar_Click(sender As Object, e As EventArgs) Handles CBAsientosBuscar.Click
        CargaAsientosDesbalanceados(DTPAsientosDesde.Value, DTPAsientosHasta.Value, TBAsientosProceso.Text)
    End Sub
    Private Sub CBAsientosBalancear_Click(sender As Object, e As EventArgs) Handles CBAsientosBalancear.Click
        If controlAcceso(6, 10, "U", True) = True Then
            BalancearAsientos()
            CargaAsientosDesbalanceados(DTPAsientosDesde.Value, DTPAsientosHasta.Value, TBAsientosProceso.Text)
        End If
    End Sub

    Private Sub ButtonOperadorActualizar_Click(sender As Object, e As EventArgs) Handles ButtonOperadorActualizar.Click

    End Sub
    '=======================================================================================FIN
End Class