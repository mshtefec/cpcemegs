﻿Public Class FrmAltaCuentaProceso
    Private cnnaltCta As New ConsultaBD(True)

    Private Sub UltraTextEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UltraTextEditor1.EditorButtonClick
        If cnnaltCta.TraeCuenta(UltraTextEditor1.Text) Then
            UltraTextEditor2.Text = cnnaltCta.DescripcionCuenta
        End If
    End Sub

    Private Sub UltraTextEditor2_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UltraTextEditor2.EditorButtonClick
        If cnnaltCta.NumeroCuenta <> "" Then
            DialogResult = DialogResult.OK
        End If
    End Sub
End Class