﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmArregloPrestamo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmArregloPrestamo))
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.TBTramo1Cuotas = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TBTramo2Cuotas = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TBNroAsiento = New System.Windows.Forms.TextBox()
        Me.LNroAsiento = New System.Windows.Forms.Label()
        Me.TBTramo3Cuotas = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TBTramo4Cuotas = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TBTramo1Monto = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.TBTramo2Monto = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.TBTramo3Monto = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.TBTramo4Monto = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.LComentario = New System.Windows.Forms.Label()
        CType(Me.TBTramo1Monto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBTramo2Monto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBTramo3Monto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBTramo4Monto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(129, 49)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(98, 20)
        Me.DateTimePicker1.TabIndex = 0
        '
        'TBTramo1Cuotas
        '
        Me.TBTramo1Cuotas.Location = New System.Drawing.Point(106, 130)
        Me.TBTramo1Cuotas.Name = "TBTramo1Cuotas"
        Me.TBTramo1Cuotas.Size = New System.Drawing.Size(65, 20)
        Me.TBTramo1Cuotas.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Button1.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(106, 320)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(143, 38)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Confirmar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(113, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Cuotas"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(200, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Monto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(27, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 16)
        Me.Label3.TabIndex = 100
        Me.Label3.Text = "Tramo 1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(27, 169)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 16)
        Me.Label4.TabIndex = 101
        Me.Label4.Text = "Tramo 2"
        '
        'TBTramo2Cuotas
        '
        Me.TBTramo2Cuotas.Location = New System.Drawing.Point(106, 168)
        Me.TBTramo2Cuotas.Name = "TBTramo2Cuotas"
        Me.TBTramo2Cuotas.Size = New System.Drawing.Size(65, 20)
        Me.TBTramo2Cuotas.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(27, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "1° Vencimiento"
        '
        'TBNroAsiento
        '
        Me.TBNroAsiento.Enabled = False
        Me.TBNroAsiento.Location = New System.Drawing.Point(129, 23)
        Me.TBNroAsiento.Name = "TBNroAsiento"
        Me.TBNroAsiento.Size = New System.Drawing.Size(98, 20)
        Me.TBNroAsiento.TabIndex = 99
        '
        'LNroAsiento
        '
        Me.LNroAsiento.AutoSize = True
        Me.LNroAsiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNroAsiento.Location = New System.Drawing.Point(45, 24)
        Me.LNroAsiento.Name = "LNroAsiento"
        Me.LNroAsiento.Size = New System.Drawing.Size(78, 16)
        Me.LNroAsiento.TabIndex = 12
        Me.LNroAsiento.Text = "Nro Asiento"
        '
        'TBTramo3Cuotas
        '
        Me.TBTramo3Cuotas.Location = New System.Drawing.Point(106, 207)
        Me.TBTramo3Cuotas.Name = "TBTramo3Cuotas"
        Me.TBTramo3Cuotas.Size = New System.Drawing.Size(65, 20)
        Me.TBTramo3Cuotas.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(27, 208)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 16)
        Me.Label6.TabIndex = 102
        Me.Label6.Text = "Tramo 3"
        '
        'TBTramo4Cuotas
        '
        Me.TBTramo4Cuotas.Location = New System.Drawing.Point(106, 244)
        Me.TBTramo4Cuotas.Name = "TBTramo4Cuotas"
        Me.TBTramo4Cuotas.Size = New System.Drawing.Size(65, 20)
        Me.TBTramo4Cuotas.TabIndex = 7
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(27, 245)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 103
        Me.Label7.Text = "Tramo 4"
        '
        'TBTramo1Monto
        '
        Me.TBTramo1Monto.Location = New System.Drawing.Point(192, 130)
        Me.TBTramo1Monto.Name = "TBTramo1Monto"
        Me.TBTramo1Monto.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Decimal]
        Me.TBTramo1Monto.Size = New System.Drawing.Size(100, 21)
        Me.TBTramo1Monto.TabIndex = 2
        '
        'TBTramo2Monto
        '
        Me.TBTramo2Monto.Location = New System.Drawing.Point(192, 169)
        Me.TBTramo2Monto.Name = "TBTramo2Monto"
        Me.TBTramo2Monto.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Decimal]
        Me.TBTramo2Monto.Size = New System.Drawing.Size(100, 21)
        Me.TBTramo2Monto.TabIndex = 4
        '
        'TBTramo3Monto
        '
        Me.TBTramo3Monto.Location = New System.Drawing.Point(192, 208)
        Me.TBTramo3Monto.Name = "TBTramo3Monto"
        Me.TBTramo3Monto.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Decimal]
        Me.TBTramo3Monto.Size = New System.Drawing.Size(100, 21)
        Me.TBTramo3Monto.TabIndex = 6
        '
        'TBTramo4Monto
        '
        Me.TBTramo4Monto.Location = New System.Drawing.Point(192, 245)
        Me.TBTramo4Monto.Name = "TBTramo4Monto"
        Me.TBTramo4Monto.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Decimal]
        Me.TBTramo4Monto.Size = New System.Drawing.Size(100, 21)
        Me.TBTramo4Monto.TabIndex = 8
        '
        'LComentario
        '
        Me.LComentario.AutoSize = True
        Me.LComentario.ForeColor = System.Drawing.Color.Red
        Me.LComentario.Location = New System.Drawing.Point(34, 291)
        Me.LComentario.Name = "LComentario"
        Me.LComentario.Size = New System.Drawing.Size(258, 13)
        Me.LComentario.TabIndex = 104
        Me.LComentario.Text = "Controle la Fecha de Vencimiento Antes de Confirmar"
        '
        'frmArregloPrestamo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(329, 370)
        Me.Controls.Add(Me.TBTramo4Monto)
        Me.Controls.Add(Me.TBTramo3Monto)
        Me.Controls.Add(Me.TBTramo2Monto)
        Me.Controls.Add(Me.TBTramo1Monto)
        Me.Controls.Add(Me.LComentario)
        Me.Controls.Add(Me.TBTramo4Cuotas)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TBTramo3Cuotas)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.LNroAsiento)
        Me.Controls.Add(Me.TBNroAsiento)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TBTramo2Cuotas)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TBTramo1Cuotas)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmArregloPrestamo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arreglo Prestamo"
        CType(Me.TBTramo1Monto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBTramo2Monto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBTramo3Monto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBTramo4Monto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TBTramo1Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TBTramo2Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TBNroAsiento As System.Windows.Forms.TextBox
    Friend WithEvents LNroAsiento As System.Windows.Forms.Label
    Friend WithEvents TBTramo3Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TBTramo4Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TBTramo1Monto As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents TBTramo2Monto As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents TBTramo3Monto As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents TBTramo4Monto As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents LComentario As System.Windows.Forms.Label
End Class
