﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmComprobanteAction
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmComprobanteAction))
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGBFechaVencimiento = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UDTFechaVencimiento = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.ULFechaVencimiento = New Infragistics.Win.Misc.UltraLabel()
        Me.UGBArreglo = New Infragistics.Win.Misc.UltraGroupBox()
        CType(Me.UGBFechaVencimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBFechaVencimiento.SuspendLayout()
        CType(Me.UDTFechaVencimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGBArreglo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGBFechaVencimiento
        '
        Me.UGBFechaVencimiento.Controls.Add(Me.UDTFechaVencimiento)
        Me.UGBFechaVencimiento.Controls.Add(Me.ULFechaVencimiento)
        Appearance2.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.FontData.BoldAsString = "True"
        Appearance2.ForeColor = System.Drawing.Color.White
        Me.UGBFechaVencimiento.HeaderAppearance = Appearance2
        Me.UGBFechaVencimiento.Location = New System.Drawing.Point(14, 14)
        Me.UGBFechaVencimiento.Name = "UGBFechaVencimiento"
        Me.UGBFechaVencimiento.Size = New System.Drawing.Size(652, 156)
        Me.UGBFechaVencimiento.TabIndex = 0
        Me.UGBFechaVencimiento.Text = "Actualización ultima fecha de vencimiento"
        Me.UGBFechaVencimiento.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UDTFechaVencimiento
        '
        Appearance1.Image = CType(resources.GetObject("Appearance1.Image"), Object)
        Appearance1.ImageHAlign = Infragistics.Win.HAlign.Center
        EditorButton1.Appearance = Appearance1
        EditorButton1.Width = 50
        Me.UDTFechaVencimiento.ButtonsRight.Add(EditorButton1)
        Me.UDTFechaVencimiento.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTFechaVencimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTFechaVencimiento.Location = New System.Drawing.Point(464, 34)
        Me.UDTFechaVencimiento.Name = "UDTFechaVencimiento"
        Me.UDTFechaVencimiento.Size = New System.Drawing.Size(176, 24)
        Me.UDTFechaVencimiento.TabIndex = 6
        '
        'ULFechaVencimiento
        '
        Me.ULFechaVencimiento.Location = New System.Drawing.Point(7, 22)
        Me.ULFechaVencimiento.Name = "ULFechaVencimiento"
        Me.ULFechaVencimiento.Size = New System.Drawing.Size(451, 36)
        Me.ULFechaVencimiento.TabIndex = 0
        Me.ULFechaVencimiento.Text = "Debe seleccionar una nueva fecha de vencimiento, la cual actualizara todas las fe" &
    "chas de los asientos del comprobante que sean mayores a la misma."
        '
        'UGBArreglo
        '
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance3.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance3.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.ForeColor = System.Drawing.Color.White
        Me.UGBArreglo.HeaderAppearance = Appearance3
        Me.UGBArreglo.Location = New System.Drawing.Point(14, 198)
        Me.UGBArreglo.Name = "UGBArreglo"
        Me.UGBArreglo.Size = New System.Drawing.Size(652, 66)
        Me.UGBArreglo.TabIndex = 1
        Me.UGBArreglo.Text = "Arreglo de asiento"
        Me.UGBArreglo.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'FrmComprobanteAction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 302)
        Me.Controls.Add(Me.UGBArreglo)
        Me.Controls.Add(Me.UGBFechaVencimiento)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmComprobanteAction"
        Me.Text = "Comprobante Acciones"
        CType(Me.UGBFechaVencimiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBFechaVencimiento.ResumeLayout(False)
        Me.UGBFechaVencimiento.PerformLayout()
        CType(Me.UDTFechaVencimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGBArreglo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents UGBFechaVencimiento As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents ULFechaVencimiento As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UDTFechaVencimiento As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UGBArreglo As Infragistics.Win.Misc.UltraGroupBox
End Class
