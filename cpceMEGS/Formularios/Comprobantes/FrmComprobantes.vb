﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class FrmComprobantes
    Private cnn As New ConsultaBD(True)
    Private DAComproba As MySqlDataAdapter
    'Private DATotales As MySqlDataAdapter

    'Private CMDComproba As MySqlCommandBuilder
    Private DSComproba As DataSet
    Private BSComproba As BindingSource
    Private BSTotales As BindingSource

    Private editarComprobante As Boolean = False
    Private editarAsiento As Boolean = False
    Private Sub FrmComprobantes_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Show()
        UDTHasta.Value = Date.Today
        UDTDesde.Value = Date.Today.AddDays(-7)
        MuestraComprobantes("com_nrocli=" & nPubNroCli & " and com_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:00' order by com_fecha DESC")
    End Sub
    Private Sub MuestraComprobantes(ByVal cCondicion As String)
        CButtonEditar.Visible = editarComprobante
        Try
            cnn.AbrirConexion()
            DAComproba = New MySqlDataAdapter
            DAComproba = cnn.consultaBDadapter(
                "(comproba left join procesos on pro_codigo=com_proceso) left join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc",
                "com_fecha,com_nrocom,com_nroasi,com_proceso CodPro,pro_nombre as proceso,com_unegos,com_nrocli,com_proceso,com_total,afi_nombre as Destinatario,com_estado as Est,com_zeta as Caja,com_nrotrabajo,com_asigrupal",
                cCondicion)
            'CMDComproba = New MySqlCommandBuilder(DAComproba)
            DSComproba = New DataSet
            DAComproba.Fill(DSComproba, "comproba")
            If editarComprobante Then
                DSComproba.Tables("comproba").Columns.Add("editarComproba", Type.GetType("System.String"))
            End If
            BSComproba = New BindingSource
            BSComproba.DataSource = DSComproba.Tables("comproba")

            Dim cCondicionTotal As String
            If editarComprobante Then
                cCondicionTotal = "tot_nroasi = " & UTEAsiento.Value & " AND tot_fecha BETWEEN '" & Format(CDate(DSComproba.Tables("comproba").Rows(BSComproba.Count - 1).Item("com_fecha").ToString), "yyyy-MM-dd") & " 00:00:00' and '" & Format(CDate(DSComproba.Tables("comproba").Rows(0).Item("com_fecha").ToString), "yyyy-MM-dd") & " 23:59:59'"
            Else
                cCondicionTotal = "tot_fecha between '" & Format(CDate(DSComproba.Tables("comproba").Rows(BSComproba.Count - 1).Item("com_fecha").ToString), "yyyy-MM-dd") & " 00:00:00' and '" & Format(CDate(DSComproba.Tables("comproba").Rows(0).Item("com_fecha").ToString), "yyyy-MM-dd") & " 23:59:59'"
            End If
            DAComproba = cnn.consultaBDadapter(
                "totales INNER JOIN plancuen ON pla_nropla=tot_nropla",
                "tot_item,tot_nrocuo,tot_nropla,pla_nombre,tot_debe,tot_haber,tot_fecven,tot_nrocheque,tot_fecdif,tot_nroasi,concat(tot_titulo,cast(tot_matricula as char)) as Destinatario,tot_unegos,tot_nrocli",
                cCondicionTotal & " ORDER BY tot_item,tot_nrocuo")

            DAComproba.Fill(DSComproba, "totales")
            If editarAsiento Then
                DSComproba.Tables("totales").Columns.Add("editar", Type.GetType("System.String"))
            End If
            BSTotales = New BindingSource
            BSTotales.DataSource = DSComproba.Tables("totales")
            'Uso editarComprobante para setear True o False
            Dim DRComprobantes As New DataRelation("MaestroDetalle", DSComproba.Tables("comproba").Columns("com_nroasi"), DSComproba.Tables("totales").Columns("tot_nroasi"), editarComprobante)
            '   DSComproba.Container
            DSComproba.Relations.Add(DRComprobantes)
            UGComproba.DataSource = DSComproba
            UGComproba.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
            'UGComproba.Text = "Comprobantes"
            With UGComproba.DisplayLayout
                With .Bands(0)
                    .Override.FilterUIType = FilterUIType.Default
                    .Columns(0).Header.Caption = "Fecha"
                    .Columns(0).Width = 120
                    '.Columns(0).Hidden = False
                    .Columns(1).Header.Caption = "Comprobante"
                    .Columns(1).Width = 85
                    .Columns(1).CellAppearance.TextHAlign = HAlign.Right
                    'La columna con ID 2 Asiento se utiliza al imprimir y en arreglo prestamo para obtener el nro de asiento
                    .Columns(2).Header.Caption = "Asiento"
                    .Columns(2).Width = 80
                    .Columns(2).CellAppearance.TextHAlign = HAlign.Right
                    'La columna con ID 3 Proceso se utiliza al imprimir para obtener el codigo proceso
                    .Columns(3).Header.Caption = "Proceso"
                    .Columns(3).Width = 65
                    .Columns(3).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(4).Header.Caption = "Descripcion"
                    .Columns(4).Width = 300
                    'La columna con ID 5 Institucion se utiliza en arreglo prestamo para obtener el unegos
                    .Columns(5).Header.Caption = "Institucion"
                    .Columns(5).Width = 30
                    .Columns(5).CellAppearance.TextHAlign = HAlign.Center
                    .Columns(6).Header.Caption = "Delegacion"
                    .Columns(6).Width = 30
                    .Columns(6).CellAppearance.TextHAlign = HAlign.Center
                    .Columns(7).Hidden = True
                    'La columna con ID 8 Importe se utiliza en arreglo prestamo para obtener el total
                    .Columns(8).Header.Caption = "Importe"
                    .Columns(8).Width = 90
                    .Columns(8).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(8).Format = "c"
                    .Columns(8).PromptChar = ""
                    .Columns(8).Style = ColumnStyle.Double
                    .Columns(8).FormatInfo = Globalization.CultureInfo.CurrentCulture
                    .Columns(9).Width = 250
                    .Columns(10).Width = 30
                    .Columns(11).Width = 40
                    .Columns(12).Width = 90
                    .Columns(13).Width = 90
                    If editarComprobante Then
                        .Columns("editarComproba").Header.Caption = "Editar"
                        .Columns("editarComproba").Width = 40
                        .Columns("editarComproba").Style = ColumnStyle.Button
                        .Columns("editarComproba").ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                        .Columns("editarComproba").CellButtonAppearance.Image = ImageList.Images(0)
                        .Columns("editarComproba").CellButtonAppearance.ImageHAlign = HAlign.Center
                    End If
                End With
                With .Bands(1)
                    .Override.FilterUIType = FilterUIType.Default
                    .Columns(0).Header.Caption = "Item"
                    .Columns(0).Width = 30

                    .Columns(1).Header.Caption = "Cuota"
                    .Columns(1).Width = 30

                    .Columns(2).Header.Caption = "Cuenta"
                    .Columns(2).Width = 80

                    .Columns(3).Header.Caption = "Descripción"
                    .Columns(3).Width = 220

                    .Columns(4).Header.Caption = "Debe"
                    .Columns(4).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(4).Width = 90
                    .Columns(4).Format = "c"
                    .Columns(4).PromptChar = ""
                    .Columns(4).Style = ColumnStyle.Double
                    .Columns(4).FormatInfo = Globalization.CultureInfo.CurrentCulture

                    .Columns(5).Header.Caption = "Haber"
                    .Columns(5).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(5).Width = 90
                    .Columns(5).Format = "c"
                    .Columns(5).PromptChar = ""
                    .Columns(5).Style = ColumnStyle.Double
                    .Columns(5).FormatInfo = Globalization.CultureInfo.CurrentCulture

                    .Columns(6).Header.Caption = "Vencimiento"
                    .Columns(6).CellAppearance.TextHAlign = HAlign.Center
                    .Columns(6).Width = 80
                    .Columns(6).Format = "d"
                    .Columns(6).PromptChar = ""
                    .Columns(6).Style = ColumnStyle.Date

                    .Columns(7).Header.Caption = "NroCheque"

                    .Columns(8).Header.Caption = "Diferida"
                    .Columns(8).CellAppearance.TextHAlign = HAlign.Center
                    .Columns(8).Width = 80
                    .Columns(8).Format = "d"
                    .Columns(8).PromptChar = ""
                    .Columns(8).Style = ColumnStyle.Date

                    .Columns(9).Hidden = True
                    If editarAsiento Then
                        .Columns("editar").Width = 40
                        .Columns("editar").Style = ColumnStyle.Button
                        .Columns("editar").ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                        .Columns("editar").CellButtonAppearance.Image = ImageList.Images(0)
                        .Columns("editar").CellButtonAppearance.ImageHAlign = HAlign.Center
                    End If

                    .Columns(0).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(0).Header.Appearance.ForeColor = Color.Black
                    .Columns(0).Header.Appearance.BackColor = Color.Red
                    .Columns(0).Header.Appearance.BackColor2 = Color.White
                    .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(1).Header.Appearance.ForeColor = Color.Black
                    .Columns(1).Header.Appearance.BackColor = Color.Red
                    .Columns(1).Header.Appearance.BackColor2 = Color.White
                    .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(2).Header.Appearance.ForeColor = Color.Black
                    .Columns(2).Header.Appearance.BackColor = Color.Red
                    .Columns(2).Header.Appearance.BackColor2 = Color.White
                    .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(3).Header.Appearance.ForeColor = Color.Black
                    .Columns(3).Header.Appearance.BackColor = Color.Red
                    .Columns(3).Header.Appearance.BackColor2 = Color.White
                    .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(4).Header.Appearance.ForeColor = Color.Black
                    .Columns(4).Header.Appearance.BackColor = Color.Red
                    .Columns(4).Header.Appearance.BackColor2 = Color.White
                    .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(5).Header.Appearance.ForeColor = Color.Black
                    .Columns(5).Header.Appearance.BackColor = Color.Red
                    .Columns(5).Header.Appearance.BackColor2 = Color.White
                    .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(6).Header.Appearance.ForeColor = Color.Black
                    .Columns(6).Header.Appearance.BackColor = Color.Red
                    .Columns(6).Header.Appearance.BackColor2 = Color.White
                    .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(7).Header.Appearance.ForeColor = Color.Black
                    .Columns(7).Header.Appearance.BackColor = Color.Red
                    .Columns(7).Header.Appearance.BackColor2 = Color.White
                    .Columns(8).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(8).Header.Appearance.ForeColor = Color.Black
                    .Columns(8).Header.Appearance.BackColor = Color.Red
                    .Columns(8).Header.Appearance.BackColor2 = Color.White
                    .Columns(9).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(9).Header.Appearance.ForeColor = Color.Black
                    .Columns(9).Header.Appearance.BackColor = Color.Red
                    .Columns(9).Header.Appearance.BackColor2 = Color.White
                    .Columns(10).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(10).Header.Appearance.ForeColor = Color.Black
                    .Columns(10).Header.Appearance.BackColor = Color.Red
                    .Columns(10).Header.Appearance.BackColor2 = Color.White
                End With
            End With
            cnn.CerrarConexion()
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MuestraDetallesComprobantes(ByVal cCondicion As String)
        Try
            cnn.AbrirConexion()
            'Dim cCondicionTotal As String
            'cCondicionTotal = "tot_fecha between '" & Format(CDate(DSComproba.Tables("comproba").Rows(BSComproba.Count - 1).Item("com_fecha").ToString), "yyyy-MM-dd") & " 00:00:00' and '" & Format(CDate(DSComproba.Tables("comproba").Rows(0).Item("com_fecha").ToString), "yyyy-MM-dd") & " 23:59:59'"
            DAComproba = New MySqlDataAdapter
            DAComproba = cnn.consultaBDadapter("totales", , cCondicion)

            DAComproba.Fill(DSComproba, "detalles")
            BSTotales = New BindingSource
            BSTotales.DataSource = DSComproba.Tables("detalles")

            UGComproba.DataSource = BSTotales
            UGComproba.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
            UGComproba.Text = "Detalles"

            cnn.CerrarConexion()
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub UGComproba_InitializeLayout(ByVal sender As Object, ByVal e As InitializeLayoutEventArgs) Handles UGComproba.InitializeLayout
        Try
            If e.Layout.Bands.Count > 1 Then
                e.Layout.Bands(1).Columns(1).FilterOperatorLocation = FilterOperatorLocation.Hidden
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UGComproba_InitializeRow(ByVal sender As Object, ByVal e As InitializeRowEventArgs) Handles UGComproba.InitializeRow
        UGComproba.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
        Try
            If UGComproba.DisplayLayout.Bands.Count > 1 Then
                If e.Row.Band.Index = 0 Then
                    If e.Row.Cells("Est").Value = "9" Then
                        'e.Row.Appearance.BackGradientStyle = GradientStyle.Vertical
                        e.Row.Appearance.BackColor = Color.Red
                        e.Row.Appearance.ForeColor = Color.Black
                        e.Row.Appearance.BackColor2 = Color.Red
                    End If
                End If
                If e.Row.Band.Layout.IsPrintLayout Or e.Row.Band.Layout.IsExportLayout Then
                    'since the ultragrid doesn't carry the selected state of a row to the print or export you have to find the real row that is
                    'being displayed to the user
                    Dim realGridRow As UltraGridRow = UGComproba.GetRowFromPrintRow(e.Row)
                    'hide the row that is being used for printing/exporting so it doesn't show up
                    e.Row.Hidden = Not realGridRow.Selected
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UGComproba_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles UGComproba.DoubleClick
        If UGComproba.ActiveRow.Band.Index = 1 Then
            'Cells: ID 5 = Institucion. ID 2 = Asiento. ID 8 = Importe
            Dim frmPrestamo As New frmArregloPrestamo(cnn, UGComproba.ActiveRow.ParentRow.Cells(5).Value, UGComproba.ActiveRow.ParentRow.Cells(2).Value, UGComproba.ActiveRow.ParentRow.Cells(8).Value)
            If (frmPrestamo.ShowDialog() = DialogResult.OK) Then
                UTEAsiento.Text = frmPrestamo.GetAsiento()
            End If
        End If
    End Sub
    Private Sub UGComproba_ClickCellButton(ByVal sender As Object, ByVal e As CellEventArgs) Handles UGComproba.ClickCellButton
        ClickActiveCell()
    End Sub
    Private Sub ClickActiveCell()
        Dim e As UltraGridCell = UGComproba.ActiveCell
        If e.Band.Index = 0 Then 'fila 0
            If e.Column.Index = 14 Then 'boton pincel
                Dim frmComprobanteAction As New FrmComprobanteAction(DAComproba, DSComproba)
                frmComprobanteAction.ShowDialog()
                If frmComprobanteAction.DialogResult = DialogResult.OK Then
                    MessageBox.Show(frmComprobanteAction.GetResMessage())
                End If
            End If
        End If
    End Sub

    Private Sub CButton1_Click(sender As Object, e As EventArgs) Handles CButton1.Click
        Try
            Dim imprimir As New Impresiones
            'Cells: ID 2 = Asiento. ID 3 = Proceso
            imprimir.ImprimirAsiento(UGComproba.ActiveRow.Cells(2).Value, UGComproba.ActiveRow.Cells(3).Value)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CButtonImprimirA5_Click(sender As Object, e As EventArgs) Handles CButtonImprimirA5.Click
        Try
            Dim imprimir As New Impresiones
            'Cells: ID 2 = Asiento. ID 3 = Proceso
            imprimir.ImprimirAsiento(UGComproba.ActiveRow.Cells(2).Value, UGComproba.ActiveRow.Cells(3).Value, "A5")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cbtRecibos_Click(sender As Object, e As EventArgs) Handles cbtRecibos.Click
        Dim impRecibo As New Impresiones
        impRecibo.ImprimirRecibo(DSComproba.Tables(0).Rows(UGComproba.ActiveRow.Index).Item("com_nroasi"))
    End Sub

    Private Sub CBtAnular_Click(sender As Object, e As EventArgs) Handles CBtAnular.Click
        If MessageBox.Show("Seguro de anular este comprobante", "Anulaciones", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
            AnulaComprobante()
        End If
    End Sub

    Private Sub AnulaComprobante()
        Dim lHaceAnulacion As Boolean = False
        Dim rowAnu As UltraGridRow = UGComproba.ActiveRow
        Dim miTrans As MySqlTransaction

        cnn.CajaActiva()

        Dim nroAsiento As Long = rowAnu.Cells("com_nroasi").Value
        Dim codProceso As String = rowAnu.Cells("codpro").Value

        '1 Listado 2 Anular Orden de Pago (proceso 02P035) 3 Anular
        'Si Accion = 3 y tiene permiso "D" puede eliminar todos los comprobantes
        If controlAcceso(2, 3, "D", False) = True Then
            lHaceAnulacion = True
        Else
            If controlAcceso(2, 1, "D", False) = True Then
                'Si Accion = 1 y tiene permiso "D" puede eliminar comprobantes del dia
                If rowAnu.Cells("Caja").Value = 0 Then
                    lHaceAnulacion = True
                ElseIf rowAnu.Cells("Caja").Value = cnn.Zeta Then
                    lHaceAnulacion = True
                End If
            ElseIf controlAcceso(2, 2, "D", False) = True Then
                'Si Accion = 2 y tiene permiso "D" puede eliminar comprobantes por
                'Orden de Pago 02H001 o Liquidacion 02LIHN
                If codProceso = "02H001" Or codProceso = "02LIHN" Then
                    lHaceAnulacion = True
                End If
            End If
        End If
        If lHaceAnulacion Then
            cnn.AbrirConexion()
            miTrans = cnn.InicioTransaccion
            Try
                cnn.ReplaceBD("UPDATE comproba SET com_estado='9' WHERE com_nroasi=" & nroAsiento & " and com_nrocli=" & rowAnu.Cells("com_nrocli").Value)
                cnn.ReplaceBD("UPDATE totales SET tot_estado='9' WHERE tot_nroasi=" & nroAsiento & " and tot_nrocli=" & rowAnu.Cells("com_nrocli").Value)
                'If rowAnu.Cells("com_nrotrabajo").Value <> "" Then
                'cnn.ReplaceBD("UPDATE obleas SET obl_estado='',obl_asiento=0 WHERE obl_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                'cnn.ReplaceBD("UPDATE totales SET tot_imppag='0' WHERE tot_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                'End If
                If codProceso = "02H001" Then
                    'si es orden de pago libero todas las liquidaciones
                    cnn.ReplaceBD("UPDATE totales SET tot_imppag='0',tot_asicancel='0' WHERE tot_asicancel=" & nroAsiento)
                ElseIf codProceso = "02LIHN" Then
                    'Si el asiento es liquidacion entonces cambio el estado de la oblea para que pueda liquidar nuevamente
                    cnn.ReplaceBD("UPDATE obleas SET obl_estado='' WHERE obl_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                    cnn.ReplaceBD("UPDATE totales SET tot_imppag='0' WHERE tot_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                ElseIf codProceso = "02R120" Then
                    'Si el asiento es 01R120 es de trabajo consulto si fue utilizado para cobrar alguno asi revierto el estado
                    If rowAnu.Cells("com_nrotrabajo").Value <> "" Then
                        'si tiene codigo de barra trabajo asignado
                        cnn.ReplaceBD("UPDATE obleas SET obl_estado='',obl_asiento=0 WHERE obl_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                        cnn.ReplaceBD("UPDATE totales SET tot_imppag='0' WHERE tot_nrolegali=" & rowAnu.Cells("com_nrotrabajo").Value)
                    End If
                    Dim estadoTrabajo As Integer = cnn.ExisteTrabajoByNroAsientoGetEstado(nroAsiento)
                    If estadoTrabajo = 3 Then 'Trabajo PAGADO
                        cnn.ReplaceBD("UPDATE trabajo SET tra_estado = '1', tra_nroasi = NULL WHERE tra_nroasi=" & nroAsiento)
                    ElseIf estadoTrabajo > 3 Then 'Trabajo PRESENTADO
                        Throw New Exception("El comprobante no puede ser anulado porque fue utilizado en una presentación de trabajo (Obleas).")
                    End If
                End If
                miTrans.Commit()
                rowAnu.Cells("Est").Value = "9"
                MessageBox.Show("Anulación efectuada satifactoriamente", "Anulaciónes", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                miTrans.Rollback()
                MessageBox.Show(ex.Message)
            End Try
            cnn.CerrarConexion()
        Else
            MessageBox.Show("El comprobante es de una caja diferente. NO se puede anular", "Anulación", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub UDTHasta_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UDTHasta.EditorButtonClick
        ' si estoy en central muestro todo
        Dim cMasCondicion As String = ""
        If CheckBox1.Checked Then
            If UNEdeleg.Value <> 0 Then
                cMasCondicion = " and tot_nrocli=" & UNEdeleg.Value
            End If
            If UTEproceso.Text.Trim <> "" Then
                cMasCondicion += " and tot_proceso like '" & UTEproceso.Text.Trim & "%'"
            End If

            MuestraDetallesComprobantes("tot_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & " 00:00:00' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:59'" & cMasCondicion & " order by tot_fecha DESC")
        Else
            If UNEdeleg.Value <> 0 Then
                cMasCondicion = " and com_nrocli=" & UNEdeleg.Value
            End If
            If UTEproceso.Text.Trim <> "" Then
                cMasCondicion += " and com_proceso like '" & UTEproceso.Text.Trim & "%'"
            End If

            If nPubNroCli = 1 Then
                MuestraComprobantes("mid(com_fecha,1,10) between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & "'" & cMasCondicion & " order by com_fecha DESC")
            Else
                MuestraComprobantes("com_nrocli=" & nPubNroCli & " and mid(com_fecha,1,10) between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & "'" & cMasCondicion & " order by com_fecha DESC")
            End If
        End If
    End Sub

    Private Sub UTEAsiento_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UTEAsiento.EditorButtonClick
        If controlAcceso(2, 4, "U", False) = True Then
            editarComprobante = True
        End If
        If controlAcceso(2, 5, "U", False) = True Then
            editarAsiento = True
        End If

        Dim condicion As String

        If CheckBox1.Checked Then
            condicion = "tot_nroasi=" & UTEAsiento.Value
            MuestraDetallesComprobantes(condicion)
        Else
            condicion = "com_nroasi=" & UTEAsiento.Value
            MuestraComprobantes(condicion)
        End If

        editarComprobante = False
        editarAsiento = False
    End Sub

    Private Sub CBtExportarProceso_Click(sender As Object, e As EventArgs) Handles CBtExportarProceso.Click
        Dim cListado(0) As String
        cListado(0) = "Listado de Comprobantes"
        Dim clsExportarExcel As New ExportarExcel
        clsExportarExcel.ExportarDatosExcel(UGComproba, cListado)
    End Sub

    Private Sub CheckBox1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBox1.Click
        If CheckBox1.Checked Then
            CButton1.Visible = False
            CBtAnular.Visible = False
            cbtRecibos.Visible = False
        Else
            CButton1.Visible = True
            CBtAnular.Visible = True
            cbtRecibos.Visible = True
        End If
    End Sub

    Private Sub CButtonEditar_Click(sender As Object, e As EventArgs) Handles CButtonEditar.Click
        'MessageBox.Show(DSComproba.Tables("comproba").Rows(0).Item("com_nroasi"))
        MessageBox.Show("Funcion en desarrollo")
    End Sub
End Class