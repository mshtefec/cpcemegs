﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComprobantes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton4 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmComprobantes))
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGComproba = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UDTDesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDTHasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ImpresionPrint = New System.Drawing.Printing.PrintDocument()
        Me.ImpresionDialog = New System.Windows.Forms.PrintDialog()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UTEAsiento = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CBtExportarProceso = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.UNEdeleg = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UTEproceso = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CButtonImprimirA5 = New System.Windows.Forms.Button()
        Me.CBtAnular = New System.Windows.Forms.Button()
        Me.cbtRecibos = New System.Windows.Forms.Button()
        Me.CButton1 = New System.Windows.Forms.Button()
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.CButtonEditar = New System.Windows.Forms.Button()
        CType(Me.UGComproba, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEdeleg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEproceso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGComproba
        '
        Me.UGComproba.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGComproba.DisplayLayout.Appearance = Appearance1
        Me.UGComproba.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGComproba.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGComproba.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGComproba.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGComproba.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGComproba.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGComproba.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGComproba.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGComproba.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGComproba.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGComproba.Location = New System.Drawing.Point(0, 52)
        Me.UGComproba.Name = "UGComproba"
        Me.UGComproba.Size = New System.Drawing.Size(1084, 462)
        Me.UGComproba.TabIndex = 1
        '
        'UDTDesde
        '
        Me.UDTDesde.DateTime = New Date(2013, 10, 8, 0, 0, 0, 0)
        Me.UDTDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTDesde.Location = New System.Drawing.Point(551, 15)
        Me.UDTDesde.Name = "UDTDesde"
        Me.UDTDesde.Size = New System.Drawing.Size(104, 24)
        Me.UDTDesde.TabIndex = 4
        Me.UDTDesde.Value = New Date(2013, 10, 8, 0, 0, 0, 0)
        '
        'UDTHasta
        '
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Center
        EditorButton4.Appearance = Appearance9
        EditorButton4.Width = 50
        Me.UDTHasta.ButtonsRight.Add(EditorButton4)
        Me.UDTHasta.DateTime = New Date(2013, 10, 8, 0, 0, 0, 0)
        Me.UDTHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTHasta.Location = New System.Drawing.Point(657, 15)
        Me.UDTHasta.Name = "UDTHasta"
        Me.UDTHasta.Size = New System.Drawing.Size(151, 24)
        Me.UDTHasta.TabIndex = 5
        Me.UDTHasta.Value = New Date(2013, 10, 8, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(458, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Rango Fecha:"
        '
        'ImpresionDialog
        '
        Me.ImpresionDialog.UseEXDialog = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(862, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Asiento:"
        '
        'UTEAsiento
        '
        Appearance7.Image = CType(resources.GetObject("Appearance7.Image"), Object)
        EditorButton2.Appearance = Appearance7
        EditorButton2.Width = 50
        Me.UTEAsiento.ButtonsRight.Add(EditorButton2)
        Me.UTEAsiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEAsiento.Location = New System.Drawing.Point(918, 17)
        Me.UTEAsiento.Name = "UTEAsiento"
        Me.UTEAsiento.Size = New System.Drawing.Size(157, 24)
        Me.UTEAsiento.TabIndex = 12
        '
        'CBtExportarProceso
        '
        Me.CBtExportarProceso.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportarProceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBtExportarProceso.Image = Global.cpceMEGS.My.Resources.Resources.PrintSetup_11011
        Me.CBtExportarProceso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtExportarProceso.Location = New System.Drawing.Point(652, 520)
        Me.CBtExportarProceso.Name = "CBtExportarProceso"
        Me.CBtExportarProceso.Size = New System.Drawing.Size(132, 38)
        Me.CBtExportarProceso.TabIndex = 126
        Me.CBtExportarProceso.Text = "Exportar"
        Me.CBtExportarProceso.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 16)
        Me.Label3.TabIndex = 127
        Me.Label3.Text = "Delegación:"
        '
        'UNEdeleg
        '
        Me.UNEdeleg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEdeleg.Location = New System.Drawing.Point(90, 15)
        Me.UNEdeleg.MaskInput = "n"
        Me.UNEdeleg.MaxValue = 9
        Me.UNEdeleg.MinValue = 1
        Me.UNEdeleg.Name = "UNEdeleg"
        Me.UNEdeleg.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEdeleg.Size = New System.Drawing.Size(43, 24)
        Me.UNEdeleg.TabIndex = 128
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(139, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 16)
        Me.Label4.TabIndex = 129
        Me.Label4.Text = "Proceso:"
        '
        'UTEproceso
        '
        Me.UTEproceso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEproceso.Location = New System.Drawing.Point(198, 15)
        Me.UTEproceso.Name = "UTEproceso"
        Me.UTEproceso.Size = New System.Drawing.Size(105, 24)
        Me.UTEproceso.TabIndex = 130
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(328, 19)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(108, 20)
        Me.CheckBox1.TabIndex = 131
        Me.CheckBox1.Text = "Solo Detalles"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CButtonImprimirA5
        '
        Me.CButtonImprimirA5.BackColor = System.Drawing.Color.Transparent
        Me.CButtonImprimirA5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CButtonImprimirA5.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.CButtonImprimirA5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButtonImprimirA5.Location = New System.Drawing.Point(185, 520)
        Me.CButtonImprimirA5.Name = "CButtonImprimirA5"
        Me.CButtonImprimirA5.Size = New System.Drawing.Size(155, 39)
        Me.CButtonImprimirA5.TabIndex = 132
        Me.CButtonImprimirA5.Text = "Imprimir A5"
        Me.CButtonImprimirA5.UseVisualStyleBackColor = False
        '
        'CBtAnular
        '
        Me.CBtAnular.BackColor = System.Drawing.Color.Transparent
        Me.CBtAnular.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBtAnular.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Critical_32xMD_color
        Me.CBtAnular.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAnular.Location = New System.Drawing.Point(504, 520)
        Me.CBtAnular.Name = "CBtAnular"
        Me.CBtAnular.Size = New System.Drawing.Size(132, 39)
        Me.CBtAnular.TabIndex = 10
        Me.CBtAnular.Text = "Anular"
        Me.CBtAnular.UseVisualStyleBackColor = False
        '
        'cbtRecibos
        '
        Me.cbtRecibos.BackColor = System.Drawing.Color.Transparent
        Me.cbtRecibos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.cbtRecibos.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.cbtRecibos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbtRecibos.Location = New System.Drawing.Point(356, 520)
        Me.cbtRecibos.Name = "cbtRecibos"
        Me.cbtRecibos.Size = New System.Drawing.Size(132, 39)
        Me.cbtRecibos.TabIndex = 9
        Me.cbtRecibos.Text = "Recibos"
        Me.cbtRecibos.UseVisualStyleBackColor = False
        '
        'CButton1
        '
        Me.CButton1.BackColor = System.Drawing.Color.Transparent
        Me.CButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CButton1.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.CButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton1.Location = New System.Drawing.Point(37, 520)
        Me.CButton1.Name = "CButton1"
        Me.CButton1.Size = New System.Drawing.Size(132, 39)
        Me.CButton1.TabIndex = 3
        Me.CButton1.Text = "Imprimir"
        Me.CButton1.UseVisualStyleBackColor = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "PencilTool_206.png")
        '
        'CButtonEditar
        '
        Me.CButtonEditar.BackColor = System.Drawing.Color.Transparent
        Me.CButtonEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButtonEditar.Image = Global.cpceMEGS.My.Resources.Resources.PencilAngled_32xSM_color
        Me.CButtonEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButtonEditar.Location = New System.Drawing.Point(800, 520)
        Me.CButtonEditar.Name = "CButtonEditar"
        Me.CButtonEditar.Size = New System.Drawing.Size(150, 38)
        Me.CButtonEditar.TabIndex = 133
        Me.CButtonEditar.Text = "Editar"
        Me.CButtonEditar.UseVisualStyleBackColor = False
        Me.CButtonEditar.Visible = False
        '
        'FrmComprobantes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1084, 562)
        Me.Controls.Add(Me.CButtonEditar)
        Me.Controls.Add(Me.CButtonImprimirA5)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.UTEproceso)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.UNEdeleg)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBtExportarProceso)
        Me.Controls.Add(Me.UTEAsiento)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CBtAnular)
        Me.Controls.Add(Me.cbtRecibos)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UDTHasta)
        Me.Controls.Add(Me.UDTDesde)
        Me.Controls.Add(Me.CButton1)
        Me.Controls.Add(Me.UGComproba)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmComprobantes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comprobantes"
        CType(Me.UGComproba, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEdeleg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEproceso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGComproba As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CButton1 As System.Windows.Forms.Button
    Friend WithEvents UDTDesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTHasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImpresionPrint As System.Drawing.Printing.PrintDocument
    Friend WithEvents ImpresionDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents cbtRecibos As System.Windows.Forms.Button
    Friend WithEvents CBtAnular As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UTEAsiento As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents CBtExportarProceso As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UNEdeleg As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UTEproceso As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CButtonImprimirA5 As System.Windows.Forms.Button
    Friend WithEvents ImageList As ImageList
    Friend WithEvents CButtonEditar As Button
End Class
