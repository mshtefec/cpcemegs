﻿Imports MySql.Data.MySqlClient
Public Class frmArregloPrestamo
    Private c_cnn As ConsultaBD
    Private r_institucion As Integer
    Private r_nro_asiento As String
    Private r_total As Double
    Private DTTot As DataTable
    Private DATot As MySqlDataAdapter
    Private controlTramo1 As Boolean
    Private controlTramo2 As Boolean
    Private controlTramo3 As Boolean
    Private controlTramo4 As Boolean
    Private montoTramo1 As Double
    Private montoTramo2 As Double
    Private montoTramo3 As Double
    Private montoTramo4 As Double
    Public Sub New(ByVal cConexion As ConsultaBD, ByVal rowInstitucion As Integer, ByVal rowNroAsiento As String, ByVal rowTotal As Double)
        InitializeComponent()
        c_cnn = cConexion
        r_institucion = rowInstitucion
        r_nro_asiento = rowNroAsiento
        r_total = rowTotal
    End Sub
    Public ReadOnly Property GetAsiento As String
        Get
            Return TBNroAsiento.Text
        End Get
    End Property
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TBTramo1Cuotas.KeyDown
        If e.KeyCode = Keys.Enter Then
            TBTramo1Monto.Text = DTTot.Rows(0).Item("tot_debe") / CInt(TBTramo1Cuotas.Text)
        End If
    End Sub

    Private Sub frmArregloPrestamo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        TBNroAsiento.Text = r_nro_asiento
        DATot = c_cnn.consultaBDadapter("totales", "*", "tot_unegos=" & r_institucion & " and tot_nrocli=1 and tot_nroasi=" & r_nro_asiento & " and tot_item=1")
        DTTot = New DataTable
        DATot.Fill(DTTot)
        DateTimePicker1.Value = Format(CDate(DTTot.Rows(0).Item("tot_fecha").ToString), "yyyy-MM-dd HH:mm:ss")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Button1.Click
        controlTramos()
        If controlTramo1 Then
            If c_cnn.AbrirConexion Then
                Dim transaction As MySqlTransaction = c_cnn.InicioTransaccion
                Try
                    Dim dFechaVencimento As Date
                    dFechaVencimento = DateSerial(DateTimePicker1.Value.AddMonths(1).Year, DateTimePicker1.Value.AddMonths(1).Month, 0)
                    c_cnn.actualizarBD(
                        "update totales set tot_debe='" & montoTramo1.ToString.Replace(",", ".") &
                        "',tot_fecven='" & Format(DateTimePicker1.Value, "yyyy-MM-dd") & "' where tot_unegos=" &
                        r_institucion & " and tot_nrocli=1 and tot_nroasi=" & r_nro_asiento & " and tot_item=1"
                    )
                    'TRAMO 1
                    Dim nCuo As Integer = 0
                    If CInt(TBTramo1Cuotas.Text) > 0 Then
                        For nCuo = 2 To TBTramo1Cuotas.Text
                            dFechaVencimento = DateSerial(dFechaVencimento.AddMonths(1).Year, dFechaVencimento.AddMonths(1).Month, 10)
                            c_cnn.ReplaceBD(
                                "insert into totales set tot_unegos='" & DTTot.Rows(0).Item("tot_unegos") & "'," &
                                "tot_proceso='" & DTTot.Rows(0).Item("tot_proceso") & "'," &
                                "tot_nrocli='" & DTTot.Rows(0).Item("tot_nrocli") & "'," &
                                "tot_nrocom='" & DTTot.Rows(0).Item("tot_nrocom") & "'," &
                                "tot_item='" & DTTot.Rows(0).Item("tot_item") & "'," &
                                "tot_nropla='" & DTTot.Rows(0).Item("tot_nropla") & "'," &
                                "tot_subpla='" & DTTot.Rows(0).Item("tot_subpla") & "'," &
                                "tot_titulo='" & DTTot.Rows(0).Item("tot_titulo") & "'," &
                                "tot_matricula='" & DTTot.Rows(0).Item("tot_matricula") & "'," &
                                "tot_tipdoc='" & DTTot.Rows(0).Item("tot_tipdoc") & "'," &
                                "tot_nrodoc='" & DTTot.Rows(0).Item("tot_nrodoc") & "'," &
                                "tot_nroope='" & nPubNroOperador & "'," &
                                "tot_nrocuo='" & nCuo & "'," &
                                "tot_fecha='" & Format(CDate(DTTot.Rows(0).Item("tot_fecha").ToString), "yyyy-MM-dd HH:mm:ss") & "'," &
                                "tot_fecven='" & Format(dFechaVencimento, "yyyy-MM-dd") & "'," &
                                "tot_fecalt='" & c_cnn.FechaHoraActual & "'," &
                                "tot_debe='" & montoTramo1.ToString.Replace(",", ".") & "'," &
                                "tot_haber='" & 0 & "'," &
                                "tot_imppag='" & 0 & "'," &
                                "tot_nroasi='" & DTTot.Rows(0).Item("tot_nroasi") & "'," &
                                "tot_asigrupal='" & 0 & "'," &
                                "tot_caja='" & DTTot.Rows(0).Item("tot_caja") & "'," &
                                "tot_zeta='" & DTTot.Rows(0).Item("tot_zeta") & "'," &
                                "tot_concepto='" & DTTot.Rows(0).Item("tot_concepto") & "'"
                            )
                        Next
                        'TRAMO 2
                        If controlTramo2 AndAlso CInt(TBTramo2Cuotas.Text) > 0 Then
                            If CInt(TBTramo1Cuotas.Text) = 1 Then 'Si tramo 1 tiene solo 1 cuota entra
                                dFechaVencimento = DateSerial(dFechaVencimento.Year, dFechaVencimento.Month, 10)
                            End If
                            For nCuo1 As Integer = 1 To TBTramo2Cuotas.Text
                                dFechaVencimento = dFechaVencimento.AddMonths(1)
                                c_cnn.ReplaceBD(
                                    "insert into totales set tot_unegos='" & DTTot.Rows(0).Item("tot_unegos") & "'," &
                                    "tot_proceso='" & DTTot.Rows(0).Item("tot_proceso") & "'," &
                                    "tot_nrocli='" & DTTot.Rows(0).Item("tot_nrocli") & "'," &
                                    "tot_nrocom='" & DTTot.Rows(0).Item("tot_nrocom") & "'," &
                                    "tot_item='" & DTTot.Rows(0).Item("tot_item") & "'," &
                                    "tot_nropla='" & DTTot.Rows(0).Item("tot_nropla") & "'," &
                                    "tot_subpla='" & DTTot.Rows(0).Item("tot_subpla") & "'," &
                                    "tot_titulo='" & DTTot.Rows(0).Item("tot_titulo") & "'," &
                                    "tot_matricula='" & DTTot.Rows(0).Item("tot_matricula") & "'," &
                                    "tot_tipdoc='" & DTTot.Rows(0).Item("tot_tipdoc") & "'," &
                                    "tot_nrodoc='" & DTTot.Rows(0).Item("tot_nrodoc") & "'," &
                                    "tot_nroope='" & nPubNroOperador & "'," &
                                    "tot_nrocuo='" & nCuo & "'," &
                                    "tot_fecha='" & Format(CDate(DTTot.Rows(0).Item("tot_fecha").ToString), "yyyy-MM-dd HH:mm:ss") & "'," &
                                    "tot_fecven='" & Format(dFechaVencimento, "yyyy-MM-dd") & "'," &
                                    "tot_fecalt='" & c_cnn.FechaHoraActual & "'," &
                                    "tot_debe='" & montoTramo2.ToString.Replace(",", ".") & "'," &
                                    "tot_haber='" & 0 & "'," &
                                    "tot_imppag='" & 0 & "'," &
                                    "tot_nroasi='" & DTTot.Rows(0).Item("tot_nroasi") & "'," &
                                    "tot_asigrupal='" & 0 & "'," &
                                    "tot_caja='" & DTTot.Rows(0).Item("tot_caja") & "'," &
                                    "tot_zeta='" & DTTot.Rows(0).Item("tot_zeta") & "'," &
                                    "tot_concepto='" & DTTot.Rows(0).Item("tot_concepto") & "'"
                                )
                                nCuo += 1
                            Next
                            'TRAMO 3
                            If controlTramo3 AndAlso CInt(TBTramo3Cuotas.Text) > 0 Then
                                For nCuo2 As Integer = 1 To TBTramo3Cuotas.Text
                                    dFechaVencimento = dFechaVencimento.AddMonths(1)
                                    c_cnn.ReplaceBD(
                                        "insert into totales set tot_unegos='" & DTTot.Rows(0).Item("tot_unegos") & "'," &
                                        "tot_proceso='" & DTTot.Rows(0).Item("tot_proceso") & "'," &
                                        "tot_nrocli='" & DTTot.Rows(0).Item("tot_nrocli") & "'," &
                                        "tot_nrocom='" & DTTot.Rows(0).Item("tot_nrocom") & "'," &
                                        "tot_item='" & DTTot.Rows(0).Item("tot_item") & "'," &
                                        "tot_nropla='" & DTTot.Rows(0).Item("tot_nropla") & "'," &
                                        "tot_subpla='" & DTTot.Rows(0).Item("tot_subpla") & "'," &
                                        "tot_titulo='" & DTTot.Rows(0).Item("tot_titulo") & "'," &
                                        "tot_matricula='" & DTTot.Rows(0).Item("tot_matricula") & "'," &
                                        "tot_tipdoc='" & DTTot.Rows(0).Item("tot_tipdoc") & "'," &
                                        "tot_nrodoc='" & DTTot.Rows(0).Item("tot_nrodoc") & "'," &
                                        "tot_nroope='" & nPubNroOperador & "'," &
                                        "tot_nrocuo='" & nCuo & "'," &
                                        "tot_fecha='" & Format(CDate(DTTot.Rows(0).Item("tot_fecha").ToString), "yyyy-MM-dd HH:mm:ss") & "'," &
                                        "tot_fecven='" & Format(dFechaVencimento, "yyyy-MM-dd") & "'," &
                                        "tot_fecalt='" & c_cnn.FechaHoraActual & "'," &
                                        "tot_debe='" & montoTramo3.ToString.Replace(",", ".") & "'," &
                                        "tot_haber='" & 0 & "'," &
                                        "tot_imppag='" & 0 & "'," &
                                        "tot_nroasi='" & DTTot.Rows(0).Item("tot_nroasi") & "'," &
                                        "tot_asigrupal='" & 0 & "'," &
                                        "tot_caja='" & DTTot.Rows(0).Item("tot_caja") & "'," &
                                        "tot_zeta='" & DTTot.Rows(0).Item("tot_zeta") & "'," &
                                        "tot_concepto='" & DTTot.Rows(0).Item("tot_concepto") & "'"
                                    )
                                    nCuo += 1
                                Next
                                'TRAMO 4
                                If controlTramo4 AndAlso CInt(TBTramo4Cuotas.Text) > 0 Then
                                    For nCuo3 As Integer = 1 To TBTramo4Cuotas.Text
                                        dFechaVencimento = dFechaVencimento.AddMonths(1)
                                        c_cnn.ReplaceBD(
                                            "insert into totales set tot_unegos='" & DTTot.Rows(0).Item("tot_unegos") & "'," &
                                            "tot_proceso='" & DTTot.Rows(0).Item("tot_proceso") & "'," &
                                            "tot_nrocli='" & DTTot.Rows(0).Item("tot_nrocli") & "'," &
                                            "tot_nrocom='" & DTTot.Rows(0).Item("tot_nrocom") & "'," &
                                            "tot_item='" & DTTot.Rows(0).Item("tot_item") & "'," &
                                            "tot_nropla='" & DTTot.Rows(0).Item("tot_nropla") & "'," &
                                            "tot_subpla='" & DTTot.Rows(0).Item("tot_subpla") & "'," &
                                            "tot_titulo='" & DTTot.Rows(0).Item("tot_titulo") & "'," &
                                            "tot_matricula='" & DTTot.Rows(0).Item("tot_matricula") & "'," &
                                            "tot_tipdoc='" & DTTot.Rows(0).Item("tot_tipdoc") & "'," &
                                            "tot_nrodoc='" & DTTot.Rows(0).Item("tot_nrodoc") & "'," &
                                            "tot_nroope='" & nPubNroOperador & "'," &
                                            "tot_nrocuo='" & nCuo & "'," &
                                            "tot_fecha='" & Format(CDate(DTTot.Rows(0).Item("tot_fecha").ToString), "yyyy-MM-dd HH:mm:ss") & "'," &
                                            "tot_fecven='" & Format(dFechaVencimento, "yyyy-MM-dd") & "'," &
                                            "tot_fecalt='" & c_cnn.FechaHoraActual & "'," &
                                            "tot_debe='" & montoTramo4.ToString.Replace(",", ".") & "'," &
                                            "tot_haber='" & 0 & "'," &
                                            "tot_imppag='" & 0 & "'," &
                                            "tot_nroasi='" & DTTot.Rows(0).Item("tot_nroasi") & "'," &
                                            "tot_asigrupal='" & 0 & "'," &
                                            "tot_caja='" & DTTot.Rows(0).Item("tot_caja") & "'," &
                                            "tot_zeta='" & DTTot.Rows(0).Item("tot_zeta") & "'," &
                                            "tot_concepto='" & DTTot.Rows(0).Item("tot_concepto") & "'"
                                        )
                                        nCuo += 1
                                    Next
                                End If
                            End If
                        End If
                    End If
                    transaction.Commit()
                    c_cnn.CerrarConexion()
                    MessageBox.Show("Termino satifactoriamente")
                    Me.DialogResult = DialogResult.OK
                    Me.Close()
                Catch ex As Exception
                    transaction.Rollback()
                    c_cnn.CerrarConexion()
                    MessageBox.Show(("Hubo un error. " & ex.Message))
                End Try
            End If
        End If
    End Sub
    Private Sub controlMontos()
        Dim montoControl As Double = 0
        'TRAMO 1
        If Me.controlTramo1 Then
            montoControl = (CInt(TBTramo1Cuotas.Text) * Me.montoTramo1)
            'TRAMO 2
            If Me.controlTramo2 Then
                montoControl = (montoControl + (CInt(TBTramo2Cuotas.Text) * Me.montoTramo2))
                'TRAMO 3
                If Me.controlTramo3 Then
                    montoControl = (montoControl + (CInt(TBTramo3Cuotas.Text) * Me.montoTramo3))
                    'TRAMO 4
                    If Me.controlTramo4 Then
                        montoControl = (montoControl + (CInt(TBTramo4Cuotas.Text) * Me.montoTramo4))
                    End If
                End If
            End If
            If Math.Round(montoControl, 4) <> r_total Then
                MessageBox.Show("La suma de los montos de las cuotas no es igual al total del comprobante")
                Me.controlTramo1 = False
                Me.controlTramo2 = False
                Me.controlTramo3 = False
                Me.controlTramo4 = False
            End If
        End If
    End Sub
    Private Sub controlTramos()
        'TRAMO 1
        If (String.IsNullOrWhiteSpace(Me.TBTramo1Cuotas.Text) Or Me.TBTramo1Monto.Value = 0) Then
            MessageBox.Show("Tramo 1 Cuotas y Monto son obligatorios")
        Else
            Me.montoTramo1 = CDbl(TBTramo1Monto.Value)
            Me.controlTramo1 = True
            'TRAMO 2
            If String.IsNullOrWhiteSpace(Me.TBTramo2Cuotas.Text) Then
                If Me.TBTramo2Monto.Value = 0 Then
                    Me.controlTramo2 = False
                Else
                    Me.controlTramo1 = False
                    MessageBox.Show("Si Tramo 2 tiene Monto, Cuotas es obligatorio")
                End If
            ElseIf Me.TBTramo2Monto.Value = 0 Then
                Me.controlTramo1 = False
                MessageBox.Show("Si Tramo 2 tiene Cuotas, Monto es obligatorio")
            Else
                Me.montoTramo2 = CDbl(TBTramo2Monto.Value)
                Me.controlTramo2 = True
                'TRAMO 3
                If String.IsNullOrWhiteSpace(Me.TBTramo3Cuotas.Text) Then
                    If Me.TBTramo3Monto.Value = 0 Then
                        Me.controlTramo3 = False
                    Else
                        Me.controlTramo1 = False
                        Me.controlTramo2 = False
                        MessageBox.Show("Si Tramo 3 tiene Monto, Cuotas es obligatorio")
                    End If
                ElseIf Me.TBTramo3Monto.Value = 0 Then
                    Me.controlTramo1 = False
                    Me.controlTramo2 = False
                    MessageBox.Show("Si Tramo 3 tiene Cuotas, Monto es obligatorio")
                Else
                    Me.montoTramo3 = CDbl(TBTramo3Monto.Value)
                    Me.controlTramo3 = True
                    'TRAMO 4
                    If String.IsNullOrWhiteSpace(Me.TBTramo4Cuotas.Text) Then
                        If Me.TBTramo4Monto.Value = 0 Then
                            Me.controlTramo4 = False
                        Else
                            Me.controlTramo1 = False
                            Me.controlTramo2 = False
                            Me.controlTramo3 = False
                            MessageBox.Show("Si Tramo 4 tiene Monto, Cuotas es obligatorio")
                        End If
                    ElseIf Me.TBTramo4Monto.Value = 0 Then
                        Me.controlTramo1 = False
                        Me.controlTramo2 = False
                        Me.controlTramo3 = False
                        MessageBox.Show("Si Tramo 4 tiene Cuotas, Monto es obligatorio")
                    Else
                        Me.montoTramo4 = CDbl(TBTramo4Monto.Value)
                        Me.controlTramo4 = True
                    End If
                End If
            End If
        End If
        Me.controlMontos()
    End Sub
End Class