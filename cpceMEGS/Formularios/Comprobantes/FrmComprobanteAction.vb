﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports Infragistics.Win

Public Class FrmComprobanteAction
    Private cnn As New ConsultaBD(True)
    Private DAComproba As MySqlDataAdapter
    Private DSComproba As DataSet
    Private BSTotales As BindingSource

    Private resMessage As String

    Public Sub New(ByVal new_DAComproba As MySqlDataAdapter, ByVal new_DSComproba As DataSet)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        DAComproba = new_DAComproba
        DSComproba = new_DSComproba
        BSTotales = New BindingSource
        BSTotales.DataSource = DSComproba.Tables("totales")
    End Sub

    Public ReadOnly Property GetResMessage() As String
        Get
            Return resMessage
        End Get
    End Property
    Private Sub UDTFechaVencimiento_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UDTFechaVencimiento.EditorButtonClick
        Dim miTrans As MySqlTransaction
        Dim actualizo As Boolean = False
        Dim fechaVencimiento As String = UDTFechaVencimiento.Value
        Dim mysqlFecha As MySqlDateTime
        mysqlFecha.Day = Mid(fechaVencimiento, 1, 2)
        mysqlFecha.Month = Mid(fechaVencimiento, 4, 2)
        mysqlFecha.Year = Mid(fechaVencimiento, 7, 4)
        Dim nroAsiento As Long
        Dim nroCli As Long

        'Dim diferenciaFecha As Integer = 0

        For Each dr As DataRow In DSComproba.Tables("totales").Rows
            'diferenciaFecha = Date.Compare(date1, date2)
            Dim diferenciaFecha As Long = DateDiff(DateInterval.Day, UDTFechaVencimiento.Value.Date, CDate(dr.Item("tot_fecven").ToString))
            If diferenciaFecha > 0 Then
                dr.Item("tot_fecven") = mysqlFecha
                nroAsiento = dr.Item("tot_nroasi")
                nroCli = dr.Item("tot_nrocli")
                actualizo = True
            End If
        Next

        If actualizo Then
            cnn.AbrirConexion()
            miTrans = cnn.InicioTransaccion
            Try
                Dim fechaVencimientoUpdate As String = Format(CDate(fechaVencimiento), "yyyy-MM-dd")
                'createUpdateFechaVencimiento()
                'BSTotales.EndEdit()
                'DAComproba.Update(DSComproba, "totales")
                'DSComproba.AcceptChanges()
                cnn.ReplaceBD(
                    "UPDATE totales SET tot_fecven='" & fechaVencimientoUpdate & "' 
                    WHERE tot_nroasi=" & nroAsiento & " 
                    AND tot_nrocli=" & nroCli & " 
                    AND tot_fecven > '" & fechaVencimientoUpdate & "' 
                    AND tot_debe > 0"
                )
                miTrans.Commit()
                resMessage = "Se actualizaron correctamente las fechas de vencimiento al " & fechaVencimiento
            Catch ex As Exception
                DSComproba.Reset()
                miTrans.Rollback()
                MessageBox.Show(ex.Message)
                'resMessage = "No se realizaron actualizaciones"
            End Try
        Else
            resMessage = "No se realizaron actualizaciones"
        End If

        DialogResult = DialogResult.OK
        Close()
    End Sub

    'Private Sub createUpdateFechaVencimiento()
    '    Dim cmd As MySqlCommand
    '    'Dim fecven As MySqlParameter
    '    ' Create the UpdateCommand.
    '    cmd = New MySqlCommand(
    '        "UPDATE totales SET tot_fecven=@tot_fecven WHERE tot_nroasi=@tot_nroasi AND tot_item=@tot_item AND tot_unegos=@tot_unegos AND tot_nrocli=@tot_nrocli AND tot_nrocuo=@tot_nrocuo",
    '        cnn.GetConexion()
    '    )

    '    cmd.Parameters.Add("@tot_fecven", MySqlDbType.VarChar, 15, "tot_fecven")
    '    'fecven.SourceVersion = DataRowVersion.Original
    '    Dim nroasi As MySqlParameter = cmd.Parameters.Add("@tot_nroasi", MySqlDbType.VarChar)
    '    nroasi.SourceVersion = DataRowVersion.Original

    '    Dim item As MySqlParameter = cmd.Parameters.Add("@tot_item", MySqlDbType.VarChar)
    '    item.SourceVersion = DataRowVersion.Original

    '    Dim unegos As MySqlParameter = cmd.Parameters.Add("@tot_unegos", MySqlDbType.VarChar)
    '    unegos.SourceVersion = DataRowVersion.Original

    '    Dim nrocli As MySqlParameter = cmd.Parameters.Add("@tot_nrocli", MySqlDbType.VarChar)
    '    nrocli.SourceVersion = DataRowVersion.Original

    '    Dim nrocuo As MySqlParameter = cmd.Parameters.Add("@tot_nrocuo", MySqlDbType.VarChar)
    '    nrocuo.SourceVersion = DataRowVersion.Original

    '    DAComproba.UpdateCommand = cmd
    'End Sub
End Class