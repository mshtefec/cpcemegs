﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win.UltraWinGrid

Public Class FrmDestinatario
    Private cnn As New ConsultaBD(True)
    Dim DADestinatario As MySqlDataAdapter
    Dim DSDestinatario As DataSet
    Dim BSDestinatario As BindingSource
    Dim c_condicion As String
    Dim c_Destinatario As String
    Dim c_Matricula As String
    Dim c_TipoDoc As String
    Dim c_Categoria As String
    Dim c_NombreCategoria As String
    Dim n_Nrodoc As Integer
    Dim c_fechaven_et As String

    Public WriteOnly Property Condicion() As String
        Set(ByVal value As String)
            If controlAcceso(4, 2, , False) = False Then
                c_condicion = "MID(afi_categoria,1,2) NOT IN ('22')"
            Else
                c_condicion = ""
            End If
            If Not String.IsNullOrEmpty(value) Then
                If Not String.IsNullOrEmpty(c_condicion) Then
                    c_condicion = c_condicion & " AND " & value
                Else
                    c_condicion = value
                End If
            End If
        End Set
    End Property

    Public ReadOnly Property NombreDestinatario() As String
        Get
            Return c_Destinatario
        End Get
    End Property

    Public ReadOnly Property MatriculaDestinatario() As String
        Get
            Return c_Matricula
        End Get
    End Property

    Public ReadOnly Property TipoDocDestinatario() As String
        Get
            Return c_TipoDoc
        End Get
    End Property

    Public ReadOnly Property NroDocDestinatario() As String
        Get
            Return n_Nrodoc
        End Get
    End Property

    Public ReadOnly Property Categoria() As String
        Get
            Return c_Categoria
        End Get
    End Property

    Public ReadOnly Property NombreCategoria() As String
        Get
            Return c_NombreCategoria
        End Get
    End Property

    Public ReadOnly Property FechaVencimientoET() As String
        Get
            Return c_fechaven_et
        End Get
    End Property

    Public Sub CargaDestinatario()
        Try
            DADestinatario = cnn.consultaBDadapter(
                "afiliado left join categorias on cat_codigo=afi_categoria",
                "concat(afi_titulo,CAST(afi_matricula as char)) as Matricula,afi_nombre as Destinatario,afi_tipdoc as TipoDoc,afi_nrodoc as Nrodoc,afi_categoria as Categoria,cat_descrip as NombreCat, afi_fecha_vencimiento_et as VencimientoET",
                c_condicion
            )
            DSDestinatario = New DataSet
            DADestinatario.Fill(DSDestinatario, "afiliado")
        Catch ex As Exception
            mostrarErrorConexion("obtener datos.")
            Close()
            Exit Sub
        End Try
        BSDestinatario = New BindingSource
        BSDestinatario.DataSource = DSDestinatario.Tables(0)
        If BSDestinatario.Count = 1 Then
            c_Destinatario = DSDestinatario.Tables(0).Rows(0).Item("Destinatario")
            c_Matricula = DSDestinatario.Tables(0).Rows(0).Item("Matricula")
            c_TipoDoc = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("tipoDoc")
            n_Nrodoc = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("NroDoc")
            c_Categoria = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("Categoria")
            If Not IsDBNull(DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("NombreCat")) Then
                c_NombreCategoria = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("NombreCat")
            End If
            c_fechaven_et = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("VencimientoET").ToString
        Else
            UGDestinatarios.DataSource = BSDestinatario
            With UGDestinatarios.DisplayLayout.Bands(0)
                .Columns(0).Width = 90
                .Columns(0).CellActivation = Activation.NoEdit
                .Columns(1).Width = 400
                .Columns(1).CellActivation = Activation.NoEdit
                .Columns(2).Width = 40
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(3).Width = 90
                .Columns(3).CellActivation = Activation.NoEdit
            End With
        End If
    End Sub

    Private Sub CButton2_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton2.Click
        If BSDestinatario.Count > 0 Then
            c_Destinatario = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("Destinatario")
            c_Matricula = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("Matricula")
            c_TipoDoc = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("tipoDoc")
            n_Nrodoc = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("NroDoc")
            c_fechaven_et = DSDestinatario.Tables(0).Rows(BSDestinatario.Position).Item("VencimientoET").ToString()
            DialogResult = DialogResult.OK
        Else
            DialogResult = DialogResult.Cancel
        End If
    End Sub
End Class