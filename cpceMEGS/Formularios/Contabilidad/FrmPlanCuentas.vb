﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win.UltraWinGrid

Public Class FrmPlanCuentas
    Private cnn As New ConsultaBD(True)
    Dim DAPlanCuenta As MySqlDataAdapter
    Dim DSPlanCuenta As DataSet
    Dim BSPlanCuenta As BindingSource
    Dim c_condicion As String
    Dim c_Descripcion As String = ""
    Dim c_Cuenta As String = ""
    Dim c_Servicio As String = ""
    Dim c_Subcuenta As String = ""


    Public WriteOnly Property Condicion() As String
        Set(ByVal value As String)
            Me.c_condicion = value
        End Set
    End Property

    Public ReadOnly Property DescripcionCuenta() As String
        Get
            Return Me.c_Descripcion
        End Get
    End Property

    Public ReadOnly Property NumeroCuenta() As String
        Get
            Return Me.c_Cuenta
        End Get
    End Property

    Public ReadOnly Property NumeroSubCuenta() As String
        Get
            Return Me.c_Subcuenta
        End Get
    End Property

    Public ReadOnly Property Servicio() As String
        Get
            Return Me.c_Servicio
        End Get
    End Property

    Public Sub CargaPlanCuenta()
        DAPlanCuenta = cnn.consultaBDadapter("plancuen", "pla_nropla as cuenta,pla_nombre as descripcion,pla_subcta,pla_servicio", c_condicion)
        DSPlanCuenta = New DataSet
        DAPlanCuenta.Fill(DSPlanCuenta, "cuentas")
        BSPlanCuenta = New BindingSource
        BSPlanCuenta.DataSource = Me.DSPlanCuenta.Tables(0)
        If BSPlanCuenta.Count = 1 Then
            c_Descripcion = Me.DSPlanCuenta.Tables(0).Rows(0).Item("descripcion")
            c_Cuenta = Me.DSPlanCuenta.Tables(0).Rows(0).Item("cuenta")
            c_Subcuenta = Me.DSPlanCuenta.Tables(0).Rows(0).Item("pla_subcta")
            c_Servicio = Me.DSPlanCuenta.Tables(0).Rows(0).Item("pla_servicio")
        Else
            UGPlanCuenta.DataSource = BSPlanCuenta
            With UGPlanCuenta.DisplayLayout.Bands(0)
                .Columns(0).Width = 90
                .Columns(0).CellActivation = Activation.NoEdit
                .Columns(1).Width = 400
                .Columns(1).CellActivation = Activation.NoEdit
                .Columns(2).Hidden = True
                .Columns(3).Hidden = True
            End With
        End If
    End Sub

    Private Sub CButton2_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton2.Click
        If BSPlanCuenta.Count > 0 Then
            c_Descripcion = Me.DSPlanCuenta.Tables(0).Rows(BSPlanCuenta.Position).Item("descripcion")
            c_Cuenta = Me.DSPlanCuenta.Tables(0).Rows(BSPlanCuenta.Position).Item("cuenta")
            c_Subcuenta = Me.DSPlanCuenta.Tables(0).Rows(BSPlanCuenta.Position).Item("pla_subcta")
            c_Servicio = Me.DSPlanCuenta.Tables(0).Rows(BSPlanCuenta.Position).Item("pla_servicio")
            DialogResult = DialogResult.OK
        Else
            DialogResult = DialogResult.Cancel
        End If
    End Sub

    Private Sub CButton2_Click(sender As Object, e As EventArgs) Handles CButton2.Click

    End Sub
End Class