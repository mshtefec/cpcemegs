﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAsientoMatricula
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAsientoMatricula))
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ValueListItem3 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem4 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem8 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem9 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem10 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem11 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem12 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImpresionPrint = New System.Drawing.Printing.PrintDocument()
        Me.ImpresionDialog = New System.Windows.Forms.PrintDialog()
        Me.GBMatriculado = New System.Windows.Forms.GroupBox()
        Me.UltraLabel5 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraLabel3 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraDateTimeEditor1 = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraComboEditor1 = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.UCEdelegacion = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.UltraLabel6 = New Infragistics.Win.Misc.UltraLabel()
        Me.ButtonCobrar = New System.Windows.Forms.Button()
        Me.DGVCuentaCte = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.TBMatricula = New System.Windows.Forms.TextBox()
        Me.TBMatriculado = New System.Windows.Forms.TextBox()
        Me.UTEMatricula = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.TTFrmAsiento = New System.Windows.Forms.ToolTip(Me.components)
        Me.BtnVer = New System.Windows.Forms.Button()
        Me.GBMatriculado.SuspendLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVCuentaCte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEMatricula, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "add.ico")
        Me.ImageList1.Images.SetKeyName(1, "Search (2).ico")
        Me.ImageList1.Images.SetKeyName(2, "delete.ico")
        Me.ImageList1.Images.SetKeyName(3, "application_view_columns.png")
        Me.ImageList1.Images.SetKeyName(4, "no.png")
        Me.ImageList1.Images.SetKeyName(5, "si.png")
        Me.ImageList1.Images.SetKeyName(6, "pay.png")
        '
        'ImpresionDialog
        '
        Me.ImpresionDialog.UseEXDialog = True
        '
        'GBMatriculado
        '
        Me.GBMatriculado.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBMatriculado.Controls.Add(Me.BtnVer)
        Me.GBMatriculado.Controls.Add(Me.UltraLabel5)
        Me.GBMatriculado.Controls.Add(Me.UltraLabel3)
        Me.GBMatriculado.Controls.Add(Me.UltraDateTimeEditor1)
        Me.GBMatriculado.Controls.Add(Me.UltraComboEditor1)
        Me.GBMatriculado.Controls.Add(Me.UCEdelegacion)
        Me.GBMatriculado.Controls.Add(Me.UltraLabel6)
        Me.GBMatriculado.Controls.Add(Me.ButtonCobrar)
        Me.GBMatriculado.Controls.Add(Me.DGVCuentaCte)
        Me.GBMatriculado.Controls.Add(Me.TBMatricula)
        Me.GBMatriculado.Controls.Add(Me.TBMatriculado)
        Me.GBMatriculado.Controls.Add(Me.UTEMatricula)
        Me.GBMatriculado.Location = New System.Drawing.Point(12, 12)
        Me.GBMatriculado.Name = "GBMatriculado"
        Me.GBMatriculado.Size = New System.Drawing.Size(1166, 669)
        Me.GBMatriculado.TabIndex = 29
        Me.GBMatriculado.TabStop = False
        Me.GBMatriculado.Text = "Matriculado"
        '
        'UltraLabel5
        '
        Appearance1.TextHAlignAsString = "Right"
        Appearance1.TextVAlignAsString = "Middle"
        Me.UltraLabel5.Appearance = Appearance1
        Me.UltraLabel5.AutoSize = True
        Me.UltraLabel5.Location = New System.Drawing.Point(498, 39)
        Me.UltraLabel5.Name = "UltraLabel5"
        Me.UltraLabel5.Size = New System.Drawing.Size(69, 17)
        Me.UltraLabel5.TabIndex = 138
        Me.UltraLabel5.Text = "Institución:"
        '
        'UltraLabel3
        '
        Appearance2.TextHAlignAsString = "Right"
        Appearance2.TextVAlignAsString = "Middle"
        Me.UltraLabel3.Appearance = Appearance2
        Me.UltraLabel3.AutoSize = True
        Me.UltraLabel3.Location = New System.Drawing.Point(514, 13)
        Me.UltraLabel3.Name = "UltraLabel3"
        Me.UltraLabel3.Size = New System.Drawing.Size(46, 17)
        Me.UltraLabel3.TabIndex = 137
        Me.UltraLabel3.Text = "Fecha:"
        '
        'UltraDateTimeEditor1
        '
        Me.UltraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UltraDateTimeEditor1.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UltraDateTimeEditor1.FormatString = ""
        Me.UltraDateTimeEditor1.Location = New System.Drawing.Point(573, 9)
        Me.UltraDateTimeEditor1.MaskInput = "{date}"
        Me.UltraDateTimeEditor1.Name = "UltraDateTimeEditor1"
        Me.UltraDateTimeEditor1.Size = New System.Drawing.Size(114, 24)
        Me.UltraDateTimeEditor1.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UltraDateTimeEditor1.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UltraDateTimeEditor1.TabIndex = 135
        '
        'UltraComboEditor1
        '
        Me.UltraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        ValueListItem3.DataValue = CType(1, Short)
        ValueListItem3.DisplayText = "SIPRES"
        ValueListItem4.DataValue = CType(2, Short)
        ValueListItem4.DisplayText = "CPCE"
        Me.UltraComboEditor1.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem3, ValueListItem4})
        Me.UltraComboEditor1.Location = New System.Drawing.Point(573, 35)
        Me.UltraComboEditor1.Name = "UltraComboEditor1"
        Me.UltraComboEditor1.Size = New System.Drawing.Size(140, 24)
        Me.UltraComboEditor1.TabIndex = 136
        '
        'UCEdelegacion
        '
        Me.UCEdelegacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        ValueListItem8.DataValue = "Resistencia"
        ValueListItem9.DataValue = "Sáenz Peña"
        ValueListItem10.DataValue = "Villa Ángela"
        ValueListItem11.DataValue = "Sudoeste"
        ValueListItem12.DataValue = "Club SDC"
        Me.UCEdelegacion.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem8, ValueListItem9, ValueListItem10, ValueListItem11, ValueListItem12})
        Me.UCEdelegacion.Location = New System.Drawing.Point(573, 61)
        Me.UCEdelegacion.Name = "UCEdelegacion"
        Me.UCEdelegacion.Size = New System.Drawing.Size(230, 24)
        Me.UCEdelegacion.TabIndex = 140
        '
        'UltraLabel6
        '
        Appearance3.TextHAlignAsString = "Right"
        Appearance3.TextVAlignAsString = "Middle"
        Me.UltraLabel6.Appearance = Appearance3
        Me.UltraLabel6.AutoSize = True
        Me.UltraLabel6.Location = New System.Drawing.Point(491, 64)
        Me.UltraLabel6.Name = "UltraLabel6"
        Me.UltraLabel6.Size = New System.Drawing.Size(76, 17)
        Me.UltraLabel6.TabIndex = 139
        Me.UltraLabel6.Text = "Delegación:"
        '
        'ButtonCobrar
        '
        Me.ButtonCobrar.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.ButtonCobrar.Location = New System.Drawing.Point(1026, 23)
        Me.ButtonCobrar.Name = "ButtonCobrar"
        Me.ButtonCobrar.Size = New System.Drawing.Size(134, 43)
        Me.ButtonCobrar.TabIndex = 134
        Me.ButtonCobrar.Text = "Cobrar"
        Me.ButtonCobrar.UseVisualStyleBackColor = False
        '
        'DGVCuentaCte
        '
        Appearance4.BackColor = System.Drawing.Color.White
        Me.DGVCuentaCte.DisplayLayout.Appearance = Appearance4
        Me.DGVCuentaCte.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.DGVCuentaCte.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.DGVCuentaCte.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance5.BackColor = System.Drawing.Color.Transparent
        Me.DGVCuentaCte.DisplayLayout.Override.CardAreaAppearance = Appearance5
        Me.DGVCuentaCte.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance6.FontData.BoldAsString = "True"
        Appearance6.FontData.Name = "Arial"
        Appearance6.FontData.SizeInPoints = 10.0!
        Appearance6.ForeColor = System.Drawing.Color.White
        Appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.DGVCuentaCte.DisplayLayout.Override.HeaderAppearance = Appearance6
        Me.DGVCuentaCte.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance7.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance7.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.DGVCuentaCte.DisplayLayout.Override.RowSelectorAppearance = Appearance7
        Appearance8.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance8.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.DGVCuentaCte.DisplayLayout.Override.SelectedRowAppearance = Appearance8
        Me.DGVCuentaCte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.DGVCuentaCte.Location = New System.Drawing.Point(6, 85)
        Me.DGVCuentaCte.Name = "DGVCuentaCte"
        Me.DGVCuentaCte.Size = New System.Drawing.Size(1154, 578)
        Me.DGVCuentaCte.TabIndex = 133
        '
        'TBMatricula
        '
        Me.TBMatricula.BackColor = System.Drawing.Color.White
        Me.TBMatricula.Enabled = False
        Me.TBMatricula.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBMatricula.ForeColor = System.Drawing.Color.Black
        Me.TBMatricula.Location = New System.Drawing.Point(176, 23)
        Me.TBMatricula.Name = "TBMatricula"
        Me.TBMatricula.ReadOnly = True
        Me.TBMatricula.Size = New System.Drawing.Size(105, 25)
        Me.TBMatricula.TabIndex = 132
        '
        'TBMatriculado
        '
        Me.TBMatriculado.BackColor = System.Drawing.Color.White
        Me.TBMatriculado.Enabled = False
        Me.TBMatriculado.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBMatriculado.ForeColor = System.Drawing.Color.Black
        Me.TBMatriculado.Location = New System.Drawing.Point(6, 54)
        Me.TBMatriculado.Name = "TBMatriculado"
        Me.TBMatriculado.ReadOnly = True
        Me.TBMatriculado.Size = New System.Drawing.Size(466, 25)
        Me.TBMatriculado.TabIndex = 131
        '
        'UTEMatricula
        '
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        EditorButton1.Appearance = Appearance9
        EditorButton1.Width = 70
        Me.UTEMatricula.ButtonsRight.Add(EditorButton1)
        Me.UTEMatricula.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UTEMatricula.Location = New System.Drawing.Point(6, 21)
        Me.UTEMatricula.MaxLength = 8
        Me.UTEMatricula.Name = "UTEMatricula"
        Me.UTEMatricula.Size = New System.Drawing.Size(164, 27)
        Me.UTEMatricula.TabIndex = 127
        '
        'Button1
        '
        Me.BtnVer.Location = New System.Drawing.Point(762, 9)
        Me.BtnVer.Name = "Button1"
        Me.BtnVer.Size = New System.Drawing.Size(75, 23)
        Me.BtnVer.TabIndex = 141
        Me.BtnVer.Text = "ver"
        Me.BtnVer.UseVisualStyleBackColor = True
        '
        'FrmAsientoMatricula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1190, 693)
        Me.Controls.Add(Me.GBMatriculado)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmAsientoMatricula"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiento"
        Me.GBMatriculado.ResumeLayout(False)
        Me.GBMatriculado.PerformLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVCuentaCte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEMatricula, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImpresionPrint As System.Drawing.Printing.PrintDocument
    Friend WithEvents ImpresionDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents GBMatriculado As System.Windows.Forms.GroupBox
    Friend WithEvents UTEMatricula As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents TBMatricula As System.Windows.Forms.TextBox
    Friend WithEvents TBMatriculado As System.Windows.Forms.TextBox
    Friend WithEvents TTFrmAsiento As System.Windows.Forms.ToolTip
    Friend WithEvents DGVCuentaCte As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents ButtonCobrar As Button
    Friend WithEvents UltraLabel5 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraLabel3 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraDateTimeEditor1 As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraComboEditor1 As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents UCEdelegacion As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents UltraLabel6 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents BtnVer As Button
End Class
