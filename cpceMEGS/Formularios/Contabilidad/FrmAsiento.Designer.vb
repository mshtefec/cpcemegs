﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAsiento))
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ValueListItem3 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem4 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton4 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
        Dim Appearance35 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton5 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
        Dim Appearance36 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance39 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGDetalles = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraTextEditor1 = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UGProcesos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraLabel1 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraLabel2 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraLabel3 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TxtHaber = New System.Windows.Forms.TextBox()
        Me.TxtDebe = New System.Windows.Forms.TextBox()
        Me.UltraDateTimeEditor1 = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraLabel4 = New Infragistics.Win.Misc.UltraLabel()
        Me.TxtConcepto1 = New System.Windows.Forms.TextBox()
        Me.UltraLabel5 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraComboEditor1 = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.TxtConcepto2 = New System.Windows.Forms.TextBox()
        Me.TxtConcepto3 = New System.Windows.Forms.TextBox()
        Me.ImpresionPrint = New System.Drawing.Printing.PrintDocument()
        Me.ImpresionDialog = New System.Windows.Forms.PrintDialog()
        Me.UltraLabel6 = New Infragistics.Win.Misc.UltraLabel()
        Me.UCEdelegacion = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.TxtDestinatarioExterno = New System.Windows.Forms.TextBox()
        Me.LabelDestinatarioExterno = New System.Windows.Forms.Label()
        Me.LabelControlConfirmar = New System.Windows.Forms.Label()
        Me.GBMatriculado = New System.Windows.Forms.GroupBox()
        Me.DGVCuentaCte = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.TBMatricula = New System.Windows.Forms.TextBox()
        Me.TBMatriculado = New System.Windows.Forms.TextBox()
        Me.UTEMatricula = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.LabelControlConfirmarImprimir = New System.Windows.Forms.Label()
        Me.CBConfirmarImprimir = New System.Windows.Forms.Button()
        Me.TTFrmAsiento = New System.Windows.Forms.ToolTip(Me.components)
        Me.CBConfirmar = New System.Windows.Forms.Button()
        Me.UTECodigoBarra = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.GBTrabajo = New System.Windows.Forms.GroupBox()
        Me.LabelMontoTrabajoTexto = New System.Windows.Forms.Label()
        Me.LabelMontoTrabajo = New System.Windows.Forms.Label()
        Me.LabelMontoSigno = New System.Windows.Forms.Label()
        Me.ULMontoCobrar = New Infragistics.Win.Misc.UltraLabel()
        Me.BMCEfectivo = New System.Windows.Forms.Button()
        Me.ULMCTexto = New Infragistics.Win.Misc.UltraLabel()
        Me.BMCCheque = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        CType(Me.UGDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTextEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGProcesos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBMatriculado.SuspendLayout()
        CType(Me.DGVCuentaCte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEMatricula, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBTrabajo.SuspendLayout()
        Me.SuspendLayout()
        '
        'UGDetalles
        '
        Me.UGDetalles.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGDetalles.DisplayLayout.Appearance = Appearance1
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGDetalles.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGDetalles.DisplayLayout.Override.HeaderAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDetalles.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDetalles.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGDetalles.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGDetalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGDetalles.Location = New System.Drawing.Point(0, 124)
        Me.UGDetalles.Name = "UGDetalles"
        Me.UGDetalles.Size = New System.Drawing.Size(990, 511)
        Me.UGDetalles.TabIndex = 7
        Me.UGDetalles.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChange
        '
        'UltraTextEditor1
        '
        Me.UltraTextEditor1.AutoSize = False
        Appearance6.Image = CType(resources.GetObject("Appearance6.Image"), Object)
        EditorButton1.Appearance = Appearance6
        EditorButton1.Key = "Left"
        EditorButton1.Width = 30
        Me.UltraTextEditor1.ButtonsLeft.Add(EditorButton1)
        Appearance7.Image = CType(resources.GetObject("Appearance7.Image"), Object)
        Appearance7.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance7.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton2.Appearance = Appearance7
        EditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton2.Key = "Right"
        Appearance8.Image = CType(resources.GetObject("Appearance8.Image"), Object)
        Appearance8.ImageBackgroundDisabled = CType(resources.GetObject("Appearance8.ImageBackgroundDisabled"), System.Drawing.Image)
        EditorButton2.PressedAppearance = Appearance8
        EditorButton2.Width = 80
        Me.UltraTextEditor1.ButtonsRight.Add(EditorButton2)
        Me.UltraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UltraTextEditor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraTextEditor1.Location = New System.Drawing.Point(75, 0)
        Me.UltraTextEditor1.Name = "UltraTextEditor1"
        Me.UltraTextEditor1.Size = New System.Drawing.Size(236, 29)
        Me.UltraTextEditor1.TabIndex = 0
        Me.TTFrmAsiento.SetToolTip(Me.UltraTextEditor1, "Ingrese el nro de proceso y presione el boton para buscarlo.")
        '
        'UGProcesos
        '
        Me.UGProcesos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance9.BackColor = System.Drawing.Color.White
        Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
        Me.UGProcesos.DisplayLayout.Appearance = Appearance9
        Me.UGProcesos.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGProcesos.DisplayLayout.InterBandSpacing = 10
        Appearance10.BackColor = System.Drawing.Color.Transparent
        Me.UGProcesos.DisplayLayout.Override.CardAreaAppearance = Appearance10
        Me.UGProcesos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance11.ForeColor = System.Drawing.Color.White
        Appearance11.TextHAlignAsString = "Left"
        Appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGProcesos.DisplayLayout.Override.HeaderAppearance = Appearance11
        Appearance12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGProcesos.DisplayLayout.Override.RowAppearance = Appearance12
        Appearance13.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance13.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGProcesos.DisplayLayout.Override.RowSelectorAppearance = Appearance13
        Me.UGProcesos.DisplayLayout.Override.RowSelectorWidth = 12
        Me.UGProcesos.DisplayLayout.Override.RowSpacingBefore = 2
        Appearance14.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
        Appearance14.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
        Appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance14.ForeColor = System.Drawing.Color.Black
        Me.UGProcesos.DisplayLayout.Override.SelectedRowAppearance = Appearance14
        Me.UGProcesos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGProcesos.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGProcesos.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
        Me.UGProcesos.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
        Me.UGProcesos.Location = New System.Drawing.Point(1093, 1)
        Me.UGProcesos.Name = "UGProcesos"
        Me.UGProcesos.Size = New System.Drawing.Size(186, 121)
        Me.UGProcesos.TabIndex = 3
        Me.UGProcesos.Visible = False
        '
        'UltraLabel1
        '
        Appearance15.BackColor = System.Drawing.Color.White
        Appearance15.TextHAlignAsString = "Left"
        Appearance15.TextVAlignAsString = "Middle"
        Me.UltraLabel1.Appearance = Appearance15
        Me.UltraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.InsetSoft
        Me.UltraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraLabel1.ImageTransparentColor = System.Drawing.Color.White
        Me.UltraLabel1.Location = New System.Drawing.Point(317, 1)
        Me.UltraLabel1.Name = "UltraLabel1"
        Me.UltraLabel1.Size = New System.Drawing.Size(447, 26)
        Me.UltraLabel1.TabIndex = 4
        Me.UltraLabel1.Visible = False
        '
        'UltraLabel2
        '
        Appearance16.TextHAlignAsString = "Right"
        Appearance16.TextVAlignAsString = "Middle"
        Me.UltraLabel2.Appearance = Appearance16
        Me.UltraLabel2.AutoSize = True
        Me.UltraLabel2.Location = New System.Drawing.Point(11, 7)
        Me.UltraLabel2.Name = "UltraLabel2"
        Me.UltraLabel2.Size = New System.Drawing.Size(58, 17)
        Me.UltraLabel2.TabIndex = 5
        Me.UltraLabel2.Text = "Proceso:"
        '
        'UltraLabel3
        '
        Appearance17.TextHAlignAsString = "Right"
        Appearance17.TextVAlignAsString = "Middle"
        Me.UltraLabel3.Appearance = Appearance17
        Me.UltraLabel3.AutoSize = True
        Me.UltraLabel3.Location = New System.Drawing.Point(805, 6)
        Me.UltraLabel3.Name = "UltraLabel3"
        Me.UltraLabel3.Size = New System.Drawing.Size(46, 17)
        Me.UltraLabel3.TabIndex = 8
        Me.UltraLabel3.Text = "Fecha:"
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Appearance18.BackColor = System.Drawing.Color.SteelBlue
        Appearance18.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal
        Me.UltraGroupBox1.ContentAreaAppearance = Appearance18
        Me.UltraGroupBox1.Controls.Add(Me.TxtHaber)
        Me.UltraGroupBox1.Controls.Add(Me.TxtDebe)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(657, 637)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(289, 55)
        Me.UltraGroupBox1.TabIndex = 18
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.VisualStudio2005
        '
        'TxtHaber
        '
        Me.TxtHaber.BackColor = System.Drawing.Color.Black
        Me.TxtHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtHaber.ForeColor = System.Drawing.SystemColors.Window
        Me.TxtHaber.Location = New System.Drawing.Point(144, 13)
        Me.TxtHaber.Name = "TxtHaber"
        Me.TxtHaber.ReadOnly = True
        Me.TxtHaber.Size = New System.Drawing.Size(126, 26)
        Me.TxtHaber.TabIndex = 19
        Me.TxtHaber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtDebe
        '
        Me.TxtDebe.BackColor = System.Drawing.Color.Black
        Me.TxtDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDebe.ForeColor = System.Drawing.SystemColors.Window
        Me.TxtDebe.Location = New System.Drawing.Point(16, 13)
        Me.TxtDebe.Name = "TxtDebe"
        Me.TxtDebe.ReadOnly = True
        Me.TxtDebe.Size = New System.Drawing.Size(126, 26)
        Me.TxtDebe.TabIndex = 18
        Me.TxtDebe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'UltraDateTimeEditor1
        '
        Me.UltraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UltraDateTimeEditor1.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UltraDateTimeEditor1.FormatString = ""
        Me.UltraDateTimeEditor1.Location = New System.Drawing.Point(857, 3)
        Me.UltraDateTimeEditor1.MaskInput = "{date}"
        Me.UltraDateTimeEditor1.Name = "UltraDateTimeEditor1"
        Me.UltraDateTimeEditor1.Size = New System.Drawing.Size(114, 24)
        Me.UltraDateTimeEditor1.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UltraDateTimeEditor1.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UltraDateTimeEditor1.TabIndex = 1
        '
        'UltraLabel4
        '
        Appearance19.TextHAlignAsString = "Right"
        Appearance19.TextVAlignAsString = "Middle"
        Me.UltraLabel4.Appearance = Appearance19
        Me.UltraLabel4.AutoSize = True
        Me.UltraLabel4.Location = New System.Drawing.Point(4, 33)
        Me.UltraLabel4.Name = "UltraLabel4"
        Me.UltraLabel4.Size = New System.Drawing.Size(66, 17)
        Me.UltraLabel4.TabIndex = 21
        Me.UltraLabel4.Text = "Concepto:"
        '
        'TxtConcepto1
        '
        Me.TxtConcepto1.Location = New System.Drawing.Point(76, 31)
        Me.TxtConcepto1.MaxLength = 100
        Me.TxtConcepto1.Name = "TxtConcepto1"
        Me.TxtConcepto1.Size = New System.Drawing.Size(688, 22)
        Me.TxtConcepto1.TabIndex = 3
        '
        'UltraLabel5
        '
        Appearance20.TextHAlignAsString = "Right"
        Appearance20.TextVAlignAsString = "Middle"
        Me.UltraLabel5.Appearance = Appearance20
        Me.UltraLabel5.AutoSize = True
        Me.UltraLabel5.Location = New System.Drawing.Point(782, 32)
        Me.UltraLabel5.Name = "UltraLabel5"
        Me.UltraLabel5.Size = New System.Drawing.Size(69, 17)
        Me.UltraLabel5.TabIndex = 23
        Me.UltraLabel5.Text = "Institución:"
        '
        'UltraComboEditor1
        '
        Me.UltraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        ValueListItem3.DataValue = CType(1, Short)
        ValueListItem3.DisplayText = "SIPRES"
        ValueListItem4.DataValue = CType(2, Short)
        ValueListItem4.DisplayText = "CPCE"
        Me.UltraComboEditor1.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem3, ValueListItem4})
        Me.UltraComboEditor1.Location = New System.Drawing.Point(857, 29)
        Me.UltraComboEditor1.Name = "UltraComboEditor1"
        Me.UltraComboEditor1.Size = New System.Drawing.Size(140, 24)
        Me.UltraComboEditor1.TabIndex = 2
        '
        'TxtConcepto2
        '
        Me.TxtConcepto2.Location = New System.Drawing.Point(76, 54)
        Me.TxtConcepto2.MaxLength = 100
        Me.TxtConcepto2.Name = "TxtConcepto2"
        Me.TxtConcepto2.Size = New System.Drawing.Size(688, 22)
        Me.TxtConcepto2.TabIndex = 4
        '
        'TxtConcepto3
        '
        Me.TxtConcepto3.Location = New System.Drawing.Point(77, 77)
        Me.TxtConcepto3.MaxLength = 100
        Me.TxtConcepto3.Name = "TxtConcepto3"
        Me.TxtConcepto3.Size = New System.Drawing.Size(687, 22)
        Me.TxtConcepto3.TabIndex = 5
        '
        'ImpresionDialog
        '
        Me.ImpresionDialog.UseEXDialog = True
        '
        'UltraLabel6
        '
        Appearance21.TextHAlignAsString = "Right"
        Appearance21.TextVAlignAsString = "Middle"
        Me.UltraLabel6.Appearance = Appearance21
        Me.UltraLabel6.AutoSize = True
        Me.UltraLabel6.Location = New System.Drawing.Point(775, 58)
        Me.UltraLabel6.Name = "UltraLabel6"
        Me.UltraLabel6.Size = New System.Drawing.Size(76, 17)
        Me.UltraLabel6.TabIndex = 24
        Me.UltraLabel6.Text = "Delegación:"
        '
        'UCEdelegacion
        '
        Me.UCEdelegacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UCEdelegacion.Location = New System.Drawing.Point(857, 55)
        Me.UCEdelegacion.Name = "UCEdelegacion"
        Me.UCEdelegacion.Size = New System.Drawing.Size(230, 24)
        Me.UCEdelegacion.TabIndex = 25
        '
        'TxtDestinatarioExterno
        '
        Me.TxtDestinatarioExterno.Location = New System.Drawing.Point(146, 100)
        Me.TxtDestinatarioExterno.MaxLength = 200
        Me.TxtDestinatarioExterno.Name = "TxtDestinatarioExterno"
        Me.TxtDestinatarioExterno.Size = New System.Drawing.Size(618, 22)
        Me.TxtDestinatarioExterno.TabIndex = 6
        Me.TTFrmAsiento.SetToolTip(Me.TxtDestinatarioExterno, "Campo Destinatario Externo se utiliza para agregar.")
        '
        'LabelDestinatarioExterno
        '
        Me.LabelDestinatarioExterno.AutoSize = True
        Me.LabelDestinatarioExterno.Location = New System.Drawing.Point(9, 103)
        Me.LabelDestinatarioExterno.Name = "LabelDestinatarioExterno"
        Me.LabelDestinatarioExterno.Size = New System.Drawing.Size(131, 16)
        Me.LabelDestinatarioExterno.TabIndex = 27
        Me.LabelDestinatarioExterno.Text = "Destinatario Externo:"
        '
        'LabelControlConfirmar
        '
        Me.LabelControlConfirmar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LabelControlConfirmar.AutoSize = True
        Me.LabelControlConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControlConfirmar.Location = New System.Drawing.Point(532, 680)
        Me.LabelControlConfirmar.Name = "LabelControlConfirmar"
        Me.LabelControlConfirmar.Size = New System.Drawing.Size(47, 13)
        Me.LabelControlConfirmar.TabIndex = 28
        Me.LabelControlConfirmar.Text = "Shift + C"
        '
        'GBMatriculado
        '
        Me.GBMatriculado.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GBMatriculado.Controls.Add(Me.DGVCuentaCte)
        Me.GBMatriculado.Controls.Add(Me.TBMatricula)
        Me.GBMatriculado.Controls.Add(Me.TBMatriculado)
        Me.GBMatriculado.Controls.Add(Me.UTEMatricula)
        Me.GBMatriculado.Location = New System.Drawing.Point(902, 124)
        Me.GBMatriculado.Name = "GBMatriculado"
        Me.GBMatriculado.Size = New System.Drawing.Size(377, 511)
        Me.GBMatriculado.TabIndex = 29
        Me.GBMatriculado.TabStop = False
        Me.GBMatriculado.Text = "Matriculado"
        '
        'DGVCuentaCte
        '
        Appearance22.BackColor = System.Drawing.SystemColors.Window
        Appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption
        Me.DGVCuentaCte.DisplayLayout.Appearance = Appearance22
        Me.DGVCuentaCte.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.DGVCuentaCte.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder
        Appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance23.BorderColor = System.Drawing.SystemColors.Window
        Me.DGVCuentaCte.DisplayLayout.GroupByBox.Appearance = Appearance23
        Appearance24.ForeColor = System.Drawing.SystemColors.GrayText
        Me.DGVCuentaCte.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance24
        Me.DGVCuentaCte.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance25.BackColor2 = System.Drawing.SystemColors.Control
        Appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance25.ForeColor = System.Drawing.SystemColors.GrayText
        Me.DGVCuentaCte.DisplayLayout.GroupByBox.PromptAppearance = Appearance25
        Me.DGVCuentaCte.DisplayLayout.MaxColScrollRegions = 1
        Me.DGVCuentaCte.DisplayLayout.MaxRowScrollRegions = 1
        Appearance26.BackColor = System.Drawing.SystemColors.Window
        Appearance26.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DGVCuentaCte.DisplayLayout.Override.ActiveCellAppearance = Appearance26
        Appearance27.BackColor = System.Drawing.SystemColors.Highlight
        Appearance27.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.DGVCuentaCte.DisplayLayout.Override.ActiveRowAppearance = Appearance27
        Me.DGVCuentaCte.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted
        Me.DGVCuentaCte.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted
        Appearance28.BackColor = System.Drawing.SystemColors.Window
        Me.DGVCuentaCte.DisplayLayout.Override.CardAreaAppearance = Appearance28
        Appearance29.BorderColor = System.Drawing.Color.Silver
        Appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter
        Me.DGVCuentaCte.DisplayLayout.Override.CellAppearance = Appearance29
        Me.DGVCuentaCte.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.DGVCuentaCte.DisplayLayout.Override.CellPadding = 0
        Appearance30.BackColor = System.Drawing.SystemColors.Control
        Appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance30.BorderColor = System.Drawing.SystemColors.Window
        Me.DGVCuentaCte.DisplayLayout.Override.GroupByRowAppearance = Appearance30
        Appearance31.TextHAlignAsString = "Left"
        Me.DGVCuentaCte.DisplayLayout.Override.HeaderAppearance = Appearance31
        Me.DGVCuentaCte.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.DGVCuentaCte.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand
        Appearance32.BackColor = System.Drawing.SystemColors.Window
        Appearance32.BorderColor = System.Drawing.Color.Silver
        Me.DGVCuentaCte.DisplayLayout.Override.RowAppearance = Appearance32
        Me.DGVCuentaCte.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance33.BackColor = System.Drawing.SystemColors.ControlLight
        Me.DGVCuentaCte.DisplayLayout.Override.TemplateAddRowAppearance = Appearance33
        Me.DGVCuentaCte.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.DGVCuentaCte.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.DGVCuentaCte.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.DGVCuentaCte.Location = New System.Drawing.Point(6, 85)
        Me.DGVCuentaCte.Name = "DGVCuentaCte"
        Me.DGVCuentaCte.Size = New System.Drawing.Size(365, 420)
        Me.DGVCuentaCte.TabIndex = 133
        Me.DGVCuentaCte.Text = "DGVCuentaCte"
        '
        'TBMatricula
        '
        Me.TBMatricula.BackColor = System.Drawing.Color.White
        Me.TBMatricula.Enabled = False
        Me.TBMatricula.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBMatricula.ForeColor = System.Drawing.Color.Black
        Me.TBMatricula.Location = New System.Drawing.Point(202, 23)
        Me.TBMatricula.Name = "TBMatricula"
        Me.TBMatricula.ReadOnly = True
        Me.TBMatricula.Size = New System.Drawing.Size(169, 25)
        Me.TBMatricula.TabIndex = 132
        '
        'TBMatriculado
        '
        Me.TBMatriculado.BackColor = System.Drawing.Color.White
        Me.TBMatriculado.Enabled = False
        Me.TBMatriculado.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBMatriculado.ForeColor = System.Drawing.Color.Black
        Me.TBMatriculado.Location = New System.Drawing.Point(6, 54)
        Me.TBMatriculado.Name = "TBMatriculado"
        Me.TBMatriculado.ReadOnly = True
        Me.TBMatriculado.Size = New System.Drawing.Size(365, 25)
        Me.TBMatriculado.TabIndex = 131
        '
        'UTEMatricula
        '
        Appearance34.Image = CType(resources.GetObject("Appearance34.Image"), Object)
        EditorButton3.Appearance = Appearance34
        EditorButton3.Width = 70
        Me.UTEMatricula.ButtonsRight.Add(EditorButton3)
        Me.UTEMatricula.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UTEMatricula.Location = New System.Drawing.Point(6, 21)
        Me.UTEMatricula.MaxLength = 8
        Me.UTEMatricula.Name = "UTEMatricula"
        Me.UTEMatricula.Size = New System.Drawing.Size(190, 27)
        Me.UTEMatricula.TabIndex = 127
        '
        'LabelControlConfirmarImprimir
        '
        Me.LabelControlConfirmarImprimir.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LabelControlConfirmarImprimir.AutoSize = True
        Me.LabelControlConfirmarImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControlConfirmarImprimir.Location = New System.Drawing.Point(1036, 680)
        Me.LabelControlConfirmarImprimir.Name = "LabelControlConfirmarImprimir"
        Me.LabelControlConfirmarImprimir.Size = New System.Drawing.Size(43, 13)
        Me.LabelControlConfirmarImprimir.TabIndex = 31
        Me.LabelControlConfirmarImprimir.Text = "Shift + I"
        '
        'CBConfirmarImprimir
        '
        Me.CBConfirmarImprimir.BackColor = System.Drawing.Color.Transparent
        Me.CBConfirmarImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBConfirmarImprimir.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.CBConfirmarImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBConfirmarImprimir.Location = New System.Drawing.Point(978, 637)
        Me.CBConfirmarImprimir.Name = "CBConfirmarImprimir"
        Me.CBConfirmarImprimir.Size = New System.Drawing.Size(152, 44)
        Me.CBConfirmarImprimir.TabIndex = 13
        Me.CBConfirmarImprimir.Text = "Confirmar e Imprimir"
        Me.TTFrmAsiento.SetToolTip(Me.CBConfirmarImprimir, "Al presionar el boton genera el asiento y envia directamente" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "la impresión del co" &
        "mprobante en hoja A5.")
        Me.CBConfirmarImprimir.UseVisualStyleBackColor = False
        '
        'CBConfirmar
        '
        Me.CBConfirmar.AccessibleName = ""
        Me.CBConfirmar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CBConfirmar.BackColor = System.Drawing.Color.Transparent
        Me.CBConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBConfirmar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.CBConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBConfirmar.Location = New System.Drawing.Point(484, 641)
        Me.CBConfirmar.Name = "CBConfirmar"
        Me.CBConfirmar.Size = New System.Drawing.Size(145, 35)
        Me.CBConfirmar.TabIndex = 14
        Me.CBConfirmar.Text = "Confirmar"
        Me.TTFrmAsiento.SetToolTip(Me.CBConfirmar, "Al presionar el boton genera el asiento y lo previsualiza" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "para imprimir manualme" &
        "nte.")
        Me.CBConfirmar.UseVisualStyleBackColor = False
        '
        'UTECodigoBarra
        '
        Me.UTECodigoBarra.AutoSize = False
        Appearance35.Image = CType(resources.GetObject("Appearance35.Image"), Object)
        Appearance35.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance35.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton4.Appearance = Appearance35
        EditorButton4.Key = "Left"
        EditorButton4.Width = 30
        Me.UTECodigoBarra.ButtonsLeft.Add(EditorButton4)
        Appearance36.Image = CType(resources.GetObject("Appearance36.Image"), Object)
        Appearance36.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance36.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton5.Appearance = Appearance36
        EditorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton5.Key = "Right"
        Appearance37.Image = CType(resources.GetObject("Appearance37.Image"), Object)
        Appearance37.ImageBackgroundDisabled = CType(resources.GetObject("Appearance37.ImageBackgroundDisabled"), System.Drawing.Image)
        EditorButton5.PressedAppearance = Appearance37
        EditorButton5.Width = 30
        Me.UTECodigoBarra.ButtonsRight.Add(EditorButton5)
        Me.UTECodigoBarra.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTECodigoBarra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTECodigoBarra.Location = New System.Drawing.Point(6, 21)
        Me.UTECodigoBarra.Name = "UTECodigoBarra"
        Me.UTECodigoBarra.Size = New System.Drawing.Size(220, 29)
        Me.UTECodigoBarra.TabIndex = 33
        Me.TTFrmAsiento.SetToolTip(Me.UTECodigoBarra, "Ingrese el nro del código de barra")
        '
        'GBTrabajo
        '
        Me.GBTrabajo.Controls.Add(Me.LabelMontoTrabajoTexto)
        Me.GBTrabajo.Controls.Add(Me.LabelMontoTrabajo)
        Me.GBTrabajo.Controls.Add(Me.LabelMontoSigno)
        Me.GBTrabajo.Controls.Add(Me.UTECodigoBarra)
        Me.GBTrabajo.Location = New System.Drawing.Point(12, 636)
        Me.GBTrabajo.Name = "GBTrabajo"
        Me.GBTrabajo.Size = New System.Drawing.Size(371, 56)
        Me.GBTrabajo.TabIndex = 32
        Me.GBTrabajo.TabStop = False
        Me.GBTrabajo.Text = "Código de Barras"
        '
        'LabelMontoTrabajoTexto
        '
        Me.LabelMontoTrabajoTexto.AutoSize = True
        Me.LabelMontoTrabajoTexto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMontoTrabajoTexto.Location = New System.Drawing.Point(233, 11)
        Me.LabelMontoTrabajoTexto.Name = "LabelMontoTrabajoTexto"
        Me.LabelMontoTrabajoTexto.Size = New System.Drawing.Size(109, 16)
        Me.LabelMontoTrabajoTexto.TabIndex = 36
        Me.LabelMontoTrabajoTexto.Text = "Monto Trabajo"
        '
        'LabelMontoTrabajo
        '
        Me.LabelMontoTrabajo.AutoSize = True
        Me.LabelMontoTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMontoTrabajo.Location = New System.Drawing.Point(245, 31)
        Me.LabelMontoTrabajo.Name = "LabelMontoTrabajo"
        Me.LabelMontoTrabajo.Size = New System.Drawing.Size(60, 16)
        Me.LabelMontoTrabajo.TabIndex = 35
        Me.LabelMontoTrabajo.Text = "0000,00"
        '
        'LabelMontoSigno
        '
        Me.LabelMontoSigno.AutoSize = True
        Me.LabelMontoSigno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMontoSigno.Location = New System.Drawing.Point(233, 31)
        Me.LabelMontoSigno.Name = "LabelMontoSigno"
        Me.LabelMontoSigno.Size = New System.Drawing.Size(16, 16)
        Me.LabelMontoSigno.TabIndex = 34
        Me.LabelMontoSigno.Text = "$"
        '
        'ULMontoCobrar
        '
        Appearance38.TextHAlignAsString = "Right"
        Appearance38.TextVAlignAsString = "Middle"
        Me.ULMontoCobrar.Appearance = Appearance38
        Me.ULMontoCobrar.AutoSize = True
        Me.ULMontoCobrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ULMontoCobrar.Location = New System.Drawing.Point(908, 80)
        Me.ULMontoCobrar.Name = "ULMontoCobrar"
        Me.ULMontoCobrar.Size = New System.Drawing.Size(70, 17)
        Me.ULMontoCobrar.TabIndex = 34
        Me.ULMontoCobrar.Text = "000000.00"
        Me.ULMontoCobrar.Visible = False
        '
        'BMCEfectivo
        '
        Me.BMCEfectivo.Location = New System.Drawing.Point(908, 99)
        Me.BMCEfectivo.Name = "BMCEfectivo"
        Me.BMCEfectivo.Size = New System.Drawing.Size(75, 23)
        Me.BMCEfectivo.TabIndex = 35
        Me.BMCEfectivo.Text = "Efectivo"
        Me.BMCEfectivo.UseVisualStyleBackColor = True
        Me.BMCEfectivo.Visible = False
        '
        'ULMCTexto
        '
        Appearance39.TextHAlignAsString = "Right"
        Appearance39.TextVAlignAsString = "Middle"
        Me.ULMCTexto.Appearance = Appearance39
        Me.ULMCTexto.AutoSize = True
        Me.ULMCTexto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ULMCTexto.Location = New System.Drawing.Point(782, 80)
        Me.ULMCTexto.Name = "ULMCTexto"
        Me.ULMCTexto.Size = New System.Drawing.Size(114, 17)
        Me.ULMCTexto.TabIndex = 36
        Me.ULMCTexto.Text = "Monto a cobrar: $"
        Me.ULMCTexto.Visible = False
        '
        'BMCCheque
        '
        Me.BMCCheque.Location = New System.Drawing.Point(1012, 99)
        Me.BMCCheque.Name = "BMCCheque"
        Me.BMCCheque.Size = New System.Drawing.Size(75, 23)
        Me.BMCCheque.TabIndex = 37
        Me.BMCCheque.Text = "Cheque"
        Me.BMCCheque.UseVisualStyleBackColor = True
        Me.BMCCheque.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "add.ico")
        Me.ImageList1.Images.SetKeyName(1, "Search (2).ico")
        Me.ImageList1.Images.SetKeyName(2, "delete.ico")
        Me.ImageList1.Images.SetKeyName(3, "application_view_columns.png")
        Me.ImageList1.Images.SetKeyName(4, "no.png")
        Me.ImageList1.Images.SetKeyName(5, "si.png")
        Me.ImageList1.Images.SetKeyName(6, "pay.png")
        '
        'FrmAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1280, 693)
        Me.Controls.Add(Me.BMCCheque)
        Me.Controls.Add(Me.ULMCTexto)
        Me.Controls.Add(Me.BMCEfectivo)
        Me.Controls.Add(Me.ULMontoCobrar)
        Me.Controls.Add(Me.GBTrabajo)
        Me.Controls.Add(Me.CBConfirmarImprimir)
        Me.Controls.Add(Me.LabelControlConfirmarImprimir)
        Me.Controls.Add(Me.GBMatriculado)
        Me.Controls.Add(Me.LabelControlConfirmar)
        Me.Controls.Add(Me.UGProcesos)
        Me.Controls.Add(Me.UltraLabel5)
        Me.Controls.Add(Me.UltraLabel4)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.CBConfirmar)
        Me.Controls.Add(Me.UGDetalles)
        Me.Controls.Add(Me.UltraLabel2)
        Me.Controls.Add(Me.UltraLabel1)
        Me.Controls.Add(Me.UltraTextEditor1)
        Me.Controls.Add(Me.UltraLabel3)
        Me.Controls.Add(Me.UltraDateTimeEditor1)
        Me.Controls.Add(Me.UltraComboEditor1)
        Me.Controls.Add(Me.TxtConcepto2)
        Me.Controls.Add(Me.TxtConcepto3)
        Me.Controls.Add(Me.TxtConcepto1)
        Me.Controls.Add(Me.TxtDestinatarioExterno)
        Me.Controls.Add(Me.UCEdelegacion)
        Me.Controls.Add(Me.UltraLabel6)
        Me.Controls.Add(Me.LabelDestinatarioExterno)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmAsiento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiento"
        CType(Me.UGDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTextEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGProcesos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBMatriculado.ResumeLayout(False)
        Me.GBMatriculado.PerformLayout()
        CType(Me.DGVCuentaCte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEMatricula, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBTrabajo.ResumeLayout(False)
        Me.GBTrabajo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGDetalles As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraTextEditor1 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UGProcesos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraLabel1 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraLabel2 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraLabel3 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents CBConfirmar As System.Windows.Forms.Button
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TxtHaber As System.Windows.Forms.TextBox
    Friend WithEvents TxtDebe As System.Windows.Forms.TextBox
    Friend WithEvents UltraDateTimeEditor1 As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraLabel4 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents TxtConcepto1 As System.Windows.Forms.TextBox
    Friend WithEvents UltraLabel5 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraComboEditor1 As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents TxtConcepto2 As System.Windows.Forms.TextBox
    Friend WithEvents TxtConcepto3 As System.Windows.Forms.TextBox
    Friend WithEvents ImpresionPrint As System.Drawing.Printing.PrintDocument
    Friend WithEvents ImpresionDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents UltraLabel6 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UCEdelegacion As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents TxtDestinatarioExterno As System.Windows.Forms.TextBox
    Friend WithEvents LabelDestinatarioExterno As System.Windows.Forms.Label
    Friend WithEvents LabelControlConfirmar As System.Windows.Forms.Label
    Friend WithEvents GBMatriculado As System.Windows.Forms.GroupBox
    Friend WithEvents UTEMatricula As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents TBMatricula As System.Windows.Forms.TextBox
    Friend WithEvents TBMatriculado As System.Windows.Forms.TextBox
    Friend WithEvents LabelControlConfirmarImprimir As System.Windows.Forms.Label
    Friend WithEvents CBConfirmarImprimir As System.Windows.Forms.Button
    Friend WithEvents TTFrmAsiento As System.Windows.Forms.ToolTip
    Friend WithEvents DGVCuentaCte As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents GBTrabajo As System.Windows.Forms.GroupBox
    Friend WithEvents UTECodigoBarra As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents LabelMontoTrabajo As System.Windows.Forms.Label
    Friend WithEvents LabelMontoSigno As System.Windows.Forms.Label
    Friend WithEvents LabelMontoTrabajoTexto As System.Windows.Forms.Label
    Friend WithEvents ULMontoCobrar As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents BMCEfectivo As Button
    Friend WithEvents ULMCTexto As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents BMCCheque As Button
    Friend WithEvents ImageList1 As ImageList
End Class
