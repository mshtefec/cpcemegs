﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl

Public Class FrmAsiento
    Private cnn As New ConsultaBD(True)
    Private DAglobalMysql As MySqlDataAdapter
    Private DSProcesos As DataSet
    Private BSProcesos As BindingSource
    Private DSDetalles As DataSet
    Private BSDetalles As BindingSource
    Private DTSubCuenta As DataTable
    Private DTCheques As DataTable
    Private DTAsiImportado As DataTable
    Private DTCuotasServicio As New DataTable
    Private DTCuotasMoratoria As New DataTable
    Private nCaja As Integer
    Private nZeta As Integer
    Private Prestamo As Double = 0
    Private permiteDestinatarioAmbos As Boolean = False
    Private permiteDestinatarioExterno As Boolean = False
    Private cuentasShowCuotas As String() = {"13010200", "13010600", "13010200", "13051000", "13051100", "13051600", "13040100", "13040400", "13050700", "13050900", "13052200"}
    Private listadoDeBancos As ValueList = New ValueList()
    'para cargar cuenta corriente
    Private DSCuentaCte As DataSet

    Dim Reporte As String = ""

    'Imprimir el Recibo
    Private DTImprimir As DataTable
    Private DTImpDeta As DataTable
    Private DTAsiento As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long
    'Para guardar los cheques aparte
    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter
    Private CantCheques As Integer = 0
    Dim msgError As String = Nothing 'Mostrar mensaje de error
    Private globalEsRecibo As Boolean = False 'Si es recibo
    Private globalNoPuedeEditarFecha As Boolean = False 'Puede editar fecha
    'Trabajos.
    'globalTrabajoId En MuestraProcesos() ConfirmarAsiento() lo seteo en 0 y BuscaTrabajoIngresado()
    Private globalTrabajoId As Integer = 0
    Private globalTrabajoEstado As Integer = 3
    'Datos del matriculado buscado
    Dim c_TipoDoc As String
    Dim n_Nrodoc As Integer

    Private rest As New Rest

    Private Sub FrmAsiento_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'CantCheques = 0
        If ComprobarFechaServidor(False) Then
            Close()
            Exit Sub
        End If
        'Si Accion = 3 y tiene permiso "U" puede cambiar la fecha
        If controlAcceso(4, 3, "U", False) = True Then
            globalNoPuedeEditarFecha = False
        Else
            globalNoPuedeEditarFecha = True
        End If
        CargaProcesos()
        CargaBancos()
        CargaDelegaciones()
    End Sub
    Private Sub CargaProcesos()
        Try
            DAglobalMysql = cnn.consultaBDadapter(
                "procesos",
                "pro_codigo AS proceso,pro_nombre AS Descripcion,pro_buscaf,pro_bloqfech,pro_cajachica,pro_instit,pro_cpto1,pro_cpto2,pro_cpto3,pro_leyenda1,pro_leyenda2,pro_leyenda3,pro_leyenda4,pro_reporte" & nPubNroCli & ",pro_catexcl,pro_destinatario",
                "pro_activo = 'Si' ORDER BY pro_nombre"
            )
            DSProcesos = New DataSet
            DAglobalMysql.Fill(DSProcesos, "procesos")
        Catch ex As Exception
            mostrarErrorConexion("obtener datos.")
            Close()
            Exit Sub
        End Try
        BSProcesos = New BindingSource
        BSProcesos.DataSource = DSProcesos.Tables(0)
        UltraDateTimeEditor1.DateTime = Now

        If globalNoPuedeEditarFecha Then
            UltraDateTimeEditor1.ReadOnly = True
        Else
            UltraDateTimeEditor1.ReadOnly = False
        End If
    End Sub
    'LISTADO DE BANCOS
    Private Sub CargaBancos()
        listadoDeBancos.ValueListItems.Add("CHACO", "CHACO")
        listadoDeBancos.ValueListItems.Add("CITIBANK", "CITIBANK")
        listadoDeBancos.ValueListItems.Add("CIUDAD", "CIUDAD")
        listadoDeBancos.ValueListItems.Add("COMAFI", "COMAFI")
        listadoDeBancos.ValueListItems.Add("CORDOBA", "CORDOBA")
        listadoDeBancos.ValueListItems.Add("CORRIENTES", "CORRIENTES")
        listadoDeBancos.ValueListItems.Add("CREDICOOP", "CREDICOOP")

        listadoDeBancos.ValueListItems.Add("ENTRE RIOS", "ENTRE RIOS")

        listadoDeBancos.ValueListItems.Add("FORMOSA", "FORMOSA")
        listadoDeBancos.ValueListItems.Add("FRANCES", "FRANCES")

        listadoDeBancos.ValueListItems.Add("GALICIA", "GALICIA")

        listadoDeBancos.ValueListItems.Add("HIPOTECARIO", "HIPOTECARIO")
        listadoDeBancos.ValueListItems.Add("HSBC", "HSBC")

        listadoDeBancos.ValueListItems.Add("ICBC", "ICBC")

        listadoDeBancos.ValueListItems.Add("MACRO", "MACRO")

        listadoDeBancos.ValueListItems.Add("NACION", "NACION")

        listadoDeBancos.ValueListItems.Add("PATAGONIA", "PATAGONIA")
        listadoDeBancos.ValueListItems.Add("PROVINCIA BSAS", "PROVINCIA BSAS")

        listadoDeBancos.ValueListItems.Add("STANDARD BANK", "STANDARD BANK")
        listadoDeBancos.ValueListItems.Add("SANTA FE", "SANTA FE")
        listadoDeBancos.ValueListItems.Add("SANTANDER RIO", "SANTANDER RIO")
        listadoDeBancos.ValueListItems.Add("SANTIAGO EST", "SANTIAGO EST")
        listadoDeBancos.ValueListItems.Add("SUPERVIELLE", "SUPERVIELLE")

        listadoDeBancos.ValueListItems.Add("TUCUMAN", "TUCUMAN")

        listadoDeBancos.ValueListItems.Add("VARIOS", "VARIOS")
    End Sub
    'LISTADO DE DELEGACIONES
    Private Sub CargaDelegaciones()
        UCEdelegacion.Items.Add(1, "Resistencia")
        UCEdelegacion.Items.Add(2, "Saenz Peña")
        UCEdelegacion.Items.Add(3, "Villa Angela")
        UCEdelegacion.Items.Add(4, "Sudoeste")
        UCEdelegacion.Items.Add(5, "Club SDC")
    End Sub

    Private Sub SeleccionaProceso()
        Dim row As DataRowView = BSProcesos.Current
        UltraTextEditor1.Text = row.Item("proceso")
        UltraLabel1.Text = row.Item("Descripcion")
        TxtConcepto1.Text = row.Item("pro_cpto1")
        TxtConcepto2.Text = row.Item("pro_cpto2")
        TxtConcepto3.Text = row.Item("pro_cpto3")
        UltraLabel1.Visible = True
        UGProcesos.Visible = False
        TxtDebe.Text = ""
        TxtHaber.Text = ""
        Reporte = row.Item("pro_reporte" & nPubNroCli)
        If row.Item("pro_bloqfech").ToString = "Si" Then
            If globalNoPuedeEditarFecha Then
                UltraDateTimeEditor1.ReadOnly = True
            Else
                UltraDateTimeEditor1.ReadOnly = False
            End If
            UltraComboEditor1.ReadOnly = True
            UCEdelegacion.ReadOnly = True
        Else
            UltraDateTimeEditor1.ReadOnly = False
            UltraComboEditor1.ReadOnly = False
            UCEdelegacion.ReadOnly = False
        End If
        If row.Item("pro_cajachica") = "Si" Then
            If cnn.CajaActiva Then
                nZeta = cnn.Zeta
                nCaja = cnn.Caja
            Else
                nZeta = 0
                nCaja = 0
                CBConfirmar.Enabled = False
                CBConfirmarImprimir.Enabled = False
            End If
        Else
            nZeta = 0
            nCaja = 0
        End If
    End Sub

    Private Sub LimpiarProceso()
        Try
            UltraTextEditor1.Text = ""
            UltraLabel1.Visible = False
            UGProcesos.Visible = False
            TxtDebe.Text = ""
            TxtHaber.Text = ""
            TxtConcepto1.Text = ""
            TxtConcepto2.Text = ""
            TxtConcepto3.Text = ""
            TxtDestinatarioExterno.Text = ""
            UltraDateTimeEditor1.DateTime = Now
            UTECodigoBarra.Text = ""
            UltraComboEditor1.Value = Nothing
            UGDetalles.DataSource = Nothing
            nZeta = 0
            nCaja = 0
            Prestamo = 0
            CBConfirmar.Enabled = True
            CBConfirmarImprimir.Enabled = True
            DSDetalles.Clear()
            DSDetalles.Dispose()
            ULMCTexto.Text = ""
            ULMCTexto.Visible = False
            ULMontoCobrar.Text = ""
            ULMontoCobrar.Visible = False
            BMCEfectivo.Visible = False
            BMCCheque.Visible = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraProcesos()
        LabelMontoTrabajo.Text = "0000,00" 'Seteo valor por defecto
        globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
        If UGProcesos.Visible Then
            SeleccionaProceso()
        Else
            Dim fila As Integer = BSProcesos.Find("proceso", UltraTextEditor1.Text)
            If fila > 0 Then
                BSProcesos.Position = fila
                SeleccionaProceso()
            Else
                UltraLabel1.Visible = False
                UGProcesos.DataSource = BSProcesos
                With UGProcesos.DisplayLayout.Bands(0)
                    .Columns(1).Width = 420
                    .Columns(2).Hidden = True
                    .Columns(3).Hidden = True
                    .Columns(4).Hidden = True
                    .Columns(5).Hidden = True
                    .Columns(6).Hidden = True
                    .Columns(7).Hidden = True
                    .Columns(8).Hidden = True
                    .Columns(9).Hidden = True
                    .Columns(10).Hidden = True
                    .Columns(11).Hidden = True
                    .Columns(12).Hidden = True
                    .Columns(13).Hidden = True
                End With
                UGProcesos.Top = 37
                UGProcesos.Left = 87
                UGProcesos.Visible = True
                UGProcesos.Height = 252
                UGProcesos.Width = 560
            End If
        End If
    End Sub

    'Private Sub ImportarAsiento()
    '    DAglobalMysql = cnn.consultaBDadapter("totales", , "tot_nroasi=" & UltraTextEditor1.Value)
    '    DTAsiImportado = New DataTable
    '    DAglobalMysql.Fill(DTAsiImportado)
    '    If DTAsiImportado.Rows.Count > 0 Then
    '        'UltraTextEditor1.Value = DTAsiImportado.Rows(0).Item("tot_proceso")
    '        UltraTextEditor1.Value = "MANUAL"
    '        MuestraProcesos()
    '        MuestraDetalles()
    '        Dim nitemImportar As Integer = 0
    '        For Each RowImportar As DataRow In DTAsiImportado.Rows
    '            If DSDetalles.Tables(0).Rows.Count > 1 Then
    '                AgregarDetalleVacio(RowImportar.Item("tot_nropla"))
    '            End If
    '            DSDetalles.Tables(0).Rows(nitemImportar).Item("cuenta") = RowImportar.Item("tot_nropla")
    '            If RowImportar.Item("tot_nrodoc") > 0 Then
    '                DSDetalles.Tables(0).Rows(nitemImportar).Item("subcuenta") = RowImportar.Item("tot_tipdoc") & RowImportar.Item("tot_nrodoc")
    '                DSDetalles.Tables(0).Rows(nitemImportar).Item("matricula") = RowImportar.Item("tot_titulo") & RowImportar.Item("tot_matricula")
    '                DSDetalles.Tables(0).Rows(nitemImportar).Item("destinatario") = True
    '            End If
    '            DSDetalles.Tables(0).Rows(nitemImportar).Item("debe") = RowImportar.Item("tot_debe")
    '            DSDetalles.Tables(0).Rows(nitemImportar).Item("haber") = RowImportar.Item("tot_haber")
    '            nitemImportar += 1
    '        Next

    '        Dim cMtr As String
    '        For Each RowDeta As UltraGridRow In UGDetalles.Rows
    '            'RowDeta.Activate()
    '            'UGDetalles.PerformAction(UltraGridAction.ActivateCell)
    '            cMtr = RowDeta.Cells("matricula").Value
    '            CargarCuentaManual()
    '            If cMtr.Trim <> "" Then
    '                UGDetalles.PerformAction(UltraGridAction.NextRow)
    '                UGDetalles.PerformAction(UltraGridAction.ActivateCell)
    '                Dim rowMtr As UltraGridRow = UGDetalles.ActiveRow
    '                rowMtr.Cells("matricula").Value = cMtr
    '                MuestraDestinatario(False)
    '                FormBuscoCuotas()
    '            End If
    '        Next
    '    End If
    'End Sub
    Private Sub ImportarAsiento()
        'Si Accion = 4 y si tiene permiso "U" puede duplicar comprobante
        If controlAcceso(4, 4, "U", False) = False Then
            Exit Sub
        End If
        DAglobalMysql = cnn.consultaBDadapter("totales", , "tot_nroasi=" & UltraTextEditor1.Value)
        DTAsiImportado = New DataTable
        DAglobalMysql.Fill(DTAsiImportado)

        Dim DTAsiImportadoClone As DataTable = New DataTable

        If DTAsiImportado.Rows.Count > 0 Then
            UltraTextEditor1.Value = DTAsiImportado.Rows(0).Item("tot_proceso")
            BuscaProcesoIngresado()
            DTAsiImportadoClone = DTAsiImportado.Copy()
            Dim nitemDetalles As Integer = DSDetalles.Tables("detalles").Rows.Count - 1
            Dim indexUtilizados As New Dictionary(Of Integer, Integer)
            'PRIMER RECORRIDO
            For Each RowImportar As DataRow In DTAsiImportado.Rows
                For index As Integer = 0 To nitemDetalles
                    If DSDetalles.Tables("detalles").Rows(index).Item("cuenta") = RowImportar.Item("tot_nropla") Then
                        If Not indexUtilizados.ContainsKey(index) Then
                            'COMENTO ESTO POR EL MOMENTO PARA QUE NO AGREGUE MAS CUENTAS QUE EL PROCESO
                            'If nitemDetalles > nitemImportar Then
                            'Else
                            '    AgregarDetalleVacio(RowImportar.Item("tot_nropla"))
                            'End If
                            DSDetalles.Tables("detalles").Rows(index).Item("cuenta") = RowImportar.Item("tot_nropla")
                            If RowImportar.Item("tot_nrodoc") > 0 Then
                                DSDetalles.Tables("detalles").Rows(index).Item("subcuenta") = RowImportar.Item("tot_tipdoc") & RowImportar.Item("tot_nrodoc")
                                DSDetalles.Tables("detalles").Rows(index).Item("matricula") = RowImportar.Item("tot_titulo") & RowImportar.Item("tot_matricula")
                                DSDetalles.Tables("detalles").Rows(index).Item("destinatario") = True
                            End If
                            DSDetalles.Tables("detalles").Rows(index).Item("debe") = RowImportar.Item("tot_debe")
                            DSDetalles.Tables("detalles").Rows(index).Item("haber") = RowImportar.Item("tot_haber")

                            indexUtilizados.Add(index, index)
                            Exit For
                        End If
                    End If
                Next
                'nitemImportar += 1
            Next
            'SI NO ENTRO EN EL PRIMERO
            If indexUtilizados.Count() <= 0 Then
                'SEGUNDO RECORRIDO
                Dim index As Integer = 0
                For Each RowImportar As DataRow In DTAsiImportado.Rows
                    If index > nitemDetalles Then
                        'AGREGO MAS CUENTAS
                        AgregarDetalleVacio(RowImportar.Item("tot_nropla"))
                    Else
                        DSDetalles.Tables("detalles").Rows(index).Item("cuenta") = RowImportar.Item("tot_nropla")
                    End If
                    index += 1
                Next
                'TERCER RECORRIDO
                For Each RowDeta As UltraGridRow In UGDetalles.Rows
                    RowDeta.Activate()
                    UGDetalles.PerformAction(UltraGridAction.ActivateCell)
                    CargarCuentaManual()
                Next
                'CUARTO RECORRIDO
                index = 0
                For Each RowImportar As DataRow In DTAsiImportado.Rows
                    'If index > nitemDetalles Then
                    '    'AGREGO MAS CUENTAS
                    '    AgregarDetalleVacio(RowImportar.Item("tot_nropla"))
                    'End If
                    DSDetalles.Tables("detalles").Rows(index).Item("cuenta") = RowImportar.Item("tot_nropla")
                    If RowImportar.Item("tot_nrodoc") > 0 Then
                        DSDetalles.Tables("detalles").Rows(index).Item("subcuenta") = RowImportar.Item("tot_tipdoc") & RowImportar.Item("tot_nrodoc")
                        DSDetalles.Tables("detalles").Rows(index).Item("matricula") = RowImportar.Item("tot_titulo") & RowImportar.Item("tot_matricula")
                        DSDetalles.Tables("detalles").Rows(index).Item("destinatario") = True
                    End If
                    DSDetalles.Tables("detalles").Rows(index).Item("debe") = RowImportar.Item("tot_debe")
                    DSDetalles.Tables("detalles").Rows(index).Item("haber") = RowImportar.Item("tot_haber")
                    index += 1
                Next
            End If
            'ULTIMO RECORRIDO
            Dim cMtr As String
            Dim tipoMatricula As String
            For Each RowDeta As UltraGridRow In UGDetalles.Rows
                RowDeta.Activate()
                UGDetalles.PerformAction(UltraGridAction.ActivateCell)
                cMtr = RowDeta.Cells("matricula").Value
                If cMtr.Trim <> "" And cMtr.Trim <> "0" Then
                    Dim rowMtr As UltraGridRow = UGDetalles.ActiveRow
                    For Each RowImportar As DataRow In DTAsiImportadoClone.Rows
                        If rowMtr.Cells("cuenta").Value = RowImportar.Item("tot_nropla") Then
                            UGDetalles.PerformAction(UltraGridAction.NextRow)
                            UGDetalles.PerformAction(UltraGridAction.ActivateCell)
                            rowMtr = UGDetalles.ActiveRow
                            tipoMatricula = Mid(cMtr, 1, 2)
                            If tipoMatricula = "CP" Then
                                rowMtr.Cells("matricula").Value = cMtr
                                MuestraDestinatario(False)
                                FormBuscoCuotas()
                            ElseIf tipoMatricula = "BC" Then
                                Try
                                    rowMtr.Cells("banco").Value = RowImportar.Item("tot_concepto")
                                    rowMtr.Cells("cheque").Value = RowImportar.Item("tot_nrocheque")
                                    rowMtr.Cells("fechaDiferida").Value = RowImportar.Item("tot_fecdif").ToString()
                                    If RowImportar.Item("tot_debe") > 0 Then
                                        rowMtr.Cells("monto").Value = RowImportar.Item("tot_debe")
                                    Else
                                        rowMtr.Cells("monto").Value = RowImportar.Item("tot_haber")
                                    End If
                                Catch ex As Exception
                                    'CONTINUA SU HUBO ERROR
                                End Try
                            End If
                            DTAsiImportadoClone.Rows.Remove(RowImportar)
                            Exit For
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Private Sub MuestraDestinatario(ByVal lLimpia As Boolean, Optional ByVal rowDesti As UltraGridRow = Nothing)
        'Si no se pasa el parametro rowDesti lo seteo con el ActiveRow
        If IsNothing(rowDesti) Then
            rowDesti = UGDetalles.ActiveRow
        End If
        If lLimpia Then
            UGProcesos.Visible = False
            GrabarDestinatario(rowDesti, lLimpia)
        Else
            Dim frmDesti As New FrmDestinatario
            If UGProcesos.Visible Then
                UGProcesos.Visible = False
                GrabarDestinatario(rowDesti, lLimpia)
            Else
                If Mid(rowDesti.Cells(3).Text, 1, 2) <> "" And Mid(rowDesti.Cells(3).Text, 3) <> "" Then
                    'si de matricula los 2 primeros caracteres no son null y los siguientes tampoco entra
                    Dim tipoMatricula As String = Mid(rowDesti.Cells(3).Text, 1, 2)
                    Dim numeroMatricula As String = Mid(rowDesti.Cells(3).Text, 3)
                    frmDesti.Condicion = "afi_titulo='" & tipoMatricula & "' and afi_matricula=" & CInt(numeroMatricula)
                Else
                    frmDesti.Condicion = "afi_nombre like '" & Trim(rowDesti.Cells(2).Text) & "%'"
                End If
                frmDesti.CargaDestinatario()
                If frmDesti.MatriculaDestinatario <> "" Then
                    If frmDesti.Categoria <> "" AndAlso DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_catexcl").ToString.Contains(frmDesti.Categoria) Then
                        MessageBox.Show("El Profesional " & frmDesti.NombreDestinatario & " esta " & frmDesti.NombreCategoria)
                        rowDesti.Cells("Destinatario").Value = ""
                        rowDesti.Cells("matricula").Value = ""
                        'rowDesti.Cells("TipoDoc").Value = ""
                        'rowDesti.Cells("NroDoc").Value = 0
                        'GrabarDestinatario(rowDesti, lLimpia)
                    Else
                        If Mid(frmDesti.MatriculaDestinatario, 1, 2) = "ET" Then
                            Dim dateNow As Date = Now()
                            Dim stringNow As String = dateNow.ToString("dd'/'MM'/'yyyy")
                            If frmDesti.FechaVencimientoET <= stringNow Then
                                rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                                rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                                rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                                rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario
                                GrabarDestinatario(rowDesti, lLimpia)
                            Else
                                MessageBox.Show(frmDesti.NombreDestinatario & " El plazo para presentar su matricula se encuentra vencido.")
                                GrabarDestinatario(rowDesti, True)
                            End If
                        Else
                            rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                            rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                            rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                            rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario
                            GrabarDestinatario(rowDesti, lLimpia)
                        End If
                    End If
                Else
                    frmDesti.ShowDialog()
                    If frmDesti.DialogResult = DialogResult.OK Then
                        If frmDesti.Categoria <> "" AndAlso DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_catexcl").ToString.Contains(frmDesti.Categoria) Then
                            MessageBox.Show("El Profesional " & frmDesti.NombreDestinatario & " esta " & frmDesti.NombreCategoria)
                        Else
                            If Mid(frmDesti.MatriculaDestinatario, 1, 2) = "ET" Then
                                Dim dateNow As Date = Now()
                                Dim stringNow As String = dateNow.ToString("dd'/'MM'/'yyyy")
                                If frmDesti.FechaVencimientoET <= stringNow Then
                                    rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                                    rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                                    rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                                    rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario
                                    GrabarDestinatario(rowDesti, lLimpia)
                                Else
                                    MessageBox.Show(frmDesti.NombreDestinatario & " El plazo para presentar su matricula se encuentra vencido.")
                                    GrabarDestinatario(rowDesti, True)
                                End If
                            Else
                                rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                                rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                                rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                                rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario

                                GrabarDestinatario(rowDesti, lLimpia)
                            End If
                        End If
                    End If
                End If
            End If
            frmDesti.Dispose()
        End If
    End Sub
    Private Sub FormBuscoCuotas(Optional ByVal rowDesti As UltraGridRow = Nothing)
        'Si no se pasa el parametro rowDesti lo seteo con el ActiveRow
        If IsNothing(rowDesti) Then
            rowDesti = UGDetalles.ActiveRow
        End If
        Dim Cuenta As String = DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta")
        If cuentasShowCuotas.Contains(Cuenta) Then 'INTERESES PARA LAS CUENTAS DEFINIDAS - MUESTRO CUOTAS TAMBIEN SIN INTERES.
            BuscoCuotas(rowDesti)
        End If
    End Sub

    Private Sub GrabarDestinatario(ByRef rowDesti As UltraGridRow, ByVal lLimpia As Boolean)
        If lLimpia Then
            rowDesti.Cells("Destinatario").Value = ""
            rowDesti.Cells("matricula").Value = ""
            rowDesti.Cells("TipoDoc").Value = ""
            rowDesti.Cells("NroDoc").Value = 0
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("subcuenta") = ""
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = ""
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = 0
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = False
            'If DSProcesos.Tables("procesos").Rows(0).Item("pro_buscaf") = "S" Then
            If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_buscaf") = "Si" Then
                For Each RowDestinatario As DataRow In DSDetalles.Tables("subcuenta").Rows
                    RowDestinatario.Item("Destinatario") = ""
                    RowDestinatario.Item("matricula") = ""
                    RowDestinatario.Item("tipodoc") = ""
                    RowDestinatario.Item("NroDoc") = 0
                Next
            End If
        Else
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Subcuenta") = rowDesti.Cells("TipoDoc").Value & Format(rowDesti.Cells("NroDoc").Value, "00000000")
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = rowDesti.Cells("Matricula").Value
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = True

            If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_buscaf") = "Si" Then
                For Each RowDestinatario As DataRow In DSDetalles.Tables("subcuenta").Rows
                    RowDestinatario.Item("Destinatario") = rowDesti.Cells("destinatario").Value
                    RowDestinatario.Item("matricula") = rowDesti.Cells("matricula").Value
                    RowDestinatario.Item("tipodoc") = rowDesti.Cells("TipoDoc").Value
                    RowDestinatario.Item("NroDoc") = rowDesti.Cells("NroDoc").Value
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("Subcuenta") = rowDesti.Cells("TipoDoc").Value & Format(rowDesti.Cells("NroDoc").Value, "00000000")
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("Matricula") = rowDesti.Cells("Matricula").Value
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("destinatario") = True
                Next
            End If

            Select Case UltraTextEditor1.Text.Substring(2, 1)
                Case "R" ' si es recibo , busco cuotas a cancelar
                    'Dim frmCuotas As New FrmCuotasImpagas(cnn, Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta"), Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_tipmov"), rowDesti.Cells("TipoDoc").Value, rowDesti.Cells("nrodoc").Value)
                    'frmCuotas.Text = "Cuotas impagas de " & rowDesti.Cells(2).Value
                    'frmCuotas.ShowDialog()
                    'If frmCuotas.DialogResult = Windows.Forms.DialogResult.OK Then
                    '    Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = frmCuotas.GetMontoImpagas
                    '    DTCuotasServicio = frmCuotas.GetCuotasCanceladas
                    '    ' agrego columna de item
                    '    Dim newCol As DataColumn
                    '    newCol = New DataColumn("item", Type.GetType("System.Int32"))
                    '    newCol.DefaultValue = rowDesti.Cells("item"ABValue - 1
                    '    DTCuotasServicio.Columns.Add(newCol)
                    'End If
                Case "P" ' si es orden da pago
                    If DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pla_cuotas") = "Si" Then
                        FrmCuotasPrestamo.ShowDialog()
                        If FrmCuotasPrestamo.DialogResult = DialogResult.OK Then
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("debe") = FrmCuotasPrestamo.ImportePrestamo
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuotas") = FrmCuotasPrestamo.CuotasPrestamo
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("fecven") = FrmCuotasPrestamo.Vencimiento
                            Prestamo = FrmCuotasPrestamo.Prestamo
                        End If
                    End If
            End Select
        End If

        SumoTotales()
    End Sub

    Private Sub BuscoCuotas(ByRef rowDesti As UltraGridRow)
        If rowDesti.Cells("nrodoc").Value > 0 Then
            Dim frmCuotas As New FrmCuotasImpagas(cnn, UltraTextEditor1.Text, DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta"), DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_tipmov"), rowDesti.Cells("TipoDoc").Value, rowDesti.Cells("nrodoc").Value)
            frmCuotas.Text = "Cuotas impagas de " & rowDesti.Cells(2).Value
            frmCuotas.StartPosition = FormStartPosition.Manual
            frmCuotas.ShowDialog()
            BuscoCuotasFormDialogResult(frmCuotas, rowDesti)
        End If
    End Sub
    Private Sub BuscoCuotasFormDialogResult(ByRef frmCuotas As FrmCuotasImpagas, ByRef rowDesti As UltraGridRow)
        If frmCuotas.DialogResult = DialogResult.OK Then
            If UltraTextEditor1.Text <> "MANUAL" And rowDesti.Cells("item").Value >= 2 Then
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("haber") = frmCuotas.GetMontoInteres
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("pto_valor") = 0
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("pto_formula") = ""
            End If

            ULMCTexto.Visible = True
            ULMontoCobrar.Visible = True
            ULMontoCobrar.Text = (frmCuotas.GetMontoImpagas + frmCuotas.GetMontoInteres)
            BMCEfectivo.Visible = True
            BMCCheque.Visible = True

            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = frmCuotas.GetMontoImpagas
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_valor") = 0
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_formula") = ""
            'si es un recibo de moratoria cargo los importes en las cuentas
            If UltraTextEditor1.Text = "01RCMO" Or UltraTextEditor1.Text = "01DCMO" Or UltraTextEditor1.Text = "MORMAN" Then
                DTCuotasMoratoria = frmCuotas.GetCuotasCanceladas
                Dim dtCuotasClone As DataTable = DTCuotasMoratoria.Copy
                'Si tiene cuotas es porque trajo de la tabla de cuo_det
                If dtCuotasClone.Rows.Count() > 0 Then
                    Dim rowCuoSelect As DataRow = dtCuotasClone.Rows(0)
                    rowCuoSelect.Item("TIPO") = 1
                    rowCuoSelect.Item("total") = 0

                    rowCuoSelect.Item("D1") = 0
                    rowCuoSelect.Item("D2") = 0
                    rowCuoSelect.Item("D3") = 0
                    rowCuoSelect.Item("C1") = 0
                    rowCuoSelect.Item("C2") = 0
                    rowCuoSelect.Item("C3") = 0
                    rowCuoSelect.Item("C4") = 0
                    rowCuoSelect.Item("C5") = 0
                    rowCuoSelect.Item("C6") = 0
                    rowCuoSelect.Item("C7") = 0
                    rowCuoSelect.Item("C8") = 0
                    Dim porcentaje As Double = 1
                    Dim calculos As New Calculos
                    For Each RowCuo_det As DataRow In DTCuotasMoratoria.Rows
                        'If RowCuo_det.Item("TIPO") = 1 Then
                        If RowCuo_det.Item("Select") Then
                            If RowCuo_det.Item("total") <> RowCuo_det.Item("totalReal") Then
                                porcentaje = (RowCuo_det.Item("total") / RowCuo_det.Item("totalReal"))
                            Else
                                porcentaje = 1
                            End If

                            rowCuoSelect.Item("total") = Math.Round(rowCuoSelect.Item("total") + RowCuo_det.Item("total"), 2)
                            'Calculo el porcentaje con el metodo de la clase Calculos
                            calculos.calculoPorcentajeMoratoria(RowCuo_det)
                            rowCuoSelect.Item("D1") += RowCuo_det.Item("D1")
                            rowCuoSelect.Item("D2") += RowCuo_det.Item("D2")
                            rowCuoSelect.Item("D3") += RowCuo_det.Item("D3")
                            rowCuoSelect.Item("C1") += RowCuo_det.Item("C1")
                            rowCuoSelect.Item("C2") += RowCuo_det.Item("C2")
                            rowCuoSelect.Item("C3") += RowCuo_det.Item("C3")
                            rowCuoSelect.Item("C4") += RowCuo_det.Item("C4")
                            rowCuoSelect.Item("C5") += RowCuo_det.Item("C5")
                            rowCuoSelect.Item("C6") += RowCuo_det.Item("C6")
                            rowCuoSelect.Item("C7") += RowCuo_det.Item("C7")
                            rowCuoSelect.Item("C8") += RowCuo_det.Item("C8")
                        End If
                        'Else
                        '    rowCuoSelect.Item("TIPO") = 2
                        'End If
                    Next
                    'If rowCuoSelect.Item("TIPO") = 1 Then
                    'Control del monto de los D y C
                    Dim Dtotal As Double = Math.Round(rowCuoSelect.Item("D1") + rowCuoSelect.Item("D2") + rowCuoSelect.Item("D3"), 2)
                    Dim Ctotal As Double = Math.Round(rowCuoSelect.Item("C1") + rowCuoSelect.Item("C2") + rowCuoSelect.Item("C3") +
                        rowCuoSelect.Item("C4") + rowCuoSelect.Item("C5") + rowCuoSelect.Item("C6") +
                        rowCuoSelect.Item("C7") + rowCuoSelect.Item("C8"), 2)

                    If (Dtotal <> Ctotal) Then
                        Do While (rowCuoSelect.Item("total") <> Dtotal) Or (rowCuoSelect.Item("total") <> Ctotal)
                            If Dtotal <> rowCuoSelect.Item("total") Then
                                If Dtotal > rowCuoSelect.Item("total") Then
                                    rowCuoSelect.Item("D3") = rowCuoSelect.Item("D3") - 0.01
                                Else
                                    rowCuoSelect.Item("D3") = rowCuoSelect.Item("D3") + 0.01
                                End If
                            ElseIf Ctotal <> rowCuoSelect.Item("total") Then
                                If Ctotal > rowCuoSelect.Item("total") Then
                                    rowCuoSelect.Item("C8") = rowCuoSelect.Item("C8") - 0.01
                                Else
                                    rowCuoSelect.Item("C8") = rowCuoSelect.Item("C8") + 0.01
                                End If
                            End If
                            Dtotal = Math.Round(rowCuoSelect.Item("D1") + rowCuoSelect.Item("D2") + rowCuoSelect.Item("D3"), 2)
                            Ctotal = Math.Round(rowCuoSelect.Item("C1") + rowCuoSelect.Item("C2") + rowCuoSelect.Item("C3") +
                                                rowCuoSelect.Item("C4") + rowCuoSelect.Item("C5") + rowCuoSelect.Item("C6") +
                                                rowCuoSelect.Item("C7") + rowCuoSelect.Item("C8"), 2)
                        Loop
                        'Fin control de montos D y C
                    End If
                    'End If
                    If rowCuoSelect.Item("total") > 0 Then
                        Dim nItMor As Integer = 4
                        If UltraTextEditor1.Text = "MORMAN" Then
                            nItMor = 5
                        End If

                        For nIt As Integer = rowDesti.Cells("item").Value - 1 To DSDetalles.Tables(0).Rows.Count - 1
                            If DSDetalles.Tables(0).Rows(nIt).Item("pto_tipmov") = "D" Then
                                DSDetalles.Tables(0).Rows(nIt).Item("Debe") = rowCuoSelect.Item(nItMor)
                            Else
                                DSDetalles.Tables(0).Rows(nIt).Item("haber") = rowCuoSelect.Item(nItMor)
                            End If
                            nItMor += 1
                        Next
                        'SumoTotales()
                    Else
                        MessageBox.Show("Para cobrar tiene 2 opciones:" & vbNewLine & vbNewLine &
                                        "- Ingresar en Total:$ (monto a cobrar) y presionar Enter." & vbNewLine &
                                        "Y luego click en el botón Confirmar." & vbNewLine & vbNewLine &
                                        "- Seleccionar las cuotas." & vbNewLine &
                                        "Y luego click en el botón Confirmar", "Atención",
                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        'BuscoCuotas(rowDesti) Si cerro y tiene proceso lo busco sino limpio el form
                        If String.IsNullOrWhiteSpace(UltraTextEditor1.Text.ToString) Then
                            LimpiarProceso()
                        Else
                            BuscaProcesoIngresado()
                        End If
                    End If
                End If
            Else
                DTCuotasServicio = frmCuotas.GetCuotasCanceladas
                ' agrego columna de item
                Dim newCol As DataColumn
                newCol = New DataColumn("item", Type.GetType("System.Int32"))
                newCol.DefaultValue = rowDesti.Cells("item").Value - 1
                DTCuotasServicio.Columns.Add(newCol)
            End If

            SumoTotales()
        Else
            If UltraTextEditor1.Text = "01RCMO" Or UltraTextEditor1.Text = "01DCMO" Or UltraTextEditor1.Text = "MORMAN" Then
                DTCuotasMoratoria = frmCuotas.GetCuotasCerroFormulario
                If (Not IsNothing(DTCuotasMoratoria)) AndAlso (DTCuotasMoratoria.Rows.Count() > 0) Then
                    MessageBox.Show("Para cobrar tiene 2 opciones:" & vbNewLine & vbNewLine &
                                        "- Ingresar en Total:$ (monto a cobrar) y presionar Enter." & vbNewLine &
                                        "Y luego click en el botón Confirmar." & vbNewLine & vbNewLine &
                                        "- Seleccionar las cuotas." & vbNewLine &
                                        "Y luego click en el botón Confirmar", "Atención",
                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    'BuscoCuotas(rowDesti) Si cerro y tiene proceso lo busco sino limpio el form
                    If String.IsNullOrWhiteSpace(UltraTextEditor1.Text.ToString) Then
                        LimpiarProceso()
                    Else
                        BuscaProcesoIngresado()
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub BuscoCuotasFormDialogResultCtaCte(ByRef frmCuotas As FrmCuotasImpagas, ByVal nroCuenta As String)
        If frmCuotas.DialogResult = DialogResult.OK Then
            Dim procesar = False
            Dim procesoCancela = Nothing
            Dim procesoCuenta = Nothing
            Dim procesoContador = 0
            Dim DTCuotasCanceladas As New DataTable
            DTCuotasCanceladas = frmCuotas.GetCuotasCanceladas
            For Each RowCuo As DataRow In DTCuotasCanceladas.Rows
                If RowCuo.Item("Select") Then
                    If procesoContador = 0 Then
                        procesoCancela = RowCuo.Item("ProCancel")
                        procesoCuenta = RowCuo.Item("Cuenta")
                        procesoContador = 1
                        procesar = True
                    ElseIf procesoCancela <> RowCuo.Item("ProCancel") Then
                        procesar = False
                    End If
                End If
            Next
            If procesar Then
                UltraTextEditor1.Text = procesoCancela
                BuscaProcesoIngresado(TBMatricula.Text)
                cargoMatriculado(0, procesoCuenta, False)

                For Each row As UltraGridRow In UGDetalles.Rows
                    If row.Cells("Cuenta").Value = nroCuenta Then
                        If row.ChildBands(1).Rows.Count > 0 Then
                            'Hasta aca llega entra solo si tiene un hijo con campos para matricula ahora falta comprar si matricula esta vacio
                            If row.ChildBands(1).Rows(0).Cells("Matricula").Value <> "" Then
                                BuscoCuotasFormDialogResult(frmCuotas, row.ChildBands(1).Rows(0))
                            End If
                        End If
                    End If
                Next
            Else
                MessageBox.Show("Solo puede seleccionar cuotas del mismo proceso.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            End If
        End If
    End Sub
    Private Sub UltraTextEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UltraTextEditor1.EditorButtonClick
        If e.Button.Key = "Right" Then
            If IsNumeric(UltraTextEditor1.Value) Then
                ImportarAsiento()
            Else
                BuscaProcesoIngresado()
            End If
        Else
            LimpiarProceso()
        End If
    End Sub

    Private Sub AgregarDetalleVacio(Optional ByVal cCuenta As Integer = 0)
        Dim nR As Integer = DSDetalles.Tables(0).Rows.Count
        DSDetalles.Tables(0).Rows.Add()
        DSDetalles.Tables(0).Rows(nR).Item("pto_item") = nR + 1
        DSDetalles.Tables(0).Rows(nR).Item("Cuenta") = cCuenta
        DSDetalles.Tables(0).Rows(nR).Item("SubCuenta") = ""
        DSDetalles.Tables(0).Rows(nR).Item("Descripcion") = ""
        DSDetalles.Tables(0).Rows(nR).Item("pla_servicio") = "No"
        DSDetalles.Tables(0).Rows(nR).Item("Matricula") = ""
        DSDetalles.Tables(0).Rows(nR).Item("pto_tipmov") = "I"
        DSDetalles.Tables(0).Rows(nR).Item("pto_valor") = 0
        DSDetalles.Tables(0).Rows(nR).Item("pto_formula") = ""
        DSDetalles.Tables(0).Rows(nR).Item("Haber") = Math.Round(0, 2)
        DSDetalles.Tables(0).Rows(nR).Item("Debe") = Math.Round(0, 2)


    End Sub

    Private Sub MuestraDetalles(Optional ByVal matricula As String = Nothing)
        'pro_destinatario=No:(NOENTRA)Permite solo matriculado. pro_destinatario=Si(ENTRA)permite destinatario externo. pro_destinatario=SN(ENTRA) permite ambos.
        If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_destinatario") = "Si" Or DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_destinatario") = "SN" Then
            TxtDestinatarioExterno.Show()
            LabelDestinatarioExterno.Show()
            permiteDestinatarioExterno = True
        ElseIf DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_destinatario") = "No" Then
            TxtDestinatarioExterno.Hide()
            LabelDestinatarioExterno.Hide()
            permiteDestinatarioExterno = False
        Else
            TxtDestinatarioExterno.Hide()
            LabelDestinatarioExterno.Hide()
            permiteDestinatarioAmbos = True
        End If

        DAglobalMysql = cnn.consultaBDadapter(
            "(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)",
            "pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio,pla_cuotas,pla_busca_matricula",
            "pto_codpro='" & UltraTextEditor1.Text & "' order by pto_item"
        )
        DSDetalles = New DataSet
        DAglobalMysql.Fill(DSDetalles, "detalles")
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        newCol.DefaultValue = UltraTextEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))

        newCol.DefaultValue = UltraComboEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("delegacion", Type.GetType("System.Int32"))
        newCol.DefaultValue = UCEdelegacion.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = False
        DSDetalles.Tables("detalles").Columns.Add(newCol)

        'si es manual agrego fila
        If DSDetalles.Tables(0).Rows.Count = 0 Then
            AgregarDetalleVacio()
            AgregarDetalleVacio()
            'Focus en fecha
            UltraDateTimeEditor1.Focus()
        End If

        DTSubCuenta = New DataTable("subcuenta")

        With DTSubCuenta
            .Columns.Add("proceso", Type.GetType("System.String"))
            .Columns.Add("item", Type.GetType("System.Int32"))
            .Columns.Add("Destinatario", Type.GetType("System.String"))
            .Columns.Add("Matricula", Type.GetType("System.String"))
            .Columns.Add("Buscar", Type.GetType("System.String"))
            .Columns.Add("Eliminar", Type.GetType("System.String"))
            .Columns.Add("Cuotas", Type.GetType("System.String"))
            .Columns.Add("TipoDoc", Type.GetType("System.String"))
            .Columns.Add("NroDoc", Type.GetType("System.Int32"))
            .Columns.Add("BuscaMatricula", Type.GetType("System.String"))
        End With

        For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
            If rowItem.Item("pla_servicio") = "Si" Then
                AgregarSubcuenta(rowItem, matricula)
            End If
        Next
        DSDetalles.Tables.Add(DTSubCuenta)

        DTCheques = New DataTable("cheques")
        With DTCheques
            .Columns.Add("proceso", Type.GetType("System.String"))
            .Columns.Add("item", Type.GetType("System.Int32"))
            .Columns.Add("Banco", Type.GetType("System.String"))
            newCol = New DataColumn("Cheque", Type.GetType("System.String"))
            newCol.DefaultValue = 0
            .Columns.Add(newCol)
            newCol = New DataColumn("FechaEmision", Type.GetType("System.String"))
            newCol.DefaultValue = Now.Date
            .Columns.Add(newCol)
            newCol = New DataColumn("FechaDiferida", Type.GetType("System.String"))
            ' newCol.DefaultValue = ""
            .Columns.Add(newCol)
            .Columns.Add("Monto", Type.GetType("System.Double"))
            .Columns.Add("Agregar", Type.GetType("System.String"))
            .Columns.Add("Eliminar", Type.GetType("System.String"))
        End With
        'los cheques por ahora no los cargo aca, se cargan en el proceso de asignacion
        For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
            If Mid(rowItem.Item("Subcuenta"), 1, 3) = "BCO" Then ' And rowItem.Item("pto_tipmov") = "D" Then
                If Not globalEsRecibo Then
                    'Si no es recibo agrego siempre la fila de cheque
                    AgregarCheques(rowItem)
                ElseIf rowItem.Item("Cuenta") = 11010102 Then
                    'Si es recibo y la cuenta es Cheques agrego la linea de cheque sino NO
                    AgregarCheques(rowItem)
                End If
            End If
        Next
        DSDetalles.Tables.Add(DTCheques)
        'reinicio cuotas
        DTCuotasServicio = New DataTable

        Dim DRDetalles As New DataRelation("RelacionCheques", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("cheques").Columns("item"))
        DSDetalles.Relations.Add(DRDetalles)
        DRDetalles = New DataRelation("RelacionSubcuenta", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("subcuenta").Columns("item"))
        DSDetalles.Relations.Add(DRDetalles)

        BSDetalles = New BindingSource
        BSDetalles.DataSource = DSDetalles
        UGDetalles.DataSource = BSDetalles
        With UGDetalles.DisplayLayout
            With .Bands(0)
                .Columns(0).Width = 90
                '.Columns(0).CellActivation = Activation.NoEdit
                .Columns(1).Width = 400
                .Columns(1).CellActivation = Activation.NoEdit
                .Columns(2).Width = 130
                .Columns(2).CellAppearance.TextHAlign = HAlign.Right
                '.Columns(2).CellActivation = Activation.NoEdit
                .Columns(2).Format = "c"
                .Columns(2).PromptChar = ""
                .Columns(2).Style = ColumnStyle.Double
                .Columns(2).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(3).Width = 130
                .Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .Columns(3).Format = "c"
                .Columns(3).PromptChar = ""
                .Columns(3).Style = ColumnStyle.Double
                .Columns(3).FormatInfo = Globalization.CultureInfo.CurrentCulture
                '.Columns(3).CellActivation = Activation.NoEdit
                .Columns(4).Hidden = True
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
                .Columns(9).Hidden = True
                .Columns(10).Hidden = True
                .Columns(11).Hidden = True
                .Columns(12).Hidden = True
                .Columns(13).Hidden = True
                .Columns(14).Hidden = True
                .Columns(15).Hidden = True
                .Columns(16).Hidden = True
                .Columns(17).Hidden = True
                .Columns(18).Hidden = True
                .Columns(19).Hidden = True
            End With

            With .Bands(1)
                'With .Header.Appearance
                '.BackColor = Color.Aqua
                'End With
                .Columns(0).Hidden = True
                .Columns(1).Hidden = True
                .Columns(2).Width = 200
                .Columns(2).ValueList = listadoDeBancos
                .Columns(2).CellActivation = Activation.AllowEdit
                .Columns(3).Width = 100
                .Columns(3).PromptChar = ""
                .Columns(3).Style = ColumnStyle.Integer
                .Columns(3).CellActivation = Activation.AllowEdit
                .Columns(4).Width = 100
                .Columns(4).PromptChar = ""
                .Columns(4).Style = ColumnStyle.Date
                .Columns(4).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(4).CellActivation = Activation.AllowEdit
                .Columns(4).Hidden = True
                .Columns(5).Width = 100
                .Columns(5).PromptChar = ""
                .Columns(5).Style = ColumnStyle.Date
                .Columns(5).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(5).CellActivation = Activation.AllowEdit
                .Columns(6).Width = 100
                .Columns(6).CellAppearance.TextHAlign = HAlign.Right
                .Columns(6).Format = "c"
                .Columns(6).PromptChar = ""
                .Columns(6).Style = ColumnStyle.Double
                .Columns(6).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(6).CellActivation = Activation.AllowEdit
                Dim column As UltraGridColumn = UGDetalles.DisplayLayout.Bands(1).Columns("Agregar")

                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50

                column = UGDetalles.DisplayLayout.Bands(1).Columns("Eliminar")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Agregar").Header.Caption = ""
                .Columns("Agregar").Style = ColumnStyle.Button
                .Columns("Agregar").CellButtonAppearance.Image = ImageList1.Images(0)
                .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center
                .Columns("Eliminar").Header.Caption = ""
                .Columns("Eliminar").Style = ColumnStyle.Button
                .Columns("Eliminar").CellButtonAppearance.Image = ImageList1.Images(2)
                .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

                .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(1).Header.Appearance.ForeColor = Color.Black
                .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(1).Header.Appearance.BackColor2 = Color.White
                .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(2).Header.Appearance.ForeColor = Color.Black
                .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(2).Header.Appearance.BackColor2 = Color.White
                .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(3).Header.Appearance.ForeColor = Color.Black
                .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(3).Header.Appearance.BackColor2 = Color.White
                .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(4).Header.Appearance.ForeColor = Color.Black
                .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(4).Header.Appearance.BackColor2 = Color.White
                .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(5).Header.Appearance.ForeColor = Color.Black
                .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(5).Header.Appearance.BackColor2 = Color.White
                .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(6).Header.Appearance.ForeColor = Color.Black
                .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(6).Header.Appearance.BackColor2 = Color.White
                .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(7).Header.Appearance.ForeColor = Color.Black
                .Columns(7).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(7).Header.Appearance.BackColor2 = Color.White
                .Columns(8).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(8).Header.Appearance.ForeColor = Color.Black
                .Columns(8).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(8).Header.Appearance.BackColor2 = Color.White
            End With

            With .Bands(2)
                .Columns(0).Hidden = True
                .Columns(1).Hidden = True
                .Columns(2).Width = 300
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(3).Width = 100
                '.Columns(3).CellActivation = Activation.NoEdit
                .Columns(3).Header.Caption = "Matricula"
                Dim column As UltraGridColumn = UGDetalles.DisplayLayout.Bands(2).Columns("Buscar")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50

                .Columns("Buscar").Header.Caption = ""
                .Columns("Buscar").Style = ColumnStyle.Button
                .Columns("Buscar").CellButtonAppearance.Image = ImageList1.Images(1)
                .Columns("Buscar").CellButtonAppearance.ImageHAlign = HAlign.Center
                column = UGDetalles.DisplayLayout.Bands(2).Columns("Eliminar")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Eliminar").Header.Caption = ""
                .Columns("Eliminar").Style = ColumnStyle.Button
                .Columns("Eliminar").CellButtonAppearance.Image = ImageList1.Images(2)
                .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

                column = UGDetalles.DisplayLayout.Bands(2).Columns("Cuotas")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Cuotas").Header.Caption = ""
                .Columns("Cuotas").Style = ColumnStyle.Button
                .Columns("Cuotas").CellButtonAppearance.Image = ImageList1.Images(3)
                .Columns("Cuotas").CellButtonAppearance.ImageHAlign = HAlign.Center

                .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(1).Header.Appearance.ForeColor = Color.Black
                .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(1).Header.Appearance.BackColor2 = Color.White
                .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(2).Header.Appearance.ForeColor = Color.Black
                .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(2).Header.Appearance.BackColor2 = Color.White
                .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(3).Header.Appearance.ForeColor = Color.Black
                .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(3).Header.Appearance.BackColor2 = Color.White
                .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(4).Header.Appearance.ForeColor = Color.Black
                .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(4).Header.Appearance.BackColor2 = Color.White
                .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(5).Header.Appearance.ForeColor = Color.Black
                .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(5).Header.Appearance.BackColor2 = Color.White
                .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(6).Header.Appearance.ForeColor = Color.Black
                .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(6).Header.Appearance.BackColor2 = Color.White
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
                .Columns(9).Hidden = True
            End With
        End With
    End Sub

    Private Sub UGDetalles_BeforeRowsDeleted(ByVal sender As Object, ByVal e As BeforeRowsDeletedEventArgs) Handles UGDetalles.BeforeRowsDeleted
        ' para que no pregunte cuando borro una fila
        e.DisplayPromptMsg = False
    End Sub
    'Aqui es el formulario central del proceso asiento
    Private Sub UGDetalles_ClickCellButton(ByVal sender As Object, ByVal e As CellEventArgs) Handles UGDetalles.ClickCellButton
        ClickActiveCell()
    End Sub
    Private Sub ClickActiveCell()
        Dim e As UltraGridCell = UGDetalles.ActiveCell
        If e.Band.Index = 1 Then ' carga cheques
            If e.Column.Index = 7 Then 'Agregar cheque
                'If MessageBox.Show("    Confirmar ", "Agregar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                If Reporte.Trim = "" Then
                    If CantCheques <= 5 Then
                        AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Row.Cells("item").Value - 1))
                        CantCheques += 1
                    End If
                Else
                    AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Row.Cells("item").Value - 1))
                End If
                SumoTotales()
                'End If
            Else ' eliminar cheque
                If MessageBox.Show("    Confirmar ", "Eliminar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    If Reporte.Trim = "" Then
                        If CantCheques > 0 Then
                            EliminarCheques(DSDetalles.Tables("detalles").Rows(e.Row.Cells("item").Value - 1))
                            CantCheques -= 1
                        End If
                    Else
                        EliminarCheques(DSDetalles.Tables("detalles").Rows(e.Row.Cells("item").Value - 1))
                    End If
                    SumoTotales()
                End If
            End If
        Else ' buscar destinatario
            Select Case e.Column.Index
                Case 4
                    MuestraDestinatario(False)
                    FormBuscoCuotas()
                Case 5
                    MuestraDestinatario(True) 'limpia el destinatario
                    FormBuscoCuotas()
                Case 6
                    BuscoCuotas(UGDetalles.ActiveRow)
            End Select
        End If
    End Sub
    'Aqui es el formulario lateral de las deudas
    Private Sub DGVCuentaCte_ClickCellButton(sender As Object, e As CellEventArgs) Handles DGVCuentaCte.ClickCellButton
        ClickActiveCellCuentaCte()
    End Sub
    Private Sub ClickActiveCellCuentaCte()
        Dim e As UltraGridCell = DGVCuentaCte.ActiveCell
        Dim row As UltraGridRow = DGVCuentaCte.ActiveRow
        Select Case e.Column.Index
            Case 7
                CargaCuentaCte(c_TipoDoc, n_Nrodoc, True, row.Cells(0).Value)
        End Select
    End Sub
    Private Sub AgregarCheques(ByRef RowSubItem As DataRow)
        Dim mydr As DataRow
        mydr = DTCheques.NewRow()
        mydr("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("Agregar") = ""
        mydr("FechaDiferida") = Format(Now, "dd-MM-yyyy")
        DTCheques.Rows.Add(mydr)
    End Sub

    Private Sub EliminarCheques(ByRef RowSubItem As DataRow)
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        With row
            'DSDetalles.Tables("Detalles").Rows(RowSubItem.Item("pto_item") - 1).Item("Debe") -= UGDetalles.ActiveRow.Cells("monto").Value
            .Delete()
        End With
        Dim nCantChe As Integer = 0
        If DTCheques.Rows.Count > 0 Then
            For Each rowDelche As DataRow In DTCheques.Rows
                If rowDelche.Item("item") = RowSubItem.Item("pto_item") Then
                    nCantChe += 1
                    'Else
                    'DTCheques.Rows.Remove(rowDelche)
                    'rowDelche.Item("Monto") = 0
                End If
            Next
        Else
            DSDetalles.Tables("Detalles").Rows(RowSubItem.Item("pto_item") - 1).Item("Haber") = 0
            DSDetalles.Tables("Detalles").Rows(RowSubItem.Item("pto_item") - 1).Item("Debe") = 0
        End If
        If nCantChe = 0 Then
            AgregarCheques(RowSubItem)
        End If
        calculoMontoCheques()
    End Sub

    Private Sub AgregarSubcuenta(ByRef RowSubItem As DataRow, Optional ByVal matricula As String = Nothing)
        Dim mydr As DataRow
        mydr = DTSubCuenta.NewRow()
        mydr.Item("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("nrodoc") = 0
        If Not IsNothing(matricula) Then
            mydr("Matricula") = matricula
        End If
        mydr("BuscaMatricula") = RowSubItem.Item("pla_busca_matricula")
        DTSubCuenta.Rows.Add(mydr)
    End Sub

    Private Sub EliminarSubcuenta(ByRef item As Integer)
        Try
            DTSubCuenta.Rows(item).Delete() ' si da error porque no hay nada en la posicion que se quiere borrar
        Catch ex As Exception
        End Try
    End Sub

    'Private Sub UGDetalles_DoubleClickCell(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs) Handles UGDetalles.DoubleClickCell
    '    If UGDetalles.ActiveCell.IsInEditMode Then
    '        DesActivarEdision()
    '    Else
    '        ActivarEdision()
    '    End If
    'End Sub

    Private Sub UGDetalles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGDetalles.InitializeRow
        Dim cell As UltraGridCell
        Dim cellNoEdit As UltraGridCell
        ' UGDetalles.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns
        UGDetalles.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free

        If e.Row.Band.Index = 0 Then
            e.Row.ExpandAll()
            If Not IsDBNull(e.Row.Cells("pto_tipmov").Value) Then
                If e.Row.Cells("pto_tipmov").Value = "I" Then
                    'permito editar el numero de cuenta porque es MANUAL
                    e.Row.Cells("Cuenta").Activation = Activation.AllowEdit

                    e.Row.Cells("Haber").Appearance.BackColor = Color.Red
                    e.Row.Cells("Haber").Appearance.ForeColor = Color.Black
                    'e.Row.Cells("Haber").Activated = True
                    e.Row.Cells("Haber").Activation = Activation.AllowEdit
                    e.Row.Cells("Debe").Appearance.BackColor = Color.Red
                    e.Row.Cells("Debe").Appearance.ForeColor = Color.Black
                    'e.Row.Cells("Debe").Activated = True
                    e.Row.Cells("Debe").Activation = Activation.AllowEdit
                Else
                    If e.Row.Cells("pto_tipmov").Value = "H" Then
                        cell = e.Row.Cells("Haber")
                        cellNoEdit = e.Row.Cells("Debe")
                    Else
                        cell = e.Row.Cells("Debe")
                        cellNoEdit = e.Row.Cells("Haber")
                    End If
                    cell.Appearance.BackColor = Color.Red
                    cell.Appearance.ForeColor = Color.White
                    cell.ActiveAppearance.ForeColor = Color.White
                    'cell.Activated = True
                    cell.Activation = Activation.AllowEdit
                    cellNoEdit.Activation = Activation.NoEdit
                    e.Row.Cells("Cuenta").Activation = Activation.NoEdit
                End If
            End If
        ElseIf e.Row.Band.Index = 2 Then 'Fila para el matriculado
            e.Row.ExpandAll()
            If Not IsDBNull(e.Row.Cells("BuscaMatricula").Value) Then
                If e.Row.Cells("BuscaMatricula").Value = "No" Then
                    e.Row.Cells("Destinatario").Activation = Activation.NoEdit
                    e.Row.Cells("Destinatario").Appearance.BackColor = Color.Red
                    e.Row.Cells("Destinatario").Appearance.ForeColor = Color.White
                    e.Row.Cells("Destinatario").ActiveAppearance.ForeColor = Color.White
                    e.Row.Cells("Matricula").Activation = Activation.NoEdit
                    e.Row.Cells("Matricula").Appearance.BackColor = Color.Red
                    e.Row.Cells("Matricula").Appearance.ForeColor = Color.White
                    e.Row.Cells("Matricula").ActiveAppearance.ForeColor = Color.White
                    e.Row.Cells("Buscar").Hidden = True
                    e.Row.Cells("Eliminar").Hidden = True
                    e.Row.Cells("Cuotas").Hidden = True
                End If
            End If
        End If
    End Sub

    Private Sub UGDetalles_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UGDetalles.KeyDown
        If e.KeyData = Keys.Insert Then
            AgregarDetalleVacio()
        End If
        CombinacionTeclasKeyDown(e)
    End Sub

    Private Sub UGDetalles_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UGDetalles.KeyPress
        Try
            Select Case e.KeyChar
                Case ChrW(Keys.Enter)
                    If UGDetalles.ActiveCell Is Nothing Then
                        UGDetalles.PerformAction(UltraGridAction.ActivateCell)
                    End If
                    If UGDetalles.ActiveCell.IsInEditMode Then
                        PostEdicion(e)
                        'DesActivarEdision(e)
                        'Si estoy cargando la cuenta
                        CargarCuentaManual()
                        SumoTotales()
                        Tabular(e)
                    ElseIf UGDetalles.ActiveCell.Column.ToString = "Cuenta" AndAlso UGDetalles.ActiveCell.Row.Cells("pto_tipmov").Value = "I" Then
                        'Entra si es manual y si esta en Cuenta para habilitar el campo
                        If UGDetalles.ActiveCell.Column.ToString = "Cuenta" Then
                            UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                        Else
                            Tabular(e)
                        End If
                    Else
                        Tabular(e)
                        'ActivarEdision(e)
                    End If
                    SumoTotales()
                Case ChrW(Keys.Tab)
                    If (UGDetalles.ActiveCell.Column.ToString = "Haber" Or UGDetalles.ActiveCell.Column.ToString = "Debe") AndAlso UGDetalles.ActiveCell.Row.Cells("pto_tipmov").Value = "I" Then
                        SumoTotales()
                    End If
                Case ChrW(Keys.Space)
                    ClickActiveCell()
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    'Si hace click en algun campo que pueda editar y este en modo edicion selecciona todo el contenido
    Private Sub UGDetalles_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles UGDetalles.MouseUp
        If UGDetalles.ActiveCell IsNot Nothing Then
            If UGDetalles.ActiveCell.IsInEditMode Then
                UGDetalles.ActiveCell.SelectAll()
            End If
        End If
    End Sub

    Private Sub CargarCuentaManual()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        'Dim rowItem As DataRow
        'If row.Index <> 0 Then
        '    If DSDetalles.Tables(0).Rows.Contains(UGDetalles.ActiveRow.Index) Then
        '        rowItem = DSDetalles.Tables(0).Rows(UGDetalles.ActiveRow.Index)
        '    Else
        '        rowItem = DSDetalles.Tables(0).Rows(0)
        '    End If
        'Else
        '    rowItem = DSDetalles.Tables(0).Rows(0)
        'End If
        If cell.Column.ToString = "Cuenta" Then
            Dim rowItem As DataRow = DSDetalles.Tables(0).Rows(UGDetalles.ActiveRow.Index)
            Dim FrmCuenta As New FrmPlanCuentas
            'If IsDBNull(cell.Value) Then
            If IsDBNull(cell.Text) Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
                'ElseIf cell.Value = "" Then
            ElseIf cell.Text = "" Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
            Else
                'FrmCuenta.Condicion = "pla_nropla='" & cell.Value & "' and pla_imputa='Si'"
                FrmCuenta.Condicion = "pla_nropla LIKE '" & cell.Text & "%' and pla_imputa='Si'"
            End If
            FrmCuenta.CargaPlanCuenta()
            If FrmCuenta.NumeroCuenta = "" Then
                FrmCuenta.ShowDialog()
            End If
            row.Cells("Cuenta").Value = FrmCuenta.NumeroCuenta
            row.Cells("Descripcion").Value = FrmCuenta.DescripcionCuenta
            row.Cells("Matricula").Value = FrmCuenta.NumeroSubCuenta
            row.Cells("pla_servicio").Value = FrmCuenta.Servicio
            ' por si hay modificacion de cuenta , eliminio si tiene subcuenta
            EliminarSubcuenta(UGDetalles.ActiveRow.Index)
            If row.Cells("pla_servicio").Value = "Si" Then
                AgregarSubcuenta(rowItem)
            End If
            If Mid(row.Cells("Matricula").Value, 1, 3) = "BCO" Then
                AgregarCheques(rowItem)
            End If
            FrmCuenta.Dispose()
        End If
    End Sub

    Private Sub calculoMontoCheques()
        Dim controlMonto As Boolean = True
        Dim lTieneChe As Boolean = False
        'por defecto el movimiento es Haber luego pregunto si es distinto de H set Debe
        Dim pto_tipmov As String = "Haber"
        For Each rowche As DataRow In DTCheques.Rows
            If Not IsDBNull(rowche.Item("Monto")) Then
                If controlMonto Then
                    If DSDetalles.Tables("Detalles").Rows(rowche.Item("item") - 1).Item("pto_tipmov") <> "H" Then
                        pto_tipmov = "Debe"
                    End If
                    DSDetalles.Tables("Detalles").Rows(rowche.Item("item") - 1).Item(pto_tipmov) = 0
                    controlMonto = False
                End If
                DSDetalles.Tables("Detalles").Rows(rowche.Item("item") - 1).Item(pto_tipmov) += rowche.Item("Monto")
            End If
        Next
    End Sub

    Private Sub PostEdicion(Optional ByVal e As KeyPressEventArgs = Nothing)
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Try
            Select Case cell.Band.Index
                Case 0
                    ' BUSCO SI TIENE CHEQUES PARA CONTROLAR LOS MONTOS INGRESADOS
                    Dim nMontoChe As Double = 0
                    Dim lTieneChe As Boolean = False
                    For Each rowche As DataRow In DTCheques.Rows
                        If rowche.Item("item") = row.Cells("pto_item").Value AndAlso Not IsDBNull(rowche.Item("Monto")) Then
                            nMontoChe += rowche.Item("Monto")
                            lTieneChe = True
                        End If
                    Next
                    If lTieneChe Then
                        If nMontoChe <> row.Cells("Debe").Value + row.Cells("Haber").Value Then
                            If row.Cells("pto_tipmov").Value = "H" Then
                                row.Cells("Haber").Value = nMontoChe
                            Else
                                row.Cells("Debe").Value = nMontoChe
                            End If
                            MessageBox.Show("No coincide el monto ingresado con el de los cheques", "Cheques", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If
                Case 1
                    If cell.Column.Index = 6 Then
                        If DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("pto_tipmov") = "H" Then
                            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") = 0
                            For Each rowche As DataRow In DTCheques.Rows
                                If rowche.Item("item") = row.Cells("item").Value Then
                                    DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") += rowche.Item("Monto")
                                End If
                            Next
                        Else
                            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") = 0
                            For Each rowche As DataRow In DTCheques.Rows
                                If rowche.Item("item") = row.Cells("item").Value Then
                                    DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") += rowche.Item("Monto")
                                End If
                            Next
                        End If
                    End If
                Case 2
                    MuestraDestinatario(False)
                    FormBuscoCuotas()
                Case Else

            End Select
        Catch ex As Exception
            MessageBox.Show("Controle los datos ingresados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SumoTotales()
        Dim nTotHaber As Double = 0
        Dim nTotDeber As Double = 0
        Dim nMontoFormula As Double = 0
        Dim Exprecion As String
        Dim nItem As Integer = 0
        Dim item(DSDetalles.Tables("Detalles").Rows.Count + 1) As Double
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            item(nItem) = RowTot.Item("debe") + RowTot.Item("haber")
        Next

        nItem = 0

        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            If RowTot.Item("pto_valor") <> 0 Then
                Exprecion = RowTot.Item("pto_valor")
                nMontoFormula = oSC.Eval(Exprecion)
                If RowTot.Item("pto_tipmov") = "D" Then
                    RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                Else
                    RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                End If
                item(nItem) = nMontoFormula
            Else
                If RowTot.Item("pto_formula").ToString.Trim <> "" Then
                    Exprecion = RowTot.Item("pto_formula")
                    For xItem = 1 To item.Length - 1
                        Exprecion = Exprecion.ToString.Replace("item" & Convert.ToString(Format(xItem, "00")), item(xItem))
                        Exprecion = Exprecion.ToString.Replace("Prestamo", Prestamo)
                    Next
                    Exprecion = Exprecion.ToString.Replace(",", ".")
                    nMontoFormula = oSC.Eval(Exprecion)
                    If RowTot.Item("pto_tipmov") = "D" Then
                        RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                    Else
                        RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                    End If
                    item(nItem) = nMontoFormula
                End If
            End If

            nTotDeber += Math.Round(RowTot.Item("debe"), 2)
            nTotHaber += Math.Round(RowTot.Item("haber"), 2)
        Next

        Dim nDifeRedondeo As Double = Math.Round(nTotDeber - nTotHaber, 2)
        'repaso para colocar la diferencia de redondeo
        If Math.Abs(nDifeRedondeo) < 0.09 Then
            nTotDeber = 0
            nTotHaber = 0

            'Aca controlo si es de sipres
            For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 And
                   RowTot.Item("Cuenta") = "31021300" Then
                    'If nDifeRedondeo < 0 Then
                    RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                    'ElseIf nDifeRedondeo > 0 Then
                    'RowTot.Item("haber") = RowTot.Item("haber") - nDifeRedondeo
                    'End If
                    nDifeRedondeo = 0
                End If
                nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
            Next
            'Fin controlo sipres

            If nDifeRedondeo <> 0 Then
                nTotDeber = 0
                nTotHaber = 0
                For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                    If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or
                       RowTot.Item("pto_formula").ToString.IndexOf("(") > 0 Then
                        'RowTot.Item("pto_formula").ToString.IndexOf("/") > 0 Or _
                        'RowTot.Item("pto_formula").ToString.IndexOf("+") > 0 Or _
                        If nDifeRedondeo <> 0 Then
                            'If nDifeRedondeo < 0 Then
                            If RowTot.Item("pto_tipmov") = "H" Then
                                'RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                                'nDifeRedondeo = 0
                            End If
                            '  Else
                            '      If RowTot.Item("pto_tipmov") = "D" Then
                            ' RowTot.Item("debe") = RowTot.Item("debe") - nDifeRedondeo
                            '  nDifeRedondeo = 0
                            ' End If

                            ' End If
                        End If
                    End If
                    nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                    nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
                Next
            End If

            If nDifeRedondeo <> 0 Then
                Dim rowTot As DataRow = DSDetalles.Tables("Detalles").Rows(DSDetalles.Tables("Detalles").Rows.Count - 1)
                If rowTot.Item("pto_tipmov") <> "I" Then ' SI ES UNA CUENTA INSERTADA
                    If rowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or
                       rowTot.Item("pto_formula").ToString.IndexOf("(") > 0 Then
                        If rowTot.Item("pto_tipmov") = "H" Then
                            rowTot.Item("haber") = rowTot.Item("haber") + nDifeRedondeo
                            nDifeRedondeo = 0
                        Else
                            'rowTot.Item("debe") = rowTot.Item("debe") + nDifeRedondeo
                        End If
                        'nDifeRedondeo = 0
                        nTotDeber = Math.Round((nTotDeber + rowTot.Item("debe")), 2)
                        nTotHaber = Math.Round((nTotHaber + rowTot.Item("haber")), 2)
                    End If
                End If
            End If
        End If

        TxtDebe.Text = Format(nTotDeber, "$ ###,##0.00")
        TxtHaber.Text = Format(nTotHaber, "$ ###,##0.00")
    End Sub

    Private Function ControlDatosIngresados() As Boolean
        Dim destinatarioConfirmado As String = "Confirmar asiento?"
        Dim nTotHaber As Double = 0
        Dim nTotDeber As Double = 0
        'Dim sipresTotHaber As Double = 0
        'Dim sipresTotDeber As Double = 0
        'Dim controlSipres As Boolean = False
        Dim montoCuentaCheque As Double = 0
        Dim sumoMontoCuentaCheque As Boolean = False
        Try
            'controlo montos
            For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                If RowTot.Item("debe") >= 0 And RowTot.Item("haber") >= 0 Then
                    nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                    nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
                    'If RowTot.Item("Cuenta") = "22040100" Then
                    '    controlSipres = True
                    'End If
                    'If controlSipres Then
                    '    sipresTotDeber += RowTot.Item("debe")
                    '    sipresTotHaber += RowTot.Item("haber")
                    'End If
                Else
                    ControlDatosIngresados = False
                    msgError = "Debe y/o Haber"
                    Exit Function
                End If

                If Mid(RowTot.Item("Subcuenta"), 1, 3) = "BCO" Then
                    If Not globalEsRecibo Then
                        'Si no es recibo sumo monto de cheque
                        sumoMontoCuentaCheque = True
                    ElseIf RowTot.Item("Cuenta") = 11010102 Then
                        'Si es recibo y la cuenta es Cheques sumo monto de cheque sino NO
                        sumoMontoCuentaCheque = True
                    End If
                    If sumoMontoCuentaCheque Then
                        If RowTot.Item("debe") > 0 Then
                            montoCuentaCheque += RowTot.Item("debe")
                        ElseIf RowTot.Item("haber") > 0 Then
                            montoCuentaCheque += RowTot.Item("haber")
                        End If
                    End If
                    sumoMontoCuentaCheque = False
                End If

                If RowTot.Item("Cuenta") = 0 Then
                    ControlDatosIngresados = False
                    msgError = "Debe completar los datos de la cuenta"
                    Exit Function
                End If
            Next

            'If sipresTotDeber <> sipresTotHaber Then
            '    Dim sipresDiferencia As Double = 0
            '    Dim sipresOperacion As Boolean = True 'True suma, False resta
            '    If sipresTotDeber > sipresTotHaber Then
            '        sipresDiferencia = sipresTotDeber - sipresTotHaber
            '        sipresOperacion = True 'suma
            '    ElseIf sipresTotDeber < sipresTotHaber Then
            '        sipresDiferencia = sipresTotHaber - sipresTotDeber
            '        sipresOperacion = False 'resta
            '    End If

            '    controlSipres = False
            '    sipresTotHaber = 0
            '    sipresTotDeber = 0

            '    For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            '        If RowTot.Item("Cuenta") = "31021300" Then
            '            If sipresOperacion Then
            '                RowTot.Item("haber") = RowTot.Item("haber") + sipresDiferencia
            '            Else
            '                RowTot.Item("haber") = RowTot.Item("haber") - sipresDiferencia
            '            End If
            '        End If
            '        If RowTot.Item("Cuenta") = "22040100" Then
            '            controlSipres = True
            '        End If
            '        If controlSipres Then
            '            sipresTotDeber += RowTot.Item("debe")
            '            sipresTotHaber += RowTot.Item("haber")
            '        End If
            '    Next
            'End If

            If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_destinatario") = "No" Then
                'If pro_destinatario=No:(ENTRA IF)Permite solo matriculado. Controlo destinatario
                If DSDetalles.Tables("subcuenta").Rows.Count > 0 Then
                    For Each RowTot As DataRow In DSDetalles.Tables("subcuenta").Rows
                        If IsDBNull(RowTot.Item("Destinatario")) Or IsDBNull(RowTot.Item("matricula")) Then
                            'si son DBNull entra.
                            ControlDatosIngresados = False
                            msgError = "Complete Destinatario y Matricula"
                            Exit Function
                        ElseIf String.IsNullOrEmpty(RowTot.Item("Destinatario")) Or String.IsNullOrEmpty(RowTot.Item("matricula")) Then
                            'sino si son vacios "" entra.
                            ControlDatosIngresados = False
                            msgError = "Complete Destinatario y Matricula"
                            Exit Function
                        End If
                    Next
                    destinatarioConfirmado = "(" + DSDetalles.Tables("subcuenta").Rows.Item(0).Item("matricula") + ") " +
                        DSDetalles.Tables("subcuenta").Rows.Item(0).Item("Destinatario")
                End If
            ElseIf DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_destinatario") = "Si" Then
                If DSDetalles.Tables("subcuenta").Rows.Count > 0 Then
                    'Else pro_destinatario=Si(ENTRA ELSE)permite destinatario externo
                    If String.IsNullOrEmpty(TxtDestinatarioExterno.Text) Then
                        'si es vacio "" entra.
                        ControlDatosIngresados = False
                        msgError = "Complete Destinatario Externo"
                        Exit Function
                    End If
                    destinatarioConfirmado = TxtDestinatarioExterno.Text
                End If
            End If
            'Controlo Monto de cheques
            If DSDetalles.Tables("cheques").Rows.Count > 0 Then
                For Each rowCheque As DataRow In DSDetalles.Tables("cheques").Rows
                    If Not IsDBNull(rowCheque.Item("Monto")) Then
                        If rowCheque.Item("Monto") <= 0 Or IsDBNull(rowCheque.Item("Banco")) Or (IsDBNull(rowCheque.Item("Cheque")) OrElse rowCheque.Item("Cheque") = 0) Then
                            ControlDatosIngresados = False
                            msgError = "Debe completar los datos del cheque"
                            Exit Function
                        Else
                            montoCuentaCheque -= rowCheque.Item("Monto")
                        End If
                    End If
                Next
                If montoCuentaCheque <> 0 Then
                    Dim messageAlerta As String = "Controle el monto de la cuenta cheques o bancos que concuerden los montos de cheques cargados. PRESIONE NO PARA EDITAR LOS VALORES. Presione SI en caso de ser correcto."
                    If MessageBox.Show(messageAlerta, "Alerta posible diferencia en montos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        'Yes continua
                    Else
                        ControlDatosIngresados = False
                        msgError = "noconfirma"
                        Exit Function
                    End If
                End If
            End If
        Catch ex As Exception
            ControlDatosIngresados = False
            Exit Function
        End Try
        'segundo metodo para controlar montos
        If nTotDeber > 0 And Math.Round(nTotDeber, 2) = Math.Round(nTotHaber, 2) Then
            ' si es MANUAL la fecha de vencimiento es igual a la fecha del asiento
            If UltraTextEditor1.Text = "02R120" Then
                Dim montoTrabajo As Integer = LabelMontoTrabajo.Text
                Dim montoHaber As Integer = TxtHaber.Text
                'Si el monto del trabajo es mayor a 0 e igual al haber y debe entra
                If montoTrabajo > 0 Then
                    If montoTrabajo = montoHaber Then
                        ControlDatosIngresados = True
                    Else
                        ControlDatosIngresados = False
                        msgError = "Controlar montro trabajo con debe y haber"
                        Exit Function
                    End If
                Else
                    ControlDatosIngresados = True
                End If
            Else
                ControlDatosIngresados = True
            End If
        Else
            ControlDatosIngresados = False
            msgError = "Debe y Haber"
            Exit Function
        End If
        If MessageBox.Show(destinatarioConfirmado, "Confirmar a nombre de:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            'Yes continua
        Else
            ControlDatosIngresados = False
            msgError = "noconfirma"
            Exit Function
        End If
    End Function
    Private Sub CBConfirmar_Click(sender As Object, e As EventArgs) Handles CBConfirmar.Click
        ConfirmarAsiento()
    End Sub
    Private Sub CBConfirmarImprimir_Click(sender As Object, e As EventArgs) Handles CBConfirmarImprimir.Click
        ConfirmarAsiento(True)
    End Sub
    Private Sub CombinacionTeclasKeyDown(ByVal e As KeyEventArgs)
        If e.Shift And e.KeyCode = Keys.C Then
            ConfirmarAsiento()
        ElseIf e.Shift And e.KeyCode = Keys.I Then
            ConfirmarAsiento(True)
        End If
    End Sub

    Private Sub ConfirmarAsiento(Optional ByVal imprimirDirecto As Boolean = False)
        If ControlDatosIngresados() Then
            Dim nNroAsiImprime As Long
            Dim cProcesoImprime As String = UltraTextEditor1.Text
            ' si es MANUAL la fecha de vencimiento es igual a la fecha del asiento
            If cProcesoImprime = "MANUAL" Then
                For Each rowFecVen As DataRow In DSDetalles.Tables("detalles").Rows
                    rowFecVen.Item("fecven") = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
                Next
            End If
            'Si tiene el id del trabajo seteo los valores para actualizar el estado
            If globalTrabajoId > 0 Then
                cnn.trabajoId = globalTrabajoId
                cnn.trabajoEstado = globalTrabajoEstado
            End If
            If permiteDestinatarioAmbos = False Then
                If permiteDestinatarioExterno Then
                    nNroAsiImprime = cnn.GraboAsiento(
                        DSDetalles.Tables("detalles"), DSDetalles.Tables("cheques"), DTCuotasServicio, DTCuotasMoratoria,
                        TxtConcepto1.Text, TxtConcepto2.Text, TxtConcepto3.Text, TxtDestinatarioExterno.Text, nCaja, nZeta, False)
                Else
                    nNroAsiImprime = cnn.GraboAsiento(
                        DSDetalles.Tables("detalles"), DSDetalles.Tables("cheques"), DTCuotasServicio, DTCuotasMoratoria,
                        TxtConcepto1.Text, TxtConcepto2.Text, TxtConcepto3.Text, "", nCaja, nZeta, False)
                End If
            Else
                nNroAsiImprime = cnn.GraboAsiento(
                    DSDetalles.Tables("detalles"), DSDetalles.Tables("cheques"), DTCuotasServicio, DTCuotasMoratoria,
                    TxtConcepto1.Text, TxtConcepto2.Text, TxtConcepto3.Text, "", nCaja, nZeta, False)
            End If
            'SI VINO UN CERO SE PRODUJO ALGUN ERROR Y NO GRABO ASIENTO
            If nNroAsiImprime > 0 Then
                'Reinicio los valores por default del trabajo
                LabelMontoTrabajo.Text = "0000,00"
                globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
                LimpiarProceso()
                Dim imprimir As New Impresiones
                If Reporte.Trim = "" Then
                    imprimir.ImprimirRecibo(nNroAsiImprime)
                Else
                    imprimir.ImprimirAsiento(nNroAsiImprime, cProcesoImprime, "", imprimirDirecto)
                End If

                Reporte = String.Empty
            End If
        Else
            If msgError <> "noconfirma" Then
                'Si confirma el envio de datos entra
                MessageBox.Show("Compruebe los datos ingresados. " + msgError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            End If
        End If
    End Sub

    Private Sub ActualizoFechaInstitucion()
        If Not DSDetalles Is Nothing Then
            For Each RowDet As DataRow In DSDetalles.Tables("detalles").Rows
                RowDet.Item("fecha") = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
                If Not UltraComboEditor1.Value Is Nothing Then
                    RowDet.Item("institucion") = UltraComboEditor1.Value
                End If
                If Not UCEdelegacion.Value Is Nothing Then
                    RowDet.Item("delegacion") = UCEdelegacion.Value
                End If
            Next
        End If
    End Sub

    Private Sub UltraDateTimeEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraDateTimeEditor1.KeyPress
        Tabular(e)
    End Sub

    Private Sub UltraDateTimeEditor1_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UltraDateTimeEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub

    Private Sub UltraComboEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraComboEditor1.KeyPress
        Tabular(e)
    End Sub

    Private Sub UltraComboEditor1_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UltraComboEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub

    Private Sub UltraTextEditor1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UltraTextEditor1.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscaProcesoIngresado()
        End If
    End Sub

    Private Sub BuscaProcesoIngresado(Optional ByVal matricula As String = Nothing)
        'CantCheques = 0
        globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
        MuestraProcesos()
        If UltraLabel1.Visible Then
            UltraComboEditor1.SelectedIndex = DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_instit") - 1 ' Convert.ToInt16(Mid(Me.UltraTextEditor1.Text, 1, 2)) - 1
            UCEdelegacion.SelectedIndex = nPubNroCli - 1
            If UltraTextEditor1.Text <> "" Then
                If UltraTextEditor1.Text.Substring(2, 1) = "R" Then
                    globalEsRecibo = True 'Si es Recibo pongo True
                Else
                    globalEsRecibo = False 'Si No es Recibo pongo False
                End If
                MuestraDetalles(matricula)
                SumoTotales()
            End If
        End If
    End Sub

    Private Sub FrmAsiento_Activated(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Activated
        UltraTextEditor1.Focus()
    End Sub

    Private Sub UltraTextEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraTextEditor1.KeyPress
        'Tabular(e)
        If e.KeyChar = ChrW(Keys.Enter) Then
            TxtConcepto1.Focus()
        End If
    End Sub

    Private Sub TxtConcepto1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto1.KeyPress
        Tabular(e)
    End Sub

    Private Sub TxtConcepto2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto2.KeyPress
        Tabular(e)
    End Sub

    Private Sub TxtConcepto3_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto3.KeyPress
        Tabular(e)
    End Sub

    Private Sub UCEdelegacion_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCEdelegacion.ValueChanged
        ActualizoFechaInstitucion()
    End Sub

    'Para cargar segun la matricula la cuenta corriente en el lateral derecho
    Private Sub UTEMatricula_KeyDown(sender As Object, e As KeyEventArgs) Handles UTEMatricula.KeyDown
        If e.KeyCode = Keys.Enter Then
            If String.IsNullOrWhiteSpace(UTEMatricula.Text) Then
                MessageBox.Show("Complete el campo con la matricula")
            Else
                BuscaMatriculado(UTEMatricula.Value, False)
            End If
        End If
    End Sub

    Private Sub UTEMatricula_EditorButtonClick(sender As Object, e As UltraWinEditors.EditorButtonEventArgs) Handles UTEMatricula.EditorButtonClick
        If String.IsNullOrWhiteSpace(UTEMatricula.Text) Then
            MessageBox.Show("Complete el campo con la matricula")
        Else
            BuscaMatriculado(UTEMatricula.Value, False)
        End If
    End Sub

    Private Sub CargaCuentaCte(ByVal tipDoc As String, ByVal nroDoc As Integer, Optional ByVal mostrarFormulario As Boolean = True, Optional ByVal nroCuenta As Integer = 0)
        Try
            If nroCuenta > 0 Then
                Dim frmCuotas As New FrmCuotasImpagas(cnn, UltraTextEditor1.Text, nroCuenta, "H", tipDoc, nroDoc)
                frmCuotas.Text = "Cuotas impagas"
                frmCuotas.StartPosition = FormStartPosition.Manual
                If mostrarFormulario Then
                    frmCuotas.ShowDialog()
                    BuscoCuotasFormDialogResultCtaCte(frmCuotas, nroCuenta)
                End If
            Else
                Dim tabla As String = "totales INNER JOIN plancuen ON pla_nropla=tot_nropla"
                Dim campos As String = "pla_nropla AS Cuenta, SUM(tot_debe-tot_haber) AS saldo, pla_nombre AS Servicio, tot_titulo, tot_matricula, tot_tipdoc, tot_nrodoc"
                'Dim condiciones As String = "tot_tipdoc='" & tipDoc & "' AND tot_nrodoc=" & nroDoc & " AND pla_servicio = 'Si' AND tot_estado <> '9' > 0 GROUP BY pla_nropla"
                Dim condiciones As String = "tot_tipdoc='" & tipDoc & "' AND tot_nrodoc=" & nroDoc & " AND pla_servicio = 'Si' AND MID(pla_nropla,1,1) = 1 AND tot_estado <> '9' GROUP BY pla_nropla"
                DAglobalMysql = cnn.consultaBDadapter(tabla, campos, condiciones)
                DSCuentaCte = New DataSet
                DAglobalMysql.Fill(DSCuentaCte, "servicios")
                'Recorro y elimino si no tiene saldo
                Dim cuentasArray() As String = {"13040400", "13040100", "13010200"}
                For Each row As DataRow In DSCuentaCte.Tables(0).Rows
                    If row("saldo") <= 0 Then
                        If cuentasArray.Contains(row("Cuenta")) Then
                            'row("saldo") = 0
                        Else
                            row.Delete()
                        End If
                    Else
                        Dim frmCuotas As New FrmCuotasImpagas(cnn, UltraTextEditor1.Text, row.Item("cuenta"), "H", tipDoc, nroDoc)
                        frmCuotas.Text = "Cuotas impagas"
                        frmCuotas.StartPosition = FormStartPosition.Manual
                        If mostrarFormulario Then
                            frmCuotas.ShowDialog()
                        End If
                    End If
                Next
                'Agrego boton cobrar
                With DSCuentaCte.Tables("servicios")
                    .Columns.Add("Cobrar", Type.GetType("System.String"))
                End With
                'Dim DRCuenta As New DataRelation("CuentaDetalle", DSCuentaCte.Tables("servicios").Columns("pla_nropla"), DSComproba.Tables("totales").Columns("tot_nroasi"), False)
                'DSComproba.Relations.Add(DRComprobantes)
                'DSCuentaCte.Tables(0).Columns.Add("Marcar", GetType(Boolean))
                DGVCuentaCte.DataSource = DSCuentaCte
                With DGVCuentaCte.DisplayLayout
                    With .Bands(0)
                        .Columns(0).Hidden = True
                        '.Columns(0).Width = 75
                        '.Columns(0).CellActivation = Activation.NoEdit
                        '.Columns(0).CellAppearance.TextHAlign = HAlign.Left
                        .Columns(1).Width = 85
                        .Columns(1).CellActivation = Activation.NoEdit
                        .Columns(1).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(1).Format = "c"
                        .Columns(2).Width = 200
                        .Columns(2).CellActivation = Activation.NoEdit
                        .Columns(2).CellAppearance.TextHAlign = HAlign.Left
                        .Columns(3).Hidden = True
                        .Columns(4).Hidden = True
                        '.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                        '.Columns(5).ReadOnly = False
                        .Columns(5).Hidden = True
                        .Columns(6).Hidden = True
                        '.Columns(7).Width = 50
                        'Boton Cobrar
                        .Columns("Cobrar").Header.Caption = "Cobrar"
                        .Columns("Cobrar").Style = ColumnStyle.Button
                        .Columns("Cobrar").CellAppearance.Image = ImageList1.Images(6)
                        .Columns("Cobrar").CellButtonAppearance.Image = ImageList1.Images(6)
                        .Columns("Cobrar").CellAppearance.ImageHAlign = HAlign.Center
                        .Columns("Cobrar").CellButtonAppearance.ImageHAlign = HAlign.Center
                        .Columns("Cobrar").Width = 60
                    End With
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BuscaMatriculado(ByVal matricula As String, Optional ByVal muestraFormulario As Boolean = True)
        Dim frmDesti As New FrmDestinatario
        If Mid(matricula, 1, 2) <> "" And Mid(matricula, 3) <> "" Then
            'si de matricula los 2 primeros caracteres no son null y los siguientes tampoco entra
            Dim tipoMatricula As String = Mid(matricula, 1, 2)
            Dim numeroMatricula As String = Mid(matricula, 3)
            frmDesti.Condicion = "afi_titulo='" & tipoMatricula & "' and afi_matricula=" & CInt(numeroMatricula)
        End If
        frmDesti.CargaDestinatario()
        If frmDesti.MatriculaDestinatario <> "" Then
            TBMatricula.Text = frmDesti.MatriculaDestinatario
            TBMatriculado.Text = frmDesti.NombreDestinatario
            CargaCuentaCte(frmDesti.TipoDocDestinatario, frmDesti.NroDocDestinatario, muestraFormulario)
        Else
            frmDesti.ShowDialog()
            If frmDesti.DialogResult = DialogResult.OK Then
                TBMatricula.Text = frmDesti.MatriculaDestinatario
                TBMatriculado.Text = frmDesti.NombreDestinatario
                CargaCuentaCte(frmDesti.TipoDocDestinatario, frmDesti.NroDocDestinatario, muestraFormulario)
            End If
        End If
        c_TipoDoc = frmDesti.TipoDocDestinatario
        n_Nrodoc = frmDesti.NroDocDestinatario
        frmDesti.Dispose()
    End Sub

    Private Sub UTECodigoBarra_EditorButtonClick(sender As Object, e As UltraWinEditors.EditorButtonEventArgs) Handles UTECodigoBarra.EditorButtonClick
        If e.Button.Key = "Right" Then
            BuscaCodigo(UTECodigoBarra.Text)
        Else
            LimpiarProceso()
        End If
    End Sub
    Private Sub UTECodigoBarra_KeyDown(sender As Object, e As KeyEventArgs) Handles UTECodigoBarra.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscaCodigo(UTECodigoBarra.Text)
        End If
    End Sub
    Private Sub BuscaTrabajoIngresado(ByVal valorCodigoBarra As Integer)
        DAglobalMysql = cnn.QueryTrabajoById(valorCodigoBarra)

        Dim DSTrabajo As DataSet = New DataSet()
        DAglobalMysql.Fill(DSTrabajo, "trabajo")
        If DSTrabajo.Tables("trabajo").Rows.Count() > 0 Then
			Dim rowTrabajo As DataRow = DSTrabajo.Tables("trabajo").Rows(0)

			If rowTrabajo.Item("tra_estado") = 1 Then
				UltraTextEditor1.Text = "02R120"
				BuscaProcesoIngresado(rowTrabajo.Item("tra_matricula"))
				cargoMatriculado(rowTrabajo.Item("tra_monto_deposito"), "21010100")
				'Completo demas campos
				TxtConcepto2.Text = "Comitente: " & rowTrabajo.Item("tra_comitentecuit") & " " & rowTrabajo.Item("tra_clientecomitente")
				TxtConcepto3.Text = "Trabajo: " & rowTrabajo.Item("tar_descrip") & ". " & Format(CDate(rowTrabajo.Item("tra_fecha_cierre").ToString()), "dd-MM-yyyy")
				LabelMontoTrabajo.Text = rowTrabajo.Item("tra_monto_deposito")
				'Guardo el ID del trabajo para utilizarlo luego de guardar el asiento y actualizar el estado del mismo
				globalTrabajoId = rowTrabajo.Item("id")
			Else
				Dim message As String = "PAGADO"
				If rowTrabajo.Item("tra_estado") = 20 Then
					message = "ANULADO"
				End If
				LabelMontoTrabajo.Text = "0000,00"
				globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
				MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA " & message, "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Error)
				LimpiarProceso()
			End If

		Else
            LabelMontoTrabajo.Text = "0000,00"
            globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
            MessageBox.Show("NO EXISTE EL TRABAJO NUMERO: " & valorCodigoBarra, "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            LimpiarProceso()
        End If
    End Sub

    Private Sub CargaInscripcion(ByVal matricula As String, proceso As String, total As Double)

        'proceso Devengamiento de Olimpiadas
        UltraTextEditor1.Text = proceso
        BuscaProcesoIngresado(matricula)
        LabelMontoTrabajo.Text = total
        cargoCuentasOlimpiadas(total)
    End Sub

    Private Sub BuscaCodigo(ByVal valorCodigoBarra As String)
        'Desconcatena el codigo en partes por _
        Dim ArrCodigo As String() = valorCodigoBarra.Split("_")

		If ArrCodigo(0) = "FICHA" Then
			ficha(ArrCodigo(1), ArrCodigo(2))
		ElseIf ArrCodigo(0) = "AGENDA" Then
			calendario(ArrCodigo(1))
		Else
			BuscaTrabajoIngresado(valorCodigoBarra)
        End If
    End Sub

	Private Sub ficha(ByVal tipo As Integer, ByVal id As Integer)
		Dim proceso = "02DI01"
		DAglobalMysql = cnn.QueryFichaById(id)
		Dim DSFicha As DataSet = New DataSet()
		DAglobalMysql.Fill(DSFicha, "ficha")
		If DSFicha.Tables("ficha").Rows.Count() > 0 Then
			Dim rowOlimpiada As DataRow = DSFicha.Tables("ficha").Rows(0)
			Dim matricula = rowOlimpiada.Item("afi_titulo") & rowOlimpiada.Item("afi_matricula")
			Dim total = rowOlimpiada.Item("importe_total")
			CargaInscripcion(matricula, proceso, total)
		Else
			MessageBox.Show("NO EXISTE LA INSCRIPCION: FICHA_" & id, "Devengamiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
		End If
	End Sub

	Private Sub calendario(ByVal id As Integer)

        Dim json As Dictionary(Of String, String) = rest.getCalendarioJson("agenda/", id)

        If Not IsNothing(json) Then
            TxtConcepto1.Text = json("concepto_1")
            TxtConcepto2.Text = json("concepto_2")
            TxtConcepto3.Text = json("concepto_3")
        Else
            MessageBox.Show("NO EXISTE LA AGENDA: AGENDA_" & id, "PARA EFECTUAR EL PAGO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub RellenaCampos(ByVal matricula As String)

        'proceso Devengamiento de Agenda
        BuscaProcesoIngresado(matricula)
    End Sub

    Private Sub cargoMatriculado(ByVal montoDeposito As Double, ByVal nroCuenta As String, Optional ByVal muestraFormBuscoCuotas As Boolean = True)
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = nroCuenta Then
                If row.ChildBands(1).Rows.Count > 0 Then
                    'Hasta aca llega entra solo si tiene un hijo con campos para matricula ahora falta comprar si matricula esta vacio
                    If row.ChildBands(1).Rows(0).Cells("Matricula").Value <> "" Then
                        'Busco el matriculado y completo campo
                        MuestraDestinatario(False, row.ChildBands(1).Rows(0))
                        If muestraFormBuscoCuotas Then
                            FormBuscoCuotas(row.ChildBands(1).Rows(0))
                        End If
                        'Completo el monto haber
                        row.Cells("Haber").Value = montoDeposito
                        Exit For
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub cargoCuentasOlimpiadas(ByVal montoDeposito As Double)
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = "13050500" Then
                If row.ChildBands(1).Rows.Count > 0 Then
                    'Hasta aca llega entra solo si tiene un hijo con campos para matricula ahora falta comprar si matricula esta vacio
                    If row.ChildBands(1).Rows(0).Cells("Matricula").Value <> "" Then
                        'Busco el matriculado y completo campo
                        MuestraDestinatario(False, row.ChildBands(1).Rows(0))
                        FormBuscoCuotas(row.ChildBands(1).Rows(0))
                        'Completo el monto haber
                        row.Cells("Haber").Value = montoDeposito
                        Exit For
                    End If
                End If
            End If
            If row.Cells("Cuenta").Value = "13031100" Then
                row.Cells("Debe").Value = montoDeposito
            End If
        Next
    End Sub

    Private Sub BMCEfectivo_Click(sender As Object, e As EventArgs) Handles BMCEfectivo.Click
        'Seteo en 0 cheque
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = "11010102" Then
                row.Cells("Debe").Value = 0
                Exit For
            End If
        Next
        'Completo el monto debe en efectivo
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = "11010101" Then
                row.Cells("Debe").Value = ULMontoCobrar.Text
                Exit For
            End If
        Next
        SumoTotales()
    End Sub

    Private Sub BMCCheque_Click(sender As Object, e As EventArgs) Handles BMCCheque.Click
        'Seteo en 0 efectivo
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = "11010101" Then
                row.Cells("Debe").Value = 0
                Exit For
            End If
        Next
        'Completo el monto debe en cheque
        For Each row As UltraGridRow In UGDetalles.Rows
            If row.Cells("Cuenta").Value = "11010102" Then
                row.Cells("Debe").Value = ULMontoCobrar.Text
                Exit For
            End If
        Next
        SumoTotales()
    End Sub
End Class