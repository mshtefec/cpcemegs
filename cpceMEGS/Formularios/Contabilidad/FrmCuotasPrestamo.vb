﻿Imports MySql.Data.MySqlClient
Public Class FrmCuotasPrestamo
    Private cnn As New ConsultaBD(True)
    Private DTCuotas As DataTable
    Private DTMonto As DataTable
    Private DAPrestamo As MySqlDataAdapter
    Private n_Prestamo As Double
    Private n_ImportePrestamo As Double
    Private n_CuotasPrestamo As Integer
    Private c_Vencimiento As String


    Public ReadOnly Property Vencimiento() As String
        Get
            Return Me.c_Vencimiento
        End Get
    End Property

    Public ReadOnly Property Prestamo() As Double
        Get
            Return Me.n_Prestamo
        End Get
    End Property

    Public ReadOnly Property ImportePrestamo() As Double
        Get
            Return Me.n_ImportePrestamo
        End Get
    End Property

    Public ReadOnly Property CuotasPrestamo() As Double
        Get
            Return Me.n_CuotasPrestamo
        End Get
    End Property

    Private Sub CargoPrestamo()
        Try
            DAPrestamo = cnn.consultaBDadapter("prestamos", "pre_importe as Importe", "pre_tipafi=1 group by pre_importe")
            DTMonto = New DataTable
            DAPrestamo.Fill(DTMonto)
            UCboMonto.DataSource = DTMonto
            DAPrestamo = cnn.consultaBDadapter("prestamos", "pre_cuota as Cuotas", "pre_tipafi=1 group by pre_cuota")
            DTCuotas = New DataTable
            DAPrestamo.Fill(DTCuotas)
            UCboCuotas.DataSource = DTCuotas
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
       
    End Sub

    Private Sub CalculoCuotas()
        If Not UCboMonto.Value Is Nothing And Not UCboCuotas.Value Is Nothing Then


            DAPrestamo = cnn.consultaBDadapter("prestamos", "pre_impcuota as Importe", "pre_tipafi=1 and pre_importe=" & UCboMonto.Value & " and pre_cuota=" & UCboCuotas.Value)
            DTMonto = New DataTable
            DAPrestamo.Fill(DTMonto)
            If DTMonto.Rows.Count > 0 Then
                UCEimporteCuota.Value = DTMonto.Rows(0).Item("importe")
            Else
                UCEimporteCuota.Value = 0
            End If

        End If
    End Sub

    Private Sub FrmCuotasPrestamo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CargoPrestamo()
    End Sub

    Private Sub UCboCuotas_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCboCuotas.ValueChanged
        CalculoCuotas()
    End Sub

    Private Sub UCboMonto_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCboMonto.ValueChanged
        CalculoCuotas()
    End Sub

    Private Sub UCEimporteCuota_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UCEimporteCuota.EditorButtonClick
        n_Prestamo = UCboMonto.Value
        n_CuotasPrestamo = UCboCuotas.Value
        n_ImportePrestamo = UCboCuotas.Value * UCEimporteCuota.Value
        c_Vencimiento = UDTEVence.Value
        DialogResult = DialogResult.OK
    End Sub
End Class