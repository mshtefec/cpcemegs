﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class FrmAsientoMatricula
    Private cnn As New ConsultaBD(True)
    Private DSProcesos As DataSet
    Private DAProcesos As MySqlDataAdapter
    Private BSProcesos As BindingSource
    Private DSDetalles As DataSet
    Private DADetalles As MySqlDataAdapter
    Private BSDetalles As BindingSource
    Private DTSubCuenta As DataTable
    Private DTCheques As DataTable
    Private DTAsiImportado As DataTable
    Private DTCuotasServicio As New DataTable
    Private DTCuotasMoratoria As New DataTable
    Private nCaja As Integer
    Private nZeta As Integer
    Private Prestamo As Double = 0
    Private permiteDestinatarioAmbos As Boolean = False
    Private permiteDestinatarioExterno As Boolean = False
    Private cuentasShowCuotas As String() = {"13010200", "13010600", "13010200", "13051000", "13051100", "13051600", "13040100", "13040400"}
    Private listadoDeBancos As ValueList = New ValueList()
    'para cargar cuenta corriente
    Private DACuentaCte As MySqlDataAdapter
    Private DSCuentaCte As DataSet

    Dim Reporte As String = ""

    'Variables Fabian
    Dim jsonconverted As String
    Dim array() As Object
    Dim resgTransaccion As Object
    Dim cont As Int16 = 1

    'Imprimir el Recibo
    Private DTImprimir As DataTable
    Private DTImpDeta As DataTable
    Private DTAsiento As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long

    'Para guardar los cheques aparteUTEMatricula
    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter
    Private CantCheques As Integer = 0
    Dim resgMatriculado As Object
    Dim matricula As String
    Dim nombre As String
    Dim tipdoc As String
    Dim nrodoc As String
    Dim deleg As Integer
    Dim nrocli As Integer
    Dim unegos As Integer

    'Mostrar mensaje de error
    Dim msgError As String = Nothing

    Private Sub FrmAsientoMatricula_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'CantCheques = 0
        If ComprobarFechaServidor(False) Then
            Close()
            Exit Sub
        End If
    End Sub
    Private Sub DGVCuentaCte_InitializeLayout(ByVal sender As Object, ByVal e As InitializeLayoutEventArgs) Handles DGVCuentaCte.InitializeLayout
        Try
            If e.Layout.Bands.Count > 1 Then
                e.Layout.Bands(1).Columns(1).FilterOperatorLocation = FilterOperatorLocation.Hidden
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGVCuentaCte_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles DGVCuentaCte.InitializeRow
        DGVCuentaCte.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
        'Try
        '    If DGVCuentaCte.DisplayLayout.Bands.Count > 1 Then
        '        If e.Row.Band.Index = 0 Then
        '            If e.Row.Cells("Saldo").Value >= 0 Then
        '                e.Row.Appearance.BackColor = Color.Red
        '                e.Row.Appearance.ForeColor = Color.White
        '                e.Row.Appearance.BackColor2 = Color.Red
        '            Else
        '                e.Row.Appearance.BackColor = Color.Green
        '                e.Row.Appearance.ForeColor = Color.White
        '                e.Row.Appearance.BackColor2 = Color.Green
        '            End If
        '        ElseIf e.Row.Band.Index = 1 Then
        '            If e.Row.Cells("Total").Value >= 0 AndAlso e.Row.Cells("ImpPagado").Value <= 0 Then
        '                e.Row.Appearance.BackColor = Color.Red
        '                e.Row.Appearance.ForeColor = Color.White
        '                e.Row.Appearance.BackColor2 = Color.Red
        '            Else
        '                e.Row.Appearance.BackColor = Color.Yellow
        '                'e.Row.Appearance.ForeColor = Color.White
        '                e.Row.Appearance.BackColor2 = Color.Yellow
        '                'Else
        '                '    e.Row.Appearance.BackColor = Color.Green
        '                '    e.Row.Appearance.ForeColor = Color.White
        '                '    e.Row.Appearance.BackColor2 = Color.Green
        '            End If
        '        End If
        '        If e.Row.Band.Layout.IsPrintLayout Or e.Row.Band.Layout.IsExportLayout Then
        '            'since the ultragrid doesn't carry the selected state of a row to the print or export you have to find the real row that is
        '            'being displayed to the user
        '            Dim realGridRow As Infragistics.Win.UltraWinGrid.UltraGridRow = DGVCuentaCte.GetRowFromPrintRow(e.Row)
        '            'hide the row that is being used for printing/exporting so it doesn't show up
        '            e.Row.Hidden = Not realGridRow.Selected
        '        End If
        '    End If
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub CargaDelegaciones()

        UCEdelegacion.Items.Add(1, "Resistencia")
        UCEdelegacion.Items.Add(2, "Saenz Peña")
        UCEdelegacion.Items.Add(3, "Villa Angela")
        UCEdelegacion.Items.Add(4, "Sudoeste")
        UCEdelegacion.Items.Add(5, "Club SDC")

    End Sub

    Private Sub CargaInstitucion()
        UltraComboEditor1.Items.Add(1, "SIPRES")
        UltraComboEditor1.Items.Add(2, "CPCE")
    End Sub

    'Para cargar segun la matricula la cuenta corriente en el lateral derecho
    Private Sub UTEMatricula_KeyDown(sender As Object, e As KeyEventArgs) Handles UTEMatricula.KeyDown
        If e.KeyCode = Keys.Enter Then
            'UCEdelegacion.Value
            If String.IsNullOrWhiteSpace(UTEMatricula.Text) Then
                MessageBox.Show("Complete el campo con la matricula")
            Else
                BuscaMatriculado(UTEMatricula.Value)
            End If
        End If
    End Sub

    Private Sub UTEMatricula_EditorButtonClick(sender As Object, e As UltraWinEditors.EditorButtonEventArgs) Handles UTEMatricula.EditorButtonClick
        If String.IsNullOrWhiteSpace(UTEMatricula.Text) Then
            MessageBox.Show("Complete el campo con la matricula")
        Else
            BuscaMatriculado(UTEMatricula.Value)
        End If
    End Sub

    Private Sub CargaCuentaCte(ByVal tipDoc As String, ByVal nroDoc As Integer)
        Try
            cont = 1
            cnn.AbrirConexion()
            Dim tabla As String = "totales INNER JOIN plancuen ON pla_nropla=tot_nropla"
            Dim campos As String = "pla_nropla AS Cuenta, SUM(tot_debe-tot_haber) AS saldo, pla_nombre AS Servicio, tot_titulo, tot_matricula, tot_tipdoc, tot_nrodoc"
            Dim condiciones As String = "tot_tipdoc='" & tipDoc & "' AND tot_nrodoc=" & nroDoc & " AND pla_servicio = 'Si' AND MID(pla_nropla,1,1) = 1 AND tot_estado <> '9' GROUP BY pla_nropla"
            DACuentaCte = cnn.consultaBDadapter(tabla, campos, condiciones)
            DSCuentaCte = New DataSet
            DACuentaCte.Fill(DSCuentaCte, "servicios")
            'Recorro y elimino si no tiene saldo
            Dim guardoTotalServiciosBorrados As Integer = 0
            For Each row As DataRow In DSCuentaCte.Tables("servicios").Rows
                If row("saldo") <= 0 Then
                    row.Delete()
                    guardoTotalServiciosBorrados += 1
                Else
                    Dim frmCuotas As New FrmCuotasImpagas(cnn, "", row.Item("cuenta"), "H", tipDoc, nroDoc)
                    frmCuotas.CargaCuotas()
                    Dim dtNew = New DataTable
                    dtNew = frmCuotas.GetCuotasCerroFormulario()
                    'dtNew.TableName = row.Item("cuenta")
                    DSCuentaCte.Merge(dtNew)
                End If
            Next
            If DSCuentaCte.Tables("servicios").Rows.Count() > guardoTotalServiciosBorrados Then
                Dim DRServicioCuotas As New DataRelation("ServicioCuotas", DSCuentaCte.Tables("servicios").Columns("Cuenta"), DSCuentaCte.Tables("cuotas").Columns("Cuenta"), False)
                DSCuentaCte.Relations.Add(DRServicioCuotas)
                'DSCuentaCte.Tables(0).Columns.Add("Marcar", GetType(Boolean))
                DGVCuentaCte.DataSource = DSCuentaCte
                DGVCuentaCte.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
                With DSCuentaCte.Tables("servicios")
                    .Columns.Add("Cuotas", Type.GetType("System.String"))
                End With
                With DSCuentaCte.Tables("cuotas")
                    .Columns.Add("Cobrar", Type.GetType("System.String"))
                End With
                With DGVCuentaCte.DisplayLayout
                    With .Bands(0)
                        .Override.FilterUIType = FilterUIType.Default
                        .Columns(0).Width = 65
                        .Columns(0).CellActivation = Activation.NoEdit
                        .Columns(0).CellAppearance.TextHAlign = HAlign.Left
                        .Columns(1).Width = 75
                        .Columns(1).CellActivation = Activation.NoEdit
                        .Columns(1).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(1).Format = "c"
                        .Columns(2).Width = 300
                        .Columns(2).CellActivation = Activation.NoEdit
                        .Columns(2).CellAppearance.TextHAlign = HAlign.Left
                        .Columns(3).Hidden = True
                        .Columns(4).Hidden = True
                        '.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                        '.Columns(5).ReadOnly = False
                        .Columns(5).Hidden = True
                        .Columns(6).Hidden = True
                        '.Columns(7).Width = 50
                        Dim column As UltraGridColumn = DGVCuentaCte.DisplayLayout.Bands(0).Columns("Cuotas")
                        column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                        column.Width = 50
                        .Columns("Cuotas").Header.Caption = ""
                        .Columns("Cuotas").Style = ColumnStyle.Button
                        .Columns("Cuotas").CellButtonAppearance.Image = ImageList1.Images(3)
                        .Columns("Cuotas").CellButtonAppearance.ImageHAlign = HAlign.Center
                        .Columns(7).Hidden = True
                        .Columns(8).Hidden = True
                    End With
                    With .Bands(1)
                        .Override.FilterUIType = FilterUIType.Default
                        .Columns(0).Hidden = True
                        .Columns(1).Hidden = True
                        .Columns(2).Header.Caption = "Proceso"
                        .Columns(2).Width = 90
                        .Columns(2).CellActivation = Activation.NoEdit
                        .Columns(3).Hidden = True
                        .Columns(4).Header.Caption = "Fecha"
                        .Columns(4).CellActivation = Activation.NoEdit
                        .Columns(4).Width = 100
                        .Columns(5).CellActivation = Activation.NoEdit
                        '.Columns(5).CellAppearance.TextHAlign = HAlign.Right
                        '.Columns(5).Format = "c"
                        .Columns(5).Width = 100
                        .Columns(6).CellActivation = Activation.NoEdit
                        .Columns(6).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(6).Format = "c"
                        .Columns(7).Hidden = True
                        .Columns(8).Header.Caption = "Pagado"
                        .Columns(8).CellActivation = Activation.NoEdit
                        .Columns(8).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(8).Format = "c"
                        .Columns(9).Header.Caption = "Interes"
                        .Columns(9).CellActivation = Activation.NoEdit
                        .Columns(9).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(9).Format = "c"
                        .Columns(10).Hidden = True
                        .Columns(11).Hidden = True
                        '.Columns(12).Hidden = True
                        .Columns(13).Hidden = True
                        .Columns(14).Hidden = True
                        .Columns(15).Hidden = True
                        .Columns(16).Hidden = True
                        '*ALERTA* Esta columna total el id se utiliza en el metodo calculaInteresTotal()
                        .Columns(17).Width = 90
                        .Columns(17).Header.Caption = "Total"
                        .Columns(17).CellActivation = Activation.NoEdit
                        .Columns(17).CellAppearance.TextHAlign = HAlign.Right
                        .Columns(17).Format = "c"
                        'totalReal
                        .Columns(18).Hidden = True
                        'nroCuota
                        .Columns(19).Hidden = True
                        'Boton Cobrar
                        .Columns("Cobrar").Header.Caption = "Cobrar"
                        .Columns("Cobrar").Style = ColumnStyle.Button
                        .Columns("Cobrar").CellAppearance.Image = ImageList1.Images(6)
                        .Columns("Cobrar").CellAppearance.ImageHAlign = HAlign.Center
                        .Columns("Cobrar").CellButtonAppearance.Image = ImageList1.Images(6)
                        .Columns("Cobrar").CellButtonAppearance.ImageHAlign = HAlign.Center
                    End With
                End With
            Else
                MessageBox.Show("El matriculado No tiene deudas")
            End If
            cnn.CerrarConexion()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub BuscaMatriculado(ByVal matricula As String)
        Dim frmDesti As New FrmDestinatario
        'deleg = delegacion
        If Mid(matricula, 1, 2) <> "" And Mid(matricula, 3) <> "" Then
            'si de matricula los 2 primeros caracteres no son null y los siguientes tampoco entra
            Dim tipoMatricula As String = Mid(matricula, 1, 2)
            Dim numeroMatricula As String = Mid(matricula, 3)
            frmDesti.Condicion = "afi_titulo='" & tipoMatricula & "' and afi_matricula=" & CInt(numeroMatricula)
        End If
        frmDesti.CargaDestinatario()
        If frmDesti.MatriculaDestinatario <> "" Then
            TBMatricula.Text = frmDesti.MatriculaDestinatario
            TBMatriculado.Text = frmDesti.NombreDestinatario
            matricula = TBMatricula.Text
            nombre = TBMatriculado.Text
            CargaCuentaCte(frmDesti.TipoDocDestinatario, frmDesti.NroDocDestinatario)
            nrodoc = frmDesti.NroDocDestinatario
            tipdoc = frmDesti.TipoDocDestinatario
        Else
            frmDesti.ShowDialog()
            If frmDesti.DialogResult = DialogResult.OK Then
                TBMatricula.Text = frmDesti.MatriculaDestinatario
                TBMatriculado.Text = frmDesti.NombreDestinatario
                CargaCuentaCte(frmDesti.TipoDocDestinatario, frmDesti.NroDocDestinatario)
            End If
        End If
        'Resguardo matriculado
        UCEdelegacion.SelectedIndex = deleg
        UltraDateTimeEditor1.DateTime = Now
        nrocli = nPubNroCli
        unegos = nPubNroIns
        resgMatriculado = New With {Key matricula, Key tipdoc, Key nrodoc}
        frmDesti.Dispose()
    End Sub
    Private Sub DGVCuentaCte_ClickCellButton(sender As Object, e As CellEventArgs) Handles DGVCuentaCte.ClickCellButton
        ClickActiveCell()
    End Sub
    Private Sub ClickActiveCell()
        Dim e As UltraGridCell = DGVCuentaCte.ActiveCell
        'Dim array As New List(Of Object)()
        Select Case e.Column.Index
            'Case 6
                'BuscoCuotas(UGDetalles.ActiveRow)
            'Presiono el boton -Cobrar- de la cuota
            Case 20
                Dim proceso As String
                Dim cuota As Integer
                Dim total As Double
                Dim asiento As Long
                Dim item As Integer
                Dim interes As Long
                If MessageBox.Show("Confirmar el pago de la cuota?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    e.Row.CellAppearance.BackColor = Color.Green
                    e.Row.CellAppearance.Image = ImageList1.Images(5)
                    e.Row.Selected = False
                    e.Row.Activated = False
                    e.Row.Activation = Activation.Disabled
                    proceso = e.Row.Cells(2).Value
                    cuota = e.Row.Cells(12).Value
                    total = e.Row.Cells(17).Value
                    interes = e.Row.Cells(9).Value
                    asiento = e.Row.Cells(10).Value
                    item = e.Row.Cells(11).Value
                    resgTransaccion = New With {Key proceso, Key cuota, Key total, Key asiento, Key interes, Key nrocli, Key unegos, Key item}
                    ReDim Preserve array(cont)
                    array(cont) = resgTransaccion
                    cont = cont + 1
                Else
                    'No confirma el pago de la cuota
                    e.Row.CellAppearance.BackColor = Color.Red
                    e.Row.CellAppearance.Image = ImageList1.Images(4)
                    e.Row.Activated = False
                End If
                e.Row.Selected = False
                array(0) = resgMatriculado
                jsonconverted = Newtonsoft.Json.JsonConvert.SerializeObject(array)
        End Select
    End Sub
    Private Sub ButtonCobrar_Click(sender As Object, e As EventArgs) Handles ButtonCobrar.Click
        If MessageBox.Show("¿Confirmar el pago?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            'Dim cobrarRest As New Rest
            Dim nNroAsiImprime As Long = 0 'cobrarRest.sendTransaccion(jsonconverted)
            'SI VINO UN CERO SE PRODUJO ALGUN ERROR Y NO GRABO ASIENTO
            If nNroAsiImprime > 0 Then
                Dim imprimir As New Impresiones
                If Reporte.Trim = "" Then
                    imprimir.ImprimirRecibo(nNroAsiImprime)
                Else
                    imprimir.ImprimirAsiento(nNroAsiImprime, "MANUAL", "", True)
                End If
                Reporte = String.Empty
            End If
        Else
            'No confirmo el pago
            MessageBox.Show("¡No se ha confirmado el pago!")
        End If
    End Sub
    Private Sub Form_Activate()
        Refresh()
    End Sub

    Private Sub BtnVer_Click(sender As Object, e As EventArgs) Handles BtnVer.Click
        'Este botón lo cree sólo para ver como queda el json no va a estar en producción
        Try
            MessageBox.Show(jsonconverted.ToString())
        Catch ex As Exception
        End Try
    End Sub
End Class