﻿Public Class FrmCuotasImpagas
    Dim c_cnn As ConsultaBD
    Dim DTCuotasImpagas As DataTable
    Dim dt_Cuotas As DataTable
    Dim c_NroPlan As String
    Dim c_Proceso As String
    Dim c_Tipdoc As String
    'Dim c_tipMov As String
    Dim n_NroDoc As Integer
    Dim n_Impagas As Double
    Dim n_Interes As Double
    'Dim c_Matricula As String

    Public Sub New(ByVal cConexion As ConsultaBD, ByVal cProceso As String, ByVal cNroPlan As String, ByVal cTipMov As String, ByVal cTipDoc As String, ByVal nNroDoc As Integer)
        InitializeComponent()
        c_cnn = cConexion
        c_NroPlan = cNroPlan
        c_Proceso = cProceso
        c_Tipdoc = cTipDoc
        'c_tipMov = cTipMov SE PASA COMO PARAMETRO PERO NO SE UTILIZA
        n_NroDoc = nNroDoc
        'c_Matricula = cMatricula
    End Sub
    Private Sub FrmCuotasImpagas_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CargaCuotas()
        TBTotal.Focus()
    End Sub

    Public ReadOnly Property GetMontoImpagas() As Double
        Get
            Return n_Impagas
        End Get
    End Property

    Public ReadOnly Property GetMontoInteres() As Double
        Get
            Return n_Interes
        End Get
    End Property

    Public ReadOnly Property GetCuotasCanceladas() As DataTable
        Get
            Return dt_Cuotas
        End Get
    End Property

    Public ReadOnly Property GetCuotasCerroFormulario() As DataTable
        Get
            Return DTCuotasImpagas
        End Get
    End Property

    Private Sub cargoDTCuotasImpagas()
        Dim calculos As New Calculos
        ' SI ES PROCESO DE RECIBO DE MORATORIA BUSCO EN LA TABLA AUXILIAR
        If c_Proceso = "01RCMO" Or c_Proceso = "01DCMO" Or c_Proceso = "MORMAN" Then
            DTCuotasImpagas = calculos.CuotasMoratoriasImpagas(c_NroPlan, c_Tipdoc, n_NroDoc)
            Dim DTCuotasImpagasClone As DataTable = DTCuotasImpagas.Copy
            'Si es menor no tiene cuotas en la tabla cuo_det entonces busco como si entrara en el ELSE
            If DTCuotasImpagas.Rows.Count() <= 0 Then
                DTCuotasImpagasClone = calculos.CuotasServicioImpagas(c_NroPlan, c_Tipdoc, n_NroDoc, True, Now.Date)

                If DTCuotasImpagasClone.Rows.Count = 0 Then
                    DialogResult = DialogResult.Cancel
                Else
                    Try
                        For Each cuotasRow As DataRow In DTCuotasImpagasClone.Rows
                            Dim row As DataRow = DTCuotasImpagas.NewRow()
                            row("TIPO") = 2
                            row("NUMERO") = 1
                            row("NCUOTA") = 1
                            'row("MATRICULA") = c_Matricula
                            row("MATRICULA") = 1
                            row("TOTAL") = cuotasRow("total")
                            row("D1") = cuotasRow("total") * montosPorcentajesMoratoria(0)
                            row("D2") = cuotasRow("total") * montosPorcentajesMoratoria(1)
                            row("D3") = cuotasRow("total") * montosPorcentajesMoratoria(2)
                            row("C1") = cuotasRow("total") * montosPorcentajesMoratoria(3)
                            row("C2") = cuotasRow("total") * montosPorcentajesMoratoria(4)
                            row("C3") = cuotasRow("total") * montosPorcentajesMoratoria(5)
                            row("C4") = cuotasRow("total") * montosPorcentajesMoratoria(6)
                            row("C5") = cuotasRow("total") * montosPorcentajesMoratoria(7)
                            row("C6") = cuotasRow("total") * montosPorcentajesMoratoria(8)
                            row("C7") = cuotasRow("total") * montosPorcentajesMoratoria(9)
                            row("C8") = cuotasRow("total") * montosPorcentajesMoratoria(10)
                            row("F_ALTA") = cuotasRow("Fecha")
                            row("F_VTO") = cuotasRow("Vencimiento")
                            row("F_PAGO") = cuotasRow("Fecha")
                            row("ASIENTO") = cuotasRow("tot_nroasi")
                            row("RECIBO") = cuotasRow("NroCompr")
                            row("Select") = 0
                            row("totalReal") = cuotasRow("totalReal")
                            row("color") = "red"
                            row("cancelada") = 0
                            DTCuotasImpagas.Rows.Add(row)
                        Next
                    Catch ex As Exception
                        MessageBox.Show("Hubo un error contacte al administrador." & vbNewLine & vbNewLine & ex.Message,
                                        "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End Try
                End If
            End If
        Else
            DTCuotasImpagas = calculos.CuotasServicioImpagas(c_NroPlan, c_Tipdoc, n_NroDoc, True, Now.Date)

            If DTCuotasImpagas.Rows.Count = 0 Then

                Dim row As DataRow
                Dim DTCuotas As DataTable
                DTCuotas = calculos.CuotasServicioProximo(c_NroPlan, c_Tipdoc, n_NroDoc, True, Now.Date)
                row = DTCuotas.Rows.Item(0)

                DialogResult = DialogResult.Cancel
                MessageBox.Show(
                    "SE ENCUENTRA AL DIA." & vbNewLine &
                    "ANTE CUALQUIER DUDA CONSULTE LA CUENTA." & vbNewLine & vbNewLine &
                    "INFORMACION ADICIONAL:" & vbNewLine & vbNewLine &
                    "PROCESO: " & row("ProCancel") & vbNewLine &
                    "IMPORTE DE CUOTA: " & row("debeReal") & vbNewLine &
                    "CUENTA: " & row("Cuenta") & vbNewLine &
                    "CUOTA ANTERIOR: " & row("nroCuota"),
                "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub

    Public Sub CargaCuotas()
        'Aca cargo el DataTable con las cuotas impagas
        cargoDTCuotasImpagas()
        'Relleno DataGridView
        If c_Proceso = "01RCMO" Or c_Proceso = "01DCMO" Or c_Proceso = "MORMAN" Then
            With DGVCuotas
                .DataSource = DTCuotasImpagas
                .Columns("NUMERO").Width = 70
                .Columns("NCUOTA").Width = 60
                .Columns("NCUOTA").HeaderText = "CUOTA"
                .Columns("F_VTO").HeaderText = "VENCE"
                .Columns("F_VTO").Width = 90
                .Columns("F_PAGO").HeaderText = "PAGO"
                .Columns("F_PAGO").Width = 90
                .Columns("SELECT").Width = 50
                .Columns("TIPO").Visible = False
                .Columns("D1").Visible = False
                .Columns("D2").Visible = False
                .Columns("D3").Visible = False
                .Columns("C1").Visible = False
                .Columns("C2").Visible = False
                .Columns("C3").Visible = False
                .Columns("C4").Visible = False
                .Columns("C5").Visible = False
                .Columns("C6").Visible = False
                .Columns("C7").Visible = False
                .Columns("C8").Visible = False
                .Columns("F_ALTA").Visible = False
                .Columns("totalReal").Visible = False
                .Columns("color").Visible = False
                .Columns("cancelada").Visible = False
            End With
        Else
            With DGVCuotas
                .DataSource = DTCuotasImpagas
                .Sort(.Columns("Fecha"), ComponentModel.ListSortDirection.Descending)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).HeaderText = "Proceso"
                .Columns(2).Width = 70
                .Columns(2).ReadOnly = True
                .Columns(3).Visible = False
                .Columns(4).HeaderText = "Fecha"
                .Columns(4).ReadOnly = True
                .Columns(4).Width = 80
                .Columns(5).ReadOnly = True
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(5).DefaultCellStyle.Format = "c"

                .Columns(6).ReadOnly = True
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).DefaultCellStyle.Format = "c"

                '.Columns(7).Width = 80
                .Columns(7).Visible = False

                .Columns(8).HeaderText = "Pagado"
                .Columns(8).ReadOnly = True
                .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(8).DefaultCellStyle.Format = "c"

                .Columns(9).HeaderText = "Interes"
                .Columns(9).ReadOnly = True
                .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(9).DefaultCellStyle.Format = "c"

                .Columns(10).Visible = False
                .Columns(11).Visible = False
                '.Columns(12).Visible = False
                '.Columns(13).Visible = False
                '.Columns(14).HeaderText = "Cancela"
                '.Columns(14).ReadOnly = True
                .Columns(14).Visible = False
                .Columns(15).Visible = False
                .Columns(16).Visible = False
                '*ALERTA* Esta columna total el id se utiliza en el metodo calculaInteresTotal()
                .Columns(17).Width = 70
                .Columns(17).HeaderText = "Total"
                .Columns(17).ReadOnly = False
                .Columns(17).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(17).DefaultCellStyle.Format = "c"
                'totalReal
                .Columns(18).Visible = False
                'nroCuota
                '.Columns(19).Visible = False
                .Columns(19).Width = 35
                .Columns(19).HeaderText = "Nro"
            End With
        End If
    End Sub

    'Private Function CuotasServicioImpagas() As DataTable
    '    Dim DsPagos As New DataSet
    '    Dim Dscuotas As New DataSet
    '    Dim DAcuotas As MySqlDataAdapter
    '    DAcuotas = c_cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_haber) as pagos", "tot_nropla='" & c_NroPlan & "' and tot_titulo='" & c_Titulo & "' and tot_matricula=" & n_Matricula & " and tot_haber>0 and tot_estado<>'9' group by tot_nropla")
    '    DAcuotas.Fill(DsPagos, "pagos")
    '    Dim nTotalPagos As Double = DsPagos.Tables(0).Rows(0).Item("pagos")
    '    '        DAcuotas = c_cnn.consultaBDadapter("totales", "tot_proceso,CAST(tot_fecha as date) as tot_fecha,tot_debe as Debe,tot_imppag as ImpPagado", "tot_nropla='" & c_NroPlan & "' and tot_titulo='" & c_Titulo & "' and tot_matricula=" & n_Matricula & " and tot_debe>0 and tot_estado<>'9' order by tot_fecha")
    '    DAcuotas = c_cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "tot_nropla as Cuenta, pla_nombre as Descripcion,tot_proceso as Proceso,tot_nrocom as NroCompr,CAST(tot_fecha as date) as Fecha,tot_debe as Debe,tot_haber as Haber,tot_imppag as ImpPagado,tot_nroasi,tot_item,tot_nrocuo,tot_asicancel", "tot_nropla='" & c_NroPlan & "' and tot_titulo='" & c_Titulo & "' and tot_matricula=" & n_Matricula & " and tot_debe>0 and tot_estado<>'9' order by tot_fecha")
    '    DAcuotas.Fill(Dscuotas, "cuotas")
    '    Dscuotas.Tables(0).Columns.Add("Select", GetType(Boolean))
    '    For Each rowCuo As DataRow In Dscuotas.Tables(0).Rows
    '        If nTotalPagos > 0 Then
    '            If nTotalPagos > rowCuo.Item("debe") Then
    '                rowCuo.Item("imppagado") = rowCuo.Item("debe")
    '                nTotalPagos -= rowCuo.Item("debe")
    '            Else
    '                rowCuo.Item("imppagado") = nTotalPagos
    '                nTotalPagos = 0
    '            End If
    '        End If
    '    Next
    '    CuotasServicioImpagas = Dscuotas.Tables(0)
    'End Function
    Private Sub DGVCuotas_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGVCuotas.CellFormatting
        Dim row As DataGridViewRow = DGVCuotas.Rows(e.RowIndex)
        Try
            If c_Proceso = "01RCMO" Or c_Proceso = "01DCMO" Or c_Proceso = "MORMAN" Then
                'If IsDBNull(row.Cells("asiento").Value) Then
                '    row.DefaultCellStyle.BackColor = Color.Red
                'ElseIf row.Cells("asiento").Value = 0 Then
                '    row.DefaultCellStyle.BackColor = Color.Red
                'Else
                '    row.DefaultCellStyle.BackColor = Color.Green
                '    row.Cells("Select").ReadOnly = True
                'End If
                If row.Cells("color").Value = "red" Then
                    row.DefaultCellStyle.BackColor = Color.Red
                ElseIf row.Cells("color").Value = "green" Then
                    row.DefaultCellStyle.BackColor = Color.Green
                    row.Cells("Select").ReadOnly = True
                ElseIf row.Cells("color").Value = "yellow" Then
                    row.DefaultCellStyle.BackColor = Color.Yellow
                End If
            Else
                If row.Cells("ImpPagado").Value = 0 Then
                    row.DefaultCellStyle.BackColor = Color.Red
                Else
                    If row.Cells("ImpPagado").Value = row.Cells("Debe").Value Then
                        row.DefaultCellStyle.BackColor = Color.Green
                        row.Cells("Select").ReadOnly = True
                    Else
                        row.DefaultCellStyle.BackColor = Color.Yellow
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DGVCuotas_CellValueChanged(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles DGVCuotas.CellValueChanged
        Try
            If c_Proceso = "01RCMO" Or c_Proceso = "01DCMO" Or c_Proceso = "MORMAN" Then

            Else
                If e.RowIndex > -1 AndAlso DGVCuotas.Columns(e.ColumnIndex).Name = "Select" Then
                    If DGVCuotas.Rows(e.RowIndex).Cells("Select").Value Then
                        n_Impagas += DGVCuotas.Rows(e.RowIndex).Cells("Debe").Value - DGVCuotas.Rows(e.RowIndex).Cells("ImpPagado").Value
                        n_Interes += DGVCuotas.Rows(e.RowIndex).Cells("interes").Value
                    Else
                        n_Impagas -= DGVCuotas.Rows(e.RowIndex).Cells("Debe").Value + DGVCuotas.Rows(e.RowIndex).Cells("ImpPagado").Value
                        n_Interes -= DGVCuotas.Rows(e.RowIndex).Cells("interes").Value
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGVCuotas_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DGVCuotas.CurrentCellDirtyStateChanged
        If DGVCuotas.IsCurrentCellDirty Then
            'DGVCuotas.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub DGVCuotas_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles DGVCuotas.KeyPress
        Try
            Select Case e.KeyChar
                Case ChrW(Keys.Enter)
                    calculaInteresSegunTotal()
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DGVCuotas_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles DGVCuotas.KeyUp
        Try
            If e.KeyCode = Keys.Enter Then
                Dim selectedCellCount As Integer =
                    DGVCuotas.GetCellCount(DataGridViewElementStates.Selected)
                If selectedCellCount > 0 Then
                    calculaInteresSegunTotal()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub calculaInteresSegunTotal()
        Dim selectedCellCount As Integer =
            DGVCuotas.GetCellCount(DataGridViewElementStates.Selected)
        Dim fila As Integer = 0
        Dim columna As Integer = 0
        If selectedCellCount > 0 Then
            If DGVCuotas.AreAllCellsSelected(True) Then
                MessageBox.Show("Selecciono todas las cuotas", "Seleccione una Cuota")
            Else
                Dim i As Integer
                For i = 0 To selectedCellCount - 1
                    fila = DGVCuotas.SelectedCells(i).RowIndex.ToString() - 1
                    'Esta columna debe coincidir con la columna de Total metodo CargaCuotas()
                    columna = DGVCuotas.SelectedCells(i).ColumnIndex.ToString()
                Next i
            End If
        End If
        'Esta columna debe coincidir con la columna de Total metodo CargaCuotas()
        If columna = 17 Then
            For Each cell In DGVCuotas.SelectedCells
                CalculaInteresCelda(fila)
            Next
            'MessageBox.Show(DGVCuotas.SelectedCells.Item("Debe").Value.ToString())
        End If
    End Sub
    Private Sub CalculaInteresCelda(ByVal fila)
        'MessageBox.Show(cell.Value.ToString())
        Dim total As Double = DGVCuotas.Item("Total", fila).Value
        'DGVCuotas.Rows.Item(fila).Cells.Item("Total").Value
        Dim totalReal As Double = DGVCuotas.Item("totalReal", fila).Value
        'DGVCuotas.Rows.Item(fila).Cells.Item("totalReal").Value
        If total <= totalReal And total > 0 And totalReal > 0 Then
            'Dim porcentaje As Double = Math.Round((100 / totalReal * total), 2)
            Dim porcentaje As Double = 100 / totalReal * total
            'Dim porcentajeInteres As Double = DGVCuotas.Rows.Item(fila).Cells.Item("Debe").Value / DGVCuotas.Rows.Item(fila).Cells.Item("interes").Value
            Dim debe As Double = Math.Round((DGVCuotas.Item("debeReal", fila).Value / 100 * porcentaje), 2)
            'DGVCuotas.Rows.Item(fila).Cells.Item("debeReal").Value / 100 * porcentaje
            Dim interes As Double = Math.Round((DGVCuotas.Item("interesReal", fila).Value / 100 * porcentaje), 2)
            'DGVCuotas.Rows.Item(fila).Cells.Item("interesReal").Value / 100 * porcentaje
            DGVCuotas.Item("Debe", fila).Value = debe
            'DGVCuotas.Rows.Item(fila).Cells.Item("Debe").Value = debe
            DGVCuotas.Item("interes", fila).Value = interes
            'DGVCuotas.Rows.Item(fila).Cells.Item("interes").Value = interes
        End If
    End Sub

    Private Sub TBTotal_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TBTotal.KeyPress
        If pubCaracteresPermiteNumero.IndexOf(e.KeyChar) = -1 Then
            ' Invalid Character
            e.Handled = True
        ElseIf e.KeyChar = "." Then
            e.KeyChar = ","
        End If
        Try
            Select Case e.KeyChar
                Case ChrW(Keys.Enter)
                    'Dim selectedCellCount As Integer = _
                    'DGVCuotas.GetCellCount(DataGridViewElementStates.Selected)
                    'For i = 0 To selectedCellCount - 1
                    'Dim fila As String = DGVCuotas.SelectedCells(i).RowIndex.ToString() - 1
                    'Esta columna debe coincidir con la columna de Total metodo CargaCuotas()
                    'Next i
                    'For Each rowCuota In DGVCuotas.Rows
                    'MessageBox.Show(rowCuota)
                    'Next
                    Dim totalAPagar As Double = Me.TBTotal.Text
                    If c_Proceso = "01RCMO" Or c_Proceso = "01DCMO" Or c_Proceso = "MORMAN" Then
                        For i As Integer = 0 To (DGVCuotas.RowCount - 1) Step +1
                            'seteo valores como inicio
                            DGVCuotas.Item("Select", i).Value = False
                            If Not IsDBNull(DGVCuotas.Item("totalReal", i).Value) Then
                                DGVCuotas.Item("Total", i).Value = DGVCuotas.Item("totalReal", i).Value
                            End If
                            'fin seteo
                            If Not IsDBNull(DGVCuotas.Item("Total", i).Value) And Not IsDBNull(DGVCuotas.Item("totalReal", i).Value) Then
                                If totalAPagar > 0 Then
                                    If totalAPagar >= DGVCuotas.Item("Total", i).Value Then
                                        totalAPagar = Math.Round((totalAPagar - DGVCuotas.Item("Total", i).Value), 2)
                                    Else
                                        DGVCuotas.Item("Total", i).Value = totalAPagar
                                        totalAPagar = 0
                                    End If
                                    DGVCuotas.Item("Select", i).Value = True
                                End If
                            End If
                        Next
                    Else
                        For i As Integer = (DGVCuotas.RowCount - 1) To 0 Step -1
                            'seteo valores como inicio
                            DGVCuotas.Item("Select", i).Value = False
                            If Not IsDBNull(DGVCuotas.Item("totalReal", i).Value) Then
                                DGVCuotas.Item("Total", i).Value = DGVCuotas.Item("totalReal", i).Value
                                CalculaInteresCelda(i)
                            End If
                            'fin seteo
                            If Not IsDBNull(DGVCuotas.Item("Total", i).Value) And Not IsDBNull(DGVCuotas.Item("Debe", i).Value) And Not IsDBNull(DGVCuotas.Item("ImpPagado", i).Value) Then
                                If DGVCuotas.Item("Debe", i).Value <> DGVCuotas.Item("ImpPagado", i).Value Then
                                    If totalAPagar > 0 Then
                                        If totalAPagar >= DGVCuotas.Item("Total", i).Value Then
                                            totalAPagar = Math.Round((totalAPagar - DGVCuotas.Item("Total", i).Value), 2)
                                            'DGVCuotas.Item("Select", i).Value = True
                                        Else
                                            DGVCuotas.Item("Total", i).Value = totalAPagar
                                            CalculaInteresCelda(i)
                                            totalAPagar = 0
                                        End If
                                        DGVCuotas.Item("Select", i).Value = True
                                    End If
                                End If
                            ElseIf Not IsDBNull(DGVCuotas.Item("totalReal", i).Value) Then
                                If DGVCuotas.Item("Debe", i).Value <> DGVCuotas.Item("ImpPagado", i).Value Then
                                    If totalAPagar > 0 Then
                                        If totalAPagar >= DGVCuotas.Item("totalReal", i).Value Then
                                            totalAPagar = Math.Round((totalAPagar - DGVCuotas.Item("Debe", i).Value), 2)
                                            'DGVCuotas.Item("Select", i).Value = True
                                        Else
                                            DGVCuotas.Item("totalReal", i).Value = totalAPagar
                                            CalculaInteresCelda(i)
                                            totalAPagar = 0
                                        End If
                                        DGVCuotas.Item("Select", i).Value = True
                                    End If
                                End If
                            End If
                        Next
                    End If
                    Tabular(e)
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TBTotal_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles TBTotal.LostFocus
        Me.TBTotal.Text = Format(CType(Me.TBTotal.Text, Decimal), "#,##0.00")
    End Sub

    Private Sub CBConfirmar_Click(sender As Object, e As EventArgs) Handles CBConfirmar.Click
        ConfirmarAsiento()
    End Sub
    Private Sub ConfirmarAsientoKeyDown(ByVal e As KeyEventArgs)
        If e.Shift And e.KeyCode = Keys.C Then
            ConfirmarAsiento()
        End If
    End Sub

    Private Sub ConfirmarAsiento()
        dt_Cuotas = DTCuotasImpagas
        DialogResult = DialogResult.OK
    End Sub

    Private Sub DGVCuotas_KeyDown(sender As Object, e As KeyEventArgs) Handles DGVCuotas.KeyDown
        ConfirmarAsientoKeyDown(e)
    End Sub
End Class