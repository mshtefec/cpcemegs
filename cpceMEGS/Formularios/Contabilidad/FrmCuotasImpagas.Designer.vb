﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCuotasImpagas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCuotasImpagas))
        Me.CBConfirmar = New System.Windows.Forms.Button()
        Me.LTotal = New System.Windows.Forms.Label()
        Me.DGVCuotas = New System.Windows.Forms.DataGridView()
        Me.TBTotal = New System.Windows.Forms.TextBox()
        Me.LabelControlConfirmar = New System.Windows.Forms.Label()
        CType(Me.DGVCuotas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CBConfirmar
        '
        Me.CBConfirmar.BackColor = System.Drawing.Color.Transparent
        Me.CBConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBConfirmar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.CBConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBConfirmar.Location = New System.Drawing.Point(227, 486)
        Me.CBConfirmar.Name = "CBConfirmar"
        Me.CBConfirmar.Size = New System.Drawing.Size(152, 35)
        Me.CBConfirmar.TabIndex = 1
        Me.CBConfirmar.Text = "Confirmar"
        Me.CBConfirmar.UseVisualStyleBackColor = False
        '
        'LTotal
        '
        Me.LTotal.AutoSize = True
        Me.LTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTotal.Location = New System.Drawing.Point(465, 493)
        Me.LTotal.Name = "LTotal"
        Me.LTotal.Size = New System.Drawing.Size(69, 20)
        Me.LTotal.TabIndex = 15
        Me.LTotal.Text = "Total: $"
        '
        'DGVCuotas
        '
        Me.DGVCuotas.AllowUserToAddRows = False
        Me.DGVCuotas.AllowUserToDeleteRows = False
        Me.DGVCuotas.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.DGVCuotas.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVCuotas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVCuotas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVCuotas.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGVCuotas.Dock = System.Windows.Forms.DockStyle.Top
        Me.DGVCuotas.Location = New System.Drawing.Point(0, 0)
        Me.DGVCuotas.Name = "DGVCuotas"
        Me.DGVCuotas.RowHeadersVisible = False
        Me.DGVCuotas.Size = New System.Drawing.Size(880, 480)
        Me.DGVCuotas.TabIndex = 2
        '
        'TBTotal
        '
        Me.TBTotal.Location = New System.Drawing.Point(540, 495)
        Me.TBTotal.Name = "TBTotal"
        Me.TBTotal.Size = New System.Drawing.Size(123, 20)
        Me.TBTotal.TabIndex = 0
        Me.TBTotal.Text = "0"
        '
        'LabelControlConfirmar
        '
        Me.LabelControlConfirmar.AutoSize = True
        Me.LabelControlConfirmar.Location = New System.Drawing.Point(174, 503)
        Me.LabelControlConfirmar.Name = "LabelControlConfirmar"
        Me.LabelControlConfirmar.Size = New System.Drawing.Size(47, 13)
        Me.LabelControlConfirmar.TabIndex = 29
        Me.LabelControlConfirmar.Text = "Shift + C"
        '
        'FrmCuotasImpagas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(880, 521)
        Me.Controls.Add(Me.LabelControlConfirmar)
        Me.Controls.Add(Me.TBTotal)
        Me.Controls.Add(Me.LTotal)
        Me.Controls.Add(Me.CBConfirmar)
        Me.Controls.Add(Me.DGVCuotas)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(90, 100)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCuotasImpagas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuotas Impagas"
        CType(Me.DGVCuotas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CBConfirmar As System.Windows.Forms.Button
    Friend WithEvents LTotal As System.Windows.Forms.Label
    Friend WithEvents DGVCuotas As System.Windows.Forms.DataGridView
    Friend WithEvents TBTotal As System.Windows.Forms.TextBox
    Friend WithEvents LabelControlConfirmar As System.Windows.Forms.Label
End Class
