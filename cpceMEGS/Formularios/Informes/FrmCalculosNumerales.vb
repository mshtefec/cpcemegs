﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win

Public Class FrmCalculosNumerales
    Private cnn As New ConsultaBD(True)
    Private DANumerales As MySqlDataAdapter
    Private DTcuentas As DataTable
    Private DTNumerales As DataTable
    Private DTDevenga As New DataTable
    Private DTAfiliados As New DataTable
    Private DTAfiliadosClon As New DataTable
    Private BSAfiliados As New BindingSource
    Private clsExportarExcel As New ExportarExcel

    Private Sub UTbCuentas_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTbCuentas.EditorButtonClick
        BuscoCuenta()
    End Sub

    Private Sub ProcesarCalculoNumerales()
        Dim nTotCapital As Double = 0
        Dim nTotNumeral As Double = 0
        Dim nTotGeneralNumeral As Double = 0
        Dim nTotAcredita As Double = 0
        Dim nTotDiasNumeral As Long = 0
        Dim nDiasNumeral As Long = 0
        Dim nDiasNumeralContador As Long = 0
        Dim nMtr As Integer = 0
        Dim cTit As String = ""
        Dim DTMovimientos As New DataTable
        Dim DTSaldoAnt As New DataTable
        Dim xReg As Integer = 0

        DTAfiliados = New DataTable
        DTAfiliadosClon = New DataTable
        BSAfiliados = New BindingSource

        DANumerales = cnn.consultaBDadapter(
            "afiliado",
            "afi_tipdoc,afi_nrodoc,afi_titulo,afi_matricula,afi_nombre,afi_debitos as capital,afi_creditos as numeral",
            "(afi_tipo = 'A' OR afi_tipo = 'D' OR afi_tipo = 'E') AND afi_matricula > 0 AND afi_generarrenta = 'SI' ORDER BY afi_matricula"
        )
        DANumerales.Fill(DTAfiliados)
        DTAfiliados.Columns.Add("cuenta", GetType(String))
        DTAfiliados.Columns.Add("proporcion", GetType(Double))
        DTAfiliados.Columns.Add("acredita", GetType(Double))
        Dim rowTotales As DataRow = DTAfiliados.NewRow
        rowTotales.Item("afi_nombre") = "TOTALES"
        rowTotales.Item("capital") = 0
        rowTotales.Item("numeral") = 0
        rowTotales.Item("proporcion") = 0
        rowTotales.Item("acredita") = 0
        DTAfiliados.Rows.Add(rowTotales)

        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Maximum = DTAfiliados.Rows.Count

        DTAfiliadosClon = DTAfiliados.Clone
        ' DTAfiliadosClon.ImportRow(rowTotales)
        BSAfiliados.DataSource = DTAfiliadosClon
        UGNumerales.DataSource = BSAfiliados
        With UGNumerales.DisplayLayout.Bands(0)
            .Columns(0).Hidden = True
            .Columns(1).Hidden = True
            .Columns(2).Width = 40
            .Columns(3).Width = 60
            .Columns(4).Width = 200
        End With
   
        For Each rowAfi As DataRow In DTAfiliados.Rows
            Application.DoEvents()
            BSAfiliados.Position = xReg

            nTotCapital = 0
            nTotNumeral = 0
            ' el ultimo registro es totales
            If Not IsDBNull(rowAfi.Item("afi_nrodoc")) Then
                For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes
                    ' If Nodo.CheckedState = CheckState.Checked Then
                    nTotCapital = 0
                    nTotNumeral = 0
                    rowAfi.Item("cuenta") = Nodo.Text.Substring(0, 8)

                    DTSaldoAnt = New DataTable
                    DTMovimientos = New DataTable
                    DANumerales = cnn.consultaBDadapter("totales", , "tot_nropla='" & Nodo.Text.Substring(0, 8) & "' and tot_fecha between '" & Format(Convert.ToDateTime(UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' and '" & Format(Convert.ToDateTime(UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' and tot_estado <> '9' and tot_tipdoc='" & rowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & rowAfi.Item("afi_nrodoc"))
                    DANumerales.Fill(DTMovimientos)
                    DANumerales = cnn.consultaBDadapter("totales", "tot_titulo,tot_matricula,sum(tot_debe-tot_haber) as saldo", "tot_nropla='" & Nodo.Text.Substring(0, 8) & "' and tot_tipdoc='" & rowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & rowAfi.Item("afi_nrodoc") & " and tot_fecha < '" & Format(Convert.ToDateTime(UDTdesde.Text), "yyyy-MM-dd") & "' and tot_estado<>'9'")
                    DANumerales.Fill(DTSaldoAnt)
                    ' nTotCapital = 0
                    If IsDBNull(DTSaldoAnt.Rows(0).Item("saldo")) Then
                        nTotCapital = 0
                    Else
                        nTotCapital = DTSaldoAnt.Rows(0).Item("saldo")
                        nDiasNumeral = DateDiff(DateInterval.Day, UDTdesde.Value, UDThasta.Value)
                        nTotDiasNumeral += nDiasNumeral
                        nTotNumeral = nDiasNumeral * DTSaldoAnt.Rows(0).Item("saldo")
                        nDiasNumeralContador += 1
                    End If

                    For Each rowMov As DataRow In DTMovimientos.Rows
                        nDiasNumeral = DateDiff(DateInterval.Day, CDate(rowMov.Item("tot_fecha").ToString), UDThasta.Value)
                        nTotDiasNumeral += nDiasNumeral
                        nTotNumeral += nDiasNumeral * (rowMov.Item("tot_debe") - rowMov.Item("tot_haber"))
                        nTotCapital += rowMov.Item("tot_debe") - rowMov.Item("tot_haber")
                        nDiasNumeralContador += 1
                    Next
                    If nTotCapital <> 0 Then
                        rowAfi.Item("capital") = FormatNumber(nTotCapital, 2)
                        rowAfi.Item("numeral") = FormatNumber(nTotNumeral, 2)
                        DTAfiliadosClon.ImportRow(rowAfi)
                        nTotGeneralNumeral += nTotNumeral
                    End If
                    ' End If
                Next
                'rowAfi.Item("capital") = FormatNumber(nTotCapital, 2)
                'rowAfi.Item("numeral") = FormatNumber(nTotNumeral, 2)
                ' DTAfiliados.Rows(BSAfiliados.Count - 1).Item("numeral") += nTotNumeral
            Else
                rowAfi.Item("numeral") = nTotGeneralNumeral
                DTAfiliadosClon.ImportRow(rowAfi)
            End If
            UltraProgressBar1.Value = xReg
            UltraProgressBar1.Refresh()
            xReg += 1
        Next

        'rowTotales.Item("proporcion") = 0
        'rowTotales.Item("numeral") = 0
        'rowTotales.Item("acredita") = 0
        'DTAfiliadosClon.ImportRow(rowTotales)
        DTAfiliadosClon.Rows(BSAfiliados.Count - 1).Item("proporcion") = 0

        For Each rowAfi As DataRow In DTAfiliadosClon.Rows
            If Not IsDBNull(rowAfi.Item("afi_nrodoc")) Then
                rowAfi.Item("proporcion") = rowAfi.Item("numeral") / nTotGeneralNumeral
                rowAfi.Item("acredita") = (rowAfi.Item("numeral") / nTotGeneralNumeral) * UCEMontoAcredita.Value
                nTotAcredita += rowAfi.Item("acredita")
                '  DTAfiliadosClon.Rows(BSAfiliados.Count - 1).Item("numeral") += rowAfi.Item("numeral")
                DTAfiliadosClon.Rows(BSAfiliados.Count - 1).Item("proporcion") += rowAfi.Item("proporcion")
            End If
        Next
        DTAfiliadosClon.Rows(BSAfiliados.Count - 1).Item("numeral") = nTotGeneralNumeral
        LTasaEfectiva.Text = "Tasa Efectiva: " & ((nTotAcredita * (nTotDiasNumeral / nDiasNumeralContador)) / nTotGeneralNumeral)
        'For I As Integer = DTAfiliadosClon.Rows.Count - 1 To 0 Step -1
        '    If Not IsDBNull(DTAfiliadosClon.Rows(I).Item("afi_nrodoc")) Then
        '        If DTAfiliadosClon.Rows(I).Item("capital") = 0 Then
        '            DTAfiliadosClon.Rows.RemoveAt(I)
        '        End If
        '    End If
        'Next
    End Sub
    Private Sub CBtProcesar_Click(sender As Object, e As EventArgs) Handles CBtProcesar.Click
        CBtDevengar.Enabled = False
        CBtExportar.Enabled = False
        ProcesarCalculoNumerales()
        CBtDevengar.Enabled = True
        CBtExportar.Enabled = True
    End Sub
    Private Sub BuscoCuenta()
        DTcuentas = New DataTable
        DANumerales = cnn.consultaBDadapter("plancuen", , "pla_nropla='" & UTbCuentas.Value & "'")
        DANumerales.Fill(DTcuentas)
        If DTcuentas.Rows.Count = 1 Then
            UltraTree1.Nodes.Add("" & DTcuentas.Rows(0).Item("pla_nropla").ToString & " " & DTcuentas.Rows(0).Item("pla_nombre").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
            UTbCuentas.Text = ""
        End If
    End Sub

    Private Sub UTbCuentas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles UTbCuentas.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscoCuenta()
        End If
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim cCtaInvolucrada As String = "Cuentas Involucradas:" & vbLf

        For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes

            If Nodo.CheckedState = CheckState.Checked Then
                cCtaInvolucrada += "     " & Nodo.Text & vbCrLf
            End If
        Next
        Dim cListado(0) As String
        cListado(0) = "ESTADO DE NUMERALES AL " & UDThasta.Value & vbLf & cCtaInvolucrada

        clsExportarExcel.ExportarDatosExcel(UGNumerales, cListado)
    End Sub

    Private Sub UCEMontoAcredita_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UCEMontoAcredita.EditorButtonClick
        AcreditaRenta()
    End Sub

    Private Sub AcreditaRenta()
        Dim xReg As Integer = 0
        Dim nTotAcredita As Double = 0
        For Each rowAfi As DataRow In DTAfiliadosClon.Rows
            Application.DoEvents()
            xReg += 1
            BSAfiliados.Position = xReg
            ' el ultimo registro es totales
            If Not IsDBNull(rowAfi.Item("afi_nrodoc")) Then
                rowAfi.Item("acredita") = 0
                For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes

                    If Nodo.CheckedState = CheckState.Checked Then
                        If rowAfi.Item("cuenta") = Nodo.Text.Substring(0, 8) Then
                            rowAfi.Item("acredita") = rowAfi.Item("proporcion") * UCEMontoAcredita.Value
                            nTotAcredita += rowAfi.Item("acredita")
                        End If

                    End If
                Next
            Else
                rowAfi.Item("acredita") = nTotAcredita
            End If
        Next
    End Sub

    Private Sub DebengarRentas()

        Dim rowDev As DataRow
        Dim nItem As Integer = 1
        Dim nTotDevenga As Double = 0
        ArmoAsientoDevengamiento()

        For Each rowAfi As DataRow In DTAfiliadosClon.Rows
            If rowAfi.Item("acredita") > 0 And Not IsDBNull(rowAfi.Item("afi_nrodoc")) Then
                nTotDevenga += Math.Round(rowAfi.Item("acredita"), 2)
                rowDev = DTDevenga.NewRow
                nItem += 1
                rowDev.Item("cuenta") = rowAfi.Item("cuenta")
                rowDev.Item("debe") = 0
                rowDev.Item("haber") = Math.Round(rowAfi.Item("acredita"), 2)
                rowDev.Item("pto_item") = nItem
                rowDev.Item("pto_tipmov") = "H"
                rowDev.Item("subcuenta") = rowAfi.Item("afi_tipdoc") & rowAfi.Item("afi_nrodoc")
                rowDev.Item("matricula") = rowAfi.Item("afi_titulo") & rowAfi.Item("afi_matricula")
                DTDevenga.Rows.Add(rowDev)
            End If
        Next
        DTDevenga.Rows(0).Item("debe") = Math.Round(nTotDevenga, 2)
        UGNumerales.DataSource = DTDevenga
    End Sub
    Private Sub ArmoAsientoDevengamiento()
        DANumerales = cnn.consultaBDadapter("(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)", "pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio,pla_cuotas", "pto_codpro='ACRRTA' order by pto_codpro,pto_item")
        DTDevenga = New DataTable
        DANumerales.Fill(DTDevenga)
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        newCol.DefaultValue = "ACRRTA"
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("delegacion", Type.GetType("System.Int32"))
        newCol.DefaultValue = nPubNroCli
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UDTfecDev.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UDTfecDev.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DTDevenga.Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = False
        DTDevenga.Columns.Add(newCol)

    End Sub
    'Private Sub UDTfecDev_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UDTfecDev.EditorButtonClick
    '    If e.Button.Key = "Right" Then
    '        UDTfecDev.Visible = False
    '        CBtDevengar.Visible = True
    '        DebengarRentas()
    '    End If
    'End Sub
    Private Sub CBtDevengar_Click(sender As Object, e As EventArgs) Handles CBtDevengar.Click
        'If CBtDevengar.Text = "Grabar Asiento" Then
        DebengarRentas()
        Dim dtChe As New DataTable
        Dim nAsiento As Long
        nAsiento = cnn.GraboAsiento(DTDevenga, dtChe, dtChe, dtChe, "", "", "", "", 0, 0, False)
        MessageBox.Show("Proceso finalizado exitosamente, Ultimo N° Asiento: " & nAsiento)
        'Else
        '    UDTfecDev.Visible = True
        '    CBtExportar.Visible = False
        '    CBtDevengar.Visible = False
        '    CBtDevengar.Text = "Grabar Asiento"
        '    UDTfecDev.Focus()
        'End If
    End Sub
End Class