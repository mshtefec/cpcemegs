﻿Imports MySql.Data.MySqlClient
Imports MSScriptControl

Public Class FrmMorososCuotasAportes
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    'Dim DTAfiliadoClone As DataTable
    'Dim DTListado As DataTable
    'Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    'Dim cmdListado As MySqlCommandBuilder
    'Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel
    Dim datosMorosos(12, 9) As Object
    Dim tableCantidadRango As New DataTable
    Private cuentasSumaCantidad As String() = {"110101", "110102", "110103", "110104", "110105", "120000", "120101", "120102", "120103", "120104", "170000", "170100", "170101"}

    Private Sub FrmMorososCuotasAportes_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Today
    End Sub

    Private Sub InitCantidadRango()
        datosMorosos(0, 0) = "De 0 cuotas"
        datosMorosos(0, 7) = CDbl(0)
        datosMorosos(1, 0) = "De " & TB1de.Text() & " a " & TB1a.Text() & " cuotas"
        datosMorosos(1, 7) = CDbl(TB1a.Text()) / 12
        datosMorosos(2, 0) = "De " & TB2de.Text() & " a " & TB2a.Text() & " cuotas"
        datosMorosos(2, 7) = CDbl(TB2a.Text()) / 12
        datosMorosos(3, 0) = "De " & TB3de.Text() & " a " & TB3a.Text() & " cuotas"
        datosMorosos(3, 7) = CDbl(TB3a.Text()) / 12
        datosMorosos(4, 0) = "De " & TB4de.Text() & " a " & TB4a.Text() & " cuotas"
        datosMorosos(4, 7) = CDbl(TB4a.Text()) / 12
        datosMorosos(5, 0) = "De " & TB5de.Text() & " a " & TB5a.Text() & " cuotas"
        datosMorosos(5, 7) = CDbl(TB5a.Text()) / 12
        datosMorosos(6, 0) = "De " & TB6de.Text() & " a " & TB6a.Text() & " cuotas"
        datosMorosos(6, 7) = CDbl(TB6a.Text()) / 12
        datosMorosos(7, 0) = "De " & TB7de.Text() & " a " & TB7a.Text() & " cuotas"
        datosMorosos(7, 7) = CDbl(TB7a.Text()) / 12
        datosMorosos(8, 0) = "De " & TB8de.Text() & " a " & TB8a.Text() & " cuotas"
        datosMorosos(8, 7) = CDbl(TB8a.Text()) / 12
        datosMorosos(9, 0) = "De " & TB9de.Text() & " a " & TB9a.Text() & " cuotas"
        datosMorosos(9, 7) = CDbl(TB9a.Text()) / 12
        datosMorosos(10, 0) = "De " & TB10de.Text() & " a " & TB10a.Text() & " cuotas"
        datosMorosos(10, 7) = CDbl(TB10a.Text()) / 12
        datosMorosos(11, 0) = "De " & TB11de.Text() & " a " & TB11a.Text() & " cuotas"
        datosMorosos(11, 7) = CDbl(TB11a.Text()) / 12
        'totales
        datosMorosos(12, 0) = "Total"
        datosMorosos(12, 1) = 0
        datosMorosos(12, 2) = 0
        datosMorosos(12, 3) = 0
        datosMorosos(12, 7) = 0
        'inicio en 0 varios valores
        For x As Integer = 0 To 11
            For y As Integer = 1 To 3
                datosMorosos(x, y) = 0
            Next
        Next
    End Sub

    Private Sub SetCantidadRango(ByVal cantidadCuotas As Integer, ByVal montoCuotas As Double, ByVal montoInteres As Double, ByVal sumadorCantidad As Integer)
        Select Case cantidadCuotas
            Case 0
                datosMorosos(0, 1) += sumadorCantidad
                datosMorosos(0, 2) += montoCuotas
                datosMorosos(0, 3) += (montoCuotas + montoInteres)
            Case TB1de.Text() To TB1a.Text()
                datosMorosos(1, 1) += sumadorCantidad
                datosMorosos(1, 2) += montoCuotas
                datosMorosos(1, 3) += (montoCuotas + montoInteres)
            Case TB2de.Text() To TB2a.Text()
                datosMorosos(2, 1) += sumadorCantidad
                datosMorosos(2, 2) += montoCuotas
                datosMorosos(2, 3) += (montoCuotas + montoInteres)
            Case TB3de.Text() To TB3a.Text()
                datosMorosos(3, 1) += sumadorCantidad
                datosMorosos(3, 2) += montoCuotas
                datosMorosos(3, 3) += (montoCuotas + montoInteres)
            Case TB4de.Text() To TB4a.Text()
                datosMorosos(4, 1) += sumadorCantidad
                datosMorosos(4, 2) += montoCuotas
                datosMorosos(4, 3) += (montoCuotas + montoInteres)
            Case TB5de.Text() To TB5a.Text()
                datosMorosos(5, 1) += sumadorCantidad
                datosMorosos(5, 2) += montoCuotas
                datosMorosos(5, 3) += (montoCuotas + montoInteres)
            Case TB6de.Text() To TB6a.Text()
                datosMorosos(6, 1) += sumadorCantidad
                datosMorosos(6, 2) += montoCuotas
                datosMorosos(6, 3) += (montoCuotas + montoInteres)
            Case TB7de.Text() To TB7a.Text()
                datosMorosos(7, 1) += sumadorCantidad
                datosMorosos(7, 2) += montoCuotas
                datosMorosos(7, 3) += (montoCuotas + montoInteres)
            Case TB8de.Text() To TB8a.Text()
                datosMorosos(8, 1) += sumadorCantidad
                datosMorosos(8, 2) += montoCuotas
                datosMorosos(8, 3) += (montoCuotas + montoInteres)
            Case TB9de.Text() To TB9a.Text()
                datosMorosos(9, 1) += sumadorCantidad
                datosMorosos(9, 2) += montoCuotas
                datosMorosos(9, 3) += (montoCuotas + montoInteres)
            Case TB10de.Text() To TB10a.Text()
                datosMorosos(10, 1) += sumadorCantidad
                datosMorosos(10, 2) += montoCuotas
                datosMorosos(10, 3) += (montoCuotas + montoInteres)
            Case TB11de.Text() To TB11a.Text()
                datosMorosos(11, 1) += sumadorCantidad
                datosMorosos(11, 2) += montoCuotas
                datosMorosos(11, 3) += (montoCuotas + montoInteres)
            Case Else
                'no entro en ninguno
        End Select
        'totales
        datosMorosos(12, 1) += sumadorCantidad
        datosMorosos(12, 2) += montoCuotas
        datosMorosos(12, 3) += (montoCuotas + montoInteres)
    End Sub

    Private Sub SetPorcentajes()
        For x As Integer = 0 To 11
            datosMorosos(x, 4) = datosMorosos(x, 1) / datosMorosos(12, 1)
            datosMorosos(x, 5) = datosMorosos(x, 2) / datosMorosos(12, 2)
            datosMorosos(x, 6) = datosMorosos(x, 3) / datosMorosos(12, 3)
            'Dolares a Valores Originales
            datosMorosos(x, 8) = datosMorosos(x, 2) / TBCotizacionDolar.Text
            'Dolares a Valores Actualizado
            datosMorosos(x, 9) = datosMorosos(x, 3) / TBCotizacionDolar.Text
        Next
    End Sub

    Private Sub CantidadRangoToDataTable()
        For x As Integer = 0 To 11
            'sumo en totales
            datosMorosos(12, 7) += datosMorosos(x, 7)
            datosMorosos(12, 8) += datosMorosos(x, 8)
            datosMorosos(12, 9) += datosMorosos(x, 9)
            'Aca formateo y dejo 2 decimales a los valores
            datosMorosos(x, 2) = Format(CDbl(datosMorosos(x, 2)), "F")
            datosMorosos(x, 3) = Format(CDbl(datosMorosos(x, 3)), "F")
            datosMorosos(x, 4) = Format(CDbl(datosMorosos(x, 4)), "0.00%")
            datosMorosos(x, 5) = Format(CDbl(datosMorosos(x, 5)), "0.00%")
            datosMorosos(x, 6) = Format(CDbl(datosMorosos(x, 6)), "0.00%")
            datosMorosos(x, 7) = Format(CDbl(datosMorosos(x, 7)), "F")
            datosMorosos(x, 8) = Format(CDbl(datosMorosos(x, 8)), "F")
            datosMorosos(x, 9) = Format(CDbl(datosMorosos(x, 9)), "F")
        Next
        'Aca formateo y dejo 2 decimales a los valores de totales
        datosMorosos(12, 2) = Format(CDbl(datosMorosos(12, 2)), "F")
        datosMorosos(12, 3) = Format(CDbl(datosMorosos(12, 3)), "F")
        'Set 100%
        datosMorosos(12, 4) = "100%"
        datosMorosos(12, 5) = "100%"
        datosMorosos(12, 6) = "100%"
        'Aca formateo y dejo 2 decimales a los valores de totales
        datosMorosos(12, 7) = Format(CDbl(datosMorosos(12, 7)), "F")
        datosMorosos(12, 8) = Format(CDbl(datosMorosos(12, 8)), "F")
        datosMorosos(12, 9) = Format(CDbl(datosMorosos(12, 9)), "F")
        'Fill your array and then
        tableCantidadRango.Columns.Add(New DataColumn("TRAMOS POR CUOTAS")) '0
        tableCantidadRango.Columns.Add(New DataColumn("CANTIDAD COLEGAS")) '1
        tableCantidadRango.Columns.Add(New DataColumn("TOTAL DEUDA")) '2
        tableCantidadRango.Columns.Add(New DataColumn("DEUDA ACTUALIZADA")) '3
        tableCantidadRango.Columns.Add(New DataColumn("% Cantidad Colegas Cumplimiento")) '4
        tableCantidadRango.Columns.Add(New DataColumn("% S/Total Deud.Origin.")) '5
        tableCantidadRango.Columns.Add(New DataColumn("% S/Total Deud.Act.")) '6
        tableCantidadRango.Columns.Add(New DataColumn("Años")) '7
        tableCantidadRango.Columns.Add(New DataColumn("Dolares a Valores Originales")) '8
        tableCantidadRango.Columns.Add(New DataColumn("Dolares a Valores Actualizado")) '9

        For x As Integer = 0 To datosMorosos.GetUpperBound(0)
            Dim row As DataRow = tableCantidadRango.NewRow
            For y As Integer = 0 To datosMorosos.GetUpperBound(1)
                row.Item(y) = datosMorosos(x, y)
            Next
            tableCantidadRango.Rows.Add(row)
        Next
    End Sub

    Private Sub ListadoMorosos()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = "afi_tipo='A' and afi_matricula > 0"
        'Dim cfiltro As String = "afi_tipo='A' and afi_matricula > 0 and mid(afi_categoria,1,2) in ('11','12')"
        'Dim cfiltro As String = "afi_tipo='A' and afi_matricula > 0 and mid(afi_categoria,1,2) in ('11','12','17')"
        Dim cTablas As String = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim DTMovimientos As New DataTable

        Dim cGrupBy As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        Dim cColumnas As String = "afi_categoria,afi_tipdoc,afi_nrodoc,0 as cuotasAportes,afi_debitos as DeudaAportes"

        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim nCantCuo As Integer = 0
        Dim nMonCuo As Double = 0
        Dim nTotMonCuo As Double = 0
        Dim nRegCuo As Integer = 0
        Dim nMonInteres As Double = 0
        Dim cCuentas(1, 3) As String
        Dim x As Integer = 0
        Dim sumador As Integer = 0
        cCuentas(0, 1) = "13040100"
        cCuentas(0, 2) = "cuotasAportes"
        cCuentas(0, 3) = "DeudaAportes"
        UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        For Each RowAfi As DataRow In DTAfiliado.Rows
            Application.DoEvents()
            DTMovimientos = cuotas.CuotasServicioImpagas(cCuentas(x, 1), RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True, Now.Date)
            nCantCuo = 0
            nMonCuo = 0
            nMonInteres = 0
            For Each rowMov As DataRow In DTMovimientos.Rows
                If CDate(rowMov.Item("vencimiento").ToString) >= UDTdesde.Value And CDate(rowMov.Item("vencimiento").ToString) <= UDThasta.Value Then
                    nRegCuo += 1
                    'nMonCuo += rowMov.Item("debe") - rowMov.Item("imppagado")
                    If rowMov.Item("debe") - rowMov.Item("imppagado") > 0 Then
                        nMonCuo += rowMov.Item("debe") - rowMov.Item("imppagado")
                        nMonInteres += rowMov.Item("interes")
                        nCantCuo += 1
                    End If
                    'Else
                    'nCantCuo += 1
                    'nMonInteres += rowMov.Item("interes")
                End If
            Next

            If cuentasSumaCantidad.Contains(RowAfi.Item("afi_categoria")) Then ' Suma cantidad PARA LAS CUENTAS DEFINIDAS
                sumador = 1
            Else
                sumador = 0
            End If
            If nCantCuo <= 0 Then
                nMonInteres = 0
            End If

            SetCantidadRango(nCantCuo, nMonCuo, nMonInteres, sumador)
            SetPorcentajes()

            UltraProgressBar1.Value += 1
        Next
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim cListado(3) As String
        cListado(0) = "Listado de Morosos Cuotas Aportes"
        clsExportarExcel.ExportarDatosExcel(UGMorososCuotasAportes, cListado)
    End Sub

    Private Sub CBtListar_Click(sender As Object, e As EventArgs) Handles CBtListar.Click
        InitCantidadRango()
        ListadoMorosos()
        CantidadRangoToDataTable()
        UGMorososCuotasAportes.DataSource = tableCantidadRango
        'With UGMorososCuotasAportes.DisplayLayout.Bands(0)
        '    .Columns(2).Format = "N2"
        '    .Columns(3).Format = "N2"
        '    .Columns(7).Format = "N2"
        '    .Columns(8).Format = "N2"
        '    .Columns(9).Format = "N2"
        'End With
    End Sub
End Class