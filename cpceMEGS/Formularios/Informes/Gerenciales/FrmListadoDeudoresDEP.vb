﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl
Public Class FrmListadoDeudoresDEP
    Dim cnn As New ConsultaBD(cPubServidor, "cpce", cPubUsuario, cPubClave)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0


    Private Sub CargaListViewAfiliado()

        DAListado = cnn.consultaBDadapter("afiliado inner join cuentas on cue_tipdoc=afi_tipdoc and cue_nrodoc=afi_nrodoc", , "afi_nrodoc=999999999")
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim objListItem As New ListViewItem

        Me.ListViewAfil.Items.Clear()
        ListViewAfil.Columns.Add("")
        ListViewAfil.Columns.Add("Campo")
        ListViewAfil.Columns.Add("Orden")
        For Each col As DataColumn In DTAfiliado.Columns

            objListItem = New ListViewItem()
            objListItem.SubItems.Add(col.Caption)
            objListItem.SubItems.Add(0)
            ListViewAfil.Items.Add(objListItem)
        Next
        ListViewAfil.Columns(0).Width = 20
        ListViewAfil.Columns(1).Width = 150
    End Sub

    Dim clsExportarExcel As New ExportarExcel

    Private Sub ListadoMorosos(Optional ByVal seguro As Boolean = False)
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String
        Dim cTablas As String
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cGrupBy As String = ""
        Dim cColumnas As String = "0 as Cantidad"
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        If seguro Then
            cfiltro = "afi_tipo = 'A' AND afi_tipdoc <> 'ECO' AND afi_matricula > 0 AND mid(afi_categoria,1,2) IN ('11','12','17') ORDER BY afi_nrodoc ASC"
            cTablas = "(afiliado left join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
            cColumnas += ",afi_nombre,afi_nrodoc,afi_fecnac,0 as Edad,afi_tipdoc,afi_titulo,afi_matricula,0 as cuotasDEP,afi_debitos as DeudaDEP,0 as cuotasDA,afi_debitos as DeudaDA,0 as cuotasAportes,afi_debitos as DeudaAportes,0 as cuotasJub,afi_debitos as DeudaJub"
        Else
            cfiltro = "afi_tipo = 'A' AND afi_tipdoc <> 'ECO' AND afi_matricula > 0 AND afi_categoria <> '220100' ORDER BY afi_matricula ASC"
            'AND mid(afi_categoria,1,2) IN ('11','12','17','20','21','22')
            cTablas = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"

            For I As Integer = 1 To nOrdenSeleccion
                For Each item As ListViewItem In ListViewAfil.Items
                    If item.Checked And CInt(item.SubItems(2).Text) = I Then
                        If cColumnas.Trim <> "" Then
                            cColumnas += ","
                        End If
                        cColumnas += item.SubItems(1).Text
                    End If
                Next
            Next

            If cColumnas = "" Then
                MessageBox.Show("Debe seleccionar los datos del profesional")
                Exit Sub
            End If
            cColumnas += ",afi_tipdoc,afi_nrodoc,afi_fecnac,0 as Edad,0 as cuotasDEP,afi_debitos as DeudaDEP,0 as cuotasDA,afi_debitos as DeudaDA,0 as cuotasAportes,afi_debitos as DeudaAportes,0 as cuotasJub,afi_debitos as DeudaJub"
        End If

        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        'Dim columns(2) As DataColumn
        'columns(0) = DTAfiliado.Columns("afi_tipdoc")
        'columns(1) = DTAfiliado.Columns("afi_nrodoc")
        'columns(2) = DTAfiliado.Columns("importe")
        'DTAfiliado.PrimaryKey = columns

        DTAfiliadoClone = DTAfiliado.Clone

        Dim nCantidadAfiliado As Integer = 0
        Dim nCantCuo As Integer = 0
        Dim nTotCantCuo As Integer = 0
        Dim nMonCuo As Double = 0
        Dim nMonInteres As Double = 0
        Dim nTotMonCuo As Double = 0
        Dim nTotMonIntereses As Double = 0
        Dim nRegCuo As Integer = 0
        Dim nMontos(3, 2) As Double
        Dim cCuentas(3, 3) As String
        Dim agregarAfiliado As Boolean

        cCuentas(0, 1) = "13010200"
        cCuentas(0, 2) = "cuotasDEP"
        cCuentas(0, 3) = "DeudaDEP"
        cCuentas(1, 1) = "13010600"
        cCuentas(1, 2) = "cuotasDA"
        cCuentas(1, 3) = "DeudaDA"
        cCuentas(2, 1) = "13040100"
        cCuentas(2, 2) = "cuotasAportes"
        cCuentas(2, 3) = "DeudaAportes"
        cCuentas(3, 1) = "13040300"
        cCuentas(3, 2) = "cuotasJub"
        cCuentas(3, 3) = "DeudaJub"
        'cCuentas(3, 1) = "22010100"
        'cCuentas(3, 2) = "CantidadACI"
        'cCuentas(3, 3) = "TotalACI"
        UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        For Each RowAfi As DataRow In DTAfiliado.Rows
            RowAfi.Item("Edad") = DateDiff("yyyy", CDate(RowAfi.Item("afi_fecnac").ToString), Now)
            agregarAfiliado = True
            Application.DoEvents()
            For x As Integer = 0 To 3
                nMontos(x, 1) = 0
                nMontos(x, 2) = 0
                DTMovimientos = cuotas.CuotasServicioImpagas(cCuentas(x, 1), RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True, Now.Date)
                nCantCuo = 0
                nMonCuo = 0
                For Each rowMov As DataRow In DTMovimientos.Rows
                    If CDate(rowMov.Item("vencimiento").ToString) < UDThasta.Value Then
                        nRegCuo += 1
                        If nMonCuo <> rowMov.Item("debe") - rowMov.Item("imppagado") Then
                            If nMonCuo > 0 Then
                                nMontos(x, 1) = nCantCuo
                                nMontos(x, 2) = nMonCuo
                            End If
                        End If
                        nMonCuo += rowMov.Item("debe") - rowMov.Item("imppagado")
                        If rowMov.Item("debe") - rowMov.Item("imppagado") > 0 Then
                            nCantCuo += 1
                        End If
                    Else
                        '   nCantCuo += 1
                        nMonInteres += rowMov.Item("interes")
                    End If
                Next

                If nMonCuo > 0 Then
                    nMontos(x, 1) = nCantCuo
                    nMontos(x, 2) = nMonCuo
                End If

                If seguro And RowAfi.Item("Edad") < 65 And nCantCuo > 3 Then
                    agregarAfiliado = False
                    Exit For
                End If
            Next

            If agregarAfiliado Then
                For x As Integer = 0 To 3
                    RowAfi.Item(cCuentas(x, 2)) = nMontos(x, 1)
                    RowAfi.Item(cCuentas(x, 3)) = nMontos(x, 2)
                Next
                nCantidadAfiliado += 1
                RowAfi.Item("Cantidad") = nCantidadAfiliado
                DTAfiliadoClone.ImportRow(RowAfi)
            End If

            UltraProgressBar1.Value += 1
        Next

        'For i As Integer = DTAfiliadoClone.Rows.Count - 1 To 0 Step -1
        'cfiltro = "S"
        'If cboAporte.Text.Trim <> "" Then
        '    If oSC.Eval(DTAfiliadoClone.Rows(i).Item("cuotasAportes").ToString.Replace(",", ".") & cboAporte.Text & UNUCuoAportes.Value.ToString.Replace(",", ".")) Then
        '        ' cfiltro = "N"
        '    Else
        '        cfiltro = "N"
        '    End If
        'End If
        'If cfiltro = "S" Then
        '    If cboDEP.Text.Trim <> "" Then
        '        If oSC.Eval(DTAfiliadoClone.Rows(i).Item("cuotasDEP").ToString.Replace(",", ".") & cboDEP.Text & UNEcuoDEP.Value.ToString.Replace(",", ".")) Then
        '        Else
        '            cfiltro = "N"

        '        End If
        '    End If
        'End If
        'If cfiltro = "S" Then
        '    If UCEreintegros.Text.Trim <> "" Then
        '        If UCEreintegros.Text = "SI" Then
        '            If DTAfiliadoClone.Rows(i).Item("Reintegros").ToString.Replace(",", ".") > 0 Then
        '            Else
        '                cfiltro = "N"

        '            End If
        '        Else
        '            If DTAfiliadoClone.Rows(i).Item("Reintegros").ToString.Replace(",", ".") = 0 Then
        '            Else
        '                cfiltro = "N"

        '            End If
        '        End If
        '    End If
        'End If

        'If cfiltro = "N" Then
        '    DTAfiliadoClone.Rows(i).Delete()
        'End If
        'Next

        UGlistados.DataSource = DTAfiliadoClone

        If seguro Then
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(1).Header.Caption = "Afiliado"
                .Columns(2).Header.Caption = "DNI Nº"
                .Columns(3).Header.Caption = "Fecha Nac"
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 8).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 7).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 6).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 5).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 4).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 3).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 2).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 1).Hidden = True
            End With
        End If
        With UGlistados.DisplayLayout.Bands(0)
            .Columns(DTAfiliadoClone.Columns.Count - 7).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 5).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"
        End With
    End Sub

    Private Sub FrmListadoDeudoresDEP_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UDThasta.Value = Now
        CargaListViewAfiliado()
    End Sub

    Private Sub ListViewAfil_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles ListViewAfil.ItemCheck

        If e.CurrentValue = CheckState.Unchecked Then
            nOrdenSeleccion += 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = nOrdenSeleccion
        Else
            nOrdenSeleccion -= 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = 0
        End If
    End Sub

    Private Sub CBtExportar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.ClickButtonArea
        Dim cListado(0) As String
        cListado(0) = "Listado Gerencial de Deudores DEP"
        'cListado(1) = "Parametro1: Deudores Aportes " & cboAporte.Text & " " & UNUCuoAportes.Value.ToString & " cuotas"
        'cListado(2) = "Parametro2: Deudores DEP " & cboDEP.Text & " " & UNEcuoDEP.Value.ToString & " cuotas"
        'cListado(3) = "Parametro3: Reintegros " & UCEreintegros.Text
        clsExportarExcel.ExportarDatosExcel(UGlistados, cListado)
    End Sub

    Private Sub CBtListar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtListar.ClickButtonArea
        ListadoMorosos()
    End Sub

    Private Sub ButtonSeguro_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ButtonSeguro.ClickButtonArea
        ListadoMorosos(True)
    End Sub
End Class