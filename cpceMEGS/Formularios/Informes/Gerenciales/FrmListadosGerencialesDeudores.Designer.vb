﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadosGerencialesDeudores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim DesignerRectTracker1 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListadosGerencialesDeudores))
        Dim DesignerRectTracker2 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker3 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker4 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker5 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim DesignerRectTracker6 As CButtonLib.DesignerRectTracker = New CButtonLib.DesignerRectTracker
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton
        Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem
        Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem
        Me.ListViewAfil = New System.Windows.Forms.ListView
        Me.UGlistados = New Infragistics.Win.UltraWinGrid.UltraGrid
        Me.CBtAcualizar = New CButtonLib.CButton
        Me.txCuenta = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.CBtListar = New CButtonLib.CButton
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.UCESaldo = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
        Me.CBtExportar = New CButtonLib.CButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
        Me.UNUCuoAportes = New Infragistics.Win.UltraWinEditors.UltraNumericEditor
        Me.UNEcuoDEP = New Infragistics.Win.UltraWinEditors.UltraNumericEditor
        Me.UCEreintegros = New Infragistics.Win.UltraWinEditors.UltraComboEditor
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCESaldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNUCuoAportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEcuoDEP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEreintegros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListViewAfil
        '
        Me.ListViewAfil.CheckBoxes = True
        Me.ListViewAfil.Dock = System.Windows.Forms.DockStyle.Left
        Me.ListViewAfil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewAfil.Location = New System.Drawing.Point(0, 0)
        Me.ListViewAfil.Name = "ListViewAfil"
        Me.ListViewAfil.Size = New System.Drawing.Size(269, 612)
        Me.ListViewAfil.TabIndex = 0
        Me.ListViewAfil.UseCompatibleStateImageBehavior = False
        Me.ListViewAfil.View = System.Windows.Forms.View.Details
        '
        'UGlistados
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
        Me.UGlistados.DisplayLayout.Appearance = Appearance1
        Me.UGlistados.DisplayLayout.InterBandSpacing = 10
        Me.UGlistados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGlistados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGlistados.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGlistados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.TextHAlignAsString = "Left"
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGlistados.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGlistados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGlistados.DisplayLayout.Override.RowAppearance = Appearance4
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.RowSelectorAppearance = Appearance6
        Me.UGlistados.DisplayLayout.Override.RowSelectorWidth = 12
        Me.UGlistados.DisplayLayout.Override.RowSpacingBefore = 2
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance5.ForeColor = System.Drawing.Color.Black
        Me.UGlistados.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGlistados.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UGlistados.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
        Me.UGlistados.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGlistados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGlistados.Location = New System.Drawing.Point(269, 0)
        Me.UGlistados.Name = "UGlistados"
        Me.UGlistados.Size = New System.Drawing.Size(715, 120)
        Me.UGlistados.TabIndex = 1
        Me.UGlistados.Text = "Listados"
        '
        'CBtAcualizar
        '
        Me.CBtAcualizar.BackColor = System.Drawing.Color.Transparent
        Me.CBtAcualizar.BorderShow = False
        DesignerRectTracker1.IsActive = False
        DesignerRectTracker1.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker1.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtAcualizar.CenterPtTracker = DesignerRectTracker1
        Me.CBtAcualizar.FocalPoints.CenterPtX = 0.0!
        Me.CBtAcualizar.FocalPoints.CenterPtY = 0.68!
        Me.CBtAcualizar.FocalPoints.FocusPtX = 0.0!
        Me.CBtAcualizar.FocalPoints.FocusPtY = 0.0!
        DesignerRectTracker2.IsActive = False
        DesignerRectTracker2.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker2.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtAcualizar.FocusPtTracker = DesignerRectTracker2
        Me.CBtAcualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAcualizar.ImageIndex = 0
        Me.CBtAcualizar.Location = New System.Drawing.Point(325, 562)
        Me.CBtAcualizar.Name = "CBtAcualizar"
        Me.CBtAcualizar.SideImage = CType(resources.GetObject("CBtAcualizar.SideImage"), System.Drawing.Image)
        Me.CBtAcualizar.SideImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.CBtAcualizar.SideImageSize = New System.Drawing.Size(32, 32)
        Me.CBtAcualizar.Size = New System.Drawing.Size(186, 38)
        Me.CBtAcualizar.TabIndex = 116
        Me.CBtAcualizar.Text = "Actualizar"
        '
        'txCuenta
        '
        Me.txCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCuenta.Location = New System.Drawing.Point(350, 444)
        Me.txCuenta.Name = "txCuenta"
        Me.txCuenta.Size = New System.Drawing.Size(285, 22)
        Me.txCuenta.TabIndex = 117
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(284, 447)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 118
        Me.Label1.Text = "Cuentas:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(293, 473)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Saldos"
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.BorderShow = False
        DesignerRectTracker3.IsActive = False
        DesignerRectTracker3.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker3.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtListar.CenterPtTracker = DesignerRectTracker3
        Me.CBtListar.FocalPoints.CenterPtX = 0.0!
        Me.CBtListar.FocalPoints.CenterPtY = 0.68!
        Me.CBtListar.FocalPoints.FocusPtX = 0.0!
        Me.CBtListar.FocalPoints.FocusPtY = 0.0!
        DesignerRectTracker4.IsActive = False
        DesignerRectTracker4.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker4.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtListar.FocusPtTracker = DesignerRectTracker4
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.ImageIndex = 0
        Me.CBtListar.Location = New System.Drawing.Point(539, 562)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.SideImage = CType(resources.GetObject("CBtListar.SideImage"), System.Drawing.Image)
        Me.CBtListar.SideImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.CBtListar.SideImageSize = New System.Drawing.Size(32, 32)
        Me.CBtListar.Size = New System.Drawing.Size(187, 38)
        Me.CBtListar.TabIndex = 121
        Me.CBtListar.Text = "Listar"
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {">", "<", "=", ">=", "<=", "<>"})
        Me.ComboBox1.Location = New System.Drawing.Point(350, 470)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(46, 24)
        Me.ComboBox1.TabIndex = 118
        '
        'UCESaldo
        '
        Me.UCESaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCESaldo.Location = New System.Drawing.Point(402, 470)
        Me.UCESaldo.Name = "UCESaldo"
        Me.UCESaldo.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCESaldo.Size = New System.Drawing.Size(107, 24)
        Me.UCESaldo.TabIndex = 119
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.BorderShow = False
        DesignerRectTracker5.IsActive = False
        DesignerRectTracker5.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker5.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtExportar.CenterPtTracker = DesignerRectTracker5
        Me.CBtExportar.FocalPoints.CenterPtX = 0.0!
        Me.CBtExportar.FocalPoints.CenterPtY = 0.68!
        Me.CBtExportar.FocalPoints.FocusPtX = 0.0!
        Me.CBtExportar.FocalPoints.FocusPtY = 0.0!
        DesignerRectTracker6.IsActive = False
        DesignerRectTracker6.TrackerRectangle = CType(resources.GetObject("DesignerRectTracker6.TrackerRectangle"), System.Drawing.RectangleF)
        Me.CBtExportar.FocusPtTracker = DesignerRectTracker6
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(757, 562)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.SideImage = CType(resources.GetObject("CBtExportar.SideImage"), System.Drawing.Image)
        Me.CBtExportar.SideImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.CBtExportar.SideImageSize = New System.Drawing.Size(32, 32)
        Me.CBtExportar.Size = New System.Drawing.Size(187, 38)
        Me.CBtExportar.TabIndex = 123
        Me.CBtExportar.Text = "Exportar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(664, 473)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta Fecha:"
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2014, 6, 3, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(753, 471)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 120
        Me.UDThasta.Value = New Date(2014, 6, 3, 0, 0, 0, 0)
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(350, 512)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(146, 20)
        Me.CheckBox1.TabIndex = 127
        Me.CheckBox1.Text = "Listado de Morosos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(659, 447)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 128
        Me.Label4.Text = "Desde Fecha:"
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(753, 443)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 24)
        Me.UDTdesde.TabIndex = 129
        Me.UDTdesde.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'UNUCuoAportes
        '
        EditorButton1.Text = "Cuotas Deudores Aportes:"
        EditorButton1.Width = 170
        Me.UNUCuoAportes.ButtonsLeft.Add(EditorButton1)
        Me.UNUCuoAportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNUCuoAportes.Location = New System.Drawing.Point(320, 182)
        Me.UNUCuoAportes.Name = "UNUCuoAportes"
        Me.UNUCuoAportes.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNUCuoAportes.Size = New System.Drawing.Size(226, 24)
        Me.UNUCuoAportes.TabIndex = 130
        '
        'UNEcuoDEP
        '
        EditorButton2.Text = "Cuotas D.E.P.:"
        EditorButton2.Width = 170
        Me.UNEcuoDEP.ButtonsLeft.Add(EditorButton2)
        Me.UNEcuoDEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEcuoDEP.Location = New System.Drawing.Point(320, 212)
        Me.UNEcuoDEP.Name = "UNEcuoDEP"
        Me.UNEcuoDEP.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEcuoDEP.Size = New System.Drawing.Size(226, 24)
        Me.UNEcuoDEP.TabIndex = 131
        '
        'UCEreintegros
        '
        EditorButton3.Text = "Reintegros Pendientes"
        EditorButton3.Width = 170
        Me.UCEreintegros.ButtonsLeft.Add(EditorButton3)
        Me.UCEreintegros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ValueListItem1.DataValue = "ValueListItem0"
        ValueListItem1.DisplayText = "SI"
        ValueListItem2.DataValue = "ValueListItem1"
        ValueListItem2.DisplayText = "NO"
        Me.UCEreintegros.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2})
        Me.UCEreintegros.Location = New System.Drawing.Point(320, 242)
        Me.UCEreintegros.Name = "UCEreintegros"
        Me.UCEreintegros.Size = New System.Drawing.Size(226, 24)
        Me.UCEreintegros.TabIndex = 132
        Me.UCEreintegros.Text = " "
        '
        'FrmListadosGerencialesDeudores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 612)
        Me.Controls.Add(Me.UCEreintegros)
        Me.Controls.Add(Me.UNEcuoDEP)
        Me.Controls.Add(Me.UNUCuoAportes)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UCESaldo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txCuenta)
        Me.Controls.Add(Me.CBtAcualizar)
        Me.Controls.Add(Me.UGlistados)
        Me.Controls.Add(Me.ListViewAfil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmListadosGerencialesDeudores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listados"
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCESaldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNUCuoAportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEcuoDEP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEreintegros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListViewAfil As System.Windows.Forms.ListView
    Friend WithEvents UGlistados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtAcualizar As CButtonLib.CButton
    Friend WithEvents txCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CBtListar As CButtonLib.CButton
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents UCESaldo As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents CBtExportar As CButtonLib.CButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UNUCuoAportes As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UNEcuoDEP As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UCEreintegros As Infragistics.Win.UltraWinEditors.UltraComboEditor
End Class
