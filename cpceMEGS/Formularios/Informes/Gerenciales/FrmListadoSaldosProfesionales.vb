﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl
Public Class FrmListadoSaldosProfesionales
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel

    Private Sub FrmListadoSaldosProfesionales_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Now
    End Sub

    Private Sub ListadoMorosos()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = "afi_tipo='A' and afi_matricula>0"
        Dim cTablas As String = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cGrupBy As String = ""
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        'If cColumnas = "" Then
        '    MessageBox.Show("Debe seleccionar los datos del profesional")
        '    Exit Sub
        'End If
        cColumnas = "afi_tipdoc,afi_nrodoc,afi_nombre as Profesional,concat(afi_titulo,afi_matricula) as Matricula,afi_debitos as DeudaAportes,afi_debitos as DeudaMoratoria,afi_debitos as DgtoMensual,afi_debitos as DgtoInteresesFdo,afi_debitos as DgtoGAyFF,afi_debitos as Total"

        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        'Dim columns(2) As DataColumn
        'columns(0) = DTAfiliado.Columns("afi_tipdoc")
        'columns(1) = DTAfiliado.Columns("afi_nrodoc")
        'columns(2) = DTAfiliado.Columns("importe")
        'DTAfiliado.PrimaryKey = columns

        DTAfiliadoClone = DTAfiliado.Clone

        Dim nCantCuo As Integer = 0
        Dim nTotCantCuo As Integer = 0
        Dim nMonCuo As Double = 0
        Dim nMonInteres As Double = 0
        Dim nTotMonCuo As Double = 0
        Dim nTotMonIntereses As Double = 0
        Dim nRegCuo As Integer = 0
        Dim nMontos(3, 2) As Double
        Dim cCuentas(4, 2) As String
        cCuentas(0, 1) = "13040100"
        cCuentas(0, 2) = "DeudaAportes"
        cCuentas(1, 1) = "13040400"
        cCuentas(1, 2) = "DeudaMoratoria"
        cCuentas(2, 1) = "22040100"
        cCuentas(2, 2) = "DgtoMensual"
        cCuentas(3, 1) = "22040700"
        cCuentas(3, 2) = "DgtoInteresesFdo"
        cCuentas(4, 1) = "22040800"
        cCuentas(4, 2) = "DgtoGAyFF"
        UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        For Each RowAfi As DataRow In DTAfiliado.Rows
            Application.DoEvents()
            nMonCuo = 0
            For x As Integer = 0 To 4
                DAListado = cnn.consultaBDadapter("totales", "tot_nropla,Sum(tot_debe-tot_haber) as saldo", "tot_nropla='" & cCuentas(x, 1) & "' and tot_fecha <='" & Format(UDThasta.Value, "yyyy-MM-dd") & "' and tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " and tot_estado<>'9'")
                DTMovimientos = New DataTable
                DAListado.Fill(DTMovimientos)
                If Not IsDBNull(DTMovimientos.Rows(0).Item("saldo")) Then
                    RowAfi.Item(cCuentas(x, 2)) = DTMovimientos.Rows(0).Item("saldo")
                    nMonCuo += DTMovimientos.Rows(0).Item("saldo")
                End If
            Next
            If nMonCuo > 0 Then
                RowAfi.Item("total") = nMonCuo
                DTAfiliadoClone.ImportRow(RowAfi)
            End If
            UltraProgressBar1.Value += 1
        Next

        UGlistados.DataSource = DTAfiliadoClone

        With UGlistados.DisplayLayout.Bands(0)
            .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 2).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"
        End With

    End Sub

    Private Sub CBtListar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtListar.Click
        ListadoMorosos()
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim cListado(0) As String
        cListado(0) = "Listado Saldos Profesionales"
        clsExportarExcel.ExportarDatosExcel(UGlistados, cListado)
    End Sub
End Class