﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl


Public Class FrmListadosGerencialesDeudores
    Dim cnn As New ConsultaBD(cPubServidor, "cpce", cPubUsuario, cPubClave)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel

    Private Sub CargaListViewAfiliado()

        DAListado = cnn.consultaBDadapter("afiliado inner join cuentas on cue_tipdoc=afi_tipdoc and cue_nrodoc=afi_nrodoc", , "afi_nrodoc=999999999")
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim objListItem As New ListViewItem

        Me.ListViewAfil.Items.Clear()
        ListViewAfil.Columns.Add("")
        ListViewAfil.Columns.Add("Campo")
        ListViewAfil.Columns.Add("Orden")
        For Each col As DataColumn In DTAfiliado.Columns

            objListItem = New ListViewItem()
            objListItem.SubItems.Add(col.Caption)
            objListItem.SubItems.Add(0)
            ListViewAfil.Items.Add(objListItem)
        Next
        ListViewAfil.Columns(0).Width = 20
        ListViewAfil.Columns(1).Width = 150
    End Sub


    Private Sub CargaListados()
        DAListado = cnn.consultaBDadapter("listados")
        DTListado = New DataTable
        cmdListado = New MySqlCommandBuilder(DAListado)
        DAListado.Fill(DTListado)
        BSListado = New BindingSource
        BSListado.DataSource = DTListado
        UGlistados.DataSource = BSListado
        With UGlistados.DisplayLayout.Bands(0)
            .Columns(0).CellActivation = UltraWinGrid.Activation.NoEdit
            .Columns(1).CellActivation = UltraWinGrid.Activation.AllowEdit
            .Columns(2).CellActivation = UltraWinGrid.Activation.AllowEdit
            .Columns(2).Width = 800
        End With
    End Sub
    Private Sub FrmListados_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UDThasta.Value = Now
        CargaListViewAfiliado()
        CargaListados()
    End Sub

    Private Sub CBtAcualizar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtAcualizar.ClickButtonArea
        If CBtAcualizar.Text = "Volver a Listados" Then
            CBtAcualizar.Text = "Actualizar"
            CargaListados()
        Else
            Try
                DAListado.Update(DTListado)
                DTListado.AcceptChanges()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If


    End Sub


    Private Sub ArmarConsulta()
        Dim DTPadron As New DataTable
        Dim cfiltro As String = "" ' DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        Dim cTablas As String = "" ' "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim cGrupBy As String = ""
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For I As Integer = 1 To nOrdenSeleccion

            For Each item As ListViewItem In ListViewAfil.Items

                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next


        If txCuenta.TextLength > 0 Then
            cfiltro += "tot_nropla  in (" & txCuenta.Text.Trim & ") and tot_fecha BETWEEN '" & Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:0:00' AND '" & Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' AND tot_estado<>'9' and (" & DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim & ") group by tot_tipdoc,tot_nrodoc,tot_nropla"
            '    cTablas += " inner join totales on tot_tipdoc=afi_tipdoc and tot_nrodoc=afi_nrodoc"
            cTablas = "((totales inner join plancuen on pla_nropla=tot_nropla) inner join afiliado on tot_tipdoc=afi_tipdoc and tot_nrodoc=afi_nrodoc) inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula"

            cColumnas += ",tot_nropla,pla_nombre,sum(tot_debe-tot_haber) as Saldo"

            '    cfiltro += " and tot_fecha<='" & Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " 23:59:59' AND tot_estado<>'9' group by tot_tipdoc,tot_nrodoc,tot_nropla"
        Else
            cTablas = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
            cfiltro = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        End If

        Try
            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTPadron = New DataTable
            DAListado.Fill(DTPadron)

            If ComboBox1.Text.Trim <> "" Then

                For i As Integer = DTPadron.Rows.Count - 1 To 0 Step -1
                    If oSC.Eval(DTPadron.Rows(i).Item("Saldo").ToString.Replace(",", ".") & ComboBox1.Text & UCESaldo.Value.ToString.Replace(",", ".")) Then
                    Else
                        DTPadron.Rows(i).Delete()
                    End If
                Next

            End If
            UGlistados.DataSource = DTPadron
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Se Produjo en error, controle los datos ingrasados")
        End Try



    End Sub

    Private Sub CBtListar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtListar.ClickButtonArea
        CBtAcualizar.Text = "Volver a Listados"
        If CheckBox1.Checked Then
            ListadoMorosos()
        Else
            ArmarConsulta()
        End If

    End Sub




    Private Sub CBtExportar_ClickButtonArea(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.ClickButtonArea
        clsExportarExcel.ExportarDatosExcel(UGlistados, DTListado.Rows(BSListado.Position).Item("lis_titulo"))
    End Sub


    Private Sub ListViewAfil_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles ListViewAfil.ItemCheck

        If e.CurrentValue = CheckState.Unchecked Then
            nOrdenSeleccion += 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = nOrdenSeleccion
        Else
            nOrdenSeleccion -= 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = 0
        End If
    End Sub

    Private Sub txCuenta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txCuenta.KeyPress
        Tabular(e)
    End Sub


    Private Sub UCESaldo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UCESaldo.KeyPress
        Tabular(e)
    End Sub

    Private Sub UltraDateTimeEditor1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UDThasta.KeyPress
        Tabular(e)
    End Sub

    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        Tabular(e)
    End Sub


    Private Sub ListadoMorosos()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        Dim cTablas As String = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cGrupBy As String = ""
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For I As Integer = 1 To nOrdenSeleccion

            For Each item As ListViewItem In ListViewAfil.Items

                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next
        cColumnas += ",afi_tipdoc,afi_nrodoc,0 as cuotas,afi_debitos as importe,afi_debitos as total,afi_debitos as interes,0 as CantCuotas,afi_debitos as TotCuotas,afi_debitos as TotIntereses,afi_debitos as CapInv,afi_debitos as FonComp,afi_debitos as GasAdm,afi_debitos as ServSoc"
        If txCuenta.TextLength > 0 Then
            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTAfiliado = New DataTable
            DAListado.Fill(DTAfiliado)
            'Dim columns(2) As DataColumn
            'columns(0) = DTAfiliado.Columns("afi_tipdoc")
            'columns(1) = DTAfiliado.Columns("afi_nrodoc")
            'columns(2) = DTAfiliado.Columns("importe")
            'DTAfiliado.PrimaryKey = columns

            DTAfiliadoClone = DTAfiliado.Clone

            Dim nCantCuo As Integer = 0
            Dim nTotCantCuo As Integer = 0
            Dim nMonCuo As Double = 0
            Dim nMonInteres As Double = 0
            Dim nTotMonCuo As Double = 0
            Dim nTotMonIntereses As Double = 0
            Dim nRegCuo As Integer = 0
            For Each RowAfi As DataRow In DTAfiliado.Rows
                DTMovimientos = cuotas.CuotasServicioImpagas(txCuenta.Text, RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True)
                For Each rowMov As DataRow In DTMovimientos.Rows
                    If CDate(rowMov.Item("vencimiento").ToString) < UDThasta.Value Then

                        nRegCuo += 1
                        If nMonCuo <> rowMov.Item("debe") - rowMov.Item("imppagado") Then
                            If nMonCuo > 0 Then
                                RowAfi.Item("cuotas") = nCantCuo
                                RowAfi.Item("importe") = nMonCuo
                                RowAfi.Item("total") = nMonCuo * nCantCuo
                                RowAfi.Item("interes") = nMonInteres

                                If txCuenta.Text = "13040100" Then
                                    Select Case nMonCuo
                                        Case 100
                                            RowAfi.Item("CapInv") = 62.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 131
                                            RowAfi.Item("CapInv") = 93.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 212
                                            RowAfi.Item("CapInv") = 174.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 110
                                            RowAfi.Item("CapInv") = 68.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 145
                                            RowAfi.Item("CapInv") = 103.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 235
                                            RowAfi.Item("CapInv") = 193.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 130
                                            RowAfi.Item("CapInv") = 82.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 170
                                            RowAfi.Item("CapInv") = 122.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 270
                                            RowAfi.Item("CapInv") = 222.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 155
                                            RowAfi.Item("CapInv") = 98.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 202
                                            RowAfi.Item("CapInv") = 145.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 320
                                            RowAfi.Item("CapInv") = 263.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 194
                                            RowAfi.Item("CapInv") = 123.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 253
                                            RowAfi.Item("CapInv") = 182.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 400
                                            RowAfi.Item("CapInv") = 329.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 75
                                            RowAfi.Item("CapInv") = 37.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 85
                                            RowAfi.Item("CapInv") = 43.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 105
                                            RowAfi.Item("CapInv") = 57.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 125
                                            RowAfi.Item("CapInv") = 68.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 157
                                            RowAfi.Item("CapInv") = 86.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo

                                    End Select
                                End If

                                DTAfiliadoClone.ImportRow(RowAfi)
                            End If
                            nTotMonCuo += nMonCuo * nCantCuo
                            nTotMonIntereses += nMonInteres
                            nMonCuo = rowMov.Item("debe") - rowMov.Item("imppagado")
                            nMonInteres = rowMov.Item("interes")
                            nTotCantCuo += nCantCuo
                            nCantCuo = 1
                        Else
                            nCantCuo += 1
                            nMonInteres += rowMov.Item("interes")
                        End If
                    End If
                Next
                '   If nMonCuo > 0 Then
                nTotMonCuo += nMonCuo * nCantCuo
                nTotMonIntereses += nMonInteres
                nTotCantCuo += nCantCuo
                RowAfi.Item("cuotas") = nCantCuo
                RowAfi.Item("importe") = nMonCuo
                RowAfi.Item("total") = nMonCuo * nCantCuo
                RowAfi.Item("interes") = nMonInteres
                RowAfi.Item("cantcuotas") = nTotCantCuo
                RowAfi.Item("totcuotas") = nTotMonCuo
                RowAfi.Item("totintereses") = nTotMonIntereses
                DTAfiliadoClone.ImportRow(RowAfi)

                ' End If
                nMonCuo = 0
                nCantCuo = 0
                nTotCantCuo = 0
                nMonInteres = 0
                nTotMonCuo = 0
                nTotMonIntereses = 0
                nRegCuo = 0
            Next

            If ComboBox1.Text.Trim <> "" Then

                For i As Integer = DTAfiliadoClone.Rows.Count - 1 To 0 Step -1
                    If oSC.Eval(DTAfiliadoClone.Rows(i).Item("total").ToString.Replace(",", ".") & ComboBox1.Text & UCESaldo.Value.ToString.Replace(",", ".")) Then
                    Else
                        DTAfiliadoClone.Rows(i).Delete()
                    End If
                Next

            End If

            UGlistados.DataSource = DTAfiliadoClone
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 2).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"

            End With
        End If

    End Sub

    Private Sub BSListado_PositionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BSListado.PositionChanged
        txCuenta.Text = DTListado.Rows(BSListado.Position).Item("lis_cuentas")
    End Sub
End Class