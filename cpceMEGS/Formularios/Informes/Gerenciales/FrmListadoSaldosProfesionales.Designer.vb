﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadoSaldosProfesionales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListadoSaldosProfesionales))
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.UGlistados = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.CBtListar = New System.Windows.Forms.Button()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2014, 6, 3, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(110, 11)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 138
        Me.UDThasta.Value = New Date(2014, 6, 3, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 139
        Me.Label3.Text = "Hasta Fecha:"
        '
        'UGlistados
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGlistados.DisplayLayout.Appearance = Appearance1
        Me.UGlistados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGlistados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGlistados.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGlistados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGlistados.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGlistados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGlistados.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UGlistados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGlistados.Location = New System.Drawing.Point(0, 69)
        Me.UGlistados.Name = "UGlistados"
        Me.UGlistados.Size = New System.Drawing.Size(812, 360)
        Me.UGlistados.TabIndex = 145
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(451, 3)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(187, 38)
        Me.CBtExportar.TabIndex = 147
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.ImageIndex = 0
        Me.CBtListar.Location = New System.Drawing.Point(239, 3)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.Size = New System.Drawing.Size(187, 38)
        Me.CBtListar.TabIndex = 146
        Me.CBtListar.Text = "Listar"
        Me.CBtListar.UseVisualStyleBackColor = False
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(0, 43)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(812, 20)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 148
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'FrmListadoSaldosProfesionales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(812, 429)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UGlistados)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmListadoSaldosProfesionales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conciliacion Deudores Aportes"
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UGlistados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents CBtListar As System.Windows.Forms.Button
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
End Class
