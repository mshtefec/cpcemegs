﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl
Public Class FrmDeudoresDEPActivosSeguro
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel
    Private permiteEditarListado As Boolean

    Private Sub CargaListViewAfiliado()
        DAListado = cnn.consultaBDadapter(
            "afiliado inner join cuentas on cue_tipdoc=afi_tipdoc and cue_nrodoc=afi_nrodoc", ,
            "afi_nrodoc=999999999"
        )
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim objListItem As New ListViewItem

        Me.ListViewAfil.Items.Clear()
        ListViewAfil.Columns.Add("")
        ListViewAfil.Columns.Add("Campo")
        ListViewAfil.Columns.Add("Orden")
        'Si no tiene Permiso U = Edit Agrego solo los campos del Public camposListadoSelect
        If controlAcceso(7, 9, "U", False) = False Then
            permiteEditarListado = False
            For Each colCaption As String In ModPrincipal.camposListadoSelect
                objListItem = New ListViewItem()
                objListItem.SubItems.Add(colCaption)
                objListItem.SubItems.Add(0)
                ListViewAfil.Items.Add(objListItem)
            Next
        Else 'Si tiene permisos Agrego todos los campos
            permiteEditarListado = True
            For Each col As DataColumn In DTAfiliado.Columns
                objListItem = New ListViewItem()
                objListItem.SubItems.Add(col.Caption)
                objListItem.SubItems.Add(0)
                ListViewAfil.Items.Add(objListItem)
            Next
        End If
        ListViewAfil.Columns(0).Width = 20
        ListViewAfil.Columns(1).Width = 150
    End Sub

    Private Sub ListadoMorosos(Optional ByVal seguro As Boolean = False, Optional ByVal activos As Boolean = False)
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String
        Dim cTablas As String
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cGrupBy As String = ""
        Dim cColumnas As String = "0 as Cantidad"
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"
        Dim cantidadCuotas As Integer = Integer.Parse(TBCantidadCuotas.Text())

        cfiltro = "afi_tipo = 'A' AND afi_tipdoc <> 'ECO' AND afi_matricula > 0"

        If seguro Then
            cfiltro = cfiltro & " AND mid(afi_categoria,1,2) IN ('11','12','17') ORDER BY afi_nrodoc ASC"
            cTablas = "(afiliado left join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
            cColumnas += ",afi_nombre,afi_nrodoc,afi_fecnac,0 as Edad,afi_tipdoc,afi_titulo,afi_matricula"
        Else
            If activos Then
                cfiltro = cfiltro & " AND mid(afi_categoria,1,2) IN ('11','12','17','21') ORDER BY afi_matricula ASC"
            Else
                cfiltro = cfiltro & " AND afi_categoria <> '220100' ORDER BY afi_matricula ASC"
            End If
            'AND mid(afi_categoria,1,2) IN ('11','12','17','20','21','22')
            cTablas = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
            cColumnas += ",afi_tipdoc,afi_nrodoc,afi_fecnac,0 as Edad"
            For I As Integer = 1 To nOrdenSeleccion
                For Each item As ListViewItem In ListViewAfil.Items
                    If item.Checked And CInt(item.SubItems(2).Text) = I Then
                        If cColumnas.Trim <> "" Then
                            cColumnas += ","
                        End If
                        cColumnas += item.SubItems(1).Text
                    End If
                Next
            Next
            'cColumnas += ",0 as cuotasDEP,afi_debitos as DeudaDEP,0 as cuotasDA,afi_debitos as DeudaDA,0 as cuotasAportes,afi_debitos as DeudaAportes,0 as cuotasJub,afi_debitos as DeudaJub"

            If cColumnas = "" Then
                MessageBox.Show("Debe seleccionar los datos del profesional")
                Exit Sub
            End If
        End If

        Dim nCantidadAfiliado As Integer = 0
        Dim nCantCuo As Integer = 0
        Dim nTotCantCuo As Integer = 0
        Dim nMonCuo As Double = 0
        Dim nMonInteres As Double = 0
        Dim nTotMonCuo As Double = 0
        Dim nTotMonIntereses As Double = 0
        Dim nRegCuo As Integer = 0
        Dim nMontos(3, 2) As Double
        Dim cCuentas(3, 3) As String
        Dim agregarAfiliado As Boolean

        Dim cantidadDeCuentas As Integer = 0
        For Each nroCuenta As String In LBCuentas.SelectedItems
            Select Case nroCuenta
                Case 13010200
                    cCuentas(cantidadDeCuentas, 1) = "13010200"
                    cCuentas(cantidadDeCuentas, 2) = "cuotasDEP"
                    cCuentas(cantidadDeCuentas, 3) = "DeudaDEP"
                    cColumnas += ",0 as cuotasDEP,afi_debitos as DeudaDEP"
                Case 13010600
                    cCuentas(cantidadDeCuentas, 1) = "13010600"
                    cCuentas(cantidadDeCuentas, 2) = "cuotasDA"
                    cCuentas(cantidadDeCuentas, 3) = "DeudaDA"
                    cColumnas += ",0 as cuotasDA,afi_debitos as DeudaDA"
                Case 13040100
                    cCuentas(cantidadDeCuentas, 1) = "13040100"
                    cCuentas(cantidadDeCuentas, 2) = "cuotasAportes"
                    cCuentas(cantidadDeCuentas, 3) = "DeudaAportes"
                    cColumnas += ",0 as cuotasAportes,afi_debitos as DeudaAportes"
                Case 13040300
                    cCuentas(cantidadDeCuentas, 1) = "13040300"
                    cCuentas(cantidadDeCuentas, 2) = "cuotasJub"
                    cCuentas(cantidadDeCuentas, 3) = "DeudaJub"
                    cColumnas += ",0 as cuotasJub,afi_debitos as DeudaJub"
                Case Else
                    MessageBox.Show("Debe seleccionar al menos una cuenta")
                    Exit Sub
            End Select
            cantidadDeCuentas += 1
        Next
        cantidadDeCuentas -= 1
        'cCuentas(3, 1) = "22010100"
        'cCuentas(3, 2) = "CantidadACI"
        'cCuentas(3, 3) = "TotalACI"
        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)
        DTAfiliadoClone = DTAfiliado.Clone

        UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        For Each RowAfi As DataRow In DTAfiliado.Rows
            RowAfi.Item("Edad") = DateDiff(DateInterval.Month, CDate(RowAfi.Item("afi_fecnac").ToString), Now) \ 12
            agregarAfiliado = True
            Application.DoEvents()
            For x As Integer = 0 To cantidadDeCuentas
                nMontos(x, 1) = 0
                nMontos(x, 2) = 0
                DTMovimientos = cuotas.CuotasServicioImpagas(cCuentas(x, 1), RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True, Now.Date)
                nCantCuo = 0
                nMonCuo = 0
                For Each rowMov As DataRow In DTMovimientos.Rows
                    If CDate(rowMov.Item("vencimiento").ToString) < UDThasta.Value Then
                        nRegCuo += 1
                        If nMonCuo <> rowMov.Item("debe") - rowMov.Item("imppagado") Then
                            If nMonCuo > 0 Then
                                nMontos(x, 1) = nCantCuo
                                nMontos(x, 2) = nMonCuo
                            End If
                        End If
                        nMonCuo += rowMov.Item("debe") - rowMov.Item("imppagado")
                        If rowMov.Item("debe") - rowMov.Item("imppagado") > 0 Then
                            nCantCuo += 1
                        End If
                    Else
                        'nCantCuo += 1
                        nMonInteres += rowMov.Item("interes")
                    End If
                Next

                If nMonCuo > 0 Then
                    nMontos(x, 1) = nCantCuo
                    nMontos(x, 2) = nMonCuo
                End If
                'Si la cantidad de cuotas es mayor a 0 entra
                If cantidadCuotas > 0 Then
                    If seguro And RowAfi.Item("Edad") > 64 Then
                        'agregarAfiliado = True es mayor lo agrego
                        Exit For
                    ElseIf seguro And RowAfi.Item("Edad") < 65 And nCantCuo > cantidadCuotas Then
                        agregarAfiliado = False
                        Exit For
                    ElseIf nCantCuo > cantidadCuotas Then
                        agregarAfiliado = False
                        Exit For
                    End If
                End If
            Next

            If agregarAfiliado Then
                For x As Integer = 0 To cantidadDeCuentas
                    RowAfi.Item(cCuentas(x, 2)) = nMontos(x, 1)
                    RowAfi.Item(cCuentas(x, 3)) = nMontos(x, 2)
                Next
                nCantidadAfiliado += 1
                RowAfi.Item("Cantidad") = nCantidadAfiliado
                DTAfiliadoClone.ImportRow(RowAfi)
            End If

            UltraProgressBar1.Value += 1
        Next

        UGlistados.DataSource = DTAfiliadoClone

        If seguro Then
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(1).Header.Caption = "Afiliado"
                .Columns(2).Header.Caption = "DNI Nº"
                .Columns(3).Header.Caption = "Fecha Nac"
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 8).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 7).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 6).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 5).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 4).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 3).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 2).Hidden = True
                .Columns(DTAfiliadoClone.Columns.Count - 1).Hidden = True
            End With
        End If

        With UGlistados.DisplayLayout.Bands(0)
            For x As Integer = 0 To cantidadDeCuentas
                x += x + 1
                .Columns(DTAfiliadoClone.Columns.Count - x).Format = "N2"
            Next
        End With
    End Sub

    Private Sub FrmListadoDeudoresDEP_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Now
        LBCuentas.SetSelected(0, True)
        LBCuentas.SetSelected(1, True)
        LBCuentas.SetSelected(2, True)
        LBCuentas.SetSelected(3, True)
        CargaListViewAfiliado()
    End Sub

    Private Sub ListViewAfil_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles ListViewAfil.ItemCheck
        If e.CurrentValue = CheckState.Unchecked Then
            nOrdenSeleccion += 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = nOrdenSeleccion
        Else
            nOrdenSeleccion -= 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = 0
        End If
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim cListado(0) As String
        cListado(0) = "Listado Gerencial de Deudores DEP"
        'cListado(1) = "Parametro1: Deudores Aportes " & cboAporte.Text & " " & UNUCuoAportes.Value.ToString & " cuotas"
        'cListado(2) = "Parametro2: Deudores DEP " & cboDEP.Text & " " & UNEcuoDEP.Value.ToString & " cuotas"
        'cListado(3) = "Parametro3: Reintegros " & UCEreintegros.Text
        clsExportarExcel.ExportarDatosExcel(UGlistados, cListado)
    End Sub

    Private Sub CBtListar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtListar.Click
        ListadoMorosos()
    End Sub
    Private Sub ButtonSeguro_Click(sender As Object, e As EventArgs) Handles ButtonSeguro.Click
        ListadoMorosos(True)
    End Sub
    Private Sub CBActivos_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBActivos.Click
        ListadoMorosos(False, True)
    End Sub
End Class