﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core

Public Class FrmListadoDeudores
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    Dim DTAfiliadoClone As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel

    Private Sub CargaListViewAfiliado()

        DAListado = cnn.consultaBDadapter("afiliado inner join cuentas on cue_tipdoc=afi_tipdoc and cue_nrodoc=afi_nrodoc", , "afi_nrodoc=999999999")
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim objListItem As New ListViewItem

        Me.ListViewAfil.Items.Clear()
        ListViewAfil.Columns.Add("")
        ListViewAfil.Columns.Add("Campo")
        ListViewAfil.Columns.Add("Orden")
        For Each col As DataColumn In DTAfiliado.Columns

            objListItem = New ListViewItem()
            objListItem.SubItems.Add(col.Caption)
            objListItem.SubItems.Add(0)
            ListViewAfil.Items.Add(objListItem)
        Next
        ListViewAfil.Columns(0).Width = 20
        ListViewAfil.Columns(1).Width = 150
    End Sub

    Private Sub FrmListadoDeudores_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Now
        CargaListViewAfiliado()
    End Sub


    Private Sub ListViewAfil_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles ListViewAfil.ItemCheck

        If e.CurrentValue = CheckState.Unchecked Then
            nOrdenSeleccion += 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = nOrdenSeleccion
        Else
            nOrdenSeleccion -= 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = 0
        End If
    End Sub

    Private Sub ListadoMorosos()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = "afi_tipo='A' and afi_matricula >0"
        Dim cTablas As String = "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim DTMovimientos As New DataTable

        Dim cGrupBy As String = ""
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For I As Integer = 1 To nOrdenSeleccion
            For Each item As ListViewItem In ListViewAfil.Items
                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next

        If cColumnas = "" Then
            MessageBox.Show("Debe seleccionar los datos del profesional")
            Exit Sub
        End If
        cColumnas += ",afi_tipdoc,afi_nrodoc,0 as cuotasAportes,afi_debitos as DeudaAportes,0 as cuotasDEP,afi_debitos as DeudaDEP,0 as CantidadReintegro,afi_debitos as Reintegros,'' as ultimoReintegro,0 as CantidadACI,afi_debitos as TotalACI"

        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        'Dim columns(2) As DataColumn
        'columns(0) = DTAfiliado.Columns("afi_tipdoc")
        'columns(1) = DTAfiliado.Columns("afi_nrodoc")
        'columns(2) = DTAfiliado.Columns("importe")
        'DTAfiliado.PrimaryKey = columns

        DTAfiliadoClone = DTAfiliado.Clone

        Dim nCantCuo As Integer = 0
        Dim nTotCantCuo As Integer = 0
        Dim nMonCuo As Double = 0
        Dim nMonInteres As Double = 0
        Dim nTotMonCuo As Double = 0
        Dim nTotMonIntereses As Double = 0
        Dim nRegCuo As Integer = 0
        Dim nMontos(3, 2) As Double
        Dim cCuentas(3, 3) As String
        cCuentas(0, 1) = "13040100"
        cCuentas(0, 2) = "cuotasAportes"
        cCuentas(0, 3) = "DeudaAportes"
        cCuentas(1, 1) = "13010200"
        cCuentas(1, 2) = "cuotasDEP"
        cCuentas(1, 3) = "DeudaDEP"
        cCuentas(2, 1) = "21010300"
        cCuentas(2, 2) = "CantidadReintegro"
        cCuentas(2, 3) = "Reintegros"
        cCuentas(3, 1) = "22010100"
        cCuentas(3, 2) = "CantidadACI"
        cCuentas(3, 3) = "TotalACI"
        UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        For Each RowAfi As DataRow In DTAfiliado.Rows
            Application.DoEvents()
            For x As Integer = 0 To 1
                nMontos(x, 1) = 0
                nMontos(x, 2) = 0
                DTMovimientos = cuotas.CuotasServicioImpagas(cCuentas(x, 1), RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True, Now.Date)
                nCantCuo = 0
                nMonCuo = 0
                For Each rowMov As DataRow In DTMovimientos.Rows

                    If CDate(rowMov.Item("vencimiento").ToString) < UDThasta.Value Then
                        nRegCuo += 1
                        If rowMov.Item("debe") - rowMov.Item("imppagado") > 0 Then
                            nCantCuo += 1
                        End If
                        If nMonCuo <> rowMov.Item("debe") - rowMov.Item("imppagado") Then
                            If nMonCuo > 0 Then
                                nMontos(x, 1) = nCantCuo
                                nMontos(x, 2) = nMonCuo
                            End If
                        End If

                        nMonCuo += rowMov.Item("debe") - rowMov.Item("imppagado")
                    Else
                        '   nCantCuo += 1
                        nMonInteres += rowMov.Item("interes")
                    End If
                Next

                If nMonCuo > 0 Then
                    nMontos(x, 1) = nCantCuo
                    nMontos(x, 2) = nMonCuo
                End If
            Next

            nMontos(2, 1) = 0
            nMontos(2, 2) = 0
            DAListado = cnn.consultaBDadapter("totales", "tot_tipdoc,tot_nrodoc,max(tot_fecha) as fecha,count(*) as cantidad,sum(tot_debe) as debe", "tot_fecha between '" & Format(UDTdesde.Value, "yyy-MM-dd") & "' and '" & Format(UDThasta.Value, "yyyy-MM-dd") & "' and tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " and tot_nropla='" & cCuentas(2, 1) & "' and tot_debe>0 group by tot_tipdoc,tot_nrodoc")
            DTMovimientos = New DataTable
            DAListado.Fill(DTMovimientos)
            If DTMovimientos.Rows.Count > 0 Then
                nMontos(2, 1) = DTMovimientos.Rows(0).Item("cantidad")
                nMontos(2, 2) = DTMovimientos.Rows(0).Item("debe")
            End If
            'For x As Integer = 0 To 2
            '    RowAfi.Item(cCuentas(x, 2)) = nMontos(x, 1)
            '    RowAfi.Item(cCuentas(x, 3)) = nMontos(x, 2)
            'Next
            If DTMovimientos.Rows.Count > 0 Then
                RowAfi.Item("ultimoReintegro") = DTMovimientos.Rows(0).Item("fecha").ToString.Substring(0, 10)
            End If
            ' DTAfiliadoClone.ImportRow(RowAfi)

            ' aporte capitalizacion individual

            nMontos(3, 1) = 0
            nMontos(3, 2) = 0

            DAListado = cnn.consultaBDadapter("totales", "tot_tipdoc,tot_nrodoc,max(tot_fecha) as fecha,count(*) as cantidad,sum(tot_debe-tot_haber) as haber", "tot_fecha between '" & Format(UDTdesde.Value, "yyy-MM-dd") & "' and '" & Format(UDThasta.Value, "yyyy-MM-dd") & "' and tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " and tot_nropla='" & cCuentas(3, 1) & "' group by tot_tipdoc,tot_nrodoc")
            DTMovimientos = New DataTable
            DAListado.Fill(DTMovimientos)
            If DTMovimientos.Rows.Count > 0 Then
                nMontos(3, 1) = DTMovimientos.Rows(0).Item("cantidad")
                nMontos(3, 2) = DTMovimientos.Rows(0).Item("haber")
            End If
            For x As Integer = 0 To 3
                RowAfi.Item(cCuentas(x, 2)) = nMontos(x, 1)
                RowAfi.Item(cCuentas(x, 3)) = nMontos(x, 2)
            Next
            'If DTMovimientos.Rows.Count > 0 Then
            '    RowAfi.Item("ultimoReintegro") = DTMovimientos.Rows(0).Item("fecha").ToString.Substring(0, 10)
            'End If
            DTAfiliadoClone.ImportRow(RowAfi)

            UltraProgressBar1.Value += 1
        Next

        For i As Integer = DTAfiliadoClone.Rows.Count - 1 To 0 Step -1
            cfiltro = "S"
            If cboAporte.Text.Trim <> "" Then
                If oSC.Eval(DTAfiliadoClone.Rows(i).Item("cuotasAportes").ToString.Replace(",", ".") & cboAporte.Text & UNUCuoAportes.Value.ToString.Replace(",", ".")) Then
                    ' cfiltro = "N"
                Else
                    cfiltro = "N"
                End If
            End If
            If cfiltro = "S" Then
                If cboDEP.Text.Trim <> "" Then
                    If oSC.Eval(DTAfiliadoClone.Rows(i).Item("cuotasDEP").ToString.Replace(",", ".") & cboDEP.Text & UNEcuoDEP.Value.ToString.Replace(",", ".")) Then
                    Else
                        cfiltro = "N"

                    End If
                End If
            End If
            If cfiltro = "S" Then
                If UCEreintegros.Text.Trim <> "" Then
                    If UCEreintegros.Text = "SI" Then
                        If DTAfiliadoClone.Rows(i).Item("Reintegros").ToString.Replace(",", ".") > 0 Then
                        Else
                            cfiltro = "N"
                        End If
                    Else
                        If DTAfiliadoClone.Rows(i).Item("Reintegros").ToString.Replace(",", ".") = 0 Then
                        Else
                            cfiltro = "N"
                        End If
                    End If
                End If
            End If

            If cfiltro = "N" Then
                DTAfiliadoClone.Rows(i).Delete()
            End If
        Next

        UGlistados.DataSource = DTAfiliadoClone

        With UGlistados.DisplayLayout.Bands(0)
            .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 2).Format = "N2"
            .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"
        End With
    End Sub
    Private Sub ListadoMorososSipres()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String
        Dim cTablas As String
        Dim cColumnas As String
        '                        Aportes,Afiliados,Asociados,Convenios,Moratorias,Viviendas,Farmacia,Femechaco,TerrenoExHindu
        Dim cuentas As String = "13040100, 13051000, 13051100, 13051600, 13040400, 13052200, 13050900, 13050700, 13051900"
        cTablas = "afiliado JOIN totales ON afi_tipdoc = tot_tipdoc AND afi_nrodoc = tot_nrodoc AND tot_nropla IN (" & cuentas & ") AND tot_estado<>'9'"
        cfiltro = "(afi_tipo='A') AND tot_fecven between '" & Format(UDTdesde.Value, "yyy-MM-dd") & "' AND '" & Format(UDThasta.Value, "yyyy-MM-dd") & "' GROUP BY afi_tipdoc, afi_nrodoc ORDER BY afi_tipdoc, afi_nrodoc"
        cColumnas = "afi_tipdoc AS 'Tipo', afi_nrodoc AS 'Documento', afi_titulo AS Titulo, afi_matricula AS Matricula, afi_nombre as Nombre, afi_telefono1 as Telefono, afi_telefono2 as 'Telefono Alt', afi_mail as Email, afi_mail_alternativo as 'Email Alt'"
        cColumnas &= ", SUM(IF(tot_nropla = '13040100', (tot_debe - tot_haber), 0)) AS saldoAportes" 'Aportes
        'cColumnas &= ", IF(tot_nropla = '13040100', SUM(tot_debe - tot_haber), 0) AS saldoAportes" 'Aportes
        cColumnas &= ", SUM(IF(tot_nropla = '13051000', (tot_debe - tot_haber), 0)) AS saldoAfiliados" 'Afiliados
        cColumnas &= ", SUM(IF(tot_nropla = '13051100', (tot_debe - tot_haber), 0)) AS saldoAsociados" 'Asociados
        cColumnas &= ", SUM(IF(tot_nropla = '13051600', (tot_debe - tot_haber), 0)) AS saldoConvenios" 'Convenios
        cColumnas &= ", SUM(IF(tot_nropla = '13040400', (tot_debe - tot_haber), 0)) AS saldoMoratorias" 'Moratorias
        cColumnas &= ", SUM(IF(tot_nropla = '13052200', (tot_debe - tot_haber), 0)) AS saldoViviendas" 'Viviendas
        cColumnas &= ", SUM(IF(tot_nropla = '13050900', (tot_debe - tot_haber), 0)) AS saldoFarmacia" 'Farmacia
        cColumnas &= ", SUM(IF(tot_nropla = '13050700', (tot_debe - tot_haber), 0)) AS saldoFemechaco" 'Femechaco
        cColumnas &= ", SUM(IF(tot_nropla = '13051900', (tot_debe - tot_haber), 0)) AS saldoTerrenoExHindu" 'TerrenoExHindu

        Dim DTMovimientos As New DataTable

        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        UGlistados.DataSource = DTAfiliado

        With UGlistados.DisplayLayout.Bands(0)
            .Columns(DTAfiliado.Columns.Count - 9).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 8).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 7).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 6).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 5).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 4).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 3).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 2).Format = "N2"
            .Columns(DTAfiliado.Columns.Count - 1).Format = "N2"
        End With
    End Sub
    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim cListado(3) As String
        cListado(0) = "Listado Gerencial de Deudores"
        cListado(1) = "Parametro1: Deudores Aportes " & cboAporte.Text & " " & UNUCuoAportes.Value.ToString & " cuotas"
        cListado(2) = "Parametro2: Deudores DEP " & cboDEP.Text & " " & UNEcuoDEP.Value.ToString & " cuotas"
        cListado(3) = "Parametro3: Reintegros " & UCEreintegros.Text
        clsExportarExcel.ExportarDatosExcel(UGlistados, cListado)
    End Sub

    Private Sub CBtNotaReclamos_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtNotaReclamos.Click
        ImprimirReclamos()
    End Sub

    Private Sub ImprimirReclamos()
        Dim objWord As New Word.Application
        Dim Documento As New Word.Document
        Dim nombreDoc As String
        ' Dim rowReclamo As DataRow

        For Each rowReclamo As DataRow In DTAfiliadoClone.Rows

            If rowReclamo.RowState <> DataRowState.Deleted Then

                Try
                    nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Aportes " & rowReclamo.Item("afi_nombre").ToString.Trim & ".doc"
                    FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\NOTA RECLAMO DEUDA APORTES-Dr  KAPEICA-10-2014.-Final.doc", nombreDoc)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Exit Sub
                End Try

                objWord = New Word.Application
                Documento = objWord.Documents.Open(nombreDoc)

                Documento.Bookmarks.Item("NombreProfesional").Range.Text = rowReclamo.Item("afi_nombre")
                Documento.Bookmarks.Item("CalleProfesional").Range.Text = rowReclamo.Item("afi_direccion")
                Documento.Bookmarks.Item("CiudadProfesional").Range.Text = rowReclamo.Item("afi_localidad")
                Documento.Bookmarks.Item("Dia").Range.Text = Now.Day
                Documento.Bookmarks.Item("Mes").Range.Text = MonthName(Now.Month)
                Documento.Bookmarks.Item("Ano").Range.Text = Now.Year
                objWord.ActiveDocument.PrintOut()
                objWord.ActiveDocument.Close()
            End If
        Next
        MessageBox.Show("Finalizo la impresion de notas de reclamos")
    End Sub

    Private Sub CBtListar_Click(sender As Object, e As EventArgs) Handles CBtListar.Click
        ListadoMorosos()
    End Sub

    Private Sub ListarSipres_Click(sender As Object, e As EventArgs) Handles ListarSipres.Click
        ListadoMorososSipres()
    End Sub
End Class