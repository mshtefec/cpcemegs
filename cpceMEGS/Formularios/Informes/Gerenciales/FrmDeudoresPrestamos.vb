﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl
Public Class FrmDeudoresPrestamos
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim DAcuotas As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim afiliadoTieneCuotas As Boolean = False
    Dim debeCuota As Double = 0
    Dim nMesMoroso As Integer = 0
    Dim totalCantidadCuotas As Integer = 0
    Dim cantidadCuotas As Integer = 0
    Dim interesCuotas As Double = 0
    Dim globalFecPagoHasta As String
    Dim globalTotalPagos As Double = 0

    Dim clsExportarExcel As New ExportarExcel
    Private Sub FrmListadoDeudoresDEP_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Now
        CBTipo.SelectedIndex = 0
    End Sub
    Private Sub ListadoMorosos(Optional ByVal seguro As Boolean = False)
        globalFecPagoHasta = Format(UDThasta.Value(), "yyyy-MM-dd")
        Dim cuentasCuotas As New Dictionary(Of Integer, String)
        Dim selectCuenta As String = CBTipo.SelectedItem
        Select Case selectCuenta
            'Case "Aportes"
            '    cuentasCuotas.Add(13040100, "Aportes")
            Case "Afiliados"
                cuentasCuotas.Add(13051000, "Afiliados")
            Case "Asociados"
                cuentasCuotas.Add(13051100, "Asociados")
            Case "Convenios"
                cuentasCuotas.Add(13051600, "Convenios")
            Case "Moratorias"
                cuentasCuotas.Add(13040400, "Moratorias")
            Case "Femechaco"
                cuentasCuotas.Add(13050700, "Femechaco")
            Case "Farmacia"
                cuentasCuotas.Add(13050900, "Farmacia")
            Case "TerrenoExHindu"
                cuentasCuotas.Add(13051900, "TerrenoExHindu")
            Case Else
                cuentasCuotas.Add(13051000, "Afiliados")
        End Select
        Dim cfiltro As String
        Dim cTablas As String
        'Dim cGrupBy As String = ""
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        If selectCuenta = "Moratorias" Then
            'Si es Moratoria
            cTablas = "afiliado JOIN totales ON afi_tipdoc = tot_tipdoc AND afi_nrodoc = tot_nrodoc AND tot_nropla = 13040400 AND tot_estado<>'9' AND tot_debe > 0"
        Else
            Dim cuenta As Integer = cuentasCuotas.Keys(0)
            cTablas = "afiliado JOIN totales ON afi_tipdoc = tot_tipdoc AND afi_nrodoc = tot_nrodoc AND tot_nropla = " & cuenta & " AND tot_estado<>'9' AND tot_debe > 0"
        End If
        cfiltro = "(afi_tipo='A' OR afi_tipo='E') GROUP BY afi_tipdoc, afi_nrodoc, tot_nroasi ORDER BY afi_tipdoc, afi_nrodoc, tot_fecha DESC"
        cColumnas = "afi_tipdoc AS 'Tipo', afi_nrodoc AS 'Documento', afi_titulo AS Titulo, afi_matricula AS Matricula, afi_nombre as Nombre, afi_telefono1 as Telefono, afi_telefono2 as 'Telefono Alt', afi_mail as Email, afi_mail_alternativo as 'Email Alt', afi_direccion AS Direccion, afi_localidad AS Localidad, afi_provincia AS Provincia, tot_nroasi AS NroAsiento, tot_fecha AS Fecha, SUM(tot_debe) AS totalDebe"
        'cColumnas = "NUMERO AS 'Nro Prestamo', afi_titulo AS Titulo, afi_matricula AS Matricula, afi_nombre as Nombre, afi_telefono1 as Telefono, afi_telefono2 as 'Telefono Alt', afi_mail as Email, afi_mail_alternativo as 'Email Alt', COUNT(NUMERO) AS 'Cantidad Cuotas', SUM(TOTAL) AS 'Total Deuda'"
        DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)
        'cTablas = "cuo_det INNER JOIN afiliado ON MATRICULA = afi_matricula AND afi_tipo = 'A'"
        'cfiltro = "F_VTO <= '" & UDThasta.DateTime.Year & "-" & UDThasta.DateTime.Month & "-" & UDThasta.DateTime.Day & "' AND F_PAGO IS NULL GROUP BY NUMERO ORDER BY NUMERO"
        'cColumnas = "NUMERO AS 'Nro Prestamo', afi_titulo AS Titulo, afi_matricula AS Matricula, afi_nombre as Nombre, afi_telefono1 as Telefono, afi_telefono2 as 'Telefono Alt', afi_mail as Email, afi_mail_alternativo as 'Email Alt', COUNT(NUMERO) AS 'Cantidad Cuotas', SUM(TOTAL) AS 'Total Deuda'"
        DTAfiliado.Columns.Add("TotalPagado", GetType(Double))
        DTAfiliado.Columns.Add("TotalCuotas", GetType(Integer))
        DTAfiliado.Columns.Add("Monto", GetType(Double))
        DTAfiliado.Columns.Add("Cuotas", GetType(Integer))
        DTAfiliado.Columns.Add("Debe", GetType(Double))

        If selectCuenta <> "Moratorias" Then
            DTAfiliado.Columns.Add("Interes", GetType(Double))
        End If

        DTAfiliado.Columns.Add("Total", GetType(Double))

        Dim afiliado As DataRow
        Dim afiliadoActual As String = Nothing
        Dim afiliadoAnterior As String = Nothing
        Dim totalAfiliados As Integer = DTAfiliado.Rows.Count - 1

        UltraProgressBar1.Maximum = totalAfiliados + 1
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Value = 0
        globalTotalPagos = 0

        For nAfiliado As Integer = totalAfiliados To 0 Step -1
            Application.DoEvents()
            afiliado = DTAfiliado.Rows(nAfiliado)
            'Guardo el afiliado actual para compararlo con el afiliado anterior
            afiliadoActual = afiliado.Item("Tipo") & afiliado.Item("Documento")
            afiliadoTieneCuotas = False
            For Each Cuenta As KeyValuePair(Of Integer, String) In cuentasCuotas
                If selectCuenta <> "Moratorias" Then
                    afiliado.Item("Interes") = 0
                End If

                afiliado.Item("TotalPagado") = 0
                afiliado.Item("TotalCuotas") = 0
                afiliado.Item("Monto") = 0
                afiliado.Item("Cuotas") = 0
                afiliado.Item("Debe") = 0
                afiliado.Item("Total") = 0

                debeCuota = 0
                nMesMoroso = 0
                totalCantidadCuotas = 0
                cantidadCuotas = 0
                interesCuotas = 0

                If IsNothing(afiliadoAnterior) Then
                    'Si es el primer afiliado entra y busca el total de pagos
                    globalTotalPagos = totalPagos(Cuenta.Key, afiliado.Item("Tipo"), afiliado.Item("Documento"))
                ElseIf afiliadoActual <> afiliadoAnterior Then
                    'Si no es el primer afiliado controla con el anterior que no sea el mismo
                    'Si no son el mismo buscar nuevamente el total de pagos sino lo mantiene
                    globalTotalPagos = totalPagos(Cuenta.Key, afiliado.Item("Tipo"), afiliado.Item("Documento"))
                End If
                afiliado.Item("TotalPagado") = globalTotalPagos
                If selectCuenta = "Moratorias" Then
                    'Si es Moratoria
                    CalculaCuotasAfiliadoMoratoria(afiliado, Cuenta, afiliado.Item("Tipo"), afiliado.Item("Documento"))
                Else
                    'globalTotalPagos = totalPagos(Cuenta.Key, afiliado.Item("Tipo"), afiliado.Item("Documento"))
                    CalculaCuotasAfiliado(afiliado, Cuenta, afiliado.Item("Tipo"), afiliado.Item("Documento"))
                End If
            Next
            'Si Traer No Deudores entra
            If CBTraerNoDeudores.Checked Then
                'Por defecto que tiene cuotas en true para mostrarlo por mas que no debe
                'afiliadoTieneCuotas = True
                If IsNothing(afiliadoAnterior) Then
                    'Si es el primer afiliado no lo borra
                ElseIf afiliadoActual = afiliadoAnterior Then
                    'Si no es el primer afiliado controla con el anterior que sea el mismo
                    'Si es el mismo lo borra
                    DTAfiliado.Rows.RemoveAt(nAfiliado)
                End If
            ElseIf afiliadoTieneCuotas = False Then
                DTAfiliado.Rows.RemoveAt(nAfiliado)
            End If
            'Guardo el afiliado anterior
            afiliadoAnterior = afiliadoActual
            UltraProgressBar1.Value += 1
        Next

        UGlistados.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.VisibleIndex

        UGlistados.DataSource = DTAfiliado
        If selectCuenta = "Moratorias" Then
            'Si es Moratoria
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(DTAfiliado.Columns.Count - 1).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 1).Header.Caption = "Total deuda"
                .Columns(DTAfiliado.Columns.Count - 2).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 2).Header.Caption = "Deuda al día"
                .Columns(DTAfiliado.Columns.Count - 3).Header.Caption = "Adeuda cuotas"
                .Columns(DTAfiliado.Columns.Count - 4).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 4).Header.Caption = "Monto cuota"
                .Columns(DTAfiliado.Columns.Count - 5).Header.Caption = "Total de cuotas"
                .Columns(DTAfiliado.Columns.Count - 6).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 6).Header.Caption = "Total Pagado"
                .Columns(DTAfiliado.Columns.Count - 7).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 7).Header.Caption = "Total moratoria"
                .Columns(DTAfiliado.Columns.Count - 8).Header.Caption = "Fecha Otorgamiento"
            End With
        Else
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(DTAfiliado.Columns.Count - 1).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 1).Header.Caption = "Total deuda"
                .Columns(DTAfiliado.Columns.Count - 2).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 2).Header.Caption = "Interés"
                .Columns(DTAfiliado.Columns.Count - 3).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 3).Header.Caption = "Deuda al día"
                .Columns(DTAfiliado.Columns.Count - 4).Header.Caption = "Adeuda cuotas"
                .Columns(DTAfiliado.Columns.Count - 5).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 5).Header.Caption = "Monto cuota"
                .Columns(DTAfiliado.Columns.Count - 6).Header.Caption = "Total de cuotas"
                .Columns(DTAfiliado.Columns.Count - 7).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 7).Header.Caption = "Total Pagado"
                .Columns(DTAfiliado.Columns.Count - 8).Format = "N2"
                .Columns(DTAfiliado.Columns.Count - 8).Header.Caption = "Total Préstamo"
                .Columns(DTAfiliado.Columns.Count - 9).Header.Caption = "Fecha Otorgamiento"
            End With
        End If
    End Sub
    Private Function totalPagos(ByVal nroCuenta As Integer, ByVal tipoDoc As String, ByVal nroDoc As Integer) As Double
        totalPagos = 0
        Dim DsPagos As New DataSet
        DAcuotas = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_haber) as pagos", "tot_nropla='" & nroCuenta & "' and tot_tipdoc='" & tipoDoc & "' and tot_nrodoc=" & nroDoc & " and tot_haber>0 and tot_estado<>'9' > 0 and tot_fecha<='" & globalFecPagoHasta & " 23:59:59' group by tot_nropla")
        DAcuotas.Fill(DsPagos, "pagos")
        If DsPagos.Tables(0).Rows.Count > 0 Then
            totalPagos = DsPagos.Tables(0).Rows(0).Item("pagos")
        End If
    End Function
    Private Sub CalculaCuotasAfiliado(ByVal afiliado As DataRow, ByVal Cuenta As KeyValuePair(Of Integer, String), ByVal cTipoDoc As String, ByVal nNroDoc As Integer)
        Dim Dscuotas As New DataSet
        Dim noSeBorroLaCuotaCancelada As Boolean = True
        Dim fechaVencimiento As String

        DAcuotas = cnn.consultaBDadapter("(totales inner join plancuen on pla_nropla=tot_nropla) left join procesos on pro_codigo=tot_proceso", "tot_nropla as Cuenta, pla_nombre as Descripcion,tot_proceso as Proceso,tot_nrocom as NroCompr,CAST(tot_fecha as date) as Fecha,tot_fecven as Vencimiento,tot_debe as Debe,tot_haber as Haber,tot_imppag as ImpPagado,0 as interes,tot_nroasi,tot_item,tot_nrocuo as nroCuota,tot_asicancel,pro_procancela as ProCancel,tot_debe as debeReal,0 as interesReal", "tot_nroasi='" & afiliado.Item("NroAsiento") & "' AND tot_nropla='" & Cuenta.Key & "' and tot_tipdoc='" & cTipoDoc & "' and tot_nrodoc=" & nNroDoc & " and tot_debe>0 and tot_estado<>'9' order by tot_fecven desc")
        DAcuotas.Fill(Dscuotas, "cuotas")

        'If afiliado.Item("matricula") = 2578 Then
        'MessageBox.Show("hola")
        'End If

        Dim rowCuo As DataRow
        For nCuo As Integer = Dscuotas.Tables(0).Rows.Count - 1 To 0 Step -1
            rowCuo = Dscuotas.Tables(0).Rows(nCuo)
            fechaVencimiento = Format(rowCuo.Item("Vencimiento").Value(), "yyyy-MM-dd")
            If fechaVencimiento <= globalFecPagoHasta Then
                If globalTotalPagos > 0 Then
                    If globalTotalPagos >= rowCuo.Item("debe") Then
                        nMesMoroso = 0
                        globalTotalPagos -= rowCuo.Item("debe")
                        'afiliado.Item("Prestamo") += rowCuo.Item("debe")
                        totalCantidadCuotas += 1
                    Else
                        rowCuo.Item("imppagado") = globalTotalPagos
                        globalTotalPagos = 0
                        totalCantidadCuotas += 1
                        cantidadCuotas += 1
                        afiliadoTieneCuotas = True
                        nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                        calcularInteres(afiliado, rowCuo, Cuenta.Key)
                    End If
                Else
                    rowCuo.Item("imppagado") = 0
                    totalCantidadCuotas += 1
                    cantidadCuotas += 1
                    afiliadoTieneCuotas = True
                    nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                    calcularInteres(afiliado, rowCuo, Cuenta.Key)
                End If
            Else
                'Sigo sumando para la totalidad del prestamo
                'afiliado.Item("Prestamo") += rowCuo.Item("debe")
                totalCantidadCuotas += 1
            End If
            If cantidadCuotas = 1 Then
                afiliado.Item("Monto") = rowCuo.Item("debe")
            End If
        Next
        afiliado.Item("Cuotas") = cantidadCuotas
        afiliado.Item("TotalCuotas") = totalCantidadCuotas
    End Sub
    Private Sub CalculaCuotasAfiliadoMoratoria(ByVal afiliado As DataRow, ByVal Cuenta As KeyValuePair(Of Integer, String), ByVal cTipoDoc As String, ByVal nNroDoc As Integer)
        Dim Dscuotas As New DataSet
        Dim noSeBorroLaCuotaCancelada As Boolean = True
        Dim fechaVencimiento As String

        DAcuotas = cnn.consultaBDadapter("(totales inner join plancuen on pla_nropla=tot_nropla) left join procesos on pro_codigo=tot_proceso", "tot_nropla as Cuenta, pla_nombre as Descripcion,tot_proceso as Proceso,tot_nrocom as NroCompr,CAST(tot_fecha as date) as Fecha,tot_fecven as Vencimiento,tot_debe as Debe,tot_haber as Haber,tot_imppag as ImpPagado,0 as interes,tot_nroasi,tot_item,tot_nrocuo as nroCuota,tot_asicancel,pro_procancela as ProCancel,tot_debe as debeReal,0 as interesReal", "tot_nroasi='" & afiliado.Item("NroAsiento") & "' AND tot_nropla='" & Cuenta.Key & "' and tot_tipdoc='" & cTipoDoc & "' and tot_nrodoc=" & nNroDoc & " and tot_debe>0 and tot_estado<>'9' order by tot_fecven desc")
        DAcuotas.Fill(Dscuotas, "cuotas")

        Dim rowCuo As DataRow
        For nCuo As Integer = Dscuotas.Tables(0).Rows.Count - 1 To 0 Step -1
            rowCuo = Dscuotas.Tables(0).Rows(nCuo)
            fechaVencimiento = Format(rowCuo.Item("Vencimiento").Value(), "yyyy-MM-dd")
            If fechaVencimiento <= globalFecPagoHasta Then
                If globalTotalPagos > 0 Then
                    If globalTotalPagos >= rowCuo.Item("debe") Then
                        nMesMoroso = 0
                        globalTotalPagos -= rowCuo.Item("debe")
                        totalCantidadCuotas += 1
                    Else
                        rowCuo.Item("imppagado") = globalTotalPagos
                        globalTotalPagos = 0
                        totalCantidadCuotas += 1
                        cantidadCuotas += 1
                        afiliadoTieneCuotas = True
                        nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                        afiliado.Item("Debe") += (rowCuo.Item("debe") - rowCuo.Item("imppagado")) 'Debe hasta la fecha
                        afiliado.Item("Total") += (rowCuo.Item("debe") - rowCuo.Item("imppagado")) 'Total que debe
                    End If
                Else
                    rowCuo.Item("imppagado") = 0
                    totalCantidadCuotas += 1
                    cantidadCuotas += 1
                    afiliadoTieneCuotas = True
                    nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                    afiliado.Item("Debe") += (rowCuo.Item("debe") - rowCuo.Item("imppagado")) 'Debe hasta la fecha
                    afiliado.Item("Total") += rowCuo.Item("debe") 'Total que debe
                End If
            Else
                'Sigo sumando para la totalidad de la deuda
                afiliado.Item("Total") += rowCuo.Item("debe")
                totalCantidadCuotas += 1
            End If
            If cantidadCuotas = 1 Then
                afiliado.Item("Monto") = rowCuo.Item("debe")
            End If
        Next
        afiliado.Item("Cuotas") = cantidadCuotas
        afiliado.Item("TotalCuotas") = totalCantidadCuotas
        'Si Traer No Deudores entra
        If CBTraerNoDeudores.Checked Then
            'Por defecto que tiene cuotas en true para mostrarlo por mas que no debe
            afiliadoTieneCuotas = True
        End If
    End Sub

    Private Sub calcularInteres(ByVal afiliado As DataRow, ByVal cuota As DataRow, ByVal cuenta As Integer)
        'INTERESES SOLO PARA DEUDORES APORTE es 1,5% mensual
        Dim porcentajeInteres As Double = 0
        If pubCuentasGeneranInteres.Contains(cuenta) Then
            'INTERESES DE 4% mensual
            If pubCuentasGeneranInteresOtros.Contains(cuenta) Then
                porcentajeInteres = pubMontosInteresSipresOtros
            Else
                porcentajeInteres = pubMontosInteresSipresAporte
            End If
        End If
        debeCuota = (cuota.Item("debe") - cuota.Item("imppagado"))
        'afiliado.Item("Prestamo") += debeCuota
        afiliado.Item("Debe") += debeCuota
        interesCuotas = (cuota.Item("debe") - cuota.Item("imppagado")) * (porcentajeInteres * nMesMoroso / 100)
        afiliado.Item("Interes") += interesCuotas
        afiliado.Item("Total") += debeCuota + interesCuotas
    End Sub

    Private Sub CBtListar_Click(sender As Object, e As EventArgs) Handles CBtListar.Click
        ListadoMorosos()
    End Sub

    Private Sub CBtExportar_Click(sender As Object, e As EventArgs) Handles CBtExportar.Click
        Dim cListado(0) As String
        cListado(0) = "Listado Gerencial de Deudores Prestamos"
        clsExportarExcel.ExportarDatosExcel(UGlistados, cListado)
    End Sub
End Class