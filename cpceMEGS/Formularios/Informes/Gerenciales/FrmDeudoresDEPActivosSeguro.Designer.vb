﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDeudoresDEPActivosSeguro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDeudoresDEPActivosSeguro))
        Me.ListViewAfil = New System.Windows.Forms.ListView()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.UGlistados = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.CBtListar = New System.Windows.Forms.Button()
        Me.ButtonSeguro = New System.Windows.Forms.Button()
        Me.LCantidadCuotas = New System.Windows.Forms.Label()
        Me.TBCantidadCuotas = New System.Windows.Forms.TextBox()
        Me.CBActivos = New System.Windows.Forms.Button()
        Me.TTDeudoresActivosSeguro = New System.Windows.Forms.ToolTip(Me.components)
        Me.LBCuentas = New System.Windows.Forms.ListBox()
        Me.LCuentas = New System.Windows.Forms.Label()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListViewAfil
        '
        Me.ListViewAfil.CheckBoxes = True
        Me.ListViewAfil.Dock = System.Windows.Forms.DockStyle.Left
        Me.ListViewAfil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewAfil.Location = New System.Drawing.Point(0, 0)
        Me.ListViewAfil.Name = "ListViewAfil"
        Me.ListViewAfil.Size = New System.Drawing.Size(269, 582)
        Me.ListViewAfil.TabIndex = 2
        Me.ListViewAfil.UseCompatibleStateImageBehavior = False
        Me.ListViewAfil.View = System.Windows.Forms.View.Details
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(369, 4)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 24)
        Me.UDTdesde.TabIndex = 143
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.UDTdesde, "No se utiliza esta fecha.")
        Me.UDTdesde.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(275, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 142
        Me.Label4.Text = "Desde Fecha:"
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2014, 6, 3, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(369, 33)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 140
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.UDThasta, "Se utiliza la fecha ingresada para controlar la fecha de vencimiento de los asien" &
        "tos.")
        Me.UDThasta.Value = New Date(2014, 6, 3, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(278, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 141
        Me.Label3.Text = "Hasta Fecha:"
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(276, 520)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(660, 20)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 149
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'UGlistados
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGlistados.DisplayLayout.Appearance = Appearance1
        Me.UGlistados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGlistados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGlistados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGlistados.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGlistados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGlistados.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGlistados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGlistados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGlistados.Location = New System.Drawing.Point(278, 62)
        Me.UGlistados.Name = "UGlistados"
        Me.UGlistados.Size = New System.Drawing.Size(660, 457)
        Me.UGlistados.TabIndex = 148
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(805, 543)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(130, 38)
        Me.CBtExportar.TabIndex = 147
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.ImageIndex = 0
        Me.CBtListar.Location = New System.Drawing.Point(276, 543)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.Size = New System.Drawing.Size(130, 38)
        Me.CBtListar.TabIndex = 146
        Me.CBtListar.Text = "Listar"
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.CBtListar, "Lista todos los matriculados menos los categoria 220100 fallecidos.")
        Me.CBtListar.UseVisualStyleBackColor = False
        '
        'ButtonSeguro
        '
        Me.ButtonSeguro.BackColor = System.Drawing.Color.Transparent
        Me.ButtonSeguro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSeguro.ImageIndex = 0
        Me.ButtonSeguro.Location = New System.Drawing.Point(548, 543)
        Me.ButtonSeguro.Name = "ButtonSeguro"
        Me.ButtonSeguro.Size = New System.Drawing.Size(130, 38)
        Me.ButtonSeguro.TabIndex = 150
        Me.ButtonSeguro.Text = "Seguro"
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.ButtonSeguro, "Genera el listado para el seguro teniendo en cuenta los matriculados" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "con categor" &
        "ias que inician con 11, 12, 17" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Tener en cuenta el campo Cantidad de Cuotas.")
        Me.ButtonSeguro.UseVisualStyleBackColor = False
        '
        'LCantidadCuotas
        '
        Me.LCantidadCuotas.AutoSize = True
        Me.LCantidadCuotas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCantidadCuotas.Location = New System.Drawing.Point(624, 8)
        Me.LCantidadCuotas.Name = "LCantidadCuotas"
        Me.LCantidadCuotas.Size = New System.Drawing.Size(129, 16)
        Me.LCantidadCuotas.TabIndex = 151
        Me.LCantidadCuotas.Text = "Cantidad de Cuotas:"
        '
        'TBCantidadCuotas
        '
        Me.TBCantidadCuotas.Location = New System.Drawing.Point(759, 5)
        Me.TBCantidadCuotas.Name = "TBCantidadCuotas"
        Me.TBCantidadCuotas.Size = New System.Drawing.Size(47, 20)
        Me.TBCantidadCuotas.TabIndex = 152
        Me.TBCantidadCuotas.Text = "3"
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.TBCantidadCuotas, "Ingresar la cantidad de cuotas maxima que puede adeudar." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Si ingresa 0 (cero) no " &
        "limita la cantidad de cuotas.")
        '
        'CBActivos
        '
        Me.CBActivos.BackColor = System.Drawing.Color.Transparent
        Me.CBActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBActivos.ImageIndex = 0
        Me.CBActivos.Location = New System.Drawing.Point(412, 543)
        Me.CBActivos.Name = "CBActivos"
        Me.CBActivos.Size = New System.Drawing.Size(130, 38)
        Me.CBActivos.TabIndex = 153
        Me.CBActivos.Text = "Activos"
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.CBActivos, "Lista todos los matriculados Activos, categorias comienzan con 11, 12, 17, 21." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "E" &
        "xcluye categorias que comienzan con 22." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CBActivos.UseVisualStyleBackColor = False
        '
        'LBCuentas
        '
        Me.LBCuentas.FormattingEnabled = True
        Me.LBCuentas.Items.AddRange(New Object() {"13010200", "13010600", "13040100", "13040300"})
        Me.LBCuentas.Location = New System.Drawing.Point(548, 4)
        Me.LBCuentas.Name = "LBCuentas"
        Me.LBCuentas.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.LBCuentas.Size = New System.Drawing.Size(70, 56)
        Me.LBCuentas.TabIndex = 154
        Me.TTDeudoresActivosSeguro.SetToolTip(Me.LBCuentas, "La cuenta seleccionada se utiliza para filtrar las cuotas" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de los asientos del ma" &
        "triculado.")
        '
        'LCuentas
        '
        Me.LCuentas.AutoSize = True
        Me.LCuentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCuentas.Location = New System.Drawing.Point(485, 9)
        Me.LCuentas.Name = "LCuentas"
        Me.LCuentas.Size = New System.Drawing.Size(57, 16)
        Me.LCuentas.TabIndex = 155
        Me.LCuentas.Text = "Cuentas"
        '
        'FrmDeudoresDEPActivosSeguro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(943, 582)
        Me.Controls.Add(Me.LCuentas)
        Me.Controls.Add(Me.LBCuentas)
        Me.Controls.Add(Me.CBActivos)
        Me.Controls.Add(Me.TBCantidadCuotas)
        Me.Controls.Add(Me.LCantidadCuotas)
        Me.Controls.Add(Me.ButtonSeguro)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.UGlistados)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ListViewAfil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmDeudoresDEPActivosSeguro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado Deudores DEP Todos | Listado Deudores Activos | Listado Habilitados para " &
    "el Seguro"
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListViewAfil As System.Windows.Forms.ListView
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents UGlistados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents CBtListar As System.Windows.Forms.Button
    Friend WithEvents ButtonSeguro As System.Windows.Forms.Button
    Friend WithEvents LCantidadCuotas As System.Windows.Forms.Label
    Friend WithEvents TBCantidadCuotas As System.Windows.Forms.TextBox
    Friend WithEvents CBActivos As System.Windows.Forms.Button
    Friend WithEvents TTDeudoresActivosSeguro As System.Windows.Forms.ToolTip
    Friend WithEvents LBCuentas As System.Windows.Forms.ListBox
    Friend WithEvents LCuentas As System.Windows.Forms.Label
End Class
