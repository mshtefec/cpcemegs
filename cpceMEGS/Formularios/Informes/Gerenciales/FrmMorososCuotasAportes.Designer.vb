﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMorososCuotasAportes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMorososCuotasAportes))
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBtListar = New System.Windows.Forms.Button()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TB1a = New System.Windows.Forms.TextBox()
        Me.TB2a = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TB2de = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TB3de = New System.Windows.Forms.TextBox()
        Me.TB3a = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TB5de = New System.Windows.Forms.TextBox()
        Me.TB5a = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TB4de = New System.Windows.Forms.TextBox()
        Me.TB4a = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TB7de = New System.Windows.Forms.TextBox()
        Me.TB7a = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TB6de = New System.Windows.Forms.TextBox()
        Me.TB6a = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TB9de = New System.Windows.Forms.TextBox()
        Me.TB9a = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TB8de = New System.Windows.Forms.TextBox()
        Me.TB8a = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TB11de = New System.Windows.Forms.TextBox()
        Me.TB11a = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TB10de = New System.Windows.Forms.TextBox()
        Me.TB10a = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.TB1de = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.UGMorososCuotasAportes = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TBCotizacionDolar = New System.Windows.Forms.TextBox()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGMorososCuotasAportes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(53, 455)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 24)
        Me.UDTdesde.TabIndex = 139
        Me.UDTdesde.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 459)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 16)
        Me.Label4.TabIndex = 138
        Me.Label4.Text = "Desde"
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2014, 6, 3, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(53, 486)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 136
        Me.UDThasta.Value = New Date(2014, 6, 3, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 490)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 16)
        Me.Label3.TabIndex = 137
        Me.Label3.Text = "Hasta"
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.ImageIndex = 0
        Me.CBtListar.Location = New System.Drawing.Point(895, 446)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.Size = New System.Drawing.Size(125, 38)
        Me.CBtListar.TabIndex = 140
        Me.CBtListar.Text = "Listar"
        Me.CBtListar.UseVisualStyleBackColor = False
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(895, 487)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(125, 38)
        Me.CBtExportar.TabIndex = 141
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(6, 422)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(1021, 20)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 145
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(185, 453)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 13)
        Me.Label1.TabIndex = 147
        Me.Label1.Text = "De"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(299, 453)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 148
        Me.Label2.Text = "cuotas"
        '
        'TB1a
        '
        Me.TB1a.Location = New System.Drawing.Point(261, 450)
        Me.TB1a.Name = "TB1a"
        Me.TB1a.Size = New System.Drawing.Size(38, 20)
        Me.TB1a.TabIndex = 149
        Me.TB1a.Text = "2"
        '
        'TB2a
        '
        Me.TB2a.Location = New System.Drawing.Point(261, 476)
        Me.TB2a.Name = "TB2a"
        Me.TB2a.Size = New System.Drawing.Size(38, 20)
        Me.TB2a.TabIndex = 152
        Me.TB2a.Text = "5"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(299, 479)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 151
        Me.Label5.Text = "cuotas"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(185, 479)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 13)
        Me.Label6.TabIndex = 150
        Me.Label6.Text = "De"
        '
        'TB2de
        '
        Me.TB2de.Location = New System.Drawing.Point(208, 476)
        Me.TB2de.Name = "TB2de"
        Me.TB2de.Size = New System.Drawing.Size(38, 20)
        Me.TB2de.TabIndex = 153
        Me.TB2de.Text = "3"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(246, 479)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(13, 13)
        Me.Label7.TabIndex = 154
        Me.Label7.Text = "a"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(246, 505)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(13, 13)
        Me.Label8.TabIndex = 159
        Me.Label8.Text = "a"
        '
        'TB3de
        '
        Me.TB3de.Location = New System.Drawing.Point(208, 502)
        Me.TB3de.Name = "TB3de"
        Me.TB3de.Size = New System.Drawing.Size(38, 20)
        Me.TB3de.TabIndex = 158
        Me.TB3de.Text = "6"
        '
        'TB3a
        '
        Me.TB3a.Location = New System.Drawing.Point(261, 502)
        Me.TB3a.Name = "TB3a"
        Me.TB3a.Size = New System.Drawing.Size(38, 20)
        Me.TB3a.TabIndex = 157
        Me.TB3a.Text = "10"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(299, 505)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 13)
        Me.Label9.TabIndex = 156
        Me.Label9.Text = "cuotas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(185, 505)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(21, 13)
        Me.Label10.TabIndex = 155
        Me.Label10.Text = "De"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(424, 479)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(13, 13)
        Me.Label11.TabIndex = 169
        Me.Label11.Text = "a"
        '
        'TB5de
        '
        Me.TB5de.Location = New System.Drawing.Point(386, 476)
        Me.TB5de.Name = "TB5de"
        Me.TB5de.Size = New System.Drawing.Size(38, 20)
        Me.TB5de.TabIndex = 168
        Me.TB5de.Text = "21"
        '
        'TB5a
        '
        Me.TB5a.Location = New System.Drawing.Point(439, 476)
        Me.TB5a.Name = "TB5a"
        Me.TB5a.Size = New System.Drawing.Size(38, 20)
        Me.TB5a.TabIndex = 167
        Me.TB5a.Text = "30"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(477, 479)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 13)
        Me.Label12.TabIndex = 166
        Me.Label12.Text = "cuotas"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(363, 479)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(21, 13)
        Me.Label13.TabIndex = 165
        Me.Label13.Text = "De"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(424, 453)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(13, 13)
        Me.Label14.TabIndex = 164
        Me.Label14.Text = "a"
        '
        'TB4de
        '
        Me.TB4de.Location = New System.Drawing.Point(386, 450)
        Me.TB4de.Name = "TB4de"
        Me.TB4de.Size = New System.Drawing.Size(38, 20)
        Me.TB4de.TabIndex = 163
        Me.TB4de.Text = "11"
        '
        'TB4a
        '
        Me.TB4a.Location = New System.Drawing.Point(439, 450)
        Me.TB4a.Name = "TB4a"
        Me.TB4a.Size = New System.Drawing.Size(38, 20)
        Me.TB4a.TabIndex = 162
        Me.TB4a.Text = "20"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(477, 453)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(39, 13)
        Me.Label15.TabIndex = 161
        Me.Label15.Text = "cuotas"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(363, 453)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(21, 13)
        Me.Label16.TabIndex = 160
        Me.Label16.Text = "De"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(603, 453)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(13, 13)
        Me.Label17.TabIndex = 179
        Me.Label17.Text = "a"
        '
        'TB7de
        '
        Me.TB7de.Location = New System.Drawing.Point(565, 450)
        Me.TB7de.Name = "TB7de"
        Me.TB7de.Size = New System.Drawing.Size(38, 20)
        Me.TB7de.TabIndex = 178
        Me.TB7de.Text = "51"
        '
        'TB7a
        '
        Me.TB7a.Location = New System.Drawing.Point(618, 450)
        Me.TB7a.Name = "TB7a"
        Me.TB7a.Size = New System.Drawing.Size(38, 20)
        Me.TB7a.TabIndex = 177
        Me.TB7a.Text = "80"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(656, 453)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(39, 13)
        Me.Label18.TabIndex = 176
        Me.Label18.Text = "cuotas"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(542, 453)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(21, 13)
        Me.Label19.TabIndex = 175
        Me.Label19.Text = "De"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(424, 506)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(13, 13)
        Me.Label20.TabIndex = 174
        Me.Label20.Text = "a"
        '
        'TB6de
        '
        Me.TB6de.Location = New System.Drawing.Point(386, 503)
        Me.TB6de.Name = "TB6de"
        Me.TB6de.Size = New System.Drawing.Size(38, 20)
        Me.TB6de.TabIndex = 173
        Me.TB6de.Text = "31"
        '
        'TB6a
        '
        Me.TB6a.Location = New System.Drawing.Point(439, 503)
        Me.TB6a.Name = "TB6a"
        Me.TB6a.Size = New System.Drawing.Size(38, 20)
        Me.TB6a.TabIndex = 172
        Me.TB6a.Text = "50"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(477, 506)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(39, 13)
        Me.Label21.TabIndex = 171
        Me.Label21.Text = "cuotas"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(363, 506)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(21, 13)
        Me.Label22.TabIndex = 170
        Me.Label22.Text = "De"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(603, 506)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(13, 13)
        Me.Label23.TabIndex = 189
        Me.Label23.Text = "a"
        '
        'TB9de
        '
        Me.TB9de.Location = New System.Drawing.Point(565, 503)
        Me.TB9de.Name = "TB9de"
        Me.TB9de.Size = New System.Drawing.Size(38, 20)
        Me.TB9de.TabIndex = 188
        Me.TB9de.Text = "101"
        '
        'TB9a
        '
        Me.TB9a.Location = New System.Drawing.Point(618, 503)
        Me.TB9a.Name = "TB9a"
        Me.TB9a.Size = New System.Drawing.Size(38, 20)
        Me.TB9a.TabIndex = 187
        Me.TB9a.Text = "120"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(656, 506)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(39, 13)
        Me.Label24.TabIndex = 186
        Me.Label24.Text = "cuotas"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(542, 506)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(21, 13)
        Me.Label25.TabIndex = 185
        Me.Label25.Text = "De"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(603, 480)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(13, 13)
        Me.Label26.TabIndex = 184
        Me.Label26.Text = "a"
        '
        'TB8de
        '
        Me.TB8de.Location = New System.Drawing.Point(565, 477)
        Me.TB8de.Name = "TB8de"
        Me.TB8de.Size = New System.Drawing.Size(38, 20)
        Me.TB8de.TabIndex = 183
        Me.TB8de.Text = "81"
        '
        'TB8a
        '
        Me.TB8a.Location = New System.Drawing.Point(618, 477)
        Me.TB8a.Name = "TB8a"
        Me.TB8a.Size = New System.Drawing.Size(38, 20)
        Me.TB8a.TabIndex = 182
        Me.TB8a.Text = "100"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(656, 480)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(39, 13)
        Me.Label27.TabIndex = 181
        Me.Label27.Text = "cuotas"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(542, 480)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(21, 13)
        Me.Label28.TabIndex = 180
        Me.Label28.Text = "De"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(779, 479)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(13, 13)
        Me.Label29.TabIndex = 199
        Me.Label29.Text = "a"
        '
        'TB11de
        '
        Me.TB11de.Location = New System.Drawing.Point(741, 476)
        Me.TB11de.Name = "TB11de"
        Me.TB11de.Size = New System.Drawing.Size(38, 20)
        Me.TB11de.TabIndex = 198
        Me.TB11de.Text = "251"
        '
        'TB11a
        '
        Me.TB11a.Location = New System.Drawing.Point(794, 476)
        Me.TB11a.Name = "TB11a"
        Me.TB11a.Size = New System.Drawing.Size(38, 20)
        Me.TB11a.TabIndex = 197
        Me.TB11a.Text = "1000"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(832, 479)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(39, 13)
        Me.Label30.TabIndex = 196
        Me.Label30.Text = "cuotas"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(718, 479)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(21, 13)
        Me.Label31.TabIndex = 195
        Me.Label31.Text = "De"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(779, 453)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(13, 13)
        Me.Label32.TabIndex = 194
        Me.Label32.Text = "a"
        '
        'TB10de
        '
        Me.TB10de.Location = New System.Drawing.Point(741, 450)
        Me.TB10de.Name = "TB10de"
        Me.TB10de.Size = New System.Drawing.Size(38, 20)
        Me.TB10de.TabIndex = 193
        Me.TB10de.Text = "121"
        '
        'TB10a
        '
        Me.TB10a.Location = New System.Drawing.Point(794, 450)
        Me.TB10a.Name = "TB10a"
        Me.TB10a.Size = New System.Drawing.Size(38, 20)
        Me.TB10a.TabIndex = 192
        Me.TB10a.Text = "250"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(832, 453)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(39, 13)
        Me.Label33.TabIndex = 191
        Me.Label33.Text = "cuotas"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(718, 453)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(21, 13)
        Me.Label34.TabIndex = 190
        Me.Label34.Text = "De"
        '
        'TB1de
        '
        Me.TB1de.Enabled = False
        Me.TB1de.Location = New System.Drawing.Point(208, 450)
        Me.TB1de.Name = "TB1de"
        Me.TB1de.Size = New System.Drawing.Size(38, 20)
        Me.TB1de.TabIndex = 200
        Me.TB1de.Text = "1"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(246, 453)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(13, 13)
        Me.Label35.TabIndex = 201
        Me.Label35.Text = "a"
        '
        'UGMorososCuotasAportes
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGMorososCuotasAportes.DisplayLayout.Appearance = Appearance1
        Me.UGMorososCuotasAportes.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGMorososCuotasAportes.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGMorososCuotasAportes.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGMorososCuotasAportes.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGMorososCuotasAportes.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGMorososCuotasAportes.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGMorososCuotasAportes.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGMorososCuotasAportes.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMorososCuotasAportes.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMorososCuotasAportes.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGMorososCuotasAportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGMorososCuotasAportes.Location = New System.Drawing.Point(6, 6)
        Me.UGMorososCuotasAportes.Name = "UGMorososCuotasAportes"
        Me.UGMorososCuotasAportes.Size = New System.Drawing.Size(1021, 410)
        Me.UGMorososCuotasAportes.TabIndex = 202
        Me.UGMorososCuotasAportes.Text = "Listado de Morosos Cuotas Aportes"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(718, 508)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(82, 13)
        Me.Label36.TabIndex = 203
        Me.Label36.Text = "Cotización dolar"
        '
        'TBCotizacionDolar
        '
        Me.TBCotizacionDolar.Location = New System.Drawing.Point(804, 505)
        Me.TBCotizacionDolar.Name = "TBCotizacionDolar"
        Me.TBCotizacionDolar.Size = New System.Drawing.Size(38, 20)
        Me.TBCotizacionDolar.TabIndex = 204
        Me.TBCotizacionDolar.Text = "1"
        '
        'FrmMorososCuotasAportes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1032, 526)
        Me.Controls.Add(Me.TBCotizacionDolar)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.UGMorososCuotasAportes)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.TB1de)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.TB11de)
        Me.Controls.Add(Me.TB11a)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.TB10de)
        Me.Controls.Add(Me.TB10a)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.TB9de)
        Me.Controls.Add(Me.TB9a)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.TB8de)
        Me.Controls.Add(Me.TB8a)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TB7de)
        Me.Controls.Add(Me.TB7a)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.TB6de)
        Me.Controls.Add(Me.TB6a)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TB5de)
        Me.Controls.Add(Me.TB5a)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TB4de)
        Me.Controls.Add(Me.TB4a)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TB3de)
        Me.Controls.Add(Me.TB3a)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TB2de)
        Me.Controls.Add(Me.TB2a)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TB1a)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMorososCuotasAportes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de Morosos Cuotas Aportes"
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGMorososCuotasAportes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBtListar As System.Windows.Forms.Button
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TB1a As System.Windows.Forms.TextBox
    Friend WithEvents TB2a As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TB2de As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TB3de As System.Windows.Forms.TextBox
    Friend WithEvents TB3a As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TB5de As System.Windows.Forms.TextBox
    Friend WithEvents TB5a As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TB4de As System.Windows.Forms.TextBox
    Friend WithEvents TB4a As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TB7de As System.Windows.Forms.TextBox
    Friend WithEvents TB7a As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TB6de As System.Windows.Forms.TextBox
    Friend WithEvents TB6a As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TB9de As System.Windows.Forms.TextBox
    Friend WithEvents TB9a As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TB8de As System.Windows.Forms.TextBox
    Friend WithEvents TB8a As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TB11de As System.Windows.Forms.TextBox
    Friend WithEvents TB11a As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TB10de As System.Windows.Forms.TextBox
    Friend WithEvents TB10a As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents TB1de As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents UGMorososCuotasAportes As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TBCotizacionDolar As System.Windows.Forms.TextBox
End Class
