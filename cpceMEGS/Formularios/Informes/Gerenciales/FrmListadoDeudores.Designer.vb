﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadoDeudores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListadoDeudores))
        Me.ListViewAfil = New System.Windows.Forms.ListView()
        Me.UCEreintegros = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.UNEcuoDEP = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.UNUCuoAportes = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBtListar = New System.Windows.Forms.Button()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.cboAporte = New System.Windows.Forms.ComboBox()
        Me.cboDEP = New System.Windows.Forms.ComboBox()
        Me.UGlistados = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.CBtNotaReclamos = New System.Windows.Forms.Button()
        Me.ListarSipres = New System.Windows.Forms.Button()
        CType(Me.UCEreintegros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEcuoDEP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNUCuoAportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListViewAfil
        '
        Me.ListViewAfil.CheckBoxes = True
        Me.ListViewAfil.Dock = System.Windows.Forms.DockStyle.Left
        Me.ListViewAfil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewAfil.Location = New System.Drawing.Point(0, 0)
        Me.ListViewAfil.Name = "ListViewAfil"
        Me.ListViewAfil.Size = New System.Drawing.Size(269, 464)
        Me.ListViewAfil.TabIndex = 1
        Me.ListViewAfil.UseCompatibleStateImageBehavior = False
        Me.ListViewAfil.View = System.Windows.Forms.View.Details
        '
        'UCEreintegros
        '
        EditorButton1.Text = "Reintegros Pendientes"
        EditorButton1.Width = 170
        Me.UCEreintegros.ButtonsLeft.Add(EditorButton1)
        Me.UCEreintegros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ValueListItem1.DataValue = "ValueListItem0"
        ValueListItem1.DisplayText = "SI"
        ValueListItem2.DataValue = "ValueListItem1"
        ValueListItem2.DisplayText = "NO"
        Me.UCEreintegros.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2})
        Me.UCEreintegros.Location = New System.Drawing.Point(585, 4)
        Me.UCEreintegros.Name = "UCEreintegros"
        Me.UCEreintegros.Size = New System.Drawing.Size(308, 24)
        Me.UCEreintegros.TabIndex = 135
        Me.UCEreintegros.Text = " "
        '
        'UNEcuoDEP
        '
        EditorButton2.Text = "Cuotas D.E.P.:"
        EditorButton2.Width = 170
        Me.UNEcuoDEP.ButtonsLeft.Add(EditorButton2)
        Me.UNEcuoDEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEcuoDEP.Location = New System.Drawing.Point(275, 34)
        Me.UNEcuoDEP.Name = "UNEcuoDEP"
        Me.UNEcuoDEP.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEcuoDEP.Size = New System.Drawing.Size(226, 24)
        Me.UNEcuoDEP.TabIndex = 134
        '
        'UNUCuoAportes
        '
        EditorButton3.Text = "Cuotas Deudores Aportes:"
        EditorButton3.Width = 170
        Me.UNUCuoAportes.ButtonsLeft.Add(EditorButton3)
        Me.UNUCuoAportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNUCuoAportes.Location = New System.Drawing.Point(275, 4)
        Me.UNUCuoAportes.Name = "UNUCuoAportes"
        Me.UNUCuoAportes.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNUCuoAportes.Size = New System.Drawing.Size(226, 24)
        Me.UNUCuoAportes.TabIndex = 133
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(640, 33)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 24)
        Me.UDTdesde.TabIndex = 139
        Me.UDTdesde.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(582, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 16)
        Me.Label4.TabIndex = 138
        Me.Label4.Text = "Desde:"
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2014, 6, 3, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(796, 33)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 136
        Me.UDThasta.Value = New Date(2014, 6, 3, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(743, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 16)
        Me.Label3.TabIndex = 137
        Me.Label3.Text = "Hasta:"
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Play_32xMD_color
        Me.CBtListar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtListar.Location = New System.Drawing.Point(278, 416)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.Size = New System.Drawing.Size(122, 38)
        Me.CBtListar.TabIndex = 140
        Me.CBtListar.Text = "Listar"
        Me.CBtListar.UseVisualStyleBackColor = False
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.Image = Global.cpceMEGS.My.Resources.Resources.PrintSetup_11011
        Me.CBtExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtExportar.Location = New System.Drawing.Point(690, 416)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(115, 38)
        Me.CBtExportar.TabIndex = 141
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'cboAporte
        '
        Me.cboAporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAporte.FormattingEnabled = True
        Me.cboAporte.Items.AddRange(New Object() {">", "<", "=", ">=", "<=", "<>"})
        Me.cboAporte.Location = New System.Drawing.Point(507, 5)
        Me.cboAporte.Name = "cboAporte"
        Me.cboAporte.Size = New System.Drawing.Size(46, 24)
        Me.cboAporte.TabIndex = 142
        '
        'cboDEP
        '
        Me.cboDEP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDEP.FormattingEnabled = True
        Me.cboDEP.Items.AddRange(New Object() {">", "<", "=", ">=", "<=", "<>"})
        Me.cboDEP.Location = New System.Drawing.Point(507, 35)
        Me.cboDEP.Name = "cboDEP"
        Me.cboDEP.Size = New System.Drawing.Size(46, 24)
        Me.cboDEP.TabIndex = 143
        '
        'UGlistados
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGlistados.DisplayLayout.Appearance = Appearance1
        Me.UGlistados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGlistados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGlistados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGlistados.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGlistados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGlistados.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGlistados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGlistados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGlistados.Location = New System.Drawing.Point(275, 63)
        Me.UGlistados.Name = "UGlistados"
        Me.UGlistados.Size = New System.Drawing.Size(762, 325)
        Me.UGlistados.TabIndex = 144
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(275, 394)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(762, 20)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 145
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'CBtNotaReclamos
        '
        Me.CBtNotaReclamos.BackColor = System.Drawing.Color.Transparent
        Me.CBtNotaReclamos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtNotaReclamos.ImageIndex = 0
        Me.CBtNotaReclamos.Location = New System.Drawing.Point(811, 416)
        Me.CBtNotaReclamos.Name = "CBtNotaReclamos"
        Me.CBtNotaReclamos.Size = New System.Drawing.Size(220, 38)
        Me.CBtNotaReclamos.TabIndex = 148
        Me.CBtNotaReclamos.Text = "Notas de Reclamos Aportes"
        Me.CBtNotaReclamos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBtNotaReclamos.UseVisualStyleBackColor = False
        Me.CBtNotaReclamos.Visible = False
        '
        'ListarSipres
        '
        Me.ListarSipres.BackColor = System.Drawing.Color.Transparent
        Me.ListarSipres.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListarSipres.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Play_32xMD_color
        Me.ListarSipres.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ListarSipres.Location = New System.Drawing.Point(406, 416)
        Me.ListarSipres.Name = "ListarSipres"
        Me.ListarSipres.Size = New System.Drawing.Size(122, 38)
        Me.ListarSipres.TabIndex = 149
        Me.ListarSipres.Text = "Sipres"
        Me.ListarSipres.UseVisualStyleBackColor = False
        '
        'FrmListadoDeudores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1039, 464)
        Me.Controls.Add(Me.ListarSipres)
        Me.Controls.Add(Me.CBtNotaReclamos)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.UGlistados)
        Me.Controls.Add(Me.cboDEP)
        Me.Controls.Add(Me.cboAporte)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.UCEreintegros)
        Me.Controls.Add(Me.UNEcuoDEP)
        Me.Controls.Add(Me.UNUCuoAportes)
        Me.Controls.Add(Me.ListViewAfil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmListadoDeudores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado Deudores"
        CType(Me.UCEreintegros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEcuoDEP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNUCuoAportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListViewAfil As System.Windows.Forms.ListView
    Friend WithEvents UCEreintegros As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents UNEcuoDEP As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UNUCuoAportes As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBtListar As System.Windows.Forms.Button
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents cboAporte As System.Windows.Forms.ComboBox
    Friend WithEvents cboDEP As System.Windows.Forms.ComboBox
    Friend WithEvents UGlistados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents CBtNotaReclamos As System.Windows.Forms.Button
    Friend WithEvents ListarSipres As System.Windows.Forms.Button
End Class
