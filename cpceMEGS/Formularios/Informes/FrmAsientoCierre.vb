﻿Imports MySql.Data.MySqlClient
Public Class FrmAsientoCierre
    Private cnn As New ConsultaBD(True)
    Private c_TipoAsiento As String
    Private DAApertura As MySqlDataAdapter
    Private dt_balance As DataTable
    Private dt_farcat As DataTable
    Public Sub New(ByVal TipoAsiento As String, ByVal dtBalance As DataTable, ByVal dtFarcat As DataTable)
        InitializeComponent()
        c_TipoAsiento = TipoAsiento
        dt_balance = dtBalance
        dt_farcat = dtFarcat
    End Sub

    Private Sub FrmAsientoCierre_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If c_TipoAsiento = "A" Then
            ActivoAsientoApertura()
        Else
            MuestraAsiento()
        End If
    End Sub

    Private Sub MuestraAsiento()
        Dim nSaldo As Double
        Dim nSaldoCta As Double
        If c_TipoAsiento = "D" Then
            Me.Text = "Asiento Determinación de Resultado"

            For x As Integer = dt_balance.Rows.Count - 1 To 0 Step -1
                If dt_balance.Rows(x).Item("pla_column") = 1 Then
                    nSaldoCta = 0
                    If dt_balance.Rows(x).Item("pla_debe") = 0 And dt_balance.Rows(x).Item("pla_haber") = 0 Then
                        dt_balance.Rows.RemoveAt(x)
                    ElseIf dt_balance.Rows(x).Item("pla_nropla") >= "3000000" And dt_balance.Rows(x).Item("pla_nropla") <= "4999999" Then
                        nSaldoCta = dt_balance.Rows(x).Item("pla_debe") - dt_balance.Rows(x).Item("pla_haber")
                        If nSaldoCta > 0 Then
                            dt_balance.Rows(x).Item("pla_haber") = nSaldoCta
                            dt_balance.Rows(x).Item("pla_debe") = 0
                            dt_balance.Rows(x).Item("saldo") = nSaldoCta
                        ElseIf nSaldoCta = 0 Then
                            dt_balance.Rows.RemoveAt(x)
                        Else
                            dt_balance.Rows(x).Item("pla_haber") = 0
                            dt_balance.Rows(x).Item("pla_debe") = nSaldoCta * -1
                            dt_balance.Rows(x).Item("saldo") = nSaldoCta * -1
                        End If
                        nSaldo += nSaldoCta
                    Else
                        dt_balance.Rows.RemoveAt(x)
                    End If

                Else
                    dt_balance.Rows.RemoveAt(x)
                End If
            Next
            Try
                '     Dim dtAux As DataTable = dt_balance.Clone
                For Each rowFar As DataRow In dt_farcat.Rows
                    nSaldoCta = 0
                    For Each rowBal As DataRow In dt_balance.Rows
                        If rowBal.Item("institucion") = rowFar.Item("far_unegos") And rowBal.Item("delegacion") = rowFar.Item("far_nrocli") Then
                            nSaldoCta += rowBal.Item("pla_debe") - rowBal.Item("pla_haber")
                        End If
                    Next
                    If nSaldoCta <> 0 Then
                        Dim rowResEj As DataRow = dt_balance.NewRow
                        rowResEj.Item("pla_nropla") = "24020200"
                        rowResEj.Item("pla_nombre") = "RESULTADO DEL EJERCICIO"
                        rowResEj.Item("institucion") = rowFar.Item("far_unegos")
                        rowResEj.Item("delegacion") = rowFar.Item("far_nrocli")
                        If nSaldoCta > 0 Then
                            rowResEj.Item("pla_debe") = 0
                            rowResEj.Item("pla_haber") = nSaldoCta
                        Else
                            rowResEj.Item("pla_debe") = nSaldoCta * -1
                            rowResEj.Item("pla_haber") = 0
                        End If
                        dt_balance.Rows.Add(rowResEj)
                    End If
                Next
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        ElseIf c_TipoAsiento = "C" Then ' asiento de cierre
            Me.Text = "Asiento Cancelación cuentas patrimoniales"
            For x As Integer = dt_balance.Rows.Count - 1 To 0 Step -1
                If dt_balance.Rows(x).Item("pla_column") = 1 Then
                    nSaldoCta = 0
                    If dt_balance.Rows(x).Item("pla_debe") = 0 And dt_balance.Rows(x).Item("pla_haber") = 0 Then
                        dt_balance.Rows.RemoveAt(x)
                    ElseIf dt_balance.Rows(x).Item("pla_nropla") <= "2999999" Then
                        nSaldoCta = dt_balance.Rows(x).Item("pla_debe") - dt_balance.Rows(x).Item("pla_haber")
                        If nSaldoCta > 0 Then
                            dt_balance.Rows(x).Item("pla_haber") = nSaldoCta
                            dt_balance.Rows(x).Item("pla_debe") = 0
                        ElseIf nSaldoCta = 0 Then
                            dt_balance.Rows.RemoveAt(x)
                        Else
                            dt_balance.Rows(x).Item("pla_haber") = 0
                            dt_balance.Rows(x).Item("pla_debe") = nSaldoCta * -1
                        End If
                        nSaldo += nSaldoCta
                    Else
                        dt_balance.Rows.RemoveAt(x)
                    End If

                Else
                    dt_balance.Rows.RemoveAt(x)
                End If
            Next
        Else ' asiento apertura
            dt_balance = New DataTable
            DAApertura = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "pla_nropla,pla_nombre,pla_madre,pla_column,tot_haber as pla_debe,tot_debe as pla_haber,6,7,tot_unegos as institucion,tot_nrocli as delegacion,cast(tot_fecha as char) as hasta,11,12,13,14,15,16,17", "tot_nroasi=" & UCEasientoApertura.Value)
            DAApertura.Fill(dt_balance)
        End If

        For Each rowSal As DataRow In dt_balance.Rows
            UNEdebe.Value += rowSal.Item("pla_debe")
            UNEhaber.Value += rowSal.Item("pla_haber")
        Next

        UGAsiento.DataSource = dt_balance
        With UGAsiento.DisplayLayout.Bands(0)
            .Columns(1).Width = 350
            .Columns(2).Hidden = True
            .Columns(3).Hidden = True
            ' .Columns(4).Hidden = True
            ' .Columns(5).Hidden = True
            .Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(4).Format = "N2"
            ' .Columns(6).Hidden = True
            .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(5).Format = "N2"
            .Columns(6).Hidden = True
            .Columns(7).Hidden = True
            .Columns(7).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(7).Format = "N2"
            ' .Columns(8).Hidden = True
            ' .Columns(9).Hidden = True
            ' .Columns(10).Hidden = True
            .Columns(11).Hidden = True
            .Columns(12).Hidden = True
            .Columns(13).Hidden = True
            .Columns(14).Hidden = True
            .Columns(15).Hidden = True
            .Columns(16).Hidden = True
            .Columns(17).Hidden = True
        End With
    End Sub

    Private Sub CBtConfirmar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtConfirmar.Click
        If MessageBox.Show("Confirma el asiento", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            If c_TipoAsiento = "D" Then
                GraboCierre("RESULT")
            ElseIf c_TipoAsiento = "C" Then
                GraboCierre("CIERRE")
            Else
                GraboCierre("APERT")
            End If
        End If
    End Sub

    Private Sub ActivoAsientoApertura()
        UCEasientoApertura.Visible = True
        UCEasientoApertura.Focus()
    End Sub

    Private Sub GraboCierre(ByVal cProceso As String)
        If cnn.AbrirConexion() Then
            cnn.Bloqueo(True)
            Dim miTrans As MySqlTransaction
            miTrans = cnn.InicioTransaccion()
            Try
                'Dim nInstitucion As Integer = dt_balance.Rows(0).Item("institucion")
                Dim cFechaCierre As String = Format(CDate(dt_balance.Rows(0).Item("hasta")), "yyyy-MM-dd") & " 23:59:59"
                Dim nDelegacion As Integer = dt_balance.Rows(0).Item("delegacion")
                Dim nNroAsiento As Long = cnn.TomaCobteAsiento("ASIENT")
                Dim nNrocomprobante As Long = cnn.TomaCobte(nPubNroIns, nPubNroCli, cProceso)

                ' si es asiento apertura tomo la fecha del cierre y sumo un dia
                If c_TipoAsiento = "A" Then
                    cFechaCierre = Format(CDate(cFechaCierre.ToString.Substring(0, 10)).AddDays(1), "yyyy-MM-dd") & " 00:00:00"
                End If

                cnn.ReplaceBD("insert into comproba set com_unegos='" & nPubNroIns & "'," & _
                      "com_nrodeleg='" & nDelegacion & "'," & _
                      "com_proceso='" & cProceso & "'," & _
                      "com_nrocli='" & nPubNroCli & "'," & _
                      "com_nrocom='" & nNrocomprobante & "'," & _
                      "com_nroasi='" & nNroAsiento & "'," & _
                      "com_asigrupal='" & 0 & "'," & _
                      "com_fecha='" & cFechaCierre & "'," & _
                      "com_total='" & UNEdebe.Value.ToString.Replace(",", ".") & "'," & _
                      "com_nroope='" & nPubNroOperador & "'," & _
                      "com_concepto1='" & "" & "'," & _
                      "com_fecalt='" & cnn.FechaHoraActual & "'")

                Dim nItem As Integer = 0
                For Each rowAsi As DataRow In dt_balance.Rows
                    nItem += 1
                    cnn.ReplaceBD("insert into totales set tot_unegos='" & rowAsi.Item("institucion") & "'," & _
                    "tot_proceso='" & cProceso & "'," & _
                    "tot_nrocli='" & rowAsi.Item("delegacion") & "'," & _
                    "tot_nrocom='" & nNrocomprobante & "'," & _
                    "tot_item='" & nItem & "'," & _
                    "tot_nropla='" & rowAsi.Item("pla_nropla") & "'," & _
                    "tot_nroope='" & nPubNroOperador & "'," & _
                    "tot_nrocuo='" & 1 & "'," & _
                    "tot_fecha='" & cFechaCierre & "'," & _
                    "tot_debe='" & rowAsi.Item("pla_debe").ToString.Replace(",", ".") & "'," & _
                    "tot_haber='" & rowAsi.Item("pla_haber").ToString.Replace(",", ".") & "'," & _
                    "tot_nroasi='" & nNroAsiento & "'")
                Next

                miTrans.Commit()
                cnn.Bloqueo(False)
                MessageBox.Show("El Asiento finalizado satifactoriamente" & vbCrLf & vbCrLf & "Asiento N° " & nNroAsiento)
            Catch ex As Exception
                miTrans.Rollback()
                cnn.Bloqueo(False)
                MessageBox.Show(ex.Message)
            End Try
            cnn.CerrarConexion()
        End If
    End Sub

    Private Sub UCEasientoApertura_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UCEasientoApertura.EditorButtonClick
        UCEasientoApertura.Visible = False
        MuestraAsiento()
    End Sub
End Class