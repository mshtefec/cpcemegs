﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports MSScriptControl
Imports Microsoft.Office.Interop

Public Class FrmListadosGerencialesDeudores
    Private cnn As New ConsultaBD(True)
    Dim DTAfiliado As DataTable
    Dim DTListado As DataTable
    Dim DTPadron As New DataTable
    Dim WithEvents BSListado As BindingSource
    Dim DAListado As MySqlDataAdapter
    Dim cmdListado As MySqlCommandBuilder
    Dim nOrdenSeleccion As Integer = 0
    Dim clsExportarExcel As New ExportarExcel
    Private permiteEditarListado As Boolean
    Private Sub FrmListados_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        UDThasta.Value = Now
        UDTdebe.Value = Now
        UDThaber.Value = Now
        CargaListViewAfiliado()
        CargaListados()
    End Sub
    Private Sub CargaListViewAfiliado()
        DAListado = cnn.consultaBDadapter("afiliado inner join cuentas on cue_tipdoc=afi_tipdoc and cue_nrodoc=afi_nrodoc", , "afi_nrodoc=999999999")
        DTAfiliado = New DataTable
        DAListado.Fill(DTAfiliado)

        Dim objListItem As New ListViewItem

        ListViewAfil.Items.Clear()
        ListViewAfil.Columns.Add("")
        ListViewAfil.Columns.Add("Campo")
        ListViewAfil.Columns.Add("Orden")
        'Si no tiene Permiso U = Edit Agrego solo los campos del Public camposListadoSelect
        If controlAcceso(7, 6, "U", False) = False Then
            permiteEditarListado = False
            For Each colCaption As String In camposListadoSelect
                objListItem = New ListViewItem()
                objListItem.SubItems.Add(colCaption)
                objListItem.SubItems.Add(0)
                ListViewAfil.Items.Add(objListItem)
            Next
        Else 'Si tiene permisos Agrego todos los campos
            permiteEditarListado = True
            For Each col As DataColumn In DTAfiliado.Columns
                objListItem = New ListViewItem()
                objListItem.SubItems.Add(col.Caption)
                objListItem.SubItems.Add(0)
                ListViewAfil.Items.Add(objListItem)
            Next
        End If
        ListViewAfil.Columns(0).Width = 20
        ListViewAfil.Columns(1).Width = 150
    End Sub
    Private Sub CargaListados()
        DAListado = cnn.consultaBDadapter("listados")
        DTListado = New DataTable
        cmdListado = New MySqlCommandBuilder(DAListado)
        DAListado.Fill(DTListado)
        BSListado = New BindingSource
        BSListado.DataSource = DTListado
        UGlistados.DataSource = BSListado
        'Si permite editar listado entra
        If permiteEditarListado Then
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(0).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(1).CellActivation = UltraWinGrid.Activation.AllowEdit
                .Columns(2).CellActivation = UltraWinGrid.Activation.AllowEdit
                .Columns(2).Width = 800
                .Columns(3).CellActivation = UltraWinGrid.Activation.AllowEdit
                .Columns(4).CellActivation = UltraWinGrid.Activation.AllowEdit
                .Columns(5).CellActivation = UltraWinGrid.Activation.AllowEdit
            End With
        Else
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(0).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(1).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(2).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(2).Width = 800
                .Columns(3).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(4).CellActivation = UltraWinGrid.Activation.NoEdit
                .Columns(5).CellActivation = UltraWinGrid.Activation.NoEdit
            End With
        End If
    End Sub

    Private Sub CBtAcualizar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtAcualizar.Click
        If CBtAcualizar.Text = "Volver a Listados" Then
            CBtAcualizar.Text = "Actualizar"
            CargaListados()
        Else
            Try
                DAListado.Update(DTListado)
                DTListado.AcceptChanges()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub ArmarConsulta()
        Dim cfiltro As String = "" ' DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        Dim cTablas As String = "" ' "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"
        UltraProgressBar1.Value = 0
        UltraProgressBar1.Minimum = 0

        For I As Integer = 1 To nOrdenSeleccion
            For Each item As ListViewItem In ListViewAfil.Items
                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next

        If txCuenta.TextLength > 0 Then
            cTablas = " totales 
                inner join plancuen on pla_nropla = tot_nropla 
                inner join afiliado on tot_tipdoc = afi_tipdoc and tot_nrodoc = afi_nrodoc 
                inner join cuentas on afi_tipdoc = cue_tipdoc AND afi_nrodoc = cue_nrodoc AND afi_titulo = cue_titulo"
            'Si se definio una relacion con otra tabla se la agrega
            If DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim <> "" Then
                cTablas &= " " & DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim
            End If
            cfiltro = "tot_nropla IN (" & txCuenta.Text.Trim & ") and
                tot_fecha BETWEEN '" &
                Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' AND
                tot_estado <> '9'"
            cfiltro &= " AND (
                (tot_debe > 0 AND tot_fecha <= '" & Format(UDTdebe.Value, "yyyy-MM-dd") & " 23:59:59') OR
                (tot_haber > 0 AND tot_fecha <= '" & Format(UDThaber.Value, "yyyy-MM-dd") & " 23:59:59')
            )"
            If DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim <> "" Then
                cfiltro &= " AND (" & DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim & ")"
            End If
            If DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim <> "" Then
                cfiltro &= " GROUP BY " & DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim
            Else
                cfiltro &= " GROUP BY tot_tipdoc,tot_nrodoc,tot_nropla"
            End If
            'Si no selecciono campos obtiene los definidos en la tabla del listado seleccionado lis_campos
            If cColumnas = "" Then
                'Si permite editar listado entra
                If permiteEditarListado Then
                    cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
                ElseIf DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim = "*" Then
                    cColumnas = "afi_nombre AS Afiliado"
                Else
                    cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
                End If
                If cColumnas = "" Then
                    cColumnas = "afi_nombre AS Afiliado"
                End If
                cColumnas &= ",sum(tot_debe-tot_haber) as Saldo"
            Else
                cColumnas &= ",sum(tot_debe-tot_haber) as Saldo"
            End If
            'cColumnas &= ",tot_nropla,pla_nombre,sum(tot_debe-tot_haber) as Saldo"
        Else
            cTablas = "afiliado inner join cuentas on afi_tipdoc = cue_tipdoc AND afi_nrodoc = cue_nrodoc AND afi_titulo = cue_titulo"
            'Si se definio una relacion con otra tabla se la agrega
            If DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim <> "" Then
                cTablas &= " " & DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim
            End If
            cfiltro = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
            If Not CBsfssFamiliares.Checked Then
                If DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim <> "" Then
                    cfiltro &= " GROUP BY " & DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim
                    'Else
                    'cfiltro &= " GROUP BY tot_tipdoc,tot_nrodoc"
                End If
            End If
            'Si no selecciono campos obtiene los definidos en la tabla del listado seleccionado lis_campos
            If cColumnas = "" Then
                'Si permite editar listado entra
                If permiteEditarListado Then
                    cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
                ElseIf DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim = "*" Then
                    cColumnas = "afi_nombre AS Afiliado"
                Else
                    cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
                End If
            End If
        End If

        Try
            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTPadron = New DataTable
            DAListado.SelectCommand.CommandTimeout = 600000 'En debug da TIMEOUT ERROR
            DAListado.Fill(DTPadron)
            DTPadron.Columns.Add("Select", GetType(Boolean))
            DTPadron.Columns("Select").SetOrdinal(0)
            If ComboBox1.Text.Trim <> "" Then
                UltraProgressBar1.Maximum = DTPadron.Rows.Count
                For i As Integer = DTPadron.Rows.Count - 1 To 0 Step -1
                    Application.DoEvents()
                    DTPadron.Rows(i).Item("Select") = False
                    If oSC.Eval(DTPadron.Rows(i).Item("Saldo").ToString.Replace(",", ".") & ComboBox1.Text & UCESaldo.Value.ToString.Replace(",", ".")) Then
                    Else
                        DTPadron.Rows.RemoveAt(i)
                    End If
                    UltraProgressBar1.Value += 1
                Next
            End If
            'FACPCE Sistema del Fondo Solidario de Salud Si marca el checkbox traigo los familiares correspondientes
            If CBsfssFamiliares.Checked Then
                DTPadron = New DataTable
                Dim DTPadronFamilia As DataTable
                UltraProgressBar1.Value = 0
                UltraProgressBar1.Minimum = 0
                UltraProgressBar1.Maximum = 6
                'cColumnas Parentezco solo tiene en cuenta si
                'esposo-a(valor 0 conviernte a 1) o hijo-a (valor 1 convierte a 2)
                For i = 1 To 6 Step 1
                    Application.DoEvents()
                    cColumnas = "
                        CONCAT(tit_matricula,afi_matricula) AS Legajo,
                        afi_aut" & i & " AS ApellidoNombre,
                        IF(afi_fil" & i & " > 0,2,1) AS Parentezco,
                        1 AS Incapacidad,
                        afi_nac" & i & " AS FechaNacimiento,
                        IF(afi_sex" & i & "='M',5,6) AS TipDoc,
                        afi_doc" & i & " AS NroDoc,
                        cue_fecmatricula AS FechaAlta,
                        '' AS FechaBaja,
                        2 AS ACargo,
                        '' AS ObraSocial
                    "
                    cfiltro = "tot_nropla IN (" & txCuenta.Text.Trim & ") and
                        tot_fecha BETWEEN '" &
                        Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                        Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' AND
                        tot_estado <> '9'"
                    If DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim <> "" Then
                        cfiltro &= " AND (" & DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim & ")"
                    End If
                    cfiltro &= " AND afi_doc" & i & " <> '' AND afi_doc" & i & " <> '0' AND afi_doc" & i & " > 0"
                    cfiltro &= " AND afi_beneficiario" & i & " <> 'Si' "
                    If DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim <> "" Then
                        cfiltro &= " GROUP BY " & DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim & ",afi_doc" & i
                    End If
                    DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
                    If i = 1 Then
                        DAListado.Fill(DTPadron)
                    Else
                        DTPadronFamilia = New DataTable
                        DAListado.Fill(DTPadronFamilia)
                        DTPadron.Merge(DTPadronFamilia)
                    End If
                    UltraProgressBar1.Value += 1
                Next i
                DTPadron.Columns.Add("Select", GetType(Boolean))
                DTPadron.Columns("Select").SetOrdinal(0)
            End If

            UGlistados.DataSource = DTPadron
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Se Produjo en error, controle los datos ingresados")
        End Try
    End Sub

    Private Sub CBtListar_Click(sender As Object, e As EventArgs) Handles CBtListar.Click
        CBtAcualizar.Text = "Volver a Listados"
        If CBListadoMorosos.Checked Then
            ListadoMorosos()
        ElseIf CBListadoPadrones.Checked Then
            ListadoPadrones()
        ElseIf CBListadoSipres.Checked Then
            ListadoSipres()
        Else
            ArmarConsulta()
        End If
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtExportar.Click
        Dim cTitulo(0) As String
        UGlistados.DisplayLayout.Bands(0).Columns(0).Hidden = True ' OCULTO LA COLUMNA SELECT
        cTitulo(0) = DTListado.Rows(BSListado.Position).Item("lis_titulo")
        clsExportarExcel.ExportarDatosExcel(UGlistados, cTitulo)
        UGlistados.DisplayLayout.Bands(0).Columns(0).Hidden = False
    End Sub

    Private Sub ListViewAfil_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles ListViewAfil.ItemCheck
        If e.CurrentValue = CheckState.Unchecked Then
            nOrdenSeleccion += 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = nOrdenSeleccion
        Else
            nOrdenSeleccion -= 1
            ListViewAfil.Items(e.Index).SubItems(2).Text = 0
        End If
    End Sub

    Private Sub txCuenta_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txCuenta.KeyPress
        Tabular(e)
    End Sub

    Private Sub UCESaldo_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCESaldo.KeyPress
        Tabular(e)
    End Sub

    Private Sub UltraDateTimeEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UDThasta.KeyPress
        Tabular(e)
    End Sub

    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles ComboBox1.KeyPress
        Tabular(e)
    End Sub

    Private Sub ListadoMorosos()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim & " GROUP BY tot_tipdoc,tot_nrodoc"
        ' "(afiliado inner join cuentas on cue_titulo=afi_titulo and cue_matricula=afi_matricula)"
        Dim cTablas As String = "totales inner join afiliado on afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc"
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For I As Integer = 1 To nOrdenSeleccion
            For Each item As ListViewItem In ListViewAfil.Items
                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next
        If cColumnas = "" Then
            MessageBox.Show("Debe seleccionar los datos del profesional")
            Exit Sub
        End If
        cColumnas += ",afi_tipdoc,afi_nrodoc,0 as cuotas,afi_debitos as importe,afi_debitos as total,afi_debitos as interes,0 as CantCuotas,afi_debitos as TotCuotas,afi_debitos as TotIntereses,afi_debitos as CapInv,afi_debitos as FonComp,afi_debitos as GasAdm,afi_debitos as ServSoc,afi_debitos as TotalACI,sum(tot_debe-tot_haber) as Saldo"
        If txCuenta.TextLength > 0 Then
            cfiltro = "tot_nropla = '" & txCuenta.Text & "' AND
                tot_fecha <= '" & Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' AND
                tot_estado <> '9'
                GROUP BY tot_tipdoc,tot_nrodoc ORDER BY tot_titulo,tot_matricula"
            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTAfiliado = New DataTable
            DAListado.Fill(DTAfiliado)

            DTAfiliado.Columns.Add("Select", GetType(Boolean))
            DTAfiliado.Columns("Select").SetOrdinal(0)

            'Dim columns(2) As DataColumn
            'columns(0) = DTAfiliado.Columns("afi_tipdoc")
            'columns(1) = DTAfiliado.Columns("afi_nrodoc")
            'columns(2) = DTAfiliado.Columns("importe")
            'DTAfiliado.PrimaryKey = columns

            DTAfiliadoClone = DTAfiliado.Clone

            Dim nCantCuo As Integer = 0
            Dim nTotCantCuo As Integer = 0
            Dim nMonCuo As Double = 0
            Dim nMonInteres As Double = 0
            Dim nTotMonCuo As Double = 0
            Dim nTotMonIntereses As Double = 0
            Dim nTotSaldo As Double = 0
            Dim nRegCuo As Integer = 0
            UltraProgressBar1.Value = 0
            UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
            UltraProgressBar1.Minimum = 0
            For Each RowAfi As DataRow In DTAfiliado.Rows
                Application.DoEvents()
                DTMovimientos = cuotas.CuotasServicioImpagas(txCuenta.Text, RowAfi.Item("afi_tipdoc"), RowAfi.Item("afi_nrodoc"), True, UDThasta.Value)
                RowAfi.Item("Select") = False
                nTotSaldo = RowAfi.Item("Saldo")
                RowAfi.Item("Saldo") = 0
                For Each rowMov As DataRow In DTMovimientos.Rows
                    If rowMov.Item("Fecha").ToString <= CDate(UDThasta.Value.ToString.Substring(0, 10)) Then
                        nRegCuo += 1
                        If nMonCuo <> (rowMov.Item("debe") - rowMov.Item("imppagado")) Then
                            RowAfi.Item("cuotas") = nCantCuo
                            RowAfi.Item("importe") = Math.Round(nMonCuo, 2)
                            RowAfi.Item("total") = Math.Round((nMonCuo * nCantCuo), 2)
                            RowAfi.Item("interes") = Math.Round(nMonInteres, 2)
                            If nMonCuo > 0 Then
                                If txCuenta.Text = "13040100" Then
                                    Select Case nMonCuo
                                        Case 100
                                            RowAfi.Item("CapInv") = 62.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 131
                                            RowAfi.Item("CapInv") = 93.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 212
                                            RowAfi.Item("CapInv") = 174.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 110
                                            RowAfi.Item("CapInv") = 68.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 145
                                            RowAfi.Item("CapInv") = 103.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 235
                                            RowAfi.Item("CapInv") = 193.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 130
                                            RowAfi.Item("CapInv") = 82.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 170
                                            RowAfi.Item("CapInv") = 122.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 270
                                            RowAfi.Item("CapInv") = 222.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 155
                                            RowAfi.Item("CapInv") = 98.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 202
                                            RowAfi.Item("CapInv") = 145.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 320
                                            RowAfi.Item("CapInv") = 263.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 194
                                            RowAfi.Item("CapInv") = 123.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 253
                                            RowAfi.Item("CapInv") = 182.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 400
                                            RowAfi.Item("CapInv") = 329.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case 75
                                            RowAfi.Item("CapInv") = 37.5 * nCantCuo
                                            RowAfi.Item("Foncomp") = 15.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 11.2 * nCantCuo
                                            RowAfi.Item("ServSoc") = 11.25 * nCantCuo
                                        Case 85
                                            RowAfi.Item("CapInv") = 43.75 * nCantCuo
                                            RowAfi.Item("Foncomp") = 16.55 * nCantCuo
                                            RowAfi.Item("GasAdm") = 12.38 * nCantCuo
                                            RowAfi.Item("ServSoc") = 12.32 * nCantCuo
                                        Case 105
                                            RowAfi.Item("CapInv") = 57.55 * nCantCuo
                                            RowAfi.Item("Foncomp") = 19.05 * nCantCuo
                                            RowAfi.Item("GasAdm") = 14.15 * nCantCuo
                                            RowAfi.Item("ServSoc") = 14.25 * nCantCuo
                                        Case 125
                                            RowAfi.Item("CapInv") = 68.43 * nCantCuo
                                            RowAfi.Item("Foncomp") = 22.71 * nCantCuo
                                            RowAfi.Item("GasAdm") = 16.87 * nCantCuo
                                            RowAfi.Item("ServSoc") = 16.99 * nCantCuo
                                        Case 157
                                            RowAfi.Item("CapInv") = 86.28 * nCantCuo
                                            RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                        Case Else
                                            'MessageBox.Show("monto de cuota :" & nMonCuo & " Matricula:" & RowAfi.Item("afi_matricula"))
                                            'RowAfi.Item("CapInv") = 86.28 * nCantCuo
                                            'RowAfi.Item("Foncomp") = 28.39 * nCantCuo
                                            'RowAfi.Item("GasAdm") = 21.09 * nCantCuo
                                            'RowAfi.Item("ServSoc") = 21.24 * nCantCuo
                                    End Select
                                End If
                            End If
                            DTAfiliadoClone.ImportRow(RowAfi)
                            nTotMonCuo += nMonCuo * nCantCuo
                            nTotMonIntereses += nMonInteres
                            nMonCuo = rowMov.Item("debe") - rowMov.Item("imppagado")
                            nMonInteres = rowMov.Item("interes")
                            nTotCantCuo += nCantCuo
                            nCantCuo = 1
                        Else
                            nCantCuo += 1
                            nMonInteres += rowMov.Item("interes")
                        End If
                    End If
                Next
                ' aporte capitalizacion indiv.
                DAListado = cnn.consultaBDadapter(
                    "totales",
                    "tot_tipdoc,tot_nrodoc,max(tot_fecha) as fecha,count(*) as cantidad,sum(tot_debe-tot_haber) as haber",
                    "tot_fecha between '" &
                    Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                    Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' AND
                    tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' AND
                    tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " AND
                    tot_nropla='22010100'
                    GROUP BY tot_tipdoc,tot_nrodoc"
                )
                DTMovimientos = New DataTable
                DAListado.Fill(DTMovimientos)
                If DTMovimientos.Rows.Count > 0 Then
                    RowAfi.Item("TotalACI") = Math.Round(DTMovimientos.Rows(0).Item("haber"), 2)
                End If
                nTotMonCuo += nMonCuo * nCantCuo
                nTotMonIntereses += nMonInteres
                nTotCantCuo += nCantCuo
                RowAfi.Item("cuotas") = nCantCuo
                RowAfi.Item("importe") = Math.Round(nMonCuo, 2)
                If nMonCuo * nCantCuo = 0 Then
                    RowAfi.Item("total") = Math.Round(nTotSaldo, 2)
                Else
                    RowAfi.Item("total") = Math.Round(nMonCuo * nCantCuo, 2)
                End If
                RowAfi.Item("interes") = Math.Round(nMonInteres, 2)
                RowAfi.Item("cantcuotas") = nTotCantCuo
                RowAfi.Item("totcuotas") = Math.Round(nTotMonCuo, 2)
                RowAfi.Item("totintereses") = Math.Round(nTotMonIntereses, 2)
                RowAfi.Item("Saldo") = Math.Round(nTotSaldo, 2)
                DTAfiliadoClone.ImportRow(RowAfi)
                ' End If
                nMonCuo = 0
                nCantCuo = 0
                nTotCantCuo = 0
                nMonInteres = 0
                nTotMonCuo = 0
                nTotMonIntereses = 0
                nTotSaldo = 0
                nRegCuo = 0
                UltraProgressBar1.Value += 1
            Next

            If ComboBox1.Text.Trim <> "" Then
                For i As Integer = DTAfiliadoClone.Rows.Count - 1 To 0 Step -1
                    If oSC.Eval(DTAfiliadoClone.Rows(i).Item("total").ToString.Replace(",", ".") & ComboBox1.Text & UCESaldo.Value.ToString.Replace(",", ".")) Then
                    Else
                        DTAfiliadoClone.Rows(i).Delete()
                    End If
                Next
            End If

            UGlistados.DataSource = DTAfiliadoClone
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(DTAfiliadoClone.Columns.Count - 12).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 11).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 10).Format = "N2"

                .Columns(DTAfiliadoClone.Columns.Count - 8).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 7).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 6).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 5).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 4).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 2).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"
            End With
        End If
    End Sub
    Private Sub ListadoSipres()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As New DataTable
        Dim nCantidadAfiliado As Integer = 0

        Dim cTablas As String = "afiliado"
        If DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim <> "" Then
            cTablas &= " " & DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim
        End If

        Dim cColumnas As String = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim

        Dim cfiltro As String = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        cfiltro &= " AND cue_fecmatricula <= '" & Format(UDThasta.Value, "yyyy-MM-dd") & "'"

        If txCuenta.TextLength > 0 Then
            Dim cuentasFiltro As String
            If txCuenta.Text.Contains(",") Then
                cuentasFiltro = " AND tot_nropla IN (" & txCuenta.Text & ")"
            Else
                cuentasFiltro = " AND tot_nropla='" & txCuenta.Text & "'"
            End If
            cfiltro &= cuentasFiltro & " AND tot_estado <> '9'"
            cfiltro &= " AND scu_vigencia BETWEEN '" &
                Format(UDTdesde.Value, "yyyy-MM-dd") & "' AND '" &
                Format(UDThasta.Value, "yyyy-MM-dd") & "'"
            cfiltro &= " AND (
                (tot_debe > 0 AND tot_fecha BETWEEN '" &
                Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59') OR
                (tot_haber > 0 AND tot_fecha BETWEEN '" &
                Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59'))"
            If DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim <> "" Then
                cfiltro &= " GROUP BY " & DTListado.Rows(BSListado.Position).Item("lis_groupby").ToString.Trim
            End If
            If DTListado.Rows(BSListado.Position).Item("lis_orderby").ToString.Trim <> "" Then
                cfiltro &= " ORDER BY " & DTListado.Rows(BSListado.Position).Item("lis_orderby").ToString.Trim
            End If

            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTAfiliado = New DataTable
            DAListado.SelectCommand.CommandTimeout = 600000 'En debug da TIMEOUT ERROR

            DAListado.Fill(DTAfiliado)

            'DTAfiliado.Columns.Add("Select", GetType(Boolean))
            'DTAfiliado.Columns("Select").SetOrdinal(0)

            Dim nCantCuo As Integer = 0
            Dim nTotCantCuo As Integer = 0
            Dim nMonCuo As Double = 0
            Dim nMonInteres As Double = 0
            Dim nTotMonCuo As Double = 0
            Dim nTotMonIntereses As Double = 0
            Dim nTotSaldo As Double = 0
            Dim nRegCuo As Integer = 0
            UltraProgressBar1.Value = 0
            UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
            UltraProgressBar1.Minimum = 0

            DTAfiliadoClone = DTAfiliado.Clone

            Dim rowAfiAgregar As DataRow = DTAfiliado.Rows(0)
            'Dim rowAfiAnterior As DataRow = DTAfiliado.Rows(0)
            Dim rowAfiAnterior As DataRow = Nothing
            'DTAfiliado.Rows(0).Item("cantidad") = 1
            'Dim agregoAfiliado As Boolean = False
            Dim dias As Integer
            Dim mesesDevengados As Integer = 0
            Dim mesesInactividad As Integer = 0
            For Each RowAfi As DataRow In DTAfiliado.Rows
                Application.DoEvents()
                'RowAfi.Item("Select") = False
                'Pregunto primero si el afiliado anterior no esta vacio
                'Si cambia el afiliado lo seteo para agregar al listado
                If Not IsNothing(rowAfiAnterior) AndAlso rowAfiAnterior.Item("id") <> RowAfi.Item("id") Then
                    CalculosSiCambiaAfiliadoListadoSipres(DTAfiliadoClone, RowAfi, rowAfiAgregar, rowAfiAnterior, nCantidadAfiliado, mesesDevengados, mesesInactividad)
                Else
                    dias = DateDiff(DateInterval.Day, CDate(rowAfiAgregar.Item("scu_vigencia").ToString), CDate(RowAfi.Item("scu_vigencia").ToString))
                    If dias > 0 Then
                        If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 5 Then
                            rowAfiAgregar.Item("situacion_actual") = CDate(RowAfi.Item("scu_vigencia").ToString)
                        Else
                            'Si Es listado de Jubilados y si la categoria es jubilado seteo la Fecha Alta Beneficio y Fecha Primer Pago
                            If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 6 And RowAfi.Item("scu_categoria").ToString = 170101 Then
                                rowAfiAgregar.Item("matriculacion") = RowAfi.Item("scu_vigencia")
                                rowAfiAgregar.Item("situacion_actual") = RowAfi.Item("scu_vigencia")
                            End If
                        End If
                    End If
                    'CALCULO
                    CalculoMeses(RowAfi, rowAfiAnterior, mesesDevengados, mesesInactividad, False, False)
                End If
                'Si entra agrego afiliado y seteo en false para no volver a agregar repetido
                'If agregoAfiliado Then
                '    agregoAfiliado = False
                '    DTAfiliadoClone.ImportRow(rowAfiAgregar)
                'End If

                'nMonCuo = 0
                'nCantCuo = 0
                'nTotCantCuo = 0
                'nMonInteres = 0
                'nTotMonCuo = 0
                'nTotMonIntereses = 0
                'nTotSaldo = 0
                'nRegCuo = 0
                rowAfiAnterior = RowAfi 'Guardo el afiliado como anterior antes de seguir recorriendo
                UltraProgressBar1.Value += 1
            Next
            CalculosSiCambiaAfiliadoListadoSipres(DTAfiliadoClone, DTAfiliado.Rows(DTAfiliado.Rows.Count() - 1), rowAfiAgregar, DTAfiliado.Rows(DTAfiliado.Rows.Count() - 2), nCantidadAfiliado, mesesDevengados, mesesInactividad)

            UGlistados.DataSource = DTAfiliadoClone
            If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 5 Then
                With UGlistados.DisplayLayout.Bands(0)
                    .Columns(0).Header.Caption = "Nro"
                    .Columns(1).Header.Caption = "ID"
                    .Columns(2).Header.Caption = "Fecha nacimiento"
                    .Columns(3).Header.Caption = "Edad"
                    .Columns(4).Header.Caption = "Sexo"
                    .Columns(5).Header.Caption = "Situación"
                    .Columns(6).Header.Caption = "Tipo"
                    .Columns(7).Header.Caption = "Fecha matriculación" 'cue_fecmatricula AS matriculacion
                    .Columns(8).Hidden = True 'scu_vigencia
                    .Columns(9).Header.Caption = "Fecha de situación actual"
                    .Columns(10).Header.Caption = "Meses de aportes devengados" 'meses_devengados
                    .Columns(11).Header.Caption = "Importe capitalizado"
                    '.Columns(12).Header.Caption = "Beneficio determinado"
                    .Columns(12).Hidden = True 'total_debe
                    .Columns(13).Hidden = True 'total_haber
                    .Columns(14).Header.Caption = "Meses inactividad"
                    .Columns(15).Header.Caption = "Meses cumplidos"
                    .Columns(16).Header.Caption = "Meses no cumplidos"
                    .Columns(17).Header.Caption = "Deuda sin plan de pago"
                    .Columns(18).Header.Caption = "Deuda con plan de pago"
                    .Columns(19).Hidden = True 'scu_categoria
                    .Columns(20).Hidden = True 'total_haberscu_categoria_anterior
                    .Columns(21).Hidden = True 'afi_tipdoc
                    .Columns(22).Hidden = True 'afi_nrodoc
                End With
            Else
                With UGlistados.DisplayLayout.Bands(0)
                    .Columns(0).Header.Caption = "Nro"
                    .Columns(1).Header.Caption = "ID"
                    .Columns(2).Header.Caption = "Fecha nacimiento"
                    .Columns(3).Header.Caption = "Edad"
                    .Columns(4).Header.Caption = "Sexo"
                    .Columns(5).Header.Caption = "Situación"
                    .Columns(6).Header.Caption = "Tipo"
                    .Columns(7).Header.Caption = "Fecha alta beneficio" 'cue_fecmatricula AS matriculacion
                    .Columns(8).Hidden = True 'scu_vigencia
                    .Columns(9).Header.Caption = "Fecha primer pago"
                    .Columns(10).Header.Caption = "Meses de servicio" 'meses_devengados
                    .Columns(11).Header.Caption = "Importe beneficio" 'importe_capitalizado query uso cuenta 41020800
                    '.Columns(12).Header.Caption = "Beneficio determinado"
                    .Columns(12).Hidden = True 'total_debe
                    .Columns(13).Hidden = True 'total_haber
                    .Columns(14).Hidden = True '= "Meses inactividad"
                    .Columns(15).Header.Caption = "Meses aportados" '"Meses cumplidos"
                    .Columns(16).Hidden = True '= "Meses no cumplidos"
                    .Columns(17).Hidden = True '= "Deuda sin plan de pago"
                    .Columns(18).Hidden = True '= "Deuda con plan de pago"
                    .Columns(19).Hidden = True 'scu_categoria
                    .Columns(20).Hidden = True 'total_haberscu_categoria_anterior
                    .Columns(21).Hidden = True 'afi_tipdoc
                    .Columns(22).Hidden = True 'afi_nrodoc
                    .Columns(23).Header.Caption = "Capital Pagado Beneficiarios"
                End With
            End If
        End If
    End Sub
    Private Sub CalculosSiCambiaAfiliadoListadoSipres(ByRef DTAfiliadoClone As DataTable, ByRef RowAfi As DataRow, ByRef rowAfiAgregar As DataRow, ByRef rowAfiAnterior As DataRow, ByRef nCantidadAfiliado As Integer, ByRef mesesDevengados As Integer, ByRef mesesInactividad As Integer)
        Dim calculo As Boolean = False
        If Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "11" Or Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "12" Then
            If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 5 Then
                calculo = True
            End If
        ElseIf Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "17" Or Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "20" Or Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "22" Then
            If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 6 Then
                calculo = True
            End If
        End If
        If calculo Then
            Select Case rowAfiAnterior.Item("scu_categoria")
                Case 110101, 120101
                    rowAfiAgregar.Item("tipo") = "A"
                Case 110102, 120102
                    rowAfiAgregar.Item("tipo") = "E"
                Case 110103, 120103
                    rowAfiAgregar.Item("tipo") = "B"
                Case 110104, 120104
                    rowAfiAgregar.Item("tipo") = "C"
                Case 170101
                    rowAfiAgregar.Item("tipo") = "Jubilado"
                Case 200000
                    rowAfiAgregar.Item("tipo") = "No activo"
                Case 220100
                    rowAfiAgregar.Item("tipo") = "Fallecido"
                Case 220200
                    rowAfiAgregar.Item("tipo") = "Baja por solicitud"
                Case 220303
                    rowAfiAgregar.Item("tipo") = "Baja contable"
                Case 220305
                    rowAfiAgregar.Item("tipo") = "Previsionados"
                Case Else
                    rowAfiAgregar.Item("tipo") = rowAfiAnterior.Item("scu_categoria")
            End Select
            nCantidadAfiliado += 1
            'End If
            'CALCULO
            CalculoMeses(RowAfi, rowAfiAnterior, mesesDevengados, mesesInactividad, False, True)
            rowAfiAgregar.Item("cantidad") = nCantidadAfiliado
            rowAfiAgregar.Item("meses_devengados") = mesesDevengados
            rowAfiAgregar.Item("meses_inactividad") = mesesInactividad
            If rowAfiAgregar.Item("total_haber") > 0 Then
                If rowAfiAgregar.Item("total_debe") > 0 Then
                    rowAfiAgregar.Item("meses_cumplidos") = Math.Round(rowAfiAgregar.Item("total_haber") / (rowAfiAgregar.Item("total_debe") / mesesDevengados))
                    If rowAfiAgregar.Item("meses_cumplidos") > mesesDevengados Then
                        rowAfiAgregar.Item("meses_cumplidos") = mesesDevengados
                        rowAfiAgregar.Item("meses_incumplidos") = 0
                    Else
                        rowAfiAgregar.Item("meses_incumplidos") = mesesDevengados - rowAfiAgregar.Item("meses_cumplidos")
                    End If
                Else
                    rowAfiAgregar.Item("meses_cumplidos") = mesesDevengados
                    rowAfiAgregar.Item("meses_incumplidos") = 0
                End If
            Else
                rowAfiAgregar.Item("meses_cumplidos") = 0
                rowAfiAgregar.Item("meses_incumplidos") = mesesDevengados
            End If
            'CALCULO EDAD
            rowAfiAgregar.Item("edad") = DateDiff(DateInterval.Month, CDate(rowAfiAnterior.Item("fecha_nacimiento").ToString), UDThasta.Value) \ 12
            'Si Es listado de Jubilados y si la categoria es jubilado busco el ultimo pago de tot_debe de 22010100
            If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 6 And rowAfiAnterior.Item("scu_categoria").ToString = 170101 Then
                DAListado = cnn.consultaBDadapter(
                    "totales",
                    "tot_debe",
                    "tot_tipdoc='" & rowAfiAnterior.Item("afi_tipdoc") & "' AND
                    tot_nrodoc='" & rowAfiAnterior.Item("afi_nrodoc") & "' AND
                    tot_nropla = 22010100 AND
                    tot_estado <> '9' AND
                    tot_debe > 0 AND
                    tot_fecha BETWEEN '" &
                    Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" &
                    Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59'
                    ORDER BY tot_fecha DESC LIMIT 0,1"
                )
                Dim DSAfiliado As DataSet = New DataSet
                DAListado.Fill(DSAfiliado, "ultimo_debe_jubilado")
                If DSAfiliado.Tables("ultimo_debe_jubilado").Rows.Count() > 0 Then
                    rowAfiAgregar.Item("importe_capitalizado") = DSAfiliado.Tables("ultimo_debe_jubilado").Rows(0).Item("tot_debe")
                End If
            End If
            DTAfiliadoClone.ImportRow(rowAfiAgregar) 'Importo el afiliado
        End If
        rowAfiAgregar = RowAfi 'Seteo el nuevo afiliado para agregar
        rowAfiAgregar.Item("situacion_actual") = RowAfi.Item("scu_vigencia")
        'Si Es listado de Jubilados y si la categoria es jubilado seteo la Fecha Alta Beneficio y Fecha Primer Pago
        If DTListado.Rows(BSListado.Position).Item("lis_codigo").ToString.Trim = 6 And RowAfi.Item("scu_categoria").ToString = 170101 Then
            rowAfiAgregar.Item("matriculacion") = RowAfi.Item("scu_vigencia")
        End If
        'RESET VALUES
        mesesDevengados = 0
        mesesInactividad = 0
        'CALCULO
        CalculoMeses(RowAfi, rowAfiAnterior, mesesDevengados, mesesInactividad, True, False)
    End Sub
    Private Sub CalculoMeses(ByRef RowAfi As DataRow, ByRef rowAfiAnterior As DataRow, ByRef mesesDevengados As Integer, ByRef mesesInactividad As Integer, ByVal esPrimero As Boolean, ByVal esUltimo As Boolean)
        Dim fechaDesde As Date
        Dim fechaHasta As Date
        If IsNothing(rowAfiAnterior) Or esPrimero Then
            'Comento esto porque sumaria la cantidad de meses desde 01/01/2000 al primer registro historial
            'fechaDesde = CDate(Format(UDTdesde.Value, "yyyy-MM-dd"))
            'fechaHasta = CDate(RowAfi.Item("scu_vigencia").ToString)
        ElseIf esUltimo Then
            fechaDesde = CDate(rowAfiAnterior.Item("scu_vigencia").ToString)
            fechaHasta = CDate(Format(UDThasta.Value, "yyyy-MM-dd"))
        Else
            fechaDesde = CDate(rowAfiAnterior.Item("scu_vigencia").ToString)
            fechaHasta = CDate(RowAfi.Item("scu_vigencia").ToString)
        End If
        If fechaDesde >= UDTdesde.Value And fechaHasta >= UDTdesde.Value Then
            If Not esUltimo Then
                If Mid(RowAfi.Item("scu_categoria_anterior"), 1, 2) = "11" Or Mid(RowAfi.Item("scu_categoria_anterior"), 1, 2) = "12" Then
                    'Calculo meses devengados desde Fecha inicio aporte a Fecha situacion actual
                    mesesDevengados += DateDiff(DateInterval.Month, fechaDesde, fechaHasta)
                Else
                    'Calculo meses de inactividad teniendo en cuenta todos los meses que no estuvo en categoria activo
                    mesesInactividad += DateDiff(DateInterval.Month, fechaDesde, fechaHasta)
                End If
            Else
                If Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "11" Or Mid(rowAfiAnterior.Item("scu_categoria"), 1, 2) = "12" Then
                    'Calculo meses devengados desde Fecha inicio aporte a Fecha situacion actual
                    mesesDevengados += DateDiff(DateInterval.Month, fechaDesde, fechaHasta)
                Else
                    'Calculo meses de inactividad teniendo en cuenta todos los meses que no estuvo en categoria activo
                    mesesInactividad += DateDiff(DateInterval.Month, fechaDesde, fechaHasta)
                End If
            End If
        End If
    End Sub
    Private Sub ListadoPadrones()
        Dim cuotas As New Calculos
        Dim DTPadron As New DataTable
        Dim cfiltro As String = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim & " GROUP BY tot_tipdoc,tot_nrodoc"
        Dim cTablas As String = "((totales inner join plancuen on pla_nropla=tot_nropla) inner join afiliado on tot_tipdoc=afi_tipdoc and tot_nrodoc=afi_nrodoc) inner join cuentas on afi_tipdoc = cue_tipdoc AND afi_nrodoc = cue_nrodoc AND afi_titulo = cue_titulo"
        Dim DTMovimientos As New DataTable
        Dim DTAfiliadoClone As DataTable
        Dim cColumnas As String = ""
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For I As Integer = 1 To nOrdenSeleccion
            For Each item As ListViewItem In ListViewAfil.Items
                If item.Checked And CInt(item.SubItems(2).Text) = I Then
                    If cColumnas.Trim <> "" Then
                        cColumnas += ","
                    End If
                    cColumnas += item.SubItems(1).Text
                End If
            Next
        Next
        'Si no selecciono campos obtiene los definidos en la tabla del listado seleccionado lis_campos
        If cColumnas = "" Then
            'Si permite editar listado entra
            If permiteEditarListado Then
                cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
            ElseIf DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim = "*" Then
                cColumnas = "afi_nombre AS Afiliado"
            Else
                cColumnas = DTListado.Rows(BSListado.Position).Item("lis_campos").ToString.Trim
            End If
        End If
        cColumnas += ",afi_tipdoc,afi_nrodoc,0 as cuotas,afi_debitos as importe,afi_debitos as total,afi_debitos as interes,0 as CantCuotas,afi_debitos as TotCuotas,afi_debitos as TotIntereses,afi_debitos as CapInv,afi_debitos as FonComp,afi_debitos as GasAdm,afi_debitos as ServSoc,afi_debitos as TotalACI,sum(tot_debe-tot_haber) as Saldo"
        cfiltro = DTListado.Rows(BSListado.Position).Item("lis_condicion").ToString.Trim
        'Si se definio una relacion con otra tabla se la agrega
        If DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim <> "" Then
            cTablas += DTListado.Rows(BSListado.Position).Item("lis_relaciones").ToString.Trim
        End If
        If txCuenta.TextLength > 0 Then
            Dim cuentasFiltro As String
            If txCuenta.Text.Contains(",") Then
                cuentasFiltro = " AND tot_nropla IN (" & txCuenta.Text & ")"
            Else
                cuentasFiltro = " AND tot_nropla='" & txCuenta.Text & "'"
            End If
            cfiltro &= cuentasFiltro & " AND tot_estado<>'9'"
            cfiltro &= " AND ((tot_debe > 0 AND tot_fecha<='" & Format(UDTdesde.Value, "yyyy-MM-dd") & " 23:59:59') OR (tot_haber > 0 AND tot_fecha <= '" & Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59'))"
            cfiltro &= " GROUP BY tot_tipdoc,tot_nrodoc ORDER BY tot_titulo,tot_matricula"

            DAListado = cnn.consultaBDadapter(cTablas, cColumnas, cfiltro)
            DTAfiliado = New DataTable
            DAListado.Fill(DTAfiliado)

            DTAfiliado.Columns.Add("Select", GetType(Boolean))
            DTAfiliado.Columns("Select").SetOrdinal(0)

            DTAfiliadoClone = DTAfiliado.Clone

            Dim nCantCuo As Integer = 0
            Dim nTotCantCuo As Integer = 0
            Dim nMonCuo As Double = 0
            Dim nMonInteres As Double = 0
            Dim nTotMonCuo As Double = 0
            Dim nTotMonIntereses As Double = 0
            Dim nTotSaldo As Double = 0
            Dim nRegCuo As Integer = 0
            UltraProgressBar1.Value = 0
            UltraProgressBar1.Maximum = DTAfiliado.Rows.Count
            UltraProgressBar1.Minimum = 0
            For Each RowAfi As DataRow In DTAfiliado.Rows
                Application.DoEvents()

                RowAfi.Item("Select") = False

                If ComboBox1.Text.Trim <> "" Then
                    If oSC.Eval(RowAfi.Item("Saldo").ToString.Replace(",", ".") & ComboBox1.Text & UCESaldo.Value.ToString.Replace(",", ".")) Then
                        DTAfiliadoClone.ImportRow(RowAfi)
                    End If
                Else
                    DTAfiliadoClone.ImportRow(RowAfi)
                End If

                nMonCuo = 0
                nCantCuo = 0
                nTotCantCuo = 0
                nMonInteres = 0
                nTotMonCuo = 0
                nTotMonIntereses = 0
                nTotSaldo = 0
                nRegCuo = 0
                UltraProgressBar1.Value += 1
            Next

            UGlistados.DataSource = DTAfiliadoClone
            With UGlistados.DisplayLayout.Bands(0)
                .Columns(DTAfiliadoClone.Columns.Count - 12).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 11).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 10).Format = "N2"

                .Columns(DTAfiliadoClone.Columns.Count - 8).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 7).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 6).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 5).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 4).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 3).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 2).Format = "N2"
                .Columns(DTAfiliadoClone.Columns.Count - 1).Format = "N2"
            End With
        End If
    End Sub

    Private Sub BSListado_PositionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles BSListado.PositionChanged
        txCuenta.Text = DTListado.Rows(BSListado.Position).Item("lis_cuentas")
    End Sub

    Private Sub ImprimirReclamos()
        Dim objWord As New Word.Application
        Dim Documento As New Word.Document
        Dim nombreDoc As String
        ' Dim rowReclamo As DataRow
        For Each rowReclamo As DataRow In DTPadron.Rows
            If Not IsDBNull(rowReclamo.Item("Select")) AndAlso rowReclamo.Item("Select") Then
                Try
                    Select Case txCuenta.Text
                        Case "13040100"
                            nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Aportes " & rowReclamo.Item("afi_nombre").ToString.Trim & ".doc"
                            FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\NOTA RECLAMO DEUDA APORTES-Dr  KAPEICA-10-2014.-Final.doc", nombreDoc)

                            'nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Aportes " & rowReclamo.Item("afi_nombre").ToString.Trim & ".doc"
                            'FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Aportes Sipres.doc", nombreDoc)
                        Case "13051000"
                            nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Convenio Especial " & rowReclamo.Item("afi_nombre").ToString.Trim & ".doc"
                            FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Ayuda Personal.doc", nombreDoc)
                        Case "13051600"
                            nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Convenio Especial " & rowReclamo.Item("afi_nombre").ToString.Trim & ".doc"
                            FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Convenio Especial.doc", nombreDoc)
                        Case Else
                            MessageBox.Show("No existe nota de reclamo para esta servicio")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Exit Sub
                End Try

                objWord = New Word.Application
                Documento = objWord.Documents.Open(nombreDoc)

                Documento.Bookmarks.Item("NombreProfesional").Range.Text = rowReclamo.Item("afi_nombre")
                Documento.Bookmarks.Item("CalleProfesional").Range.Text = rowReclamo.Item("afi_direccion")
                Documento.Bookmarks.Item("CiudadProfesional").Range.Text = rowReclamo.Item("afi_localidad")
                Documento.Bookmarks.Item("Dia").Range.Text = Now.Day
                Documento.Bookmarks.Item("Mes").Range.Text = MonthName(Now.Month)
                Documento.Bookmarks.Item("Ano").Range.Text = Now.Year
                objWord.ActiveDocument.PrintOut()
                objWord.ActiveDocument.Close()
            End If
        Next
        MessageBox.Show("Finalizo la impresion de notas de reclamos")
    End Sub

    Private Sub CBtNotaReclamos_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtNotaReclamos.Click
        ImprimirReclamos()
    End Sub

    Private Sub CBtAcualizar_Click(sender As Object, e As EventArgs) Handles CBtAcualizar.Click

    End Sub

    Private Sub CBtNotaReclamos_Click(sender As Object, e As EventArgs) Handles CBtNotaReclamos.Click

    End Sub
End Class