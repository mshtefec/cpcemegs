﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win.UltraWinGrid
Imports Infragistics.Win.UltraWinEditors

Public Class FrmMoviCheques
    Private cnn As New ConsultaBD(True)
    Dim DAMoviCheque As MySqlDataAdapter
    Dim DTMoviCheque As DataTable
    Private clsExportarExcel As New ExportarExcel

    Private Sub FrmMoviCheques_Load(sender As Object, e As EventArgs) Handles Me.Load
        UDTFechaVencimientoDesde.Value = Date.Today
        UDTFechaVencimiento.Value = Date.Today.AddMonths(6)
        MuestraMovimientoChequesQuery()
        loadValEstados()
    End Sub
    Private Sub UltraTextEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTEmoviCheque.EditorButtonClick
        DAMoviCheque = cnn.consultaBDadapter(
            "totales left join afiliado on afi_tipdoc=tot_tipdes and afi_nrodoc=tot_nrodes",
            "tot_nrocheque AS Cheque, tot_estche AS Estado, tot_fecha AS Fecha, tot_fecdif AS Diferida, tot_unegos AS Inst,
            tot_nrocli AS Deleg, tot_proceso AS Proceso, tot_nrocom AS NroCom, tot_nroasi AS asiento, tot_debe AS Debe,
            tot_haber AS Haber, tot_nropla AS Cuenta, afi_nombre AS Destinatario, concat(afi_titulo,afi_matricula) AS Mtr",
            "tot_nrocheque = " & UTEmoviCheque.Value & "
            AND tot_nrocheque > 0
            AND tot_estado <> '9'
            ORDER BY tot_fecha DESC"
        )
        MuestraMovimientoCheques()
    End Sub
    Private Sub UDTFechaVencimiento_EditorButtonClick(sender As Object, e As EditorButtonEventArgs) Handles UDTFechaVencimiento.EditorButtonClick
        MuestraMovimientoChequesQuery()
    End Sub

    Private Sub MuestraMovimientoChequesQuery()
        DAMoviCheque = cnn.consultaBDadapter(
            "totales AS t 
                LEFT JOIN afiliado AS a ON (a.afi_tipdoc = tot_tipdes AND a.afi_nrodoc = tot_nrodes)
                LEFT JOIN comproba AS c ON (t.tot_nroasi = c.com_nroasi)
                LEFT JOIN afiliado AS b ON (c.com_tipdoc = b.afi_tipdoc AND c.com_nrodoc = b.afi_nrodoc)",
            "t.tot_nrocheque AS Cheque,
                t.tot_estche AS Estado, 
                t.tot_fecha AS Fecha, 
                t.tot_fecdif AS Diferida, 
                t.tot_unegos AS Inst,
                t.tot_nrocli AS Deleg, 
                t.tot_proceso AS Proceso, 
                t.tot_nrocom AS NroCom, 
                t.tot_nroasi AS asiento, 
                t.tot_debe AS Debe,
                t.tot_haber AS Haber, 
                t.tot_nropla AS Cuenta, 
                a.afi_nombre AS Destinatario, 
                concat(a.afi_titulo,a.afi_matricula) AS Mtr,
                b.afi_nrodoc AS Dni,
                b.afi_nombre AS Afiliado",
            "t.tot_fecdif BETWEEN '" & Format(UDTFechaVencimientoDesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" & Format(UDTFechaVencimiento.Value, "yyyy-MM-dd") & " 23:59:59'
            AND t.tot_nrocheque > 0
            AND t.tot_estado <> '9'
            ORDER BY t.tot_fecha DESC"
        )
        MuestraMovimientoCheques()
    End Sub
    Private Sub MuestraMovimientoCheques()
        DTMoviCheque = New DataTable
        DAMoviCheque.Fill(DTMoviCheque)
        UGMoviCheque.DataSource = DTMoviCheque
        UGMoviCheque.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
        With UGMoviCheque.DisplayLayout.Bands(0)
            .Columns(1).Width = 100
            .Columns(1).Style = ColumnStyle.DropDownList
            .Columns(1).ValueList = valEstados
            .Columns(2).Width = 130
            .Columns(3).Width = 130
            .Columns(4).Width = 30
            .Columns(5).Width = 30
            .Columns(6).Width = 60
            .Columns(9).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(9).Width = 90
            .Columns(9).Format = "c"
            .Columns(9).PromptChar = ""
            .Columns(9).Style = ColumnStyle.Double
            .Columns(9).FormatInfo = Globalization.CultureInfo.CurrentCulture
            .Columns(10).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(10).Width = 90
            .Columns(10).Format = "c"
            .Columns(10).PromptChar = ""
            .Columns(10).Style = ColumnStyle.Double
            .Columns(10).FormatInfo = Globalization.CultureInfo.CurrentCulture
            .Columns(11).Width = 70
            .Columns(12).Width = 200
        End With
    End Sub

    Private Sub CBtExportar_Click(sender As Object, e As EventArgs) Handles CBtExportar.Click
        Dim cTitulo(0) As String
        cTitulo(0) = "Movimiento de Cheques - Periodo desde: " & UDTFechaVencimientoDesde.Value & " hasta: " & UDTFechaVencimiento.Value
        clsExportarExcel.ExportarDatosExcel(UGMoviCheque, cTitulo)
    End Sub
End Class