﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win

Public Class FrmLibroMayor
    Private cnn As New ConsultaBD(True)
    Dim DScuenta As DataSet
    Dim DAcuenta As MySqlDataAdapter

    Private DAFarcat As MySqlDataAdapter
    Private DTFarcat As DataTable

    Private SoloLetras As String

    Private Sub CargaPlanCuenta()
        If cnn.AbrirConexion Then
            DAcuenta = cnn.consultaBDadapter("plancuen", "pla_nropla,pla_nombre", "1 ORDER BY pla_nombre")
            DScuenta = New DataSet
            DAcuenta.Fill(DScuenta, "cuentas")
            UCcuenta.DataSource = DScuenta.Tables(0)
            ' UCcuenta.DisplayLayout.Bands(0).Columns(0).Hidden = True
            UCcuenta.DisplayLayout.Bands(0).Columns(1).Width = 300
            UCcuenta.DisplayLayout.Bands(0).Columns(1).Header.Caption = "Cuenta"
            UCcuenta.DisplayMember = "pla_nombre"
            UCcuenta.ValueMember = "pla_nropla"
            cnn.CerrarConexion()
        End If
    End Sub

    Private Sub FrmLibroMayor_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        'Cargo el farcat en la grilla
        Try
            'CargaPlanCuenta()
            UDTdesde.Value = Now
            UDThasta.Value = Now
            DAFarcat = cnn.consultaBDadapter("farcat", , )
            'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
            DTFarcat = New DataTable
            DAFarcat.Fill(DTFarcat)

            If DTFarcat.Rows.Count > 0 Then
                For Each rowFarcat As DataRow In DTFarcat.Rows
                    UltraTree1.Nodes.Add("" & rowFarcat.Item("far_nombre").ToString & " " & rowFarcat.Item("far_sucursal").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
                Next
            End If

            For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
                Nodo.CheckedState = CheckState.Checked
            Next

            UltraTree1.Focus()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar las Sucursales", "SUCURSALES", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End Try
    End Sub

    Private Sub UCcuenta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub CBtProcesar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtProcesar.Click
        'If Not IsNothing(Me.TxCuenta.Text) AndAlso Me.TxCuenta.TextLength > 0 Then
        ProcesaLibroMayor()
        'Else
        'MessageBox.Show("Debe ingresar una Cuenta.Campo obligatorio", "LIBRRO MAYOR", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        'Me.TxCuenta.Focus()
        'End If
    End Sub

    Private Sub ProcesaLibroMayor()
        Try
            If cnn.AbrirConexion Then
                'SALDO
                Dim DTMayor As DataTable
                DTMayor = New DataTable
                DTMayor = GeneroLibro()
                If RadioButton1.Checked Then
                    Dim frmResumen As New FrmReportes(DTMayor, , "CRLibroMayor.rpt", "Libro Mayor")
                    frmResumen.ShowDialog()
                End If
                If RadioButton2.Checked Then
                    Dim frmResumen As New FrmReportes(DTMayor, , "CRLibroMayorSubcuenta.rpt", "Libro Mayor agrupado por subcuenta")
                    frmResumen.ShowDialog()
                End If
                If RadioButton3.Checked Then
                    Dim frmResumen As New FrmReportes(DTMayor, , "CRLibroMayorProceso.rpt", "Libro Mayor agrupado por proceso")
                    frmResumen.ShowDialog()
                End If
            Else
                MessageBox.Show("No se pudo establecer la conexion con el servidor. Por Favor Verifique", "LIBRO MAYOR", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1)
            End If

        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error " & ex.Message, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            cnn.CerrarConexion()
        End Try
    End Sub

    Private Function GeneroLibro() As DataTable
        Dim DALibro As New MySqlDataAdapter
        Dim DSLibro As New DataSet
        Dim DTLibro As New DataTable
        Dim nSaldoAnterior As Double = 0
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty
        Dim cConsulta As String = String.Empty
        Dim nSaldo As Double = 0
        Dim xReg As Integer = 0
        Dim desde As Integer
        Dim hasta As Integer
        Dim sumador As Integer
        'Cargo las unidades de negocio el y nro de cliente
        For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
            If Nodo.CheckedState = CheckState.Checked Then
                If cInstituciones.Length = 0 Then
                    cUnegos = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                    cInstituciones = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                Else
                    If Not cUnegos.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))) Then
                        cUnegos += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                    End If
                    If Not cInstituciones.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))) Then
                        cInstituciones += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                    End If
                End If
            End If
        Next
        'Armo la consulta
        If Not IsNothing(Me.TxProceso.Text) AndAlso Me.TxProceso.TextLength > 0 Then
            If cConsulta.Length > 0 Then
                cConsulta &= " and "
            End If
            cConsulta &= "tot_proceso like '" & Me.TxProceso.Text.Trim & "%'"
        End If
        If Not IsNothing(Me.TxCuenta.Text) AndAlso Me.TxCuenta.TextLength > 0 Then
            If cConsulta.Length > 0 Then
                cConsulta &= " and "
            End If
            cConsulta &= "tot_nropla = '" & Me.TxCuenta.Text & "'"
        End If
        If Not IsNothing(Me.TxMatricula.Text) AndAlso Me.TxMatricula.TextLength > 0 Then
            If cConsulta.Length > 0 Then
                cConsulta &= " and "
            End If

            Dim Titulo As String
            Dim Matricula As Long
            SoloLetras = ""

            Matricula = SoloNumeros(Me.TxMatricula.Text)
            Titulo = SoloLetras

            cConsulta &= "tot_titulo = '" & Trim(Titulo) & "' and tot_matricula = " & Matricula & ""
        Else
            If Not IsNothing(Me.TxSubCuenta.Text) AndAlso Me.TxSubCuenta.TextLength > 0 Then
                If cConsulta.Length > 0 Then
                    cConsulta &= " and "
                End If
                cConsulta &= "tot_subpla = '" & Trim(Me.TxSubCuenta.Text) & "'"
            End If
        End If

        DALibro = cnn.consultaBDadapter("totales", "sum(tot_debe - tot_haber) as Saldo", "" & cConsulta & " and tot_fecha < '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_estado <> '9'")
        DALibro.Fill(DSLibro, "saldo")
        If DSLibro.Tables(0).Rows.Count > 0 Then
            If Not IsDBNull(DSLibro.Tables(0).Rows(0).Item("Saldo")) Then
                nSaldoAnterior = DSLibro.Tables(0).Rows(0).Item("Saldo")
            Else
                nSaldoAnterior = 0
            End If
        End If

        DSLibro.Clear()

        If RadioButton1.Checked Then ' detallado         
            'Saldo Agrupado por Nro Plan???
            'DALibro = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_debe - tot_haber) as Saldo", "" & cConsulta & " and tot_fecha < '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") group by tot_nropla")
            'DALibro = cnn.consultaBDadapter("(((totales inner join comproba on com_unegos=tot_unegos and com_nrocli=tot_nrocli and com_nroasi=tot_nroasi) left Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) inner Join plancuen ON pla_nropla = tot_nropla) left join afiliado ON afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta, CONCAT(totales.tot_titulo,'',CAST(totales.tot_matricula AS char)) as SubCuenta,afi_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,com_concepto1 as concepto,concat(com_concepto2,' ',com_concepto3) as concepto1,tot_debe as debe,tot_haber as haber,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom,' ',IF(tot_nrocheque>0,IF(tot_letcheque='T','Tra.N°','Che.N°'),''),IF(tot_nrocheque>0,tot_nrocheque,''))as Char) as operacion,tot_tipdoc,tot_nrodoc", "tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_estado <> '9' ORDER BY tot_fecha")
            DALibro = cnn.consultaBDadapter("((totales left Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) left Join plancuen ON pla_nropla = tot_nropla) left join afiliado ON afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta, CONCAT(totales.tot_titulo,'',CAST(totales.tot_matricula AS char)) as SubCuenta,afi_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,'' as concepto,'' as concepto1,tot_debe as debe,tot_haber as haber,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom,' ',IF(tot_nrocheque>0,IF(tot_letcheque='T','Tra.N°','Che.N°'),''),IF(tot_nrocheque>0,tot_nrocheque,''))as Char) as operacion,tot_tipdoc,tot_nrodoc", "tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_estado <> '9' ORDER BY tot_fecha")
            DALibro.Fill(DTLibro)
        End If

        If RadioButton2.Checked Then ' agrupado por subcuenta
            'DALibro = cnn.consultaBDadapter("(((totales left join comproba on com_unegos=tot_unegos and com_nrocli=tot_nrocli and com_nroasi=tot_nroasi) Inner Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) Inner Join plancuen ON pla_nropla = tot_nropla) inner join afiliado ON afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta,CONCAT(totales.tot_titulo,'',CAST(totales.tot_matricula AS char)) as SubCuenta,afi_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,com_concepto1 as concepto,sum(tot_debe) as debe,sum(tot_haber) as haber,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom)as Char) as operacion,tot_tipdoc,tot_nrodoc", "tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_estado <> '9' GROUP BY tot_tipdoc,tot_nrodoc ORDER BY afi_matricula")
            DALibro = cnn.consultaBDadapter(
                "(((totales left join comproba on com_unegos=tot_unegos and com_nrocli=tot_nrocli and com_nroasi=tot_nroasi) Inner Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) Inner Join plancuen ON pla_nropla = tot_nropla) inner join afiliado ON afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc",
                "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta,CONCAT(totales.tot_titulo,'',CAST(totales.tot_matricula AS char)) as SubCuenta,afi_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,com_concepto1 as concepto," &
                "SUM(IF(tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59', tot_debe, 0)) AS debe,SUM(IF(tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59', tot_haber, 0)) AS haber," &
                "sum(tot_debe) as debeTotal,sum(tot_haber) as haberTotal,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom)as Char) as operacion,tot_tipdoc,tot_nrodoc",
                "tot_fecha BETWEEN '2000-01-01 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_estado <> '9' GROUP BY tot_tipdoc,tot_nrodoc ORDER BY afi_matricula"
            )
            'DALibro = cnn.consultaBDadapter("(((totales left join comproba on com_unegos=tot_unegos and com_nrocli=tot_nrocli and com_nroasi=tot_nroasi) Inner Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) Inner Join plancuen ON pla_nropla = tot_nropla) inner join afiliado ON afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc", "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta,CONCAT(totales.tot_titulo,'',CAST(totales.tot_matricula AS char)) as SubCuenta,afi_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,com_concepto1 as concepto,sum(tot_debe) as debe,sum(tot_haber) as haber,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom)as Char) as operacion,tot_tipdoc,tot_nrodoc", "tot_fecha between '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & "' and '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_estado <> '9' GROUP BY tot_tipdoc,tot_nrodoc ORDER BY afi_matricula")
            DALibro.Fill(DTLibro)
            desde = DTLibro.Rows.Count - 1
            hasta = 0
            sumador = -1
            For i As Integer = desde To hasta Step sumador
                nSaldo = DTLibro.Rows.Item(i).Item("debeTotal") - DTLibro.Rows.Item(i).Item("haberTotal")
                If nSaldo = 0 Then
                    DTLibro.Rows.RemoveAt(i)
                End If
            Next
        End If

        If RadioButton3.Checked Then ' agrupado por proceso            
            DALibro = cnn.consultaBDadapter("(((totales inner join comproba on com_unegos=tot_unegos and com_nrocli=tot_nrocli and com_nroasi=tot_nroasi) Inner Join farcat ON far_unegos = tot_unegos and far_nrocli = tot_nrocli) Inner Join plancuen ON pla_nropla = tot_nropla) inner join procesos ON pro_codigo=tot_proceso", "tot_nropla as cuenta,far_nombre as entidad, pla_nombre as descricuenta,pro_codigo as SubCuenta,pro_nombre as descrisubcuenta,CAST(tot_fecha as char) as fecha,tot_nroasi as asiento,com_concepto1 as concepto,sum(tot_debe) as debe,sum(tot_haber) as haber,tot_proceso as proceso,CAST(CONCAT(totales.tot_unegos,'-',totales.tot_proceso,'-',totales.tot_nrocli,'-',totales.tot_nrocom)as Char) as operacion,tot_tipdoc,tot_nrodoc", "tot_fecha BETWEEN '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' AND " & cConsulta & " AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") AND tot_estado <> '9' GROUP BY tot_proceso")
            DALibro.Fill(DTLibro)
        End If

        DTLibro.Columns.Add("delegacion", GetType(String))
        DTLibro.Columns.Add("desde", GetType(String))
        DTLibro.Columns.Add("hasta", GetType(String))
        DTLibro.Columns.Add("saldo", GetType(Double))
        DTLibro.Columns.Add("saldoanterior", GetType(Double))

        nSaldo = nSaldoAnterior
        UltraProgressBar1.Minimum = 0
        UltraProgressBar1.Maximum = DTLibro.Rows.Count

        If RadioButton1.Checked Or RadioButton3.Checked Then
            desde = 0
            hasta = DTLibro.Rows.Count - 1
            sumador = +1
        End If
        If RadioButton2.Checked Then
            desde = DTLibro.Rows.Count - 1
            hasta = 0
            sumador = -1
        End If
        For i As Integer = desde To hasta Step sumador
            Application.DoEvents()
            If RadioButton1.Checked Then
                nSaldo += DTLibro.Rows.Item(i).Item("debe") - DTLibro.Rows.Item(i).Item("haber")
                'End If
                ' si es detallado busco comproba
                'If RadioButton1.Checked Then
                DALibro = cnn.consultaBDadapter("comproba", "com_concepto1 as concepto,concat(com_concepto2,' ',com_concepto3) as concepto1, com_destinatario", "com_nroasi=" & DTLibro.Rows.Item(i).Item("asiento"))
                DSLibro = New DataSet
                DALibro.Fill(DSLibro, "comproba")
                If DSLibro.Tables(0).Rows.Count > 0 Then
                    DTLibro.Rows.Item(i).Item("concepto") = DSLibro.Tables(0).Rows(0).Item("concepto")
                    DTLibro.Rows.Item(i).Item("concepto1") = DSLibro.Tables(0).Rows(0).Item("concepto1")
                    If Not IsDBNull(DTLibro.Rows.Item(i).Item("SubCuenta")) And Not IsDBNull(DSLibro.Tables(0).Rows(0).Item("com_destinatario")) Then
                        If DTLibro.Rows.Item(i).Item("SubCuenta") = "0" And DTLibro.Rows.Item(i).Item("haber") > 0 And Not String.IsNullOrEmpty(DSLibro.Tables(0).Rows(0).Item("com_destinatario")) Then
                            DTLibro.Rows.Item(i).Item("SubCuenta") = "SD"
                            DTLibro.Rows.Item(i).Item("descrisubcuenta") = DSLibro.Tables(0).Rows(0).Item("com_destinatario")
                        End If
                    End If
                End If
            End If

            If RadioButton2.Checked Then
                DALibro = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_debe - tot_haber) as Saldo", " " & cConsulta & " and tot_fecha < '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 23:59:59' AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_tipdoc='" & DTLibro.Rows.Item(i).Item("tot_tipdoc") & "' and tot_nrodoc=" & DTLibro.Rows.Item(i).Item("tot_nrodoc") & " and tot_estado <> '9' group by tot_nropla")
                'DALibro = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_debe - tot_haber) as Saldo", " " & cConsulta & " and tot_fecha < '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & "' and tot_tipdoc='" & row.Item("tot_tipdoc") & "' and tot_nrodoc=" & row.Item("tot_nrodoc") & " and tot_estado <> '9' group by tot_subpla")

                DSLibro = New DataSet
                DALibro.Fill(DSLibro, "saldo")
                If DSLibro.Tables(0).Rows.Count > 0 Then
                    nSaldoAnterior = DSLibro.Tables(0).Rows(0).Item("Saldo")
                Else
                    nSaldoAnterior = 0
                End If
                nSaldo = nSaldoAnterior + DTLibro.Rows.Item(i).Item("debe") - DTLibro.Rows.Item(i).Item("haber")
            End If

            If RadioButton3.Checked Then
                DALibro = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_debe - tot_haber) as Saldo", "" & cConsulta & " and tot_fecha < '" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 23:59:59' AND tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") and tot_proceso='" & DTLibro.Rows.Item(i).Item("proceso") & "' and tot_estado <> '9' group by tot_proceso")
                DSLibro = New DataSet
                DALibro.Fill(DSLibro, "saldo")
                If DSLibro.Tables(0).Rows.Count > 0 Then
                    nSaldoAnterior = DSLibro.Tables(0).Rows(0).Item("Saldo")
                Else
                    nSaldoAnterior = 0
                End If
                nSaldo = nSaldoAnterior + DTLibro.Rows.Item(i).Item("debe") - DTLibro.Rows.Item(i).Item("haber")
            End If

            DTLibro.Rows.Item(i).Item("saldoanterior") = nSaldoAnterior
            DTLibro.Rows.Item(i).Item("saldo") = nSaldo
            DTLibro.Rows.Item(i).Item("desde") = UDTdesde.Text
            DTLibro.Rows.Item(i).Item("hasta") = UDThasta.Text
            DTLibro.Rows.Item(i).Item("entidad") = cUnegos
            DTLibro.Rows.Item(i).Item("delegacion") = cInstituciones
            If RadioButton2.Checked And nSaldo = 0 Then
                DTLibro.Rows.RemoveAt(i)
            End If
            xReg += 1
            UltraProgressBar1.Value = xReg
            UltraProgressBar1.Refresh()
        Next

        If DTLibro.Rows.Count > 0 Then
            Return DTLibro
        Else
            Return Nothing
        End If
    End Function


    Private Sub Tabular_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraTree1.KeyPress, UltraGroupBox1.KeyPress, UDThasta.KeyPress, UDTdesde.KeyPress, UCcuenta.KeyPress, TxSubCuenta.KeyPress, TxProceso.KeyPress, TxCuenta.KeyPress, CBtProcesar.KeyPress, RadioButton3.KeyPress, RadioButton2.KeyPress, RadioButton1.KeyPress, TxMatricula.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Public Function SoloNumeros(ByVal strCadena As String) As Object
        Dim SoloNumero As String = ""

        Dim index As Integer
        For index = 1 To Len(strCadena)
            If (Mid$(strCadena, index, 1) Like "#") Or Mid$(strCadena, index, 1) = "-" Then
                SoloNumero = SoloNumero & Mid$(strCadena, index, 1)
            Else
                SoloLetras = SoloLetras & Mid$(strCadena, index, 1)
            End If
        Next
        Return SoloNumero
    End Function




End Class