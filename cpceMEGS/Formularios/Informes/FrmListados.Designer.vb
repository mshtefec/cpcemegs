﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmListadosGerencialesDeudores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListadosGerencialesDeudores))
        Me.ListViewAfil = New System.Windows.Forms.ListView()
        Me.UGlistados = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtAcualizar = New System.Windows.Forms.Button()
        Me.txCuenta = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CBtListar = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.UCESaldo = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.CBListadoMorosos = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.CBtNotaReclamos = New System.Windows.Forms.Button()
        Me.CBListadoPadrones = New System.Windows.Forms.CheckBox()
        Me.TTAyuda = New System.Windows.Forms.ToolTip(Me.components)
        Me.CBListadoSipres = New System.Windows.Forms.CheckBox()
        Me.CBsfssFamiliares = New System.Windows.Forms.CheckBox()
        Me.UDTdebe = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.LabelFechaDebe = New System.Windows.Forms.Label()
        Me.UDThaber = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.LabelFechaHaber = New System.Windows.Forms.Label()
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCESaldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdebe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThaber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListViewAfil
        '
        Me.ListViewAfil.CheckBoxes = True
        Me.ListViewAfil.Dock = System.Windows.Forms.DockStyle.Left
        Me.ListViewAfil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListViewAfil.Location = New System.Drawing.Point(0, 0)
        Me.ListViewAfil.Name = "ListViewAfil"
        Me.ListViewAfil.Size = New System.Drawing.Size(269, 612)
        Me.ListViewAfil.TabIndex = 0
        Me.ListViewAfil.UseCompatibleStateImageBehavior = False
        Me.ListViewAfil.View = System.Windows.Forms.View.Details
        '
        'UGlistados
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGlistados.DisplayLayout.Appearance = Appearance1
        Me.UGlistados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGlistados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGlistados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGlistados.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGlistados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGlistados.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGlistados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGlistados.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGlistados.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGlistados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGlistados.Location = New System.Drawing.Point(269, 0)
        Me.UGlistados.Name = "UGlistados"
        Me.UGlistados.Size = New System.Drawing.Size(781, 474)
        Me.UGlistados.TabIndex = 1
        '
        'CBtAcualizar
        '
        Me.CBtAcualizar.BackColor = System.Drawing.Color.Transparent
        Me.CBtAcualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAcualizar.ImageIndex = 0
        Me.CBtAcualizar.Location = New System.Drawing.Point(273, 573)
        Me.CBtAcualizar.Name = "CBtAcualizar"
        Me.CBtAcualizar.Size = New System.Drawing.Size(162, 38)
        Me.CBtAcualizar.TabIndex = 116
        Me.CBtAcualizar.Text = "Actualizar"
        Me.CBtAcualizar.UseVisualStyleBackColor = False
        '
        'txCuenta
        '
        Me.txCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txCuenta.Location = New System.Drawing.Point(336, 474)
        Me.txCuenta.Name = "txCuenta"
        Me.txCuenta.Size = New System.Drawing.Size(285, 22)
        Me.txCuenta.TabIndex = 117
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(270, 477)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 16)
        Me.Label1.TabIndex = 118
        Me.Label1.Text = "Cuentas:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(279, 502)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 16)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Saldos"
        '
        'CBtListar
        '
        Me.CBtListar.BackColor = System.Drawing.Color.Transparent
        Me.CBtListar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtListar.ImageIndex = 0
        Me.CBtListar.Location = New System.Drawing.Point(446, 573)
        Me.CBtListar.Name = "CBtListar"
        Me.CBtListar.Size = New System.Drawing.Size(162, 38)
        Me.CBtListar.TabIndex = 121
        Me.CBtListar.Text = "Listar"
        Me.CBtListar.UseVisualStyleBackColor = False
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {">", "<", "=", ">=", "<=", "<>"})
        Me.ComboBox1.Location = New System.Drawing.Point(336, 499)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(46, 24)
        Me.ComboBox1.TabIndex = 118
        Me.TTAyuda.SetToolTip(Me.ComboBox1, "Si no selecciona ninguna opcion no controla el monto.")
        '
        'UCESaldo
        '
        Me.UCESaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCESaldo.Location = New System.Drawing.Point(388, 499)
        Me.UCESaldo.Name = "UCESaldo"
        Me.UCESaldo.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCESaldo.Size = New System.Drawing.Size(107, 24)
        Me.UCESaldo.TabIndex = 119
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(621, 573)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(162, 38)
        Me.CBtExportar.TabIndex = 123
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(634, 504)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 16)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "Hasta Fecha:"
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(723, 500)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 24)
        Me.UDThasta.TabIndex = 120
        Me.UDThasta.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'CBListadoMorosos
        '
        Me.CBListadoMorosos.AutoSize = True
        Me.CBListadoMorosos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBListadoMorosos.Location = New System.Drawing.Point(336, 530)
        Me.CBListadoMorosos.Name = "CBListadoMorosos"
        Me.CBListadoMorosos.Size = New System.Drawing.Size(146, 20)
        Me.CBListadoMorosos.TabIndex = 127
        Me.CBListadoMorosos.Text = "Listado de Morosos"
        Me.CBListadoMorosos.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(629, 477)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 128
        Me.Label4.Text = "Desde Fecha:"
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(723, 473)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 24)
        Me.UDTdesde.TabIndex = 129
        Me.UDTdesde.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(273, 550)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(765, 20)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 146
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'CBtNotaReclamos
        '
        Me.CBtNotaReclamos.BackColor = System.Drawing.Color.Transparent
        Me.CBtNotaReclamos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtNotaReclamos.ImageIndex = 0
        Me.CBtNotaReclamos.Location = New System.Drawing.Point(796, 573)
        Me.CBtNotaReclamos.Name = "CBtNotaReclamos"
        Me.CBtNotaReclamos.Size = New System.Drawing.Size(162, 38)
        Me.CBtNotaReclamos.TabIndex = 147
        Me.CBtNotaReclamos.Text = "Notas de Reclamos"
        Me.CBtNotaReclamos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBtNotaReclamos.UseVisualStyleBackColor = False
        '
        'CBListadoPadrones
        '
        Me.CBListadoPadrones.AutoSize = True
        Me.CBListadoPadrones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBListadoPadrones.Location = New System.Drawing.Point(488, 530)
        Me.CBListadoPadrones.Name = "CBListadoPadrones"
        Me.CBListadoPadrones.Size = New System.Drawing.Size(152, 20)
        Me.CBListadoPadrones.TabIndex = 148
        Me.CBListadoPadrones.Text = "Listado de Padrones"
        Me.TTAyuda.SetToolTip(Me.CBListadoPadrones, resources.GetString("CBListadoPadrones.ToolTip"))
        Me.CBListadoPadrones.UseVisualStyleBackColor = True
        '
        'CBListadoSipres
        '
        Me.CBListadoSipres.AutoSize = True
        Me.CBListadoSipres.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBListadoSipres.Location = New System.Drawing.Point(646, 530)
        Me.CBListadoSipres.Name = "CBListadoSipres"
        Me.CBListadoSipres.Size = New System.Drawing.Size(132, 20)
        Me.CBListadoSipres.TabIndex = 149
        Me.CBListadoSipres.Text = "Listado de Sipres"
        Me.TTAyuda.SetToolTip(Me.CBListadoSipres, resources.GetString("CBListadoSipres.ToolTip"))
        Me.CBListadoSipres.UseVisualStyleBackColor = True
        '
        'CBsfssFamiliares
        '
        Me.CBsfssFamiliares.AutoSize = True
        Me.CBsfssFamiliares.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBsfssFamiliares.Location = New System.Drawing.Point(905, 530)
        Me.CBsfssFamiliares.Name = "CBsfssFamiliares"
        Me.CBsfssFamiliares.Size = New System.Drawing.Size(145, 20)
        Me.CBsfssFamiliares.TabIndex = 150
        Me.CBsfssFamiliares.Text = "Cod. 400 Familiares"
        Me.TTAyuda.SetToolTip(Me.CBsfssFamiliares, resources.GetString("CBsfssFamiliares.ToolTip"))
        Me.CBsfssFamiliares.UseVisualStyleBackColor = True
        '
        'UDTdebe
        '
        Me.UDTdebe.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDTdebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdebe.Location = New System.Drawing.Point(944, 473)
        Me.UDTdebe.Name = "UDTdebe"
        Me.UDTdebe.Size = New System.Drawing.Size(97, 24)
        Me.UDTdebe.TabIndex = 154
        Me.UDTdebe.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'LabelFechaDebe
        '
        Me.LabelFechaDebe.AutoSize = True
        Me.LabelFechaDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFechaDebe.Location = New System.Drawing.Point(827, 477)
        Me.LabelFechaDebe.Name = "LabelFechaDebe"
        Me.LabelFechaDebe.Size = New System.Drawing.Size(114, 16)
        Me.LabelFechaDebe.TabIndex = 153
        Me.LabelFechaDebe.Text = "Fecha Max Debe:"
        '
        'UDThaber
        '
        Me.UDThaber.DateTime = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.UDThaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThaber.Location = New System.Drawing.Point(944, 500)
        Me.UDThaber.Name = "UDThaber"
        Me.UDThaber.Size = New System.Drawing.Size(97, 24)
        Me.UDThaber.TabIndex = 151
        Me.UDThaber.Value = New Date(2000, 1, 1, 0, 0, 0, 0)
        '
        'LabelFechaHaber
        '
        Me.LabelFechaHaber.AutoSize = True
        Me.LabelFechaHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFechaHaber.Location = New System.Drawing.Point(824, 504)
        Me.LabelFechaHaber.Name = "LabelFechaHaber"
        Me.LabelFechaHaber.Size = New System.Drawing.Size(118, 16)
        Me.LabelFechaHaber.TabIndex = 152
        Me.LabelFechaHaber.Text = "Fecha Max Haber:"
        '
        'FrmListadosGerencialesDeudores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1050, 612)
        Me.Controls.Add(Me.UDTdebe)
        Me.Controls.Add(Me.LabelFechaDebe)
        Me.Controls.Add(Me.UDThaber)
        Me.Controls.Add(Me.LabelFechaHaber)
        Me.Controls.Add(Me.CBsfssFamiliares)
        Me.Controls.Add(Me.CBListadoSipres)
        Me.Controls.Add(Me.CBListadoPadrones)
        Me.Controls.Add(Me.CBtNotaReclamos)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CBListadoMorosos)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.CBtListar)
        Me.Controls.Add(Me.UCESaldo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txCuenta)
        Me.Controls.Add(Me.CBtAcualizar)
        Me.Controls.Add(Me.UGlistados)
        Me.Controls.Add(Me.ListViewAfil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmListadosGerencialesDeudores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listados Padrones"
        CType(Me.UGlistados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCESaldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdebe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThaber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListViewAfil As System.Windows.Forms.ListView
    Friend WithEvents UGlistados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtAcualizar As System.Windows.Forms.Button
    Friend WithEvents txCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CBtListar As System.Windows.Forms.Button
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents UCESaldo As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents CBListadoMorosos As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents CBtNotaReclamos As System.Windows.Forms.Button
    Friend WithEvents CBListadoPadrones As System.Windows.Forms.CheckBox
    Friend WithEvents TTAyuda As System.Windows.Forms.ToolTip
    Friend WithEvents CBListadoSipres As System.Windows.Forms.CheckBox
    Friend WithEvents CBsfssFamiliares As CheckBox
    Friend WithEvents UDTdebe As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents LabelFechaDebe As Label
    Friend WithEvents UDThaber As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents LabelFechaHaber As Label
End Class
