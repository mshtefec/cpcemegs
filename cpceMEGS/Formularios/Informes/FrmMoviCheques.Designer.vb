﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMoviCheques
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMoviCheques))
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UTEmoviCheque = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UGMoviCheque = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelFecha = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UDTFechaVencimiento = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.PanelBotones = New System.Windows.Forms.Panel()
        Me.UDTFechaVencimientoDesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.PanelContenido = New System.Windows.Forms.Panel()
        Me.CBtExportar = New System.Windows.Forms.Button()
        CType(Me.UTEmoviCheque, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGMoviCheque, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTFechaVencimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelBotones.SuspendLayout()
        CType(Me.UDTFechaVencimientoDesde, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelContenido.SuspendLayout()
        Me.SuspendLayout()
        '
        'UTEmoviCheque
        '
        Appearance1.Image = CType(resources.GetObject("Appearance1.Image"), Object)
        EditorButton1.Appearance = Appearance1
        Appearance2.Image = CType(resources.GetObject("Appearance2.Image"), Object)
        EditorButton1.PressedAppearance = Appearance2
        EditorButton1.Width = 70
        Me.UTEmoviCheque.ButtonsRight.Add(EditorButton1)
        Me.UTEmoviCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEmoviCheque.Location = New System.Drawing.Point(77, 3)
        Me.UTEmoviCheque.Name = "UTEmoviCheque"
        Me.UTEmoviCheque.Size = New System.Drawing.Size(206, 28)
        Me.UTEmoviCheque.TabIndex = 0
        '
        'UGMoviCheque
        '
        Appearance3.BackColor = System.Drawing.Color.White
        Me.UGMoviCheque.DisplayLayout.Appearance = Appearance3
        Me.UGMoviCheque.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGMoviCheque.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGMoviCheque.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance4.BackColor = System.Drawing.Color.Transparent
        Me.UGMoviCheque.DisplayLayout.Override.CardAreaAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance5.FontData.BoldAsString = "True"
        Appearance5.FontData.Name = "Arial"
        Appearance5.FontData.SizeInPoints = 10.0!
        Appearance5.ForeColor = System.Drawing.Color.White
        Appearance5.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGMoviCheque.DisplayLayout.Override.HeaderAppearance = Appearance5
        Me.UGMoviCheque.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMoviCheque.DisplayLayout.Override.RowSelectorAppearance = Appearance6
        Appearance7.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance7.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMoviCheque.DisplayLayout.Override.SelectedRowAppearance = Appearance7
        Me.UGMoviCheque.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UGMoviCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGMoviCheque.Location = New System.Drawing.Point(0, 0)
        Me.UGMoviCheque.Name = "UGMoviCheque"
        Me.UGMoviCheque.Size = New System.Drawing.Size(1024, 470)
        Me.UGMoviCheque.TabIndex = 26
        Me.UGMoviCheque.Text = "Movimientos"
        '
        'LabelNumero
        '
        Me.LabelNumero.AutoSize = True
        Me.LabelNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelNumero.Location = New System.Drawing.Point(12, 8)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(59, 16)
        Me.LabelNumero.TabIndex = 27
        Me.LabelNumero.Text = "Número:"
        '
        'LabelFecha
        '
        Me.LabelFecha.AutoSize = True
        Me.LabelFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFecha.Location = New System.Drawing.Point(289, 8)
        Me.LabelFecha.Name = "LabelFecha"
        Me.LabelFecha.Size = New System.Drawing.Size(185, 16)
        Me.LabelFecha.TabIndex = 28
        Me.LabelFecha.Text = "Fecha de vencimiento desde:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(596, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 16)
        Me.Label1.TabIndex = 104
        Me.Label1.Text = "hasta:"
        '
        'UDTFechaVencimiento
        '
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        Appearance9.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance9.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton3.Appearance = Appearance9
        EditorButton3.Width = 30
        Me.UDTFechaVencimiento.ButtonsRight.Add(EditorButton3)
        Me.UDTFechaVencimiento.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTFechaVencimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTFechaVencimiento.Location = New System.Drawing.Point(646, 5)
        Me.UDTFechaVencimiento.Name = "UDTFechaVencimiento"
        Me.UDTFechaVencimiento.Size = New System.Drawing.Size(135, 24)
        Me.UDTFechaVencimiento.TabIndex = 103
        '
        'PanelBotones
        '
        Me.PanelBotones.Controls.Add(Me.CBtExportar)
        Me.PanelBotones.Controls.Add(Me.Label1)
        Me.PanelBotones.Controls.Add(Me.UDTFechaVencimiento)
        Me.PanelBotones.Controls.Add(Me.UDTFechaVencimientoDesde)
        Me.PanelBotones.Controls.Add(Me.LabelNumero)
        Me.PanelBotones.Controls.Add(Me.LabelFecha)
        Me.PanelBotones.Controls.Add(Me.UTEmoviCheque)
        Me.PanelBotones.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelBotones.Location = New System.Drawing.Point(0, 0)
        Me.PanelBotones.Name = "PanelBotones"
        Me.PanelBotones.Size = New System.Drawing.Size(1024, 32)
        Me.PanelBotones.TabIndex = 104
        '
        'UDTFechaVencimientoDesde
        '
        Me.UDTFechaVencimientoDesde.DateTime = New Date(2012, 11, 5, 0, 0, 0, 0)
        Me.UDTFechaVencimientoDesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTFechaVencimientoDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTFechaVencimientoDesde.Location = New System.Drawing.Point(480, 5)
        Me.UDTFechaVencimientoDesde.Name = "UDTFechaVencimientoDesde"
        Me.UDTFechaVencimientoDesde.Size = New System.Drawing.Size(110, 24)
        Me.UDTFechaVencimientoDesde.TabIndex = 102
        Me.UDTFechaVencimientoDesde.Value = New Date(2012, 11, 5, 0, 0, 0, 0)
        '
        'PanelContenido
        '
        Me.PanelContenido.Controls.Add(Me.UGMoviCheque)
        Me.PanelContenido.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelContenido.Location = New System.Drawing.Point(0, 32)
        Me.PanelContenido.Name = "PanelContenido"
        Me.PanelContenido.Size = New System.Drawing.Size(1024, 470)
        Me.PanelContenido.TabIndex = 105
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(838, 2)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(183, 28)
        Me.CBtExportar.TabIndex = 125
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'FrmMoviCheques
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1024, 502)
        Me.Controls.Add(Me.PanelContenido)
        Me.Controls.Add(Me.PanelBotones)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMoviCheques"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Movimientos de cheques"
        CType(Me.UTEmoviCheque, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGMoviCheque, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTFechaVencimiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelBotones.ResumeLayout(False)
        Me.PanelBotones.PerformLayout()
        CType(Me.UDTFechaVencimientoDesde, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelContenido.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UTEmoviCheque As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UGMoviCheque As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents LabelNumero As Label
    Friend WithEvents LabelFecha As Label
    Friend WithEvents UDTFechaVencimiento As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents PanelBotones As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents UDTFechaVencimientoDesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents PanelContenido As Panel
    Friend WithEvents CBtExportar As Button
End Class
