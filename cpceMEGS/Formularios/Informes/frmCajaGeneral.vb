﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports System.Data.OleDb
Imports MySql.Data.Types

Public Class frmCajaGeneral
    Private cnn As New ConsultaBD(True)
    Private cmdCaja As MySqlCommandBuilder
    Private mysqlFecha As MySqlDateTime
    Private DTAfiCajaGral As New DataTable
    Private DTCajaGral As New DataTable
    Private DACajaGral As New MySqlDataAdapter
    Private DAFarcat As MySqlDataAdapter
    Private DTFarcat As DataTable
    Private DTPlancue As New DataTable
    Private DAPlancue As New MySqlDataAdapter
    Private DASaldo As New MySqlDataAdapter
    Private DTSaldos As New DataTable
    Private DTSaldo As New DataTable
    Private DTReporte As New DataTable
    Private reporte As New DSCajaGeneral
    Private rowDatosSaldo As DSCajaGeneral.DTSaldosRow
    Private rowDatosReporte As DSCajaGeneral.DTReporteRow

    Private Sub frmCajaGeneral_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        'Cargo el farcat en la grilla
        Try
            DAFarcat = cnn.consultaBDadapter("farcat", , )
            'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
            DTFarcat = New DataTable
            DAFarcat.Fill(DTFarcat)

            If DTFarcat.Rows.Count > 0 Then
                For Each rowFarcat As DataRow In DTFarcat.Rows
                    UltraTree1.Nodes.Add("" & rowFarcat.Item("far_nombre").ToString & " " & rowFarcat.Item("far_sucursal").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
                Next
            End If

            'For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
            '    Nodo.CheckedState = CheckState.Checked
            'Next            

            UltraTree1.Focus()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar las Sucursales", "SUCURSALES", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End Try
        
    End Sub

    Private Sub CBtProcesar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtProcesar.Click
        UltraProgressBar1.Maximum = 100
        UltraProgressBar1.Value = 0
        UltraProgressBar1.Refresh()

        ProcesarCaja()
    End Sub

    Private Sub ProcesarCaja()
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty
        Dim Fecha As String          'Cargo la fecha del inicio del balance
        '------------------------------------------------------
        'Saco la fecha del ultimo balance para calcular de ahi
        'los saldos anteriores
        '------------------------------------------------------
        If Convert.ToDateTime(Me.UDTdesde.Text).Month >= 7 Then
            Fecha = Convert.ToDateTime(Me.UDTdesde.Text).Year & "-01-07"
        Else
            Fecha = (Convert.ToDateTime(Me.UDTdesde.Text).Year - 1) & "-01-07"
        End If

        reporte = New DSCajaGeneral

        Try
            'Cargo las unidades de negocio el y nro de cliente
            For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
                If Nodo.CheckedState = CheckState.Checked Then
                    If cInstituciones.Length = 0 Then
                        If Not cUnegos.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))) Then
                            cUnegos = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                        End If
                        If Not cInstituciones.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))) Then
                            cInstituciones = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                        End If
                    Else
                        If Not cUnegos.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))) Then
                            cUnegos += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                        End If
                        If Not cInstituciones.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))) Then
                            cInstituciones += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                        End If
                    End If
                End If
            Next

            'No se marco ninguna entidad
            If cUnegos <> String.Empty Then

                DACajaGral = cnn.consultaBDadapter("((totales inner join plancuen on pla_nropla=tot_nropla) inner join procesos on " & _
                                               "pro_codigo=tot_proceso) left join afiliado on afi_tipdoc=tot_tipdoc and " & _
                                               "afi_nrodoc=tot_nrodoc", "tot_nroasi,tot_fecha,tot_unegos,tot_nrocli,tot_nrocom,pro_codigo, " & _
                                               "tot_debe,tot_haber,pla_nomcorto,tot_nrocheque,tot_concepto,afi_nombre,pla_nropla", "tot_unegos in (" & cUnegos & ") " & _
                                               "and tot_nrocli in (" & cInstituciones & ") and pla_caja='Si' and tot_fecha >='" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' " & _
                                               "and tot_fecha <='" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' and tot_estado<>'9' order by Tot_fecha")

                DTCajaGral = New DataTable
                DACajaGral.Fill(DTCajaGral)

                If DTCajaGral.Rows.Count > 0 Then

                    'Armo el Reporte
                    UltraProgressBar1.Maximum = DTCajaGral.Rows.Count

                    For Each rowCajaGral As DataRow In DTCajaGral.Rows
                        Application.DoEvents()
                        ' SI EL PROCESO CONTIENE UNA P ES UN ORDEN DE PAGO, NO ENTRA EN LA CAJA
                        If rowCajaGral("pro_codigo").ToString.Substring(0, 3) <> "01P" And _
                           rowCajaGral("pro_codigo").ToString.Substring(0, 3) <> "02P" And _
                           rowCajaGral("pro_codigo").ToString.Substring(0, 3) <> "02H" Then


                            rowDatosReporte = reporte.DTReporte.NewDTReporteRow

                            'rowReporte = DTReporte.NewRow()
                            rowDatosReporte.Item("fecha") = rowCajaGral("tot_fecha")
                            rowDatosReporte.Item("entidad") = cUnegos
                            rowDatosReporte.Item("sucursal") = cInstituciones
                            rowDatosReporte.Item("zona") = cInstituciones
                            rowDatosReporte.Item("comprobante") = rowCajaGral("tot_nrocom")
                            rowDatosReporte.Item("proceso") = rowCajaGral("pro_codigo")
                            rowDatosReporte.Item("entrada") = rowCajaGral("tot_debe")
                            rowDatosReporte.Item("salida") = rowCajaGral("tot_haber")

                            If rowCajaGral("tot_nrocheque").ToString.Trim.Length > 1 Then
                                rowDatosReporte.Item("concepto") = rowCajaGral("tot_nrocheque")
                                rowDatosReporte.Item("banco") = rowCajaGral("tot_concepto")
                            Else
                                rowDatosReporte.Item("concepto") = rowCajaGral("pla_nomcorto")
                            End If

                            ' esta busqueda de afiliado es por problema con asientos duplicado con el sistema anterior
                            If rowCajaGral("afi_nombre").ToString = "SIN DEFINIR" Then
                                DACajaGral = cnn.consultaBDadapter("comproba inner join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc", , "com_nroasi=" & rowCajaGral.Item("tot_nroasi") & " and com_nrocli=" & rowCajaGral.Item("tot_nrocli"))
                                DTAfiCajaGral = New DataTable
                                DACajaGral.Fill(DTAfiCajaGral)
                                If DTAfiCajaGral.Rows.Count > 0 Then
                                    rowDatosReporte.Item("destino") = DTAfiCajaGral.Rows(DTAfiCajaGral.Rows.Count - 1).Item("afi_nombre")
                                End If
                            Else
                                rowDatosReporte.Item("destino") = rowCajaGral("afi_nombre")
                            End If
                            rowDatosReporte.Item("desde") = Me.UDTdesde.Value ' Format(Me.UDTdesde.Value, "dd/MM/yyyy")
                            rowDatosReporte.Item("hasta") = Me.UDThasta.Value ' Format(Me.UDThasta.Value, "dd/MM/yyyy")
                            rowDatosReporte.Item("in") = rowCajaGral("tot_unegos")
                            rowDatosReporte.Item("zona") = rowCajaGral("tot_nrocli")
                            rowDatosReporte.Item("nropla") = rowCajaGral("pla_nropla")

                            reporte.DTReporte.AddDTReporteRow(rowDatosReporte)
                        End If

                        UltraProgressBar1.Value += 1
                        UltraProgressBar1.Refresh()

                    Next

                    'Saco las cuentas que intervienen

                    DAPlancue = cnn.consultaBDadapter("((totales inner join plancuen " & _
                                                      "on pla_nropla=tot_nropla) inner join procesos on pro_codigo=tot_proceso)", "distinct(pla_nropla), " & _
                                                      "pla_nomcorto", "tot_unegos in (" & cUnegos & ")and tot_nrocli in (" & cInstituciones & ") " & _
                                                      "and pla_caja='Si' and tot_fecha >='" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & " 00:00:00' " & _
                                                      "and tot_fecha <='" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & " 23:59:59' and tot_estado<>'9'")
                    DTPlancue = New DataTable
                    DAPlancue.Fill(DTPlancue)

                    If DTPlancue.Rows.Count > 0 Then
                        'Armo el datatable con el nro de cta, el nombre y Saco los saldos anteriores de las ctas
                        'para el subreporte                     

                        For Each row As DataRow In DTPlancue.Rows
                            DTSaldo = New DataTable
                            rowDatosSaldo = reporte.DTSaldos.NewDTSaldosRow
                            rowDatosSaldo.Item("NroPla") = row("pla_nropla")
                            rowDatosSaldo.Item("Fecha") = Fecha
                            rowDatosSaldo.Item("Descripcion") = row("pla_nomcorto")

                            Dim DAFechaApertura As MySqlDataAdapter
                            Dim DTFechaApertura As New DataTable

                            DAFechaApertura = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "max(TOT_FECHA) as Fecha", "tot_proceso ='APERT' and pla_caja='Si'")

                            DAFechaApertura.Fill(DTFechaApertura)

                            If DTFechaApertura.Rows.Count > 0 Then
                                Fecha = DTFechaApertura.Rows(0).Item("fecha").ToString
                            End If

                            'Saco los saldos anteriores de las ctas

                            Dim Fecha1 As String = Convert.ToDateTime(Me.UDTdesde.Text).AddDays(-1)

                            DASaldo = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "sum(tot_debe)-sum(tot_haber) " & _
                                    "as Saldo", "tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") " & _
                                    "and tot_nropla = " & row("pla_nropla") & " and pla_caja='Si' " & _
                                    "and tot_fecha >='" & Format(Convert.ToDateTime(Fecha), "yyyy-MM-dd") & " 00:00:00' and tot_estado <> '9' " & _
                                    "and tot_fecha <'" & Format(Convert.ToDateTime(Fecha1), "yyyy-MM-dd") & " 23:59:59' and mid(tot_proceso,1,3) not in ('01P','02P','02H')")
                            DTSaldo = New DataTable
                            DASaldo.Fill(DTSaldo)

                            If DTSaldo.Rows.Count > 0 Then
                                If Not IsDBNull(DTSaldo.Rows(0).Item("Saldo")) Then
                                    rowDatosSaldo.Item("SaldoAnt") = DTSaldo.Rows(0).Item("Saldo")
                                Else
                                    rowDatosSaldo.Item("SaldoAnt") = 0
                                End If
                            Else
                                rowDatosSaldo.Item("SaldoAnt") = 0
                            End If

                            reporte.DTSaldos.AddDTSaldosRow(rowDatosSaldo)

                        Next
                    End If

                    'Calculo los Salos de las Ctas entre las Fechas
                    Dim Entrada, Salida As Double

                    For Each rowSaldos As DataRow In reporte.DTSaldos.Rows  'DTSaldos.Rows
                        For Each row As DataRow In reporte.DTReporte.Rows  'DTReporte.Rows
                            If row("NroPla").ToString = rowSaldos("NroPla").ToString Then
                                Entrada += CDbl(row("entrada"))
                                Salida += CDbl(row("salida"))
                            End If
                        Next

                        rowSaldos("Entradas") = Entrada
                        rowSaldos("Salidas") = Salida
                        rowSaldos("Saldo") = Entrada - Salida
                        Entrada = 0
                        Salida = 0

                    Next

                    Dim frmResumen As New FrmReportes(DTReporte, reporte, "CRCajaGeneral.rpt", "Caja General")
                    frmResumen.ShowDialog()
                Else
                    MessageBox.Show("No hay datos para mostrar", "PROCESAR CAJA", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                End If
            Else
                MessageBox.Show("Debe seleccionar una Institucion", "PROCESAR CAJA", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Me.UltraTree1.Focus()
                Exit Sub                
            End If

        Catch ex As Exception
            MessageBox.Show("SE PRODUJO UN ERROR " & ex.Message.ToString, "PROCESAR CAJA", MessageBoxButtons.OK, MessageBoxIcon.Stop)            
            Exit Sub
        End Try

    End Sub

    Private Sub UDTdesde_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UDThasta.KeyPress, UDTdesde.KeyPress, UltraTree1.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

   

    Private Sub ArmarDataTables()
        'Reporte
        DTReporte = New DataTable

        DTReporte.Columns.Add("fecha", GetType(Date))
        DTReporte.Columns.Add("entidad", GetType(String))
        DTReporte.Columns.Add("sucursal", GetType(String))
        DTReporte.Columns.Add("comprobante", GetType(String))
        DTReporte.Columns.Add("proceso", GetType(String))
        DTReporte.Columns.Add("entrada", GetType(Double))
        DTReporte.Columns.Add("salida", GetType(Double))
        DTReporte.Columns.Add("concepto", GetType(String))
        DTReporte.Columns.Add("banco", GetType(String))
        DTReporte.Columns.Add("destino", GetType(String))
        DTReporte.Columns.Add("desde", GetType(String))
        DTReporte.Columns.Add("hasta", GetType(String))
        DTReporte.Columns.Add("in", GetType(String))
        DTReporte.Columns.Add("zona", GetType(String))
        DTReporte.Columns.Add("nropla", GetType(String))


        'SubReporte y Saldos

        DTSaldos = New DataTable
        DTSaldos.Columns.Add("NroPla", GetType(String))
        DTSaldos.Columns.Add("Descripcion", GetType(String))
        DTSaldos.Columns.Add("SaldoAnt", GetType(Double))
        DTSaldos.Columns.Add("Saldo", GetType(Double))

    End Sub

    'Private Sub ProcesarCaja()

    '    Dim cInstituciones As String = ""
    '    Dim cUnegos As String = String.Empty
    '    Dim Fecha As String          'Cargo la fecha del inicio del balance
    '    '------------------------------------------------------
    '    'Saco la fecha del ultimo balance para calcular de ahi
    '    'los saldos anteriores
    '    '------------------------------------------------------
    '    If Convert.ToDateTime(Me.UDTdesde.Text).Month >= 7 Then
    '        Fecha = Convert.ToDateTime(Me.UDTdesde.Text).Year & "-07-01"
    '    Else
    '        Fecha = (Convert.ToDateTime(Me.UDTdesde.Text).Year - 1) & "-07-01"
    '    End If

    '    Try

    '        'Cargo las unidades de negocio el y nro de cliente
    '        For Each Nodo As UltraWinTree.UltraTreeNode In Me.UltraTree1.Nodes
    '            If Nodo.CheckedState = CheckState.Checked Then
    '                If cInstituciones.Length = 0 Then
    '                    cUnegos = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
    '                    cInstituciones = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
    '                Else
    '                    cUnegos += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
    '                    cInstituciones += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
    '                End If
    '            End If
    '        Next

    '        'No se marco ninguna entidad
    '        If cUnegos <> String.Empty Then

    '            DACajaGral = cnn.consultaBDadapter("(((totales inner join comproba on com_nroasi=tot_nroasi) " & _
    '                                           "inner join plancuen on pla_nropla=tot_nropla) inner join procesos on " & _
    '                                           "pro_codigo=tot_proceso) left join afiliado on afi_tipdoc=com_tipdoc and " & _
    '                                           "afi_nrodoc=com_nrodoc", "tot_fecha,tot_unegos,tot_nrocli,tot_nrocom,pro_codigo, " & _
    '                                           "tot_debe,tot_haber,pla_nomcorto,tot_nrocheque,tot_concepto,afi_nombre,pla_nropla", "tot_unegos in (" & cUnegos & ") " & _
    '                                           "and tot_nrocli in (" & cInstituciones & ") and pla_caja='Si' and tot_fecha >='" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & "' " & _
    '                                           "and tot_fecha <='" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & "'")
    '            ' 

    '            DACajaGral.Fill(DTCajaGral)

    '            If DTCajaGral.Rows.Count > 0 Then

    '                'Armo el Reporte
    '                UltraProgressBar1.Maximum = DTCajaGral.Rows.Count

    '                For Each rowCajaGral As DataRow In DTCajaGral.Rows

    '                    rowDatosReporte = reporte.DTReporte.NewDTReporteRow

    '                    'rowReporte = DTReporte.NewRow()
    '                    rowDatosReporte.Item("fecha") = rowCajaGral("tot_fecha")
    '                    rowDatosReporte.Item("entidad") = cUnegos
    '                    rowDatosReporte.Item("zona") = cInstituciones
    '                    rowDatosReporte.Item("comprobante") = rowCajaGral("tot_nrocom")
    '                    rowDatosReporte.Item("proceso") = rowCajaGral("pro_codigo")
    '                    rowDatosReporte.Item("entrada") = rowCajaGral("tot_debe")
    '                    rowDatosReporte.Item("salida") = rowCajaGral("tot_haber")

    '                    If rowCajaGral("pla_nropla") = "11010102" Then
    '                        rowDatosReporte.Item("concepto") = "CHEQUE"
    '                    Else
    '                        rowDatosReporte.Item("concepto") = rowCajaGral("pla_nomcorto")
    '                    End If

    '                    rowDatosReporte.Item("banco") = rowCajaGral("tot_concepto")
    '                    rowDatosReporte.Item("destino") = rowCajaGral("afi_nombre")
    '                    rowDatosReporte.Item("desde") = Me.UDTdesde.Text
    '                    rowDatosReporte.Item("hasta") = Me.UDThasta.Text
    '                    rowDatosReporte.Item("in") = rowCajaGral("tot_unegos")
    '                    rowDatosReporte.Item("zona") = rowCajaGral("tot_nrocli")
    '                    rowDatosReporte.Item("nropla") = rowCajaGral("pla_nropla")

    '                    reporte.DTReporte.AddDTReporteRow(rowDatosReporte)

    '                    UltraProgressBar1.Value += 1
    '                    UltraProgressBar1.Refresh()

    '                Next

    '                'Saco las cuentas que intervienen

    '                DAPlancue = cnn.consultaBDadapter("(((totales inner join comproba on com_nroasi=tot_nroasi) inner join plancuen " & _
    '                                                  "on pla_nropla=tot_nropla) inner join procesos on pro_codigo=tot_proceso)", "distinct(pla_nropla), " & _
    '                                                  "pla_nomcorto", "tot_unegos in (" & cUnegos & ")and tot_nrocli in (" & cInstituciones & ") " & _
    '                                                  "and pla_caja='Si' and tot_fecha >='" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & "' " & _
    '                                                  "and tot_fecha <='" & Format(Convert.ToDateTime(Me.UDThasta.Text), "yyyy-MM-dd") & "'")
    '                DAPlancue.Fill(DTPlancue)

    '                If DTPlancue.Rows.Count > 0 Then
    '                    'Armo el datatable con el nro de cta, el nombre y Saco los saldos anteriores de las ctas
    '                    'para el subreporte                         

    '                    For Each row As DataRow In DTPlancue.Rows
    '                        DTSaldo = New DataTable
    '                        rowDatosSaldo = reporte.DTSaldos.NewDTSaldosRow
    '                        rowDatosSaldo.Item("NroPla") = row("pla_nropla")
    '                        If row("pla_nropla") = "11010102" Then
    '                            rowDatosSaldo.Item("Descripcion") = "CHEQUES"
    '                        Else
    '                            rowDatosSaldo.Item("Descripcion") = row("pla_nomcorto")
    '                        End If

    '                        'Saco los saldos anteriores de las ctas

    '                        DASaldo = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "sum(tot_debe)-sum(tot_haber) " & _
    '                                "as Saldo", "tot_unegos in (" & cUnegos & ")and tot_nrocli in (" & cInstituciones & ") " & _
    '                                "and tot_nropla = " & row("pla_nropla") & " and pla_caja='Si' " & _
    '                                "and tot_fecha <'" & Format(Convert.ToDateTime(Me.UDTdesde.Text), "yyyy-MM-dd") & "'")
    '                        'and tot_fecha >='" & Format(Convert.ToDateTime(Fecha), "yyyy-MM-dd") & "' " & _

    '                        DASaldo.Fill(DTSaldo)

    '                        If DTSaldo.Rows.Count > 0 Then
    '                            If Not IsDBNull(DTSaldo.Rows(0).Item("Saldo")) Then
    '                                rowDatosSaldo.Item("SaldoAnt") = DTSaldo.Rows(0).Item("Saldo")
    '                            Else
    '                                rowDatosSaldo.Item("SaldoAnt") = 0
    '                            End If
    '                        Else
    '                            rowDatosSaldo.Item("SaldoAnt") = 0
    '                        End If

    '                        reporte.DTSaldos.AddDTSaldosRow(rowDatosSaldo)

    '                    Next
    '                End If

    '                'Calculo los Salos de las Ctas entre las Fechas
    '                Dim Entrada, Salida As Double

    '                For Each rowSaldos As DataRow In reporte.DTSaldos.Rows  'DTSaldos.Rows
    '                    For Each row As DataRow In reporte.DTReporte.Rows  'DTReporte.Rows
    '                        If row("NroPla").ToString = rowSaldos("NroPla").ToString Then
    '                            Entrada += CDbl(row("entrada"))
    '                            Salida += CDbl(row("salida"))
    '                        End If
    '                    Next

    '                    rowSaldos("Entradas") = Entrada
    '                    rowSaldos("Salidas") = Salida
    '                    rowSaldos("Saldo") = Entrada - Salida
    '                    Entrada = 0
    '                    Salida = 0

    '                Next

    '                Dim frmResumen As New FrmReportes(DTReporte, reporte, "CRCaja1.rpt", "Caja General")
    '                frmResumen.ShowDialog()

    '            End If
    '        Else
    '            MessageBox.Show("Debe seleccionar una Institucion", "PROCESAR CAJA", MessageBoxButtons.OK, MessageBoxIcon.Hand)
    '            Me.UltraTree1.Focus()
    '            Exit Sub
    '        End If

    '    Catch ex As Exception
    '        MessageBox.Show("SE PRODUJO UN ERROR " & ex.Message.ToString, "PROCESAR CAJA", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    '        Exit Sub
    '    End Try

    'End Sub


End Class

