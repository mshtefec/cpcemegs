﻿Imports MySql.Data.MySqlClient
Public Class FrmLibroDiario
    Private cnn As New ConsultaBD(True)
    Private DADiario As MySqlDataAdapter
    Private DTDiario As DataTable

    Private Sub GeneroDiario()
        DADiario = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "DATE_FORMAT(tot_fecha,'%Y-%m') as fecha,tot_nropla as cuenta,pla_nombre as nombre,sum(tot_debe) as debe,sum(tot_haber) as haber", "tot_estado <> '9' AND tot_fecha between '" & Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' and '" & Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:59' GROUP BY DATE_FORMAT(tot_fecha,'%Y-%m'),tot_nropla ORDER BY DATE_FORMAT(tot_fecha,'%Y-%m'),tot_nropla")
        DTDiario = New DataTable
        DADiario.Fill(DTDiario)

        Dim col As New DataColumn
        col.ColumnName = "hoja"
        col.Caption = "hoja"
        col.DataType = GetType(Integer)
        col.DefaultValue = 0
        If UNEhoja.Value > 0 Then
            col.DefaultValue = (UNEhoja.Value - 1)
        End If
        DTDiario.Columns.Add(col)

        col = New DataColumn
        col.Caption = "Transporte"
        col.DataType = GetType(Double)
        col.DefaultValue = 0
        If UCEtransporte.Value > 0 Then
            col.DefaultValue = UCEtransporte.Value
        End If
        DTDiario.Columns.Add(col)

        For Each row As DataRow In DTDiario.Rows
            UCEDebe.Value += row.Item("debe")
            UCEHaber.Value += row.Item("haber")
        Next
        UGDiario.DataSource = DTDiario
        With UGDiario.DisplayLayout.Bands(0)
            .Columns(2).Width = 300
            .Columns(3).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(5).Hidden = True
        End With
        Dim frmDiario As New FrmReportes(DTDiario, , "CRLibroDiario.rpt", "Libro Diario General")
        frmDiario.ShowDialog()
    End Sub

    Private Sub CBtProcesar_Click(sender As Object, e As EventArgs) Handles CBtProcesar.Click
        GeneroDiario()
    End Sub
End Class