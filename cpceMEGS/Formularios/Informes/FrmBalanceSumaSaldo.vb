﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win

Public Class FrmBalanceSumaSaldo
    Private cnn As New ConsultaBD(True)
    Private DAFarcat As MySqlDataAdapter
    Private DTFarcat As DataTable
    Private Sub CBtProcesar_Click(sender As Object, e As EventArgs) Handles CBtProcesar.Click
        If RadioButton1.Checked Then
            Dim frmDetResul As New FrmAsientoCierre("D", GeneroCierre(), DTFarcat)
            frmDetResul.ShowDialog()
        ElseIf RadioButton2.Checked Then
            Dim frmDetResul As New FrmAsientoCierre("C", GeneroCierre(), DTFarcat)
            frmDetResul.ShowDialog()
        ElseIf RadioButton3.Checked Then
            Dim frmDetResul As New FrmAsientoCierre("A", Nothing, DTFarcat)
            frmDetResul.ShowDialog()
        Else
            GeneroBalance()
        End If
    End Sub
    Private Sub GeneroBalance()
        Dim DABal As MySqlDataAdapter
        Dim DTBalance As New DataTable
        Dim DTPlancue As New DataTable
        Dim DTMadre As New DataTable
        Dim DTTotalCta As New DataTable
        Dim DTSaldoAnt As New DataTable
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty
        Dim cConsulta As String = String.Empty

        'Cargo las unidades de negocio el y nro de cliente
        For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes
            If Nodo.CheckedState = CheckState.Checked Then
                If cInstituciones.Length = 0 Then
                    cUnegos = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                    cInstituciones = Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                Else
                    If Not cUnegos.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))) Then
                        cUnegos += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_unegos"))
                    End If
                    If Not cInstituciones.Contains(Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))) Then
                        cInstituciones += "," & Convert.ToString(DTFarcat.Rows(Nodo.Index).Item("far_nrocli"))
                    End If
                End If
            End If
        Next

        Try
            DABal = cnn.consultaBDadapter("plancuen")
            DABal.Fill(DTMadre)

            DABal = cnn.consultaBDadapter(
                "plancuen",
                "pla_nropla,pla_nombre,pla_madre,pla_column,pla_debe,pla_haber",
                "pla_nropla between '" & txtDesdeCta.Text & "' and '" & TxHastaCta.Text & "' order by pla_nropla"
            )
            DABal.Fill(DTPlancue)
            DTPlancue.Columns.Add("saldoAnterior", GetType(Double))
            DTPlancue.Columns.Add("saldo", GetType(Double))
            DTPlancue.Columns.Add("institucion", GetType(Integer))
            DTPlancue.Columns.Add("delegacion", GetType(Integer))
            DTPlancue.Columns.Add("entidad", GetType(String))
            DTPlancue.Columns.Add("descripcion", GetType(String))
            DTPlancue.Columns.Add("desde", GetType(String))
            DTPlancue.Columns.Add("hasta", GetType(String))
            DTPlancue.Columns.Add("columna1", GetType(Double))
            DTPlancue.Columns.Add("columna2", GetType(Double))
            DTPlancue.Columns.Add("columna3", GetType(Double))
            DTPlancue.Columns.Add("columna4", GetType(Double))

            DTBalance = DTPlancue.Clone
            'Resto 1 para que despues llegue bien al final la barra sin error
            'UltraProgressBar1.Maximum = DTPlancue.Rows.Count - 1
            UltraProgressBar1.Maximum = DTPlancue.Rows.Count
            UltraProgressBar1.Value = 0
            UltraProgressBar1.Refresh()

            Dim DTPlanTmp As DataTable = DTPlancue
            Dim FilaMadre() As DataRow
            Dim CuentaMadre As String
            Dim MadreDebe As Double
            Dim MadreHaber As Double
            Dim MadreSaldo As Double
            Dim MadreSaldoAnterior As Double
            Dim nColumna As Integer
            Dim nDelegacion As Integer = 0
            Dim nInstitucion As Integer = 0
            For Each RowTot As DataRow In DTPlancue.Rows
                Application.DoEvents()
                ' DABal = cnn.consultaBDadapter("totales INNER JOIN comproba ON com_nroasi=tot_nroasi", "tot_nropla,sum(tot_debe) as debe,sum(tot_haber) as haber,sum(tot_debe)-sum(tot_haber) as Saldo", "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha between '" & Format(Convert.ToDateTime(Me.UDTdesde.Value), "yyyy-MM-dd") & "' and '" & Format(Convert.ToDateTime(Me.UDThasta.Value), "yyyy-MM-dd") & "' and tot_unegos=" & UltraComboEditor1.Value)
                DABal = cnn.consultaBDadapter(
                    "totales",
                    "tot_nropla,sum(tot_debe) as debe,sum(tot_haber) as haber,sum(tot_debe-tot_haber) as Saldo",
                    "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha between '" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & " 00:00:00' and '" & Format(Convert.ToDateTime(UDThasta.Value), "yyyy-MM-dd") & " 23:59:59' and tot_estado<>'9' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") group by tot_nropla"
                )

                DABal.SelectCommand.CommandTimeout = 300
                DTTotalCta = New DataTable
                DABal.Fill(DTTotalCta)
                ' DsTotalCta.Tables(0).Columns.Add("Select", GetType(Boolean))
                If DTTotalCta.Rows.Count > 0 Then
                    Dim oDataRow As DataRow = DTTotalCta.Rows(0)
                    RowTot("delegacion") = nDelegacion
                    RowTot("entidad") = cUnegos
                    RowTot("descripcion") = cInstituciones
                    RowTot("desde") = UDTdesde.Value
                    RowTot("hasta") = UDThasta.Value
                    If IsDBNull(DTTotalCta.Rows(0).Item("debe")) Then
                        RowTot("pla_debe") = 0
                    Else
                        RowTot("pla_debe") = DTTotalCta.Rows(0).Item("debe")
                    End If
                    If IsDBNull(DTTotalCta.Rows(0).Item("haber")) Then
                        RowTot("pla_haber") = 0
                    Else
                        RowTot("pla_haber") = DTTotalCta.Rows(0).Item("haber")
                    End If
                    If IsDBNull(DTTotalCta.Rows(0).Item("Saldo")) Then
                        RowTot("saldo") = 0
                    Else
                        RowTot("saldo") = DTTotalCta.Rows(0).Item("Saldo")
                    End If
                    If UltraCheckEditor1.Checked Then
                        DABal = cnn.consultaBDadapter(
                            "totales",
                            "tot_nropla,sum(tot_debe-tot_haber) as Saldo",
                            "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha < '" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & "' and tot_estado<>'9' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") group by tot_nropla"
                        )
                        DTSaldoAnt = New DataTable
                        DABal.Fill(DTSaldoAnt)

                        If DTSaldoAnt.Rows.Count > 0 Then
                            If IsDBNull(DTSaldoAnt.Rows(0).Item("Saldo")) Then
                                RowTot("SaldoAnterior") = 0
                            Else
                                RowTot("SaldoAnterior") = DTSaldoAnt.Rows(0).Item("Saldo")
                            End If
                        Else
                            RowTot("saldoAnterior") = 0
                        End If
                    Else
                        RowTot("saldoAnterior") = 0
                    End If

                    nColumna = 1
                    RowTot("pla_column") = nColumna
                    CuentaMadre = RowTot.Item("pla_madre")
                    MadreDebe = RowTot("pla_debe")
                    MadreHaber = RowTot("pla_haber")
                    MadreSaldo = RowTot("saldo")
                    MadreSaldoAnterior = RowTot("saldoAnterior")

                    DTBalance.ImportRow(RowTot)
                    'If CuentaMadre = "11030000" Then
                    '    Dim holastop As String = ""
                    'End If
                    If MadreDebe > 0 Or MadreHaber > 0 Then
                        Do While True
                            FilaMadre = DTPlanTmp.Select("pla_nropla='" & CuentaMadre & "'")
                            If FilaMadre.Length = 0 Then
                                Exit Do
                            Else
                                nColumna += 1
                                FilaMadre(0).Item("entidad") = cUnegos
                                FilaMadre(0).Item("descripcion") = cInstituciones
                                FilaMadre(0).Item("desde") = UDTdesde.Value
                                FilaMadre(0).Item("hasta") = UDThasta.Value
                                FilaMadre(0).Item("pla_debe") += MadreDebe
                                FilaMadre(0).Item("pla_haber") += MadreHaber
                                If IsDBNull(FilaMadre(0).Item("saldo")) Then
                                    FilaMadre(0).Item("saldo") = MadreSaldo
                                Else
                                    FilaMadre(0).Item("saldo") += MadreSaldo
                                End If
                                If IsDBNull(FilaMadre(0).Item("saldoAnterior")) Then
                                    FilaMadre(0).Item("saldoAnterior") = MadreSaldoAnterior
                                Else
                                    FilaMadre(0).Item("saldoAnterior") += MadreSaldoAnterior
                                End If
                                FilaMadre(0).Item("pla_column") = nColumna
                                CuentaMadre = FilaMadre(0).Item("pla_madre")
                            End If
                        Loop
                    End If
                End If
                DTTotalCta.Clear()
                UltraProgressBar1.Value += 1
                UltraProgressBar1.Refresh()
            Next

            DTTotalCta.Clear()
            DABal.Dispose()
            Dim nSaldoBalance1 As Double = 0
            Dim nSaldoBalance2 As Double = 0
            Dim nSaldoBalance3 As Double = 0
            Dim nSaldoBalance4 As Double = 0

            For X As Integer = DTPlancue.Rows.Count - 1 To 0 Step -1
                'If IsDBNull(DTPlancue.Rows(X).Item("saldo")) AndAlso DTPlancue.Rows(X).Item("saldo") = 0 Then
                If DTPlancue.Rows(X).Item("pla_debe") = 0 And DTPlancue.Rows(X).Item("pla_haber") = 0 Then
                    DTPlancue.Rows.RemoveAt(X)
                Else
                    Select Case DTPlancue.Rows(X).Item("pla_column")
                        Case 4
                            DTPlancue.Rows(X).Item("columna1") = DTPlancue.Rows(X).Item("saldo")
                            nSaldoBalance1 += DTPlancue.Rows(X).Item("saldo")
                            'Case 4
                        Case 3
                            DTPlancue.Rows(X).Item("columna2") = DTPlancue.Rows(X).Item("saldo")
                            nSaldoBalance2 += DTPlancue.Rows(X).Item("saldo")
                        Case 2
                            DTPlancue.Rows(X).Item("columna3") = DTPlancue.Rows(X).Item("saldo")
                            nSaldoBalance3 += DTPlancue.Rows(X).Item("saldo")
                        Case 1
                            DTPlancue.Rows(X).Item("columna4") = DTPlancue.Rows(X).Item("saldo")
                            nSaldoBalance4 += DTPlancue.Rows(X).Item("saldo")
                    End Select
                End If
            Next
            Dim rowSaldoBalance As DataRow = DTPlancue.NewRow
            '  rowSaldoBalance.Item("columna1") = nSaldoBalance1
            '  rowSaldoBalance.Item("columna2") = nSaldoBalance2
            '  rowSaldoBalance.Item("columna3") = nSaldoBalance3
            rowSaldoBalance.Item("columna4") = nSaldoBalance4
            DTPlancue.Rows.Add(rowSaldoBalance)
            UGBalance.DataSource = DTPlancue
            With UGBalance.DisplayLayout.Bands(0)
                .Columns(2).Hidden = True
                .Columns(3).Hidden = True
                .Columns(4).Hidden = True
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
                .Columns(9).Hidden = True
                .Columns(10).Hidden = True
                .Columns(11).Hidden = True
                .Columns(12).Hidden = True
                .Columns(13).Hidden = True
                '.Columns(DTPlancue.Columns.Count - 4).Format = "N2"
                '.Columns(DTPlancue.Columns.Count - 3).Format = "N2"
                '.Columns(DTPlancue.Columns.Count - 2).Format = "N2"
                '.Columns(DTPlancue.Columns.Count - 1).Format = "N2"
            End With
            Dim cListado(0) As String
            cListado(0) = "Balance de Suma y Saldo - Periodo desde: " & UDTdesde.Value.ToString.Substring(0, 10) & " al " & UDThasta.Value.ToString.Substring(0, 10) & " - Desde cuenta:" & txtDesdeCta.Text & " Hasta Cuenta:" & TxHastaCta.Text
            Dim clsExportarExcel As New ExportarExcel
            clsExportarExcel.ExportarDatosExcel(UGBalance, cListado)
            'Dim frmResumen As New FrmReportes(DTPlancue, , "CRBalance.rpt", "Balance de Suma y Saldo")
            'frmResumen.ShowDialog()
            'Hago el Calculo de las ctas para sumas hacia arriba
        Catch Mx As MySqlException
            MessageBox.Show("Se Produjo un Error,Por Favor Controle ", Mx.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error,Por Favor Controle " & ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Finally
            cnn.CerrarConexion()
        End Try
    End Sub

    Private Function GeneroCierre() As DataTable
        Dim DABal As MySqlDataAdapter
        Dim DTPlancue As New DataTable
        Dim DTBalance As New DataTable
        Dim DTMadre As New DataTable
        Dim DTTotalCta As New DataTable
        Dim DTSaldoAnt As New DataTable
        Dim cInstituciones As String = ""
        Dim cUnegos As String = String.Empty
        Dim cConsulta As String = String.Empty

        Try
            DABal = cnn.consultaBDadapter("plancuen")
            DABal.Fill(DTMadre)

            DABal = cnn.consultaBDadapter(
                "plancuen",
                "pla_nropla,pla_nombre,pla_madre,pla_column,pla_debe,pla_haber",
                "1 order by pla_nropla"
            )
            DABal.Fill(DTPlancue)
            DTPlancue.Columns.Add("saldoAnterior", GetType(Double))
            DTPlancue.Columns.Add("saldo", GetType(Double))
            DTPlancue.Columns.Add("institucion", GetType(Integer))
            DTPlancue.Columns.Add("delegacion", GetType(Integer))
            DTPlancue.Columns.Add("entidad", GetType(String))
            DTPlancue.Columns.Add("descripcion", GetType(String))
            DTPlancue.Columns.Add("desde", GetType(String))
            DTPlancue.Columns.Add("hasta", GetType(String))

            UltraProgressBar1.Maximum = DTPlancue.Rows.Count
            UltraProgressBar1.Value = 0
            UltraProgressBar1.Refresh()

            Dim DTPlanTmp As DataTable = DTPlancue
            Dim CuentaMadre As String
            Dim MadreDebe As Double
            Dim MadreHaber As Double
            Dim MadreSaldo As Double
            Dim MadreSaldoAnterior As Double
            Dim nColumna As Integer
            Dim nDelegacion As Integer = 0
            Dim nInstitucion As Integer = 0

            DTBalance = DTPlancue.Clone

            For Each RowTot As DataRow In DTPlancue.Rows
                Application.DoEvents()
                ' DABal = cnn.consultaBDadapter("totales INNER JOIN comproba ON com_nroasi=tot_nroasi", "tot_nropla,sum(tot_debe) as debe,sum(tot_haber) as haber,sum(tot_debe)-sum(tot_haber) as Saldo", "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha between '" & Format(Convert.ToDateTime(Me.UDTdesde.Value), "yyyy-MM-dd") & "' and '" & Format(Convert.ToDateTime(Me.UDThasta.Value), "yyyy-MM-dd") & "' and tot_unegos=" & UltraComboEditor1.Value)
                For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes
                    If Nodo.CheckedState = CheckState.Checked Then
                        nInstitucion = DTFarcat.Rows(Nodo.Index).Item("far_unegos")
                        nDelegacion = DTFarcat.Rows(Nodo.Index).Item("far_nrocli")
                        ' DABal = cnn.consultaBDadapter("totales", "tot_nropla,sum(tot_debe) as debe,sum(tot_haber) as haber,sum(tot_debe-tot_haber) as Saldo", "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha between '" & Format(Convert.ToDateTime(Me.UDTdesde.Value), "yyyy-MM-dd") & " 00:00:00' and '" & Format(Convert.ToDateTime(Me.UDThasta.Value), "yyyy-MM-dd") & " 23:59:59' and tot_estado<>'9' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") group by tot_nropla")
                        DABal = cnn.consultaBDadapter(
                            "totales",
                            "tot_nropla,sum(tot_debe) as debe,sum(tot_haber) as haber,sum(tot_debe-tot_haber) as Saldo",
                            "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha between '" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & " 00:00:00' and '" & Format(Convert.ToDateTime(UDThasta.Value), "yyyy-MM-dd") & " 23:59:59' and tot_estado<>'9' and tot_unegos =" & nInstitucion & " and tot_nrocli = " & nDelegacion & " group by tot_nropla"
                        )

                        DABal.SelectCommand.CommandTimeout = 300
                        DTTotalCta = New DataTable
                        DABal.Fill(DTTotalCta)
                        ' DsTotalCta.Tables(0).Columns.Add("Select", GetType(Boolean))
                        If DTTotalCta.Rows.Count > 0 Then
                            Dim oDataRow As DataRow = DTTotalCta.Rows(0)
                            RowTot("institucion") = nInstitucion
                            RowTot("delegacion") = nDelegacion
                            RowTot("entidad") = cUnegos
                            RowTot("descripcion") = cInstituciones
                            RowTot("desde") = UDTdesde.Value
                            RowTot("hasta") = UDThasta.Value
                            If IsDBNull(DTTotalCta.Rows(0).Item("debe")) Then
                                RowTot("pla_debe") = 0
                            Else
                                RowTot("pla_debe") = DTTotalCta.Rows(0).Item("debe")
                            End If
                            If IsDBNull(DTTotalCta.Rows(0).Item("haber")) Then
                                RowTot("pla_haber") = 0
                            Else
                                RowTot("pla_haber") = DTTotalCta.Rows(0).Item("haber")
                            End If
                            If IsDBNull(DTTotalCta.Rows(0).Item("Saldo")) Then
                                RowTot("saldo") = 0
                            Else
                                RowTot("saldo") = DTTotalCta.Rows(0).Item("Saldo")
                            End If
                            If UltraCheckEditor1.Checked Then
                                DABal = cnn.consultaBDadapter(
                                    "totales",
                                    "tot_nropla,sum(tot_debe-tot_haber) as Saldo",
                                    "tot_nropla='" & RowTot.Item("pla_nropla") & "' and tot_fecha < '" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & "' and tot_estado<>'9' and tot_unegos in (" & cUnegos & ") and tot_nrocli in (" & cInstituciones & ") group by tot_nropla"
                                )
                                DTSaldoAnt = New DataTable
                                DABal.Fill(DTSaldoAnt)

                                If DTSaldoAnt.Rows.Count > 0 Then
                                    If IsDBNull(DTSaldoAnt.Rows(0).Item("Saldo")) Then
                                        RowTot("SaldoAnterior") = 0
                                    Else
                                        RowTot("SaldoAnterior") = DTSaldoAnt.Rows(0).Item("Saldo")
                                    End If
                                Else
                                    RowTot("saldoAnterior") = 0
                                End If
                            Else
                                RowTot("saldoAnterior") = 0
                            End If

                            nColumna = 1
                            RowTot("pla_column") = nColumna
                            CuentaMadre = RowTot.Item("pla_madre")
                            MadreDebe = RowTot("pla_debe")
                            MadreHaber = RowTot("pla_haber")
                            MadreSaldo = RowTot("saldo")
                            MadreSaldoAnterior = RowTot("saldoAnterior")

                            DTBalance.ImportRow(RowTot)
                        End If
                    End If
                Next

                DTTotalCta.Clear()
                UltraProgressBar1.Value += 1
                UltraProgressBar1.Refresh()
            Next

            DTTotalCta.Clear()
            DABal.Dispose()
            'Hago el Calculo de las ctas para sumas hacia arriba
        Catch Mx As MySqlException
            MessageBox.Show("Se Produjo un Error,Por Favor Controle ", Mx.Message, MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error,Por Favor Controle " & ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Finally
            cnn.CerrarConexion()
        End Try

        Return DTBalance
    End Function

    Private Sub FrmBalanceSumaSaldo_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        DAFarcat = cnn.consultaBDadapter("farcat", , )
        'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
        DTFarcat = New DataTable
        DAFarcat.Fill(DTFarcat)

        If DTFarcat.Rows.Count > 0 Then
            For Each rowFarcat As DataRow In DTFarcat.Rows
                UltraTree1.Nodes.Add("" & rowFarcat.Item("far_nombre").ToString & " " & rowFarcat.Item("far_sucursal").ToString & "").Override.NodeStyle = UltraWinTree.NodeStyle.CheckBox
            Next
        End If
    End Sub

    Private Sub MarcoTodoLosDelegaciones()
        For Each Nodo As UltraWinTree.UltraTreeNode In UltraTree1.Nodes
            Nodo.CheckedState = CheckState.Checked
        Next
        UltraTree1.Enabled = False
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RadioButton1.CheckedChanged
        MarcoTodoLosDelegaciones()
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RadioButton2.CheckedChanged
        MarcoTodoLosDelegaciones()
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles RadioButton3.CheckedChanged
        MarcoTodoLosDelegaciones()
    End Sub
End Class