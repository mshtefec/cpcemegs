﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBalanceSumaSaldo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBalanceSumaSaldo))
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ScrollBarLook1 As Infragistics.Win.UltraWinScrollBar.ScrollBarLook = New Infragistics.Win.UltraWinScrollBar.ScrollBarLook()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.CBtProcesar = New System.Windows.Forms.Button()
        Me.UltraTree1 = New Infragistics.Win.UltraWinTree.UltraTree()
        Me.UltraCheckEditor1 = New Infragistics.Win.UltraWinEditors.UltraCheckEditor()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.UGBalance = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDesdeCta = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxHastaCta = New System.Windows.Forms.TextBox()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCheckEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UGBalance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(233, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(14, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Desde"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(12, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 21)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Institución"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDThasta
        '
        Me.UDThasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDThasta.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDThasta.FormatString = ""
        Me.UDThasta.Location = New System.Drawing.Point(312, 9)
        Me.UDThasta.MaskInput = "{date}"
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(112, 21)
        Me.UDThasta.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDThasta.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDThasta.TabIndex = 46
        '
        'UDTdesde
        '
        Me.UDTdesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTdesde.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDTdesde.FormatString = ""
        Me.UDTdesde.Location = New System.Drawing.Point(93, 9)
        Me.UDTdesde.MaskInput = "{date}"
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(112, 21)
        Me.UDTdesde.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDTdesde.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDTdesde.TabIndex = 45
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UltraProgressBar1.Location = New System.Drawing.Point(12, 301)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(419, 33)
        Me.UltraProgressBar1.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Segmented
        Me.UltraProgressBar1.TabIndex = 51
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'CBtProcesar
        '
        Me.CBtProcesar.BackColor = System.Drawing.Color.Transparent
        Me.CBtProcesar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtProcesar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtProcesar.ImageIndex = 0
        Me.CBtProcesar.Location = New System.Drawing.Point(121, 457)
        Me.CBtProcesar.Name = "CBtProcesar"
        Me.CBtProcesar.Size = New System.Drawing.Size(192, 50)
        Me.CBtProcesar.TabIndex = 52
        Me.CBtProcesar.Text = "Procesar"
        Me.CBtProcesar.UseVisualStyleBackColor = False
        '
        'UltraTree1
        '
        Me.UltraTree1.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.UltraTree1.Location = New System.Drawing.Point(92, 93)
        Me.UltraTree1.Name = "UltraTree1"
        Me.UltraTree1.NodeConnectorColor = System.Drawing.SystemColors.ControlDark
        Me.UltraTree1.Size = New System.Drawing.Size(258, 202)
        Me.UltraTree1.TabIndex = 53
        '
        'UltraCheckEditor1
        '
        Me.UltraCheckEditor1.Location = New System.Drawing.Point(93, 36)
        Me.UltraCheckEditor1.Name = "UltraCheckEditor1"
        Me.UltraCheckEditor1.Size = New System.Drawing.Size(166, 20)
        Me.UltraCheckEditor1.TabIndex = 54
        Me.UltraCheckEditor1.Text = "Con Saldo Anterior"
        '
        'UltraGroupBox1
        '
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.UltraGroupBox1.Appearance = Appearance1
        Me.UltraGroupBox1.Controls.Add(Me.RadioButton3)
        Me.UltraGroupBox1.Controls.Add(Me.RadioButton2)
        Me.UltraGroupBox1.Controls.Add(Me.RadioButton1)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(12, 342)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(419, 110)
        Me.UltraGroupBox1.TabIndex = 55
        Me.UltraGroupBox1.Text = "Opciones de Cierre"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2003
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.BackColor = System.Drawing.Color.White
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.Location = New System.Drawing.Point(120, 76)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(131, 19)
        Me.RadioButton3.TabIndex = 2
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Asiento de Apertura"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.White
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(120, 51)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(118, 19)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Asiento de Cierre"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.White
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(120, 26)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(188, 19)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Determinacion de Resultados"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'UGBalance
        '
        Appearance2.BackColor = System.Drawing.Color.White
        Appearance2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(221, Byte), Integer))
        Appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UGBalance.DisplayLayout.AddNewBox.Appearance = Appearance2
        Me.UGBalance.DisplayLayout.AddNewBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Appearance3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance3.ImageBackground = CType(resources.GetObject("Appearance3.ImageBackground"), System.Drawing.Image)
        Appearance3.ImageBackgroundAlpha = Infragistics.Win.Alpha.UseAlphaLevel
        Appearance3.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance3.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UGBalance.DisplayLayout.AddNewBox.ButtonAppearance = Appearance3
        Me.UGBalance.DisplayLayout.AddNewBox.ButtonConnectorColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Me.UGBalance.DisplayLayout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance4.BackColor = System.Drawing.Color.White
        Me.UGBalance.DisplayLayout.Appearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance5.FontData.Name = "Trebuchet MS"
        Appearance5.FontData.SizeInPoints = 9.0!
        Appearance5.ForeColor = System.Drawing.Color.White
        Appearance5.TextHAlignAsString = "Center"
        Me.UGBalance.DisplayLayout.CaptionAppearance = Appearance5
        Me.UGBalance.DisplayLayout.FixedHeaderOffImage = CType(resources.GetObject("UGBalance.DisplayLayout.FixedHeaderOffImage"), System.Drawing.Image)
        Me.UGBalance.DisplayLayout.FixedHeaderOnImage = CType(resources.GetObject("UGBalance.DisplayLayout.FixedHeaderOnImage"), System.Drawing.Image)
        Me.UGBalance.DisplayLayout.FixedRowOffImage = CType(resources.GetObject("UGBalance.DisplayLayout.FixedRowOffImage"), System.Drawing.Image)
        Me.UGBalance.DisplayLayout.FixedRowOnImage = CType(resources.GetObject("UGBalance.DisplayLayout.FixedRowOnImage"), System.Drawing.Image)
        Appearance6.FontData.BoldAsString = "True"
        Appearance6.FontData.Name = "Trebuchet MS"
        Appearance6.FontData.SizeInPoints = 10.0!
        Appearance6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(177, Byte), Integer))
        Appearance6.ImageBackground = CType(resources.GetObject("Appearance6.ImageBackground"), System.Drawing.Image)
        Appearance6.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 3)
        Appearance6.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UGBalance.DisplayLayout.GroupByBox.Appearance = Appearance6
        Me.UGBalance.DisplayLayout.GroupByBox.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.None
        Me.UGBalance.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGBalance.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGBalance.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGBalance.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.None
        Me.UGBalance.DisplayLayout.Override.BorderStyleHeader = Infragistics.Win.UIElementBorderStyle.None
        Me.UGBalance.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.None
        Me.UGBalance.DisplayLayout.Override.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless
        Appearance7.BackColor = System.Drawing.Color.Transparent
        Me.UGBalance.DisplayLayout.Override.CardAreaAppearance = Appearance7
        Appearance8.BorderColor = System.Drawing.Color.Transparent
        Appearance8.FontData.Name = "Verdana"
        Me.UGBalance.DisplayLayout.Override.CellAppearance = Appearance8
        Appearance9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance9.ImageBackground = CType(resources.GetObject("Appearance9.ImageBackground"), System.Drawing.Image)
        Appearance9.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Appearance9.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UGBalance.DisplayLayout.Override.CellButtonAppearance = Appearance9
        Appearance10.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.UGBalance.DisplayLayout.Override.FilterCellAppearance = Appearance10
        Appearance11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance11.ImageBackground = CType(resources.GetObject("Appearance11.ImageBackground"), System.Drawing.Image)
        Appearance11.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(6, 3, 6, 3)
        Me.UGBalance.DisplayLayout.Override.FilterClearButtonAppearance = Appearance11
        Appearance12.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Appearance12.BackColorAlpha = Infragistics.Win.Alpha.Opaque
        Me.UGBalance.DisplayLayout.Override.FilterRowPromptAppearance = Appearance12
        Me.UGBalance.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.None
        Appearance13.FontData.BoldAsString = "True"
        Appearance13.FontData.Name = "Trebuchet MS"
        Appearance13.FontData.SizeInPoints = 10.0!
        Appearance13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance13.ImageBackground = CType(resources.GetObject("Appearance13.ImageBackground"), System.Drawing.Image)
        Appearance13.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Tiled
        Appearance13.TextHAlignAsString = "Left"
        Appearance13.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGBalance.DisplayLayout.Override.HeaderAppearance = Appearance13
        Me.UGBalance.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Me.UGBalance.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.XPThemed
        Appearance14.BorderColor = System.Drawing.Color.Transparent
        Me.UGBalance.DisplayLayout.Override.RowAppearance = Appearance14
        Appearance15.BackColor = System.Drawing.Color.White
        Me.UGBalance.DisplayLayout.Override.RowSelectorAppearance = Appearance15
        Appearance16.BorderColor = System.Drawing.Color.Transparent
        Appearance16.ForeColor = System.Drawing.Color.Black
        Me.UGBalance.DisplayLayout.Override.SelectedCellAppearance = Appearance16
        Appearance17.BorderColor = System.Drawing.Color.Transparent
        Appearance17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(27, Byte), Integer), CType(CType(85, Byte), Integer))
        Appearance17.ImageBackground = CType(resources.GetObject("Appearance17.ImageBackground"), System.Drawing.Image)
        Appearance17.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(1, 1, 1, 4)
        Appearance17.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        Me.UGBalance.DisplayLayout.Override.SelectedRowAppearance = Appearance17
        Appearance18.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 4, 2, 4)
        Appearance18.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.Appearance = Appearance18
        Appearance19.ImageBackground = CType(resources.GetObject("Appearance19.ImageBackground"), System.Drawing.Image)
        Appearance19.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(3, 2, 3, 2)
        ScrollBarLook1.AppearanceHorizontal = Appearance19
        Appearance20.ImageBackground = CType(resources.GetObject("Appearance20.ImageBackground"), System.Drawing.Image)
        Appearance20.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 3, 2, 3)
        Appearance20.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.AppearanceVertical = Appearance20
        Appearance21.ImageBackground = CType(resources.GetObject("Appearance21.ImageBackground"), System.Drawing.Image)
        Appearance21.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(0, 2, 0, 1)
        ScrollBarLook1.TrackAppearanceHorizontal = Appearance21
        Appearance22.ImageBackground = CType(resources.GetObject("Appearance22.ImageBackground"), System.Drawing.Image)
        Appearance22.ImageBackgroundStretchMargins = New Infragistics.Win.ImageBackgroundStretchMargins(2, 0, 1, 0)
        Appearance22.ImageBackgroundStyle = Infragistics.Win.ImageBackgroundStyle.Stretched
        ScrollBarLook1.TrackAppearanceVertical = Appearance22
        Me.UGBalance.DisplayLayout.ScrollBarLook = ScrollBarLook1
        Me.UGBalance.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGBalance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.UGBalance.Location = New System.Drawing.Point(0, 0)
        Me.UGBalance.Name = "UGBalance"
        Me.UGBalance.Size = New System.Drawing.Size(444, 405)
        Me.UGBalance.TabIndex = 56
        Me.UGBalance.UseOsThemes = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGBalance.Visible = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(14, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 21)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Desde Cuenta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDesdeCta
        '
        Me.txtDesdeCta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesdeCta.Location = New System.Drawing.Point(118, 59)
        Me.txtDesdeCta.MaxLength = 8
        Me.txtDesdeCta.Name = "txtDesdeCta"
        Me.txtDesdeCta.Size = New System.Drawing.Size(73, 22)
        Me.txtDesdeCta.TabIndex = 58
        Me.txtDesdeCta.Text = "10000000"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(200, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 21)
        Me.Label5.TabIndex = 59
        Me.Label5.Text = "Hasta Cuenta:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxHastaCta
        '
        Me.TxHastaCta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxHastaCta.Location = New System.Drawing.Point(305, 59)
        Me.TxHastaCta.MaxLength = 8
        Me.TxHastaCta.Name = "TxHastaCta"
        Me.TxHastaCta.Size = New System.Drawing.Size(73, 22)
        Me.TxHastaCta.TabIndex = 60
        Me.TxHastaCta.Text = "99999999"
        '
        'FrmBalanceSumaSaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(444, 519)
        Me.Controls.Add(Me.TxHastaCta)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtDesdeCta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.UltraCheckEditor1)
        Me.Controls.Add(Me.UltraTree1)
        Me.Controls.Add(Me.CBtProcesar)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.UGBalance)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmBalanceSumaSaldo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Balance de Suma y Saldo"
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCheckEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UGBalance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents CBtProcesar As System.Windows.Forms.Button
    Friend WithEvents UltraTree1 As Infragistics.Win.UltraWinTree.UltraTree
    Friend WithEvents UltraCheckEditor1 As Infragistics.Win.UltraWinEditors.UltraCheckEditor
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents UGBalance As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDesdeCta As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxHastaCta As System.Windows.Forms.TextBox
End Class
