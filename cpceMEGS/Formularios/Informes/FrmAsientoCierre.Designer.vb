﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAsientoCierre
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAsientoCierre))
        Me.UGAsiento = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UNEdebe = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.UNEhaber = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.CBtConfirmar = New System.Windows.Forms.Button()
        Me.UCEasientoApertura = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        CType(Me.UGAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEdebe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEhaber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEasientoApertura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGAsiento
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGAsiento.DisplayLayout.Appearance = Appearance1
        Me.UGAsiento.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGAsiento.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGAsiento.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGAsiento.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGAsiento.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGAsiento.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGAsiento.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGAsiento.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGAsiento.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGAsiento.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGAsiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGAsiento.Location = New System.Drawing.Point(0, 0)
        Me.UGAsiento.Name = "UGAsiento"
        Me.UGAsiento.Size = New System.Drawing.Size(684, 405)
        Me.UGAsiento.TabIndex = 2
        '
        'UNEdebe
        '
        Me.UNEdebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEdebe.Location = New System.Drawing.Point(410, 405)
        Me.UNEdebe.Name = "UNEdebe"
        Me.UNEdebe.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Double]
        Me.UNEdebe.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEdebe.ReadOnly = True
        Me.UNEdebe.Size = New System.Drawing.Size(122, 23)
        Me.UNEdebe.TabIndex = 3
        '
        'UNEhaber
        '
        Me.UNEhaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEhaber.Location = New System.Drawing.Point(535, 405)
        Me.UNEhaber.Name = "UNEhaber"
        Me.UNEhaber.NumericType = Infragistics.Win.UltraWinEditors.NumericType.[Double]
        Me.UNEhaber.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEhaber.ReadOnly = True
        Me.UNEhaber.Size = New System.Drawing.Size(122, 23)
        Me.UNEhaber.TabIndex = 4
        '
        'CBtConfirmar
        '
        Me.CBtConfirmar.BackColor = System.Drawing.Color.Transparent
        Me.CBtConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtConfirmar.ImageIndex = 0
        Me.CBtConfirmar.Location = New System.Drawing.Point(234, 434)
        Me.CBtConfirmar.Name = "CBtConfirmar"
        Me.CBtConfirmar.Size = New System.Drawing.Size(192, 37)
        Me.CBtConfirmar.TabIndex = 53
        Me.CBtConfirmar.Text = "Confirmar"
        Me.CBtConfirmar.UseVisualStyleBackColor = False
        '
        'UCEasientoApertura
        '
        EditorButton1.Text = "N° Asiento Cierre:"
        EditorButton1.Width = 185
        Me.UCEasientoApertura.ButtonsLeft.Add(EditorButton1)
        Appearance6.Image = CType(resources.GetObject("Appearance6.Image"), Object)
        EditorButton2.Appearance = Appearance6
        EditorButton2.Width = 80
        Me.UCEasientoApertura.ButtonsRight.Add(EditorButton2)
        Me.UCEasientoApertura.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEasientoApertura.Location = New System.Drawing.Point(72, 164)
        Me.UCEasientoApertura.MaskInput = "nnnnnnnnnn"
        Me.UCEasientoApertura.Name = "UCEasientoApertura"
        Me.UCEasientoApertura.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEasientoApertura.Size = New System.Drawing.Size(505, 34)
        Me.UCEasientoApertura.TabIndex = 0
        Me.UCEasientoApertura.Visible = False
        '
        'FrmAsientoCierre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(684, 478)
        Me.Controls.Add(Me.UCEasientoApertura)
        Me.Controls.Add(Me.CBtConfirmar)
        Me.Controls.Add(Me.UNEhaber)
        Me.Controls.Add(Me.UNEdebe)
        Me.Controls.Add(Me.UGAsiento)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmAsientoCierre"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmAsientoCierre"
        CType(Me.UGAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEdebe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEhaber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEasientoApertura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGAsiento As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UNEdebe As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UNEhaber As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents CBtConfirmar As System.Windows.Forms.Button
    Friend WithEvents UCEasientoApertura As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
End Class
