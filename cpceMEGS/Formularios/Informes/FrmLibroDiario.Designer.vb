﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLibroDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLibroDiario))
        Me.UNEhoja = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.CBtProcesar = New System.Windows.Forms.Button()
        Me.UCEtransporte = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UGDiario = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UCEHaber = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEDebe = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        CType(Me.UNEhoja, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEtransporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGDiario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEHaber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEDebe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UNEhoja
        '
        Me.UNEhoja.Location = New System.Drawing.Point(98, 27)
        Me.UNEhoja.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.UNEhoja.MaxValue = 9999999
        Me.UNEhoja.MinValue = 0
        Me.UNEhoja.Name = "UNEhoja"
        Me.UNEhoja.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEhoja.Size = New System.Drawing.Size(112, 21)
        Me.UNEhoja.TabIndex = 2
        '
        'CBtProcesar
        '
        Me.CBtProcesar.BackColor = System.Drawing.Color.Transparent
        Me.CBtProcesar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtProcesar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtProcesar.ImageIndex = 0
        Me.CBtProcesar.Location = New System.Drawing.Point(491, 3)
        Me.CBtProcesar.Name = "CBtProcesar"
        Me.CBtProcesar.Size = New System.Drawing.Size(158, 50)
        Me.CBtProcesar.TabIndex = 53
        Me.CBtProcesar.Text = "Procesar"
        Me.CBtProcesar.UseVisualStyleBackColor = False
        '
        'UCEtransporte
        '
        Me.UCEtransporte.Location = New System.Drawing.Point(302, 27)
        Me.UCEtransporte.Name = "UCEtransporte"
        Me.UCEtransporte.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEtransporte.Size = New System.Drawing.Size(112, 21)
        Me.UCEtransporte.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(223, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 21)
        Me.Label5.TabIndex = 58
        Me.Label5.Text = "Hasta"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(19, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 21)
        Me.Label6.TabIndex = 57
        Me.Label6.Text = "Desde"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTdesde
        '
        Me.UDTdesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTdesde.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDTdesde.FormatString = ""
        Me.UDTdesde.Location = New System.Drawing.Point(98, 3)
        Me.UDTdesde.MaskInput = "{date}"
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(112, 21)
        Me.UDTdesde.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDTdesde.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDTdesde.TabIndex = 0
        '
        'UDThasta
        '
        Me.UDThasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDThasta.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDThasta.FormatString = ""
        Me.UDThasta.Location = New System.Drawing.Point(302, 3)
        Me.UDThasta.MaskInput = "{date}"
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(112, 21)
        Me.UDThasta.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDThasta.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDThasta.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(19, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 21)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "Hoja:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(223, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 21)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "Transporte:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UGDiario
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGDiario.DisplayLayout.Appearance = Appearance1
        Me.UGDiario.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGDiario.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGDiario.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGDiario.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGDiario.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGDiario.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDiario.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDiario.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGDiario.Location = New System.Drawing.Point(0, 59)
        Me.UGDiario.Name = "UGDiario"
        Me.UGDiario.Size = New System.Drawing.Size(719, 380)
        Me.UGDiario.TabIndex = 61
        '
        'UCEHaber
        '
        Me.UCEHaber.Location = New System.Drawing.Point(605, 445)
        Me.UCEHaber.Name = "UCEHaber"
        Me.UCEHaber.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEHaber.Size = New System.Drawing.Size(112, 21)
        Me.UCEHaber.TabIndex = 62
        '
        'UCEDebe
        '
        Me.UCEDebe.Location = New System.Drawing.Point(491, 445)
        Me.UCEDebe.Name = "UCEDebe"
        Me.UCEDebe.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEDebe.Size = New System.Drawing.Size(112, 21)
        Me.UCEDebe.TabIndex = 63
        '
        'FrmLibroDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(719, 469)
        Me.Controls.Add(Me.UCEDebe)
        Me.Controls.Add(Me.UCEHaber)
        Me.Controls.Add(Me.UGDiario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.UCEtransporte)
        Me.Controls.Add(Me.CBtProcesar)
        Me.Controls.Add(Me.UNEhoja)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmLibroDiario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Libro Diario"
        CType(Me.UNEhoja, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEtransporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGDiario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEHaber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEDebe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UNEhoja As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents CBtProcesar As System.Windows.Forms.Button
    Friend WithEvents UCEtransporte As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UGDiario As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UCEHaber As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEDebe As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
End Class
