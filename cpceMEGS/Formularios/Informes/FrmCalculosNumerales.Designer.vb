﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCalculosNumerales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCalculosNumerales))
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBtProcesar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UTbCuentas = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UltraTree1 = New Infragistics.Win.UltraWinTree.UltraTree()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.UGNumerales = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UCEMontoAcredita = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.CBtDevengar = New System.Windows.Forms.Button()
        Me.UDTfecDev = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.LTasaEfectiva = New System.Windows.Forms.Label()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTbCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGNumerales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEMontoAcredita, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTfecDev, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(-2, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(154, 23)
        Me.Label3.TabIndex = 49
        Me.Label3.Text = "Cuentas Involucradas:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CBtProcesar
        '
        Me.CBtProcesar.BackColor = System.Drawing.Color.Transparent
        Me.CBtProcesar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtProcesar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtProcesar.ImageIndex = 0
        Me.CBtProcesar.Location = New System.Drawing.Point(761, 8)
        Me.CBtProcesar.Name = "CBtProcesar"
        Me.CBtProcesar.Size = New System.Drawing.Size(143, 24)
        Me.CBtProcesar.TabIndex = 51
        Me.CBtProcesar.Text = "Procesar"
        Me.CBtProcesar.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(564, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(345, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 21)
        Me.Label1.TabIndex = 54
        Me.Label1.Text = "Desde"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTdesde
        '
        Me.UDTdesde.DateTime = New Date(2015, 7, 1, 0, 0, 0, 0)
        Me.UDTdesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTdesde.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDTdesde.FormatString = ""
        Me.UDTdesde.Location = New System.Drawing.Point(424, 9)
        Me.UDTdesde.MaskInput = "{date}"
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(112, 21)
        Me.UDTdesde.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDTdesde.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDTdesde.TabIndex = 52
        Me.UDTdesde.Value = New Date(2015, 7, 1, 0, 0, 0, 0)
        '
        'UDThasta
        '
        Me.UDThasta.DateTime = New Date(2016, 6, 30, 0, 0, 0, 0)
        Me.UDThasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDThasta.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDThasta.FormatString = ""
        Me.UDThasta.Location = New System.Drawing.Point(643, 9)
        Me.UDThasta.MaskInput = "{date}"
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(112, 21)
        Me.UDThasta.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDThasta.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDThasta.TabIndex = 53
        Me.UDThasta.Value = New Date(2016, 6, 30, 0, 0, 0, 0)
        '
        'UTbCuentas
        '
        Appearance1.Image = CType(resources.GetObject("Appearance1.Image"), Object)
        EditorButton1.Appearance = Appearance1
        EditorButton1.Width = 50
        Me.UTbCuentas.ButtonsRight.Add(EditorButton1)
        Me.UTbCuentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTbCuentas.Location = New System.Drawing.Point(152, 36)
        Me.UTbCuentas.MaxLength = 8
        Me.UTbCuentas.Name = "UTbCuentas"
        Me.UTbCuentas.Size = New System.Drawing.Size(173, 24)
        Me.UTbCuentas.TabIndex = 56
        '
        'UltraTree1
        '
        Me.UltraTree1.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.UltraTree1.Location = New System.Drawing.Point(-2, 62)
        Me.UltraTree1.Name = "UltraTree1"
        Me.UltraTree1.NodeConnectorColor = System.Drawing.SystemColors.ControlDark
        Me.UltraTree1.Size = New System.Drawing.Size(327, 267)
        Me.UltraTree1.TabIndex = 57
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.Location = New System.Drawing.Point(345, 36)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(572, 28)
        Me.UltraProgressBar1.TabIndex = 58
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Enabled = False
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(479, 365)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(150, 30)
        Me.CBtExportar.TabIndex = 124
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'UGNumerales
        '
        Appearance2.BackColor = System.Drawing.Color.White
        Me.UGNumerales.DisplayLayout.Appearance = Appearance2
        Me.UGNumerales.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGNumerales.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGNumerales.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGNumerales.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance3.BackColor = System.Drawing.Color.Transparent
        Me.UGNumerales.DisplayLayout.Override.CardAreaAppearance = Appearance3
        Me.UGNumerales.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance4.FontData.BoldAsString = "True"
        Appearance4.FontData.Name = "Arial"
        Appearance4.FontData.SizeInPoints = 10.0!
        Appearance4.ForeColor = System.Drawing.Color.White
        Appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGNumerales.DisplayLayout.Override.HeaderAppearance = Appearance4
        Me.UGNumerales.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGNumerales.DisplayLayout.Override.RowSelectorAppearance = Appearance5
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGNumerales.DisplayLayout.Override.SelectedRowAppearance = Appearance6
        Me.UGNumerales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGNumerales.Location = New System.Drawing.Point(331, 70)
        Me.UGNumerales.Name = "UGNumerales"
        Me.UGNumerales.Size = New System.Drawing.Size(598, 259)
        Me.UGNumerales.TabIndex = 125
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(-2, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 23)
        Me.Label2.TabIndex = 126
        Me.Label2.Text = "Monto a acreditar:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEMontoAcredita
        '
        EditorButton2.Width = 50
        Me.UCEMontoAcredita.ButtonsRight.Add(EditorButton2)
        Me.UCEMontoAcredita.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEMontoAcredita.Location = New System.Drawing.Point(118, 9)
        Me.UCEMontoAcredita.Name = "UCEMontoAcredita"
        Me.UCEMontoAcredita.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEMontoAcredita.Size = New System.Drawing.Size(207, 24)
        Me.UCEMontoAcredita.TabIndex = 127
        '
        'CBtDevengar
        '
        Me.CBtDevengar.BackColor = System.Drawing.Color.Transparent
        Me.CBtDevengar.Enabled = False
        Me.CBtDevengar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtDevengar.ImageIndex = 0
        Me.CBtDevengar.Location = New System.Drawing.Point(670, 365)
        Me.CBtDevengar.Name = "CBtDevengar"
        Me.CBtDevengar.Size = New System.Drawing.Size(150, 30)
        Me.CBtDevengar.TabIndex = 128
        Me.CBtDevengar.Text = "Devengar Renta"
        Me.CBtDevengar.UseVisualStyleBackColor = False
        '
        'UDTfecDev
        '
        EditorButton3.Key = "Left"
        EditorButton3.Text = "Fecha devengamiento:"
        EditorButton3.Width = 180
        Me.UDTfecDev.ButtonsLeft.Add(EditorButton3)
        Me.UDTfecDev.DateTime = New Date(2016, 10, 22, 0, 0, 0, 0)
        Me.UDTfecDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTfecDev.Location = New System.Drawing.Point(467, 333)
        Me.UDTfecDev.Name = "UDTfecDev"
        Me.UDTfecDev.Size = New System.Drawing.Size(353, 28)
        Me.UDTfecDev.TabIndex = 129
        Me.UDTfecDev.Value = New Date(2016, 10, 22, 0, 0, 0, 0)
        '
        'LTasaEfectiva
        '
        Me.LTasaEfectiva.AutoSize = True
        Me.LTasaEfectiva.Location = New System.Drawing.Point(12, 365)
        Me.LTasaEfectiva.Name = "LTasaEfectiva"
        Me.LTasaEfectiva.Size = New System.Drawing.Size(76, 13)
        Me.LTasaEfectiva.TabIndex = 130
        Me.LTasaEfectiva.Text = "Tasa Efectiva:"
        '
        'FrmCalculosNumerales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(929, 398)
        Me.Controls.Add(Me.LTasaEfectiva)
        Me.Controls.Add(Me.UDTfecDev)
        Me.Controls.Add(Me.CBtDevengar)
        Me.Controls.Add(Me.UCEMontoAcredita)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.UGNumerales)
        Me.Controls.Add(Me.CBtExportar)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.UltraTree1)
        Me.Controls.Add(Me.UTbCuentas)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.CBtProcesar)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmCalculosNumerales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calculos Numerales"
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTbCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGNumerales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEMontoAcredita, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTfecDev, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBtProcesar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UTbCuentas As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UltraTree1 As Infragistics.Win.UltraWinTree.UltraTree
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents UGNumerales As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UCEMontoAcredita As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents CBtDevengar As System.Windows.Forms.Button
    Friend WithEvents UDTfecDev As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents LTasaEfectiva As System.Windows.Forms.Label
End Class
