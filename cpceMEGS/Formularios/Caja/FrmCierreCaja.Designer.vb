﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCierreCaja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCierreCaja))
        Me.UltraDateTimeEditor1 = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.LblFecha = New System.Windows.Forms.Label()
        Me.CBtCerrar = New System.Windows.Forms.Button()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraDateTimeEditor1
        '
        Me.UltraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UltraDateTimeEditor1.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UltraDateTimeEditor1.FormatString = ""
        Me.UltraDateTimeEditor1.Location = New System.Drawing.Point(114, 37)
        Me.UltraDateTimeEditor1.MaskInput = "{date}"
        Me.UltraDateTimeEditor1.Name = "UltraDateTimeEditor1"
        Me.UltraDateTimeEditor1.Size = New System.Drawing.Size(151, 21)
        Me.UltraDateTimeEditor1.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UltraDateTimeEditor1.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UltraDateTimeEditor1.TabIndex = 21
        '
        'LblFecha
        '
        Me.LblFecha.BackColor = System.Drawing.Color.SteelBlue
        Me.LblFecha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.ForeColor = System.Drawing.SystemColors.Window
        Me.LblFecha.Location = New System.Drawing.Point(20, 37)
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(93, 21)
        Me.LblFecha.TabIndex = 112
        Me.LblFecha.Text = "Proxima Caja:"
        Me.LblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CBtCerrar
        '
        Me.CBtCerrar.BackColor = System.Drawing.Color.Transparent
        Me.CBtCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBtCerrar.ForeColor = System.Drawing.Color.Black
        Me.CBtCerrar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Blocked_32xMD_color
        Me.CBtCerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtCerrar.Location = New System.Drawing.Point(87, 94)
        Me.CBtCerrar.Name = "CBtCerrar"
        Me.CBtCerrar.Size = New System.Drawing.Size(142, 39)
        Me.CBtCerrar.TabIndex = 113
        Me.CBtCerrar.Text = "Cerrar"
        Me.CBtCerrar.UseVisualStyleBackColor = False
        '
        'FrmCierreCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(327, 169)
        Me.Controls.Add(Me.CBtCerrar)
        Me.Controls.Add(Me.LblFecha)
        Me.Controls.Add(Me.UltraDateTimeEditor1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCierreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cierre de Caja"
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UltraDateTimeEditor1 As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents LblFecha As System.Windows.Forms.Label
    Friend WithEvents CBtCerrar As System.Windows.Forms.Button
End Class
