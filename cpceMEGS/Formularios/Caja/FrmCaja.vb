﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Public Class FrmCaja
    Private cnn As New ConsultaBD(True)
    Private DSComproba As DataSet
    Private DAComproba As MySqlDataAdapter
    Private DTCaja As DataTable
    Private DACaja As MySqlDataAdapter
    Private BSCaja As BindingSource
    Private cmdCaja As MySqlCommandBuilder
    Private mysqlFecha As MySqlDateTime


    Private Sub MuestraComproba()
        DAComproba = cnn.consultaBDadapter("(comproba inner join procesos on pro_codigo=com_proceso) left join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc", "com_fecha as Fecha,com_unegos as Institucion,com_proceso as Proceso,pro_nombre as Descripcion,com_nrocli as Delegacion,com_nrocom as Comprobante,com_nroasi as Asiento,com_total as Importe,afi_nombre as Destinatario,com_estado as Estado,com_asiant as NroAsiAnterior", "1 order by com_fecha desc")
        DSComproba = New DataSet
        DAComproba.Fill(DSComproba, "comproba")
        UltraGrid1.DataSource = DSComproba.Tables(0)
        With UltraGrid1.DisplayLayout.Bands(0)
            .Columns(0).Width = 130
            .Columns(3).Width = 300
            .Columns(7).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(8).Width = 300
        End With
    End Sub

    Private Sub MuestraCaja()
        Try
            '  DACaja = cnn.consultaBDadapter("(cajas a LEFT JOIN operador b ON b.ope_nroope=a.caj_opeape) LEFT JOIN operador c ON c.ope_nroope=a.caj_opecie", "caj_nrocli as Delegacion,caj_caja as Caja,caj_zeta as Zeta,caj_cajact as Activa,caj_turno as Turno,caj_fecven as Fecha,caj_fecape as Apertura,b.ope_nombre as AbrioCajero,caj_feccie as Cierre,c.ope_nombre as CerroCajero")
            Dim tabla As String = "cajas"
            Dim campos As String = "caj_nrocli as Delegacion,caj_caja as Caja,caj_zeta as Zeta,caj_cajact as Activa,caj_turno as Turno,caj_fecven as Fecha,caj_fecape as Apertura,caj_opeape as AbrioCajero,caj_feccie as Cierre,caj_opecie as CerroCajero"
            Dim condiciones As String = "caj_nrocli=" & nPubNroCli
            DACaja = cnn.consultaBDadapter(tabla, campos, condiciones)
            DTCaja = New DataTable
            DACaja.Fill(DTCaja)
            Dim columns(1) As DataColumn
            columns(0) = DTCaja.Columns("Zeta")
            DTCaja.PrimaryKey = columns

            cmdCaja = New MySqlCommandBuilder(DACaja)
            BSCaja = New BindingSource
            BSCaja.DataSource = DTCaja
            UltraGrid1.DataSource = BSCaja
            With UltraGrid1.DisplayLayout.Bands(0)
                .Columns(5).Width = 75
                .Columns(6).Width = 140
                .Columns(8).Width = 140
            End With
            BSCaja.Position = DTCaja.Rows.Count
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CierreCaja()
        If DTCaja.Rows(BSCaja.Position).Item("Activa") = "*" Then
            Dim frmCierra As New FrmCierreCaja
            frmCierra.ShowDialog()
            If frmCierra.DialogResult = DialogResult.OK Then
                Dim RowCajaActual As DataRow = DTCaja.Rows(BSCaja.Position)
                Dim RowCajaNueva As DataRow = DTCaja.NewRow
                Dim dFechaCaja As MySqlDateTime
                RowCajaActual.Item("Activa") = ""
                dFechaCaja.Day = frmCierra.FechaCaja.Day
                dFechaCaja.Month = frmCierra.FechaCaja.Month
                dFechaCaja.Year = frmCierra.FechaCaja.Year
                RowCajaNueva.Item("Delegacion") = RowCajaActual.Item("Delegacion")
                RowCajaNueva.Item("Caja") = RowCajaActual.Item("Caja")
                RowCajaNueva.Item("Zeta") = RowCajaActual.Item("Zeta") + 1
                RowCajaNueva.Item("Fecha") = dFechaCaja
                RowCajaNueva.Item("Activa") = "*"
                RowCajaNueva.Item("Turno") = RowCajaActual.Item("Turno")
                dFechaCaja.Day = Now.Day
                dFechaCaja.Month = Now.Month
                dFechaCaja.Year = Now.Year
                dFechaCaja.Hour = Now.Hour
                dFechaCaja.Minute = Now.Minute
                dFechaCaja.Second = Now.Second
                RowCajaNueva.Item("Apertura") = dFechaCaja
                RowCajaActual.Item("Cierre") = dFechaCaja
                RowCajaActual.Item("CerroCajero") = nPubNroOperador
                RowCajaNueva.Item("AbrioCajero") = nPubNroOperador
                DTCaja.Rows.Add(RowCajaNueva)
                DACaja.Update(DTCaja)
            End If
        Else
            MessageBox.Show("Debe seleccionar la caja activa para poder cerrarla", "Caja", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub FrmCaja_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'If nPubNroOperador <> 100 Then
        DTPdesde.Visible = False
        DTPhasta.Visible = False
        Button1.Visible = False
        Button2.Visible = False
        Button3.Visible = False
        Button4.Visible = False
        'End If
        MuestraCaja()
    End Sub
    Private Sub CBtCerrar_Click(sender As Object, e As EventArgs) Handles CBtCerrar.Click
        CierreCaja()
    End Sub
    Private Sub CBtListar_Click(sender As Object, e As EventArgs) Handles CBtListar.Click
        ListaCaja()
    End Sub

    Private Sub ListaCaja()
        Dim DSOperadores As New DataSet
        Dim DTListaCaja As New DataTable
        Dim DAListaCaja As New MySqlDataAdapter

        Dim tabla As String = "(((totales inner join comproba on com_nroasi=tot_nroasi) inner join plancuen on pla_nropla=tot_nropla) inner join procesos on pro_codigo=tot_proceso) LEFT JOIN afiliado ON afi_tipdoc = com_tipdoc and afi_nrodoc = com_nrodoc LEFT JOIN operador ON com_nroope = ope_nroope"
        Dim campos As String = "tot_zeta as caja,CAST(tot_fecha AS char) as fecha,CAST(tot_fecha AS char) as fecapertura,tot_subpla as opeapertura,CAST(tot_fecha AS char) as feccierre,tot_subpla as opecierre,tot_nropla as cuenta,pla_nombre as descripcion,pro_numerador as numerador,tot_proceso as proceso,tot_nrocom as nrocomproba,pro_nombre as descriproceso,tot_nroasi as asiento,CONCAT(afi_titulo,CAST(afi_matricula as char),'-',afi_nombre) as destinatario,(tot_debe-tot_haber) as importe,tot_concepto as banco,tot_nrocheque as cheque, ope_nombre AS nombrecorto"
        Dim condiciones As String = "tot_nrocli=" & DTCaja.Rows(BSCaja.Position).Item("delegacion") & " and tot_caja=" & DTCaja.Rows(BSCaja.Position).Item("caja") & " and tot_zeta=" & DTCaja.Rows(BSCaja.Position).Item("zeta") & " and tot_estado<>'9' and pla_cajachica='Si'"

        DAListaCaja = cnn.consultaBDadapter(tabla, campos, condiciones)
        DAListaCaja.Fill(DTListaCaja)

        DAListaCaja = cnn.consultaBDadapter("operador", , "ope_nroope=" & DTCaja.Rows(BSCaja.Position).Item("AbrioCajero"))
        DAListaCaja.Fill(DSOperadores, "Abrio")

        DAListaCaja = cnn.consultaBDadapter("operador", , "ope_nroope=" & DTCaja.Rows(BSCaja.Position).Item("CerroCajero"))
        DAListaCaja.Fill(DSOperadores, "Cerro")

        For Each rowLisCaj As DataRow In DTListaCaja.Rows
            rowLisCaj.Item("fecha") = DTCaja.Rows(BSCaja.Position).Item("Fecha").ToString
            rowLisCaj.Item("fecapertura") = DTCaja.Rows(BSCaja.Position).Item("Apertura").ToString
            rowLisCaj.Item("opeapertura") = DSOperadores.Tables("Abrio").Rows(0).Item("ope_nombre").ToString
            rowLisCaj.Item("feccierre") = DTCaja.Rows(BSCaja.Position).Item("Cierre").ToString
            If DSOperadores.Tables("Cerro").Rows.Count > 0 Then
                rowLisCaj.Item("opecierre") = DSOperadores.Tables("Cerro").Rows(0).Item("ope_nombre").ToString
            End If
        Next
        Dim frmResumen As New FrmReportes(DTListaCaja, , "CRCaja.rpt", "Caja Diaria")
        frmResumen.ShowDialog()
    End Sub

End Class