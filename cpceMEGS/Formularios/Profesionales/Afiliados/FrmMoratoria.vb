﻿Imports MySql.Data.MySqlClient
Public Class FrmMoratoria
    Private cnn As New ConsultaBD(True)
    Private DTmoratoria As DataTable
    Private DTmoratoriaCargadas As DataTable
    Private BSmoratoria As BindingSource
    Private DAmoratoria As MySqlDataAdapter
    Private r_Matricula As DataRow
    Public Sub New(ByVal RowMatricula As DataRow)
        InitializeComponent()
        r_Matricula = RowMatricula
    End Sub
    Private Sub UltraNumericEditor1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UNEcuoTramo1.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor1.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor2.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor3.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor4.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor5.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor6.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor7.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor8.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor9.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor10.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub UltraCurrencyEditor11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraCurrencyEditor11.KeyPress
        sumototales()
        Tabular(e)
    End Sub
    Private Sub ButtonAgregar_Click(sender As Object, e As EventArgs) Handles ButtonAgregar.Click
        If UNEcuoTramo1.Value > 0 And UCEimpTramo1.Value > 0 Then
            If (UltraCurrencyEditor1.Value + UltraCurrencyEditor2.Value + UltraCurrencyEditor3.Value) - (UltraCurrencyEditor4.Value + UltraCurrencyEditor5.Value + UltraCurrencyEditor6.Value + UltraCurrencyEditor7.Value + UltraCurrencyEditor8.Value + UltraCurrencyEditor9.Value + UltraCurrencyEditor10.Value + UltraCurrencyEditor11.Value) = 0 Then
                AgregaCuotaMoratoria()
                sumototales()
            Else
                MessageBox.Show("Controle los montos ingresados, estan desbalanceado el debe con el haber", "Moratoria", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub
    Private Sub sumototales()
        UCEtotal.Value = UltraCurrencyEditor1.Value + UltraCurrencyEditor2.Value + UltraCurrencyEditor3.Value
        If UCEtotal.Value > 0 Then
            UCEH1.Value = UltraCurrencyEditor1.Value / UCEtotal.Value * 100
            UCEH2.Value = UltraCurrencyEditor2.Value / UCEtotal.Value * 100
            UCEH3.Value = UltraCurrencyEditor3.Value / UCEtotal.Value * 100
            UCED1.Value = UltraCurrencyEditor4.Value / UCEtotal.Value * 100
            UCED2.Value = UltraCurrencyEditor5.Value / UCEtotal.Value * 100
            UCED3.Value = UltraCurrencyEditor6.Value / UCEtotal.Value * 100
            UCED4.Value = UltraCurrencyEditor7.Value / UCEtotal.Value * 100
            UCED5.Value = UltraCurrencyEditor8.Value / UCEtotal.Value * 100
            UCED6.Value = UltraCurrencyEditor9.Value / UCEtotal.Value * 100
            UCED7.Value = UltraCurrencyEditor10.Value / UCEtotal.Value * 100
            UCED8.Value = UltraCurrencyEditor11.Value / UCEtotal.Value * 100
        End If
        UCEdiferencia.Value = 0
        For Each rowCuo As DataRow In DTmoratoria.Rows
            UCEdiferencia.Value += rowCuo.Item("cuotas") * rowCuo.Item("total")
        Next
        UCEdiferencia.Value -= UCEtotal.Value
    End Sub
    Private Function CreoTableMoratoria() As DataTable
        Dim DTPresentacion As New DataTable
        DTPresentacion.TableName = "moratoria"

        Dim dc As DataColumn

        dc = New DataColumn("matricula", GetType(String))
        dc.DefaultValue = r_Matricula.Item("afi_matricula")
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("cuotas", GetType(Integer))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("Total", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("H1", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("H2", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("H3", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D1", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D2", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D3", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D4", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D5", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D6", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D7", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        dc = New DataColumn("D8", GetType(Double))
        dc.DefaultValue = 0
        DTPresentacion.Columns.Add(dc)

        Return DTPresentacion
    End Function

    Private Sub FrmMoratoria_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        DTmoratoria = New DataTable
        DTmoratoria = CreoTableMoratoria()
        BSmoratoria = New BindingSource
        BSmoratoria.DataSource = DTmoratoria
        DataGridView1.DataSource = BSmoratoria
        With DataGridView1
            .Columns(0).Width = 60
            .Columns(1).Width = 60
            .Columns(2).Width = 70
            .Columns(2).DefaultCellStyle.Format = "N2"
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(3).Width = 70
            .Columns(3).DefaultCellStyle.Format = "N2"
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(4).Width = 70
            .Columns(4).DefaultCellStyle.Format = "N2"
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(5).Width = 70
            .Columns(5).DefaultCellStyle.Format = "N2"
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(6).Width = 70
            .Columns(6).DefaultCellStyle.Format = "N2"
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(7).Width = 70
            .Columns(7).DefaultCellStyle.Format = "N2"
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(8).Width = 70
            .Columns(8).DefaultCellStyle.Format = "N2"
            .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(9).Width = 70
            .Columns(9).DefaultCellStyle.Format = "N2"
            .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(10).Width = 70
            .Columns(10).DefaultCellStyle.Format = "N2"
            .Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(11).Width = 70
            .Columns(11).DefaultCellStyle.Format = "N2"
            .Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(12).Width = 70
            .Columns(12).DefaultCellStyle.Format = "N2"
            .Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(13).Width = 70
            .Columns(13).DefaultCellStyle.Format = "N2"
            .Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
    End Sub

    Private Sub AgregaCuotaMoratoria()
        Dim rowNew As DataRow = DTmoratoria.NewRow
        rowNew.Item("cuotas") = UNEcuoTramo1.Value
        rowNew.Item("total") = UCEimpTramo1.Value
        rowNew.Item("H1") = Math.Round(UCEimpTramo1.Value * UCEH1.Value / 100, 2)
        rowNew.Item("H2") = Math.Round(UCEimpTramo1.Value * UCEH2.Value / 100, 2)
        rowNew.Item("H3") = Math.Round(UCEimpTramo1.Value * UCEH3.Value / 100, 2)
        rowNew.Item("D1") = Math.Round(UCEimpTramo1.Value * UCED1.Value / 100, 2)
        'rowNew.Item("D2") = Math.Round(UCEimpTramo1.Value * UCED2.Value / 100, 2)
        rowNew.Item("D2") = 0
        rowNew.Item("D3") = Math.Round(UCEimpTramo1.Value * UCED3.Value / 100, 2)
        'rowNew.Item("D4") = Math.Round(UCEimpTramo1.Value * UCED4.Value / 100, 2)
        rowNew.Item("D4") = 0
        rowNew.Item("D5") = Math.Round(UCEimpTramo1.Value * UCED5.Value / 100, 2)
        rowNew.Item("D6") = Math.Round(UCEimpTramo1.Value * UCED6.Value / 100, 2)
        rowNew.Item("D7") = Math.Round(UCEimpTramo1.Value * UCED7.Value / 100, 2)
        rowNew.Item("D8") = Math.Round(UCEimpTramo1.Value * UCED8.Value / 100, 2)
        'CONTROLO MONTOS
        Dim totH As Double = Math.Round(rowNew.Item("H1") + rowNew.Item("H2") + rowNew.Item("H3"), 2)
        Dim totD As Double = Math.Round(rowNew.Item("D1") + rowNew.Item("D3") + rowNew.Item("D5") + rowNew.Item("D6") + rowNew.Item("D7") + rowNew.Item("D8"), 2)
        Dim diferencia As Double = Math.Round(totH - totD, 2) 'la diferencia puede ser positiva o negativa: 1 o -1
        'If totH > totD Then 'diferencia va a ser positiva: 801 > 800
        If totD = rowNew.Item("total") Then '800 = 800
            If Math.Round(rowNew.Item("H3"), 2) <> Math.Round(rowNew.Item("D7"), 2) Then
                rowNew.Item("H3") = rowNew.Item("H3") - diferencia '801-1=800
            Else
                rowNew.Item("H2") = rowNew.Item("H2") - diferencia '801-1=800
            End If
        ElseIf totH = rowNew.Item("total") Then '801 = 801
            If Math.Round(rowNew.Item("H3"), 2) <> Math.Round(rowNew.Item("D7"), 2) Then
                rowNew.Item("D7") = rowNew.Item("D7") + diferencia '800+1=801
            Else
                rowNew.Item("D6") = rowNew.Item("D6") + diferencia '800+1=801
            End If
        End If
        DTmoratoria.Rows.Add(rowNew)
    End Sub
    Private Sub CBActualizar_Click(sender As Object, e As EventArgs) Handles CBActualizar.Click
        If DTmoratoria.Rows.Count > 0 Then
            If UCEdiferencia.Value = 0 Then
                If MessageBox.Show("Confirma el alta de la moratoria", "Moratoria", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    GrabaMoratoria()
                End If
            Else
                MessageBox.Show("Tiene una diferencia de " & UCEdiferencia.Value, "Moratoria", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub
    Private Sub GrabaMoratoria()
        Dim nNroMoratoria As Integer = 0
        Dim nNroCuo As Integer = 0
        Dim dFecVen As Date = UltraDateTimeEditor1.Value
        Dim DTNroMor As New DataTable
        Dim miTrans As MySqlTransaction
        DAmoratoria = cnn.consultaBDadapter("cuo_det", "max(numero) as numero")
        DAmoratoria.Fill(DTNroMor)
        nNroMoratoria = DTNroMor.Rows(0).Item("numero") + 1
        If cnn.AbrirConexion Then
            miTrans = cnn.InicioTransaccion
            Dim nNroAsiento As Long = cnn.TomaCobteAsiento("ASIENT")
            Dim nNrocomprobante As Long = cnn.TomaCobte(1, nPubNroCli, "MANUAL")
            Dim FechaHora As String
            Dim nItem As Integer = 0
            FechaHora = Format(UDTotorgada.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)

            Try
                If UCEtotal.Value > 0 Then
                    cnn.ReplaceBD("insert into totales set tot_unegos='" & 1 & "'," & "tot_proceso='MANUAL'," &
                                       "tot_nrocli='" & nPubNroCli & "'," &
                                       "tot_nrocom='" & nNrocomprobante & "'," &
                                       "tot_item='" & 1 & "'," &
                                       "tot_nropla='13040400'," &
                                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," &
                                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," &
                                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," &
                                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," &
                                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," &
                                       "tot_nroope='" & nPubNroOperador & "'," &
                                       "tot_nrocuo='" & 1 & "'," &
                                       "tot_fecha='" & FechaHora & "'," &
                                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                       "tot_fecalt='" & cnn.FechaHoraActual & "'," &
                                       "tot_debe='" & UCEtotal.Value.ToString.Replace(",", ".") & "'," &
                                       "tot_nroasi='" & nNroAsiento & "'")
                End If

                If UltraCurrencyEditor1.Value > 0 Then
                    cnn.ReplaceBD("insert into totales set tot_unegos='" & 1 & "'," & "tot_proceso='MANUAL'," &
                                       "tot_nrocli='" & nPubNroCli & "'," &
                                       "tot_nrocom='" & nNrocomprobante & "'," &
                                       "tot_item='" & 2 & "'," &
                                       "tot_nropla='13040100'," &
                                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," &
                                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," &
                                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," &
                                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," &
                                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," &
                                       "tot_nroope='" & nPubNroOperador & "'," &
                                       "tot_nrocuo='" & 1 & "'," &
                                       "tot_fecha='" & FechaHora & "'," &
                                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                       "tot_fecalt='" & cnn.FechaHoraActual & "'," &
                                       "tot_haber='" & UltraCurrencyEditor1.Value.ToString.Replace(",", ".") & "'," &
                                       "tot_nroasi='" & nNroAsiento & "'")
                End If

                If UltraCurrencyEditor2.Value > 0 Then
                    cnn.ReplaceBD("insert into totales set tot_unegos='" & 1 & "'," & "tot_proceso='MANUAL'," &
                                       "tot_nrocli='" & nPubNroCli & "'," &
                                       "tot_nrocom='" & nNrocomprobante & "'," &
                                       "tot_item='" & 3 & "'," &
                                       "tot_nropla='22040700'," &
                                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," &
                                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," &
                                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," &
                                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," &
                                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," &
                                       "tot_nroope='" & nPubNroOperador & "'," &
                                       "tot_nrocuo='" & 1 & "'," &
                                       "tot_fecha='" & FechaHora & "'," &
                                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                       "tot_fecalt='" & cnn.FechaHoraActual & "'," &
                                       "tot_haber='" & UltraCurrencyEditor2.Value.ToString.Replace(",", ".") & "'," &
                                       "tot_nroasi='" & nNroAsiento & "'")
                End If

                If UltraCurrencyEditor3.Value > 0 Then
                    cnn.ReplaceBD("insert into totales set tot_unegos='" & 1 & "'," & "tot_proceso='MANUAL'," &
                                       "tot_nrocli='" & nPubNroCli & "'," &
                                       "tot_nrocom='" & nNrocomprobante & "'," &
                                       "tot_item='" & 4 & "'," &
                                       "tot_nropla='22040800'," &
                                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," &
                                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," &
                                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," &
                                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," &
                                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," &
                                       "tot_nroope='" & nPubNroOperador & "'," &
                                       "tot_nrocuo='" & 1 & "'," &
                                       "tot_fecha='" & FechaHora & "'," &
                                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                       "tot_fecalt='" & cnn.FechaHoraActual & "'," &
                                       "tot_haber='" & UltraCurrencyEditor3.Value.ToString.Replace(",", ".") & "'," &
                                       "tot_nroasi='" & nNroAsiento & "'")
                End If

                'If UltraCurrencyEditor5.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 5 & "'," & _
                '                       "tot_nropla='22020100'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor5.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor6.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 6 & "'," & _
                '                       "tot_nropla='22010200'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor6.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor7.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 7 & "'," & _
                '                       "tot_nropla='22020200'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor7.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor8.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 8 & "'," & _
                '                       "tot_nropla='31021400'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor8.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor9.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 9 & "'," & _
                '                       "tot_nropla='31021300'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor9.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor10.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 10 & "'," & _
                '                       "tot_nropla='31021800'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor10.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                'If UltraCurrencyEditor11.Value > 0 Then
                '    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='MANUAL'," & _
                '                       "tot_nrocli='" & nPubNroCli & "'," & _
                '                       "tot_nrocom='" & nNrocomprobante & "'," & _
                '                       "tot_item='" & 11 & "'," & _
                '                       "tot_nropla='22030406'," & _
                '                       "tot_subpla='" & r_Matricula.Item("afi_tipdoc") & Format(r_Matricula.Item("afi_nrodoc"), "00000000") & "'," & _
                '                       "tot_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," & _
                '                       "tot_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," & _
                '                       "tot_titulo='" & r_Matricula.Item("afi_titulo") & "'," & _
                '                       "tot_matricula='" & r_Matricula.Item("afi_matricula") & "'," & _
                '                       "tot_nroope='" & nPubNroOperador & "'," & _
                '                       "tot_nrocuo='" & 1 & "'," & _
                '                       "tot_fecha='" & FechaHora & "'," & _
                '                       "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," & _
                '                       "tot_fecalt='" & cnn.FechaHoraActual & "'," & _
                '                       "tot_haber='" & UltraCurrencyEditor11.Value.ToString.Replace(",", ".") & "'," & _
                '                       "tot_nroasi='" & nNroAsiento & "'")
                'End If

                cnn.ReplaceBD("insert into comproba set com_unegos='" & 1 & "'," &
                    "com_nrodeleg='" & nPubNroCli & "'," &
                    "com_proceso='MANUAL'," &
                    "com_nrocli='" & nPubNroCli & "'," &
                    "com_nrocom='" & nNrocomprobante & "'," &
                    "com_nroasi='" & nNroAsiento & "'," &
                    "com_fecha='" & FechaHora & "'," &
                    "com_fecalt='" & cnn.FechaHoraActual & "'," &
                    "com_total='" & UCEtotal.Value.ToString.Replace(",", ".") & "'," &
                    "com_imppag='" & UCEtotal.Value.ToString.Replace(",", ".") & "'," &
                    "com_titulo='" & r_Matricula.Item("afi_titulo") & "'," &
                    "com_matricula='" & r_Matricula.Item("afi_matricula") & "'," &
                    "com_tipdoc='" & r_Matricula.Item("afi_tipdoc") & "'," &
                    "com_nrodoc='" & r_Matricula.Item("afi_nrodoc") & "'," &
                    "com_nroope='" & nPubNroOperador & "'")

                For Each rowMor As DataRow In DTmoratoria.Rows
                    For nX As Integer = 1 To rowMor.Item("cuotas")
                        nNroCuo += 1
                        cnn.actualizarBD("INSERT INTO CUO_DET (tipo,numero,ncuota,matricula,total,d1,d2,d3,c1,c2,c3,c4,c5,c6,c7,c8,f_alta,f_vto) " &
                            "VALUES ('1','" & nNroMoratoria & "','" & nNroCuo & "','" & rowMor.Item("matricula") & "','" &
                            (rowMor.Item("H1") + rowMor.Item("H2") + rowMor.Item("H3")).ToString.Replace(",", ".") & "','" &
                            rowMor.Item("H1").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("H2").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("H3").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D1").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D2").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D3").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D4").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D5").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D6").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D7").ToString.Replace(",", ".") & "','" &
                            rowMor.Item("D8").ToString.Replace(",", ".") & "','" &
                            Format(Now.Date, "yyyy-MM-dd") & "','" &
                            Format(dFecVen, "yyyy-MM-dd") & "')")
                        dFecVen = dFecVen.AddMonths(1)
                    Next
                Next
                miTrans.Commit()
            Catch ex As Exception
                miTrans.Rollback()
                MessageBox.Show(ex.Message)
            End Try

            cnn.CerrarConexion()

            MessageBox.Show("Alta realizada satifactoriamente")
        End If
    End Sub

    Private Sub UltraDateTimeEditor1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraDateTimeEditor1.KeyPress
        Tabular(e)
    End Sub

    Private Sub UltraCurrencyEditor12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UCEdiferencia.KeyPress
        sumototales()
        Tabular(e)
    End Sub

    Private Sub CButton3_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton3.Click
        If BSmoratoria.Position > -1 Then
            UCEdiferencia.Value -= DTmoratoria.Rows(BSmoratoria.Position).Item("cuotas") * DTmoratoria.Rows(BSmoratoria.Position).Item("total")
            DTmoratoria.Rows.RemoveAt(BSmoratoria.Position)
        End If
    End Sub
End Class