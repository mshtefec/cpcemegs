﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMoratoria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton4 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton5 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton6 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton7 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton8 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton9 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton10 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton11 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton12 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton13 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton14 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton15 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton16 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim EditorButton17 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMoratoria))
        Me.UNEcuoTramo1 = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.UltraCurrencyEditor1 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor2 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor3 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor4 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor5 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor6 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor7 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor8 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor9 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor10 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor11 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ButtonAgregar = New System.Windows.Forms.Button()
        Me.CBActualizar = New System.Windows.Forms.Button()
        Me.UltraDateTimeEditor1 = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UCEimpTramo1 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEdiferencia = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEtotal = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEH1 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEH2 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCEH3 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED1 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED2 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED3 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED4 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED5 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED6 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED7 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UCED8 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.CButton3 = New System.Windows.Forms.Button()
        Me.UDTotorgada = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        CType(Me.UNEcuoTramo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEimpTramo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEdiferencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEtotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEH1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEH2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEH3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCED8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTotorgada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UNEcuoTramo1
        '
        EditorButton1.Text = "Cuotas Tramo:"
        EditorButton1.Width = 110
        Me.UNEcuoTramo1.ButtonsLeft.Add(EditorButton1)
        Me.UNEcuoTramo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEcuoTramo1.Location = New System.Drawing.Point(450, 17)
        Me.UNEcuoTramo1.MaxValue = 100
        Me.UNEcuoTramo1.MinValue = 0
        Me.UNEcuoTramo1.Name = "UNEcuoTramo1"
        Me.UNEcuoTramo1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEcuoTramo1.Size = New System.Drawing.Size(172, 24)
        Me.UNEcuoTramo1.TabIndex = 13
        '
        'UltraCurrencyEditor1
        '
        EditorButton2.Text = "H1-Deudores Aportes:"
        EditorButton2.Width = 200
        Me.UltraCurrencyEditor1.ButtonsLeft.Add(EditorButton2)
        Me.UltraCurrencyEditor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor1.Location = New System.Drawing.Point(12, 17)
        Me.UltraCurrencyEditor1.Name = "UltraCurrencyEditor1"
        Me.UltraCurrencyEditor1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor1.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor1.TabIndex = 2
        '
        'UltraCurrencyEditor2
        '
        EditorButton3.Text = "H2-Intereses Fdo.Cap.- Comp.:"
        EditorButton3.Width = 200
        Me.UltraCurrencyEditor2.ButtonsLeft.Add(EditorButton3)
        Me.UltraCurrencyEditor2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor2.Location = New System.Drawing.Point(12, 47)
        Me.UltraCurrencyEditor2.Name = "UltraCurrencyEditor2"
        Me.UltraCurrencyEditor2.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor2.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor2.TabIndex = 3
        '
        'UltraCurrencyEditor3
        '
        EditorButton4.Text = "H3-Intereses Gs.Adm. y Ss.Ss.:"
        EditorButton4.Width = 200
        Me.UltraCurrencyEditor3.ButtonsLeft.Add(EditorButton4)
        Me.UltraCurrencyEditor3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor3.Location = New System.Drawing.Point(12, 77)
        Me.UltraCurrencyEditor3.Name = "UltraCurrencyEditor3"
        Me.UltraCurrencyEditor3.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor3.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor3.TabIndex = 4
        '
        'UltraCurrencyEditor4
        '
        EditorButton5.Text = "D1-Aportes Cap. Ind. General:"
        EditorButton5.Width = 200
        Me.UltraCurrencyEditor4.ButtonsLeft.Add(EditorButton5)
        Me.UltraCurrencyEditor4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor4.Location = New System.Drawing.Point(12, 107)
        Me.UltraCurrencyEditor4.Name = "UltraCurrencyEditor4"
        Me.UltraCurrencyEditor4.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor4.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor4.TabIndex = 5
        '
        'UltraCurrencyEditor5
        '
        EditorButton6.Text = "D2-Aportes Cap. Ind. Especial:"
        EditorButton6.Width = 200
        Me.UltraCurrencyEditor5.ButtonsLeft.Add(EditorButton6)
        Me.UltraCurrencyEditor5.Enabled = False
        Me.UltraCurrencyEditor5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor5.Location = New System.Drawing.Point(12, 137)
        Me.UltraCurrencyEditor5.Name = "UltraCurrencyEditor5"
        Me.UltraCurrencyEditor5.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor5.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor5.TabIndex = 6
        '
        'UltraCurrencyEditor6
        '
        EditorButton7.Text = "D3-Fondo Compensa. General:"
        EditorButton7.Width = 200
        Me.UltraCurrencyEditor6.ButtonsLeft.Add(EditorButton7)
        Me.UltraCurrencyEditor6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor6.Location = New System.Drawing.Point(12, 167)
        Me.UltraCurrencyEditor6.Name = "UltraCurrencyEditor6"
        Me.UltraCurrencyEditor6.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor6.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor6.TabIndex = 7
        '
        'UltraCurrencyEditor7
        '
        EditorButton8.Text = "D4-Fondo Compensa. Especial:"
        EditorButton8.Width = 200
        Me.UltraCurrencyEditor7.ButtonsLeft.Add(EditorButton8)
        Me.UltraCurrencyEditor7.Enabled = False
        Me.UltraCurrencyEditor7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor7.Location = New System.Drawing.Point(12, 197)
        Me.UltraCurrencyEditor7.Name = "UltraCurrencyEditor7"
        Me.UltraCurrencyEditor7.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor7.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor7.TabIndex = 8
        '
        'UltraCurrencyEditor8
        '
        EditorButton9.Text = "D5-Servicios Sociales:"
        EditorButton9.Width = 200
        Me.UltraCurrencyEditor8.ButtonsLeft.Add(EditorButton9)
        Me.UltraCurrencyEditor8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor8.Location = New System.Drawing.Point(12, 227)
        Me.UltraCurrencyEditor8.Name = "UltraCurrencyEditor8"
        Me.UltraCurrencyEditor8.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor8.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor8.TabIndex = 9
        '
        'UltraCurrencyEditor9
        '
        EditorButton10.Text = "D6-Gastos Administrativos:"
        EditorButton10.Width = 200
        Me.UltraCurrencyEditor9.ButtonsLeft.Add(EditorButton10)
        Me.UltraCurrencyEditor9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor9.Location = New System.Drawing.Point(12, 257)
        Me.UltraCurrencyEditor9.Name = "UltraCurrencyEditor9"
        Me.UltraCurrencyEditor9.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor9.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor9.TabIndex = 10
        '
        'UltraCurrencyEditor10
        '
        EditorButton11.Text = "D7-Intereses Gs.Adm. y Ss.Ss:"
        EditorButton11.Width = 200
        Me.UltraCurrencyEditor10.ButtonsLeft.Add(EditorButton11)
        Me.UltraCurrencyEditor10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor10.Location = New System.Drawing.Point(12, 287)
        Me.UltraCurrencyEditor10.Name = "UltraCurrencyEditor10"
        Me.UltraCurrencyEditor10.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor10.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor10.TabIndex = 11
        '
        'UltraCurrencyEditor11
        '
        EditorButton12.Text = "D8-Intereses Fdos Cap y Comp:"
        EditorButton12.Width = 200
        Me.UltraCurrencyEditor11.ButtonsLeft.Add(EditorButton12)
        Me.UltraCurrencyEditor11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor11.Location = New System.Drawing.Point(12, 317)
        Me.UltraCurrencyEditor11.Name = "UltraCurrencyEditor11"
        Me.UltraCurrencyEditor11.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor11.Size = New System.Drawing.Size(314, 24)
        Me.UltraCurrencyEditor11.TabIndex = 12
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(431, 119)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(541, 192)
        Me.DataGridView1.TabIndex = 300
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.BackColor = System.Drawing.Color.Transparent
        Me.ButtonAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAgregar.ImageIndex = 0
        Me.ButtonAgregar.Location = New System.Drawing.Point(688, 77)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(128, 38)
        Me.ButtonAgregar.TabIndex = 16
        Me.ButtonAgregar.Text = "Agregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = False
        '
        'CBActualizar
        '
        Me.CBActualizar.BackColor = System.Drawing.Color.Transparent
        Me.CBActualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBActualizar.ImageIndex = 0
        Me.CBActualizar.Location = New System.Drawing.Point(742, 319)
        Me.CBActualizar.Name = "CBActualizar"
        Me.CBActualizar.Size = New System.Drawing.Size(128, 38)
        Me.CBActualizar.TabIndex = 117
        Me.CBActualizar.Text = "Actualizar"
        Me.CBActualizar.UseVisualStyleBackColor = False
        '
        'UltraDateTimeEditor1
        '
        EditorButton13.Text = "1° Vencimiento:"
        EditorButton13.Width = 110
        Me.UltraDateTimeEditor1.ButtonsLeft.Add(EditorButton13)
        Me.UltraDateTimeEditor1.CausesValidation = False
        Me.UltraDateTimeEditor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraDateTimeEditor1.Location = New System.Drawing.Point(450, 82)
        Me.UltraDateTimeEditor1.Name = "UltraDateTimeEditor1"
        Me.UltraDateTimeEditor1.Size = New System.Drawing.Size(216, 24)
        Me.UltraDateTimeEditor1.TabIndex = 15
        '
        'UCEimpTramo1
        '
        EditorButton14.Text = "Importe Cuota Tramo:"
        EditorButton14.Width = 160
        Me.UCEimpTramo1.ButtonsLeft.Add(EditorButton14)
        Me.UCEimpTramo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEimpTramo1.Location = New System.Drawing.Point(638, 17)
        Me.UCEimpTramo1.Name = "UCEimpTramo1"
        Me.UCEimpTramo1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEimpTramo1.Size = New System.Drawing.Size(298, 24)
        Me.UCEimpTramo1.TabIndex = 14
        '
        'UCEdiferencia
        '
        EditorButton15.Text = "Diferencia:"
        EditorButton15.Width = 80
        Me.UCEdiferencia.ButtonsLeft.Add(EditorButton15)
        Me.UCEdiferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEdiferencia.Location = New System.Drawing.Point(638, 47)
        Me.UCEdiferencia.MaskInput = "{currency:-9.2}"
        Me.UCEdiferencia.Name = "UCEdiferencia"
        Me.UCEdiferencia.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEdiferencia.ReadOnly = True
        Me.UCEdiferencia.Size = New System.Drawing.Size(298, 24)
        Me.UCEdiferencia.TabIndex = 160
        '
        'UCEtotal
        '
        EditorButton16.Text = "TOTAL:"
        EditorButton16.Width = 200
        Me.UCEtotal.ButtonsLeft.Add(EditorButton16)
        Me.UCEtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEtotal.Location = New System.Drawing.Point(12, 347)
        Me.UCEtotal.Name = "UCEtotal"
        Me.UCEtotal.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEtotal.ReadOnly = True
        Me.UCEtotal.Size = New System.Drawing.Size(314, 24)
        Me.UCEtotal.TabIndex = 123
        '
        'UCEH1
        '
        Me.UCEH1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEH1.Location = New System.Drawing.Point(331, 17)
        Me.UCEH1.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCEH1.Name = "UCEH1"
        Me.UCEH1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEH1.ReadOnly = True
        Me.UCEH1.Size = New System.Drawing.Size(90, 24)
        Me.UCEH1.TabIndex = 135
        '
        'UCEH2
        '
        Me.UCEH2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEH2.Location = New System.Drawing.Point(331, 47)
        Me.UCEH2.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCEH2.Name = "UCEH2"
        Me.UCEH2.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEH2.ReadOnly = True
        Me.UCEH2.Size = New System.Drawing.Size(90, 24)
        Me.UCEH2.TabIndex = 136
        '
        'UCEH3
        '
        Me.UCEH3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEH3.Location = New System.Drawing.Point(331, 77)
        Me.UCEH3.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCEH3.Name = "UCEH3"
        Me.UCEH3.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEH3.ReadOnly = True
        Me.UCEH3.Size = New System.Drawing.Size(90, 24)
        Me.UCEH3.TabIndex = 137
        '
        'UCED1
        '
        Me.UCED1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED1.Location = New System.Drawing.Point(331, 107)
        Me.UCED1.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED1.Name = "UCED1"
        Me.UCED1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED1.ReadOnly = True
        Me.UCED1.Size = New System.Drawing.Size(90, 24)
        Me.UCED1.TabIndex = 138
        '
        'UCED2
        '
        Me.UCED2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED2.Location = New System.Drawing.Point(331, 137)
        Me.UCED2.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED2.Name = "UCED2"
        Me.UCED2.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED2.ReadOnly = True
        Me.UCED2.Size = New System.Drawing.Size(90, 24)
        Me.UCED2.TabIndex = 139
        '
        'UCED3
        '
        Me.UCED3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED3.Location = New System.Drawing.Point(331, 167)
        Me.UCED3.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED3.Name = "UCED3"
        Me.UCED3.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED3.ReadOnly = True
        Me.UCED3.Size = New System.Drawing.Size(90, 24)
        Me.UCED3.TabIndex = 140
        '
        'UCED4
        '
        Me.UCED4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED4.Location = New System.Drawing.Point(331, 197)
        Me.UCED4.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED4.Name = "UCED4"
        Me.UCED4.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED4.ReadOnly = True
        Me.UCED4.Size = New System.Drawing.Size(90, 24)
        Me.UCED4.TabIndex = 141
        '
        'UCED5
        '
        Me.UCED5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED5.Location = New System.Drawing.Point(331, 227)
        Me.UCED5.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED5.Name = "UCED5"
        Me.UCED5.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED5.ReadOnly = True
        Me.UCED5.Size = New System.Drawing.Size(90, 24)
        Me.UCED5.TabIndex = 142
        '
        'UCED6
        '
        Me.UCED6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED6.Location = New System.Drawing.Point(331, 257)
        Me.UCED6.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED6.Name = "UCED6"
        Me.UCED6.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED6.ReadOnly = True
        Me.UCED6.Size = New System.Drawing.Size(90, 24)
        Me.UCED6.TabIndex = 143
        '
        'UCED7
        '
        Me.UCED7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED7.Location = New System.Drawing.Point(331, 287)
        Me.UCED7.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED7.Name = "UCED7"
        Me.UCED7.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED7.ReadOnly = True
        Me.UCED7.Size = New System.Drawing.Size(90, 24)
        Me.UCED7.TabIndex = 144
        '
        'UCED8
        '
        Me.UCED8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCED8.Location = New System.Drawing.Point(331, 317)
        Me.UCED8.MaskInput = "{LOC}nnn.nnnn%"
        Me.UCED8.Name = "UCED8"
        Me.UCED8.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCED8.ReadOnly = True
        Me.UCED8.Size = New System.Drawing.Size(90, 24)
        Me.UCED8.TabIndex = 145
        '
        'CButton3
        '
        Me.CButton3.BackColor = System.Drawing.Color.Transparent
        Me.CButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButton3.ImageIndex = 0
        Me.CButton3.Location = New System.Drawing.Point(822, 77)
        Me.CButton3.Name = "CButton3"
        Me.CButton3.Size = New System.Drawing.Size(128, 38)
        Me.CButton3.TabIndex = 146
        Me.CButton3.Text = "Eliminar"
        Me.CButton3.UseVisualStyleBackColor = False
        '
        'UDTotorgada
        '
        EditorButton17.Text = "Otorgada:"
        EditorButton17.Width = 110
        Me.UDTotorgada.ButtonsLeft.Add(EditorButton17)
        Me.UDTotorgada.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTotorgada.Location = New System.Drawing.Point(450, 328)
        Me.UDTotorgada.Name = "UDTotorgada"
        Me.UDTotorgada.Size = New System.Drawing.Size(216, 24)
        Me.UDTotorgada.TabIndex = 147
        '
        'FrmMoratoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 388)
        Me.Controls.Add(Me.UDTotorgada)
        Me.Controls.Add(Me.CButton3)
        Me.Controls.Add(Me.UCED8)
        Me.Controls.Add(Me.UCED7)
        Me.Controls.Add(Me.UCED6)
        Me.Controls.Add(Me.UCED5)
        Me.Controls.Add(Me.UCED4)
        Me.Controls.Add(Me.UCED3)
        Me.Controls.Add(Me.UCED2)
        Me.Controls.Add(Me.UCED1)
        Me.Controls.Add(Me.UCEH3)
        Me.Controls.Add(Me.UCEH2)
        Me.Controls.Add(Me.UCEH1)
        Me.Controls.Add(Me.UCEtotal)
        Me.Controls.Add(Me.UCEdiferencia)
        Me.Controls.Add(Me.UCEimpTramo1)
        Me.Controls.Add(Me.UltraDateTimeEditor1)
        Me.Controls.Add(Me.CBActualizar)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.UltraCurrencyEditor11)
        Me.Controls.Add(Me.UltraCurrencyEditor10)
        Me.Controls.Add(Me.UltraCurrencyEditor9)
        Me.Controls.Add(Me.UltraCurrencyEditor8)
        Me.Controls.Add(Me.UltraCurrencyEditor7)
        Me.Controls.Add(Me.UltraCurrencyEditor6)
        Me.Controls.Add(Me.UltraCurrencyEditor5)
        Me.Controls.Add(Me.UltraCurrencyEditor4)
        Me.Controls.Add(Me.UltraCurrencyEditor3)
        Me.Controls.Add(Me.UltraCurrencyEditor2)
        Me.Controls.Add(Me.UltraCurrencyEditor1)
        Me.Controls.Add(Me.UNEcuoTramo1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmMoratoria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Moratorias"
        CType(Me.UNEcuoTramo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEimpTramo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEdiferencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEtotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEH1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEH2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEH3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCED8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTotorgada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UNEcuoTramo1 As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UltraCurrencyEditor1 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor2 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor3 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor4 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor5 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor6 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor7 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor8 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor9 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor10 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor11 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ButtonAgregar As System.Windows.Forms.Button
    Friend WithEvents CBActualizar As System.Windows.Forms.Button
    Friend WithEvents UltraDateTimeEditor1 As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UCEimpTramo1 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEdiferencia As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEtotal As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEH1 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEH2 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCEH3 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED1 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED2 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED3 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED4 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED5 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED6 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED7 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UCED8 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents CButton3 As System.Windows.Forms.Button
    Friend WithEvents UDTotorgada As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
End Class
