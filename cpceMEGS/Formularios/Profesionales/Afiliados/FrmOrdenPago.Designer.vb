﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOrdenPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmOrdenPago))
        Me.UGOrdenPago = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TxNetoReintegro = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TxPagoServicios = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtTotReintegro = New System.Windows.Forms.TextBox()
        Me.UGFormaPagos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.VBtConfirmar = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.CBtGenerarOrdPag = New System.Windows.Forms.Button()
        Me.CBtAgregarPagoServicios = New System.Windows.Forms.Button()
        Me.LabelDebe = New System.Windows.Forms.Label()
        Me.LabelMontoDebe = New System.Windows.Forms.Label()
        Me.LabelSignoDebe = New System.Windows.Forms.Label()
        Me.LabelHaber = New System.Windows.Forms.Label()
        Me.LabelMontoHaber = New System.Windows.Forms.Label()
        Me.LabelSignoHaber = New System.Windows.Forms.Label()
        Me.CBEnviarCorreo = New System.Windows.Forms.CheckBox()
        CType(Me.UGOrdenPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UGFormaPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGOrdenPago
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGOrdenPago.DisplayLayout.Appearance = Appearance1
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGOrdenPago.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGOrdenPago.DisplayLayout.Override.HeaderAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGOrdenPago.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGOrdenPago.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGOrdenPago.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGOrdenPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGOrdenPago.Location = New System.Drawing.Point(0, 0)
        Me.UGOrdenPago.Name = "UGOrdenPago"
        Me.UGOrdenPago.Size = New System.Drawing.Size(1084, 289)
        Me.UGOrdenPago.TabIndex = 0
        Me.UGOrdenPago.Text = "Estado de cuenta"
        '
        'UltraGroupBox1
        '
        Appearance6.BackColor = System.Drawing.Color.SteelBlue
        Appearance6.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal
        Me.UltraGroupBox1.ContentAreaAppearance = Appearance6
        Me.UltraGroupBox1.Controls.Add(Me.TxNetoReintegro)
        Me.UltraGroupBox1.Controls.Add(Me.Label3)
        Me.UltraGroupBox1.Controls.Add(Me.TxPagoServicios)
        Me.UltraGroupBox1.Controls.Add(Me.Label2)
        Me.UltraGroupBox1.Controls.Add(Me.Label1)
        Me.UltraGroupBox1.Controls.Add(Me.TxtTotReintegro)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(820, 290)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(264, 113)
        Me.UltraGroupBox1.TabIndex = 19
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'TxNetoReintegro
        '
        Me.TxNetoReintegro.BackColor = System.Drawing.Color.Black
        Me.TxNetoReintegro.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxNetoReintegro.ForeColor = System.Drawing.SystemColors.Window
        Me.TxNetoReintegro.Location = New System.Drawing.Point(141, 79)
        Me.TxNetoReintegro.Name = "TxNetoReintegro"
        Me.TxNetoReintegro.ReadOnly = True
        Me.TxNetoReintegro.Size = New System.Drawing.Size(111, 26)
        Me.TxNetoReintegro.TabIndex = 24
        Me.TxNetoReintegro.Text = "0,00"
        Me.TxNetoReintegro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(128, 18)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Neto a Reintegrar:"
        '
        'TxPagoServicios
        '
        Me.TxPagoServicios.BackColor = System.Drawing.Color.Black
        Me.TxPagoServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxPagoServicios.ForeColor = System.Drawing.SystemColors.Window
        Me.TxPagoServicios.Location = New System.Drawing.Point(141, 15)
        Me.TxPagoServicios.Name = "TxPagoServicios"
        Me.TxPagoServicios.ReadOnly = True
        Me.TxPagoServicios.Size = New System.Drawing.Size(111, 26)
        Me.TxPagoServicios.TabIndex = 22
        Me.TxPagoServicios.Text = "0,00"
        Me.TxPagoServicios.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 18)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Pago de Servicios:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 18)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Total a Reintegrar:"
        '
        'TxtTotReintegro
        '
        Me.TxtTotReintegro.BackColor = System.Drawing.Color.Black
        Me.TxtTotReintegro.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotReintegro.ForeColor = System.Drawing.SystemColors.Window
        Me.TxtTotReintegro.Location = New System.Drawing.Point(141, 47)
        Me.TxtTotReintegro.Name = "TxtTotReintegro"
        Me.TxtTotReintegro.ReadOnly = True
        Me.TxtTotReintegro.Size = New System.Drawing.Size(111, 26)
        Me.TxtTotReintegro.TabIndex = 19
        Me.TxtTotReintegro.Text = "0,00"
        Me.TxtTotReintegro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'UGFormaPagos
        '
        Me.UGFormaPagos.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance7.BackColor = System.Drawing.Color.White
        Me.UGFormaPagos.DisplayLayout.Appearance = Appearance7
        Appearance8.BackColor = System.Drawing.Color.Transparent
        Me.UGFormaPagos.DisplayLayout.Override.CardAreaAppearance = Appearance8
        Appearance9.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance9.FontData.BoldAsString = "True"
        Appearance9.FontData.Name = "Arial"
        Appearance9.FontData.SizeInPoints = 10.0!
        Appearance9.ForeColor = System.Drawing.Color.White
        Appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGFormaPagos.DisplayLayout.Override.HeaderAppearance = Appearance9
        Appearance10.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance10.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGFormaPagos.DisplayLayout.Override.RowSelectorAppearance = Appearance10
        Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGFormaPagos.DisplayLayout.Override.SelectedRowAppearance = Appearance11
        Me.UGFormaPagos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGFormaPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGFormaPagos.Location = New System.Drawing.Point(0, 290)
        Me.UGFormaPagos.Name = "UGFormaPagos"
        Me.UGFormaPagos.Size = New System.Drawing.Size(820, 154)
        Me.UGFormaPagos.TabIndex = 20
        Me.UGFormaPagos.Text = "Formas de Pago"
        Me.UGFormaPagos.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChange
        '
        'VBtConfirmar
        '
        Me.VBtConfirmar.BackColor = System.Drawing.Color.Transparent
        Me.VBtConfirmar.Enabled = False
        Me.VBtConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VBtConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.VBtConfirmar.ImageIndex = 0
        Me.VBtConfirmar.Location = New System.Drawing.Point(850, 450)
        Me.VBtConfirmar.Name = "VBtConfirmar"
        Me.VBtConfirmar.Size = New System.Drawing.Size(209, 50)
        Me.VBtConfirmar.TabIndex = 21
        Me.VBtConfirmar.Text = "Confirmar"
        Me.VBtConfirmar.UseVisualStyleBackColor = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "add.ico")
        Me.ImageList1.Images.SetKeyName(1, "Search (2).ico")
        Me.ImageList1.Images.SetKeyName(2, "delete.ico")
        '
        'CBtGenerarOrdPag
        '
        Me.CBtGenerarOrdPag.BackColor = System.Drawing.Color.Transparent
        Me.CBtGenerarOrdPag.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtGenerarOrdPag.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtGenerarOrdPag.ImageIndex = 0
        Me.CBtGenerarOrdPag.Location = New System.Drawing.Point(397, 450)
        Me.CBtGenerarOrdPag.Name = "CBtGenerarOrdPag"
        Me.CBtGenerarOrdPag.Size = New System.Drawing.Size(309, 50)
        Me.CBtGenerarOrdPag.TabIndex = 22
        Me.CBtGenerarOrdPag.Text = "Generar Orden de Pago"
        Me.CBtGenerarOrdPag.UseVisualStyleBackColor = False
        Me.CBtGenerarOrdPag.Visible = False
        '
        'CBtAgregarPagoServicios
        '
        Me.CBtAgregarPagoServicios.BackColor = System.Drawing.Color.Transparent
        Me.CBtAgregarPagoServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAgregarPagoServicios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAgregarPagoServicios.ImageIndex = 0
        Me.CBtAgregarPagoServicios.Location = New System.Drawing.Point(737, 450)
        Me.CBtAgregarPagoServicios.Name = "CBtAgregarPagoServicios"
        Me.CBtAgregarPagoServicios.Size = New System.Drawing.Size(221, 50)
        Me.CBtAgregarPagoServicios.TabIndex = 23
        Me.CBtAgregarPagoServicios.Text = "Agregar Pago de Servicios"
        Me.CBtAgregarPagoServicios.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBtAgregarPagoServicios.UseVisualStyleBackColor = False
        Me.CBtAgregarPagoServicios.Visible = False
        '
        'LabelDebe
        '
        Me.LabelDebe.AutoSize = True
        Me.LabelDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDebe.Location = New System.Drawing.Point(10, 473)
        Me.LabelDebe.Name = "LabelDebe"
        Me.LabelDebe.Size = New System.Drawing.Size(46, 16)
        Me.LabelDebe.TabIndex = 39
        Me.LabelDebe.Text = "Debe"
        '
        'LabelMontoDebe
        '
        Me.LabelMontoDebe.AutoSize = True
        Me.LabelMontoDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMontoDebe.Location = New System.Drawing.Point(22, 493)
        Me.LabelMontoDebe.Name = "LabelMontoDebe"
        Me.LabelMontoDebe.Size = New System.Drawing.Size(60, 16)
        Me.LabelMontoDebe.TabIndex = 38
        Me.LabelMontoDebe.Text = "0000,00"
        '
        'LabelSignoDebe
        '
        Me.LabelSignoDebe.AutoSize = True
        Me.LabelSignoDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSignoDebe.Location = New System.Drawing.Point(10, 487)
        Me.LabelSignoDebe.Name = "LabelSignoDebe"
        Me.LabelSignoDebe.Size = New System.Drawing.Size(16, 16)
        Me.LabelSignoDebe.TabIndex = 37
        Me.LabelSignoDebe.Text = "$"
        '
        'LabelHaber
        '
        Me.LabelHaber.AutoSize = True
        Me.LabelHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelHaber.Location = New System.Drawing.Point(164, 473)
        Me.LabelHaber.Name = "LabelHaber"
        Me.LabelHaber.Size = New System.Drawing.Size(51, 16)
        Me.LabelHaber.TabIndex = 42
        Me.LabelHaber.Text = "Haber"
        '
        'LabelMontoHaber
        '
        Me.LabelMontoHaber.AutoSize = True
        Me.LabelMontoHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMontoHaber.Location = New System.Drawing.Point(176, 493)
        Me.LabelMontoHaber.Name = "LabelMontoHaber"
        Me.LabelMontoHaber.Size = New System.Drawing.Size(60, 16)
        Me.LabelMontoHaber.TabIndex = 41
        Me.LabelMontoHaber.Text = "0000,00"
        '
        'LabelSignoHaber
        '
        Me.LabelSignoHaber.AutoSize = True
        Me.LabelSignoHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSignoHaber.Location = New System.Drawing.Point(164, 493)
        Me.LabelSignoHaber.Name = "LabelSignoHaber"
        Me.LabelSignoHaber.Size = New System.Drawing.Size(16, 16)
        Me.LabelSignoHaber.TabIndex = 40
        Me.LabelSignoHaber.Text = "$"
        '
        'CBEnviarCorreo
        '
        Me.CBEnviarCorreo.AutoSize = True
        Me.CBEnviarCorreo.Checked = True
        Me.CBEnviarCorreo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBEnviarCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBEnviarCorreo.Location = New System.Drawing.Point(13, 450)
        Me.CBEnviarCorreo.Name = "CBEnviarCorreo"
        Me.CBEnviarCorreo.Size = New System.Drawing.Size(103, 17)
        Me.CBEnviarCorreo.TabIndex = 43
        Me.CBEnviarCorreo.Text = "Enviar Correo"
        Me.CBEnviarCorreo.UseVisualStyleBackColor = True
        '
        'FrmOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1084, 512)
        Me.Controls.Add(Me.CBEnviarCorreo)
        Me.Controls.Add(Me.LabelHaber)
        Me.Controls.Add(Me.LabelMontoHaber)
        Me.Controls.Add(Me.LabelSignoHaber)
        Me.Controls.Add(Me.LabelDebe)
        Me.Controls.Add(Me.LabelMontoDebe)
        Me.Controls.Add(Me.LabelSignoDebe)
        Me.Controls.Add(Me.CBtAgregarPagoServicios)
        Me.Controls.Add(Me.CBtGenerarOrdPag)
        Me.Controls.Add(Me.VBtConfirmar)
        Me.Controls.Add(Me.UGFormaPagos)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.UGOrdenPago)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmOrdenPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Orden de Pago"
        CType(Me.UGOrdenPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UGFormaPagos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGOrdenPago As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TxtTotReintegro As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxPagoServicios As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxNetoReintegro As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents UGFormaPagos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents VBtConfirmar As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CBtGenerarOrdPag As System.Windows.Forms.Button
    Friend WithEvents CBtAgregarPagoServicios As System.Windows.Forms.Button
    Friend WithEvents LabelDebe As Label
    Friend WithEvents LabelMontoDebe As Label
    Friend WithEvents LabelSignoDebe As Label
    Friend WithEvents LabelHaber As Label
    Friend WithEvents LabelMontoHaber As Label
    Friend WithEvents LabelSignoHaber As Label
    Friend WithEvents CBEnviarCorreo As CheckBox
End Class
