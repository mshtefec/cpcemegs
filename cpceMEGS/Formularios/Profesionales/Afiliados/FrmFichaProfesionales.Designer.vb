﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmFichaProfesionales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFichaProfesionales))
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraGridBand1 As Infragistics.Win.UltraWinGrid.UltraGridBand = New Infragistics.Win.UltraWinGrid.UltraGridBand("", -1)
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGBTitulos = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UDTficha = New System.Windows.Forms.MaskedTextBox()
        Me.UDTmatricula = New System.Windows.Forms.MaskedTextBox()
        Me.UDTdiploma = New System.Windows.Forms.MaskedTextBox()
        Me.UDTgraduacion = New System.Windows.Forms.MaskedTextBox()
        Me.cbCategoria = New System.Windows.Forms.ComboBox()
        Me.txMatricula = New System.Windows.Forms.TextBox()
        Me.cbTitulo = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButtonAdd = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonEdit = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSave = New System.Windows.Forms.ToolStripButton()
        Me.UGBDatos = New Infragistics.Win.Misc.UltraGroupBox()
        Me.CBGenerarRenta = New System.Windows.Forms.ComboBox()
        Me.LGenerarRenta = New System.Windows.Forms.Label()
        Me.MtxExcepIVA = New System.Windows.Forms.MaskedTextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.MtxcbuCredito = New System.Windows.Forms.MaskedTextBox()
        Me.lbCbuCredito = New System.Windows.Forms.Label()
        Me.TxMailAlternativo = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Mtxcbu = New System.Windows.Forms.MaskedTextBox()
        Me.lbCbuDebito = New System.Windows.Forms.Label()
        Me.MtxExcepDGR = New System.Windows.Forms.MaskedTextBox()
        Me.MtxExcepDGI = New System.Windows.Forms.MaskedTextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cbGanancia = New System.Windows.Forms.ComboBox()
        Me.lbGanancia = New System.Windows.Forms.Label()
        Me.CboTipdoc = New System.Windows.Forms.ComboBox()
        Me.TxDocumento = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbDelegacion = New System.Windows.Forms.ComboBox()
        Me.cbProvincia = New System.Windows.Forms.ComboBox()
        Me.cbCivil = New System.Windows.Forms.ComboBox()
        Me.cbSexo = New System.Windows.Forms.ComboBox()
        Me.MtxFecNac = New System.Windows.Forms.MaskedTextBox()
        Me.MtxDGR = New System.Windows.Forms.MaskedTextBox()
        Me.MtxCuit = New System.Windows.Forms.MaskedTextBox()
        Me.MtxCelu = New System.Windows.Forms.MaskedTextBox()
        Me.MtxTelefono = New System.Windows.Forms.MaskedTextBox()
        Me.TxMail = New System.Windows.Forms.TextBox()
        Me.TxGaranteDe = New System.Windows.Forms.TextBox()
        Me.TxLocalidad = New System.Windows.Forms.TextBox()
        Me.TxDireccion = New System.Windows.Forms.TextBox()
        Me.TxNombre = New System.Windows.Forms.TextBox()
        Me.TxApellido = New System.Windows.Forms.TextBox()
        Me.lbMail = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lbCivil = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lbSexo = New System.Windows.Forms.Label()
        Me.lbFecNac = New System.Windows.Forms.Label()
        Me.lbNombre = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxCodPos = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.CButtonActualizar = New System.Windows.Forms.Button()
        Me.CButtonEditar = New System.Windows.Forms.Button()
        Me.CBtCtaCte = New System.Windows.Forms.Button()
        Me.CBtPago = New System.Windows.Forms.Button()
        Me.UGFlia = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.cbtOrdenPago = New System.Windows.Forms.Button()
        Me.CButton3 = New System.Windows.Forms.Button()
        Me.UGBPrestamo = New Infragistics.Win.Misc.UltraGroupBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TBGarantedeNrodoc = New System.Windows.Forms.TextBox()
        Me.TBGaranteNrodoc = New System.Windows.Forms.TextBox()
        Me.TBGarantedeTipdoc = New System.Windows.Forms.TextBox()
        Me.TBGaranteTipdoc = New System.Windows.Forms.TextBox()
        Me.UTEGarantedeNumdoc = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CBGarantedeTipdoc = New System.Windows.Forms.ComboBox()
        Me.UTEGaranteNumdoc = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CBGaranteTipdoc = New System.Windows.Forms.ComboBox()
        Me.TBGarantede = New System.Windows.Forms.TextBox()
        Me.LabelGaranteDe = New System.Windows.Forms.Label()
        Me.TBGarante = New System.Windows.Forms.TextBox()
        Me.LabelGarante = New System.Windows.Forms.Label()
        Me.UGBJubilacion = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TxJubiPeriodosRestantes = New System.Windows.Forms.TextBox()
        Me.TxJubiPeriodosCobrados = New System.Windows.Forms.TextBox()
        Me.LabelJubiPeriodosRestantes = New System.Windows.Forms.Label()
        Me.LabelJubiPeriodosCobrados = New System.Windows.Forms.Label()
        Me.TxJubiImporteCuota = New System.Windows.Forms.TextBox()
        Me.LabelJubiImporteCuota = New System.Windows.Forms.Label()
        Me.GBJubiObservacion = New System.Windows.Forms.GroupBox()
        Me.TxJubiObservacion = New System.Windows.Forms.TextBox()
        Me.CBJubiAbonaCuota = New System.Windows.Forms.ComboBox()
        Me.LabelJubiAbonaCuota = New System.Windows.Forms.Label()
        Me.TxJubiEdadResolucion = New System.Windows.Forms.TextBox()
        Me.LabelJubiEdadResolucion = New System.Windows.Forms.Label()
        Me.TxJubiFondoCompensador = New System.Windows.Forms.TextBox()
        Me.LabelJubiFondoCompensador = New System.Windows.Forms.Label()
        Me.TxJubiNroResolucion = New System.Windows.Forms.TextBox()
        Me.LabelJubiNroResolucion = New System.Windows.Forms.Label()
        Me.TxJubiCapitalizacion = New System.Windows.Forms.TextBox()
        Me.TxJubiCoeficiente = New System.Windows.Forms.TextBox()
        Me.TxJubiPeriodoOpto = New System.Windows.Forms.TextBox()
        Me.LabelJubiCapitalizacion = New System.Windows.Forms.Label()
        Me.LabelJubiCoeficiente = New System.Windows.Forms.Label()
        Me.LabelJubiPeriodoOpto = New System.Windows.Forms.Label()
        Me.MtxJubilacion = New System.Windows.Forms.MaskedTextBox()
        Me.LabelJubiFecha = New System.Windows.Forms.Label()
        Me.UGBObraSocial = New Infragistics.Win.Misc.UltraGroupBox()
        Me.cbObraSocial = New System.Windows.Forms.ComboBox()
        Me.Lb_obrasocial = New System.Windows.Forms.Label()
        Me.TabControlFicha = New System.Windows.Forms.TabControl()
        Me.TPDatos = New System.Windows.Forms.TabPage()
        Me.UGBET = New Infragistics.Win.Misc.UltraGroupBox()
        Me.MtxFechaVencimientoET = New System.Windows.Forms.MaskedTextBox()
        Me.LFechaVencimientoET = New System.Windows.Forms.Label()
        Me.TPFamiliaPrestamo = New System.Windows.Forms.TabPage()
        Me.UGBFamilia = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TPJubilacion = New System.Windows.Forms.TabPage()
        Me.TPTarjetas = New System.Windows.Forms.TabPage()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TxDebNaranja = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TxDebMastercard = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxDebVisa = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxDebTuya = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.UGBTitulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBTitulos.SuspendLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        CType(Me.UGBDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBDatos.SuspendLayout()
        CType(Me.UGFlia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGBPrestamo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBPrestamo.SuspendLayout()
        CType(Me.UTEGarantedeNumdoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEGaranteNumdoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGBJubilacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBJubilacion.SuspendLayout()
        Me.GBJubiObservacion.SuspendLayout()
        CType(Me.UGBObraSocial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBObraSocial.SuspendLayout()
        Me.TabControlFicha.SuspendLayout()
        Me.TPDatos.SuspendLayout()
        CType(Me.UGBET, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBET.SuspendLayout()
        Me.TPFamiliaPrestamo.SuspendLayout()
        CType(Me.UGBFamilia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBFamilia.SuspendLayout()
        Me.TPJubilacion.SuspendLayout()
        Me.TPTarjetas.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UGBTitulos
        '
        Me.UGBTitulos.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBTitulos.Controls.Add(Me.UDTficha)
        Me.UGBTitulos.Controls.Add(Me.UDTmatricula)
        Me.UGBTitulos.Controls.Add(Me.UDTdiploma)
        Me.UGBTitulos.Controls.Add(Me.UDTgraduacion)
        Me.UGBTitulos.Controls.Add(Me.cbCategoria)
        Me.UGBTitulos.Controls.Add(Me.txMatricula)
        Me.UGBTitulos.Controls.Add(Me.cbTitulo)
        Me.UGBTitulos.Controls.Add(Me.Label21)
        Me.UGBTitulos.Controls.Add(Me.Label20)
        Me.UGBTitulos.Controls.Add(Me.Label19)
        Me.UGBTitulos.Controls.Add(Me.Label18)
        Me.UGBTitulos.Controls.Add(Me.Label17)
        Me.UGBTitulos.Controls.Add(Me.Label3)
        Me.UGBTitulos.Controls.Add(Me.Label1)
        Me.UGBTitulos.Controls.Add(Me.BindingNavigator1)
        Me.UGBTitulos.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGBTitulos.ForeColor = System.Drawing.Color.Black
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance1.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance1.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance1.FontData.BoldAsString = "True"
        Appearance1.ForeColor = System.Drawing.Color.White
        Me.UGBTitulos.HeaderAppearance = Appearance1
        Me.UGBTitulos.Location = New System.Drawing.Point(441, 4)
        Me.UGBTitulos.Name = "UGBTitulos"
        Me.UGBTitulos.Size = New System.Drawing.Size(474, 215)
        Me.UGBTitulos.TabIndex = 145
        Me.UGBTitulos.Text = "TITULOS"
        Me.UGBTitulos.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UDTficha
        '
        Me.UDTficha.BackColor = System.Drawing.Color.White
        Me.UDTficha.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UDTficha.ForeColor = System.Drawing.Color.Black
        Me.UDTficha.Location = New System.Drawing.Point(390, 112)
        Me.UDTficha.Mask = "00/00/0000"
        Me.UDTficha.Name = "UDTficha"
        Me.UDTficha.ReadOnly = True
        Me.UDTficha.Size = New System.Drawing.Size(79, 25)
        Me.UDTficha.TabIndex = 182
        Me.UDTficha.ValidatingType = GetType(Date)
        '
        'UDTmatricula
        '
        Me.UDTmatricula.BackColor = System.Drawing.Color.White
        Me.UDTmatricula.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UDTmatricula.ForeColor = System.Drawing.Color.Black
        Me.UDTmatricula.Location = New System.Drawing.Point(138, 112)
        Me.UDTmatricula.Mask = "00/00/0000"
        Me.UDTmatricula.Name = "UDTmatricula"
        Me.UDTmatricula.ReadOnly = True
        Me.UDTmatricula.Size = New System.Drawing.Size(79, 25)
        Me.UDTmatricula.TabIndex = 181
        Me.UDTmatricula.ValidatingType = GetType(Date)
        '
        'UDTdiploma
        '
        Me.UDTdiploma.BackColor = System.Drawing.Color.White
        Me.UDTdiploma.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UDTdiploma.ForeColor = System.Drawing.Color.Black
        Me.UDTdiploma.Location = New System.Drawing.Point(390, 83)
        Me.UDTdiploma.Mask = "00/00/0000"
        Me.UDTdiploma.Name = "UDTdiploma"
        Me.UDTdiploma.ReadOnly = True
        Me.UDTdiploma.Size = New System.Drawing.Size(79, 25)
        Me.UDTdiploma.TabIndex = 180
        Me.UDTdiploma.ValidatingType = GetType(Date)
        '
        'UDTgraduacion
        '
        Me.UDTgraduacion.BackColor = System.Drawing.Color.White
        Me.UDTgraduacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UDTgraduacion.ForeColor = System.Drawing.Color.Black
        Me.UDTgraduacion.Location = New System.Drawing.Point(138, 83)
        Me.UDTgraduacion.Mask = "00/00/0000"
        Me.UDTgraduacion.Name = "UDTgraduacion"
        Me.UDTgraduacion.ReadOnly = True
        Me.UDTgraduacion.Size = New System.Drawing.Size(79, 25)
        Me.UDTgraduacion.TabIndex = 123
        Me.UDTgraduacion.ValidatingType = GetType(Date)
        '
        'cbCategoria
        '
        Me.cbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCategoria.Enabled = False
        Me.cbCategoria.FormattingEnabled = True
        Me.cbCategoria.Location = New System.Drawing.Point(138, 141)
        Me.cbCategoria.Name = "cbCategoria"
        Me.cbCategoria.Size = New System.Drawing.Size(331, 26)
        Me.cbCategoria.TabIndex = 175
        '
        'txMatricula
        '
        Me.txMatricula.BackColor = System.Drawing.Color.White
        Me.txMatricula.ForeColor = System.Drawing.Color.Black
        Me.txMatricula.Location = New System.Drawing.Point(138, 55)
        Me.txMatricula.Name = "txMatricula"
        Me.txMatricula.ReadOnly = True
        Me.txMatricula.Size = New System.Drawing.Size(79, 25)
        Me.txMatricula.TabIndex = 163
        Me.txMatricula.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cbTitulo
        '
        Me.cbTitulo.BackColor = System.Drawing.Color.White
        Me.cbTitulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTitulo.Enabled = False
        Me.cbTitulo.ForeColor = System.Drawing.Color.Black
        Me.cbTitulo.FormattingEnabled = True
        Me.cbTitulo.Location = New System.Drawing.Point(138, 26)
        Me.cbTitulo.Name = "cbTitulo"
        Me.cbTitulo.Size = New System.Drawing.Size(260, 26)
        Me.cbTitulo.TabIndex = 162
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.White
        Me.Label21.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label21.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(7, 143)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(125, 21)
        Me.Label21.TabIndex = 174
        Me.Label21.Text = "*Categoría Activa:"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.White
        Me.Label20.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label20.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(239, 114)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(140, 21)
        Me.Label20.TabIndex = 173
        Me.Label20.Text = "Fecha Ficha de Sipres:"
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.White
        Me.Label19.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label19.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(7, 114)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(125, 21)
        Me.Label19.TabIndex = 172
        Me.Label19.Text = "*Fecha Matrícula:"
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.White
        Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label18.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(239, 85)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(140, 21)
        Me.Label18.TabIndex = 171
        Me.Label18.Text = "Fecha Diploma:"
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.White
        Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label17.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(7, 85)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(125, 21)
        Me.Label17.TabIndex = 170
        Me.Label17.Text = "*Fecha Graduación:"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(7, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 21)
        Me.Label3.TabIndex = 169
        Me.Label3.Text = "*Matrícula:"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(7, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 21)
        Me.Label1.TabIndex = 168
        Me.Label1.Text = "*Título:"
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.AutoSize = False
        Me.BindingNavigator1.BackColor = System.Drawing.Color.RoyalBlue
        Me.BindingNavigator1.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.ToolStripButtonAdd, Me.ToolStripButtonEdit, Me.ToolStripButtonSave})
        Me.BindingNavigator1.Location = New System.Drawing.Point(3, 176)
        Me.BindingNavigator1.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigator1.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigator1.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigator1.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BindingNavigator1.Size = New System.Drawing.Size(468, 36)
        Me.BindingNavigator1.TabIndex = 161
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(37, 33)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 33)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 33)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 36)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 36)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 33)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 33)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 36)
        '
        'ToolStripButtonAdd
        '
        Me.ToolStripButtonAdd.AutoSize = False
        Me.ToolStripButtonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonAdd.Image = CType(resources.GetObject("ToolStripButtonAdd.Image"), System.Drawing.Image)
        Me.ToolStripButtonAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonAdd.Name = "ToolStripButtonAdd"
        Me.ToolStripButtonAdd.RightToLeftAutoMirrorImage = True
        Me.ToolStripButtonAdd.Size = New System.Drawing.Size(50, 35)
        Me.ToolStripButtonAdd.Tag = "Agregar"
        Me.ToolStripButtonAdd.Text = "Altas"
        Me.ToolStripButtonAdd.ToolTipText = "Agregar"
        '
        'ToolStripButtonEdit
        '
        Me.ToolStripButtonEdit.AutoSize = False
        Me.ToolStripButtonEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonEdit.Image = CType(resources.GetObject("ToolStripButtonEdit.Image"), System.Drawing.Image)
        Me.ToolStripButtonEdit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButtonEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonEdit.Name = "ToolStripButtonEdit"
        Me.ToolStripButtonEdit.Size = New System.Drawing.Size(50, 35)
        Me.ToolStripButtonEdit.Tag = "Modificar"
        Me.ToolStripButtonEdit.Text = "ToolStripButton2"
        Me.ToolStripButtonEdit.ToolTipText = "Modificar " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'ToolStripButtonSave
        '
        Me.ToolStripButtonSave.AutoSize = False
        Me.ToolStripButtonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonSave.Enabled = False
        Me.ToolStripButtonSave.Image = CType(resources.GetObject("ToolStripButtonSave.Image"), System.Drawing.Image)
        Me.ToolStripButtonSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButtonSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonSave.Name = "ToolStripButtonSave"
        Me.ToolStripButtonSave.Size = New System.Drawing.Size(50, 35)
        Me.ToolStripButtonSave.Tag = "Confirmar"
        Me.ToolStripButtonSave.Text = "ToolStripButton3"
        Me.ToolStripButtonSave.ToolTipText = "Confirmar" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'UGBDatos
        '
        Me.UGBDatos.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBDatos.Controls.Add(Me.CBGenerarRenta)
        Me.UGBDatos.Controls.Add(Me.LGenerarRenta)
        Me.UGBDatos.Controls.Add(Me.MtxExcepIVA)
        Me.UGBDatos.Controls.Add(Me.Label32)
        Me.UGBDatos.Controls.Add(Me.MtxcbuCredito)
        Me.UGBDatos.Controls.Add(Me.lbCbuCredito)
        Me.UGBDatos.Controls.Add(Me.TxMailAlternativo)
        Me.UGBDatos.Controls.Add(Me.Label30)
        Me.UGBDatos.Controls.Add(Me.Mtxcbu)
        Me.UGBDatos.Controls.Add(Me.lbCbuDebito)
        Me.UGBDatos.Controls.Add(Me.MtxExcepDGR)
        Me.UGBDatos.Controls.Add(Me.MtxExcepDGI)
        Me.UGBDatos.Controls.Add(Me.Label27)
        Me.UGBDatos.Controls.Add(Me.Label26)
        Me.UGBDatos.Controls.Add(Me.cbGanancia)
        Me.UGBDatos.Controls.Add(Me.lbGanancia)
        Me.UGBDatos.Controls.Add(Me.CboTipdoc)
        Me.UGBDatos.Controls.Add(Me.TxDocumento)
        Me.UGBDatos.Controls.Add(Me.Label15)
        Me.UGBDatos.Controls.Add(Me.Label2)
        Me.UGBDatos.Controls.Add(Me.cbDelegacion)
        Me.UGBDatos.Controls.Add(Me.cbProvincia)
        Me.UGBDatos.Controls.Add(Me.cbCivil)
        Me.UGBDatos.Controls.Add(Me.cbSexo)
        Me.UGBDatos.Controls.Add(Me.MtxFecNac)
        Me.UGBDatos.Controls.Add(Me.MtxDGR)
        Me.UGBDatos.Controls.Add(Me.MtxCuit)
        Me.UGBDatos.Controls.Add(Me.MtxCelu)
        Me.UGBDatos.Controls.Add(Me.MtxTelefono)
        Me.UGBDatos.Controls.Add(Me.TxMail)
        Me.UGBDatos.Controls.Add(Me.TxGaranteDe)
        Me.UGBDatos.Controls.Add(Me.TxLocalidad)
        Me.UGBDatos.Controls.Add(Me.TxDireccion)
        Me.UGBDatos.Controls.Add(Me.TxNombre)
        Me.UGBDatos.Controls.Add(Me.TxApellido)
        Me.UGBDatos.Controls.Add(Me.lbMail)
        Me.UGBDatos.Controls.Add(Me.Label23)
        Me.UGBDatos.Controls.Add(Me.Label22)
        Me.UGBDatos.Controls.Add(Me.lbCivil)
        Me.UGBDatos.Controls.Add(Me.Label14)
        Me.UGBDatos.Controls.Add(Me.Label13)
        Me.UGBDatos.Controls.Add(Me.Label12)
        Me.UGBDatos.Controls.Add(Me.Label10)
        Me.UGBDatos.Controls.Add(Me.Label9)
        Me.UGBDatos.Controls.Add(Me.Label8)
        Me.UGBDatos.Controls.Add(Me.lbSexo)
        Me.UGBDatos.Controls.Add(Me.lbFecNac)
        Me.UGBDatos.Controls.Add(Me.lbNombre)
        Me.UGBDatos.Controls.Add(Me.Label4)
        Me.UGBDatos.Controls.Add(Me.TxCodPos)
        Me.UGBDatos.Controls.Add(Me.Label11)
        Me.UGBDatos.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGBDatos.ForeColor = System.Drawing.Color.Black
        Appearance2.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance2.FontData.BoldAsString = "True"
        Appearance2.ForeColor = System.Drawing.Color.White
        Me.UGBDatos.HeaderAppearance = Appearance2
        Me.UGBDatos.Location = New System.Drawing.Point(3, 4)
        Me.UGBDatos.Name = "UGBDatos"
        Me.UGBDatos.Size = New System.Drawing.Size(434, 540)
        Me.UGBDatos.TabIndex = 146
        Me.UGBDatos.Text = "DATOS DEL AFILIADO               * campos obligatorios ** al menos 1"
        Me.UGBDatos.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'CBGenerarRenta
        '
        Me.CBGenerarRenta.BackColor = System.Drawing.Color.White
        Me.CBGenerarRenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBGenerarRenta.Enabled = False
        Me.CBGenerarRenta.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.CBGenerarRenta.ForeColor = System.Drawing.Color.Black
        Me.CBGenerarRenta.FormattingEnabled = True
        Me.CBGenerarRenta.Items.AddRange(New Object() {"SI", "NO"})
        Me.CBGenerarRenta.Location = New System.Drawing.Point(376, 420)
        Me.CBGenerarRenta.Name = "CBGenerarRenta"
        Me.CBGenerarRenta.Size = New System.Drawing.Size(52, 26)
        Me.CBGenerarRenta.TabIndex = 130
        '
        'LGenerarRenta
        '
        Me.LGenerarRenta.BackColor = System.Drawing.Color.White
        Me.LGenerarRenta.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LGenerarRenta.ForeColor = System.Drawing.Color.Black
        Me.LGenerarRenta.Location = New System.Drawing.Point(264, 422)
        Me.LGenerarRenta.Name = "LGenerarRenta"
        Me.LGenerarRenta.Size = New System.Drawing.Size(106, 20)
        Me.LGenerarRenta.TabIndex = 129
        Me.LGenerarRenta.Text = "Generar Renta:"
        Me.LGenerarRenta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MtxExcepIVA
        '
        Me.MtxExcepIVA.BackColor = System.Drawing.Color.White
        Me.MtxExcepIVA.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxExcepIVA.ForeColor = System.Drawing.Color.Black
        Me.MtxExcepIVA.Location = New System.Drawing.Point(179, 447)
        Me.MtxExcepIVA.Mask = "00/00/0000"
        Me.MtxExcepIVA.Name = "MtxExcepIVA"
        Me.MtxExcepIVA.ReadOnly = True
        Me.MtxExcepIVA.Size = New System.Drawing.Size(79, 25)
        Me.MtxExcepIVA.TabIndex = 127
        Me.MtxExcepIVA.ValidatingType = GetType(Date)
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.Color.White
        Me.Label32.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Black
        Me.Label32.Location = New System.Drawing.Point(6, 448)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(167, 20)
        Me.Label32.TabIndex = 128
        Me.Label32.Text = "Vigencia Excepción IVA:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MtxcbuCredito
        '
        Me.MtxcbuCredito.BackColor = System.Drawing.Color.White
        Me.MtxcbuCredito.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxcbuCredito.ForeColor = System.Drawing.Color.Black
        Me.MtxcbuCredito.Location = New System.Drawing.Point(105, 504)
        Me.MtxcbuCredito.Mask = "0000-0000-0000-0000-0000-00"
        Me.MtxcbuCredito.Name = "MtxcbuCredito"
        Me.MtxcbuCredito.ReadOnly = True
        Me.MtxcbuCredito.Size = New System.Drawing.Size(222, 25)
        Me.MtxcbuCredito.TabIndex = 126
        '
        'lbCbuCredito
        '
        Me.lbCbuCredito.BackColor = System.Drawing.Color.White
        Me.lbCbuCredito.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCbuCredito.ForeColor = System.Drawing.Color.Black
        Me.lbCbuCredito.Location = New System.Drawing.Point(6, 504)
        Me.lbCbuCredito.Name = "lbCbuCredito"
        Me.lbCbuCredito.Size = New System.Drawing.Size(93, 20)
        Me.lbCbuCredito.TabIndex = 125
        Me.lbCbuCredito.Text = "CBU Crédito:"
        Me.lbCbuCredito.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxMailAlternativo
        '
        Me.TxMailAlternativo.BackColor = System.Drawing.Color.White
        Me.TxMailAlternativo.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxMailAlternativo.ForeColor = System.Drawing.Color.Black
        Me.TxMailAlternativo.Location = New System.Drawing.Point(105, 336)
        Me.TxMailAlternativo.Name = "TxMailAlternativo"
        Me.TxMailAlternativo.ReadOnly = True
        Me.TxMailAlternativo.Size = New System.Drawing.Size(272, 25)
        Me.TxMailAlternativo.TabIndex = 123
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.Color.White
        Me.Label30.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(6, 337)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(93, 20)
        Me.Label30.TabIndex = 124
        Me.Label30.Text = "Alternativo:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Mtxcbu
        '
        Me.Mtxcbu.BackColor = System.Drawing.Color.White
        Me.Mtxcbu.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Mtxcbu.ForeColor = System.Drawing.Color.Black
        Me.Mtxcbu.Location = New System.Drawing.Point(105, 476)
        Me.Mtxcbu.Mask = "0000-0000-0000-0000-0000-00"
        Me.Mtxcbu.Name = "Mtxcbu"
        Me.Mtxcbu.ReadOnly = True
        Me.Mtxcbu.Size = New System.Drawing.Size(222, 25)
        Me.Mtxcbu.TabIndex = 122
        '
        'lbCbuDebito
        '
        Me.lbCbuDebito.BackColor = System.Drawing.Color.White
        Me.lbCbuDebito.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCbuDebito.ForeColor = System.Drawing.Color.Black
        Me.lbCbuDebito.Location = New System.Drawing.Point(6, 476)
        Me.lbCbuDebito.Name = "lbCbuDebito"
        Me.lbCbuDebito.Size = New System.Drawing.Size(93, 20)
        Me.lbCbuDebito.TabIndex = 121
        Me.lbCbuDebito.Text = "CBU Débito:"
        Me.lbCbuDebito.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MtxExcepDGR
        '
        Me.MtxExcepDGR.BackColor = System.Drawing.Color.White
        Me.MtxExcepDGR.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxExcepDGR.ForeColor = System.Drawing.Color.Black
        Me.MtxExcepDGR.Location = New System.Drawing.Point(179, 420)
        Me.MtxExcepDGR.Mask = "00/00/0000"
        Me.MtxExcepDGR.Name = "MtxExcepDGR"
        Me.MtxExcepDGR.ReadOnly = True
        Me.MtxExcepDGR.Size = New System.Drawing.Size(79, 25)
        Me.MtxExcepDGR.TabIndex = 101
        Me.MtxExcepDGR.ValidatingType = GetType(Date)
        '
        'MtxExcepDGI
        '
        Me.MtxExcepDGI.BackColor = System.Drawing.Color.White
        Me.MtxExcepDGI.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxExcepDGI.ForeColor = System.Drawing.Color.Black
        Me.MtxExcepDGI.Location = New System.Drawing.Point(179, 393)
        Me.MtxExcepDGI.Mask = "00/00/0000"
        Me.MtxExcepDGI.Name = "MtxExcepDGI"
        Me.MtxExcepDGI.ReadOnly = True
        Me.MtxExcepDGI.Size = New System.Drawing.Size(79, 25)
        Me.MtxExcepDGI.TabIndex = 100
        Me.MtxExcepDGI.ValidatingType = GetType(Date)
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.White
        Me.Label27.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(6, 421)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(167, 20)
        Me.Label27.TabIndex = 119
        Me.Label27.Text = "Vigencia Excepción DGR:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label26
        '
        Me.Label26.BackColor = System.Drawing.Color.White
        Me.Label26.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(6, 393)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(167, 20)
        Me.Label26.TabIndex = 118
        Me.Label26.Text = "Vigencia Excepción DGI:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbGanancia
        '
        Me.cbGanancia.BackColor = System.Drawing.Color.White
        Me.cbGanancia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGanancia.Enabled = False
        Me.cbGanancia.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbGanancia.ForeColor = System.Drawing.Color.Black
        Me.cbGanancia.FormattingEnabled = True
        Me.cbGanancia.Items.AddRange(New Object() {"SI", "NO"})
        Me.cbGanancia.Location = New System.Drawing.Point(105, 364)
        Me.cbGanancia.Name = "cbGanancia"
        Me.cbGanancia.Size = New System.Drawing.Size(56, 26)
        Me.cbGanancia.TabIndex = 99
        '
        'lbGanancia
        '
        Me.lbGanancia.BackColor = System.Drawing.Color.White
        Me.lbGanancia.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbGanancia.ForeColor = System.Drawing.Color.Black
        Me.lbGanancia.Location = New System.Drawing.Point(6, 365)
        Me.lbGanancia.Name = "lbGanancia"
        Me.lbGanancia.Size = New System.Drawing.Size(93, 20)
        Me.lbGanancia.TabIndex = 116
        Me.lbGanancia.Text = "Ganancias:"
        Me.lbGanancia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CboTipdoc
        '
        Me.CboTipdoc.BackColor = System.Drawing.Color.White
        Me.CboTipdoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboTipdoc.Enabled = False
        Me.CboTipdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.CboTipdoc.ForeColor = System.Drawing.Color.Black
        Me.CboTipdoc.FormattingEnabled = True
        Me.CboTipdoc.Items.AddRange(New Object() {"DNI", "LE", "LC", "CI", "NN"})
        Me.CboTipdoc.Location = New System.Drawing.Point(105, 27)
        Me.CboTipdoc.Name = "CboTipdoc"
        Me.CboTipdoc.Size = New System.Drawing.Size(57, 26)
        Me.CboTipdoc.TabIndex = 79
        '
        'TxDocumento
        '
        Me.TxDocumento.BackColor = System.Drawing.Color.White
        Me.TxDocumento.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDocumento.ForeColor = System.Drawing.Color.Black
        Me.TxDocumento.Location = New System.Drawing.Point(168, 28)
        Me.TxDocumento.Name = "TxDocumento"
        Me.TxDocumento.ReadOnly = True
        Me.TxDocumento.Size = New System.Drawing.Size(90, 25)
        Me.TxDocumento.TabIndex = 80
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.White
        Me.Label15.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(223, 282)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(93, 23)
        Me.Label15.TabIndex = 82
        Me.Label15.Text = "**Celular:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 21)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "*Documento:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbDelegacion
        '
        Me.cbDelegacion.BackColor = System.Drawing.Color.White
        Me.cbDelegacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbDelegacion.Enabled = False
        Me.cbDelegacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbDelegacion.ForeColor = System.Drawing.Color.Black
        Me.cbDelegacion.FormattingEnabled = True
        Me.cbDelegacion.Items.AddRange(New Object() {"Sede Central", "Saenz Peña", "Villa angela", "Sudoeste"})
        Me.cbDelegacion.Location = New System.Drawing.Point(104, 252)
        Me.cbDelegacion.Name = "cbDelegacion"
        Me.cbDelegacion.Size = New System.Drawing.Size(195, 26)
        Me.cbDelegacion.TabIndex = 94
        '
        'cbProvincia
        '
        Me.cbProvincia.BackColor = System.Drawing.Color.White
        Me.cbProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbProvincia.Enabled = False
        Me.cbProvincia.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbProvincia.ForeColor = System.Drawing.Color.Black
        Me.cbProvincia.FormattingEnabled = True
        Me.cbProvincia.Items.AddRange(New Object() {"Chaco", "Corrientes", "Formosa", "Misiones", "Santa Fe", "Entre Rios", "Santiago del Estero", "Salta", "Jujuy", "Tucuman", "Catamarca", "La Rioja", "San Juan", "Cordoba", "Mendoza", "Buenos Aires", "La Pampa", "Neuquen", "Chubut", "Rio Negro", "Santa Cruz", "Tierra del Fuego"})
        Me.cbProvincia.Location = New System.Drawing.Point(104, 222)
        Me.cbProvincia.Name = "cbProvincia"
        Me.cbProvincia.Size = New System.Drawing.Size(223, 26)
        Me.cbProvincia.TabIndex = 92
        '
        'cbCivil
        '
        Me.cbCivil.BackColor = System.Drawing.Color.White
        Me.cbCivil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCivil.Enabled = False
        Me.cbCivil.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbCivil.ForeColor = System.Drawing.Color.Black
        Me.cbCivil.FormattingEnabled = True
        Me.cbCivil.Items.AddRange(New Object() {"S", "C", "V", "D"})
        Me.cbCivil.Location = New System.Drawing.Point(379, 138)
        Me.cbCivil.Name = "cbCivil"
        Me.cbCivil.Size = New System.Drawing.Size(49, 26)
        Me.cbCivil.TabIndex = 88
        '
        'cbSexo
        '
        Me.cbSexo.BackColor = System.Drawing.Color.White
        Me.cbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSexo.Enabled = False
        Me.cbSexo.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbSexo.ForeColor = System.Drawing.Color.Black
        Me.cbSexo.FormattingEnabled = True
        Me.cbSexo.Items.AddRange(New Object() {"M", "F"})
        Me.cbSexo.Location = New System.Drawing.Point(249, 138)
        Me.cbSexo.Name = "cbSexo"
        Me.cbSexo.Size = New System.Drawing.Size(41, 26)
        Me.cbSexo.TabIndex = 87
        '
        'MtxFecNac
        '
        Me.MtxFecNac.BackColor = System.Drawing.Color.White
        Me.MtxFecNac.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxFecNac.ForeColor = System.Drawing.Color.Black
        Me.MtxFecNac.Location = New System.Drawing.Point(105, 137)
        Me.MtxFecNac.Mask = "00/00/0000"
        Me.MtxFecNac.Name = "MtxFecNac"
        Me.MtxFecNac.ReadOnly = True
        Me.MtxFecNac.Size = New System.Drawing.Size(79, 25)
        Me.MtxFecNac.TabIndex = 86
        Me.MtxFecNac.ValidatingType = GetType(Date)
        '
        'MtxDGR
        '
        Me.MtxDGR.BackColor = System.Drawing.Color.White
        Me.MtxDGR.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxDGR.ForeColor = System.Drawing.Color.Black
        Me.MtxDGR.Location = New System.Drawing.Point(322, 54)
        Me.MtxDGR.Mask = "00-00000000-0"
        Me.MtxDGR.Name = "MtxDGR"
        Me.MtxDGR.ReadOnly = True
        Me.MtxDGR.Size = New System.Drawing.Size(106, 25)
        Me.MtxDGR.TabIndex = 83
        '
        'MtxCuit
        '
        Me.MtxCuit.BackColor = System.Drawing.Color.White
        Me.MtxCuit.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxCuit.ForeColor = System.Drawing.Color.Black
        Me.MtxCuit.Location = New System.Drawing.Point(105, 55)
        Me.MtxCuit.Mask = "00-00000000-0"
        Me.MtxCuit.Name = "MtxCuit"
        Me.MtxCuit.ReadOnly = True
        Me.MtxCuit.Size = New System.Drawing.Size(106, 25)
        Me.MtxCuit.TabIndex = 81
        '
        'MtxCelu
        '
        Me.MtxCelu.BackColor = System.Drawing.Color.White
        Me.MtxCelu.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxCelu.ForeColor = System.Drawing.Color.Black
        Me.MtxCelu.Location = New System.Drawing.Point(322, 281)
        Me.MtxCelu.Mask = "(9999)000000000"
        Me.MtxCelu.Name = "MtxCelu"
        Me.MtxCelu.ReadOnly = True
        Me.MtxCelu.Size = New System.Drawing.Size(106, 25)
        Me.MtxCelu.TabIndex = 96
        '
        'MtxTelefono
        '
        Me.MtxTelefono.BackColor = System.Drawing.Color.White
        Me.MtxTelefono.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxTelefono.ForeColor = System.Drawing.Color.Black
        Me.MtxTelefono.Location = New System.Drawing.Point(105, 281)
        Me.MtxTelefono.Mask = "(9999)000000000"
        Me.MtxTelefono.Name = "MtxTelefono"
        Me.MtxTelefono.ReadOnly = True
        Me.MtxTelefono.Size = New System.Drawing.Size(106, 25)
        Me.MtxTelefono.TabIndex = 95
        '
        'TxMail
        '
        Me.TxMail.BackColor = System.Drawing.Color.White
        Me.TxMail.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxMail.ForeColor = System.Drawing.Color.Black
        Me.TxMail.Location = New System.Drawing.Point(105, 308)
        Me.TxMail.Name = "TxMail"
        Me.TxMail.ReadOnly = True
        Me.TxMail.Size = New System.Drawing.Size(272, 25)
        Me.TxMail.TabIndex = 97
        '
        'TxGaranteDe
        '
        Me.TxGaranteDe.BackColor = System.Drawing.Color.White
        Me.TxGaranteDe.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxGaranteDe.ForeColor = System.Drawing.Color.Black
        Me.TxGaranteDe.Location = New System.Drawing.Point(263, 393)
        Me.TxGaranteDe.Name = "TxGaranteDe"
        Me.TxGaranteDe.ReadOnly = True
        Me.TxGaranteDe.Size = New System.Drawing.Size(165, 25)
        Me.TxGaranteDe.TabIndex = 98
        '
        'TxLocalidad
        '
        Me.TxLocalidad.BackColor = System.Drawing.Color.White
        Me.TxLocalidad.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxLocalidad.ForeColor = System.Drawing.Color.Black
        Me.TxLocalidad.Location = New System.Drawing.Point(104, 194)
        Me.TxLocalidad.Name = "TxLocalidad"
        Me.TxLocalidad.ReadOnly = True
        Me.TxLocalidad.Size = New System.Drawing.Size(223, 25)
        Me.TxLocalidad.TabIndex = 90
        '
        'TxDireccion
        '
        Me.TxDireccion.BackColor = System.Drawing.Color.White
        Me.TxDireccion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDireccion.ForeColor = System.Drawing.Color.Black
        Me.TxDireccion.Location = New System.Drawing.Point(105, 166)
        Me.TxDireccion.Name = "TxDireccion"
        Me.TxDireccion.ReadOnly = True
        Me.TxDireccion.Size = New System.Drawing.Size(323, 25)
        Me.TxDireccion.TabIndex = 89
        '
        'TxNombre
        '
        Me.TxNombre.BackColor = System.Drawing.Color.White
        Me.TxNombre.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxNombre.ForeColor = System.Drawing.Color.Black
        Me.TxNombre.Location = New System.Drawing.Point(105, 110)
        Me.TxNombre.Name = "TxNombre"
        Me.TxNombre.ReadOnly = True
        Me.TxNombre.Size = New System.Drawing.Size(323, 25)
        Me.TxNombre.TabIndex = 85
        '
        'TxApellido
        '
        Me.TxApellido.BackColor = System.Drawing.Color.White
        Me.TxApellido.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxApellido.ForeColor = System.Drawing.Color.Black
        Me.TxApellido.Location = New System.Drawing.Point(104, 82)
        Me.TxApellido.Name = "TxApellido"
        Me.TxApellido.ReadOnly = True
        Me.TxApellido.Size = New System.Drawing.Size(324, 25)
        Me.TxApellido.TabIndex = 84
        '
        'lbMail
        '
        Me.lbMail.BackColor = System.Drawing.Color.White
        Me.lbMail.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMail.ForeColor = System.Drawing.Color.Black
        Me.lbMail.Location = New System.Drawing.Point(6, 309)
        Me.lbMail.Name = "lbMail"
        Me.lbMail.Size = New System.Drawing.Size(93, 23)
        Me.lbMail.TabIndex = 112
        Me.lbMail.Text = "*Email:"
        Me.lbMail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.White
        Me.Label23.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(223, 57)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(93, 21)
        Me.Label23.TabIndex = 111
        Me.Label23.Text = "DGR:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.White
        Me.Label22.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(6, 57)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(93, 20)
        Me.Label22.TabIndex = 110
        Me.Label22.Text = "*CUIT:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbCivil
        '
        Me.lbCivil.BackColor = System.Drawing.Color.White
        Me.lbCivil.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCivil.ForeColor = System.Drawing.Color.Black
        Me.lbCivil.Location = New System.Drawing.Point(296, 139)
        Me.lbCivil.Name = "lbCivil"
        Me.lbCivil.Size = New System.Drawing.Size(78, 23)
        Me.lbCivil.TabIndex = 109
        Me.lbCivil.Text = "*Est. Civil:"
        Me.lbCivil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.White
        Me.Label14.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(6, 282)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 23)
        Me.Label14.TabIndex = 108
        Me.Label14.Text = "**Teléfono:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.White
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(226, 371)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(202, 20)
        Me.Label13.TabIndex = 107
        Me.Label13.Text = "Garante de: utilizar campos PRESTAMO"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.White
        Me.Label12.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(6, 252)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 23)
        Me.Label12.TabIndex = 106
        Me.Label12.Text = "*Delegación:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.White
        Me.Label10.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(6, 224)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 21)
        Me.Label10.TabIndex = 105
        Me.Label10.Text = "*Provincia:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.White
        Me.Label9.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(6, 194)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 21)
        Me.Label9.TabIndex = 104
        Me.Label9.Text = "*Localidad:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(6, 166)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(93, 21)
        Me.Label8.TabIndex = 103
        Me.Label8.Text = "*Dirección:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbSexo
        '
        Me.lbSexo.BackColor = System.Drawing.Color.White
        Me.lbSexo.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbSexo.ForeColor = System.Drawing.Color.Black
        Me.lbSexo.Location = New System.Drawing.Point(190, 139)
        Me.lbSexo.Name = "lbSexo"
        Me.lbSexo.Size = New System.Drawing.Size(53, 23)
        Me.lbSexo.TabIndex = 102
        Me.lbSexo.Text = "*Sexo:"
        Me.lbSexo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbFecNac
        '
        Me.lbFecNac.BackColor = System.Drawing.Color.White
        Me.lbFecNac.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbFecNac.ForeColor = System.Drawing.Color.Black
        Me.lbFecNac.Location = New System.Drawing.Point(6, 137)
        Me.lbFecNac.Name = "lbFecNac"
        Me.lbFecNac.Size = New System.Drawing.Size(93, 23)
        Me.lbFecNac.TabIndex = 101
        Me.lbFecNac.Text = "*Fecha Nac.:"
        Me.lbFecNac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbNombre
        '
        Me.lbNombre.BackColor = System.Drawing.Color.White
        Me.lbNombre.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbNombre.ForeColor = System.Drawing.Color.Black
        Me.lbNombre.Location = New System.Drawing.Point(6, 110)
        Me.lbNombre.Name = "lbNombre"
        Me.lbNombre.Size = New System.Drawing.Size(93, 21)
        Me.lbNombre.TabIndex = 100
        Me.lbNombre.Text = "*Nombre:"
        Me.lbNombre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(6, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 21)
        Me.Label4.TabIndex = 99
        Me.Label4.Text = "*Apellido:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxCodPos
        '
        Me.TxCodPos.BackColor = System.Drawing.Color.White
        Me.TxCodPos.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxCodPos.ForeColor = System.Drawing.Color.Black
        Me.TxCodPos.Location = New System.Drawing.Point(376, 222)
        Me.TxCodPos.Name = "TxCodPos"
        Me.TxCodPos.ReadOnly = True
        Me.TxCodPos.Size = New System.Drawing.Size(52, 25)
        Me.TxCodPos.TabIndex = 93
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.White
        Me.Label11.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(333, 222)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 23)
        Me.Label11.TabIndex = 91
        Me.Label11.Text = "*C.P.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CButtonActualizar
        '
        Me.CButtonActualizar.BackColor = System.Drawing.Color.Transparent
        Me.CButtonActualizar.Enabled = False
        Me.CButtonActualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButtonActualizar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.CButtonActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButtonActualizar.Location = New System.Drawing.Point(5, 585)
        Me.CButtonActualizar.Name = "CButtonActualizar"
        Me.CButtonActualizar.Size = New System.Drawing.Size(150, 38)
        Me.CButtonActualizar.TabIndex = 115
        Me.CButtonActualizar.Text = "Actualizar"
        Me.CButtonActualizar.UseVisualStyleBackColor = False
        '
        'CButtonEditar
        '
        Me.CButtonEditar.BackColor = System.Drawing.Color.Transparent
        Me.CButtonEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButtonEditar.Image = Global.cpceMEGS.My.Resources.Resources.PencilAngled_32xSM_color
        Me.CButtonEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButtonEditar.Location = New System.Drawing.Point(165, 585)
        Me.CButtonEditar.Name = "CButtonEditar"
        Me.CButtonEditar.Size = New System.Drawing.Size(150, 38)
        Me.CButtonEditar.TabIndex = 114
        Me.CButtonEditar.Text = "Editar"
        Me.CButtonEditar.UseVisualStyleBackColor = False
        '
        'CBtCtaCte
        '
        Me.CBtCtaCte.BackColor = System.Drawing.Color.Transparent
        Me.CBtCtaCte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtCtaCte.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CBtCtaCte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtCtaCte.Location = New System.Drawing.Point(323, 585)
        Me.CBtCtaCte.Name = "CBtCtaCte"
        Me.CBtCtaCte.Size = New System.Drawing.Size(150, 38)
        Me.CBtCtaCte.TabIndex = 147
        Me.CBtCtaCte.Text = "Cuenta Corriente"
        Me.CBtCtaCte.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBtCtaCte.UseVisualStyleBackColor = False
        '
        'CBtPago
        '
        Me.CBtPago.BackColor = System.Drawing.Color.Transparent
        Me.CBtPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtPago.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Alert_32xMD_color
        Me.CBtPago.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtPago.Location = New System.Drawing.Point(479, 585)
        Me.CBtPago.Name = "CBtPago"
        Me.CBtPago.Size = New System.Drawing.Size(150, 38)
        Me.CBtPago.TabIndex = 148
        Me.CBtPago.Text = "Liquidaciones"
        Me.CBtPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBtPago.UseVisualStyleBackColor = False
        '
        'UGFlia
        '
        Appearance3.BackColor = System.Drawing.Color.White
        Me.UGFlia.DisplayLayout.Appearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        UltraGridBand1.Header.Appearance = Appearance4
        Me.UGFlia.DisplayLayout.BandsSerializer.Add(UltraGridBand1)
        Me.UGFlia.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGFlia.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Appearance5.BackColor = System.Drawing.Color.Transparent
        Me.UGFlia.DisplayLayout.Override.CardAreaAppearance = Appearance5
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance6.FontData.BoldAsString = "True"
        Appearance6.FontData.Name = "Arial"
        Appearance6.FontData.SizeInPoints = 10.0!
        Appearance6.ForeColor = System.Drawing.Color.White
        Appearance6.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGFlia.DisplayLayout.Override.HeaderAppearance = Appearance6
        Appearance7.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance7.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGFlia.DisplayLayout.Override.RowSelectorAppearance = Appearance7
        Appearance8.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance8.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGFlia.DisplayLayout.Override.SelectedRowAppearance = Appearance8
        Me.UGFlia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGFlia.Location = New System.Drawing.Point(6, 25)
        Me.UGFlia.Name = "UGFlia"
        Me.UGFlia.Size = New System.Drawing.Size(1046, 416)
        Me.UGFlia.TabIndex = 149
        '
        'cbtOrdenPago
        '
        Me.cbtOrdenPago.BackColor = System.Drawing.Color.Transparent
        Me.cbtOrdenPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbtOrdenPago.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Alert_32xMD_color
        Me.cbtOrdenPago.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbtOrdenPago.Location = New System.Drawing.Point(637, 585)
        Me.cbtOrdenPago.Name = "cbtOrdenPago"
        Me.cbtOrdenPago.Size = New System.Drawing.Size(150, 38)
        Me.cbtOrdenPago.TabIndex = 150
        Me.cbtOrdenPago.Text = "Orden de Pago"
        Me.cbtOrdenPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cbtOrdenPago.UseVisualStyleBackColor = False
        '
        'CButton3
        '
        Me.CButton3.BackColor = System.Drawing.Color.Transparent
        Me.CButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CButton3.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Warning_32xMD_color
        Me.CButton3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton3.Location = New System.Drawing.Point(795, 585)
        Me.CButton3.Name = "CButton3"
        Me.CButton3.Size = New System.Drawing.Size(150, 38)
        Me.CButton3.TabIndex = 151
        Me.CButton3.Text = "Moratorias"
        Me.CButton3.UseVisualStyleBackColor = False
        '
        'UGBPrestamo
        '
        Me.UGBPrestamo.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBPrestamo.Controls.Add(Me.Label29)
        Me.UGBPrestamo.Controls.Add(Me.TBGarantedeNrodoc)
        Me.UGBPrestamo.Controls.Add(Me.TBGaranteNrodoc)
        Me.UGBPrestamo.Controls.Add(Me.TBGarantedeTipdoc)
        Me.UGBPrestamo.Controls.Add(Me.TBGaranteTipdoc)
        Me.UGBPrestamo.Controls.Add(Me.UTEGarantedeNumdoc)
        Me.UGBPrestamo.Controls.Add(Me.CBGarantedeTipdoc)
        Me.UGBPrestamo.Controls.Add(Me.UTEGaranteNumdoc)
        Me.UGBPrestamo.Controls.Add(Me.CBGaranteTipdoc)
        Me.UGBPrestamo.Controls.Add(Me.TBGarantede)
        Me.UGBPrestamo.Controls.Add(Me.LabelGaranteDe)
        Me.UGBPrestamo.Controls.Add(Me.TBGarante)
        Me.UGBPrestamo.Controls.Add(Me.LabelGarante)
        Me.UGBPrestamo.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UGBPrestamo.ForeColor = System.Drawing.Color.Black
        Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance11.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance11.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance11.FontData.BoldAsString = "True"
        Appearance11.ForeColor = System.Drawing.Color.White
        Me.UGBPrestamo.HeaderAppearance = Appearance11
        Me.UGBPrestamo.Location = New System.Drawing.Point(6, 459)
        Me.UGBPrestamo.Name = "UGBPrestamo"
        Me.UGBPrestamo.Size = New System.Drawing.Size(1058, 82)
        Me.UGBPrestamo.TabIndex = 152
        Me.UGBPrestamo.Text = "PRESTAMO"
        Me.UGBPrestamo.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Calibri", 10.0!)
        Me.Label29.Location = New System.Drawing.Point(789, 26)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(166, 52)
        Me.Label29.TabIndex = 132
        Me.Label29.Text = "(*) Para poner en blanco un garante debe seleccionar nulo el Tipo de documento."
        '
        'TBGarantedeNrodoc
        '
        Me.TBGarantedeNrodoc.BackColor = System.Drawing.Color.White
        Me.TBGarantedeNrodoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGarantedeNrodoc.ForeColor = System.Drawing.Color.Black
        Me.TBGarantedeNrodoc.Location = New System.Drawing.Point(371, 53)
        Me.TBGarantedeNrodoc.Name = "TBGarantedeNrodoc"
        Me.TBGarantedeNrodoc.ReadOnly = True
        Me.TBGarantedeNrodoc.Size = New System.Drawing.Size(86, 25)
        Me.TBGarantedeNrodoc.TabIndex = 131
        '
        'TBGaranteNrodoc
        '
        Me.TBGaranteNrodoc.BackColor = System.Drawing.Color.White
        Me.TBGaranteNrodoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGaranteNrodoc.ForeColor = System.Drawing.Color.Black
        Me.TBGaranteNrodoc.Location = New System.Drawing.Point(371, 25)
        Me.TBGaranteNrodoc.Name = "TBGaranteNrodoc"
        Me.TBGaranteNrodoc.ReadOnly = True
        Me.TBGaranteNrodoc.Size = New System.Drawing.Size(86, 25)
        Me.TBGaranteNrodoc.TabIndex = 130
        '
        'TBGarantedeTipdoc
        '
        Me.TBGarantedeTipdoc.BackColor = System.Drawing.Color.White
        Me.TBGarantedeTipdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGarantedeTipdoc.ForeColor = System.Drawing.Color.Black
        Me.TBGarantedeTipdoc.Location = New System.Drawing.Point(319, 53)
        Me.TBGarantedeTipdoc.Name = "TBGarantedeTipdoc"
        Me.TBGarantedeTipdoc.ReadOnly = True
        Me.TBGarantedeTipdoc.Size = New System.Drawing.Size(49, 25)
        Me.TBGarantedeTipdoc.TabIndex = 129
        '
        'TBGaranteTipdoc
        '
        Me.TBGaranteTipdoc.BackColor = System.Drawing.Color.White
        Me.TBGaranteTipdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGaranteTipdoc.ForeColor = System.Drawing.Color.Black
        Me.TBGaranteTipdoc.Location = New System.Drawing.Point(319, 25)
        Me.TBGaranteTipdoc.Name = "TBGaranteTipdoc"
        Me.TBGaranteTipdoc.ReadOnly = True
        Me.TBGaranteTipdoc.Size = New System.Drawing.Size(49, 25)
        Me.TBGaranteTipdoc.TabIndex = 128
        '
        'UTEGarantedeNumdoc
        '
        Appearance9.Image = CType(resources.GetObject("Appearance9.Image"), Object)
        EditorButton1.Appearance = Appearance9
        EditorButton1.Width = 70
        Me.UTEGarantedeNumdoc.ButtonsRight.Add(EditorButton1)
        Me.UTEGarantedeNumdoc.Enabled = False
        Me.UTEGarantedeNumdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UTEGarantedeNumdoc.Location = New System.Drawing.Point(161, 52)
        Me.UTEGarantedeNumdoc.MaxLength = 8
        Me.UTEGarantedeNumdoc.Name = "UTEGarantedeNumdoc"
        Me.UTEGarantedeNumdoc.Size = New System.Drawing.Size(153, 27)
        Me.UTEGarantedeNumdoc.TabIndex = 127
        '
        'CBGarantedeTipdoc
        '
        Me.CBGarantedeTipdoc.BackColor = System.Drawing.Color.White
        Me.CBGarantedeTipdoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBGarantedeTipdoc.Enabled = False
        Me.CBGarantedeTipdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.CBGarantedeTipdoc.ForeColor = System.Drawing.Color.Black
        Me.CBGarantedeTipdoc.FormattingEnabled = True
        Me.CBGarantedeTipdoc.Items.AddRange(New Object() {"", "DNI", "LE", "LC", "CI", "NN"})
        Me.CBGarantedeTipdoc.Location = New System.Drawing.Point(101, 53)
        Me.CBGarantedeTipdoc.Name = "CBGarantedeTipdoc"
        Me.CBGarantedeTipdoc.Size = New System.Drawing.Size(57, 26)
        Me.CBGarantedeTipdoc.TabIndex = 126
        '
        'UTEGaranteNumdoc
        '
        Appearance10.Image = CType(resources.GetObject("Appearance10.Image"), Object)
        EditorButton2.Appearance = Appearance10
        EditorButton2.Width = 70
        Me.UTEGaranteNumdoc.ButtonsRight.Add(EditorButton2)
        Me.UTEGaranteNumdoc.Enabled = False
        Me.UTEGaranteNumdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UTEGaranteNumdoc.Location = New System.Drawing.Point(161, 23)
        Me.UTEGaranteNumdoc.MaxLength = 8
        Me.UTEGaranteNumdoc.Name = "UTEGaranteNumdoc"
        Me.UTEGaranteNumdoc.Size = New System.Drawing.Size(153, 27)
        Me.UTEGaranteNumdoc.TabIndex = 125
        '
        'CBGaranteTipdoc
        '
        Me.CBGaranteTipdoc.BackColor = System.Drawing.Color.White
        Me.CBGaranteTipdoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBGaranteTipdoc.Enabled = False
        Me.CBGaranteTipdoc.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.CBGaranteTipdoc.ForeColor = System.Drawing.Color.Black
        Me.CBGaranteTipdoc.FormattingEnabled = True
        Me.CBGaranteTipdoc.Items.AddRange(New Object() {"", "DNI", "LE", "LC", "CI", "NN"})
        Me.CBGaranteTipdoc.Location = New System.Drawing.Point(101, 24)
        Me.CBGaranteTipdoc.Name = "CBGaranteTipdoc"
        Me.CBGaranteTipdoc.Size = New System.Drawing.Size(57, 26)
        Me.CBGaranteTipdoc.TabIndex = 122
        '
        'TBGarantede
        '
        Me.TBGarantede.BackColor = System.Drawing.Color.White
        Me.TBGarantede.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGarantede.ForeColor = System.Drawing.Color.Black
        Me.TBGarantede.Location = New System.Drawing.Point(460, 53)
        Me.TBGarantede.Name = "TBGarantede"
        Me.TBGarantede.ReadOnly = True
        Me.TBGarantede.Size = New System.Drawing.Size(323, 25)
        Me.TBGarantede.TabIndex = 124
        '
        'LabelGaranteDe
        '
        Me.LabelGaranteDe.BackColor = System.Drawing.Color.White
        Me.LabelGaranteDe.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelGaranteDe.ForeColor = System.Drawing.Color.Black
        Me.LabelGaranteDe.Location = New System.Drawing.Point(6, 54)
        Me.LabelGaranteDe.Name = "LabelGaranteDe"
        Me.LabelGaranteDe.Size = New System.Drawing.Size(93, 20)
        Me.LabelGaranteDe.TabIndex = 123
        Me.LabelGaranteDe.Text = "Garante de:"
        Me.LabelGaranteDe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TBGarante
        '
        Me.TBGarante.BackColor = System.Drawing.Color.White
        Me.TBGarante.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TBGarante.ForeColor = System.Drawing.Color.Black
        Me.TBGarante.Location = New System.Drawing.Point(460, 25)
        Me.TBGarante.Name = "TBGarante"
        Me.TBGarante.ReadOnly = True
        Me.TBGarante.Size = New System.Drawing.Size(323, 25)
        Me.TBGarante.TabIndex = 122
        '
        'LabelGarante
        '
        Me.LabelGarante.BackColor = System.Drawing.Color.White
        Me.LabelGarante.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelGarante.ForeColor = System.Drawing.Color.Black
        Me.LabelGarante.Location = New System.Drawing.Point(6, 26)
        Me.LabelGarante.Name = "LabelGarante"
        Me.LabelGarante.Size = New System.Drawing.Size(93, 20)
        Me.LabelGarante.TabIndex = 122
        Me.LabelGarante.Text = "Garante:"
        Me.LabelGarante.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UGBJubilacion
        '
        Me.UGBJubilacion.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBJubilacion.Controls.Add(Me.TxJubiPeriodosRestantes)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiPeriodosCobrados)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiPeriodosRestantes)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiPeriodosCobrados)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiImporteCuota)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiImporteCuota)
        Me.UGBJubilacion.Controls.Add(Me.GBJubiObservacion)
        Me.UGBJubilacion.Controls.Add(Me.CBJubiAbonaCuota)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiAbonaCuota)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiEdadResolucion)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiEdadResolucion)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiFondoCompensador)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiFondoCompensador)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiNroResolucion)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiNroResolucion)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiCapitalizacion)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiCoeficiente)
        Me.UGBJubilacion.Controls.Add(Me.TxJubiPeriodoOpto)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiCapitalizacion)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiCoeficiente)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiPeriodoOpto)
        Me.UGBJubilacion.Controls.Add(Me.MtxJubilacion)
        Me.UGBJubilacion.Controls.Add(Me.LabelJubiFecha)
        Me.UGBJubilacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGBJubilacion.ForeColor = System.Drawing.Color.Black
        Appearance12.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance12.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance12.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance12.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance12.FontData.BoldAsString = "True"
        Appearance12.ForeColor = System.Drawing.Color.White
        Me.UGBJubilacion.HeaderAppearance = Appearance12
        Me.UGBJubilacion.Location = New System.Drawing.Point(6, 6)
        Me.UGBJubilacion.Name = "UGBJubilacion"
        Me.UGBJubilacion.Size = New System.Drawing.Size(1058, 430)
        Me.UGBJubilacion.TabIndex = 153
        Me.UGBJubilacion.Text = "JUBILACIÓN"
        Me.UGBJubilacion.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'TxJubiPeriodosRestantes
        '
        Me.TxJubiPeriodosRestantes.BackColor = System.Drawing.Color.White
        Me.TxJubiPeriodosRestantes.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiPeriodosRestantes.ForeColor = System.Drawing.Color.Black
        Me.TxJubiPeriodosRestantes.Location = New System.Drawing.Point(914, 58)
        Me.TxJubiPeriodosRestantes.Name = "TxJubiPeriodosRestantes"
        Me.TxJubiPeriodosRestantes.ReadOnly = True
        Me.TxJubiPeriodosRestantes.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiPeriodosRestantes.TabIndex = 198
        '
        'TxJubiPeriodosCobrados
        '
        Me.TxJubiPeriodosCobrados.BackColor = System.Drawing.Color.White
        Me.TxJubiPeriodosCobrados.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiPeriodosCobrados.ForeColor = System.Drawing.Color.Black
        Me.TxJubiPeriodosCobrados.Location = New System.Drawing.Point(914, 27)
        Me.TxJubiPeriodosCobrados.Name = "TxJubiPeriodosCobrados"
        Me.TxJubiPeriodosCobrados.ReadOnly = True
        Me.TxJubiPeriodosCobrados.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiPeriodosCobrados.TabIndex = 197
        '
        'LabelJubiPeriodosRestantes
        '
        Me.LabelJubiPeriodosRestantes.BackColor = System.Drawing.Color.White
        Me.LabelJubiPeriodosRestantes.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiPeriodosRestantes.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiPeriodosRestantes.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiPeriodosRestantes.Location = New System.Drawing.Point(668, 60)
        Me.LabelJubiPeriodosRestantes.Name = "LabelJubiPeriodosRestantes"
        Me.LabelJubiPeriodosRestantes.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiPeriodosRestantes.TabIndex = 196
        Me.LabelJubiPeriodosRestantes.Text = "Períodos Restantes:"
        '
        'LabelJubiPeriodosCobrados
        '
        Me.LabelJubiPeriodosCobrados.BackColor = System.Drawing.Color.White
        Me.LabelJubiPeriodosCobrados.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiPeriodosCobrados.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiPeriodosCobrados.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiPeriodosCobrados.Location = New System.Drawing.Point(668, 30)
        Me.LabelJubiPeriodosCobrados.Name = "LabelJubiPeriodosCobrados"
        Me.LabelJubiPeriodosCobrados.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiPeriodosCobrados.TabIndex = 195
        Me.LabelJubiPeriodosCobrados.Text = "Períodos Cobrados:"
        '
        'TxJubiImporteCuota
        '
        Me.TxJubiImporteCuota.BackColor = System.Drawing.Color.White
        Me.TxJubiImporteCuota.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiImporteCuota.ForeColor = System.Drawing.Color.Black
        Me.TxJubiImporteCuota.Location = New System.Drawing.Point(583, 58)
        Me.TxJubiImporteCuota.Name = "TxJubiImporteCuota"
        Me.TxJubiImporteCuota.ReadOnly = True
        Me.TxJubiImporteCuota.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiImporteCuota.TabIndex = 194
        '
        'LabelJubiImporteCuota
        '
        Me.LabelJubiImporteCuota.BackColor = System.Drawing.Color.White
        Me.LabelJubiImporteCuota.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiImporteCuota.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiImporteCuota.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiImporteCuota.Location = New System.Drawing.Point(337, 60)
        Me.LabelJubiImporteCuota.Name = "LabelJubiImporteCuota"
        Me.LabelJubiImporteCuota.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiImporteCuota.TabIndex = 193
        Me.LabelJubiImporteCuota.Text = "Importe de Cuota Actual:"
        '
        'GBJubiObservacion
        '
        Me.GBJubiObservacion.BackColor = System.Drawing.Color.White
        Me.GBJubiObservacion.Controls.Add(Me.TxJubiObservacion)
        Me.GBJubiObservacion.Location = New System.Drawing.Point(6, 198)
        Me.GBJubiObservacion.Name = "GBJubiObservacion"
        Me.GBJubiObservacion.Size = New System.Drawing.Size(656, 164)
        Me.GBJubiObservacion.TabIndex = 192
        Me.GBJubiObservacion.TabStop = False
        Me.GBJubiObservacion.Text = "Observación"
        '
        'TxJubiObservacion
        '
        Me.TxJubiObservacion.Location = New System.Drawing.Point(6, 24)
        Me.TxJubiObservacion.Multiline = True
        Me.TxJubiObservacion.Name = "TxJubiObservacion"
        Me.TxJubiObservacion.ReadOnly = True
        Me.TxJubiObservacion.Size = New System.Drawing.Size(644, 134)
        Me.TxJubiObservacion.TabIndex = 191
        '
        'CBJubiAbonaCuota
        '
        Me.CBJubiAbonaCuota.BackColor = System.Drawing.Color.White
        Me.CBJubiAbonaCuota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBJubiAbonaCuota.Enabled = False
        Me.CBJubiAbonaCuota.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.CBJubiAbonaCuota.ForeColor = System.Drawing.Color.Black
        Me.CBJubiAbonaCuota.FormattingEnabled = True
        Me.CBJubiAbonaCuota.Items.AddRange(New Object() {"SI", "NO"})
        Me.CBJubiAbonaCuota.Location = New System.Drawing.Point(252, 150)
        Me.CBJubiAbonaCuota.Name = "CBJubiAbonaCuota"
        Me.CBJubiAbonaCuota.Size = New System.Drawing.Size(79, 26)
        Me.CBJubiAbonaCuota.TabIndex = 189
        '
        'LabelJubiAbonaCuota
        '
        Me.LabelJubiAbonaCuota.BackColor = System.Drawing.Color.White
        Me.LabelJubiAbonaCuota.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiAbonaCuota.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiAbonaCuota.Location = New System.Drawing.Point(6, 152)
        Me.LabelJubiAbonaCuota.Name = "LabelJubiAbonaCuota"
        Me.LabelJubiAbonaCuota.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiAbonaCuota.TabIndex = 188
        Me.LabelJubiAbonaCuota.Text = "Se abona en una sola cuota:"
        Me.LabelJubiAbonaCuota.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxJubiEdadResolucion
        '
        Me.TxJubiEdadResolucion.BackColor = System.Drawing.Color.White
        Me.TxJubiEdadResolucion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiEdadResolucion.ForeColor = System.Drawing.Color.Black
        Me.TxJubiEdadResolucion.Location = New System.Drawing.Point(252, 57)
        Me.TxJubiEdadResolucion.Name = "TxJubiEdadResolucion"
        Me.TxJubiEdadResolucion.ReadOnly = True
        Me.TxJubiEdadResolucion.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiEdadResolucion.TabIndex = 187
        '
        'LabelJubiEdadResolucion
        '
        Me.LabelJubiEdadResolucion.BackColor = System.Drawing.Color.White
        Me.LabelJubiEdadResolucion.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiEdadResolucion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiEdadResolucion.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiEdadResolucion.Location = New System.Drawing.Point(6, 61)
        Me.LabelJubiEdadResolucion.Name = "LabelJubiEdadResolucion"
        Me.LabelJubiEdadResolucion.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiEdadResolucion.TabIndex = 186
        Me.LabelJubiEdadResolucion.Text = "Edad al realizar la resolución:"
        '
        'TxJubiFondoCompensador
        '
        Me.TxJubiFondoCompensador.BackColor = System.Drawing.Color.White
        Me.TxJubiFondoCompensador.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiFondoCompensador.ForeColor = System.Drawing.Color.Black
        Me.TxJubiFondoCompensador.Location = New System.Drawing.Point(583, 118)
        Me.TxJubiFondoCompensador.Name = "TxJubiFondoCompensador"
        Me.TxJubiFondoCompensador.ReadOnly = True
        Me.TxJubiFondoCompensador.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiFondoCompensador.TabIndex = 185
        '
        'LabelJubiFondoCompensador
        '
        Me.LabelJubiFondoCompensador.BackColor = System.Drawing.Color.White
        Me.LabelJubiFondoCompensador.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiFondoCompensador.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiFondoCompensador.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiFondoCompensador.Location = New System.Drawing.Point(337, 122)
        Me.LabelJubiFondoCompensador.Name = "LabelJubiFondoCompensador"
        Me.LabelJubiFondoCompensador.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiFondoCompensador.TabIndex = 184
        Me.LabelJubiFondoCompensador.Text = "Fondo compensador:"
        '
        'TxJubiNroResolucion
        '
        Me.TxJubiNroResolucion.BackColor = System.Drawing.Color.White
        Me.TxJubiNroResolucion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiNroResolucion.ForeColor = System.Drawing.Color.Black
        Me.TxJubiNroResolucion.Location = New System.Drawing.Point(583, 27)
        Me.TxJubiNroResolucion.Name = "TxJubiNroResolucion"
        Me.TxJubiNroResolucion.ReadOnly = True
        Me.TxJubiNroResolucion.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiNroResolucion.TabIndex = 183
        '
        'LabelJubiNroResolucion
        '
        Me.LabelJubiNroResolucion.BackColor = System.Drawing.Color.White
        Me.LabelJubiNroResolucion.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiNroResolucion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiNroResolucion.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiNroResolucion.Location = New System.Drawing.Point(337, 31)
        Me.LabelJubiNroResolucion.Name = "LabelJubiNroResolucion"
        Me.LabelJubiNroResolucion.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiNroResolucion.TabIndex = 182
        Me.LabelJubiNroResolucion.Text = "Nro de resolución:"
        '
        'TxJubiCapitalizacion
        '
        Me.TxJubiCapitalizacion.BackColor = System.Drawing.Color.White
        Me.TxJubiCapitalizacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiCapitalizacion.ForeColor = System.Drawing.Color.Black
        Me.TxJubiCapitalizacion.Location = New System.Drawing.Point(252, 118)
        Me.TxJubiCapitalizacion.Name = "TxJubiCapitalizacion"
        Me.TxJubiCapitalizacion.ReadOnly = True
        Me.TxJubiCapitalizacion.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiCapitalizacion.TabIndex = 181
        '
        'TxJubiCoeficiente
        '
        Me.TxJubiCoeficiente.BackColor = System.Drawing.Color.White
        Me.TxJubiCoeficiente.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiCoeficiente.ForeColor = System.Drawing.Color.Black
        Me.TxJubiCoeficiente.Location = New System.Drawing.Point(583, 87)
        Me.TxJubiCoeficiente.Name = "TxJubiCoeficiente"
        Me.TxJubiCoeficiente.ReadOnly = True
        Me.TxJubiCoeficiente.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiCoeficiente.TabIndex = 180
        '
        'TxJubiPeriodoOpto
        '
        Me.TxJubiPeriodoOpto.BackColor = System.Drawing.Color.White
        Me.TxJubiPeriodoOpto.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxJubiPeriodoOpto.ForeColor = System.Drawing.Color.Black
        Me.TxJubiPeriodoOpto.Location = New System.Drawing.Point(252, 87)
        Me.TxJubiPeriodoOpto.Name = "TxJubiPeriodoOpto"
        Me.TxJubiPeriodoOpto.ReadOnly = True
        Me.TxJubiPeriodoOpto.Size = New System.Drawing.Size(79, 25)
        Me.TxJubiPeriodoOpto.TabIndex = 179
        '
        'LabelJubiCapitalizacion
        '
        Me.LabelJubiCapitalizacion.BackColor = System.Drawing.Color.White
        Me.LabelJubiCapitalizacion.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiCapitalizacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiCapitalizacion.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiCapitalizacion.Location = New System.Drawing.Point(6, 122)
        Me.LabelJubiCapitalizacion.Name = "LabelJubiCapitalizacion"
        Me.LabelJubiCapitalizacion.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiCapitalizacion.TabIndex = 176
        Me.LabelJubiCapitalizacion.Text = "Monto de capitalización al jubilarse:"
        '
        'LabelJubiCoeficiente
        '
        Me.LabelJubiCoeficiente.BackColor = System.Drawing.Color.White
        Me.LabelJubiCoeficiente.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiCoeficiente.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiCoeficiente.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiCoeficiente.Location = New System.Drawing.Point(337, 91)
        Me.LabelJubiCoeficiente.Name = "LabelJubiCoeficiente"
        Me.LabelJubiCoeficiente.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiCoeficiente.TabIndex = 174
        Me.LabelJubiCoeficiente.Text = "Coeficiente:"
        '
        'LabelJubiPeriodoOpto
        '
        Me.LabelJubiPeriodoOpto.BackColor = System.Drawing.Color.White
        Me.LabelJubiPeriodoOpto.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiPeriodoOpto.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiPeriodoOpto.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiPeriodoOpto.Location = New System.Drawing.Point(6, 91)
        Me.LabelJubiPeriodoOpto.Name = "LabelJubiPeriodoOpto"
        Me.LabelJubiPeriodoOpto.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiPeriodoOpto.TabIndex = 172
        Me.LabelJubiPeriodoOpto.Text = "Períodos por los que optó:"
        '
        'MtxJubilacion
        '
        Me.MtxJubilacion.BackColor = System.Drawing.Color.White
        Me.MtxJubilacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxJubilacion.ForeColor = System.Drawing.Color.Black
        Me.MtxJubilacion.Location = New System.Drawing.Point(252, 27)
        Me.MtxJubilacion.Mask = "00/00/0000"
        Me.MtxJubilacion.Name = "MtxJubilacion"
        Me.MtxJubilacion.ReadOnly = True
        Me.MtxJubilacion.Size = New System.Drawing.Size(79, 25)
        Me.MtxJubilacion.TabIndex = 123
        Me.MtxJubilacion.ValidatingType = GetType(Date)
        '
        'LabelJubiFecha
        '
        Me.LabelJubiFecha.BackColor = System.Drawing.Color.White
        Me.LabelJubiFecha.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LabelJubiFecha.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJubiFecha.ForeColor = System.Drawing.Color.Black
        Me.LabelJubiFecha.Location = New System.Drawing.Point(6, 31)
        Me.LabelJubiFecha.Name = "LabelJubiFecha"
        Me.LabelJubiFecha.Size = New System.Drawing.Size(240, 21)
        Me.LabelJubiFecha.TabIndex = 170
        Me.LabelJubiFecha.Text = "Fecha de jubilación/resolución:"
        '
        'UGBObraSocial
        '
        Me.UGBObraSocial.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBObraSocial.Controls.Add(Me.cbObraSocial)
        Me.UGBObraSocial.Controls.Add(Me.Lb_obrasocial)
        Me.UGBObraSocial.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGBObraSocial.ForeColor = System.Drawing.Color.Black
        Appearance13.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance13.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance13.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance13.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance13.FontData.BoldAsString = "True"
        Appearance13.ForeColor = System.Drawing.Color.White
        Me.UGBObraSocial.HeaderAppearance = Appearance13
        Me.UGBObraSocial.Location = New System.Drawing.Point(444, 394)
        Me.UGBObraSocial.Name = "UGBObraSocial"
        Me.UGBObraSocial.Size = New System.Drawing.Size(473, 150)
        Me.UGBObraSocial.TabIndex = 171
        Me.UGBObraSocial.Text = "OBRA SOCIAL"
        Me.UGBObraSocial.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'cbObraSocial
        '
        Me.cbObraSocial.BackColor = System.Drawing.Color.White
        Me.cbObraSocial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbObraSocial.Enabled = False
        Me.cbObraSocial.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.cbObraSocial.ForeColor = System.Drawing.Color.Black
        Me.cbObraSocial.FormattingEnabled = True
        Me.cbObraSocial.Location = New System.Drawing.Point(97, 26)
        Me.cbObraSocial.Name = "cbObraSocial"
        Me.cbObraSocial.Size = New System.Drawing.Size(154, 26)
        Me.cbObraSocial.TabIndex = 131
        '
        'Lb_obrasocial
        '
        Me.Lb_obrasocial.BackColor = System.Drawing.Color.White
        Me.Lb_obrasocial.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Lb_obrasocial.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_obrasocial.ForeColor = System.Drawing.Color.Black
        Me.Lb_obrasocial.Location = New System.Drawing.Point(6, 27)
        Me.Lb_obrasocial.Name = "Lb_obrasocial"
        Me.Lb_obrasocial.Size = New System.Drawing.Size(85, 21)
        Me.Lb_obrasocial.TabIndex = 170
        Me.Lb_obrasocial.Text = "Prestador:"
        '
        'TabControlFicha
        '
        Me.TabControlFicha.Controls.Add(Me.TPDatos)
        Me.TabControlFicha.Controls.Add(Me.TPFamiliaPrestamo)
        Me.TabControlFicha.Controls.Add(Me.TPJubilacion)
        Me.TabControlFicha.Controls.Add(Me.TPTarjetas)
        Me.TabControlFicha.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TabControlFicha.Dock = System.Windows.Forms.DockStyle.Top
        Me.TabControlFicha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlFicha.Location = New System.Drawing.Point(0, 0)
        Me.TabControlFicha.Name = "TabControlFicha"
        Me.TabControlFicha.SelectedIndex = 0
        Me.TabControlFicha.Size = New System.Drawing.Size(1083, 579)
        Me.TabControlFicha.TabIndex = 172
        '
        'TPDatos
        '
        Me.TPDatos.Controls.Add(Me.UGBET)
        Me.TPDatos.Controls.Add(Me.UGBDatos)
        Me.TPDatos.Controls.Add(Me.UGBObraSocial)
        Me.TPDatos.Controls.Add(Me.UGBTitulos)
        Me.TPDatos.Location = New System.Drawing.Point(4, 25)
        Me.TPDatos.Name = "TPDatos"
        Me.TPDatos.Padding = New System.Windows.Forms.Padding(3)
        Me.TPDatos.Size = New System.Drawing.Size(1075, 550)
        Me.TPDatos.TabIndex = 0
        Me.TPDatos.Text = "Datos"
        Me.TPDatos.UseVisualStyleBackColor = True
        '
        'UGBET
        '
        Me.UGBET.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBET.Controls.Add(Me.MtxFechaVencimientoET)
        Me.UGBET.Controls.Add(Me.LFechaVencimientoET)
        Me.UGBET.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UGBET.ForeColor = System.Drawing.Color.Black
        Appearance14.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance14.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance14.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance14.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance14.FontData.BoldAsString = "True"
        Appearance14.ForeColor = System.Drawing.Color.White
        Me.UGBET.HeaderAppearance = Appearance14
        Me.UGBET.Location = New System.Drawing.Point(443, 228)
        Me.UGBET.Name = "UGBET"
        Me.UGBET.Size = New System.Drawing.Size(473, 150)
        Me.UGBET.TabIndex = 172
        Me.UGBET.Text = "MATRICULA EN TRAMITE"
        Me.UGBET.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'MtxFechaVencimientoET
        '
        Me.MtxFechaVencimientoET.BackColor = System.Drawing.Color.White
        Me.MtxFechaVencimientoET.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.MtxFechaVencimientoET.ForeColor = System.Drawing.Color.Black
        Me.MtxFechaVencimientoET.Location = New System.Drawing.Point(161, 24)
        Me.MtxFechaVencimientoET.Mask = "00/00/0000"
        Me.MtxFechaVencimientoET.Name = "MtxFechaVencimientoET"
        Me.MtxFechaVencimientoET.ReadOnly = True
        Me.MtxFechaVencimientoET.Size = New System.Drawing.Size(79, 25)
        Me.MtxFechaVencimientoET.TabIndex = 131
        Me.MtxFechaVencimientoET.ValidatingType = GetType(Date)
        '
        'LFechaVencimientoET
        '
        Me.LFechaVencimientoET.BackColor = System.Drawing.Color.White
        Me.LFechaVencimientoET.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.LFechaVencimientoET.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LFechaVencimientoET.ForeColor = System.Drawing.Color.Black
        Me.LFechaVencimientoET.Location = New System.Drawing.Point(6, 27)
        Me.LFechaVencimientoET.Name = "LFechaVencimientoET"
        Me.LFechaVencimientoET.Size = New System.Drawing.Size(149, 21)
        Me.LFechaVencimientoET.TabIndex = 170
        Me.LFechaVencimientoET.Text = "Fecha de vencimiento:"
        '
        'TPFamiliaPrestamo
        '
        Me.TPFamiliaPrestamo.Controls.Add(Me.UGBFamilia)
        Me.TPFamiliaPrestamo.Controls.Add(Me.UGBPrestamo)
        Me.TPFamiliaPrestamo.Location = New System.Drawing.Point(4, 25)
        Me.TPFamiliaPrestamo.Name = "TPFamiliaPrestamo"
        Me.TPFamiliaPrestamo.Padding = New System.Windows.Forms.Padding(3)
        Me.TPFamiliaPrestamo.Size = New System.Drawing.Size(1075, 550)
        Me.TPFamiliaPrestamo.TabIndex = 1
        Me.TPFamiliaPrestamo.Text = "Familiares y Prestamo"
        Me.TPFamiliaPrestamo.UseVisualStyleBackColor = True
        '
        'UGBFamilia
        '
        Me.UGBFamilia.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UGBFamilia.Controls.Add(Me.UGFlia)
        Me.UGBFamilia.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.UGBFamilia.ForeColor = System.Drawing.Color.Black
        Appearance15.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance15.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance15.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance15.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance15.FontData.BoldAsString = "True"
        Appearance15.ForeColor = System.Drawing.Color.White
        Me.UGBFamilia.HeaderAppearance = Appearance15
        Me.UGBFamilia.Location = New System.Drawing.Point(6, 6)
        Me.UGBFamilia.Name = "UGBFamilia"
        Me.UGBFamilia.Size = New System.Drawing.Size(1058, 447)
        Me.UGBFamilia.TabIndex = 153
        Me.UGBFamilia.Text = "GRUPO FAMILIAR Y/O BENEFICIARIOS"
        Me.UGBFamilia.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'TPJubilacion
        '
        Me.TPJubilacion.Controls.Add(Me.UGBJubilacion)
        Me.TPJubilacion.Location = New System.Drawing.Point(4, 25)
        Me.TPJubilacion.Name = "TPJubilacion"
        Me.TPJubilacion.Padding = New System.Windows.Forms.Padding(3)
        Me.TPJubilacion.Size = New System.Drawing.Size(1075, 550)
        Me.TPJubilacion.TabIndex = 2
        Me.TPJubilacion.Text = "Jubilación"
        Me.TPJubilacion.UseVisualStyleBackColor = True
        '
        'TPTarjetas
        '
        Me.TPTarjetas.Controls.Add(Me.UltraGroupBox1)
        Me.TPTarjetas.Location = New System.Drawing.Point(4, 25)
        Me.TPTarjetas.Name = "TPTarjetas"
        Me.TPTarjetas.Padding = New System.Windows.Forms.Padding(3)
        Me.TPTarjetas.Size = New System.Drawing.Size(1075, 550)
        Me.TPTarjetas.TabIndex = 3
        Me.TPTarjetas.Text = "Tarjetas"
        Me.TPTarjetas.UseVisualStyleBackColor = True
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
        Me.UltraGroupBox1.Controls.Add(Me.TxDebNaranja)
        Me.UltraGroupBox1.Controls.Add(Me.Label16)
        Me.UltraGroupBox1.Controls.Add(Me.TxDebMastercard)
        Me.UltraGroupBox1.Controls.Add(Me.Label7)
        Me.UltraGroupBox1.Controls.Add(Me.TxDebVisa)
        Me.UltraGroupBox1.Controls.Add(Me.Label6)
        Me.UltraGroupBox1.Controls.Add(Me.TxDebTuya)
        Me.UltraGroupBox1.Controls.Add(Me.Label5)
        Me.UltraGroupBox1.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraGroupBox1.ForeColor = System.Drawing.Color.Black
        Appearance16.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance16.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance16.BackColorDisabled = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance16.BackColorDisabled2 = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Appearance16.FontData.BoldAsString = "True"
        Appearance16.ForeColor = System.Drawing.Color.White
        Me.UltraGroupBox1.HeaderAppearance = Appearance16
        Me.UltraGroupBox1.Location = New System.Drawing.Point(8, 6)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(473, 192)
        Me.UltraGroupBox1.TabIndex = 173
        Me.UltraGroupBox1.Text = "TARJETAS DE CRÉDITO"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'TxDebNaranja
        '
        Me.TxDebNaranja.BackColor = System.Drawing.Color.White
        Me.TxDebNaranja.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDebNaranja.ForeColor = System.Drawing.Color.Black
        Me.TxDebNaranja.Location = New System.Drawing.Point(161, 141)
        Me.TxDebNaranja.Name = "TxDebNaranja"
        Me.TxDebNaranja.ReadOnly = True
        Me.TxDebNaranja.Size = New System.Drawing.Size(219, 25)
        Me.TxDebNaranja.TabIndex = 177
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.White
        Me.Label16.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label16.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(6, 143)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(149, 21)
        Me.Label16.TabIndex = 176
        Me.Label16.Text = " Tarjeta Naranja"
        '
        'TxDebMastercard
        '
        Me.TxDebMastercard.BackColor = System.Drawing.Color.White
        Me.TxDebMastercard.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDebMastercard.ForeColor = System.Drawing.Color.Black
        Me.TxDebMastercard.Location = New System.Drawing.Point(161, 102)
        Me.TxDebMastercard.Name = "TxDebMastercard"
        Me.TxDebMastercard.ReadOnly = True
        Me.TxDebMastercard.Size = New System.Drawing.Size(219, 25)
        Me.TxDebMastercard.TabIndex = 175
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label7.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(6, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(149, 21)
        Me.Label7.TabIndex = 174
        Me.Label7.Text = " Tarjeta Mastercard"
        '
        'TxDebVisa
        '
        Me.TxDebVisa.BackColor = System.Drawing.Color.White
        Me.TxDebVisa.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDebVisa.ForeColor = System.Drawing.Color.Black
        Me.TxDebVisa.Location = New System.Drawing.Point(161, 65)
        Me.TxDebVisa.Name = "TxDebVisa"
        Me.TxDebVisa.ReadOnly = True
        Me.TxDebVisa.Size = New System.Drawing.Size(219, 25)
        Me.TxDebVisa.TabIndex = 173
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(6, 67)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(149, 21)
        Me.Label6.TabIndex = 172
        Me.Label6.Text = " Tarjeta Visa"
        '
        'TxDebTuya
        '
        Me.TxDebTuya.BackColor = System.Drawing.Color.White
        Me.TxDebTuya.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Bold)
        Me.TxDebTuya.ForeColor = System.Drawing.Color.Black
        Me.TxDebTuya.Location = New System.Drawing.Point(161, 28)
        Me.TxDebTuya.Name = "TxDebTuya"
        Me.TxDebTuya.ReadOnly = True
        Me.TxDebTuya.Size = New System.Drawing.Size(219, 25)
        Me.TxDebTuya.TabIndex = 171
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label5.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(149, 21)
        Me.Label5.TabIndex = 170
        Me.Label5.Text = " Tarjeta Tuya"
        '
        'FrmFichaProfesionales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1083, 627)
        Me.Controls.Add(Me.TabControlFicha)
        Me.Controls.Add(Me.CButton3)
        Me.Controls.Add(Me.cbtOrdenPago)
        Me.Controls.Add(Me.CBtPago)
        Me.Controls.Add(Me.CBtCtaCte)
        Me.Controls.Add(Me.CButtonActualizar)
        Me.Controls.Add(Me.CButtonEditar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmFichaProfesionales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ficha del Profesional"
        CType(Me.UGBTitulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBTitulos.ResumeLayout(False)
        Me.UGBTitulos.PerformLayout()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        CType(Me.UGBDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBDatos.ResumeLayout(False)
        Me.UGBDatos.PerformLayout()
        CType(Me.UGFlia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGBPrestamo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBPrestamo.ResumeLayout(False)
        Me.UGBPrestamo.PerformLayout()
        CType(Me.UTEGarantedeNumdoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEGaranteNumdoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGBJubilacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBJubilacion.ResumeLayout(False)
        Me.UGBJubilacion.PerformLayout()
        Me.GBJubiObservacion.ResumeLayout(False)
        Me.GBJubiObservacion.PerformLayout()
        CType(Me.UGBObraSocial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBObraSocial.ResumeLayout(False)
        Me.TabControlFicha.ResumeLayout(False)
        Me.TPDatos.ResumeLayout(False)
        CType(Me.UGBET, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBET.ResumeLayout(False)
        Me.UGBET.PerformLayout()
        Me.TPFamiliaPrestamo.ResumeLayout(False)
        CType(Me.UGBFamilia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBFamilia.ResumeLayout(False)
        Me.TPJubilacion.ResumeLayout(False)
        Me.TPTarjetas.ResumeLayout(False)
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UGBTitulos As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButtonAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButtonSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents cbCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents txMatricula As System.Windows.Forms.TextBox
    Friend WithEvents cbTitulo As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UGBDatos As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents CboTipdoc As System.Windows.Forms.ComboBox
    Friend WithEvents TxDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbDelegacion As System.Windows.Forms.ComboBox
    Friend WithEvents cbProvincia As System.Windows.Forms.ComboBox
    Friend WithEvents cbCivil As System.Windows.Forms.ComboBox
    Friend WithEvents cbSexo As System.Windows.Forms.ComboBox
    Friend WithEvents MtxFecNac As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxDGR As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxCuit As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxCelu As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxTelefono As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TxMail As System.Windows.Forms.TextBox
    Friend WithEvents TxGaranteDe As System.Windows.Forms.TextBox
    Friend WithEvents TxLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents TxDireccion As System.Windows.Forms.TextBox
    Friend WithEvents TxNombre As System.Windows.Forms.TextBox
    Friend WithEvents TxApellido As System.Windows.Forms.TextBox
    Friend WithEvents lbMail As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lbCivil As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lbSexo As System.Windows.Forms.Label
    Friend WithEvents lbFecNac As System.Windows.Forms.Label
    Friend WithEvents lbNombre As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxCodPos As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents CButtonEditar As System.Windows.Forms.Button
    Friend WithEvents CButtonActualizar As System.Windows.Forms.Button
    Friend WithEvents CBtCtaCte As System.Windows.Forms.Button
    Friend WithEvents CBtPago As System.Windows.Forms.Button
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cbGanancia As System.Windows.Forms.ComboBox
    Friend WithEvents lbGanancia As System.Windows.Forms.Label
    Friend WithEvents MtxExcepDGR As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxExcepDGI As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UGFlia As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents cbtOrdenPago As System.Windows.Forms.Button
    Friend WithEvents CButton3 As System.Windows.Forms.Button
    Friend WithEvents lbCbuDebito As System.Windows.Forms.Label
    Friend WithEvents UGBPrestamo As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TBGarante As System.Windows.Forms.TextBox
    Friend WithEvents LabelGarante As System.Windows.Forms.Label
    Friend WithEvents TBGarantede As System.Windows.Forms.TextBox
    Friend WithEvents LabelGaranteDe As System.Windows.Forms.Label
    Friend WithEvents CBGaranteTipdoc As System.Windows.Forms.ComboBox
    Friend WithEvents UTEGaranteNumdoc As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UTEGarantedeNumdoc As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents CBGarantedeTipdoc As System.Windows.Forms.ComboBox
    Friend WithEvents TBGarantedeNrodoc As System.Windows.Forms.TextBox
    Friend WithEvents TBGaranteNrodoc As System.Windows.Forms.TextBox
    Friend WithEvents TBGarantedeTipdoc As System.Windows.Forms.TextBox
    Friend WithEvents TBGaranteTipdoc As System.Windows.Forms.TextBox
    Friend WithEvents Mtxcbu As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UDTgraduacion As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UDTficha As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UDTmatricula As System.Windows.Forms.MaskedTextBox
    Friend WithEvents UDTdiploma As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TxMailAlternativo As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents MtxcbuCredito As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lbCbuCredito As System.Windows.Forms.Label
    Friend WithEvents MtxExcepIVA As MaskedTextBox
    Friend WithEvents Label32 As Label
    Friend WithEvents LGenerarRenta As Label
    Friend WithEvents CBGenerarRenta As ComboBox
    Friend WithEvents UGBJubilacion As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents MtxJubilacion As MaskedTextBox
    Friend WithEvents LabelJubiFecha As Label
    Friend WithEvents UGBObraSocial As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents cbObraSocial As ComboBox
    Friend WithEvents Lb_obrasocial As Label
    Friend WithEvents TabControlFicha As TabControl
    Friend WithEvents TPDatos As TabPage
    Friend WithEvents TPFamiliaPrestamo As TabPage
    Friend WithEvents TPJubilacion As TabPage
    Friend WithEvents UGBFamilia As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents LabelJubiCapitalizacion As Label
    Friend WithEvents LabelJubiCoeficiente As Label
    Friend WithEvents LabelJubiPeriodoOpto As Label
    Friend WithEvents TxJubiPeriodoOpto As TextBox
    Friend WithEvents TxJubiCapitalizacion As TextBox
    Friend WithEvents TxJubiCoeficiente As TextBox
    Friend WithEvents UGBET As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents LFechaVencimientoET As Label
    Friend WithEvents MtxFechaVencimientoET As MaskedTextBox
    Friend WithEvents TxJubiNroResolucion As TextBox
    Friend WithEvents LabelJubiNroResolucion As Label
    Friend WithEvents TxJubiFondoCompensador As TextBox
    Friend WithEvents LabelJubiFondoCompensador As Label
    Friend WithEvents TxJubiEdadResolucion As TextBox
    Friend WithEvents LabelJubiEdadResolucion As Label
    Friend WithEvents CBJubiAbonaCuota As ComboBox
    Friend WithEvents LabelJubiAbonaCuota As Label
    Friend WithEvents TxJubiObservacion As TextBox
    Friend WithEvents GBJubiObservacion As GroupBox
    Friend WithEvents TxJubiPeriodosRestantes As TextBox
    Friend WithEvents TxJubiPeriodosCobrados As TextBox
    Friend WithEvents LabelJubiPeriodosRestantes As Label
    Friend WithEvents LabelJubiPeriodosCobrados As Label
    Friend WithEvents TxJubiImporteCuota As TextBox
    Friend WithEvents LabelJubiImporteCuota As Label
    Friend WithEvents TPTarjetas As TabPage
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TxDebTuya As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents TxDebNaranja As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents TxDebMastercard As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TxDebVisa As TextBox
    Friend WithEvents Label6 As Label
End Class
