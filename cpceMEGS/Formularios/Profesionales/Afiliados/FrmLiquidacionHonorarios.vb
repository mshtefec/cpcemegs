﻿Imports MySql.Data.MySqlClient

Public Class FrmLiquidacionHonorarios
    Private cnn As New ConsultaBD(True)
    Private DAglobalMysql As MySqlDataAdapter
    Private c_rowProf As DataRow
    Private DTComitente As DataTable
    'Private DAComitente As MySqlDataAdapter
    Private BSComitente As BindingSource
    Private DSTrabajo As DataSet
    'Private DATrabajo As MySqlDataAdapter
    Private BSTrabajo As BindingSource
    Private DSHonorarios As DataSet
    Private DAHonorarios As MySqlDataAdapter 'Usa Command
    Private BSHonorarios As BindingSource
    Private cmdHonorarios As MySqlCommandBuilder
    Private DSRetenciones As New DataSet
    Private DARetenciones As New MySqlDataAdapter 'Usa Command
    Private BSRetenciones As New BindingSource
    Private cmdRetenciones As MySqlCommandBuilder
    Private nNroComitente As Integer
    Private DTLegali As DataTable
    'Private DALegali As MySqlDataAdapter
    Private cmdLegali As MySqlCommandBuilder
    'Trabajos.
    'globalTrabajoId En GrabaObleas() EditarObleas() MuestraDatosObleas() lo seteo en 0 y BuscaTrabajoIngresado()
    Private globalTrabajoId As Integer = 0
    Private globalTrabajoEstado As Integer = 7
    Private globalTrabajoNroAsi As Long = 0
    Private globalTrabajoMontos As Long() = {0, 0, 0, 0, 0, 0} '0 = Activo 1 = Pasivo 2 = montoDeposito 3 = ImportePeriodo 4 = Meses 5 = PorcentajeSindico

    Public Sub New(ByVal rowProf As DataRow)
        'Tener en cuenta que accedo desde la Ficha del profesional y desde el Listado de Trabajos.
        InitializeComponent()
        c_rowProf = rowProf
    End Sub
    Private Sub FrmLiquidacionHonorarios_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CargoLegalizaciones()
        CBResolucion.SelectedIndex = 0
    End Sub

    Private Sub CargoComitente()
        nNroComitente = 0
        DTComitente = New DataTable
        DAglobalMysql = cnn.consultaBDadapter(
            "comitente",
            "afi_nombre,afi_nrodoc,afi_tipoiva",
            "afi_tipdoc='" & DTLegali.Rows(UcboLegalizacion.SelectedRow.Index).Item("obl_tipcte") & "' and afi_nrodoc=" & DTLegali.Rows(UcboLegalizacion.SelectedRow.Index).Item("obl_nrocte")
        )
        DAglobalMysql.Fill(DTComitente)
        BSComitente = New BindingSource
        BSComitente.DataSource = DTComitente
        ULcomitente.Text = DTComitente.Rows(BSComitente.Position).Item("afi_nombre")

        'COMENTE ESTOOOOOOOOOOOOOOOOOOOOO 29/04/2016
        'If DTComitente.Rows(BSComitente.Position).Item("afi_tipoiva") = "EX" Then
        '    UCEbonifica.Text = "SI"
        'Else
        '    UCEbonifica.Text = "NO"
        'End If
        'FIN COMENTE ESTOOOOOOOOOOOOOOOOOOOOO 29/04/2016

        '   If UltraTextEditor2.Value <> "" And UltraTextEditor3.Value > 0 Then
        ' MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
        ' End If
    End Sub

    Private Sub Limpialiquidacion()
        DSRetenciones.Dispose()
        BSRetenciones.Dispose()
        DARetenciones.Dispose()
        Label3.Visible = False
        txHonorarios.Visible = False
        TxBonifica.Visible = False
        Label8.Visible = False
        Label10.Visible = False
        TxTotRet.Visible = False
        TxReiPend.Visible = False
        TxReiPend.Text = ""
        nNroComitente = 0
    End Sub
    Private Function getTableCBResolucion(ByVal indexCBResolucion As Integer) As String
        If indexCBResolucion = 0 Then
            getTableCBResolucion = "thcalcac"
        ElseIf indexCBResolucion = 1 Then
            getTableCBResolucion = "thcalcac2017"
        ElseIf indexCBResolucion = 2 Then
            getTableCBResolucion = "thcalcac2016"
        ElseIf indexCBResolucion = 3 Then
            getTableCBResolucion = "thcalcac2015"
        Else
            getTableCBResolucion = "thcalcac2014"
        End If

        Return getTableCBResolucion
    End Function
    Private Sub CargoTrabajos()
        DSTrabajo = New DataSet

        DAglobalMysql = cnn.consultaBDadapter(
            "tareas",
            "tar_codigo,tar_descrip,tar_ctaing,tar_tareasaldo",
            "tar_codigo=" & DTLegali.Rows(UcboLegalizacion.SelectedRow.Index).Item("obl_tarea")
        )
        DAglobalMysql.Fill(DSTrabajo, "trabajo")
        ' si esta en estado pendiente ya tiene liquidaciones realizadas
        If DTLegali.Rows(UcboLegalizacion.SelectedRow.Index).Item("Est") = "P" Then
            Dim nTareaSaldo As Integer = DSTrabajo.Tables(0).Rows(0).Item("tar_tareasaldo")

            DAglobalMysql = cnn.consultaBDadapter(
                "tareas",
                "tar_codigo,tar_descrip,tar_ctaing,tar_tareasaldo",
                "tar_codigo=" & nTareaSaldo
            )
            DSTrabajo = New DataSet
            DAglobalMysql.Fill(DSTrabajo, "trabajo")
        End If
        'Entra si encontro la tarea saldo en caso de no encontrar
        If DSTrabajo.Tables(0).Rows.Count > 0 Then
            BSTrabajo = New BindingSource
            BSTrabajo.DataSource = DSTrabajo.Tables(0)
            ULtarea.Text = DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_descrip")
            Dim tablaCalculosSegunResolucion As String = getTableCBResolucion(CBResolucion.SelectedIndex)
            Dim FrmCalcHonor As New FrmCalculoHonorarios(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"), tablaCalculosSegunResolucion, globalTrabajoMontos)
            If FrmCalcHonor.ShowDialog() = DialogResult.OK Then
                ' bonificacion del 50% entidades sin fines de lucro
                txHonorarios.Text = FormatCurrency(FrmCalcHonor.UltraCurrencyEditor1.Value, , , TriState.True, TriState.True)

                If UCEbonifica.Text = "SI" Then
                    TxBonifica.Text = FormatCurrency(FrmCalcHonor.UltraCurrencyEditor1.Value / 2, , , TriState.True, TriState.True)
                Else
                    TxBonifica.Text = FormatCurrency(0, , , TriState.True, TriState.True)
                End If
                txHonorarios.Visible = True
                TxBonifica.Visible = True
                Label3.Visible = True
                If globalTrabajoId <> 0 Then
                    UTEReciboButtonRightClick()
                End If
                If UTERecibo.Value > 0 Then
                    MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
                End If
            Else
                LimpiaTrabajos()
                Limpialiquidacion()
                UcboLegalizacion.Focus()
            End If
        Else
            MessageBox.Show("EL TIPO DE TRABAJO NO PERMITE LIQUIDAR SALDO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            LimpiaTrabajos()
            Limpialiquidacion()
            UcboLegalizacion.Focus()
        End If
    End Sub

    Private Sub MuestraHonorariosReintegro()
        If UGRecibos.Visible And Not BSHonorarios Is Nothing Then
            UGRecibos.Visible = False
            Dim nTotHonorarios As Double = 0
            For Each rowHon As DataRow In DSHonorarios.Tables(0).Rows
                If rowHon.Item("Select") Then
                    If rowHon.Item("fecdif").ToString <> "00/00/0000" AndAlso CDate(rowHon.Item("fecdif").ToString) > Date.Today Then
                        If MessageBox.Show("La fecha diferida del cheque es del " & rowHon.Item("fecdif").ToString & ", desea aplicar al pago?", "Liquidación de honorarios", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            nTotHonorarios += rowHon.Item("importe")
                        Else
                            ' saco el tilde
                            rowHon.Item("Select") = False
                        End If
                    Else
                        nTotHonorarios += rowHon.Item("importe")
                    End If
                End If
            Next
            If nTotHonorarios > 0 Then
                UTERecibo.Text = FormatCurrency(nTotHonorarios, 2, , TriState.True, TriState.True)

                MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
            Else
                LimpiaTrabajos()
            End If
        Else
            UGComitentes.Visible = False
            UGTrabajos.Visible = False
            DSHonorarios = New DataSet
            'DAHonorarios = cnn.consultaBDadapter("totales inner join comproba on com_nroasi=tot_nroasi", "tot_fecha as Fecha,tot_fecdif as FecDif,tot_proceso as Proceso,tot_nrocom as Recibo,tot_debe as Importe,tot_imppag,tot_nroasi,tot_item,tot_nrocuo", "tot_proceso='02R120' and tot_debe>0 and tot_imppag=0 and com_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "' and com_nrodoc=" & c_rowProf.Item("afi_nrodoc") & " order by tot_fecha")
            Dim queryNroAsiento As String = ""
            If globalTrabajoNroAsi <> 0 Then
                queryNroAsiento = " AND tot_nroasi = " & globalTrabajoNroAsi
            End If
            Dim condiciones As String =
                "com_proceso = '02R120' AND com_tipdoc='" & c_rowProf.Item("afi_tipdoc") &
                "' AND com_nrodoc=" & c_rowProf.Item("afi_nrodoc") &
                " AND tot_debe > 0 AND tot_imppag = 0 AND com_estado <> 9 AND tot_nrolegali = " &
                UcboLegalizacion.Value & queryNroAsiento

            DAHonorarios = cnn.consultaBDadapter(
                "comproba INNER JOIN totales ON com_nroasi=tot_nroasi AND com_unegos=tot_unegos AND com_nrocli=tot_nrocli",
                "tot_fecha as Fecha,tot_fecdif as FecDif,tot_proceso as Proceso,tot_nrocom as Recibo,tot_debe as Importe,tot_imppag,tot_nroasi,tot_item,tot_nrocuo",
                condiciones
            )

            DAHonorarios.Fill(DSHonorarios, "honorario")
            DSHonorarios.Tables(0).Columns.Add("Select", GetType(Boolean))
            cmdHonorarios = New MySqlCommandBuilder(DAHonorarios)
            BSHonorarios = New BindingSource
            BSHonorarios.DataSource = DSHonorarios.Tables(0)
            If BSHonorarios.Count = 0 Then
                MessageBox.Show("No se encontro recibos de honorarios a reintegrar")
            Else
                If DSHonorarios.Tables("honorario").Rows.Count() = 1 Then
                    DSHonorarios.Tables("honorario").Rows(0).Item("Select") = True
                Else
                    For Each rowHon As DataRow In DSHonorarios.Tables("honorario").Rows
                        rowHon.Item("Select") = False
                    Next
                End If

                With UGRecibos
                    .DataSource = BSHonorarios
                    .Top = 173
                    .Left = 120
                    .Width = 470
                    .Height = 300
                    .Visible = True
                    '.DisplayLayout.Bands(0).Columns(0).Header.Caption = "Comitente"
                    .DisplayLayout.Bands(0).Columns(0).Width = 80
                    .DisplayLayout.Bands(0).Columns(1).Width = 80
                    '.DisplayLayout.Bands(0).Columns(1).Header.Caption = "Numero"
                    .DisplayLayout.Bands(0).Columns(2).Width = 60
                    '.DisplayLayout.Bands(0).Columns(2).Hidden = True
                    .DisplayLayout.Bands(0).Columns(4).Format = "c"
                    .DisplayLayout.Bands(0).Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                    .DisplayLayout.Bands(0).Columns(5).Hidden = True
                    .DisplayLayout.Bands(0).Columns(6).Hidden = True
                    .DisplayLayout.Bands(0).Columns(7).Hidden = True
                    .DisplayLayout.Bands(0).Columns(8).Hidden = True
                End With

                If DSHonorarios.Tables("honorario").Rows.Count() = 1 Then
                    UTEReciboButtonRightClick()
                End If
            End If
        End If
    End Sub

    Private Sub LimpiaTrabajos()
        Label8.Visible = False
        Label10.Visible = False
        TxTotRet.Visible = False
        TxReiPend.Visible = False
        TxReiPend.Text = ""
        DSRetenciones.Dispose()
        BSRetenciones.Dispose()
        DARetenciones.Dispose()
        UTERecibo.Text = ""
        UGRecibos.Visible = False
        TxtDebe.Text = ""
        TxtHaber.Text = ""
    End Sub

    Private Sub MuestraRetenciones(ByVal nTarea As Integer)
        Dim nMontoDeposito As Double = Math.Round(Convert.ToDouble(Trim(UTERecibo.Text.Replace("$", ""))), 2)
        Dim nMontoHonorario As Double = Math.Round(Convert.ToDouble(Trim(txHonorarios.Text.Replace("$", ""))), 2)
        Dim nMontoBonifica As Double = Math.Round(Convert.ToDouble(Trim(TxBonifica.Text.Replace("$", ""))), 2)
        Dim nMontoAporte As Double = Math.Round(Convert.ToDouble(Trim(UCEAporte.Text.Replace("$", ""))), 2)

        Dim Retencion As New Calculos

        Label8.Visible = True
        Label10.Visible = True
        TxTotRet.Text = ""
        TxTotRet.Visible = True
        TxReiPend.Text = ""
        TxReiPend.Visible = True

        DSRetenciones = New DataSet
		DARetenciones = cnn.consultaBDadapter(
			"tareashcon inner join hconcept on hco_concepto=thc_concepto",
			"thc_tarea,thc_concepto,hco_descripcion,hco_porcentaje as porcentaje,thc_importe as sobre,thc_importe as importe,thc_porcentaje,thc_retimporte,thc_retsinlucro,thc_retconlucro,hco_tarea,hco_montoacum,thc_montomayor",
			"thc_tarea=" & nTarea & " order by hco_orden"
		)
		DARetenciones.Fill(DSRetenciones, "retenciones")
        Dim dc As DataColumn
        dc = New DataColumn("tipdoc", GetType(String))
        dc.MaxLength = 3
        dc.DefaultValue = c_rowProf.Item("afi_tipdoc")
        DSRetenciones.Tables(0).Columns.Add(dc)
        dc = New DataColumn("nrodoc", GetType(Integer))
        dc.DefaultValue = c_rowProf.Item("afi_nrodoc")
        DSRetenciones.Tables(0).Columns.Add(dc)
        ' la primer retencion (141) le cargo la cuenta para el asiento
        DSRetenciones.Tables(0).Rows(0).Item("hco_tarea") = DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_ctaing")
        cmdRetenciones = New MySqlCommandBuilder(DARetenciones)
        BSRetenciones = New BindingSource
        BSRetenciones.DataSource = DSRetenciones.Tables(0)
        UGRetenciones.DataSource = BSRetenciones
        With UGRetenciones.DisplayLayout.Bands(0)
            .Columns(0).Hidden = True
            .Columns(1).Hidden = True
            .Columns(2).Header.Caption = "Retención"
            .Columns(2).Width = 340
            .Columns(3).Format = "N"
            .Columns(3).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(4).Format = "c"
            .Columns(4).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(5).Format = "c"
            .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(6).Hidden = True
            .Columns(7).Hidden = True
            .Columns(8).Hidden = True
            .Columns(9).Hidden = True
            .Columns(10).Hidden = True
            .Columns(11).Hidden = True
            .Columns(12).Hidden = True
            .Columns(13).Hidden = True
            .Columns(14).Hidden = True
        End With

        Try
            'Al nMontoHonorario - UCEIva.Value - nMontoBonifica le resto iva y si bonifica tambien.
            TxTotRet.Text = FormatCurrency(Retencion.CalculoRetencion(
                DSRetenciones, c_rowProf,
                nMontoHonorario - UCEIva.Value - nMontoBonifica,
                nMontoHonorario - nMontoBonifica,
                nMontoHonorario - nMontoAporte - UCEIva.Value - nMontoBonifica,
                UTERecibo.Value - UCEIva.Value,
                IIf(UCEbonifica.Text = "NO", False, True),
                IIf(UCEganancias.Text = "NO", False, True),
                UDTdgi.Value, UDTdgr.Value, UDTiva.Value), , ,
                TriState.True, TriState.True
            )
            If Retencion.GetAporteCoincide() = False Then
                MessageBox.Show("Controlar los montos de aportes no coinciden!")
            End If
            TxReiPend.Text = FormatCurrency(nMontoDeposito - Convert.ToDouble(TxTotRet.Text.Replace("$", "")), 2, , TriState.False, TriState.True)

            Dim nReinPend As Double = Math.Round(Convert.ToDouble(TxReiPend.Text.Replace("$", "")), 2)
            Dim nTotDeber As Double = Math.Round(Convert.ToDouble(UTERecibo.Text.Replace("$", "")), 2)
            Dim nTotHaber As Double = 0

            For Each rowRet As DataRow In DSRetenciones.Tables(0).Rows
                If rowRet.Item("importe") > 0 Then
                    nTotHaber += Math.Round(rowRet.Item("importe"), 2)
                End If
            Next

            nTotHaber += nReinPend

            TxtDebe.Text = Format(nTotDeber, "$ ###,##0.00")
            TxtHaber.Text = Format(nTotHaber, "$ ###,##0.00")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GrabaLiquidacion()
        If cnn.AbrirConexion Then
            Dim miTrans As MySqlTransaction
            cnn.Bloqueo(True)
            miTrans = cnn.InicioTransaccion
            Try
                Dim nHonorario As Double = Math.Round(Convert.ToDouble(txHonorarios.Text.Replace("$", "")), 2)
                Dim nDeposito As Double = Math.Round(Convert.ToDouble(UTERecibo.Text.Replace("$", "")), 2)
                Dim nReinPend As Double = Math.Round(Convert.ToDouble(TxReiPend.Text.Replace("$", "")), 2)
                Dim FechaHora As String
                Dim nItem As Integer = 0
                FechaHora = Format(Now.Date, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)

				Dim nNroAsiento As Long = cnn.TomaCobteAsiento("ASIENT")
				Dim nNrocomprobante As Long = cnn.TomaCobte(nPubNroIns, nPubNroCli, "02LIHN")
				Dim nPubNroInsSipres As Integer = 1
				Dim nNroAsiento01L As Long = cnn.TomaCobteAsiento("ASIENT") 'continuo para el 01L155
				Dim nNrocomprobante01L As Long = cnn.TomaCobte(nPubNroInsSipres, nPubNroCli, "01L155") 'continuo para el 01L155 nroIns = 1 SIPRES
				Dim cTitDestinatario As String = ""
                Dim nMatDestinatario As Integer = 0
                Dim bGanancias As Boolean = IIf(UCEganancias.Text = "NO", False, True)
                Dim totBonifica As String = "0"
                Dim totBonificaDouble As Double = 0
                If UCEganancias.Text = "NO" Then
                    totBonificaDouble = nDeposito - UCEIva.Value
                    totBonifica = totBonificaDouble.ToString.Replace(",", ".")
                End If

                nItem += 1
				' HONORARIOS A REINTEGRAR
				cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," & "tot_proceso='02LIHN'," &
					"tot_nrocli='" & nPubNroCli & "'," &
					"tot_nrocom='" & nNrocomprobante & "'," &
					"tot_item='" & nItem & "'," &
					"tot_nropla='21010100'," &
					"tot_subpla='" & c_rowProf.Item("afi_tipdoc") & Format(c_rowProf.Item("afi_nrodoc"), "00000000") & "'," &
					"tot_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
					"tot_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
					"tot_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
					"tot_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
					"tot_nroope='" & nPubNroOperador & "'," &
					"tot_nrocuo='" & 1 & "'," &
					"tot_fecha='" & FechaHora & "'," &
					"tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
					"tot_fecalt='" & FechaHora & "'," &
					"tot_debe='" & nDeposito.ToString.Replace(",", ".") & "'," &
					"tot_iva='" & UCEIva.Value.ToString.Replace(",", ".") & "'," &
					"tot_bonifica='" & totBonifica & "'," &
					"tot_nroasi='" & nNroAsiento & "'")
				For Each RowRet As DataRow In DSRetenciones.Tables(0).Rows
					If RowRet.Item("importe") > 0 Then
						nItem += 1
						'Redondeo
						Dim sobre As Double = Math.Round(RowRet.Item("sobre"), 2)
						Dim importe As Double = Math.Round(RowRet.Item("importe"), 2)
						totBonifica = "0"
						If UCEganancias.Text = "NO" And RowRet.Item("thc_concepto") = 141 Then
							totBonifica = importe.ToString.Replace(",", ".")
						End If
						'fin redondeo
						cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," &
							"tot_proceso='02LIHN'," &
							"tot_nrocli='" & nPubNroCli & "'," &
							"tot_nrocom='" & nNrocomprobante & "'," &
							"tot_item='" & nItem & "'," &
							"tot_nropla='" & RowRet.Item("hco_tarea") & "'," &
							"tot_subpla='" & c_rowProf.Item("afi_tipdoc") & Format(c_rowProf.Item("afi_nrodoc"), "00000000") & "'," &
							"tot_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
							"tot_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
							"tot_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
							"tot_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
							"tot_nroope='" & nPubNroOperador & "'," &
							"tot_nrocuo='" & 1 & "'," &
							"tot_fecha='" & FechaHora & "'," &
							"tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
							"tot_fecalt='" & FechaHora & "'," &
							"tot_haber='" & importe.ToString.Replace(",", ".") & "'," &
							"tot_codretencion='" & RowRet.Item("thc_concepto") & "'," &
							"tot_porcentaje='" & RowRet.Item("porcentaje").ToString.Replace(",", ".") & "'," &
							"tot_sobre='" & sobre.ToString.Replace(",", ".") & "'," &
							"tot_bonifica='" & totBonifica & "'," &
							"tot_nroasi='" & nNroAsiento & "'")
					End If
				Next

				' reintegro pendientes
				nItem += 1
                cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroIns & "'," &
                              "tot_proceso='02LIHN'," &
                              "tot_nrocli='" & nPubNroCli & "'," &
                              "tot_nrocom='" & nNrocomprobante & "'," &
                              "tot_item='" & nItem & "'," &
                              "tot_nropla='21010300'," &
                              "tot_subpla='" & c_rowProf.Item("afi_tipdoc") & Format(c_rowProf.Item("afi_nrodoc"), "00000000") & "'," &
                              "tot_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
                              "tot_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
                              "tot_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
                              "tot_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
                              "tot_nroope='" & nPubNroOperador & "'," &
                              "tot_nrocuo='" & 1 & "'," &
                              "tot_fecha='" & FechaHora & "'," &
                              "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                              "tot_fecalt='" & FechaHora & "'," &
                              "tot_haber='" & nReinPend.ToString.Replace(",", ".") & "'," &
                              "tot_nroasi='" & nNroAsiento & "'")

				cnn.ReplaceBD("insert into comproba set com_unegos='" & nPubNroIns & "'," &
							  "com_nrodeleg='" & nPubNroCli & "'," &
							  "com_proceso='02LIHN'," &
							  "com_nrocli='" & nPubNroCli & "'," &
							  "com_nrocom='" & nNrocomprobante & "'," &
							  "com_nroasi='" & nNroAsiento & "'," &
							  "com_fecha='" & FechaHora & "'," &
							  "com_fecalt='" & FechaHora & "'," &
							  "com_total='" & nReinPend.ToString.Replace(",", ".") & "'," &
							  "com_imppag='" & nHonorario.ToString.Replace(",", ".") & "'," &
							  "com_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
							  "com_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
							  "com_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
							  "com_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
							  "com_tipcmt='COM'," &
							  "com_nrocmt='" & DTComitente.Rows(BSComitente.Position).Item("afi_nrodoc") & "'," &
							  "com_nroope='" & nPubNroOperador & "'," &
							  "com_tarea='" & DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo") & "'," &
							  "com_nrotrabajo='" & UcboLegalizacion.Text & "'")
				Dim cEstadoOblea As String = "L"
                If DSHonorarios Is Nothing Then
                Else
                    For Each rowHon As DataRow In DSHonorarios.Tables(0).Rows
                        If rowHon.Item("Select") Then
                            rowHon.Item("tot_imppag") = rowHon.Item("importe")
                        Else
                            cEstadoOblea = "P"
                        End If
                    Next
                    DAHonorarios.Update(DSHonorarios.Tables(0))
                End If
				' marco como liquidado la obleas
				cnn.ReplaceBD("UPDATE obleas SET obl_estado='" & cEstadoOblea & "',obl_asiento='" & nNroAsiento & "' WHERE obl_nrolegali=" & UcboLegalizacion.Text)

                'Dim importeTotal As Double = nDeposito * 0.01 / 100
                If DSRetenciones.Tables(0).Rows.Item(1).Item(1) = 147 Then
                    Dim importeTotal As Double = DSRetenciones.Tables(0).Rows.Item(1).Item(5) 'ES IGUAL A nDeposito * 0.01 / 100, TOMO EL IMPORTE YA CALCULADO EN EL PASO ANTERIOR
                    Dim importeFormula As Double = importeTotal
                    Dim columnImp As String
                    Dim porcentajes As MySqlDataAdapter = cnn.buscaProcetotePorcentaje("01L155")
                    Dim DSPorcentajes As DataSet = New DataSet()
                    porcentajes.Fill(DSPorcentajes, "porcentajes")

                    If (c_rowProf.Item("afi_titulo") <> "EC") Then
                        nItem = 1
                        'PROCESO 01L155 (EL IMPORTE ES SOBRE EL 1% del Total)

                        'variables para colocar la diferencia de redondeo si es que la hay
                        Dim nTotHaber As Double = 0
                        Dim nTotDeber As Double = 0
                        Dim nMonto As Double = 0

                        For Each RowRet As DataRow In DSPorcentajes.Tables(0).Rows

                            'Redondeo
                            If (RowRet.Item("pto_formula") <> "") Then
                                Dim formula As String = RowRet.Item("pto_formula").Substring(7)
                                formula = formula.Replace(".", ",")
                                importeFormula = Math.Round(importeTotal * formula, 2)
                            End If

                            If (RowRet.Item("pto_tipmov") = "D") Then
                                columnImp = "debe"
                                nTotDeber = importeTotal 'resguardo debe para calcular si hay diferencia
                            Else
                                columnImp = "haber"
                                nTotHaber += importeFormula 'resguardo sumatoria haber para calcular si hay diferencia
                            End If

                            If (RowRet.Item("pto_nropla") = "31021300") Then
                                nMonto = importeFormula 'resguardo importe haber de la cuenta gastos para distribuir si hay diferencia
                            End If

                            'fin redondeo
                            cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroInsSipres & "'," &
                                "tot_proceso='01L155'," &
                                "tot_nrocli='" & nPubNroCli & "'," &
                                "tot_nrocom='" & nNrocomprobante01L & "'," &
                                "tot_item='" & RowRet.Item("pto_item") & "'," &
                                "tot_nropla='" & RowRet.Item("pto_nropla") & "'," &
                                "tot_subpla='" & c_rowProf.Item("afi_tipdoc") & Format(c_rowProf.Item("afi_nrodoc"), "00000000") & "'," &
                                "tot_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
                                "tot_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
                                "tot_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
                                "tot_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
                                "tot_nroope='" & nPubNroOperador & "'," &
                                "tot_nrocuo='" & 1 & "'," &
                                "tot_fecha='" & FechaHora & "'," &
                                "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                "tot_fecalt='" & FechaHora & "'," &
                                "tot_" & columnImp & "='" & importeFormula.ToString.Replace(",", ".") & "'," &
                                "tot_codretencion='" & 0 & "'," &
                                "tot_porcentaje='" & 0 & "'," &
                                "tot_sobre='" & nDeposito.ToString.Replace(",", ".") & "'," &
                                "tot_bonifica='" & totBonifica & "'," &
                                "tot_nroasi='" & nNroAsiento01L & "'")
                        Next

                        'chequeo si hay diferencia y actualizo
                        Dim nDifeRedondeo As Double = Math.Round(nTotDeber - nTotHaber, 2)
                        If Math.Abs(nDifeRedondeo) < 0.09 Then
                            Dim diferencia As Double = nMonto + nDifeRedondeo
                            cnn.ReplaceBD("update totales set tot_haber='" & diferencia.ToString.Replace(",", ".") & "'" &
                                "where tot_proceso='01L155'" &
                                "and tot_nroasi='" & nNroAsiento01L & "'" &
                                "and tot_nropla='31021300'")
                        End If

                        cnn.ReplaceBD("insert into comproba set com_unegos='" & nPubNroInsSipres & "'," &
                                  "com_nrodeleg='" & nPubNroCli & "'," &
                                  "com_proceso='01L155'," &
                                  "com_nrocli='" & nPubNroCli & "'," &
                                  "com_nrocom='" & nNrocomprobante01L & "'," &
                                  "com_nroasi='" & nNroAsiento01L & "'," &
                                  "com_fecha='" & FechaHora & "'," &
                                  "com_fecalt='" & FechaHora & "'," &
                                  "com_total='" & importeTotal & "'," &
                                  "com_imppag='" & nHonorario.ToString.Replace(",", ".") & "'," &
                                  "com_titulo='" & c_rowProf.Item("afi_titulo") & "'," &
                                  "com_matricula='" & c_rowProf.Item("afi_matricula") & "'," &
                                  "com_tipdoc='" & c_rowProf.Item("afi_tipdoc") & "'," &
                                  "com_nrodoc='" & c_rowProf.Item("afi_nrodoc") & "'," &
                                  "com_tipcmt='COM'," &
                                  "com_nrocmt='" & DTComitente.Rows(BSComitente.Position).Item("afi_nrodoc") & "'," &
                                  "com_nroope='" & nPubNroOperador & "'," &
                                  "com_tarea='" & DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo") & "'," &
                                  "com_nrotrabajo='" & UcboLegalizacion.Text & "'")

                        'FIN PROCESO 01L155
                    Else
                        'ESTUDIO CONTABLE
                        Dim estudiosContablesDataAdapter As MySqlDataAdapter = cnn.GetEstudioContable(c_rowProf.Item("afi_titulo"), c_rowProf.Item("afi_matricula"))
                        Dim DSEstudioContable As DataSet = New DataSet()
                        estudiosContablesDataAdapter.Fill(DSEstudioContable, "estudioContable")

                        If (DSEstudioContable.Tables(0).Rows.Count = 0) Then
                            'SI EL ESTUDIO NO TIENE PROFESIONALES O NO SE ENCUENTRA
                            'BUSCO LA OBLEA POR NRO DE LEGALIZACION PARA OBTENER DATOS DEL QUE FIRMA
                            Dim firmanteDataAdapter As MySqlDataAdapter = cnn.GetFirmante(UcboLegalizacion.Text)
                            Dim DSFirmante As DataSet = New DataSet()
                            firmanteDataAdapter.Fill(DSFirmante, "firmante")

                            nItem = 1
                            'PROCESO 01L155 (EL IMPORTE ES SOBRE EL 1% del Total)
                            For Each RowRet As DataRow In DSPorcentajes.Tables(0).Rows
                                If (RowRet.Item("pto_tipmov") = "D") Then
                                    columnImp = "debe"
                                Else
                                    columnImp = "haber"
                                End If
                                'Redondeo
                                If (RowRet.Item("pto_formula") <> "") Then
                                    Dim formula As String = RowRet.Item("pto_formula").Substring(7)
                                    formula = formula.Replace(".", ",")
                                    importeFormula = Math.Round(importeTotal * formula, 2)
                                End If
                                'fin redondeo
                                cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroInsSipres & "'," &
                                "tot_proceso='01L155'," &
                                "tot_nrocli='" & nPubNroCli & "'," &
                                "tot_nrocom='" & nNrocomprobante01L & "'," &
                                "tot_item='" & RowRet.Item("pto_item") & "'," &
                                "tot_nropla='" & RowRet.Item("pto_nropla") & "'," &
                                "tot_subpla='" & DSFirmante.Tables(0).Rows(0)("afi_tipdoc") & Format(DSFirmante.Tables(0).Rows(0)("afi_nrodoc"), "00000000") & "'," &
                                "tot_tipdoc='" & DSFirmante.Tables(0).Rows(0)("afi_tipdoc") & "'," &
                                "tot_nrodoc='" & DSFirmante.Tables(0).Rows(0)("afi_nrodoc") & "'," &
                                "tot_titulo='" & DSFirmante.Tables(0).Rows(0)("afi_titulo") & "'," &
                                "tot_matricula='" & DSFirmante.Tables(0).Rows(0)("afi_matricula") & "'," &
                                "tot_nroope='" & nPubNroOperador & "'," &
                                "tot_nrocuo='" & 1 & "'," &
                                "tot_fecha='" & FechaHora & "'," &
                                "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                "tot_fecalt='" & FechaHora & "'," &
                                "tot_" & columnImp & "='" & importeFormula.ToString.Replace(",", ".") & "'," &
                                "tot_codretencion='" & 0 & "'," &
                                "tot_porcentaje='" & 0 & "'," &
                                "tot_sobre='" & nDeposito.ToString.Replace(",", ".") & "'," &
                                "tot_bonifica='" & totBonifica & "'," &
                                "tot_nroasi='" & nNroAsiento01L & "'")
                            Next

                            cnn.ReplaceBD("insert into comproba set com_unegos='" & nPubNroInsSipres & "'," &
                                  "com_nrodeleg='" & nPubNroCli & "'," &
                                  "com_proceso='01L155'," &
                                  "com_nrocli='" & nPubNroCli & "'," &
                                  "com_nrocom='" & nNrocomprobante01L & "'," &
                                  "com_nroasi='" & nNroAsiento01L & "'," &
                                  "com_fecha='" & FechaHora & "'," &
                                  "com_fecalt='" & FechaHora & "'," &
                                  "com_total='" & importeTotal & "'," &
                                  "com_imppag='" & nHonorario.ToString.Replace(",", ".") & "'," &
                                  "com_titulo='" & DSFirmante.Tables(0).Rows(0)("afi_titulo") & "'," &
                                  "com_matricula='" & DSFirmante.Tables(0).Rows(0)("afi_matricula") & "'," &
                                  "com_tipdoc='" & DSFirmante.Tables(0).Rows(0)("afi_tipdoc") & "'," &
                                  "com_nrodoc='" & DSFirmante.Tables(0).Rows(0)("afi_nrodoc") & "'," &
                                  "com_tipcmt='COM'," &
                                  "com_nrocmt='" & DTComitente.Rows(BSComitente.Position).Item("afi_nrodoc") & "'," &
                                  "com_nroope='" & nPubNroOperador & "'," &
                                  "com_tarea='" & DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo") & "'," &
                                  "com_nrotrabajo='" & UcboLegalizacion.Text & "'")

                            'FIN PROCESO 01L155 PARA ESTUDIOS CONTABLES
                        Else
                            'SI EL ESTUDIO TIENE PROFESIONALES A CARGO
                            Dim calcularNro As Boolean = False

                            For Each RowProf As DataRow In DSEstudioContable.Tables(0).Rows
                                Dim importeCorrespondiente As Double = importeTotal * RowProf.Item("ec_porcentaje")
                                If (calcularNro) Then
                                    nNroAsiento01L = cnn.TomaCobteAsiento("ASIENT") 'continuo para el 01L155
                                    nNrocomprobante01L = cnn.TomaCobte(nPubNroInsSipres, nPubNroCli, "01L155") 'continuo para el 01L155 nroIns = 1 SIPRES
                                End If
                                nItem = 1
                                'PROCESO 01L155 (EL IMPORTE ES SOBRE EL 1% del Total)
                                For Each RowRet As DataRow In DSPorcentajes.Tables(0).Rows
                                    If (RowRet.Item("pto_tipmov") = "D") Then
                                        columnImp = "debe"
                                    Else
                                        columnImp = "haber"
                                    End If
                                    'Redondeo
                                    importeFormula = importeCorrespondiente

                                    If (RowRet.Item("pto_formula") <> "") Then
                                        Dim formula As String = RowRet.Item("pto_formula").Substring(7)
                                        formula = formula.Replace(".", ",")
                                        importeFormula = Math.Round(importeCorrespondiente * formula, 2)
                                    End If
                                    'fin redondeo
                                    cnn.ReplaceBD("insert into totales set tot_unegos='" & nPubNroInsSipres & "'," &
                                        "tot_proceso='01L155'," &
                                        "tot_nrocli='" & nPubNroCli & "'," &
                                        "tot_nrocom='" & nNrocomprobante01L & "'," &
                                        "tot_item='" & RowRet.Item("pto_item") & "'," &
                                        "tot_nropla='" & RowRet.Item("pto_nropla") & "'," &
                                        "tot_subpla='" & RowProf.Item("ec_prof_tipdoc") & Format(RowProf.Item("ec_prof_nrodoc"), "00000000") & "'," &
                                        "tot_tipdoc='" & RowProf.Item("ec_prof_tipdoc") & "'," &
                                        "tot_nrodoc='" & RowProf.Item("ec_prof_nrodoc") & "'," &
                                        "tot_titulo='" & RowProf.Item("ec_prof_titulo") & "'," &
                                        "tot_matricula='" & RowProf.Item("ec_prof_matricula") & "'," &
                                        "tot_nroope='" & nPubNroOperador & "'," &
                                        "tot_nrocuo='" & 1 & "'," &
                                        "tot_fecha='" & FechaHora & "'," &
                                        "tot_fecven='" & Format(Now.Date, "yyyy-MM-dd") & "'," &
                                        "tot_fecalt='" & FechaHora & "'," &
                                        "tot_" & columnImp & "='" & importeFormula.ToString.Replace(",", ".") & "'," &
                                        "tot_codretencion='" & 0 & "'," &
                                        "tot_porcentaje='" & 0 & "'," &
                                        "tot_sobre='" & nDeposito.ToString.Replace(",", ".") & "'," &
                                        "tot_bonifica='" & totBonifica & "'," &
                                        "tot_nroasi='" & nNroAsiento01L & "'")
                                Next

                                cnn.ReplaceBD("insert into comproba set com_unegos='" & nPubNroInsSipres & "'," &
                                      "com_nrodeleg='" & nPubNroCli & "'," &
                                      "com_proceso='01L155'," &
                                      "com_nrocli='" & nPubNroCli & "'," &
                                      "com_nrocom='" & nNrocomprobante01L & "'," &
                                      "com_nroasi='" & nNroAsiento01L & "'," &
                                      "com_fecha='" & FechaHora & "'," &
                                      "com_fecalt='" & FechaHora & "'," &
                                      "com_total='" & importeCorrespondiente & "'," &
                                      "com_imppag='" & nHonorario.ToString.Replace(",", ".") & "'," &
                                      "com_titulo='" & RowProf.Item("ec_prof_titulo") & "'," &
                                      "com_matricula='" & RowProf.Item("ec_prof_matricula") & "'," &
                                      "com_tipdoc='" & RowProf.Item("ec_prof_tipdoc") & "'," &
                                      "com_nrodoc='" & RowProf.Item("ec_prof_nrodoc") & "'," &
                                      "com_tipcmt='COM'," &
                                      "com_nrocmt='" & DTComitente.Rows(BSComitente.Position).Item("afi_nrodoc") & "'," &
                                      "com_nroope='" & nPubNroOperador & "'," &
                                      "com_tarea='" & DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo") & "'," &
                                      "com_nrotrabajo='" & UcboLegalizacion.Text & "'")

                                calcularNro = True
                                'FIN PROCESO 01L155 PARA ESTUDIOS CONTABLES
                            Next
                        End If
                    End If
                End If

                'Si tiene el id del trabajo seteo los valores para actualizar el estado
                If globalTrabajoId > 0 Then
                    cnn.trabajoId = globalTrabajoId
                    cnn.trabajoEstado = globalTrabajoEstado
                    cnn.trabajoNroAsientoLiquidacion = nNroAsiento
                    cnn.CambiarEstadoTrabajo()
                End If
                miTrans.Commit()

                cnn.Bloqueo(False)
				Dim imprimir As New Impresiones
				imprimir.ImprimirAsiento(nNroAsiento, "02LIHN")
			Catch ex As Exception
                miTrans.Rollback()
                cnn.Bloqueo(False)
                MessageBox.Show(ex.Message)
            End Try

            cnn.CerrarConexion()
        End If
    End Sub
    Private Sub CBConfirmar_Click(sender As Object, e As EventArgs) Handles CBConfirmar.Click
        'Controlo permiso Liquidacion Honorario Create
        If controlAcceso(1, 7, "C", True) = True Then
            If Trim(TxReiPend.Text) <> "" AndAlso FormatNumber(TxReiPend.Text) > 0 AndAlso TxtDebe.Text = TxtHaber.Text Then
                CBConfirmar.Enabled = False
                GrabaLiquidacion()
            Else
                MessageBox.Show("EL MONTO DEl REINTEGRO ES MENOR A CERO", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub
    Private Sub UTERecibo_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTERecibo.EditorButtonClick
        If e.Button.Key = "Right" Then
            UTEReciboButtonRightClick()
        Else
            LimpiaTrabajos()
        End If
    End Sub
    Private Sub UTEReciboButtonRightClick()
        If Not UcboLegalizacion.Value Is Nothing Then
            If UTERecibo.Value > 0 Then
                MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
            Else
                MuestraHonorariosReintegro()
            End If
        End If
    End Sub
    'DGI
    Private Sub UDTdgi_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UDTdgi.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UDTdgi_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UDTdgi.ValueChanged
        If UTERecibo.Value > 0 Then
            MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
        End If
    End Sub
    'DGR
    Private Sub UDTdgr_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UDTdgr.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UDTdgr_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UDTdgr.ValueChanged
        If UTERecibo.Value > 0 Then
            MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
        End If
    End Sub
    'IVA
    Private Sub UDTiva_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UDTiva.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UDTiva_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UDTiva.ValueChanged
        If UTERecibo.Value > 0 Then
            MuestraRetenciones(DSTrabajo.Tables(0).Rows(BSTrabajo.Position).Item("tar_codigo"))
        End If
    End Sub

    Private Sub CargoLegalizaciones()
        DAglobalMysql = cnn.consultaBDadapter(
            "obleas INNER JOIN comitente on afi_tipdoc=obl_tipcte and afi_nrodoc=obl_nrocte",
            "obl_nrolegali AS NroLegalizacion,obl_nrooblea AS NroOblea,obl_fecha AS fecha,obl_estado AS Est,obl_tippro,obl_nropro,obl_tipcte,obl_nrocte,obl_tarea,afi_nombre AS Comitente",
            "obl_tippro='" & c_rowProf.Item("afi_tipdoc") & "' AND obl_nropro=" & c_rowProf.Item("afi_nrodoc") & " AND obl_estado IN ('','I','P') GROUP BY obl_nrolegali"
        )
        DTLegali = New DataTable
        DAglobalMysql.Fill(DTLegali)
        UcboLegalizacion.DataSource = DTLegali
        With UcboLegalizacion.DisplayLayout.Bands(0)
            .Columns(0).Width = 80
            .Columns(0).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(1).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
            .Columns(2).Width = 130
            .Columns(3).Width = 30
            .Columns(4).Hidden = True
            .Columns(5).Hidden = True
            .Columns(6).Hidden = True
            .Columns(7).Hidden = True
            .Columns(8).Hidden = True
            .Columns(9).Width = 200
        End With
    End Sub

    Private Sub UcboLegalizacion_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UcboLegalizacion.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub UcboLegalizacion_RowSelected(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.RowSelectedEventArgs) Handles UcboLegalizacion.RowSelected
        UcboLegalizacionRowSelected()
    End Sub
    Private Sub UcboLegalizacionRowSelected()
        ULcomitente.Focus() ' PARA DESACTIVAR EL UCBOLEGALIZACION
        LimpiaTrabajos()
        Limpialiquidacion()
        CargoComitente()
        CargoTrabajos()
    End Sub
    Private Sub UCEganancias_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEganancias.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub UCEbonifica_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEbonifica.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub UCEIva_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEIva.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UCEbonifica_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCEbonifica.ValueChanged
        If UcboLegalizacion.Value IsNot Nothing Then
            CargoTrabajos()
        End If

    End Sub
    Private Sub UCEganancias_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCEganancias.ValueChanged
        If UcboLegalizacion.Value IsNot Nothing Then
            CargoTrabajos()
        End If
    End Sub
    'TRABAJOS
    Private Sub UTECodigoBarra_EditorButtonClick(sender As Object, e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTECodigoBarra.EditorButtonClick
        If e.Button.Key = "Right" Then
            BuscaTrabajoIngresado(UTECodigoBarra.Text)
        End If
    End Sub
    Private Sub UTECodigoBarra_KeyDown(sender As Object, e As KeyEventArgs) Handles UTECodigoBarra.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscaTrabajoIngresado(UTECodigoBarra.Text)
        End If
    End Sub
    Private Sub BuscaTrabajoIngresado(ByVal valorCodigoBarra As Integer)
        SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
        DAglobalMysql = cnn.QueryTrabajoById(valorCodigoBarra)
        Dim DSTrabajo As DataSet = New DataSet()
        DAglobalMysql.Fill(DSTrabajo, "trabajo")

        'CbtAltaOlbeas_Click(CbtAltaOlbeas, Nothing)

        If DSTrabajo.Tables("trabajo").Rows.Count() > 0 Then
            Dim rowTrabajo As DataRow = DSTrabajo.Tables("trabajo").Rows(0)
            'Control si el estado del trabajo es PRESENTADO entra
            If rowTrabajo.Item("tra_estado") = 5 Then
                'Controlo si tiene campo meses
                Dim meses As Integer = 0
                If Not IsDBNull(rowTrabajo.Item("tra_meses")) Then
                    meses = rowTrabajo.Item("tra_meses")
                End If
                Dim montoDeposito As Double = 0
                If Not IsDBNull(rowTrabajo.Item("tra_monto_deposito")) Then
                    montoDeposito = rowTrabajo.Item("tra_monto_deposito")
                End If
                Dim importePeriodo As Double = 0
                If Not IsDBNull(rowTrabajo.Item("tra_importe_periodo")) Then
                    importePeriodo = rowTrabajo.Item("tra_importe_periodo")
                End If
                Dim porcentajeSindico As Double = 0
                If Not IsDBNull(rowTrabajo.Item("tra_porcentaje_sindico")) Then
                    porcentajeSindico = rowTrabajo.Item("tra_porcentaje_sindico")
                End If
                Dim aporte As Double = 0
                If Not IsDBNull(rowTrabajo.Item("tra_monto_aporte")) Then
                    aporte = rowTrabajo.Item("tra_monto_aporte")
                End If
                Dim iva As Double = 0
                If Not IsDBNull(rowTrabajo.Item("tra_monto_iva")) Then
                    iva = rowTrabajo.Item("tra_monto_iva")
                End If
                'Completo campos
                'Guardo el ID del trabajo para utilizarlo luego de guardar el asiento y actualizar el estado del mismo
                SetTrabajoDefaultValues(
                    {rowTrabajo.Item("tra_importe1"), rowTrabajo.Item("tra_importe2"), montoDeposito, importePeriodo, meses, porcentajeSindico},
                    rowTrabajo.Item("id"), rowTrabajo.Item("tra_nroasi")
                )
                If (Not IsDBNull(rowTrabajo.Item("tra_auditoria_tipo")) AndAlso rowTrabajo.Item("tra_auditoria_tipo") = "SFL") Or
                    (Not IsDBNull(rowTrabajo.Item("tra_esauditor")) AndAlso rowTrabajo.Item("tra_esauditor") = "SI") Then
                    'UCEbonifica.SelectedText() = "SI"
                    UCEbonifica.SelectedIndex = 0
                End If
                UCEIva.Text = rowTrabajo.Item("tra_monto_iva")
                UCEAporte.Text = rowTrabajo.Item("tra_monto_aporte")
                'Completo campo UcboLegalizacion y automaticamente ejecuta el metodo para calcular
                UcboLegalizacion.Text = rowTrabajo.Item("tra_nrolegali")
            ElseIf rowTrabajo.Item("tra_estado") = 7 Then
                SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA LIQUIDADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf rowTrabajo.Item("tra_estado") = 8 Then
                SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA EL REINTEGRO LISTO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf rowTrabajo.Item("tra_estado") = 15 Then
                SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA COBRADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf rowTrabajo.Item("tra_estado") = 20 Then
                SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA ANULADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", NO SE ENCUENTRA PRESENTADO O PAGADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        Else
            SetTrabajoDefaultValues({0, 0, 0, 0, 0, 0})
            MessageBox.Show("NO EXISTE EL TRABAJO NUMERO: " & valorCodigoBarra, "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub
    Private Sub SetTrabajoDefaultValues(ByVal values As Long(), Optional ByVal trabajoId As Integer = 0, Optional ByVal trabajoNroAsi As Long = 0)
        globalTrabajoId = trabajoId
        globalTrabajoNroAsi = trabajoNroAsi
        globalTrabajoMontos = values '0 = Activo 1 = Pasivo 2 = montoDeposito 3 = ImportePeriodo 4 = Meses 5 = PorcentajeSindico
    End Sub
End Class