﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCalculoHonorarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCalculoHonorarios))
        Me.UCEactivo = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.LabelLinea1 = New System.Windows.Forms.Label()
        Me.LabelLinea2 = New System.Windows.Forms.Label()
        Me.UCEpasivo = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.LabelLinea3 = New System.Windows.Forms.Label()
        Me.UCEingreso = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.UltraCurrencyEditor1 = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.LabelLinea4 = New System.Windows.Forms.Label()
        Me.UCEimportePeriodo = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.LabelLinea5 = New System.Windows.Forms.Label()
        Me.UCEporcentajeSindico = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        Me.LabelLinea6 = New System.Windows.Forms.Label()
        Me.UCEmeses = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
        CType(Me.UCEactivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEpasivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEingreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraCurrencyEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEimportePeriodo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEporcentajeSindico, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEmeses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UCEactivo
        '
        Me.UCEactivo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEactivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEactivo.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEactivo.FormatString = ""
        Me.UCEactivo.Location = New System.Drawing.Point(244, 12)
        Me.UCEactivo.MaskInput = "{currency:12.2}"
        Me.UCEactivo.Name = "UCEactivo"
        Me.UCEactivo.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEactivo.Size = New System.Drawing.Size(206, 28)
        Me.UCEactivo.TabIndex = 0
        '
        'LabelLinea1
        '
        Me.LabelLinea1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea1.CausesValidation = False
        Me.LabelLinea1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea1.Location = New System.Drawing.Point(28, 12)
        Me.LabelLinea1.Name = "LabelLinea1"
        Me.LabelLinea1.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea1.TabIndex = 114
        Me.LabelLinea1.Text = "Monto del Activo:"
        Me.LabelLinea1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelLinea2
        '
        Me.LabelLinea2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea2.CausesValidation = False
        Me.LabelLinea2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea2.Location = New System.Drawing.Point(28, 43)
        Me.LabelLinea2.Name = "LabelLinea2"
        Me.LabelLinea2.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea2.TabIndex = 115
        Me.LabelLinea2.Text = "Monto del Pasivo:"
        Me.LabelLinea2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEpasivo
        '
        Me.UCEpasivo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEpasivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEpasivo.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEpasivo.Location = New System.Drawing.Point(244, 43)
        Me.UCEpasivo.MaskInput = "{currency:12.2}"
        Me.UCEpasivo.Name = "UCEpasivo"
        Me.UCEpasivo.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEpasivo.Size = New System.Drawing.Size(206, 28)
        Me.UCEpasivo.TabIndex = 1
        '
        'LabelLinea3
        '
        Me.LabelLinea3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea3.CausesValidation = False
        Me.LabelLinea3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea3.Location = New System.Drawing.Point(28, 74)
        Me.LabelLinea3.Name = "LabelLinea3"
        Me.LabelLinea3.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea3.TabIndex = 117
        Me.LabelLinea3.Text = "Monto de Ingresos:"
        Me.LabelLinea3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEingreso
        '
        Me.UCEingreso.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEingreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEingreso.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEingreso.Location = New System.Drawing.Point(244, 74)
        Me.UCEingreso.MaskInput = "{currency:12.2}"
        Me.UCEingreso.Name = "UCEingreso"
        Me.UCEingreso.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEingreso.Size = New System.Drawing.Size(206, 28)
        Me.UCEingreso.TabIndex = 2
        '
        'UltraCurrencyEditor1
        '
        Appearance1.ForeColor = System.Drawing.Color.Red
        Me.UltraCurrencyEditor1.Appearance = Appearance1
        Appearance2.ImageHAlign = Infragistics.Win.HAlign.Left
        EditorButton1.Appearance = Appearance2
        EditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton1.Key = "Left"
        Appearance3.FontData.BoldAsString = "True"
        EditorButton1.PressedAppearance = Appearance3
        EditorButton1.Text = "Calcular"
        EditorButton1.Width = 90
        Me.UltraCurrencyEditor1.ButtonsLeft.Add(EditorButton1)
        Appearance4.ImageHAlign = Infragistics.Win.HAlign.Left
        EditorButton2.Appearance = Appearance4
        EditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton2.Key = "Right"
        EditorButton2.Text = "Aceptar"
        EditorButton2.Width = 90
        Me.UltraCurrencyEditor1.ButtonsRight.Add(EditorButton2)
        Me.UltraCurrencyEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon
        Me.UltraCurrencyEditor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraCurrencyEditor1.Location = New System.Drawing.Point(28, 218)
        Me.UltraCurrencyEditor1.Name = "UltraCurrencyEditor1"
        Me.UltraCurrencyEditor1.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraCurrencyEditor1.Size = New System.Drawing.Size(422, 28)
        Me.UltraCurrencyEditor1.TabIndex = 4
        '
        'LabelLinea4
        '
        Me.LabelLinea4.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea4.CausesValidation = False
        Me.LabelLinea4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea4.Location = New System.Drawing.Point(28, 105)
        Me.LabelLinea4.Name = "LabelLinea4"
        Me.LabelLinea4.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea4.TabIndex = 119
        Me.LabelLinea4.Text = "Cantidad de Meses:"
        Me.LabelLinea4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEimportePeriodo
        '
        Me.UCEimportePeriodo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEimportePeriodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEimportePeriodo.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEimportePeriodo.Location = New System.Drawing.Point(244, 136)
        Me.UCEimportePeriodo.MaskInput = "{currency:12.2}"
        Me.UCEimportePeriodo.Name = "UCEimportePeriodo"
        Me.UCEimportePeriodo.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEimportePeriodo.Size = New System.Drawing.Size(206, 28)
        Me.UCEimportePeriodo.TabIndex = 120
        '
        'LabelLinea5
        '
        Me.LabelLinea5.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea5.CausesValidation = False
        Me.LabelLinea5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea5.Location = New System.Drawing.Point(28, 136)
        Me.LabelLinea5.Name = "LabelLinea5"
        Me.LabelLinea5.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea5.TabIndex = 121
        Me.LabelLinea5.Text = "Importe Periodo:"
        Me.LabelLinea5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEporcentajeSindico
        '
        Me.UCEporcentajeSindico.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEporcentajeSindico.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEporcentajeSindico.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEporcentajeSindico.FormatString = ""
        Me.UCEporcentajeSindico.Location = New System.Drawing.Point(244, 167)
        Me.UCEporcentajeSindico.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals
        Me.UCEporcentajeSindico.MaskInput = "{currency:5.2}"
        Me.UCEporcentajeSindico.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.UCEporcentajeSindico.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.UCEporcentajeSindico.Name = "UCEporcentajeSindico"
        Me.UCEporcentajeSindico.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEporcentajeSindico.Size = New System.Drawing.Size(206, 28)
        Me.UCEporcentajeSindico.TabIndex = 122
        '
        'LabelLinea6
        '
        Me.LabelLinea6.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelLinea6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelLinea6.CausesValidation = False
        Me.LabelLinea6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLinea6.Location = New System.Drawing.Point(28, 167)
        Me.LabelLinea6.Name = "LabelLinea6"
        Me.LabelLinea6.Size = New System.Drawing.Size(202, 28)
        Me.LabelLinea6.TabIndex = 123
        Me.LabelLinea6.Text = "Porcentaje Sindico:"
        Me.LabelLinea6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UCEmeses
        '
        Me.UCEmeses.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2000
        Me.UCEmeses.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UCEmeses.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UCEmeses.FormatString = ""
        Me.UCEmeses.Location = New System.Drawing.Point(244, 105)
        Me.UCEmeses.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.IncludeLiterals
        Me.UCEmeses.MaskInput = "{LOC}nn.nn"
        Me.UCEmeses.MaxValue = New Decimal(New Integer() {9999, 0, 0, 131072})
        Me.UCEmeses.MinValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.UCEmeses.Name = "UCEmeses"
        Me.UCEmeses.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UCEmeses.Size = New System.Drawing.Size(206, 28)
        Me.UCEmeses.TabIndex = 3
        '
        'FrmCalculoHonorarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(488, 261)
        Me.Controls.Add(Me.UCEporcentajeSindico)
        Me.Controls.Add(Me.LabelLinea6)
        Me.Controls.Add(Me.UCEimportePeriodo)
        Me.Controls.Add(Me.LabelLinea5)
        Me.Controls.Add(Me.UCEmeses)
        Me.Controls.Add(Me.LabelLinea4)
        Me.Controls.Add(Me.UltraCurrencyEditor1)
        Me.Controls.Add(Me.UCEingreso)
        Me.Controls.Add(Me.LabelLinea3)
        Me.Controls.Add(Me.UCEpasivo)
        Me.Controls.Add(Me.LabelLinea2)
        Me.Controls.Add(Me.LabelLinea1)
        Me.Controls.Add(Me.UCEactivo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCalculoHonorarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calculo Honorarios en Materia Administrativa y Comercial"
        CType(Me.UCEactivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEpasivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEingreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraCurrencyEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEimportePeriodo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEporcentajeSindico, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEmeses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UCEactivo As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents LabelLinea1 As System.Windows.Forms.Label
    Friend WithEvents LabelLinea2 As System.Windows.Forms.Label
    Friend WithEvents UCEpasivo As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents LabelLinea3 As System.Windows.Forms.Label
    Friend WithEvents UCEingreso As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents UltraCurrencyEditor1 As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents LabelLinea4 As System.Windows.Forms.Label
    Friend WithEvents UCEimportePeriodo As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents LabelLinea5 As Label
    Friend WithEvents UCEporcentajeSindico As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents LabelLinea6 As Label
    Friend WithEvents UCEmeses As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
End Class
