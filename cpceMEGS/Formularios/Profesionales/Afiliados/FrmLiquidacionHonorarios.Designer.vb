﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLiquidacionHonorarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim ValueListItem3 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
		Dim ValueListItem4 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
		Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
		Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
		Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
		Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLiquidacionHonorarios))
		Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
		Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance33 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance34 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance40 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance41 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
		Dim Appearance37 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance38 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Dim Appearance42 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
		Me.UGComitentes = New Infragistics.Win.UltraWinGrid.UltraGrid()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.txHonorarios = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.UCEganancias = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.UDTdgi = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.UDTdgr = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.UCEbonifica = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.UGRetenciones = New Infragistics.Win.UltraWinGrid.UltraGrid()
		Me.CBConfirmar = New System.Windows.Forms.Button()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.TxTotRet = New System.Windows.Forms.TextBox()
		Me.TxReiPend = New System.Windows.Forms.TextBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.UTERecibo = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
		Me.TxBonifica = New System.Windows.Forms.TextBox()
		Me.UGTrabajos = New Infragistics.Win.UltraWinGrid.UltraGrid()
		Me.UGRecibos = New Infragistics.Win.UltraWinGrid.UltraGrid()
		Me.UcboLegalizacion = New Infragistics.Win.UltraWinGrid.UltraCombo()
		Me.ULcomitente = New Infragistics.Win.Misc.UltraLabel()
		Me.ULtarea = New Infragistics.Win.Misc.UltraLabel()
		Me.UCEIva = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.LResolucion = New System.Windows.Forms.Label()
		Me.CBResolucion = New System.Windows.Forms.ComboBox()
		Me.GBTrabajo = New System.Windows.Forms.GroupBox()
		Me.UTECodigoBarra = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.UDTiva = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
		Me.UGBControl = New Infragistics.Win.Misc.UltraGroupBox()
		Me.TxtHaber = New System.Windows.Forms.TextBox()
		Me.TxtDebe = New System.Windows.Forms.TextBox()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.UCEAporte = New Infragistics.Win.UltraWinEditors.UltraCurrencyEditor()
		CType(Me.UGComitentes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UCEganancias, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UDTdgi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UDTdgr, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UCEbonifica, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UGRetenciones, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UTERecibo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UGTrabajos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UGRecibos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UcboLegalizacion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UCEIva, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GBTrabajo.SuspendLayout()
		CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UDTiva, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.UGBControl, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.UGBControl.SuspendLayout()
		CType(Me.UCEAporte, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'UGComitentes
		'
		Me.UGComitentes.Anchor = System.Windows.Forms.AnchorStyles.None
		Appearance1.BackColor = System.Drawing.Color.White
		Appearance1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
		Me.UGComitentes.DisplayLayout.Appearance = Appearance1
		Me.UGComitentes.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
		Me.UGComitentes.DisplayLayout.InterBandSpacing = 10
		Appearance2.BackColor = System.Drawing.Color.Transparent
		Me.UGComitentes.DisplayLayout.Override.CardAreaAppearance = Appearance2
		Me.UGComitentes.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
		Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance3.ForeColor = System.Drawing.Color.White
		Appearance3.TextHAlignAsString = "Left"
		Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
		Me.UGComitentes.DisplayLayout.Override.HeaderAppearance = Appearance3
		Appearance4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGComitentes.DisplayLayout.Override.RowAppearance = Appearance4
		Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UGComitentes.DisplayLayout.Override.RowSelectorAppearance = Appearance5
		Me.UGComitentes.DisplayLayout.Override.RowSelectorWidth = 12
		Me.UGComitentes.DisplayLayout.Override.RowSpacingBefore = 2
		Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
		Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
		Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance6.ForeColor = System.Drawing.Color.Black
		Me.UGComitentes.DisplayLayout.Override.SelectedRowAppearance = Appearance6
		Me.UGComitentes.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
		Me.UGComitentes.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGComitentes.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
		Me.UGComitentes.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
		Me.UGComitentes.Location = New System.Drawing.Point(662, 24)
		Me.UGComitentes.Name = "UGComitentes"
		Me.UGComitentes.Size = New System.Drawing.Size(76, 26)
		Me.UGComitentes.TabIndex = 7
		Me.UGComitentes.Visible = False
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(6, 91)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(83, 21)
		Me.Label1.TabIndex = 8
		Me.Label1.Text = "Comitente:"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label2
		'
		Me.Label2.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(6, 120)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(83, 21)
		Me.Label2.TabIndex = 9
		Me.Label2.Text = "Tarea:"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label3
		'
		Me.Label3.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(640, 78)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(99, 95)
		Me.Label3.TabIndex = 11
		Me.Label3.Text = "  Honorarios" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "   Bonificación"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.Label3.Visible = False
		'
		'txHonorarios
		'
		Me.txHonorarios.BackColor = System.Drawing.Color.Aqua
		Me.txHonorarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txHonorarios.Location = New System.Drawing.Point(641, 97)
		Me.txHonorarios.Name = "txHonorarios"
		Me.txHonorarios.ReadOnly = True
		Me.txHonorarios.Size = New System.Drawing.Size(96, 26)
		Me.txHonorarios.TabIndex = 14
		Me.txHonorarios.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		Me.txHonorarios.Visible = False
		'
		'Label4
		'
		Me.Label4.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(7, 61)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(84, 23)
		Me.Label4.TabIndex = 15
		Me.Label4.Text = "Ganancias:"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UCEganancias
		'
		Me.UCEganancias.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UCEganancias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		ValueListItem3.DataValue = CType(1, Short)
		ValueListItem3.DisplayText = "SI"
		ValueListItem4.DataValue = CType(2, Short)
		ValueListItem4.DisplayText = "NO"
		Me.UCEganancias.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem3, ValueListItem4})
		Me.UCEganancias.Location = New System.Drawing.Point(91, 61)
		Me.UCEganancias.Name = "UCEganancias"
		Me.UCEganancias.Size = New System.Drawing.Size(49, 24)
		Me.UCEganancias.TabIndex = 3
		Me.UCEganancias.Text = "NO"
		'
		'Label5
		'
		Me.Label5.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.Location = New System.Drawing.Point(7, 31)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(106, 23)
		Me.Label5.TabIndex = 27
		Me.Label5.Text = "Excepción DGI"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UDTdgi
		'
		Me.UDTdgi.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UDTdgi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UDTdgi.FormatProvider = New System.Globalization.CultureInfo("es-AR")
		Me.UDTdgi.FormatString = ""
		Me.UDTdgi.Location = New System.Drawing.Point(119, 31)
		Me.UDTdgi.MaskInput = "{date}"
		Me.UDTdgi.Name = "UDTdgi"
		Me.UDTdgi.Size = New System.Drawing.Size(109, 24)
		Me.UDTdgi.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
		Me.UDTdgi.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
		Me.UDTdgi.TabIndex = 1
		Me.UDTdgi.Value = Nothing
		'
		'Label6
		'
		Me.Label6.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.Location = New System.Drawing.Point(235, 31)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(106, 23)
		Me.Label6.TabIndex = 29
		Me.Label6.Text = "Excepción DGR"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UDTdgr
		'
		Me.UDTdgr.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UDTdgr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UDTdgr.FormatProvider = New System.Globalization.CultureInfo("es-AR")
		Me.UDTdgr.FormatString = ""
		Me.UDTdgr.Location = New System.Drawing.Point(348, 31)
		Me.UDTdgr.MaskInput = "{date}"
		Me.UDTdgr.Name = "UDTdgr"
		Me.UDTdgr.Size = New System.Drawing.Size(109, 24)
		Me.UDTdgr.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
		Me.UDTdgr.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
		Me.UDTdgr.TabIndex = 2
		Me.UDTdgr.Value = Nothing
		'
		'Label7
		'
		Me.Label7.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.Location = New System.Drawing.Point(147, 61)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(81, 23)
		Me.Label7.TabIndex = 31
		Me.Label7.Text = "Bonifica:"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UCEbonifica
		'
		Me.UCEbonifica.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UCEbonifica.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		ValueListItem1.DataValue = CType(1, Short)
		ValueListItem1.DisplayText = "SI"
		ValueListItem2.DataValue = CType(2, Short)
		ValueListItem2.DisplayText = "NO"
		Me.UCEbonifica.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2})
		Me.UCEbonifica.Location = New System.Drawing.Point(235, 61)
		Me.UCEbonifica.Name = "UCEbonifica"
		Me.UCEbonifica.Size = New System.Drawing.Size(49, 24)
		Me.UCEbonifica.TabIndex = 4
		Me.UCEbonifica.Text = "NO"
		'
		'Label9
		'
		Me.Label9.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.Location = New System.Drawing.Point(7, 3)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(106, 23)
		Me.Label9.TabIndex = 35
		Me.Label9.Text = "Legalización"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UGRetenciones
		'
		Me.UGRetenciones.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Appearance7.BackColor = System.Drawing.Color.White
		Me.UGRetenciones.DisplayLayout.Appearance = Appearance7
		Appearance8.BackColor = System.Drawing.Color.Transparent
		Me.UGRetenciones.DisplayLayout.Override.CardAreaAppearance = Appearance8
		Appearance9.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
		Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
		Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance9.FontData.BoldAsString = "True"
		Appearance9.FontData.Name = "Arial"
		Appearance9.FontData.SizeInPoints = 10.0!
		Appearance9.ForeColor = System.Drawing.Color.White
		Appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
		Me.UGRetenciones.DisplayLayout.Override.HeaderAppearance = Appearance9
		Appearance10.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
		Appearance10.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
		Appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UGRetenciones.DisplayLayout.Override.RowSelectorAppearance = Appearance10
		Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
		Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
		Appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UGRetenciones.DisplayLayout.Override.SelectedRowAppearance = Appearance11
		Me.UGRetenciones.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
		Me.UGRetenciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
		Me.UGRetenciones.Location = New System.Drawing.Point(6, 179)
		Me.UGRetenciones.Name = "UGRetenciones"
		Me.UGRetenciones.Size = New System.Drawing.Size(733, 337)
		Me.UGRetenciones.TabIndex = 36
		Me.UGRetenciones.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChange
		'
		'CBConfirmar
		'
		Me.CBConfirmar.BackColor = System.Drawing.Color.Transparent
		Me.CBConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CBConfirmar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
		Me.CBConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.CBConfirmar.Location = New System.Drawing.Point(277, 529)
		Me.CBConfirmar.Name = "CBConfirmar"
		Me.CBConfirmar.Size = New System.Drawing.Size(154, 41)
		Me.CBConfirmar.TabIndex = 7
		Me.CBConfirmar.Text = "Confirmar"
		Me.CBConfirmar.UseVisualStyleBackColor = False
		'
		'Label8
		'
		Me.Label8.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.Location = New System.Drawing.Point(483, 519)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(163, 21)
		Me.Label8.TabIndex = 38
		Me.Label8.Text = "Total Retenciones:"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Label8.Visible = False
		'
		'Label10
		'
		Me.Label10.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label10.Location = New System.Drawing.Point(483, 546)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(163, 21)
		Me.Label10.TabIndex = 39
		Me.Label10.Text = "Reintegro Pendiente:"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Label10.Visible = False
		'
		'TxTotRet
		'
		Me.TxTotRet.BackColor = System.Drawing.Color.Aqua
		Me.TxTotRet.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxTotRet.Location = New System.Drawing.Point(646, 517)
		Me.TxTotRet.Name = "TxTotRet"
		Me.TxTotRet.ReadOnly = True
		Me.TxTotRet.Size = New System.Drawing.Size(96, 26)
		Me.TxTotRet.TabIndex = 40
		Me.TxTotRet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.TxTotRet.Visible = False
		'
		'TxReiPend
		'
		Me.TxReiPend.BackColor = System.Drawing.Color.Aqua
		Me.TxReiPend.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxReiPend.Location = New System.Drawing.Point(646, 544)
		Me.TxReiPend.Name = "TxReiPend"
		Me.TxReiPend.ReadOnly = True
		Me.TxReiPend.Size = New System.Drawing.Size(96, 26)
		Me.TxReiPend.TabIndex = 41
		Me.TxReiPend.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.TxReiPend.Visible = False
		'
		'Label11
		'
		Me.Label11.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label11.Location = New System.Drawing.Point(7, 149)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(83, 21)
		Me.Label11.TabIndex = 42
		Me.Label11.Text = "Recibo:"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UTERecibo
		'
		Me.UTERecibo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Appearance12.BackColor = System.Drawing.Color.Cyan
		Appearance12.TextHAlignAsString = "Center"
		Me.UTERecibo.Appearance = Appearance12
		Me.UTERecibo.AutoSize = False
		Me.UTERecibo.BackColor = System.Drawing.Color.Cyan
		Appearance13.Image = CType(resources.GetObject("Appearance13.Image"), Object)
		EditorButton1.Appearance = Appearance13
		EditorButton1.Key = "Left"
		EditorButton1.Width = 30
		Me.UTERecibo.ButtonsLeft.Add(EditorButton1)
		Appearance14.Image = CType(resources.GetObject("Appearance14.Image"), Object)
		EditorButton2.Appearance = Appearance14
		EditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
		EditorButton2.Key = "Right"
		Appearance15.Image = CType(resources.GetObject("Appearance15.Image"), Object)
		Appearance15.ImageBackgroundDisabled = CType(resources.GetObject("Appearance15.ImageBackgroundDisabled"), System.Drawing.Image)
		EditorButton2.PressedAppearance = Appearance15
		EditorButton2.Text = "Aceptar"
		EditorButton2.Width = 90
		Me.UTERecibo.ButtonsRight.Add(EditorButton2)
		Me.UTERecibo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UTERecibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UTERecibo.Location = New System.Drawing.Point(90, 148)
		Me.UTERecibo.Name = "UTERecibo"
		Me.UTERecibo.Size = New System.Drawing.Size(547, 25)
		Me.UTERecibo.TabIndex = 6
		'
		'TxBonifica
		'
		Me.TxBonifica.BackColor = System.Drawing.Color.Aqua
		Me.TxBonifica.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxBonifica.Location = New System.Drawing.Point(641, 144)
		Me.TxBonifica.Name = "TxBonifica"
		Me.TxBonifica.ReadOnly = True
		Me.TxBonifica.Size = New System.Drawing.Size(96, 26)
		Me.TxBonifica.TabIndex = 44
		Me.TxBonifica.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		Me.TxBonifica.Visible = False
		'
		'UGTrabajos
		'
		Me.UGTrabajos.Anchor = System.Windows.Forms.AnchorStyles.None
		Appearance16.BackColor = System.Drawing.Color.White
		Appearance16.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
		Me.UGTrabajos.DisplayLayout.Appearance = Appearance16
		Me.UGTrabajos.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
		Me.UGTrabajos.DisplayLayout.InterBandSpacing = 10
		Appearance17.BackColor = System.Drawing.Color.Transparent
		Me.UGTrabajos.DisplayLayout.Override.CardAreaAppearance = Appearance17
		Me.UGTrabajos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
		Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance18.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance18.ForeColor = System.Drawing.Color.White
		Appearance18.TextHAlignAsString = "Left"
		Appearance18.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
		Me.UGTrabajos.DisplayLayout.Override.HeaderAppearance = Appearance18
		Appearance19.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGTrabajos.DisplayLayout.Override.RowAppearance = Appearance19
		Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance20.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UGTrabajos.DisplayLayout.Override.RowSelectorAppearance = Appearance20
		Me.UGTrabajos.DisplayLayout.Override.RowSelectorWidth = 12
		Me.UGTrabajos.DisplayLayout.Override.RowSpacingBefore = 2
		Appearance21.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
		Appearance21.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
		Appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance21.ForeColor = System.Drawing.Color.Black
		Me.UGTrabajos.DisplayLayout.Override.SelectedRowAppearance = Appearance21
		Me.UGTrabajos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
		Me.UGTrabajos.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGTrabajos.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
		Me.UGTrabajos.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
		Me.UGTrabajos.Location = New System.Drawing.Point(580, 25)
		Me.UGTrabajos.Name = "UGTrabajos"
		Me.UGTrabajos.Size = New System.Drawing.Size(76, 25)
		Me.UGTrabajos.TabIndex = 45
		Me.UGTrabajos.Visible = False
		'
		'UGRecibos
		'
		Me.UGRecibos.Anchor = System.Windows.Forms.AnchorStyles.None
		Appearance22.BackColor = System.Drawing.Color.White
		Appearance22.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
		Me.UGRecibos.DisplayLayout.Appearance = Appearance22
		Me.UGRecibos.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
		Me.UGRecibos.DisplayLayout.InterBandSpacing = 10
		Appearance23.BackColor = System.Drawing.Color.Transparent
		Me.UGRecibos.DisplayLayout.Override.CardAreaAppearance = Appearance23
		Me.UGRecibos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
		Appearance24.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance24.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance24.ForeColor = System.Drawing.Color.White
		Appearance24.TextHAlignAsString = "Left"
		Appearance24.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
		Me.UGRecibos.DisplayLayout.Override.HeaderAppearance = Appearance24
		Appearance25.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGRecibos.DisplayLayout.Override.RowAppearance = Appearance25
		Appearance26.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance26.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UGRecibos.DisplayLayout.Override.RowSelectorAppearance = Appearance26
		Me.UGRecibos.DisplayLayout.Override.RowSelectorWidth = 12
		Me.UGRecibos.DisplayLayout.Override.RowSpacingBefore = 2
		Appearance27.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
		Appearance27.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
		Appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance27.ForeColor = System.Drawing.Color.Black
		Me.UGRecibos.DisplayLayout.Override.SelectedRowAppearance = Appearance27
		Me.UGRecibos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
		Me.UGRecibos.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UGRecibos.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
		Me.UGRecibos.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
		Me.UGRecibos.Location = New System.Drawing.Point(497, 25)
		Me.UGRecibos.Name = "UGRecibos"
		Me.UGRecibos.Size = New System.Drawing.Size(77, 26)
		Me.UGRecibos.TabIndex = 46
		Me.UGRecibos.Visible = False
		'
		'UcboLegalizacion
		'
		Appearance28.TextHAlignAsString = "Center"
		Me.UcboLegalizacion.Appearance = Appearance28
		Me.UcboLegalizacion.AutoSize = False
		Appearance29.BackColor = System.Drawing.Color.White
		Appearance29.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
		Me.UcboLegalizacion.DisplayLayout.Appearance = Appearance29
		Me.UcboLegalizacion.DisplayLayout.InterBandSpacing = 10
		Appearance30.BackColor = System.Drawing.Color.Transparent
		Me.UcboLegalizacion.DisplayLayout.Override.CardAreaAppearance = Appearance30
		Appearance31.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance31.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance31.ForeColor = System.Drawing.Color.White
		Appearance31.TextHAlignAsString = "Left"
		Appearance31.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
		Me.UcboLegalizacion.DisplayLayout.Override.HeaderAppearance = Appearance31
		Appearance32.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UcboLegalizacion.DisplayLayout.Override.RowAppearance = Appearance32
		Appearance33.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
		Appearance33.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Me.UcboLegalizacion.DisplayLayout.Override.RowSelectorAppearance = Appearance33
		Me.UcboLegalizacion.DisplayLayout.Override.RowSelectorWidth = 12
		Me.UcboLegalizacion.DisplayLayout.Override.RowSpacingBefore = 2
		Appearance34.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
		Appearance34.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
		Appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
		Appearance34.ForeColor = System.Drawing.Color.Black
		Me.UcboLegalizacion.DisplayLayout.Override.SelectedRowAppearance = Appearance34
		Me.UcboLegalizacion.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
		Me.UcboLegalizacion.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
		Me.UcboLegalizacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UcboLegalizacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UcboLegalizacion.Location = New System.Drawing.Point(119, 3)
		Me.UcboLegalizacion.Name = "UcboLegalizacion"
		Me.UcboLegalizacion.Size = New System.Drawing.Size(110, 22)
		Me.UcboLegalizacion.TabIndex = 0
		'
		'ULcomitente
		'
		Me.ULcomitente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Appearance40.BackColor = System.Drawing.Color.White
		Me.ULcomitente.Appearance = Appearance40
		Me.ULcomitente.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded1
		Me.ULcomitente.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Rounded1
		Me.ULcomitente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
		Me.ULcomitente.Location = New System.Drawing.Point(90, 91)
		Me.ULcomitente.Name = "ULcomitente"
		Me.ULcomitente.Size = New System.Drawing.Size(547, 23)
		Me.ULcomitente.TabIndex = 48
		'
		'ULtarea
		'
		Me.ULtarea.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Appearance41.BackColor = System.Drawing.Color.White
		Me.ULtarea.Appearance = Appearance41
		Me.ULtarea.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded1
		Me.ULtarea.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Rounded1
		Me.ULtarea.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
		Me.ULtarea.Location = New System.Drawing.Point(90, 120)
		Me.ULtarea.Name = "ULtarea"
		Me.ULtarea.Size = New System.Drawing.Size(547, 23)
		Me.ULtarea.TabIndex = 49
		'
		'UCEIva
		'
		Me.UCEIva.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UCEIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UCEIva.FormatProvider = New System.Globalization.CultureInfo("es-AR")
		Me.UCEIva.FormatString = ""
		Me.UCEIva.Location = New System.Drawing.Point(348, 61)
		Me.UCEIva.MaskInput = ""
		Me.UCEIva.Name = "UCEIva"
		Me.UCEIva.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
		Me.UCEIva.Size = New System.Drawing.Size(109, 24)
		Me.UCEIva.TabIndex = 5
		'
		'Label12
		'
		Me.Label12.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label12.Location = New System.Drawing.Point(290, 61)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(51, 23)
		Me.Label12.TabIndex = 52
		Me.Label12.Text = "IVA:"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'LResolucion
		'
		Me.LResolucion.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.LResolucion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.LResolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.LResolucion.Location = New System.Drawing.Point(235, 3)
		Me.LResolucion.Name = "LResolucion"
		Me.LResolucion.Size = New System.Drawing.Size(106, 23)
		Me.LResolucion.TabIndex = 53
		Me.LResolucion.Text = "Resolución"
		Me.LResolucion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'CBResolucion
		'
		Me.CBResolucion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.CBResolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.CBResolucion.FormattingEnabled = True
		Me.CBResolucion.Items.AddRange(New Object() {"2017-12", "2017-05", "2016-15", "2015-15", "2014-15"})
		Me.CBResolucion.Location = New System.Drawing.Point(348, 5)
		Me.CBResolucion.Name = "CBResolucion"
		Me.CBResolucion.Size = New System.Drawing.Size(109, 21)
		Me.CBResolucion.TabIndex = 54
		'
		'GBTrabajo
		'
		Me.GBTrabajo.Controls.Add(Me.UTECodigoBarra)
		Me.GBTrabajo.Location = New System.Drawing.Point(7, 520)
		Me.GBTrabajo.Name = "GBTrabajo"
		Me.GBTrabajo.Size = New System.Drawing.Size(219, 50)
		Me.GBTrabajo.TabIndex = 113
		Me.GBTrabajo.TabStop = False
		Me.GBTrabajo.Text = "Código de Barras"
		'
		'UTECodigoBarra
		'
		Me.UTECodigoBarra.AutoSize = False
		Appearance37.Image = CType(resources.GetObject("Appearance37.Image"), Object)
		Appearance37.ImageHAlign = Infragistics.Win.HAlign.Center
		Appearance37.ImageVAlign = Infragistics.Win.VAlign.Middle
		EditorButton3.Appearance = Appearance37
		EditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
		EditorButton3.Key = "Right"
		Appearance38.Image = CType(resources.GetObject("Appearance38.Image"), Object)
		Appearance38.ImageBackgroundDisabled = CType(resources.GetObject("Appearance38.ImageBackgroundDisabled"), System.Drawing.Image)
		EditorButton3.PressedAppearance = Appearance38
		EditorButton3.Width = 30
		Me.UTECodigoBarra.ButtonsRight.Add(EditorButton3)
		Me.UTECodigoBarra.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UTECodigoBarra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UTECodigoBarra.Location = New System.Drawing.Point(6, 20)
		Me.UTECodigoBarra.Name = "UTECodigoBarra"
		Me.UTECodigoBarra.Size = New System.Drawing.Size(207, 29)
		Me.UTECodigoBarra.TabIndex = 33
		'
		'Label13
		'
		Me.Label13.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label13.Location = New System.Drawing.Point(465, 30)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(106, 23)
		Me.Label13.TabIndex = 115
		Me.Label13.Text = "Excepción IVA"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UDTiva
		'
		Me.UDTiva.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UDTiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UDTiva.FormatProvider = New System.Globalization.CultureInfo("es-AR")
		Me.UDTiva.FormatString = ""
		Me.UDTiva.Location = New System.Drawing.Point(578, 30)
		Me.UDTiva.MaskInput = "{date}"
		Me.UDTiva.Name = "UDTiva"
		Me.UDTiva.Size = New System.Drawing.Size(109, 24)
		Me.UDTiva.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
		Me.UDTiva.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
		Me.UDTiva.TabIndex = 114
		Me.UDTiva.Value = Nothing
		'
		'UGBControl
		'
		Me.UGBControl.Anchor = System.Windows.Forms.AnchorStyles.None
		Appearance42.BackColor = System.Drawing.Color.SteelBlue
		Appearance42.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal
		Me.UGBControl.ContentAreaAppearance = Appearance42
		Me.UGBControl.Controls.Add(Me.TxtHaber)
		Me.UGBControl.Controls.Add(Me.TxtDebe)
		Me.UGBControl.Location = New System.Drawing.Point(483, 570)
		Me.UGBControl.Name = "UGBControl"
		Me.UGBControl.Size = New System.Drawing.Size(257, 55)
		Me.UGBControl.TabIndex = 116
		Me.UGBControl.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.VisualStudio2005
		'
		'TxtHaber
		'
		Me.TxtHaber.BackColor = System.Drawing.Color.Black
		Me.TxtHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxtHaber.ForeColor = System.Drawing.SystemColors.Window
		Me.TxtHaber.Location = New System.Drawing.Point(131, 13)
		Me.TxtHaber.Name = "TxtHaber"
		Me.TxtHaber.ReadOnly = True
		Me.TxtHaber.Size = New System.Drawing.Size(122, 26)
		Me.TxtHaber.TabIndex = 19
		Me.TxtHaber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'TxtDebe
		'
		Me.TxtDebe.BackColor = System.Drawing.Color.Black
		Me.TxtDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxtDebe.ForeColor = System.Drawing.SystemColors.Window
		Me.TxtDebe.Location = New System.Drawing.Point(6, 13)
		Me.TxtDebe.Name = "TxtDebe"
		Me.TxtDebe.ReadOnly = True
		Me.TxtDebe.Size = New System.Drawing.Size(122, 26)
		Me.TxtDebe.TabIndex = 18
		Me.TxtDebe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'Label14
		'
		Me.Label14.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label14.Location = New System.Drawing.Point(465, 61)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(62, 23)
		Me.Label14.TabIndex = 117
		Me.Label14.Text = "Aporte:"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'UCEAporte
		'
		Me.UCEAporte.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
		Me.UCEAporte.Enabled = False
		Me.UCEAporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.UCEAporte.FormatProvider = New System.Globalization.CultureInfo("es-AR")
		Me.UCEAporte.FormatString = ""
		Me.UCEAporte.Location = New System.Drawing.Point(533, 61)
		Me.UCEAporte.MaskInput = ""
		Me.UCEAporte.Name = "UCEAporte"
		Me.UCEAporte.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
		Me.UCEAporte.Size = New System.Drawing.Size(104, 24)
		Me.UCEAporte.TabIndex = 118
		'
		'FrmLiquidacionHonorarios
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
		Me.ClientSize = New System.Drawing.Size(742, 623)
		Me.Controls.Add(Me.UCEAporte)
		Me.Controls.Add(Me.Label14)
		Me.Controls.Add(Me.UGBControl)
		Me.Controls.Add(Me.Label13)
		Me.Controls.Add(Me.UDTiva)
		Me.Controls.Add(Me.GBTrabajo)
		Me.Controls.Add(Me.CBResolucion)
		Me.Controls.Add(Me.LResolucion)
		Me.Controls.Add(Me.UCEIva)
		Me.Controls.Add(Me.Label12)
		Me.Controls.Add(Me.ULtarea)
		Me.Controls.Add(Me.ULcomitente)
		Me.Controls.Add(Me.UcboLegalizacion)
		Me.Controls.Add(Me.UGRecibos)
		Me.Controls.Add(Me.UGTrabajos)
		Me.Controls.Add(Me.TxBonifica)
		Me.Controls.Add(Me.Label11)
		Me.Controls.Add(Me.TxReiPend)
		Me.Controls.Add(Me.TxTotRet)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.UGComitentes)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.UDTdgi)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.UDTdgr)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.UCEganancias)
		Me.Controls.Add(Me.UCEbonifica)
		Me.Controls.Add(Me.Label7)
		Me.Controls.Add(Me.UGRetenciones)
		Me.Controls.Add(Me.CBConfirmar)
		Me.Controls.Add(Me.txHonorarios)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Label10)
		Me.Controls.Add(Me.UTERecibo)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "FrmLiquidacionHonorarios"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Liquidacion de Honorarios"
		CType(Me.UGComitentes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UCEganancias, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UDTdgi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UDTdgr, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UCEbonifica, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UGRetenciones, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UTERecibo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UGTrabajos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UGRecibos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UcboLegalizacion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UCEIva, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GBTrabajo.ResumeLayout(False)
		CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UDTiva, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.UGBControl, System.ComponentModel.ISupportInitialize).EndInit()
		Me.UGBControl.ResumeLayout(False)
		Me.UGBControl.PerformLayout()
		CType(Me.UCEAporte, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents UGComitentes As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txHonorarios As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents UCEganancias As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents UDTdgi As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents UDTdgr As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents UCEbonifica As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents UGRetenciones As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBConfirmar As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TxTotRet As System.Windows.Forms.TextBox
    Friend WithEvents TxReiPend As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents UTERecibo As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents TxBonifica As System.Windows.Forms.TextBox
    Friend WithEvents UGTrabajos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UGRecibos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UcboLegalizacion As Infragistics.Win.UltraWinGrid.UltraCombo
    Friend WithEvents ULcomitente As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents ULtarea As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UCEIva As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents LResolucion As System.Windows.Forms.Label
    Friend WithEvents CBResolucion As System.Windows.Forms.ComboBox
    Friend WithEvents GBTrabajo As System.Windows.Forms.GroupBox
    Friend WithEvents UTECodigoBarra As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents Label13 As Label
    Friend WithEvents UDTiva As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UGBControl As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TxtHaber As TextBox
    Friend WithEvents TxtDebe As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents UCEAporte As Infragistics.Win.UltraWinEditors.UltraCurrencyEditor
End Class
