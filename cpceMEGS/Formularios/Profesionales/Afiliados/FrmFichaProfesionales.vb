﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Public Class FrmFichaProfesionales
    Private cnn As New ConsultaBD(True)
    Private c_drProf As DataRow
    Private c_dsProf As DataSet
    Private c_daProf As MySqlDataAdapter
    Private c_cnn As ConsultaBD
    Private l_alta As Boolean
    Private l_Actualizacion As Boolean
    Private c_tipPadron As String
    Private c_accion As Integer
    Private DAMatricula As MySqlDataAdapter
    Private DSMatricula As DataSet
    Private DTFamiliar As DataTable
    Private cmdMatricula As MySqlCommandBuilder
    Private bindingCuenta As New BindingSource
    Private bindingBase As BindingManagerBase
    Private dFecMysql As MySqlDateTime
    Private dFecJubMysql As MySqlDateTime
    Private dFecVenETMysql As MySqlDateTime
    Private dFecMysqlGraduacion As MySqlDateTime
    Private dFecMysqlDiploma As MySqlDateTime
    Private dFecMysqlMatricula As MySqlDateTime
    Private dFecMysqlFicha As MySqlDateTime
    Private n_nRow As Integer
    Private formularios As New List(Of Form)
    Private DAAfiliado As MySqlDataAdapter
    Private DSAfiliado As DataSet
    Private CMDAfiliado As MySqlCommandBuilder
    Public Sub New(ByRef tipoPadron As String, ByRef dsProf As DataSet, ByRef daProf As MySqlDataAdapter, ByVal nRow As Integer, ByVal lAlta As Boolean, ByVal conexion As ConsultaBD)
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        c_dsProf = dsProf
        c_daProf = daProf
        n_nRow = nRow
        l_alta = lAlta
        c_tipPadron = tipoPadron
        c_cnn = conexion
        'Sin Transparencia
        AllowTransparency = False
    End Sub

    Public ReadOnly Property GetActualicion() As Boolean
        Get
            Return l_Actualizacion
        End Get
    End Property

    Private Sub FrmFichaProfesionales_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Select Case c_tipPadron
            Case "A" ' profesionales
                c_accion = 1
            Case "P" ' proveedores
                c_accion = 3
                'quito o agrego los * de los label
                lbFecNac.Text = lbFecNac.Text.Substring(1)
                lbSexo.Text = lbSexo.Text.Substring(1)
                lbCivil.Text = lbCivil.Text.Substring(1)
                lbMail.Text = lbMail.Text.Substring(1)
                lbNombre.Text = lbNombre.Text.Substring(1)
                lbGanancia.Text = lbGanancia.Text.Insert(0, "*")
                TabControlFicha.TabPages.Remove(TPJubilacion)
                TabControlFicha.TabPages.Remove(TPFamiliaPrestamo)
                TabControlFicha.TabPages.Remove(TPTarjetas)
                lbCbuCredito.Hide()
                lbCbuDebito.Hide()
                Mtxcbu.Hide()
                MtxcbuCredito.Hide()
            Case "E" ' Empleados
                c_accion = 5
                TabControlFicha.TabPages.Remove(TPJubilacion)
            Case Else
        End Select
        l_Actualizacion = False
        If l_alta Then
            If c_tipPadron = "A" Or c_tipPadron = "E" Then
                ' SI ES AFILIADO SE CARGA MANUAL EL NRO. DOC.
                ' DESHABILITO LA CARGA DE TITULOS
                UGBTitulos.Enabled = False
                CboTipdoc.Enabled = True
                TxDocumento.ReadOnly = False
            Else
                If c_dsProf.Tables(0).Rows.Count = 0 Then
                    TxDocumento.Text = 1
                Else
                    'Busco el numero de doc mayor y le sumo 1
                    TxDocumento.Text = c_dsProf.Tables(0).Compute("Max(afi_nrodoc)", Nothing) + 1
                    'TxDocumento.Text = c_dsProf.Tables(0).Rows(c_dsProf.Tables(0).Rows.Count - 1).Item("afi_nrodoc") + 1
                End If
            End If
            c_drProf = c_dsProf.Tables(0).NewRow
            EditarProfesional(True)
            UGBPrestamo.Hide()
        Else
            c_drProf = c_dsProf.Tables(0).Rows(n_nRow)
            cargoDatosProfesional()
            CargaTitulo()
            CargaCategoria()
            CargoMatriculas()
            EditarProfesional(False)
        End If
        If c_tipPadron = "A" Or c_tipPadron = "E" Then
            CargaFamiliar()
        Else
            UGBTitulos.Visible = False
            UGBET.Visible = False
            UGBFamilia.Visible = False
            UGBPrestamo.Visible = False
            UGBObraSocial.Visible = False
            UGBJubilacion.Visible = False
            CBtPago.Visible = False
            cbtOrdenPago.Visible = False
            CButton3.Visible = False
            LGenerarRenta.Visible = False
            CBGenerarRenta.Visible = False
            Width = 510
        End If
    End Sub

    Private Sub cargoDatosProfesional()
        Try
            CboTipdoc.Text = c_drProf.Item("afi_tipdoc")
            TxDocumento.Text = c_drProf.Item("afi_nrodoc")
            If InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") = 0 Then ' SI NO TIENE COMA
                TxApellido.Text = c_drProf.Item("afi_nombre")
            Else
                TxApellido.Text = Strings.Left((Trim(c_drProf.Item("afi_nombre"))), InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") - 1)
                TxNombre.Text = Mid(Trim(c_drProf.Item("afi_nombre")), InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") + 2)
            End If
            MtxFecNac.Text = c_drProf.Item("afi_fecnac").ToString
            MtxCuit.Text = c_drProf.Item("afi_cuit")
            MtxDGR.Text = c_drProf.Item("afi_dgr")
            cbSexo.Text = c_drProf.Item("afi_sexo")
            cbCivil.Text = c_drProf.Item("afi_civil")
            cbProvincia.Text = c_drProf.Item("afi_provincia")
            cbDelegacion.SelectedIndex = CInt(c_drProf("afi_zona") - 1)
            TxDireccion.Text = c_drProf.Item("afi_direccion")
            TxLocalidad.Text = c_drProf.Item("afi_localidad")
            TxCodPos.Text = c_drProf.Item("afi_codpos")
            MtxTelefono.Text = c_drProf.Item("afi_telefono1")
            MtxCelu.Text = c_drProf.Item("afi_telefono2")
            TxMail.Text = c_drProf.Item("afi_mail")
            TxDebTuya.Text = c_drProf.Item("afi_deb_tuya")
            TxDebVisa.Text = c_drProf.Item("afi_deb_visa")
            TxDebMastercard.Text = c_drProf.Item("afi_deb_mastercard")
            TxDebNaranja.Text = c_drProf.Item("afi_deb_naranja")
            CargaObraSocial(Convert.ToInt32(c_drProf.Item("afi_obrasocial")))
            If IsDBNull(c_drProf.Item("afi_mail_alternativo")) Then
                TxMailAlternativo.Text = ""
            Else
                TxMailAlternativo.Text = c_drProf.Item("afi_mail_alternativo")
            End If
            TxGaranteDe.Text = c_drProf.Item("afi_garante")
            'controlo lo que tiene cargado en ganacias
            If c_drProf.Item("afi_ganancias") = "S" Then
                cbGanancia.Text = "SI"
            ElseIf c_drProf.Item("afi_ganancias") = "N" Then
                cbGanancia.Text = "NO"
            ElseIf c_drProf.Item("afi_ganancias") = "" Then
                cbGanancia.Text = "NO"
            Else
                cbGanancia.Text = c_drProf.Item("afi_ganancias")
            End If
            MtxExcepDGI.Text = c_drProf.Item("afi_dgiexcep").ToString
            MtxExcepDGR.Text = c_drProf.Item("afi_dgrexcep").ToString
            MtxExcepIVA.Text = c_drProf.Item("afi_ivaexcep").ToString
            If (c_tipPadron = "A" Or c_tipPadron = "E" Or c_tipPadron = "P") Then
                Mtxcbu.Text = c_drProf.Item("afi_cbu")
                If IsDBNull(c_drProf.Item("afi_cbu_credito")) Then
                    MtxcbuCredito.Text = ""
                Else
                    MtxcbuCredito.Text = c_drProf.Item("afi_cbu_credito")
                End If
            End If
            If (c_tipPadron = "A" Or c_tipPadron = "E") Then
                CBGenerarRenta.Text = c_drProf.Item("afi_generarrenta")
                'prestamo
                If Not (IsDBNull(c_drProf.Item("afi_garante_tipdoc")) And IsDBNull(c_drProf.Item("afi_garante_nrodoc"))) Then
                    Dim dataRowGarante = GetAfiliadoArray(c_drProf.Item("afi_garante_tipdoc"), c_drProf.Item("afi_garante_nrodoc"))
                    TBGarante.Text = dataRowGarante.Item("afi_nombre")
                    TBGaranteTipdoc.Text = dataRowGarante.Item("afi_tipdoc")
                    TBGaranteNrodoc.Text = dataRowGarante.Item("afi_nrodoc")
                Else
                    TBGarante.Text = ""
                    TBGaranteTipdoc.Text = ""
                    TBGaranteNrodoc.Text = ""
                End If
                If Not (IsDBNull(c_drProf.Item("afi_garantede_tipdoc")) And IsDBNull(c_drProf.Item("afi_garantede_nrodoc"))) Then
                    Dim dataRowGarantede = GetAfiliadoArray(c_drProf.Item("afi_garantede_tipdoc"), c_drProf.Item("afi_garantede_nrodoc"))
                    TBGarantede.Text = dataRowGarantede.Item("afi_nombre")
                    TBGarantedeTipdoc.Text = dataRowGarantede.Item("afi_tipdoc")
                    TBGarantedeNrodoc.Text = dataRowGarantede.Item("afi_nrodoc")
                Else
                    TBGarantede.Text = ""
                    TBGarantedeTipdoc.Text = ""
                    TBGarantedeNrodoc.Text = ""
                End If
            End If
            If (c_tipPadron = "A") Then
                'Matricula en tramite
                MtxFechaVencimientoET.Text = c_drProf.Item("afi_fecha_vencimiento_et").ToString
                'Fin Matricula en tramite
                'Jubilacion
                MtxJubilacion.Text = c_drProf.Item("afi_fecha_jubilacion").ToString
                TxJubiNroResolucion.Text = c_drProf.Item("afi_jubi_nro_resolucion")
                TxJubiEdadResolucion.Text = c_drProf.Item("afi_jubi_edad_resolucion")
                TxJubiPeriodoOpto.Text = c_drProf.Item("afi_jubi_periodo_opto")
                TxJubiCoeficiente.Text = c_drProf.Item("afi_jubi_coeficiente")
                TxJubiCapitalizacion.Text = c_drProf.Item("afi_jubi_capitalizacion")
                TxJubiFondoCompensador.Text = c_drProf.Item("afi_jubi_fondo_compensador")
                CBJubiAbonaCuota.Text = c_drProf.Item("afi_jubi_abona_cuota")
                TxJubiObservacion.Text = c_drProf.Item("afi_jubi_observacion")
                TxJubiImporteCuota.Text = c_drProf.Item("afi_jubi_importe_cuota")
                TxJubiPeriodosCobrados.Text = c_drProf.Item("afi_jubi_periodos_cobrados")
                TxJubiPeriodosRestantes.Text = c_drProf.Item("afi_jubi_periodos_restantes")
                'Fin Jubilacion
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EditarProfesional(ByVal lEditar As Boolean)
        Dim habilitar As Boolean
        Dim noeditar As Boolean
        If lEditar Then
            habilitar = True
            noeditar = False
        Else
            habilitar = False
            noeditar = True
        End If
        Try
            If (c_tipPadron = "A" Or c_tipPadron = "E") Then
                'Me.CboTipdoc.Enabled = lEditar
                'Me.TxDocumento.Enabled = lEditar
                Mtxcbu.ReadOnly = noeditar
                MtxcbuCredito.ReadOnly = noeditar
                'Prestamo
                CBGaranteTipdoc.Enabled = habilitar
                UTEGaranteNumdoc.Enabled = habilitar
                'Me.TBGarante.Enabled = lEditar
                CBGarantedeTipdoc.Enabled = habilitar
                UTEGarantedeNumdoc.Enabled = habilitar
                'Me.TBGarantede.Enabled = lEditar
            ElseIf (c_tipPadron = "P") Then
                Mtxcbu.ReadOnly = noeditar
                MtxcbuCredito.ReadOnly = noeditar
            End If
            If (c_tipPadron = "A") Then
                'Matricula en tramite
                MtxFechaVencimientoET.ReadOnly = noeditar
                'Fin Matricula en tramite
                'Jubilacion
                MtxJubilacion.ReadOnly = noeditar
                TxJubiNroResolucion.ReadOnly = noeditar
                TxJubiEdadResolucion.ReadOnly = noeditar
                TxJubiPeriodoOpto.ReadOnly = noeditar
                TxJubiCoeficiente.ReadOnly = noeditar
                TxJubiCapitalizacion.ReadOnly = noeditar
                TxJubiFondoCompensador.ReadOnly = noeditar
                TxJubiImporteCuota.ReadOnly = noeditar
                TxJubiPeriodosCobrados.ReadOnly = noeditar
                TxJubiPeriodosRestantes.ReadOnly = noeditar
                CBJubiAbonaCuota.Enabled = habilitar
                TxJubiObservacion.ReadOnly = noeditar
                'Fin Jubilacion
            End If
            cbSexo.Enabled = habilitar
            cbCivil.Enabled = habilitar
            cbProvincia.Enabled = habilitar
            cbDelegacion.Enabled = habilitar
            TxApellido.ReadOnly = noeditar
            TxNombre.ReadOnly = noeditar
            MtxFecNac.ReadOnly = noeditar
            MtxCuit.ReadOnly = noeditar
            MtxDGR.ReadOnly = noeditar
            TxDireccion.ReadOnly = noeditar
            TxLocalidad.ReadOnly = noeditar
            TxCodPos.ReadOnly = noeditar
            MtxTelefono.ReadOnly = noeditar
            MtxCelu.ReadOnly = noeditar
            TxMail.ReadOnly = noeditar
            TxMailAlternativo.ReadOnly = noeditar
            TxGaranteDe.ReadOnly = noeditar
            cbGanancia.Enabled = habilitar
            CBGenerarRenta.Enabled = habilitar
            MtxExcepDGI.ReadOnly = noeditar
            MtxExcepDGR.ReadOnly = noeditar
            MtxExcepIVA.ReadOnly = noeditar
            cbObraSocial.Enabled = habilitar
            If lEditar Then
                UGFlia.DisplayLayout.Bands(0).Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True
            Else
                UGFlia.DisplayLayout.Bands(0).Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EditarTitulo(ByVal lEditar As Boolean)
        Dim habilitar As Boolean
        Dim noeditar As Boolean
        If lEditar Then
            habilitar = True
            noeditar = False
        Else
            habilitar = False
            noeditar = True
        End If
        'cboTitulo.Enabled = lEditar
        If txMatricula.Text = "" Then
            txMatricula.ReadOnly = noeditar
        Else
            'Si no esta vacio el campo no lo puede editar
            txMatricula.ReadOnly = True
        End If
        UDTgraduacion.ReadOnly = noeditar
        UDTdiploma.ReadOnly = noeditar
        UDTmatricula.ReadOnly = noeditar
        UDTficha.ReadOnly = noeditar
        cbCategoria.Enabled = habilitar
    End Sub

    Private Function ActualizoDatosProfesional() As Boolean
        ActualizoDatosProfesional = True
        Dim message As String
        If c_cnn.AbrirConexion() Then
            'Transaccion objeto en mysql y poder hacer rollback
            Dim miTrans As MySqlTransaction
            miTrans = c_cnn.InicioTransaccion()
            'Transaccion
            Try
                If l_alta Then
                    c_drProf.Item("afi_tipdoc") = CboTipdoc.Text
                    c_drProf.Item("afi_nrodoc") = TxDocumento.Text
                    ' SI NO ES PROFESIONAL EN TITULO VA EL DISMINUTIVO DEL TIPO DE DOCUMENTO Y EN MATRICULA EL NRO DOC.
                    If c_tipPadron <> "A" Then
                        If c_tipPadron = "E" Then
                            c_drProf.Item("afi_titulo") = "PE"
                            Dim DTEmp As New DataTable
                            DAMatricula = c_cnn.consultaBDadapter("afiliado", "max(afi_matricula) as nroempr", "afi_titulo='PE'")
                            DAMatricula.Fill(DTEmp)
                            If DTEmp.Rows.Count = 1 Then
                                c_drProf.Item("afi_matricula") = DTEmp.Rows(0).Item("nroempr") + 1
                            Else
                                c_drProf.Item("afi_matricula") = 1
                            End If
                        Else
                            c_drProf.Item("afi_titulo") = Mid(CboTipdoc.Text, 1, 2)
                            c_drProf.Item("afi_matricula") = TxDocumento.Text
                        End If
                    End If
                    message = "el alta."
                Else
                    message = "la actualización."
                End If

                c_drProf.Item("afi_tipo") = c_tipPadron

                If IsDate(MtxFecNac.Text) Then
                    dFecMysql.Day = MtxFecNac.Text.Substring(0, 2)
                    dFecMysql.Month = MtxFecNac.Text.Substring(3, 2)
                    dFecMysql.Year = MtxFecNac.Text.Substring(6, 4)
                Else
                    dFecMysql.Day = "00"
                    dFecMysql.Month = "00"
                    dFecMysql.Year = "0000"
                End If

                If IsDate(MtxJubilacion.Text) Then
                    dFecJubMysql.Day = MtxJubilacion.Text.Substring(0, 2)
                    dFecJubMysql.Month = MtxJubilacion.Text.Substring(3, 2)
                    dFecJubMysql.Year = MtxJubilacion.Text.Substring(6, 4)
                Else
                    dFecJubMysql.Day = "00"
                    dFecJubMysql.Month = "00"
                    dFecJubMysql.Year = "0000"
                End If

                If IsDate(MtxFechaVencimientoET.Text) Then
                    dFecVenETMysql.Day = MtxFechaVencimientoET.Text.Substring(0, 2)
                    dFecVenETMysql.Month = MtxFechaVencimientoET.Text.Substring(3, 2)
                    dFecVenETMysql.Year = MtxFechaVencimientoET.Text.Substring(6, 4)
                Else
                    dFecVenETMysql.Day = "00"
                    dFecVenETMysql.Month = "00"
                    dFecVenETMysql.Year = "0000"
                End If
                'voy a hacer el control de campos obligatorios
                If validaCampo(MtxCuit) Then
                    ActualizoDatosProfesional = False
                End If
                If validaCampo(TxApellido) Then
                    ActualizoDatosProfesional = False
                End If
                If c_tipPadron <> "P" Then
                    If validaCampo(TxNombre) Then
                        ActualizoDatosProfesional = False
                    End If
                    If validaCampo(MtxFecNac) Then
                        ActualizoDatosProfesional = False
                    End If
                    If validaCampo(cbSexo) Then
                        ActualizoDatosProfesional = False
                    End If
                    If validaCampo(cbCivil) Then
                        ActualizoDatosProfesional = False
                    End If
                    If cuentaNumerosMask(MtxTelefono) < 9 And cuentaNumerosMask(MtxCelu) < 9 Then
                        ActualizoDatosProfesional = False
                        MessageBox.Show("Debe cargar Celular O Telefono", "Dato requerido", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    If validaCampo(TxMail) Then
                        ActualizoDatosProfesional = False
                    End If
                    If validaCampo(cbGanancia) Then
                        ActualizoDatosProfesional = False
                    End If
                End If
                If validaCampo(TxDireccion) Then
                    ActualizoDatosProfesional = False
                End If
                If validaCampo(TxLocalidad) Then
                    ActualizoDatosProfesional = False
                End If
                If validaCampo(cbProvincia) Then
                    ActualizoDatosProfesional = False
                End If
                If validaCampo(TxCodPos) Then
                    ActualizoDatosProfesional = False
                End If
                If validaCampo(cbDelegacion) Then
                    ActualizoDatosProfesional = False
                End If

                'afi_nombre
                If String.IsNullOrWhiteSpace(TxApellido.Text) Then
                    ActualizoDatosProfesional = False
                Else
                    If String.IsNullOrWhiteSpace(TxNombre.Text) Then
                        c_drProf.Item("afi_nombre") = Trim(TxApellido.Text)
                    Else
                        c_drProf.Item("afi_nombre") = Trim(TxApellido.Text) & ", " & Trim(TxNombre.Text)
                    End If
                End If
                c_drProf.Item("afi_fecnac") = dFecMysql
                c_drProf.Item("afi_cuit") = MtxCuit.Text
                c_drProf.Item("afi_dgr") = MtxDGR.Text
                c_drProf.Item("afi_sexo") = cbSexo.Text
                If String.IsNullOrWhiteSpace(cbObraSocial.Text) Then
                    c_drProf.Item("afi_obrasocial") = 0
                Else
                    c_drProf.Item("afi_obrasocial") = cbObraSocial.SelectedValue.ToString()
                End If
                c_drProf.Item("afi_civil") = cbCivil.Text
                c_drProf.Item("afi_provincia") = cbProvincia.Text
                c_drProf.Item("afi_zona") = Format(cbDelegacion.SelectedIndex + 1, "0000")
                c_drProf.Item("afi_direccion") = TxDireccion.Text
                c_drProf.Item("afi_localidad") = TxLocalidad.Text
                If IsNumeric(TxCodPos.Text) Then
                    c_drProf.Item("afi_codpos") = Convert.ToInt16(TxCodPos.Text)
                Else
                    c_drProf.Item("afi_codpos") = 0
                End If
                c_drProf.Item("afi_telefono1") = MtxTelefono.Text
                c_drProf.Item("afi_telefono2") = MtxCelu.Text
                c_drProf.Item("afi_mail") = TxMail.Text
                c_drProf.Item("afi_mail_alternativo") = TxMailAlternativo.Text
                c_drProf.Item("afi_garante") = TxGaranteDe.Text
                c_drProf.Item("afi_ganancias") = cbGanancia.Text
                'CBU debito
                If Mtxcbu.Text = "    -    -    -    -    -" Then
                    c_drProf.Item("afi_cbu") = ""
                Else
                    c_drProf.Item("afi_cbu") = Mtxcbu.Text.Replace("-", "")
                End If
                'CBU Credito
                If MtxcbuCredito.Text = "    -    -    -    -    -" Then
                    c_drProf.Item("afi_cbu_credito") = ""
                Else
                    c_drProf.Item("afi_cbu_credito") = MtxcbuCredito.Text.Replace("-", "")
                End If
                If IsDate(MtxExcepDGI.Text) Then
                    dFecMysql.Day = MtxExcepDGI.Text.Substring(0, 2)
                    dFecMysql.Month = MtxExcepDGI.Text.Substring(3, 2)
                    dFecMysql.Year = MtxExcepDGI.Text.Substring(6, 4)
                Else
                    dFecMysql.Day = "00"
                    dFecMysql.Month = "00"
                    dFecMysql.Year = "0000"
                End If
                c_drProf.Item("afi_dgiexcep") = dFecMysql
                If IsDate(MtxExcepDGR.Text) Then
                    dFecMysql.Day = MtxExcepDGR.Text.Substring(0, 2)
                    dFecMysql.Month = MtxExcepDGR.Text.Substring(3, 2)
                    dFecMysql.Year = MtxExcepDGR.Text.Substring(6, 4)
                Else
                    dFecMysql.Day = "00"
                    dFecMysql.Month = "00"
                    dFecMysql.Year = "0000"
                End If
                c_drProf.Item("afi_dgrexcep") = dFecMysql
                If IsDate(MtxExcepIVA.Text) Then
                    dFecMysql.Day = MtxExcepIVA.Text.Substring(0, 2)
                    dFecMysql.Month = MtxExcepIVA.Text.Substring(3, 2)
                    dFecMysql.Year = MtxExcepIVA.Text.Substring(6, 4)
                Else
                    dFecMysql.Day = "00"
                    dFecMysql.Month = "00"
                    dFecMysql.Year = "0000"
                End If
                c_drProf.Item("afi_ivaexcep") = dFecMysql
                If (c_tipPadron = "A") Then
                    'Matricula en tramite
                    c_drProf.Item("afi_fecha_vencimiento_et") = dFecVenETMysql
                    'Fin Matricula en tramite
                    'Jubilacion
                    c_drProf.Item("afi_fecha_jubilacion") = dFecJubMysql
                    c_drProf.Item("afi_jubi_nro_resolucion") = TxJubiNroResolucion.Text
                    If IsNumeric(TxJubiEdadResolucion.Text) Then
                        c_drProf.Item("afi_jubi_edad_resolucion") = Convert.ToInt16(TxJubiEdadResolucion.Text)
                    Else
                        c_drProf.Item("afi_jubi_edad_resolucion") = 0
                    End If
                    If IsNumeric(TxJubiPeriodoOpto.Text) Then
                        c_drProf.Item("afi_jubi_periodo_opto") = Convert.ToInt16(TxJubiPeriodoOpto.Text)
                    Else
                        c_drProf.Item("afi_jubi_periodo_opto") = 0
                    End If
                    If IsNumeric(TxJubiCoeficiente.Text) Then
                        c_drProf.Item("afi_jubi_coeficiente") = Convert.ToDecimal(TxJubiCoeficiente.Text)
                    Else
                        c_drProf.Item("afi_jubi_coeficiente") = 0
                    End If
                    If IsNumeric(TxJubiCapitalizacion.Text) Then
                        c_drProf.Item("afi_jubi_capitalizacion") = Convert.ToDecimal(TxJubiCapitalizacion.Text)
                    Else
                        c_drProf.Item("afi_jubi_capitalizacion") = 0
                    End If
                    If IsNumeric(TxJubiFondoCompensador.Text) Then
                        c_drProf.Item("afi_jubi_fondo_compensador") = Convert.ToDecimal(TxJubiFondoCompensador.Text)
                    Else
                        c_drProf.Item("afi_jubi_fondo_compensador") = 0
                    End If
                    If IsNumeric(TxJubiImporteCuota.Text) Then
                        c_drProf.Item("afi_jubi_importe_cuota") = Convert.ToDecimal(TxJubiImporteCuota.Text)
                    Else
                        c_drProf.Item("afi_jubi_importe_cuota") = 0
                    End If
                    If IsNumeric(TxJubiPeriodosCobrados.Text) Then
                        c_drProf.Item("afi_jubi_periodos_cobrados") = Convert.ToDecimal(TxJubiPeriodosCobrados.Text)
                    Else
                        c_drProf.Item("afi_jubi_periodos_cobrados") = 0
                    End If
                    If IsNumeric(TxJubiPeriodosRestantes.Text) Then
                        c_drProf.Item("afi_jubi_periodos_restantes") = Convert.ToDecimal(TxJubiPeriodosRestantes.Text)
                    Else
                        c_drProf.Item("afi_jubi_periodos_restantes") = 0
                    End If
                    c_drProf.Item("afi_jubi_abona_cuota") = CBJubiAbonaCuota.Text
                    c_drProf.Item("afi_jubi_observacion") = TxJubiObservacion.Text
                    'Fin Jubilacion
                End If
                'prestamo
                If (c_tipPadron = "A" Or c_tipPadron = "E") Then
                    c_drProf.Item("afi_generarrenta") = CBGenerarRenta.Text
                    'GARANTE --------------------------------------
                    Dim garanteActualizar As Boolean = False
                    Dim garanteActualizarNulo As Boolean = False
                    'Si los campos no estan vacios entra
                    If Not (String.IsNullOrEmpty(TBGaranteTipdoc.Text) And String.IsNullOrEmpty(TBGaranteNrodoc.Text)) Then
                        'Si es dbnull
                        If IsDBNull(c_drProf.Item("afi_garante_tipdoc")) And IsDBNull(c_drProf.Item("afi_garante_nrodoc")) Then
                            garanteActualizar = True
                        Else
                            'Si el garante almacenado es distinto al agregado entra
                            If (c_drProf.Item("afi_garante_tipdoc") & c_drProf.Item("afi_garante_nrodoc")) <> (TBGaranteTipdoc.Text & TBGaranteNrodoc.Text) Then
                                garanteActualizar = True
                            End If
                        End If
                    ElseIf Not (IsDBNull(c_drProf.Item("afi_garante_tipdoc")) And IsDBNull(c_drProf.Item("afi_garante_nrodoc"))) Then
                        'Si entro es porque los campos estan vacios y antes tenian valores. Osea esta eliminando un garante.
                        garanteActualizarNulo = True
                    End If
                    If garanteActualizar Then
                        c_drProf.Item("afi_garante_tipdoc") = TBGaranteTipdoc.Text
                        c_drProf.Item("afi_garante_nrodoc") = TBGaranteNrodoc.Text
                        'traigo Garante y set Afiliado
                        GetAfiliado(TBGaranteTipdoc.Text, TBGaranteNrodoc.Text, "garante")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garantede_tipdoc") = c_drProf.Item("afi_tipdoc")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garantede_nrodoc") = c_drProf.Item("afi_nrodoc")
                        DAAfiliado.Update(DSAfiliado, "garante")
                    End If
                    If garanteActualizarNulo Then
                        'traiga Garante y set Nulo en garante de
                        GetAfiliado(c_drProf.Item("afi_garante_tipdoc"), c_drProf.Item("afi_garante_nrodoc"), "garante")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garantede_tipdoc") = DBNull.Value
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garantede_nrodoc") = DBNull.Value
                        'Set Nulo garante en actual
                        c_drProf.Item("afi_garante_tipdoc") = DBNull.Value
                        c_drProf.Item("afi_garante_nrodoc") = DBNull.Value
                        DAAfiliado.Update(DSAfiliado, "garante")
                    End If
                    'GARANTE DE -----------------------------------
                    garanteActualizar = False
                    garanteActualizarNulo = False
                    'Si los campos no estan vacios entra
                    If Not (String.IsNullOrEmpty(TBGarantedeTipdoc.Text) And String.IsNullOrEmpty(TBGarantedeNrodoc.Text)) Then
                        'Si es dbnull
                        If IsDBNull(c_drProf.Item("afi_garantede_tipdoc")) And IsDBNull(c_drProf.Item("afi_garantede_nrodoc")) Then
                            garanteActualizar = True
                        Else
                            'Si el garante de almacenado es distinto al agregado entra
                            If (c_drProf.Item("afi_garantede_tipdoc") & c_drProf.Item("afi_garantede_nrodoc")) <> (TBGarantedeTipdoc.Text & TBGarantedeNrodoc.Text) Then
                                garanteActualizar = True
                            End If
                        End If
                    ElseIf Not (IsDBNull(c_drProf.Item("afi_garantede_tipdoc")) And IsDBNull(c_drProf.Item("afi_garantede_nrodoc"))) Then
                        'Si entro es porque los campos estan vacios y antes tenian valores. Osea esta eliminando un garantede.
                        garanteActualizarNulo = True
                    End If
                    If garanteActualizar Then
                        c_drProf.Item("afi_garantede_tipdoc") = TBGarantedeTipdoc.Text
                        c_drProf.Item("afi_garantede_nrodoc") = TBGarantedeNrodoc.Text
                        'traiga Garantede y set Afiliado
                        GetAfiliado(TBGarantedeTipdoc.Text, TBGarantedeNrodoc.Text, "garantede")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garante_tipdoc") = c_drProf.Item("afi_tipdoc")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garante_nrodoc") = c_drProf.Item("afi_nrodoc")
                        DAAfiliado.Update(DSAfiliado, "garantede")
                    End If
                    If garanteActualizarNulo Then
                        'traiga Garantede y set Nulo en garante
                        GetAfiliado(c_drProf.Item("afi_garantede_tipdoc"), c_drProf.Item("afi_garantede_nrodoc"), "garantede")
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garante_tipdoc") = DBNull.Value
                        DSAfiliado.Tables(0).Rows(0).Item("afi_garante_nrodoc") = DBNull.Value
                        'Set Nulo garante de en actual
                        c_drProf.Item("afi_garantede_tipdoc") = DBNull.Value
                        c_drProf.Item("afi_garantede_nrodoc") = DBNull.Value
                        DAAfiliado.Update(DSAfiliado, "garantede")
                    End If
                End If
                'actualizo familiares
                If Not DTFamiliar Is Nothing Then
                    Dim validoFlia As Boolean = False
                    Dim nFlia As Integer = 0
                    For Each rowflia As DataRow In DTFamiliar.Rows
                        nFlia += 1
                        If rowflia.Item("fecnac").ToString.Length > 0 Then
                            dFecMysql.Day = rowflia.Item("fecnac").ToString.Substring(0, 2)
                            dFecMysql.Month = rowflia.Item("fecnac").ToString.Substring(3, 2)
                            dFecMysql.Year = rowflia.Item("fecnac").ToString.Substring(6, 4)

                            c_drProf.Item("afi_nac" & nFlia) = dFecMysql
                        Else
                            c_drProf.Item("afi_nac" & nFlia) = DBNull.Value
                        End If

                        If IsDBNull(rowflia.Item("Documento")) Then
                            c_drProf.Item("afi_doc" & nFlia) = ""
                        Else
                            If c_drProf.Item("afi_doc" & nFlia) = "" Then
                                c_drProf.Item("afi_doc" & nFlia) = rowflia.Item("Documento")
                            ElseIf c_drProf.Item("afi_doc" & nFlia) = 0 Then
                                rowflia.Item("Documento") = ""
                                rowflia.Item("Nombre") = ""
                                rowflia.Item("sexo") = ""
                                rowflia.Item("fil") = ""
                                rowflia.Item("beneficiario") = ""
                                rowflia.Item("fecNac") = ""
                                c_drProf.Item("afi_doc" & nFlia) = rowflia.Item("Documento")
                                c_drProf.Item("afi_aut" & nFlia) = rowflia.Item("Nombre")
                                c_drProf.Item("afi_sex" & nFlia) = rowflia.Item("sexo")
                                c_drProf.Item("afi_fil" & nFlia) = rowflia.Item("fil")
                                c_drProf.Item("afi_beneficiario" & nFlia) = rowflia.Item("beneficiario")
                                c_drProf.Item("afi_nac" & nFlia) = DBNull.Value
                            Else
                                c_drProf.Item("afi_doc" & nFlia) = rowflia.Item("Documento")
                            End If
                        End If

                        If IsDBNull(rowflia.Item("Nombre")) Then
                            c_drProf.Item("afi_aut" & nFlia) = ""
                        Else
                            c_drProf.Item("afi_aut" & nFlia) = rowflia.Item("Nombre")
                        End If

                        If IsDBNull(rowflia.Item("sexo")) Then
                            c_drProf.Item("afi_sex" & nFlia) = ""
                        Else
                            c_drProf.Item("afi_sex" & nFlia) = rowflia.Item("sexo")
                        End If

                        If IsDBNull(rowflia.Item("fil")) Then
                            c_drProf.Item("afi_fil" & nFlia) = ""
                        Else
                            c_drProf.Item("afi_fil" & nFlia) = rowflia.Item("fil")
                        End If

                        c_drProf.Item("afi_beneficiario" & nFlia) = rowflia.Item("beneficiario")
                        'Siempre que se elija "Solo beneficiario" como opcion de afiliado se guardara por defecto que es beneficiario
                        If c_drProf.Item("afi_fil" & nFlia) = "4" Then
                            c_drProf.Item("afi_beneficiario" & nFlia) = "Si"
                        End If

                        'Validacion
                        'chequeo que todos esten vacios sino entra en el else
                        If c_drProf.Item("afi_doc" & nFlia) = "" And
                            c_drProf.Item("afi_aut" & nFlia) = "" And
                            (
                                c_drProf.Item("afi_nac" & nFlia).ToString.Length = 0 Or
                                c_drProf.Item("afi_nac" & nFlia).ToString = "00/00/0000 00:00:00"
                            ) And
                            c_drProf.Item("afi_sex" & nFlia) = "" And
                            c_drProf.Item("afi_fil" & nFlia) = "" And
                            c_drProf.Item("afi_beneficiario" & nFlia).ToString.Length = 0 Then
                            'true
                        Else
                            'chequeo si algunos de esos 4 campos al menos tiene valor o se quiso setear
                            If c_drProf.Item("afi_doc" & nFlia) = "" Or
                                c_drProf.Item("afi_aut" & nFlia) = "" Or
                                (
                                    c_drProf.Item("afi_nac" & nFlia).ToString.Length = 0 OrElse
                                    c_drProf.Item("afi_nac" & nFlia).ToString = "00/00/0000 00:00:00"
                                ) Or
                                c_drProf.Item("afi_sex" & nFlia) = "" Or
                                c_drProf.Item("afi_fil" & nFlia) = "" Or
                                c_drProf.Item("afi_beneficiario" & nFlia).ToString.Length = 0 Then

                                ActualizoDatosProfesional = False
                                MessageBox.Show("Todos los datos del Familiar y/o Beneficiario de la fila:  " & nFlia & " deben rellenarse.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            End If
                        End If
                    Next
                End If

                If ActualizoDatosProfesional Then
                    'Si es l_alta true tengo que hacer el Add es new
                    If l_alta Then
                        c_dsProf.Tables(0).Rows.Add(c_drProf)
                        'CREO EL USUARIO WEB SOLO SI ES ALTA Y TIPO A
                        If c_tipPadron = "A" Then
                            Dim taUsuario As DSUsuarioTableAdapters.fos_userTableAdapter = New DSUsuarioTableAdapters.fos_userTableAdapter()
                            Dim dsUsuario As DSUsuario = New DSUsuario()
                            Dim rowUsuario As DataRow = dsUsuario.fos_user.NewRow()
                            rowUsuario.Item("username") = c_drProf.Item("afi_nrodoc")
                            rowUsuario.Item("username_canonical") = c_drProf.Item("afi_nrodoc")
                            rowUsuario.Item("email") = c_drProf.Item("afi_nrodoc")
                            rowUsuario.Item("email_canonical") = c_drProf.Item("afi_nrodoc")
                            rowUsuario.Item("enabled") = False
                            rowUsuario.Item("salt") = False
                            rowUsuario.Item("password") = False
                            rowUsuario.Item("locked") = 0
                            rowUsuario.Item("expired") = 0
                            rowUsuario.Item("roles") = "a:0:{}"
                            rowUsuario.Item("credentials_expired") = 0
                            rowUsuario.Item("usr_tipdoc") = c_drProf.Item("afi_tipdoc")
                            rowUsuario.Item("usr_nrodoc") = c_drProf.Item("afi_nrodoc")
                            dsUsuario.fos_user.Rows.Add(rowUsuario)
                            taUsuario.Adapter.Update(dsUsuario)
                        End If
                    End If
                    'Es Edit
                    c_daProf.Update(c_dsProf, "profesional")
                    miTrans.Commit()
                    l_Actualizacion = True
                    'Si es Edit
                    If Not l_alta Then
                        c_dsProf.Clear()
                    End If
                    'Alta
                    c_daProf.Fill(c_dsProf, "profesional")
                    'Si es Edit
                    If Not l_alta Then
                        c_drProf = c_dsProf.Tables(0).Rows(n_nRow)
                    End If
                    mostrarMensajeExitoso(message)
                    FrmFichaProfesionales_Load(Me, New EventArgs)
                Else
                    miTrans.Rollback()
                    MessageBox.Show("Controlar los datos ingresados.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
                c_cnn.CerrarConexion()
            Catch ex As MySqlException
                ActualizoDatosProfesional = False
                miTrans.Rollback()
                c_dsProf.RejectChanges()
                c_cnn.CerrarConexion()
                If ex.Message.StartsWith("Duplicate entry") Then
                    If l_alta Then
                        MessageBox.Show("Ya existe el DNI.")
                    Else
                        MessageBox.Show("Ya existe el Profesional como Garante o Garante De.")
                    End If
                Else
                    MessageBox.Show(ex.Message)
                End If
                'reload datos
                'c_dsProf.Clear()
                'c_daProf.Fill(c_dsProf, "profesional")
                'reload form
                FrmFichaProfesionales_Load(Me, New EventArgs)
            Catch ex As Exception
                ActualizoDatosProfesional = False
                miTrans.Rollback()
                c_dsProf.RejectChanges()
                If ex.Message.StartsWith("Infracción de simultaneidad") Then
                    MessageBox.Show("Ya existe el DNI, el Profesional como Garante o Garante De.")
                Else
                    MessageBox.Show(ex.Message)
                End If
                c_cnn.CerrarConexion()
                'reload datos
                'c_dsProf.Clear()
                'c_daProf.Fill(c_dsProf, "profesional")
                'reload form
                FrmFichaProfesionales_Load(Me, New EventArgs)
            End Try
        End If

        Return ActualizoDatosProfesional
    End Function

    Private Sub CargaTitulo()
        Dim dsTitulo As New DataSet
        Dim daTitulo As MySqlDataAdapter

        daTitulo = c_cnn.consultaBDadapter("titulos")
        daTitulo.Fill(dsTitulo, "titulo")
        cbTitulo.DataSource = dsTitulo.Tables(0)
        cbTitulo.DisplayMember = dsTitulo.Tables(0).Columns("tit_descripcion").ToString
        cbTitulo.ValueMember = dsTitulo.Tables(0).Columns("tit_idtitulo").ToString
    End Sub

    Private Sub CargaCategoria()
        Dim dsCategoria As New DataSet
        Dim daCategoria As MySqlDataAdapter

        daCategoria = c_cnn.consultaBDadapter("categorias")
        daCategoria.Fill(dsCategoria, "categoria")
        cbCategoria.DataSource = dsCategoria.Tables(0)
        cbCategoria.DisplayMember = dsCategoria.Tables(0).Columns("cat_descrip").ToString
        cbCategoria.ValueMember = dsCategoria.Tables(0).Columns("cat_codigo").ToString
    End Sub

    Private Sub CargaObraSocial(Optional ByVal index As Integer = 0)
        Dim dsObraSocial As New DataSet
        Dim daObraSocial As MySqlDataAdapter

        daObraSocial = c_cnn.consultaBDadapter("obrasocial", "*", "obr_activo = 1 ORDER BY obr_nombre")
        daObraSocial.Fill(dsObraSocial, "obrasocial")
        dsObraSocial.Tables(0).Rows.Add(0, "Ninguno", 1)
        cbObraSocial.DataSource = dsObraSocial.Tables(0)
        cbObraSocial.DisplayMember = dsObraSocial.Tables(0).Columns("obr_nombre").ToString
        cbObraSocial.ValueMember = dsObraSocial.Tables(0).Columns("id").ToString
        'valor por defecto seleccionado
        cbObraSocial.SelectedValue = index
    End Sub

    Private Sub CargoMatriculas()
        Try
            DAMatricula = c_cnn.consultaBDadapter("cuentas",
                "cue_titulo,cue_matricula as Matricula,
                 cue_fecgraduacion as Graduacion,
                 cue_fecdiploma as Diploma,
                 cue_fecmatricula as Matriculado,
                 cue_fecficha as Ficha,
                 cue_categoria as cat,
                 cue_titulo,cue_tipdoc,
                 cue_nrodoc",
                "cue_tipdoc='" & c_drProf.Item("afi_tipdoc") & "' and cue_nrodoc=" & c_drProf.Item("afi_nrodoc") & " ORDER BY CASE WHEN cue_categoria=200000 THEN 2 ELSE 1 END, cue_fecficha"
            )
            cmdMatricula = New MySqlCommandBuilder(DAMatricula)
            DSMatricula = New DataSet
            DAMatricula.Fill(DSMatricula, "matricula")
            bindingCuenta.DataSource = DSMatricula.Tables(0)
            BindingNavigator1.BindingSource = bindingCuenta
            bindingBase = BindingContext(DSMatricula, "matricula")
            If DSMatricula.Tables(0).Rows.Count > 0 Then
                MuestraMatricula(0)
                bindingCuenta.Position = 0
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MuestraMatricula(ByVal nrow As Integer)
        If nrow < 0 Then
            'With UDTgraduacion
            '    .Format = DateTimePickerFormat.Custom
            '    .CustomFormat = " "
            '    .Value.ToString("")
            'End With
            'With dtpDiploma
            '    .Format = DateTimePickerFormat.Custom
            '    .CustomFormat = " "
            '    .Value.ToString("")
            'End With
            'With dtpMatricula
            '    .Format = DateTimePickerFormat.Custom
            '    .CustomFormat = " "
            '    .Value.ToString("")
            'End With
            'With dtpficha
            '    .Format = DateTimePickerFormat.Custom
            '    .CustomFormat = " "
            '    .Value.ToString("")
            'End With

            cbTitulo.Text = ""
            cbCategoria.Text = ""
            'cbObraSocial.Text = ""
        Else
            txMatricula.Text = DSMatricula.Tables(0).Rows(nrow).Item("matricula").ToString
            If Not IsDate(DSMatricula.Tables(0).Rows(nrow).Item("Graduacion").ToString) Then
                UDTgraduacion.Text = ""
            Else
                UDTgraduacion.Text = DSMatricula.Tables(0).Rows(nrow).Item("Graduacion").ToString
            End If

            If Not IsDate(DSMatricula.Tables(0).Rows(nrow).Item("Diploma").ToString) Then
                UDTdiploma.Text = ""
            Else
                UDTdiploma.Text = DSMatricula.Tables(0).Rows(nrow).Item("Diploma").ToString
            End If
            If Not IsDate(DSMatricula.Tables(0).Rows(nrow).Item("Matriculado").ToString) Then
                UDTmatricula.Text = ""
            Else
                UDTmatricula.Text = DSMatricula.Tables(0).Rows(nrow).Item("Matriculado").ToString
            End If

            If Not IsDate(DSMatricula.Tables(0).Rows(nrow).Item("Ficha").ToString) Then
                UDTficha.Text = ""
            Else
                UDTficha.Text = DSMatricula.Tables(0).Rows(nrow).Item("Ficha").ToString
            End If
            cbTitulo.SelectedValue = DSMatricula.Tables(0).Rows(nrow).Item("cue_titulo")
            cbCategoria.SelectedValue = DSMatricula.Tables(0).Rows(nrow).Item("cat").ToString
        End If
    End Sub

    Private Sub FrmFichaProfesionales_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        For Each frm As Form In formularios
            frm.Close()
        Next
    End Sub

    Private Sub CargaFamiliar()
        Dim RowFlia As DataRow
        Dim valSex As New Infragistics.Win.ValueList
        Dim valPar As New Infragistics.Win.ValueList
        Dim valBen As New Infragistics.Win.ValueList

        valSex.ValueListItems.Add("F", "Femenino")
        valSex.ValueListItems.Add("M", "Masculino")

        valPar.ValueListItems.Add("0", "Esposo/a")
        valPar.ValueListItems.Add("1", "Hijo/a")
        valPar.ValueListItems.Add("2", "Tutela")
        valPar.ValueListItems.Add("3", "Conviviente")
        valPar.ValueListItems.Add("4", "Solo Beneficiario")

        valBen.ValueListItems.Add("No", "No")
        valBen.ValueListItems.Add("Si", "Si")
        valBen.ValueListItems.Add("Ambos", "Ambos")

        DTFamiliar = New DataTable
        DTFamiliar.Columns.Add("Documento", GetType(String))
        DTFamiliar.Columns.Add("Nombre", GetType(String))
        DTFamiliar.Columns.Add("fecNac", GetType(String))
        DTFamiliar.Columns.Add("Sexo", GetType(String))
        DTFamiliar.Columns.Add("Fil", GetType(String))
        DTFamiliar.Columns.Add("Beneficiario", GetType(String))
        For x As Integer = 1 To 6
            RowFlia = DTFamiliar.NewRow
            RowFlia.Item("documento") = c_drProf.Item("afi_doc" & x)
            RowFlia.Item("Nombre") = c_drProf.Item("afi_aut" & x)
            RowFlia.Item("FecNac") = c_drProf.Item("afi_nac" & x)
            RowFlia.Item("Sexo") = c_drProf.Item("afi_sex" & x)
            RowFlia.Item("Fil") = c_drProf.Item("afi_fil" & x)
            RowFlia.Item("Beneficiario") = c_drProf.Item("afi_beneficiario" & x)
            DTFamiliar.Rows.Add(RowFlia)
        Next
        UGFlia.DataSource = DTFamiliar
        With UGFlia.DisplayLayout.Bands(0)
            .Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False
            .Columns(0).Width = 70
            .Columns(0).Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Integer
            .Columns(1).Width = 210
            .Columns(1).Header.Caption = "Apellido y Nombre"
            .Columns(2).Width = 110
            .Columns(2).Header.Caption = "Fecha de Nac."
            .Columns(2).Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Date
            .Columns(3).Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList
            .Columns(3).ValueList = valSex
            .Columns(3).Width = 75
            .Columns(4).Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList
            .Columns(4).ValueList = valPar
            .Columns(4).Width = 110
            .Columns(4).Header.Caption = "Miembro"
            .Columns(5).Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList
            .Columns(5).ValueList = valBen
        End With
    End Sub

    Private Sub BindingNavigatorMoveNextItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BindingNavigatorMoveNextItem.Click
        bindingBase.Position += 1
        MuestraMatricula(bindingBase.Position)
    End Sub

    Private Sub BindingNavigatorMovePreviousItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BindingNavigatorMovePreviousItem.Click
        bindingBase.Position -= 1
        MuestraMatricula(bindingBase.Position)
    End Sub

    Private Sub BindingNavigatorMoveLastItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BindingNavigatorMoveLastItem.Click
        bindingBase.Position = DSMatricula.Tables(0).Rows.Count
        MuestraMatricula(bindingBase.Position)
    End Sub

    Private Sub BindingNavigatorMoveFirstItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BindingNavigatorMoveFirstItem.Click
        MuestraMatricula(0)
    End Sub

    Private Sub ToolStripButtonEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripButtonEdit.Click
        'Controlo accion Titulos permiso Update
        If controlAcceso(1, 10, "U", True) = True Then
            EditarTitulo(True)
            ToolStripButtonSave.Enabled = True
        End If
    End Sub

    Private Sub ToolStripButtonAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripButtonAdd.Click
        'Controlo accion Titulos permiso Create
        If controlAcceso(1, 10, "C", True) = True Then
            dFecMysql.Day = Today.Day
            dFecMysql.Month = Today.Month
            dFecMysql.Year = Today.Year
            DSMatricula.Tables(0).Rows.Add()
            DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("cue_tipdoc") = c_drProf.Item("afi_tipdoc")
            DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("cue_nrodoc") = c_drProf.Item("afi_nrodoc")
            '   DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("Graduacion") = dFecMysql
            ''   DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("Matriculado") = dFecMysql
            '  DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("Diploma") = dFecMysql
            '  DSMatricula.Tables(0).Rows(DSMatricula.Tables(0).Rows.Count - 1).Item("Ficha") = dFecMysql
            BindingNavigatorMoveLastItem.PerformClick()

            MuestraMatricula(bindingBase.Position)
            '  CargoSubCuenta(bindingBase.Position)
            EditarTitulo(True)

            cbTitulo.Enabled = True
            ToolStripButtonSave.Enabled = True
            ' txMatricula.Enabled = True
            ' txMatricula.Text = 0
            cbTitulo.Focus()
        End If
    End Sub

    Private Sub ToolStripButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripButtonSave.Click
        'Controlo accion Titulos permiso Create o Update
        If controlAcceso(1, 10, "CU", True) = True Then

            'Validacion
            Dim check As Boolean = True
            If validaCampo(cbTitulo) Then
                check = False
            End If
            If validaCampo(txMatricula) Then
                check = False
            End If
            If validaCampo(UDTgraduacion) Then
                check = False
            End If
            If validaCampo(UDTmatricula) Then
                check = False
            End If
            If validaCampo(cbCategoria) Then
                check = False
            End If
            If validaCampo(cbObraSocial) Then
                check = False
            End If
            If check = False Then
                Exit Sub
            End If

            'Gracuacion Fecha
            If IsDate(UDTgraduacion.Text) Then
                dFecMysqlGraduacion.Day = UDTgraduacion.Text.Substring(0, 2)
                dFecMysqlGraduacion.Month = UDTgraduacion.Text.Substring(3, 2)
                dFecMysqlGraduacion.Year = UDTgraduacion.Text.Substring(6, 4)
            Else
                dFecMysqlGraduacion.Day = "00"
                dFecMysqlGraduacion.Month = "00"
                dFecMysqlGraduacion.Year = "0000"
            End If
            'Matricula Fecha
            If IsDate(UDTmatricula.Text) Then
                dFecMysqlMatricula.Day = UDTmatricula.Text.Substring(0, 2)
                dFecMysqlMatricula.Month = UDTmatricula.Text.Substring(3, 2)
                dFecMysqlMatricula.Year = UDTmatricula.Text.Substring(6, 4)
            Else
                dFecMysqlMatricula.Day = "00"
                dFecMysqlMatricula.Month = "00"
                dFecMysqlMatricula.Year = "0000"
            End If
            'Diploma Fecha
            If IsDate(UDTdiploma.Text) Then
                dFecMysqlDiploma.Day = UDTdiploma.Text.Substring(0, 2)
                dFecMysqlDiploma.Month = UDTdiploma.Text.Substring(3, 2)
                dFecMysqlDiploma.Year = UDTdiploma.Text.Substring(6, 4)
            Else
                dFecMysqlDiploma.Day = "00"
                dFecMysqlDiploma.Month = "00"
                dFecMysqlDiploma.Year = "0000"
            End If
            'Ficha Fecha
            If IsDate(UDTficha.Text) Then
                dFecMysqlFicha.Day = UDTficha.Text.Substring(0, 2)
                dFecMysqlFicha.Month = UDTficha.Text.Substring(3, 2)
                dFecMysqlFicha.Year = UDTficha.Text.Substring(6, 4)
            Else
                dFecMysqlFicha.Day = "00"
                dFecMysqlFicha.Month = "00"
                dFecMysqlFicha.Year = "0000"
            End If

            If isNotValidCamposTitulo() Then
                Exit Sub
            End If

            EditarTitulo(False)
            cbTitulo.Enabled = False
            ToolStripButtonSave.Enabled = False

            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Graduacion") = dFecMysqlGraduacion
            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Matriculado") = dFecMysqlMatricula
            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Diploma") = dFecMysqlDiploma
            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Ficha") = dFecMysqlFicha

            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("cue_titulo") = cbTitulo.SelectedValue
            DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Cat") = cbCategoria.SelectedValue

            Dim DASubCuenta As MySqlDataAdapter
            Dim cmdSubCuenta As New MySqlCommandBuilder
            Dim DTSubCuenta As New DataTable

            DASubCuenta = c_cnn.consultaBDadapter("subcuenta", "scu_tipdoc,scu_nrodoc,scu_titulo,scu_matricula,scu_catant,scu_categoria,scu_vigencia,scu_fecha,scu_nota1,scu_nroope", "scu_tipdoc='" & c_drProf.Item("afi_tipdoc") & "' AND scu_nrodoc=" & c_drProf.Item("afi_nrodoc") & " ORDER BY scu_fecha DESC")
            cmdSubCuenta = New MySqlCommandBuilder(DASubCuenta)
            DASubCuenta.Fill(DTSubCuenta)
            If c_cnn.AbrirConexion Then
                Dim countCategoriaNoActivo As Integer = 0
                For Each row As DataRow In DSMatricula.Tables(0).Rows
                    If (row.Item("cat") <> 200000 And Mid(row.Item("cat"), 1, 2) <> 22) Then
                        countCategoriaNoActivo += 1
                    End If
                Next

                If countCategoriaNoActivo < 2 Then
                    Dim miTrans As MySqlTransaction
                    miTrans = c_cnn.InicioTransaccion()
                    Try
                        If bindingBase.Position = 0 Then
                            Dim lActScta As Boolean = False
                            If DTSubCuenta.Rows.Count > 0 Then
                                If DTSubCuenta.Rows(0).Item("scu_categoria") <> cbCategoria.SelectedValue Then
                                    lActScta = True
                                End If
                            Else
                                lActScta = True
                            End If
                            If lActScta Then
                                'Muestro input para agregar comentario
                                Dim nota1 As String = " "
                                While (nota1 = " " Or String.IsNullOrWhiteSpace(nota1))
                                    nota1 = InputBox("Ingrese comentario por cambio de categoría.", "Cambio de categoría", " ")
                                    If nota1 = "" Then
                                        MessageBox.Show("No se guardaron los cambios.")
                                        miTrans.Rollback()
                                        c_cnn.CerrarConexion()
                                        cbCategoria.SelectedValue = DTSubCuenta.Rows(0).Item("scu_categoria")
                                        Exit Sub
                                    End If
                                End While
                                'If MsgBox("Desea almacenar la modificación en el historial de categorías?", vbYesNo + vbQuestion, "Comentario") = MsgBoxResult.Yes Then
                                Dim rowSubcta As DataRow = DTSubCuenta.NewRow
                                dFecMysql.Day = Now.Day
                                dFecMysql.Month = Now.Month
                                dFecMysql.Year = Now.Year
                                rowSubcta.Item("scu_tipdoc") = c_drProf.Item("afi_tipdoc")
                                rowSubcta.Item("scu_nrodoc") = c_drProf.Item("afi_nrodoc")
                                rowSubcta.Item("scu_titulo") = c_drProf.Item("afi_titulo")
                                rowSubcta.Item("scu_matricula") = c_drProf.Item("afi_matricula")
                                rowSubcta.Item("scu_catant") = c_drProf.Item("afi_categoria")
                                rowSubcta.Item("scu_categoria") = cbCategoria.SelectedValue
                                rowSubcta.Item("scu_vigencia") = dFecMysql
                                rowSubcta.Item("scu_nota1") = nota1
                                rowSubcta.Item("scu_nroope") = nPubNroOperador
                                dFecMysql.Hour = Now.Hour
                                dFecMysql.Minute = Now.Minute
                                dFecMysql.Second = Now.Second
                                rowSubcta.Item("scu_fecha") = dFecMysql
                                DTSubCuenta.Rows.Add(rowSubcta)
                                DASubCuenta.Update(DTSubCuenta)
                                'End If
                            End If
                            c_drProf.Item("afi_matricula") = txMatricula.Text
                            c_drProf.Item("afi_titulo") = cbTitulo.SelectedValue
                            c_drProf.Item("afi_categoria") = cbCategoria.SelectedValue
                            c_drProf.Item("afi_obrasocial") = cbObraSocial.SelectedValue
                            c_daProf.Update(c_dsProf, "profesional")
                            c_dsProf.Clear()
                            c_daProf.Fill(c_dsProf, "profesional")
                            c_drProf = c_dsProf.Tables(0).Rows(n_nRow)

                            l_Actualizacion = True
                        End If
                        DSMatricula.Tables(0).Rows(bindingBase.Position).Item("Matricula") = txMatricula.Text
                        DAMatricula.Update(DSMatricula, "matricula")
                        miTrans.Commit()
                    Catch ex As Exception
                        MessageBox.Show("No se pudo actualizar, se produjo el siguiente error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        miTrans.Rollback()
                    End Try
                Else
                    MessageBox.Show("Error al actualizar, solo debe tener una categoría activa.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
                CargoMatriculas()
                MuestraMatricula(0)
                c_cnn.CerrarConexion()
            End If
        End If
    End Sub
    Private Sub CButtonEditar_Click(sender As Object, e As EventArgs) Handles CButtonEditar.Click
        'Controlo permiso Update
        If controlAcceso(1, c_accion, "U", True) = True Then
            CButtonActualizar.Enabled = True
            EditarProfesional(True)
            CargaObraSocial(cbObraSocial.SelectedValue)
        End If
    End Sub
    Private Sub CButtonActualizar_Click(sender As Object, e As EventArgs) Handles CButtonActualizar.Click
        'Controlo permiso Create o Update
        If controlAcceso(1, c_accion, "CU", True) = True Then
            If ActualizoDatosProfesional() Then
                CButtonActualizar.Enabled = False
                EditarProfesional(False)
                'Si era un alta cambio a false
                l_alta = False
            End If
        End If
    End Sub
    Private Sub CBtCtaCte_Click(sender As Object, e As EventArgs) Handles CBtCtaCte.Click
        'Dim FrmCuenta As New FrmCtaCte(c_cnn, c_drProf.Item("afi_tipdoc"), c_drProf.Item("afi_nrodoc"), c_drProf.Item("afi_titulo"), c_drProf.Item("afi_matricula"))
        Dim FrmCuenta As New FrmCtaCte(c_cnn, c_drProf)
        formularios.Add(FrmCuenta)
        FrmCuenta.Show()
    End Sub
    Private Sub CBtPago_Click(sender As Object, e As EventArgs) Handles CBtPago.Click
        'Controlo permiso Liquidacion Honorario
        If controlAcceso(1, 7, , True) = True Then
            Dim FrmLiquidacion As New FrmLiquidacionHonorarios(c_drProf)
            formularios.Add(FrmLiquidacion)
            FrmLiquidacion.UCEganancias.Text = cbGanancia.Text
            FrmLiquidacion.UDTdgi.Value = MtxExcepDGI.Text
            FrmLiquidacion.UDTdgr.Value = MtxExcepDGR.Text
            FrmLiquidacion.UDTiva.Value = MtxExcepIVA.Text
            FrmLiquidacion.Show()
        End If
    End Sub
    Private Sub cbtOrdenPago_Click(sender As Object, e As EventArgs) Handles cbtOrdenPago.Click
        'Controlo permiso Orden de Pago
        If controlAcceso(1, 8, , True) = True Then
            Dim FrmOrden As New FrmOrdenPago(c_cnn, c_drProf.Item("afi_tipdoc"), c_drProf.Item("afi_nrodoc"), c_drProf.Item("afi_titulo"), c_drProf.Item("afi_matricula"), c_drProf.Item("afi_mail"))
            formularios.Add(FrmOrden)
            FrmOrden.Show()
        End If
    End Sub
    Private Sub CButton3_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton3.Click
        'Controlo permiso Moratoria
        If controlAcceso(1, 9, , True) = True Then
            Dim FrmMora As New FrmVeoMoratorias(c_drProf)
            formularios.Add(FrmMora)
            FrmMora.Show()
        End If
    End Sub

    Private Sub UTEGaranteNumdoc_EditorButtonClick(sender As Object, e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTEGaranteNumdoc.EditorButtonClick
        'If los valores ingresados en garante son diferentes a los seteados en garantede
        If String.IsNullOrWhiteSpace(TBGarantedeTipdoc.Text & TBGarantedeNrodoc.Text) Or ((CBGaranteTipdoc.SelectedItem & UTEGaranteNumdoc.Value) <> (TBGarantedeTipdoc.Text & TBGarantedeNrodoc.Text)) Then
            Dim afiliadoArray = GetAfiliadoArray(CBGaranteTipdoc.SelectedItem, UTEGaranteNumdoc.Value)
            If Not IsNothing(afiliadoArray) Then
                TBGarante.Text = afiliadoArray.Item("afi_nombre")
                TBGaranteTipdoc.Text = afiliadoArray.Item("afi_tipdoc")
                TBGaranteNrodoc.Text = afiliadoArray.Item("afi_nrodoc")
            Else
                TBGarante.Text = ""
                TBGaranteTipdoc.Text = ""
                TBGaranteNrodoc.Text = ""
            End If
        Else
            MessageBox.Show("No se puede seleccionar el mismo profesional")
        End If
    End Sub

    Private Sub UTEGarantedeNumdoc_EditorButtonClick(sender As Object, e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UTEGarantedeNumdoc.EditorButtonClick
        'If los valores ingresados en garantede son diferentes a los seteados en garante
        If String.IsNullOrEmpty(TBGaranteTipdoc.Text & TBGaranteNrodoc.Text) Or ((CBGarantedeTipdoc.SelectedItem & UTEGarantedeNumdoc.Value) <> (TBGaranteTipdoc.Text & TBGaranteNrodoc.Text)) Then
            Dim afiliadoArray = GetAfiliadoArray(CBGarantedeTipdoc.SelectedItem, UTEGarantedeNumdoc.Value)
            If Not IsNothing(afiliadoArray) Then
                TBGarantede.Text = afiliadoArray.Item("afi_nombre")
                TBGarantedeTipdoc.Text = afiliadoArray.Item("afi_tipdoc")
                TBGarantedeNrodoc.Text = afiliadoArray.Item("afi_nrodoc")
            Else
                TBGarantede.Text = ""
                TBGarantedeTipdoc.Text = ""
                TBGarantedeNrodoc.Text = ""
            End If
        Else
            MessageBox.Show("No se puede seleccionar el mismo profesional")
        End If
    End Sub
    'Devuelve el afiliado en el DataSet DSAfiliado
    Private Sub GetAfiliado(ByVal afi_tipdoc As String, ByVal afi_nrodoc As String, Optional ByVal tipo As String = "afiliado")
        Dim campos As String = "
            afi_nombre,
            afi_titulo,
            afi_matricula,
            afi_fecnac,
            afi_tipdoc,
            afi_nrodoc,
            afi_direccion,
            afi_localidad,
            afi_provincia,
            afi_zona,
            afi_codpos,
            afi_telefono1,
            afi_telefono2,
            afi_mail,
            afi_mail_alternativo,
            afi_civil,
            afi_cuit,
            afi_dgr,
            afi_sexo,
            afi_garante,
            afi_garante_tipdoc,
            afi_garante_nrodoc,
            afi_garantede_tipdoc,
            afi_garantede_nrodoc,
            afi_tipo,afi_ganancias,
            afi_categoria,
            afi_dgiexcep,
            afi_dgrexcep,
            afi_ivaexcep,
            afi_doc1,afi_aut1,afi_nac1,afi_sex1,afi_fil1,
            afi_doc2,afi_aut2,afi_nac2,afi_sex2,afi_fil2,
            afi_doc3,afi_aut3,afi_nac3,afi_sex3,afi_fil3,
            afi_doc4,afi_aut4,afi_nac4,afi_sex4,afi_fil4,
            afi_doc5,afi_aut5,afi_nac5,afi_sex5,afi_fil5,
            afi_doc6,afi_aut6,afi_nac6,afi_sex6,afi_fil6,
            afi_cbu,
            afi_cbu_credito,
            afi_deb_tuya,
            afi_deb_visa,
            afi_deb_mastercard,
            afi_deb_naranja,
            afi_generarrenta,
            afi_fecha_jubilacion,
            afi_jubi_nro_resolucion,
            afi_jubi_edad_resolucion,
            afi_jubi_periodo_opto,
            afi_jubi_coeficiente,
            afi_jubi_capitalizacion,
            afi_jubi_fondo_compensador,
            afi_jubi_abona_cuota,
            afi_jubi_observacion,
            afi_jubi_importe_cuota,
            afi_jubi_periodos_cobrados,
            afi_jubi_periodos_restantes,
            afi_obrasocial,
            afi_fecha_vencimiento_et"
        Dim condiciones As String = "afi_tipdoc='" & afi_tipdoc & "' AND afi_nrodoc='" & afi_nrodoc & "'"
        DAAfiliado = c_cnn.consultaBDadapter("afiliado", campos, condiciones)
        CMDAfiliado = New MySqlCommandBuilder(DAAfiliado)
        DSAfiliado = New DataSet
        DAAfiliado.Fill(DSAfiliado, tipo)
    End Sub
    'Devuelve el afiliado como Array
    Private Function GetAfiliadoArray(ByVal afi_tipdoc As String, ByVal afi_nrodoc As String)
        GetAfiliado(afi_tipdoc, afi_nrodoc)
        Dim afiliadoArray = (From row In DSAfiliado.Tables(0) Where (row("afi_tipdoc").ToString = afi_tipdoc And row("afi_nrodoc") = afi_nrodoc)).ToArray
        If afiliadoArray.Count() > 0 Then
            Return afiliadoArray(0)
        Else
            Return Nothing
        End If
    End Function
    'Metodo utilizado para validar los campos de fechas de los titulos
    Private Function isNotValidCamposTitulo() As Boolean
        Dim resDiff As Long

        If IsDate(UDTgraduacion.Text) And IsDate(UDTdiploma.Text) Then
            resDiff = DateDiff(DateInterval.Day, dFecMysqlGraduacion.Value, dFecMysqlDiploma.Value)
            If resDiff < 0 Then
                UDTdiploma.ForeColor = Color.Red
                MessageBox.Show("La fecha del diploma debe ser posterior a la de graduación", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If
        End If
        UDTdiploma.ForeColor = Color.Black

        If IsDate(UDTdiploma.Text) And IsDate(UDTmatricula.Text) Then
            resDiff = DateDiff(DateInterval.Day, dFecMysqlDiploma.Value, dFecMysqlMatricula.Value)
            If resDiff < 0 Then
                UDTmatricula.ForeColor = Color.Red
                MessageBox.Show("La fecha de matricula debe ser posterior a la de diploma", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If
        End If
        UDTmatricula.ForeColor = Color.Black

        If IsDate(UDTmatricula.Text) And IsDate(UDTficha.Text) Then
            resDiff = DateDiff(DateInterval.Day, dFecMysqlMatricula.Value, dFecMysqlFicha.Value)
            If resDiff < 0 Then
                UDTficha.ForeColor = Color.Red
                MessageBox.Show("La fecha de ficha debe ser posterior a la de matricula", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If
        End If
        UDTficha.ForeColor = Color.Black

        Return False
    End Function

    Private Sub TxApellido_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxApellido.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteProfesional.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxNombre_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxNombre.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteProfesional.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxDireccion_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxDireccion.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteProfesional.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxLocalidad_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxLocalidad.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteProfesional.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Function validaCampo(ByVal sender As Object) As Boolean
        Dim campoNombre As String = sender.Name
        If String.IsNullOrWhiteSpace(sender.Text) Then
            campoNombre = sender.Name.Remove(0, 2)
            MessageBox.Show("Debe cargar " + campoNombre, "Dato requerido", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return True
            Exit Function
        ElseIf campoNombre.Substring(0, 3) = "UDT" Or campoNombre.Substring(0, 3) = "Mtx" Then
            If sender.MaskCompleted = False Then
                campoNombre = sender.Name.Remove(0, 3)
                MessageBox.Show("Debe Elegir " + campoNombre, "Dato requerido", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
                Exit Function
            End If
        End If

        Return False
    End Function

    Private Function cuentaNumerosMask(ByVal mask As MaskedTextBox) As Integer
        Dim count As Integer = 0
        For Each pos As Char In mask.Text
            If IsNumeric(pos) Then
                count += 1
            End If
        Next
        Return count
    End Function
End Class