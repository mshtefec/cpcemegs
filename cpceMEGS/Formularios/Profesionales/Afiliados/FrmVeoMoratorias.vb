﻿Imports MySql.Data.MySqlClient
Public Class FrmVeoMoratorias
    Private cnn As New ConsultaBD(True)
    Private DTmoratoria As DataTable
    Private DTmoratoriaCargadas As DataTable
    Private DAmoratoria As MySqlDataAdapter
    Private r_Matricula As DataRow
    Dim clsExportarExcel As New ExportarExcel
    Public Sub New(ByVal RowMatricula As DataRow)
        InitializeComponent()
        r_Matricula = RowMatricula
    End Sub
    Private Sub MostrarMoratoriasCargadas()
        DAmoratoria = cnn.consultaBDadapter("cuo_det", , "matricula=" & r_Matricula.Item("afi_matricula"))
        DTmoratoriaCargadas = New DataTable
        DAmoratoria.Fill(DTmoratoriaCargadas)
        UGMoratorias.DataSource = DTmoratoriaCargadas
    End Sub

    Private Sub FrmVeoMoratorias_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        MostrarMoratoriasCargadas()
    End Sub

    Private Sub CBExportar_Click(sender As Object, e As EventArgs) Handles CBExportar.Click
        clsExportarExcel.ExportarDatosExcel(UGMoratorias, "Moratoria Profesional " & r_Matricula("afi_titulo") & r_Matricula("afi_matricula") & " - " & r_Matricula("afi_nombre"))
    End Sub

    Private Sub CBAlta_Click(sender As Object, e As EventArgs) Handles CBAlta.Click
        'Controlo permiso Moratoria Create
        If controlAcceso(1, 9, "C", True) = True Then
            Dim FrmMora As New FrmMoratoria(r_Matricula)
            FrmMora.ShowDialog()
            MostrarMoratoriasCargadas()
        End If
    End Sub
End Class