﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCtaCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCtaCte))
        Me.dgvServicios = New System.Windows.Forms.DataGridView()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbtImprimir = New System.Windows.Forms.Button()
        Me.UltraGroupBox2 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.DgvCategorias = New System.Windows.Forms.DataGridView()
        Me.CButton4 = New System.Windows.Forms.Button()
        CType(Me.dgvServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox2.SuspendLayout()
        CType(Me.DgvCategorias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvServicios
        '
        Me.dgvServicios.AllowUserToAddRows = False
        Me.dgvServicios.AllowUserToDeleteRows = False
        Me.dgvServicios.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvServicios.Location = New System.Drawing.Point(0, 0)
        Me.dgvServicios.Name = "dgvServicios"
        Me.dgvServicios.Size = New System.Drawing.Size(545, 188)
        Me.dgvServicios.TabIndex = 0
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularDoubleSolid
        Me.UltraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox1.Controls.Add(Me.UDThasta)
        Me.UltraGroupBox1.Controls.Add(Me.UDTdesde)
        Me.UltraGroupBox1.Controls.Add(Me.Label2)
        Me.UltraGroupBox1.Controls.Add(Me.Label1)
        Me.UltraGroupBox1.Controls.Add(Me.cbtImprimir)
        Appearance1.BackColor = System.Drawing.Color.Blue
        Appearance1.FontData.BoldAsString = "False"
        Appearance1.ForeColor = System.Drawing.Color.White
        Me.UltraGroupBox1.HeaderAppearance = Appearance1
        Me.UltraGroupBox1.Location = New System.Drawing.Point(551, 0)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(259, 148)
        Me.UltraGroupBox1.TabIndex = 2
        Me.UltraGroupBox1.Text = "Resumen de cuenta"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UDThasta
        '
        Me.UDThasta.Location = New System.Drawing.Point(107, 60)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(97, 21)
        Me.UDThasta.TabIndex = 8
        '
        'UDTdesde
        '
        Me.UDTdesde.Location = New System.Drawing.Point(107, 33)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(97, 21)
        Me.UDTdesde.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(55, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 14)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Hasta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(56, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 14)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Desde:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cbtImprimir
        '
        Me.cbtImprimir.BackColor = System.Drawing.Color.Transparent
        Me.cbtImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.cbtImprimir.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.cbtImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cbtImprimir.Location = New System.Drawing.Point(68, 97)
        Me.cbtImprimir.Name = "cbtImprimir"
        Me.cbtImprimir.Size = New System.Drawing.Size(126, 32)
        Me.cbtImprimir.TabIndex = 4
        Me.cbtImprimir.Text = "Imprimir"
        Me.cbtImprimir.UseVisualStyleBackColor = False
        '
        'UltraGroupBox2
        '
        Me.UltraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularDoubleSolid
        Me.UltraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox2.Controls.Add(Me.CBtExportar)
        Me.UltraGroupBox2.Controls.Add(Me.DgvCategorias)
        Me.UltraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UltraGroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Appearance2.BackColor = System.Drawing.Color.Blue
        Appearance2.ForeColor = System.Drawing.Color.White
        Me.UltraGroupBox2.HeaderAppearance = Appearance2
        Me.UltraGroupBox2.Location = New System.Drawing.Point(0, 194)
        Me.UltraGroupBox2.Name = "UltraGroupBox2"
        Me.UltraGroupBox2.Size = New System.Drawing.Size(814, 177)
        Me.UltraGroupBox2.TabIndex = 4
        Me.UltraGroupBox2.Text = "Historial de categoria"
        Me.UltraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(774, 140)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(31, 30)
        Me.CBtExportar.TabIndex = 125
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'DgvCategorias
        '
        Me.DgvCategorias.AllowUserToAddRows = False
        Me.DgvCategorias.AllowUserToDeleteRows = False
        Me.DgvCategorias.AllowUserToResizeColumns = False
        Me.DgvCategorias.AllowUserToResizeRows = False
        Me.DgvCategorias.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.DgvCategorias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvCategorias.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DgvCategorias.Location = New System.Drawing.Point(4, 26)
        Me.DgvCategorias.Name = "DgvCategorias"
        Me.DgvCategorias.Size = New System.Drawing.Size(806, 147)
        Me.DgvCategorias.TabIndex = 4
        '
        'CButton4
        '
        Me.CButton4.BackColor = System.Drawing.Color.Transparent
        Me.CButton4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CButton4.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Warning_32xMD_color
        Me.CButton4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton4.Location = New System.Drawing.Point(569, 154)
        Me.CButton4.Name = "CButton4"
        Me.CButton4.Size = New System.Drawing.Size(222, 34)
        Me.CButton4.TabIndex = 153
        Me.CButton4.Text = "Notas de Reclamos"
        Me.CButton4.UseVisualStyleBackColor = False
        '
        'FrmCtaCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(814, 371)
        Me.Controls.Add(Me.CButton4)
        Me.Controls.Add(Me.UltraGroupBox2)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.dgvServicios)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmCtaCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuenta Corriente"
        CType(Me.dgvServicios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox2.ResumeLayout(False)
        CType(Me.DgvCategorias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvServicios As System.Windows.Forms.DataGridView
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents cbtImprimir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox2 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents DgvCategorias As System.Windows.Forms.DataGridView
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents CButton4 As System.Windows.Forms.Button
End Class
