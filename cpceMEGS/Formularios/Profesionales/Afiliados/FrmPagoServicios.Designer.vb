﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPagoServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Left")
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPagoServicios))
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ValueListItem3 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem4 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim ValueListItem1 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem2 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem5 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim ValueListItem6 As Infragistics.Win.ValueListItem = New Infragistics.Win.ValueListItem()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGDetalles = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraTextEditor1 = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UGProcesos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraLabel1 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraLabel2 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraLabel3 = New Infragistics.Win.Misc.UltraLabel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.CBConfirmar = New System.Windows.Forms.Button()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.TxtHaber = New System.Windows.Forms.TextBox()
        Me.TxtDebe = New System.Windows.Forms.TextBox()
        Me.UltraDateTimeEditor1 = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraLabel4 = New Infragistics.Win.Misc.UltraLabel()
        Me.TxtConcepto1 = New System.Windows.Forms.TextBox()
        Me.UltraLabel5 = New Infragistics.Win.Misc.UltraLabel()
        Me.UltraComboEditor1 = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.TxtConcepto2 = New System.Windows.Forms.TextBox()
        Me.TxtConcepto3 = New System.Windows.Forms.TextBox()
        Me.ImpresionPrint = New System.Drawing.Printing.PrintDocument()
        Me.ImpresionDialog = New System.Windows.Forms.PrintDialog()
        Me.UltraLabel6 = New Infragistics.Win.Misc.UltraLabel()
        Me.UCEdelegacion = New Infragistics.Win.UltraWinEditors.UltraComboEditor()
        Me.ULDetalle = New Infragistics.Win.Misc.UltraLabel()
        CType(Me.UGDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTextEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGProcesos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGDetalles
        '
        Me.UGDetalles.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGDetalles.DisplayLayout.Appearance = Appearance1
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGDetalles.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGDetalles.DisplayLayout.Override.HeaderAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDetalles.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGDetalles.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGDetalles.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGDetalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGDetalles.Location = New System.Drawing.Point(0, 53)
        Me.UGDetalles.Name = "UGDetalles"
        Me.UGDetalles.Size = New System.Drawing.Size(840, 560)
        Me.UGDetalles.TabIndex = 6
        Me.UGDetalles.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChange
        '
        'UltraTextEditor1
        '
        Me.UltraTextEditor1.AutoSize = False
        Appearance6.Image = CType(resources.GetObject("Appearance6.Image"), Object)
        EditorButton1.Appearance = Appearance6
        EditorButton1.Key = "Left"
        EditorButton1.Width = 30
        Me.UltraTextEditor1.ButtonsLeft.Add(EditorButton1)
        Appearance7.Image = CType(resources.GetObject("Appearance7.Image"), Object)
        EditorButton2.Appearance = Appearance7
        EditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton2.Key = "Right"
        Appearance8.Image = CType(resources.GetObject("Appearance8.Image"), Object)
        Appearance8.ImageBackgroundDisabled = CType(resources.GetObject("Appearance8.ImageBackgroundDisabled"), System.Drawing.Image)
        EditorButton2.PressedAppearance = Appearance8
        EditorButton2.Width = 80
        Me.UltraTextEditor1.ButtonsRight.Add(EditorButton2)
        Me.UltraTextEditor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraTextEditor1.Location = New System.Drawing.Point(86, 0)
        Me.UltraTextEditor1.Name = "UltraTextEditor1"
        Me.UltraTextEditor1.Size = New System.Drawing.Size(236, 29)
        Me.UltraTextEditor1.TabIndex = 0
        '
        'UGProcesos
        '
        Me.UGProcesos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Appearance9.BackColor = System.Drawing.Color.White
        Me.UGProcesos.DisplayLayout.Appearance = Appearance9
        Appearance10.BackColor = System.Drawing.Color.Transparent
        Me.UGProcesos.DisplayLayout.Override.CardAreaAppearance = Appearance10
        Me.UGProcesos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance11.FontData.BoldAsString = "True"
        Appearance11.FontData.Name = "Arial"
        Appearance11.FontData.SizeInPoints = 10.0!
        Appearance11.ForeColor = System.Drawing.Color.White
        Appearance11.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGProcesos.DisplayLayout.Override.HeaderAppearance = Appearance11
        Appearance12.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance12.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGProcesos.DisplayLayout.Override.RowSelectorAppearance = Appearance12
        Appearance13.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance13.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGProcesos.DisplayLayout.Override.SelectedRowAppearance = Appearance13
        Me.UGProcesos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGProcesos.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
        Me.UGProcesos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGProcesos.Location = New System.Drawing.Point(781, 87)
        Me.UGProcesos.Name = "UGProcesos"
        Me.UGProcesos.Size = New System.Drawing.Size(59, 190)
        Me.UGProcesos.TabIndex = 3
        Me.UGProcesos.Visible = False
        '
        'UltraLabel1
        '
        Appearance14.BackColor = System.Drawing.Color.White
        Appearance14.TextHAlignAsString = "Left"
        Appearance14.TextVAlignAsString = "Middle"
        Me.UltraLabel1.Appearance = Appearance14
        Me.UltraLabel1.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.InsetSoft
        Me.UltraLabel1.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Solid
        Me.UltraLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraLabel1.ImageTransparentColor = System.Drawing.Color.White
        Me.UltraLabel1.Location = New System.Drawing.Point(328, 1)
        Me.UltraLabel1.Name = "UltraLabel1"
        Me.UltraLabel1.Size = New System.Drawing.Size(447, 26)
        Me.UltraLabel1.TabIndex = 4
        Me.UltraLabel1.Visible = False
        '
        'UltraLabel2
        '
        Appearance15.TextHAlignAsString = "Right"
        Appearance15.TextVAlignAsString = "Middle"
        Me.UltraLabel2.Appearance = Appearance15
        Me.UltraLabel2.AutoSize = True
        Me.UltraLabel2.Location = New System.Drawing.Point(22, 7)
        Me.UltraLabel2.Name = "UltraLabel2"
        Me.UltraLabel2.Size = New System.Drawing.Size(58, 17)
        Me.UltraLabel2.TabIndex = 5
        Me.UltraLabel2.Text = "Proceso:"
        '
        'UltraLabel3
        '
        Appearance16.TextHAlignAsString = "Right"
        Appearance16.TextVAlignAsString = "Middle"
        Me.UltraLabel3.Appearance = Appearance16
        Me.UltraLabel3.AutoSize = True
        Me.UltraLabel3.Location = New System.Drawing.Point(34, 62)
        Me.UltraLabel3.Name = "UltraLabel3"
        Me.UltraLabel3.Size = New System.Drawing.Size(46, 17)
        Me.UltraLabel3.TabIndex = 8
        Me.UltraLabel3.Text = "Fecha:"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "add.ico")
        Me.ImageList1.Images.SetKeyName(1, "Search (2).ico")
        Me.ImageList1.Images.SetKeyName(2, "delete.ico")
        Me.ImageList1.Images.SetKeyName(3, "application_view_columns.png")
        '
        'CBConfirmar
        '
        Me.CBConfirmar.BackColor = System.Drawing.Color.Transparent
        Me.CBConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBConfirmar.ImageIndex = 0
        Me.CBConfirmar.Location = New System.Drawing.Point(216, 625)
        Me.CBConfirmar.Name = "CBConfirmar"
        Me.CBConfirmar.Size = New System.Drawing.Size(184, 37)
        Me.CBConfirmar.TabIndex = 13
        Me.CBConfirmar.Text = "Confirmar"
        Me.CBConfirmar.UseVisualStyleBackColor = False
        '
        'UltraGroupBox1
        '
        Appearance17.BackColor = System.Drawing.Color.SteelBlue
        Appearance17.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal
        Me.UltraGroupBox1.ContentAreaAppearance = Appearance17
        Me.UltraGroupBox1.Controls.Add(Me.TxtHaber)
        Me.UltraGroupBox1.Controls.Add(Me.TxtDebe)
        Me.UltraGroupBox1.Location = New System.Drawing.Point(515, 615)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(289, 55)
        Me.UltraGroupBox1.TabIndex = 18
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'TxtHaber
        '
        Me.TxtHaber.BackColor = System.Drawing.Color.Black
        Me.TxtHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtHaber.ForeColor = System.Drawing.SystemColors.Window
        Me.TxtHaber.Location = New System.Drawing.Point(144, 13)
        Me.TxtHaber.Name = "TxtHaber"
        Me.TxtHaber.ReadOnly = True
        Me.TxtHaber.Size = New System.Drawing.Size(126, 26)
        Me.TxtHaber.TabIndex = 19
        Me.TxtHaber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TxtDebe
        '
        Me.TxtDebe.BackColor = System.Drawing.Color.Black
        Me.TxtDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDebe.ForeColor = System.Drawing.SystemColors.Window
        Me.TxtDebe.Location = New System.Drawing.Point(16, 13)
        Me.TxtDebe.Name = "TxtDebe"
        Me.TxtDebe.ReadOnly = True
        Me.TxtDebe.Size = New System.Drawing.Size(126, 26)
        Me.TxtDebe.TabIndex = 18
        Me.TxtDebe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'UltraDateTimeEditor1
        '
        Me.UltraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UltraDateTimeEditor1.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UltraDateTimeEditor1.FormatString = ""
        Me.UltraDateTimeEditor1.Location = New System.Drawing.Point(86, 59)
        Me.UltraDateTimeEditor1.MaskInput = "{date}"
        Me.UltraDateTimeEditor1.Name = "UltraDateTimeEditor1"
        Me.UltraDateTimeEditor1.Size = New System.Drawing.Size(236, 24)
        Me.UltraDateTimeEditor1.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UltraDateTimeEditor1.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UltraDateTimeEditor1.TabIndex = 1
        '
        'UltraLabel4
        '
        Appearance18.TextHAlignAsString = "Right"
        Appearance18.TextVAlignAsString = "Middle"
        Me.UltraLabel4.Appearance = Appearance18
        Me.UltraLabel4.AutoSize = True
        Me.UltraLabel4.Location = New System.Drawing.Point(14, 87)
        Me.UltraLabel4.Name = "UltraLabel4"
        Me.UltraLabel4.Size = New System.Drawing.Size(66, 17)
        Me.UltraLabel4.TabIndex = 21
        Me.UltraLabel4.Text = "Concepto:"
        '
        'TxtConcepto1
        '
        Me.TxtConcepto1.Location = New System.Drawing.Point(86, 85)
        Me.TxtConcepto1.MaxLength = 100
        Me.TxtConcepto1.Name = "TxtConcepto1"
        Me.TxtConcepto1.Size = New System.Drawing.Size(688, 22)
        Me.TxtConcepto1.TabIndex = 3
        '
        'UltraLabel5
        '
        Appearance19.TextHAlignAsString = "Right"
        Appearance19.TextVAlignAsString = "Middle"
        Me.UltraLabel5.Appearance = Appearance19
        Me.UltraLabel5.AutoSize = True
        Me.UltraLabel5.Location = New System.Drawing.Point(331, 62)
        Me.UltraLabel5.Name = "UltraLabel5"
        Me.UltraLabel5.Size = New System.Drawing.Size(69, 17)
        Me.UltraLabel5.TabIndex = 23
        Me.UltraLabel5.Text = "Institución:"
        '
        'UltraComboEditor1
        '
        Me.UltraComboEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        ValueListItem3.DataValue = CType(1, Short)
        ValueListItem3.DisplayText = "SIPRES"
        ValueListItem4.DataValue = CType(2, Short)
        ValueListItem4.DisplayText = "CPCE"
        Me.UltraComboEditor1.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem3, ValueListItem4})
        Me.UltraComboEditor1.Location = New System.Drawing.Point(406, 59)
        Me.UltraComboEditor1.Name = "UltraComboEditor1"
        Me.UltraComboEditor1.Size = New System.Drawing.Size(236, 24)
        Me.UltraComboEditor1.TabIndex = 2
        '
        'TxtConcepto2
        '
        Me.TxtConcepto2.Location = New System.Drawing.Point(86, 30)
        Me.TxtConcepto2.MaxLength = 100
        Me.TxtConcepto2.Name = "TxtConcepto2"
        Me.TxtConcepto2.Size = New System.Drawing.Size(688, 22)
        Me.TxtConcepto2.TabIndex = 4
        '
        'TxtConcepto3
        '
        Me.TxtConcepto3.Location = New System.Drawing.Point(87, 131)
        Me.TxtConcepto3.MaxLength = 100
        Me.TxtConcepto3.Name = "TxtConcepto3"
        Me.TxtConcepto3.Size = New System.Drawing.Size(688, 22)
        Me.TxtConcepto3.TabIndex = 5
        '
        'ImpresionDialog
        '
        Me.ImpresionDialog.UseEXDialog = True
        '
        'UltraLabel6
        '
        Appearance20.TextHAlignAsString = "Right"
        Appearance20.TextVAlignAsString = "Middle"
        Me.UltraLabel6.Appearance = Appearance20
        Me.UltraLabel6.AutoSize = True
        Me.UltraLabel6.Location = New System.Drawing.Point(648, 62)
        Me.UltraLabel6.Name = "UltraLabel6"
        Me.UltraLabel6.Size = New System.Drawing.Size(76, 17)
        Me.UltraLabel6.TabIndex = 24
        Me.UltraLabel6.Text = "Delegación:"
        '
        'UCEdelegacion
        '
        Me.UCEdelegacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        ValueListItem1.DataValue = CType(1, Short)
        ValueListItem1.DisplayText = "Resistencia"
        ValueListItem2.DataValue = CType(2, Short)
        ValueListItem2.DisplayText = "Saenz Peña"
        ValueListItem5.DataValue = CType(3, Short)
        ValueListItem5.DisplayText = "Villa Angela"
        ValueListItem6.DataValue = CType(4, Short)
        ValueListItem6.DisplayText = "Sudoeste"
        Me.UCEdelegacion.Items.AddRange(New Infragistics.Win.ValueListItem() {ValueListItem1, ValueListItem2, ValueListItem5, ValueListItem6})
        Me.UCEdelegacion.Location = New System.Drawing.Point(730, 59)
        Me.UCEdelegacion.Name = "UCEdelegacion"
        Me.UCEdelegacion.Size = New System.Drawing.Size(236, 24)
        Me.UCEdelegacion.TabIndex = 25
        '
        'ULDetalle
        '
        Appearance21.TextHAlignAsString = "Right"
        Appearance21.TextVAlignAsString = "Middle"
        Me.ULDetalle.Appearance = Appearance21
        Me.ULDetalle.AutoSize = True
        Me.ULDetalle.Location = New System.Drawing.Point(30, 36)
        Me.ULDetalle.Name = "ULDetalle"
        Me.ULDetalle.Size = New System.Drawing.Size(50, 17)
        Me.ULDetalle.TabIndex = 26
        Me.ULDetalle.Text = "Detalle:"
        '
        'FrmPagoServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(840, 668)
        Me.Controls.Add(Me.ULDetalle)
        Me.Controls.Add(Me.UGProcesos)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.CBConfirmar)
        Me.Controls.Add(Me.UGDetalles)
        Me.Controls.Add(Me.UltraLabel2)
        Me.Controls.Add(Me.UltraLabel1)
        Me.Controls.Add(Me.UltraTextEditor1)
        Me.Controls.Add(Me.UltraLabel3)
        Me.Controls.Add(Me.UltraDateTimeEditor1)
        Me.Controls.Add(Me.UltraComboEditor1)
        Me.Controls.Add(Me.TxtConcepto2)
        Me.Controls.Add(Me.TxtConcepto3)
        Me.Controls.Add(Me.TxtConcepto1)
        Me.Controls.Add(Me.UltraLabel4)
        Me.Controls.Add(Me.UltraLabel5)
        Me.Controls.Add(Me.UltraLabel6)
        Me.Controls.Add(Me.UCEdelegacion)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.Name = "FrmPagoServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asiento"
        CType(Me.UGDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTextEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGProcesos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UltraDateTimeEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraComboEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UCEdelegacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGDetalles As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraTextEditor1 As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UGProcesos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraLabel1 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraLabel2 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraLabel3 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CBConfirmar As System.Windows.Forms.Button
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents TxtHaber As System.Windows.Forms.TextBox
    Friend WithEvents TxtDebe As System.Windows.Forms.TextBox
    Friend WithEvents UltraDateTimeEditor1 As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraLabel4 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents TxtConcepto1 As System.Windows.Forms.TextBox
    Friend WithEvents UltraLabel5 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UltraComboEditor1 As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents TxtConcepto2 As System.Windows.Forms.TextBox
    Friend WithEvents TxtConcepto3 As System.Windows.Forms.TextBox
    Friend WithEvents ImpresionPrint As System.Drawing.Printing.PrintDocument
    Friend WithEvents ImpresionDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents UltraLabel6 As Infragistics.Win.Misc.UltraLabel
    Friend WithEvents UCEdelegacion As Infragistics.Win.UltraWinEditors.UltraComboEditor
    Friend WithEvents ULDetalle As Infragistics.Win.Misc.UltraLabel
End Class
