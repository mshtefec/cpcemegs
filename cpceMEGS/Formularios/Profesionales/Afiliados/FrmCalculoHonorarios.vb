﻿Public Class FrmCalculoHonorarios
    Private Calc As New Calculos
    Private n_tarea As Integer
    Private n_tablaCalculosResolucion As String
    Private n_trabajoMontos As Long()

    Public Sub New(ByVal nTarea As Integer, ByVal tablaCalculosResolucion As String, Optional ByVal trabajoMontos As Long() = Nothing)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        n_tarea = nTarea
        n_tablaCalculosResolucion = tablaCalculosResolucion
        n_trabajoMontos = trabajoMontos
    End Sub
    Private Sub FrmCalculoHonorarios_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Select Case n_tarea
            Case 1, 2 'calculado con activo, pasivo e ingreso
                LabelLinea4.Visible = False
                LabelLinea5.Visible = False
                LabelLinea6.Visible = False
                UCEmeses.Visible = False
                UCEimportePeriodo.Visible = False
                UCEporcentajeSindico.Visible = False
                If Not IsNothing(n_trabajoMontos) Then
                    UCEactivo.Value = n_trabajoMontos(0)
                    UCEpasivo.Value = 0
                    UCEingreso.Value = n_trabajoMontos(1)
                    UCELeftCalculoHonorarios()
                End If
            Case 3 'calculado sobre monto otros conceptos
                LabelLinea4.Visible = False
                LabelLinea5.Visible = False
                UCEmeses.Visible = False
                UCEimportePeriodo.Visible = False
                If Not IsNothing(n_trabajoMontos) Then
                    UCEactivo.Value = n_trabajoMontos(0)
                    UCEpasivo.Value = 0
                    UCEingreso.Value = n_trabajoMontos(1)
                    UCEporcentajeSindico.Value = n_trabajoMontos(5)
                    UCELeftCalculoHonorarios()
                End If
            Case 6, 9, 11
                If Not IsNothing(n_trabajoMontos) Then
                    UCEactivo.Value = n_trabajoMontos(0)
                    UCEpasivo.Value = 0
                    UCEingreso.Value = n_trabajoMontos(1)
                    UCEmeses.Value = n_trabajoMontos(4)
                    UCEimportePeriodo.Value = n_trabajoMontos(3)
                    UCEporcentajeSindico.Value = n_trabajoMontos(5)
                    UCELeftCalculoHonorarios()
                End If
            Case 7, 10 'Un solo estado contable
                LabelLinea5.Visible = False
                LabelLinea6.Visible = False
                UCEimportePeriodo.Visible = False
                UCEporcentajeSindico.Visible = False
                If Not IsNothing(n_trabajoMontos) Then
                    UCEactivo.Value = n_trabajoMontos(0)
                    UCEpasivo.Value = 0
                    UCEingreso.Value = n_trabajoMontos(1)
                    UCEmeses.Value = n_trabajoMontos(4)
                    UCELeftCalculoHonorarios()
                End If
            Case 4, 5, 17, 19, 20, 21, 23, 24, 26, 27, 28, 29, 30, 31 'digitado
                LabelLinea1.Text = "Monto Honorarios Pactados:"
                LabelLinea2.Visible = False
                LabelLinea3.Visible = False
                LabelLinea4.Visible = False
                LabelLinea5.Visible = False
                LabelLinea6.Visible = False
                UCEpasivo.Visible = False
                UCEingreso.Visible = False
                UCEmeses.Visible = False
                UCEimportePeriodo.Visible = False
                UCEporcentajeSindico.Visible = False
                If Not IsNothing(n_trabajoMontos) Then
                    UCEactivo.Value = n_trabajoMontos(2)
                    UCELeftCalculoHonorarios()
                End If
                'Case 9 'calculado con activo, pasivo e ingreso dividido meses
                '    UCEmeses.MaskInput = "nnnn"
                'Case 22
                '    LabelLinea4.Text = "Porcentaje de Reducción:"
        End Select
    End Sub
    Private Sub UCEactivo_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEactivo.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UCEpasivo_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEpasivo.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UCEingreso_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEingreso.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
    Private Sub UltraCurrencyEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UltraCurrencyEditor1.EditorButtonClick
        If e.Button.Key = "Left" Then
            UCELeftCalculoHonorarios()
        Else
            DialogResult = DialogResult.OK
        End If
    End Sub
    Private Sub UCELeftCalculoHonorarios()
        Dim nMontoIngreso As String = Convert.ToString(IIf(UCEingreso.Value > (UCEactivo.Value + UCEpasivo.Value), UCEingreso.Value, UCEactivo.Value + UCEpasivo.Value))
        nMontoIngreso = nMontoIngreso.ToString.Replace(",", ".")
        UltraCurrencyEditor1.Value = Calc.CalculoHonorarios(
            n_tarea, nMontoIngreso, UCEmeses.Value, UCEporcentajeSindico.Value, UCEimportePeriodo.Value, n_tablaCalculosResolucion
        )
    End Sub
    Private Sub UCEmeses_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UCEmeses.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub
End Class