﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVeoMoratorias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmVeoMoratorias))
        Me.UGMoratorias = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBAlta = New System.Windows.Forms.Button()
        Me.CBExportar = New System.Windows.Forms.Button()
        CType(Me.UGMoratorias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGMoratorias
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGMoratorias.DisplayLayout.Appearance = Appearance1
        Me.UGMoratorias.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGMoratorias.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGMoratorias.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGMoratorias.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGMoratorias.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGMoratorias.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGMoratorias.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMoratorias.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMoratorias.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGMoratorias.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGMoratorias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGMoratorias.Location = New System.Drawing.Point(0, 0)
        Me.UGMoratorias.Name = "UGMoratorias"
        Me.UGMoratorias.Size = New System.Drawing.Size(883, 436)
        Me.UGMoratorias.TabIndex = 14
        '
        'CBAlta
        '
        Me.CBAlta.BackColor = System.Drawing.Color.Transparent
        Me.CBAlta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBAlta.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBAlta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBAlta.Location = New System.Drawing.Point(27, 442)
        Me.CBAlta.Name = "CBAlta"
        Me.CBAlta.Size = New System.Drawing.Size(128, 38)
        Me.CBAlta.TabIndex = 117
        Me.CBAlta.Text = "Alta"
        Me.CBAlta.UseVisualStyleBackColor = False
        '
        'CBExportar
        '
        Me.CBExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBExportar.Image = Global.cpceMEGS.My.Resources.Resources.PrintSetup_11011
        Me.CBExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBExportar.Location = New System.Drawing.Point(189, 442)
        Me.CBExportar.Name = "CBExportar"
        Me.CBExportar.Size = New System.Drawing.Size(128, 38)
        Me.CBExportar.TabIndex = 118
        Me.CBExportar.Text = "Exportar"
        Me.CBExportar.UseVisualStyleBackColor = False
        '
        'FrmVeoMoratorias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(883, 483)
        Me.Controls.Add(Me.CBExportar)
        Me.Controls.Add(Me.CBAlta)
        Me.Controls.Add(Me.UGMoratorias)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmVeoMoratorias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Moratorias"
        CType(Me.UGMoratorias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UGMoratorias As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBAlta As System.Windows.Forms.Button
    Friend WithEvents CBExportar As System.Windows.Forms.Button
End Class
