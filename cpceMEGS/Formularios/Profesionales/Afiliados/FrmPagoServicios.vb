﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl

Public Class FrmPagoServicios
    Private cnn As New ConsultaBD(True)
    Private DSProcesos As DataSet
    Private DAProcesos As MySqlDataAdapter
    Private BSProcesos As BindingSource
    Private DSDetalles As DataSet
    Private DADetalles As MySqlDataAdapter
    Private BSDetalles As BindingSource
    Private DTSubCuenta As DataTable
    Private DTCheques As DataTable
    Private DTProf As DataTable
    Private DAProf As MySqlDataAdapter
    Private DTAsiImportado As DataTable
    Private DTCuotasServicio As New DataTable
    Private DTCuotasMoratoria As New DataTable
    Private nCaja As Integer
    Private nZeta As Integer
    Private Prestamo As Double = 0

    Dim Reporte As String = ""
    'Imprimir el Recibo
    Private DTImprimir As DataTable
    Private DTImpDeta As DataTable
    Private DTAsiento As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long
    'Para guardar los cheques aparte
    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter
    Private CantCheques As Integer = 0
    Private c_TipDoc As String
    Private n_NroDoc As Integer
    Private c_Titulo As String
    Private n_Matricula As Integer
    Private n_Total As Double

    Public Sub New(ByVal cTipdoc As String, ByVal nNrodoc As Integer, ByVal cTitulo As String, ByVal nMatricula As Integer, ByVal nTotal As Double)
        InitializeComponent()
        c_TipDoc = cTipdoc
        n_NroDoc = nNrodoc
        c_Titulo = cTitulo
        n_Matricula = nMatricula
        n_Total = nTotal
    End Sub

    Public ReadOnly Property GetDetalles() As DataTable
        Get
            Return DSDetalles.Tables(0)
        End Get
    End Property

    Public ReadOnly Property GetCuotasServicios() As DataTable
        Get
            Return DTCuotasServicio
        End Get
    End Property

    Private Sub CargaProcesos()
        Try
            DAProcesos = cnn.consultaBDadapter(
                "procesos",
                "pro_codigo AS proceso,pro_nombre AS Descripcion,pro_buscaf,pro_bloqfech,pro_cajachica,pro_instit,pro_cpto1,pro_cpto2,pro_cpto3,pro_leyenda1,pro_leyenda2,pro_leyenda3,pro_leyenda4,pro_reporte" & nPubNroCli & ",pro_catexcl,pro_destinatario",
                "pro_activo = 'Si' ORDER BY pro_nombre"
            )
            DSProcesos = New DataSet
            DAProcesos.Fill(DSProcesos, "procesos")
        Catch ex As Exception
            mostrarErrorConexion("obtener datos.")
            Close()
            Exit Sub
        End Try
        BSProcesos = New BindingSource
        BSProcesos.DataSource = DSProcesos.Tables(0)
        UltraDateTimeEditor1.DateTime = Now
    End Sub

    Private Sub SeleccionaProceso()
        Dim row As DataRowView = BSProcesos.Current
        UltraTextEditor1.Text = row.Item("proceso")
        UltraLabel1.Text = row.Item("Descripcion")
        TxtConcepto1.Text = row.Item("pro_cpto1")
        TxtConcepto2.Text = row.Item("pro_cpto2")
        TxtConcepto3.Text = row.Item("pro_cpto3")
        UltraLabel1.Visible = True
        UGProcesos.Visible = False
        TxtDebe.Text = ""
        TxtHaber.Text = ""
        Reporte = row.Item("pro_reporte" & nPubNroCli)
        If row.Item("pro_bloqfech").ToString = "Si" Then
            UltraDateTimeEditor1.ReadOnly = True
            UltraComboEditor1.ReadOnly = True
            UCEdelegacion.ReadOnly = True
        Else
            UltraDateTimeEditor1.ReadOnly = False
            UltraComboEditor1.ReadOnly = False
            UCEdelegacion.ReadOnly = False
        End If
        If row.Item("pro_cajachica") = "Si" Then
            If cnn.CajaActiva Then
                nZeta = cnn.Zeta
                nCaja = cnn.Caja
            Else
                nZeta = 0
                nCaja = 0
                CBConfirmar.Enabled = False
            End If
        Else
            nZeta = 0
            nCaja = 0
        End If
    End Sub

    Private Sub LimpiarProceso()
        Try
            UltraTextEditor1.Text = ""
            UltraLabel1.Visible = False
            UGProcesos.Visible = False
            TxtDebe.Text = ""
            TxtHaber.Text = ""
            TxtConcepto1.Text = ""
            TxtConcepto2.Text = ""
            TxtConcepto3.Text = ""
            UltraDateTimeEditor1.DateTime = Now
            UltraComboEditor1.Value = Nothing
            UGDetalles.DataSource = Nothing
            nZeta = 0
            nCaja = 0
            Prestamo = 0
            CBConfirmar.Enabled = True
            DSDetalles.Clear()
            DSDetalles.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MuestraProcesos()
        If UGProcesos.Visible Then
            SeleccionaProceso()
        Else
            Dim fila As Integer = BSProcesos.Find("proceso", UltraTextEditor1.Text)

            If fila > 0 Then
                BSProcesos.Position = fila
                SeleccionaProceso()
            Else
                UltraLabel1.Visible = False
                UGProcesos.DataSource = BSProcesos

                With UGProcesos.DisplayLayout.Bands(0)
                    .Columns(1).Width = 420
                    .Columns(2).Hidden = True
                    .Columns(3).Hidden = True
                    .Columns(4).Hidden = True
                    .Columns(5).Hidden = True
                    .Columns(6).Hidden = True
                    .Columns(7).Hidden = True
                    .Columns(8).Hidden = True
                    .Columns(9).Hidden = True
                    .Columns(10).Hidden = True
                    .Columns(11).Hidden = True
                    .Columns(12).Hidden = True
                    .Columns(13).Hidden = True
                End With
                UGProcesos.Top = 37
                UGProcesos.Left = 87
                UGProcesos.Visible = True
                UGProcesos.Height = 252
                UGProcesos.Width = 560
            End If
        End If
    End Sub

    Private Sub MuestraDestinatario(ByVal lLimpia As Boolean)
        Dim rowDesti As UltraGridRow = UGDetalles.ActiveRow
        If lLimpia Then
            UGProcesos.Visible = False
            GrabarDestinatario(rowDesti, lLimpia)
        Else
            Dim frmDesti As New FrmDestinatario
            If UGProcesos.Visible Then
                UGProcesos.Visible = False
                GrabarDestinatario(rowDesti, lLimpia)
            Else
                If Trim(rowDesti.Cells(3).Text) <> "" Then
                    frmDesti.Condicion = "afi_titulo='" & Mid(rowDesti.Cells(3).Text, 1, 2) & "' and afi_matricula=" & CInt(Mid(rowDesti.Cells(3).Text, 3))
                Else
                    frmDesti.Condicion = "afi_nombre like '" & Trim(rowDesti.Cells(2).Text) & "%'"
                End If
                frmDesti.CargaDestinatario()
                If frmDesti.MatriculaDestinatario <> "" Then
                    If frmDesti.Categoria <> "" AndAlso DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_catexcl").ToString.Contains(frmDesti.Categoria) Then

                        MessageBox.Show("El Profesional " & frmDesti.NombreDestinatario & " esta " & frmDesti.NombreCategoria)
                    Else
                        rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                        rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                        rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                        rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario
                        GrabarDestinatario(rowDesti, lLimpia)
                    End If
                Else
                    frmDesti.ShowDialog()
                    If frmDesti.DialogResult = DialogResult.OK Then
                        If frmDesti.Categoria <> "" AndAlso DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_catexcl").ToString.Contains(frmDesti.Categoria) Then
                            MessageBox.Show("El Profesional " & frmDesti.NombreDestinatario & " esta " & frmDesti.NombreCategoria)
                        Else
                            rowDesti.Cells("Destinatario").Value = frmDesti.NombreDestinatario
                            rowDesti.Cells("matricula").Value = frmDesti.MatriculaDestinatario
                            rowDesti.Cells("TipoDoc").Value = frmDesti.TipoDocDestinatario
                            rowDesti.Cells("NroDoc").Value = frmDesti.NroDocDestinatario

                            GrabarDestinatario(rowDesti, lLimpia)
                        End If
                    End If
                End If
            End If
            frmDesti.Dispose()
        End If
    End Sub

    Private Sub GrabarDestinatario(ByRef rowDesti As UltraGridRow, ByVal lLimpia As Boolean)
        If lLimpia Then
            rowDesti.Cells("Destinatario").Value = ""
            rowDesti.Cells("matricula").Value = ""
            rowDesti.Cells("TipoDoc").Value = ""
            rowDesti.Cells("NroDoc").Value = 0
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("subcuenta") = ""
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = ""
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = 0
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = False
            ' If DSProcesos.Tables("procesos").Rows(0).Item("pro_buscaf") = "S" Then
            If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_buscaf") = "Si" Then
                For Each RowDestinatario As DataRow In DSDetalles.Tables("subcuenta").Rows
                    RowDestinatario.Item("Destinatario") = ""
                    RowDestinatario.Item("matricula") = ""
                    RowDestinatario.Item("tipodoc") = ""
                    RowDestinatario.Item("NroDoc") = 0
                Next
            End If
        Else
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Subcuenta") = rowDesti.Cells("TipoDoc").Value & Format(rowDesti.Cells("NroDoc").Value, "00000000")
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("Matricula") = rowDesti.Cells("Matricula").Value
            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("destinatario") = True

            If DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_buscaf") = "Si" Then
                For Each RowDestinatario As DataRow In DSDetalles.Tables("subcuenta").Rows
                    RowDestinatario.Item("Destinatario") = rowDesti.Cells("destinatario").Value
                    RowDestinatario.Item("matricula") = rowDesti.Cells("matricula").Value
                    RowDestinatario.Item("tipodoc") = rowDesti.Cells("TipoDoc").Value
                    RowDestinatario.Item("NroDoc") = rowDesti.Cells("NroDoc").Value
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("Subcuenta") = rowDesti.Cells("TipoDoc").Value & Format(rowDesti.Cells("NroDoc").Value, "00000000")
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("Matricula") = rowDesti.Cells("Matricula").Value
                    DSDetalles.Tables(0).Rows(RowDestinatario.Item("item") - 1).Item("destinatario") = True

                Next
            End If

            Select Case UltraTextEditor1.Text.Substring(2, 1)
                Case "R" ' si es recibo , busco cuotas a cancelar
                    'Dim frmCuotas As New FrmCuotasImpagas(cnn, Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta"), Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_tipmov"), rowDesti.Cells("TipoDoc").Value, rowDesti.Cells("nrodoc").Value)
                    'frmCuotas.Text = "Cuotas impagas de " & rowDesti.Cells(2).Value
                    'frmCuotas.ShowDialog()
                    'If frmCuotas.DialogResult = Windows.Forms.DialogResult.OK Then
                    '    Me.DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = frmCuotas.GetMontoImpagas
                    '    DTCuotasServicio = frmCuotas.GetCuotasCanceladas
                    '    ' agrego columna de item
                    '    Dim newCol As DataColumn
                    '    newCol = New DataColumn("item", Type.GetType("System.Int32"))
                    '    newCol.DefaultValue = rowDesti.Cells("item"ABValue - 1
                    '    DTCuotasServicio.Columns.Add(newCol)

                    'End If
                Case "P" ' si es orden da pago
                    If DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pla_cuotas") = "Si" Then
                        FrmCuotasPrestamo.ShowDialog()
                        If FrmCuotasPrestamo.DialogResult = DialogResult.OK Then
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("debe") = FrmCuotasPrestamo.ImportePrestamo
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuotas") = FrmCuotasPrestamo.CuotasPrestamo
                            DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("fecven") = FrmCuotasPrestamo.Vencimiento
                            Prestamo = FrmCuotasPrestamo.Prestamo
                        End If
                    End If
            End Select
        End If

        SumoTotales()
    End Sub

    Private Sub BuscoCuotas(ByRef rowDesti As UltraGridRow)
        If rowDesti.Cells("nrodoc").Value > 0 Then
            Dim frmCuotas As New FrmCuotasImpagas(cnn, UltraTextEditor1.Text, DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("cuenta"), DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_tipmov"), rowDesti.Cells("TipoDoc").Value, rowDesti.Cells("nrodoc").Value)
            frmCuotas.Text = "Cuotas impagas de " & rowDesti.Cells(2).Value
            frmCuotas.StartPosition = FormStartPosition.Manual
            frmCuotas.ShowDialog()
            If frmCuotas.DialogResult = DialogResult.OK Then
                If UltraTextEditor1.Text <> "MANUAL" And rowDesti.Cells("item").Value >= 2 Then
                    DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("haber") = frmCuotas.GetMontoInteres
                    DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("pto_valor") = 0
                    DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 2).Item("pto_formula") = ""
                End If
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("haber") = frmCuotas.GetMontoImpagas
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_valor") = 0
                DSDetalles.Tables(0).Rows(rowDesti.Cells("item").Value - 1).Item("pto_formula") = ""
                'si es un recibo de moratoria cargo los importes en las cuentas
                If UltraTextEditor1.Text = "01RCMO" Or UltraTextEditor1.Text = "01DCMO" Or UltraTextEditor1.Text = "MORMAN" Then
                    DTCuotasMoratoria = frmCuotas.GetCuotasCanceladas
                    Dim dtCuotasClone As DataTable = DTCuotasMoratoria.Copy
                    'Si tiene cuotas es porque trajo de la tabla de cuo_det
                    If dtCuotasClone.Rows.Count() > 0 Then
                        Dim rowCuoSelect As DataRow = dtCuotasClone.Rows(0)
                        rowCuoSelect.Item("TIPO") = 1
                        rowCuoSelect.Item("total") = 0

                        rowCuoSelect.Item("D1") = 0
                        rowCuoSelect.Item("D2") = 0
                        rowCuoSelect.Item("D3") = 0
                        rowCuoSelect.Item("C1") = 0
                        rowCuoSelect.Item("C2") = 0
                        rowCuoSelect.Item("C3") = 0
                        rowCuoSelect.Item("C4") = 0
                        rowCuoSelect.Item("C5") = 0
                        rowCuoSelect.Item("C6") = 0
                        rowCuoSelect.Item("C7") = 0
                        rowCuoSelect.Item("C8") = 0
                        Dim porcentaje As Double = 1

                        For Each RowCuo_det As DataRow In DTCuotasMoratoria.Rows
                            'If RowCuo_det.Item("TIPO") = 1 Then
                            If RowCuo_det.Item("Select") Then
                                If RowCuo_det.Item("total") <> RowCuo_det.Item("totalReal") Then
                                    porcentaje = (RowCuo_det.Item("total") / RowCuo_det.Item("totalReal"))
                                End If
                                rowCuoSelect.Item("total") += RowCuo_det.Item("total")
                                rowCuoSelect.Item("D1") += Math.Round((RowCuo_det.Item("D1") * porcentaje), 2)
                                rowCuoSelect.Item("D2") += Math.Round((RowCuo_det.Item("D2") * porcentaje), 2)
                                rowCuoSelect.Item("D3") += Math.Round((RowCuo_det.Item("D3") * porcentaje), 2)
                                rowCuoSelect.Item("C1") += Math.Round((RowCuo_det.Item("C1") * porcentaje), 2)
                                rowCuoSelect.Item("C2") += Math.Round((RowCuo_det.Item("C2") * porcentaje), 2)
                                rowCuoSelect.Item("C3") += Math.Round((RowCuo_det.Item("C3") * porcentaje), 2)
                                rowCuoSelect.Item("C4") += Math.Round((RowCuo_det.Item("C4") * porcentaje), 2)
                                rowCuoSelect.Item("C5") += Math.Round((RowCuo_det.Item("C5") * porcentaje), 2)
                                rowCuoSelect.Item("C6") += Math.Round((RowCuo_det.Item("C6") * porcentaje), 2)
                                rowCuoSelect.Item("C7") += Math.Round((RowCuo_det.Item("C7") * porcentaje), 2)
                                rowCuoSelect.Item("C8") += Math.Round((RowCuo_det.Item("C8") * porcentaje), 2)
                            End If
                            'Else
                            '    rowCuoSelect.Item("TIPO") = 2
                            'End If
                        Next
                        'If rowCuoSelect.Item("TIPO") = 1 Then
                        'Control del monto de los D y C
                        Dim Dtotal As Double = Math.Round(rowCuoSelect.Item("D1") + rowCuoSelect.Item("D2") + rowCuoSelect.Item("D3"), 2)
                        Dim Ctotal As Double = Math.Round(rowCuoSelect.Item("C1") + rowCuoSelect.Item("C2") + rowCuoSelect.Item("C3") +
                            rowCuoSelect.Item("C4") + rowCuoSelect.Item("C5") + rowCuoSelect.Item("C6") +
                            rowCuoSelect.Item("C7") + rowCuoSelect.Item("C8"), 2)

                        If (Dtotal <> Ctotal) Then
                            Do While (rowCuoSelect.Item("total") <> Dtotal) Or (rowCuoSelect.Item("total") <> Ctotal)
                                If Dtotal <> rowCuoSelect.Item("total") Then
                                    If Dtotal > rowCuoSelect.Item("total") Then
                                        rowCuoSelect.Item("D3") = rowCuoSelect.Item("D3") - 0.01
                                    Else
                                        rowCuoSelect.Item("D3") = rowCuoSelect.Item("D3") + 0.01
                                    End If
                                ElseIf Ctotal <> rowCuoSelect.Item("total") Then
                                    If Ctotal > rowCuoSelect.Item("total") Then
                                        rowCuoSelect.Item("C8") = rowCuoSelect.Item("C8") - 0.01
                                    Else
                                        rowCuoSelect.Item("C8") = rowCuoSelect.Item("C8") + 0.01
                                    End If
                                End If
                                Dtotal = Math.Round(rowCuoSelect.Item("D1") + rowCuoSelect.Item("D2") + rowCuoSelect.Item("D3"), 2)
                                Ctotal = Math.Round(rowCuoSelect.Item("C1") + rowCuoSelect.Item("C2") + rowCuoSelect.Item("C3") +
                                                    rowCuoSelect.Item("C4") + rowCuoSelect.Item("C5") + rowCuoSelect.Item("C6") +
                                                    rowCuoSelect.Item("C7") + rowCuoSelect.Item("C8"), 2)
                            Loop
                            'Fin control de montos D y C
                        End If
                        'End If
                        If rowCuoSelect.Item("total") > 0 Then
                            Dim nItMor As Integer = 4
                            If UltraTextEditor1.Text = "MORMAN" Then
                                nItMor = 5
                            End If

                            For nIt As Integer = rowDesti.Cells("item").Value - 1 To DSDetalles.Tables(0).Rows.Count - 1
                                If DSDetalles.Tables(0).Rows(nIt).Item("pto_tipmov") = "D" Then
                                    DSDetalles.Tables(0).Rows(nIt).Item("Debe") = rowCuoSelect.Item(nItMor)
                                Else
                                    DSDetalles.Tables(0).Rows(nIt).Item("haber") = rowCuoSelect.Item(nItMor)
                                End If
                                nItMor += 1
                            Next
                            'SumoTotales()
                        Else
                            MessageBox.Show("Para cobrar tiene 2 opciones:" & vbNewLine & vbNewLine &
                                            "- Ingresar en Total:$ (monto a cobrar) y presionar Enter." & vbNewLine &
                                            "Y luego click en el botón Confirmar." & vbNewLine & vbNewLine &
                                            "- Seleccionar las cuotas." & vbNewLine &
                                            "Y luego click en el botón Confirmar", "Atención",
                                MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                            'BuscoCuotas(rowDesti) Si cerro y tiene proceso lo busco sino limpio el form
                            If String.IsNullOrWhiteSpace(UltraTextEditor1.Text.ToString) Then
                                LimpiarProceso()
                            Else
                                BuscaProcesoIngresado()
                            End If
                        End If
                    End If
                Else
                    DTCuotasServicio = frmCuotas.GetCuotasCanceladas
                    ' agrego columna de item
                    Dim newCol As DataColumn
                    newCol = New DataColumn("item", Type.GetType("System.Int32"))
                    newCol.DefaultValue = rowDesti.Cells("item").Value - 1
                    DTCuotasServicio.Columns.Add(newCol)
                End If

                SumoTotales()
            Else
                If UltraTextEditor1.Text = "01RCMO" Or UltraTextEditor1.Text = "01DCMO" Or UltraTextEditor1.Text = "MORMAN" Then
                    DTCuotasMoratoria = frmCuotas.GetCuotasCerroFormulario
                    If (Not IsNothing(DTCuotasMoratoria)) AndAlso (DTCuotasMoratoria.Rows.Count() > 0) Then
                        MessageBox.Show("Para cobrar tiene 2 opciones:" & vbNewLine & vbNewLine &
                                        "- Ingresar en Total:$ (monto a cobrar) y presionar Enter." & vbNewLine &
                                        "Y luego click en el botón Confirmar." & vbNewLine & vbNewLine &
                                        "- Seleccionar las cuotas." & vbNewLine &
                                        "Y luego click en el botón Confirmar", "Atención",
                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        'BuscoCuotas(rowDesti) Si cerro y tiene proceso lo busco sino limpio el form
                        If String.IsNullOrWhiteSpace(UltraTextEditor1.Text.ToString) Then
                            LimpiarProceso()
                        Else
                            BuscaProcesoIngresado()
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub FrmAsiento_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        DAProf = cnn.consultaBDadapter("afiliado", , "afi_tipdoc='" & c_TipDoc & "' and afi_nrodoc=" & n_NroDoc)
        DTProf = New DataTable
        DAProf.Fill(DTProf)
        CargaProcesos()
    End Sub

    Private Sub UltraTextEditor1_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles UltraTextEditor1.DragOver

    End Sub

    Private Sub UltraTextEditor1_EditorButtonClick(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinEditors.EditorButtonEventArgs) Handles UltraTextEditor1.EditorButtonClick
        If e.Button.Key = "Right" Then
            BuscaProcesoIngresado()
        Else
            LimpiarProceso()
        End If
    End Sub

    Private Sub AgregarFormaPago()
        Dim nR As Integer = DSDetalles.Tables(0).Rows.Count
        Dim rowDet As DataRow = DSDetalles.Tables(0).Rows(0)
        '  DSDetalles.Tables(0).Rows.Add()
        If UltraTextEditor1.Value.ToString.Substring(0, 2) = "01" Then ' SI ES SIPRES CARGO LA CUENTA PUENTA
            rowDet.Item("Cuenta") = "13070100"
            rowDet.Item("Descripcion") = "SIPRES -3314-(Deudor Consejo)"
        Else
            rowDet.Item("Cuenta") = "21010300"
            rowDet.Item("Descripcion") = "Honorarios a Reintegrar"
        End If
    End Sub

    Private Sub MuestraDetalles()
        DADetalles = cnn.consultaBDadapter("(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)", "pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio,pla_cuotas", "pto_codpro='" & UltraTextEditor1.Text & "' order by pto_item")
        DSDetalles = New DataSet
        DADetalles.Fill(DSDetalles, "detalles")
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        newCol.DefaultValue = UltraTextEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))
        newCol.DefaultValue = UltraComboEditor1.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("delegacion", Type.GetType("System.Int32"))
        newCol.DefaultValue = UCEdelegacion.Value
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = False
        DSDetalles.Tables("detalles").Columns.Add(newCol)
        newCol = New DataColumn("concepto2", Type.GetType("System.String"))
        newCol.DefaultValue = ""
        DSDetalles.Tables("detalles").Columns.Add(newCol)

        AgregarFormaPago()

        DTSubCuenta = New DataTable("subcuenta")
        With DTSubCuenta
            .Columns.Add("proceso", Type.GetType("System.String"))
            .Columns.Add("item", Type.GetType("System.Int32"))
            .Columns.Add("Destinatario", Type.GetType("System.String"))
            .Columns.Add("Matricula", Type.GetType("System.String"))
            .Columns.Add("Buscar", Type.GetType("System.String"))
            .Columns.Add("Eliminar", Type.GetType("System.String"))
            .Columns.Add("Cuotas", Type.GetType("System.String"))
            .Columns.Add("TipoDoc", Type.GetType("System.String"))
            .Columns.Add("NroDoc", Type.GetType("System.Int32"))
        End With

        For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
            If rowItem.Item("pla_servicio") = "Si" Then
                AgregarSubcuenta(rowItem)
            End If
        Next
        DSDetalles.Tables.Add(DTSubCuenta)
        'DTCheques = New DataTable("cheques")
        'With DTCheques
        '    .Columns.Add("proceso", Type.GetType("System.String"))
        '    .Columns.Add("item", Type.GetType("System.Int32"))
        '    .Columns.Add("Banco", Type.GetType("System.String"))
        '    newCol = New DataColumn("Cheque", Type.GetType("System.String"))
        '    newCol.DefaultValue = 0
        '    .Columns.Add(newCol)
        '    newCol = New DataColumn("FechaEmision", Type.GetType("System.String"))
        '    newCol.DefaultValue = Now.Date
        '    .Columns.Add(newCol)
        '    newCol = New DataColumn("FechaDiferida", Type.GetType("System.String"))
        '    ' newCol.DefaultValue = ""
        '    .Columns.Add(newCol)
        '    .Columns.Add("Monto", Type.GetType("System.Double"))
        '    .Columns.Add("Agregar", Type.GetType("System.String"))
        '    .Columns.Add("Eliminar", Type.GetType("System.String"))

        'End With

        '' los cheques por ahora no los cargo aca, se cargan en el proceso de asignacion

        'For Each rowItem As DataRow In DSDetalles.Tables("detalles").Rows
        '    If Mid(rowItem.Item("Subcuenta"), 1, 3) = "BCO" Then ' And rowItem.Item("pto_tipmov") = "D" Then
        '        AgregarCheques(rowItem)
        '    End If
        'Next
        'DSDetalles.Tables.Add(DTCheques)

        '' reinicio cuotas
        'DTCuotasServicio = New DataTable

        'Dim DRDetalles As New DataRelation("RelacionCheques", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("cheques").Columns("item"))
        'DSDetalles.Relations.Add(DRDetalles)
        Dim DRDetalles As New DataRelation("RelacionSubcuenta", DSDetalles.Tables("detalles").Columns("pto_item"), DSDetalles.Tables("subcuenta").Columns("item"))
        DSDetalles.Relations.Add(DRDetalles)

        BSDetalles = New BindingSource
        BSDetalles.DataSource = DSDetalles
        UGDetalles.DataSource = BSDetalles

        With UGDetalles.DisplayLayout
            With .Bands(0)
                .Columns(0).Width = 90
                .Columns(0).CellActivation = Activation.NoEdit
                .Columns(1).Width = 400
                .Columns(1).CellActivation = Activation.NoEdit
                .Columns(2).Width = 130
                .Columns(2).CellAppearance.TextHAlign = HAlign.Right
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(2).Format = "c"
                .Columns(2).PromptChar = ""
                .Columns(2).Style = ColumnStyle.Double
                .Columns(2).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(3).Width = 130
                .Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .Columns(3).Format = "c"
                .Columns(3).PromptChar = ""
                .Columns(3).Style = ColumnStyle.Double
                .Columns(3).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(3).CellActivation = Activation.NoEdit
                .Columns(4).Hidden = True
                .Columns(5).Hidden = True
                .Columns(6).Hidden = True
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
                .Columns(9).Hidden = True
                .Columns(10).Hidden = True
                .Columns(11).Hidden = True
                .Columns(12).Hidden = True
                .Columns(13).Hidden = True
                .Columns(14).Hidden = True
                .Columns(15).Hidden = True
                .Columns(16).Hidden = True
                .Columns(17).Hidden = True
                .Columns(18).Hidden = True
                .Columns(19).Hidden = True
            End With
            'With .Bands(1)
            '    '   With .Header.Appearance
            '    '.BackColor = Color.Aqua
            '    'End With
            '    .Columns(0).Hidden = True
            '    .Columns(1).Hidden = True
            '    .Columns(2).Width = 200
            '    .Columns(3).Width = 100
            '    .Columns(4).Width = 100
            '    .Columns(4).PromptChar = ""
            '    .Columns(4).Style = ColumnStyle.Date
            '    .Columns(4).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
            '    .Columns(4).CellActivation = Activation.NoEdit
            '    .Columns(4).Hidden = True
            '    .Columns(5).Width = 100
            '    .Columns(5).PromptChar = ""
            '    .Columns(5).Style = ColumnStyle.Date
            '    .Columns(5).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
            '    .Columns(5).CellActivation = Activation.NoEdit
            '    .Columns(6).Width = 100
            '    .Columns(6).CellAppearance.TextHAlign = HAlign.Right
            '    .Columns(6).Format = "c"
            '    .Columns(6).PromptChar = ""
            '    .Columns(6).Style = ColumnStyle.Double
            '    .Columns(6).FormatInfo = System.Globalization.CultureInfo.CurrentCulture
            '    .Columns(6).CellActivation = Activation.NoEdit
            '    Dim column As UltraGridColumn = Me.UGDetalles.DisplayLayout.Bands(1).Columns("Agregar")

            '    column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
            '    column.Width = 50

            '    column = Me.UGDetalles.DisplayLayout.Bands(1).Columns("Eliminar")
            '    column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
            '    column.Width = 50
            '    .Columns("Agregar").Header.Caption = ""
            '    .Columns("Agregar").Style = UltraWinGrid.ColumnStyle.Button
            '    .Columns("Agregar").CellButtonAppearance.Image = Me.ImageList1.Images(0)
            '    .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center
            '    .Columns("Eliminar").Header.Caption = ""
            '    .Columns("Eliminar").Style = UltraWinGrid.ColumnStyle.Button
            '    .Columns("Eliminar").CellButtonAppearance.Image = Me.ImageList1.Images(2)
            '    .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center


            '    .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(1).Header.Appearance.ForeColor = Color.Black
            '    .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(1).Header.Appearance.BackColor2 = Color.White
            '    .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(2).Header.Appearance.ForeColor = Color.Black
            '    .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(2).Header.Appearance.BackColor2 = Color.White
            '    .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(3).Header.Appearance.ForeColor = Color.Black
            '    .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(3).Header.Appearance.BackColor2 = Color.White
            '    .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(4).Header.Appearance.ForeColor = Color.Black
            '    .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(4).Header.Appearance.BackColor2 = Color.White
            '    .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(5).Header.Appearance.ForeColor = Color.Black
            '    .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(5).Header.Appearance.BackColor2 = Color.White
            '    .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(6).Header.Appearance.ForeColor = Color.Black
            '    .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(6).Header.Appearance.BackColor2 = Color.White
            '    .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(7).Header.Appearance.ForeColor = Color.Black
            '    .Columns(7).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(7).Header.Appearance.BackColor2 = Color.White
            '    .Columns(8).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
            '    .Columns(8).Header.Appearance.ForeColor = Color.Black
            '    .Columns(8).Header.Appearance.BackColor = Color.BlueViolet
            '    .Columns(8).Header.Appearance.BackColor2 = Color.White
            'End With

            With .Bands(1)
                .Columns(0).Hidden = True
                .Columns(1).Hidden = True
                .Columns(2).Width = 300
                .Columns(2).CellActivation = Activation.NoEdit
                .Columns(3).Width = 100
                .Columns(3).CellActivation = Activation.NoEdit
                .Columns(3).Header.Caption = "Matricula"
                Dim column As UltraGridColumn = UGDetalles.DisplayLayout.Bands(1).Columns("Buscar")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50

                .Columns("Buscar").Header.Caption = ""
                .Columns("Buscar").Style = ColumnStyle.Button
                .Columns("Buscar").CellButtonAppearance.Image = ImageList1.Images(1)
                .Columns("Buscar").CellButtonAppearance.ImageHAlign = HAlign.Center
                column = UGDetalles.DisplayLayout.Bands(1).Columns("Eliminar")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Eliminar").Header.Caption = ""
                .Columns("Eliminar").Style = ColumnStyle.Button
                .Columns("Eliminar").CellButtonAppearance.Image = ImageList1.Images(2)
                .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

                column = UGDetalles.DisplayLayout.Bands(1).Columns("Cuotas")
                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 50
                .Columns("Cuotas").Header.Caption = ""
                .Columns("Cuotas").Style = ColumnStyle.Button
                .Columns("Cuotas").CellButtonAppearance.Image = ImageList1.Images(3)
                .Columns("Cuotas").CellButtonAppearance.ImageHAlign = HAlign.Center
                .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(1).Header.Appearance.ForeColor = Color.Black
                .Columns(1).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(1).Header.Appearance.BackColor2 = Color.White
                .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(2).Header.Appearance.ForeColor = Color.Black
                .Columns(2).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(2).Header.Appearance.BackColor2 = Color.White
                .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(3).Header.Appearance.ForeColor = Color.Black
                .Columns(3).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(3).Header.Appearance.BackColor2 = Color.White
                .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(4).Header.Appearance.ForeColor = Color.Black
                .Columns(4).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(4).Header.Appearance.BackColor2 = Color.White
                .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(5).Header.Appearance.ForeColor = Color.Black
                .Columns(5).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(5).Header.Appearance.BackColor2 = Color.White
                .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                .Columns(6).Header.Appearance.ForeColor = Color.Black
                .Columns(6).Header.Appearance.BackColor = Color.BlueViolet
                .Columns(6).Header.Appearance.BackColor2 = Color.White
                .Columns(7).Hidden = True
                .Columns(8).Hidden = True
            End With
        End With
    End Sub

    Private Sub UGDetalles_AfterCellUpdate(ByVal sender As Object, ByVal e As CellEventArgs) Handles UGDetalles.AfterCellUpdate
        Select Case e.Cell.Band.Index
            Case 0
                UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.NoEdit
                UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.NoEdit
                UGDetalles.PerformAction(UltraGridAction.ExitEditMode)
            Case 1
                'UGDetalles.DisplayLayout.Bands(1).Columns("Banco").CellActivation = Activation.NoEdit
                'UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.NoEdit
                'UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.NoEdit
                'UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.NoEdit
                'UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.NoEdit
                'UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode)
            Case 2

        End Select
    End Sub

    Private Sub UGDetalles_BeforeRowsDeleted(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs) Handles UGDetalles.BeforeRowsDeleted
        ' para que no pregunte cuando borro una fila
        e.DisplayPromptMsg = False
    End Sub

    Private Sub UGDetalles_ClickCellButton(ByVal sender As Object, ByVal e As CellEventArgs) Handles UGDetalles.ClickCellButton
        If e.Cell.Band.Index = 1 Then ' carga cheques
            '    If e.Cell.Column.Index = 7 Then 'Agregar cheque
            '        If MessageBox.Show("    Confirmar ", "Agregar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '            If Reporte.Trim = "" Then
            '                If CantCheques <= 5 Then
            '                    AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))
            '                    CantCheques += 1
            '                End If
            '            Else
            '                AgregarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))

            '            End If
            '        End If
            '    Else ' eliminar cheque
            '        If MessageBox.Show("    Confirmar ", "Eliminar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '            If Reporte.Trim = "" Then
            '                If CantCheques > 0 Then
            '                    CantCheques -= 1
            '                End If
            '            End If
            '            EliminarCheques(DSDetalles.Tables("detalles").Rows(e.Cell.Row.Cells("item").Value - 1))
            '            SumoTotales()
            '        End If

            '    End If
            'Else ' buscar destinatario
            Select Case e.Cell.Column.Index
                Case 4
                    MuestraDestinatario(False)
                Case 5
                    MuestraDestinatario(True)
                Case 6
                    BuscoCuotas(UGDetalles.ActiveRow)
            End Select
        End If
    End Sub

    Private Sub AgregarCheques(ByRef RowSubItem As DataRow)
        Dim mydr As DataRow
        mydr = DTCheques.NewRow()
        mydr("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("Agregar") = ""
        DTCheques.Rows.Add(mydr)
    End Sub

    Private Sub EliminarCheques(ByRef RowSubItem As DataRow)
        With UGDetalles.ActiveRow
            DSDetalles.Tables("Detalles").Rows(RowSubItem.Item("pto_item") - 1).Item("Debe") -= UGDetalles.ActiveRow.Cells("monto").Value

            .Delete()
            Dim nCantChe As Integer = 0
            For Each rowDelche As DataRow In DTCheques.Rows
                If rowDelche.Item("item") = RowSubItem.Item("pto_item") Then
                    nCantChe += 1
                End If
            Next
            If nCantChe = 0 Then
                AgregarCheques(RowSubItem)
            End If
        End With
    End Sub

    Private Sub AgregarSubcuenta(ByRef RowSubItem As DataRow)
        Dim mydr As DataRow
        mydr = DTSubCuenta.NewRow()
        mydr.Item("proceso") = RowSubItem.Item("proceso")
        mydr("item") = RowSubItem.Item("pto_item")
        mydr("destinatario") = DTProf.Rows(0).Item("afi_nombre")
        mydr("matricula") = c_Titulo & n_Matricula
        mydr("tipodoc") = c_TipDoc
        mydr("nrodoc") = n_NroDoc
        DTSubCuenta.Rows.Add(mydr)
    End Sub

    Private Sub EliminarSubcuenta(ByRef item As Integer)
        Try
            DTSubCuenta.Rows(item).Delete() ' si da error porque no hay nada en la posicion que se quiere borrar
        Catch ex As Exception
        End Try

    End Sub

    'Private Sub UGDetalles_DoubleClickCell(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs) Handles UGDetalles.DoubleClickCell
    '    If UGDetalles.ActiveCell.IsInEditMode Then
    '        DesActivarEdision()
    '    Else
    '        ActivarEdision()
    '    End If
    'End Sub

    Private Sub UGDetalles_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGDetalles.InitializeRow
        Dim cell As UltraGridCell

        ' UGDetalles.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns
        UGDetalles.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free

        If e.Row.Band.Index = 0 Then
            e.Row.ExpandAll()
            If IsDBNull(e.Row.Cells("pto_tipmov").Value) Then

            Else
                If e.Row.Cells("pto_tipmov").Value = "I" Then
                    e.Row.Cells("Haber").Appearance.BackColor = Color.Red
                    e.Row.Cells("Haber").Appearance.ForeColor = Color.Black
                    e.Row.Cells("Debe").Appearance.BackColor = Color.Red
                    e.Row.Cells("Debe").Appearance.ForeColor = Color.Black
                Else
                    If e.Row.Cells("pto_tipmov").Value = "H" Then
                        cell = e.Row.Cells("Haber")
                    Else
                        cell = e.Row.Cells("Debe")
                    End If
                    cell.Appearance.BackColor = Color.Red
                    cell.Appearance.ForeColor = Color.White
                    cell.ActiveAppearance.ForeColor = Color.White
                End If
            End If
        End If
    End Sub


    Private Sub UGDetalles_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UGDetalles.KeyPress
        Try
            Select Case e.KeyChar
                Case ChrW(Keys.Enter)
                    If UGDetalles.ActiveCell Is Nothing Then
                        UGDetalles.PerformAction(UltraGridAction.ActivateCell)
                    End If
                    If UGDetalles.ActiveCell.IsInEditMode Then
                        DesActivarEdision()
                        ' si estoy cargando la cuenta
                        CargarCuentaManual()
                        SumoTotales()
                    Else
                        ActivarEdision()
                    End If
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CargarCuentaManual()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Dim rowItem As DataRow = DSDetalles.Tables(0).Rows(UGDetalles.ActiveRow.Index)
        If cell.Column.ToString = "Cuenta" Then
            Dim FrmCuenta As New FrmPlanCuentas
            If IsDBNull(cell.Value) Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
            ElseIf cell.Value = "" Then
                FrmCuenta.Condicion = "pla_imputa='Si'"
            Else
                FrmCuenta.Condicion = "pla_nropla='" & cell.Value & "' and pla_imputa='Si'"
            End If
            FrmCuenta.CargaPlanCuenta()
            If FrmCuenta.NumeroCuenta = "" Then
                FrmCuenta.ShowDialog()
            End If
            row.Cells("Cuenta").Value = FrmCuenta.NumeroCuenta
            row.Cells("Descripcion").Value = FrmCuenta.DescripcionCuenta
            row.Cells("Matricula").Value = FrmCuenta.NumeroSubCuenta
            row.Cells("pla_servicio").Value = FrmCuenta.Servicio
            If row.Cells("pla_servicio").Value = "Si" Then
                AgregarSubcuenta(rowItem)
            Else
                EliminarSubcuenta(UGDetalles.ActiveRow.Index)
            End If
            If Mid(row.Cells("Matricula").Value, 1, 3) = "BCO" Then
                AgregarCheques(rowItem)
            End If
            FrmCuenta.Dispose()
        End If
    End Sub

    Private Sub ActivarEdision()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Try
            '    If row.Index = 0 Then ' SOLO PERMITO MODIFICAR LA PRIMARA CUENTA
            Select Case cell.Band.Index
                Case 0
                    If cell.Column.ToString = "Haber" And row.Cells("pto_tipmov").Text = "H" And row.Cells("pto_formula").Text.Trim = "" Then
                        UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                        UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                    End If
                    If cell.Column.ToString = "Debe" And row.Cells("pto_tipmov").Text = "D" And row.Cells("pto_formula").Text.Trim = "" Then
                        UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                        UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                    End If
                    If row.Cells("pto_tipmov").Text = "I" Then
                        Select Case cell.Column.ToString
                            Case "Debe"
                                If row.Cells("haber").Value = 0 Then
                                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                                    UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                                End If
                            Case "Haber"
                                If row.Cells("Debe").Value = 0 Then
                                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.AllowEdit
                                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.AllowEdit
                                    UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                                End If
                            Case "Cuenta"
                                UGDetalles.DisplayLayout.Bands(0).Columns(0).CellActivation = Activation.AllowEdit
                                UGDetalles.PerformAction(UltraGridAction.EnterEditMode)
                        End Select
                    End If
                Case 1
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("banco").CellActivation = Activation.AllowEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.AllowEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.AllowEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.AllowEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.AllowEdit
                    '    UGDetalles.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)
                Case 2
                    UGDetalles.DisplayLayout.Bands(2).Columns(cell.Column.Index).CellActivation = Activation.AllowEdit
                    UGDetalles.PerformAction(UltraGridAction.EnterEditMode)

            End Select
            '    End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        End Try
    End Sub

    Private Sub DesActivarEdision()
        Dim cell As UltraGridCell = UGDetalles.ActiveCell
        Dim row As UltraGridRow = UGDetalles.ActiveRow
        Try
            Select Case cell.Band.Index
                Case 0
                    UGDetalles.DisplayLayout.Bands(0).Columns(0).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(0).Columns(2).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(0).Columns(3).CellActivation = Activation.NoEdit
                    UGDetalles.PerformAction(UltraGridAction.ExitEditMode)

                    ' BUSCO SI TIENE CHEQUES PARA CONTROLAR LOS MONTOS INGRESADOS
                    Dim nMontoChe As Double = 0
                    Dim lTieneChe As Boolean = False
                    'For Each rowche As DataRow In DTCheques.Rows
                    '    If rowche.Item("item") = row.Cells("pto_item").Value AndAlso Not IsDBNull(rowche.Item("Monto")) Then
                    '        nMontoChe += rowche.Item("Monto")
                    '        lTieneChe = True
                    '    End If
                    'Next
                    If lTieneChe Then
                        If nMontoChe <> row.Cells("Debe").Value + row.Cells("Haber").Value Then
                            If row.Cells("pto_tipmov").Value = "H" Then
                                row.Cells("Haber").Value = nMontoChe
                            Else
                                row.Cells("Debe").Value = nMontoChe
                            End If
                            MessageBox.Show("No coincide el monto ingresado con el de los cheques", "Cheques", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If
                    'Case 1
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("cheque").CellActivation = Activation.NoEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("FechaEmision").CellActivation = Activation.NoEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("FechaDiferida").CellActivation = Activation.NoEdit
                    '    UGDetalles.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.NoEdit
                    '    If cell.Column.Index = 6 Then
                    '        If DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("pto_tipmov") = "H" Then

                    '            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") = 0
                    '            For Each rowche As DataRow In DTCheques.Rows
                    '                If rowche.Item("item") = row.Cells("item").Value Then
                    '                    DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Haber") += rowche.Item("Monto")

                    '                End If
                    '            Next

                    '        Else

                    '            DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") = 0
                    '            For Each rowche As DataRow In DTCheques.Rows
                    '                If rowche.Item("item") = row.Cells("item").Value Then
                    '                    DSDetalles.Tables("Detalles").Rows(row.Cells("item").Value - 1).Item("Debe") += rowche.Item("Monto")
                    '                End If
                    '            Next

                    '        End If
                    '    End If
                Case 1
                    UGDetalles.DisplayLayout.Bands(2).Columns(2).CellActivation = Activation.NoEdit
                    UGDetalles.DisplayLayout.Bands(2).Columns(3).CellActivation = Activation.NoEdit
                    MuestraDestinatario(False)
            End Select
            UGDetalles.PerformAction(UltraGridAction.ExitEditMode)

        Catch ex As Exception
            MessageBox.Show("Controle los datos ingresados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SumoTotales()
        Dim nTotHaber As Double = 0
        Dim nTotDeber As Double = 0
        Dim nMontoFormula As Double = 0
        Dim Exprecion As String
        Dim nItem As Integer = 0
        Dim item(DSDetalles.Tables("Detalles").Rows.Count + 1) As Double
        Dim oSC As New ScriptControl
        oSC.Language = "VBScript"

        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            item(nItem) = RowTot.Item("debe") + RowTot.Item("haber")
        Next

        nItem = 0

        For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
            nItem += 1
            If RowTot.Item("pto_valor") <> 0 Then
                Exprecion = RowTot.Item("pto_valor")
                nMontoFormula = oSC.Eval(Exprecion)
                If RowTot.Item("pto_tipmov") = "D" Then
                    RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                Else
                    RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                End If
                item(nItem) = nMontoFormula
            Else
                If RowTot.Item("pto_formula").ToString.Trim <> "" Then
                    Exprecion = RowTot.Item("pto_formula")
                    For xItem = 1 To item.Length - 1
                        Exprecion = Exprecion.ToString.Replace("item" & Convert.ToString(Format(xItem, "00")), item(xItem))
                        Exprecion = Exprecion.ToString.Replace("Prestamo", Prestamo)
                    Next
                    Exprecion = Exprecion.ToString.Replace(",", ".")
                    nMontoFormula = oSC.Eval(Exprecion)
                    If RowTot.Item("pto_tipmov") = "D" Then
                        RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                    Else
                        RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                    End If
                    item(nItem) = nMontoFormula
                End If
            End If

            nTotDeber += Math.Round(RowTot.Item("debe"), 2)
            nTotHaber += Math.Round(RowTot.Item("haber"), 2)
            'nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
            'nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
        Next

        Dim nDifeRedondeo As Double = Math.Round(nTotDeber - nTotHaber, 2)
        'Dim nDifeRedondeo As Double = nTotDeber - nTotHaber
        'repaso para colocar la diferencia de redondeo
        If Math.Abs(nDifeRedondeo) < 0.09 Then
            nTotDeber = 0
            nTotHaber = 0

            'Aca controlo si es de sipres
            For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 And
                   RowTot.Item("Cuenta") = "31021300" Then
                    'If nDifeRedondeo < 0 Then
                    RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                    'ElseIf nDifeRedondeo > 0 Then
                    'RowTot.Item("haber") = RowTot.Item("haber") - nDifeRedondeo
                    'End If
                    nDifeRedondeo = 0
                End If
                nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
            Next
            'Fin controlo sipres

            If nDifeRedondeo <> 0 Then
                nTotDeber = 0
                nTotHaber = 0
                For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                    If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or
                       RowTot.Item("pto_formula").ToString.IndexOf("(") > 0 Then
                        'RowTot.Item("pto_formula").ToString.IndexOf("/") > 0 Or _
                        'RowTot.Item("pto_formula").ToString.IndexOf("+") > 0 Or _
                        If nDifeRedondeo <> 0 Then
                            'If nDifeRedondeo < 0 Then
                            If RowTot.Item("pto_tipmov") = "H" Then
                                'RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                                'nDifeRedondeo = 0
                            End If
                            '  Else
                            '      If RowTot.Item("pto_tipmov") = "D" Then
                            ' RowTot.Item("debe") = RowTot.Item("debe") - nDifeRedondeo
                            '  nDifeRedondeo = 0
                            ' End If

                            ' End If
                        End If
                    End If
                    nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                    nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
                Next
            End If

            If nDifeRedondeo <> 0 Then
                Dim rowTot As DataRow = DSDetalles.Tables("Detalles").Rows(DSDetalles.Tables("Detalles").Rows.Count - 1)
                If rowTot.Item("pto_tipmov") <> "I" Then ' SI ES UNA CUENTA INSERTADA
                    If rowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or
                       rowTot.Item("pto_formula").ToString.IndexOf("(") > 0 Then
                        If rowTot.Item("pto_tipmov") = "H" Then
                            rowTot.Item("haber") = rowTot.Item("haber") + nDifeRedondeo
                            nDifeRedondeo = 0
                        Else
                            'rowTot.Item("debe") = rowTot.Item("debe") + nDifeRedondeo
                        End If
                        'nDifeRedondeo = 0
                        nTotDeber = Math.Round((nTotDeber + rowTot.Item("debe")), 2)
                        nTotHaber = Math.Round((nTotHaber + rowTot.Item("haber")), 2)
                    End If
                End If
            End If
        End If

        TxtDebe.Text = Format(nTotDeber, "$ ###,##0.00")
        TxtHaber.Text = Format(nTotHaber, "$ ###,##0.00")
    End Sub

    Private Function CtrlMontosIngresados() As Boolean
        Dim nTotHaber As Double = 0
        Dim nTotDeber As Double = 0
        Try
            For Each RowTot As DataRow In DSDetalles.Tables("Detalles").Rows
                If RowTot.Item("debe") >= 0 And RowTot.Item("haber") >= 0 Then
                    nTotDeber += Math.Round(RowTot.Item("debe"), 2)
                    nTotHaber += Math.Round(RowTot.Item("haber"), 2)
                Else
                    CtrlMontosIngresados = False
                    Exit Function
                End If
            Next
        Catch ex As Exception
            CtrlMontosIngresados = False
            Exit Function
        End Try

        If nTotDeber > 0 And Math.Round(nTotDeber, 2) = Math.Round(nTotHaber, 2) Then
            CtrlMontosIngresados = True
        Else
            CtrlMontosIngresados = False
        End If
    End Function
    Private Sub CBConfirmar_Click(sender As Object, e As EventArgs) Handles CBConfirmar.Click
        If CtrlMontosIngresados() Then
            If n_Total < DSDetalles.Tables(0).Rows(0).Item("debe") Then
                MessageBox.Show("El importe supera el total que puede descontar", "Reintegro de honorarios")
                Exit Sub
            End If
            DSDetalles.Tables(0).Rows(0).Item("concepto2") = TxtConcepto2.Text
            DialogResult = DialogResult.OK
        Else
            MessageBox.Show("Compruebe los datos ingresados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If
    End Sub
    'Private Sub CierraAsiento()
    '    If cnn.AbrirConexion Then
    '        Dim miTrans As MySqlTransaction
    '        miTrans = cnn.InicioTransaccion
    '        Try
    '            Dim FechaHora As String
    '            FechaHora = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
    '            ' Dim nNroAsiento As Long = cnn.TomaCobte(UltraComboEditor1.Value, nPubNroCli, "ASIENT")
    '            Dim nNroAsiento As Long = cnn.TomaCobte(nPubNroIns, nPubNroCli, "ASIENT")
    '            Dim nNrocomprobante As Long = cnn.TomaCobte(UltraComboEditor1.Value, nPubNroCli, UltraTextEditor1.Text)
    '            Dim cTipDocDestinatario As String = ""
    '            Dim nNroDocDestinatario As Integer = 0
    '            Dim cTitMtrDestinatario As String = ""
    '            Dim nMatMtrDestinataria As Integer = 0
    '            Dim cTipDoc As String = ""
    '            Dim nNroDoc As Integer = 0
    '            Dim cTitMtr As String = ""
    '            Dim nMatMtr As Integer = 0
    '            Dim nItem As Integer = 0
    '            Dim ltieneCheques As Boolean = False
    '            For Each RowAsi As DataRow In DSDetalles.Tables("detalles").Rows
    '                If RowAsi.Item("Debe") > 0 Or RowAsi.Item("Haber") > 0 Then
    '                    If RowAsi.Item("Subcuenta") <> "" Then
    '                        cTipDocDestinatario = Mid(RowAsi.Item("subcuenta"), 1, 3)
    '                        nNroDocDestinatario = CInt(Mid(RowAsi.Item("subcuenta"), 4))
    '                        cTitMtrDestinatario = Mid(RowAsi.Item("Matricula"), 1, 2)
    '                        nMatMtrDestinataria = CInt(Mid(RowAsi.Item("Matricula"), 3))
    '                        cTipDoc = Mid(RowAsi.Item("subcuenta"), 1, 3)
    '                        nNroDoc = CInt(Mid(RowAsi.Item("subcuenta"), 4))
    '                        cTitMtr = Mid(RowAsi.Item("Matricula"), 1, 2)
    '                        nMatMtr = CInt(Mid(RowAsi.Item("Matricula"), 3))
    '                    Else
    '                        cTipDoc = ""
    '                        nNroDoc = 0
    '                        cTitMtr = ""
    '                        nMatMtr = 0
    '                    End If
    '                    ltieneCheques = False
    '                    For Each RowChe As DataRow In DSDetalles.Tables("cheques").Rows
    '                        If RowAsi.Item("pto_item") = RowChe.Item("item") Then
    '                            ltieneCheques = True
    '                            nItem += 1
    '                            cnn.ReplaceBD("insert into cheques set che_nrocli='" & nPubNroCli & "'," & _
    '                                                                  "che_origen='T'," & _
    '                                                                  "che_estado='C'," & _
    '                                                                  "che_nrochequera='" & 0 & "'," & _
    '                                                                  "che_letser=''," & _
    '                                                                  "che_nroser='" & RowChe.Item("cheque") & "'," & _
    '                                                                  "che_tipdoc='ENT'" & _
    '                                                                  "che_nrodoc='" & nPubNroCli & "'" & _
    '                                                                  "che_nroasi='" & nNroAsiento & "'," & _
    '                                                                  "che_item='" & nItem & "'," & _
    '                                                                  "che_fecha='" & FechaHora & "'," & _
    '                                                                  "che_fecche='" & Format(CDate(RowChe.Item("fechaEmision")), "yyyy-MM-dd") & "'," & _
    '                                                                  "che_fecdif='" & Format(CDate(RowChe.Item("fechaDiferida")), "yyyy-MM-dd") & "'," & _
    '                                                                  "che_tipbco='" & cTipDoc & "'," & _
    '                                                                  "che_nrobco='" & nNroDoc & "'," & _
    '                                                                  "che_cuebco='" & RowAsi.Item("cuenta") & "'," & _
    '                                                                  "che_impche='" & RowChe.Item("monto").ToString.Replace(",", ".") & "'")
    '                            cnn.ReplaceBD("insert into totales set tot_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                         "tot_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                         "tot_nrocli='" & nPubNroCli & "'," & _
    '                                         "tot_nrocom='" & nNrocomprobante & "'," & _
    '                                         "tot_item='" & nItem & "'," & _
    '                                         "tot_nropla='" & RowAsi.Item("cuenta") & "'," & _
    '                                         "tot_subpla='" & RowAsi.Item("subcuenta") & "'," & _
    '                                         "tot_titulo='" & cTitMtr & "'," & _
    '                                         "tot_matricula='" & nMatMtr & "'," & _
    '                                         "tot_tipdoc='" & cTipDoc & "'," & _
    '                                         "tot_nrodoc='" & nNroDoc & "'," & _
    '                                         "tot_nrocuo='" & 0 & "'," & _
    '                                         "tot_fecha='" & FechaHora & "'," & _
    '                                         "tot_nroser='" & RowChe.Item("cheque") & "'," & _
    '                                         "tot_fecche='" & Format(CDate(RowChe.Item("fechaEmision")), "yyyy-MM-dd") & "'," & _
    '                                         "tot_fecdif='" & Format(CDate(RowChe.Item("fechaDiferida")), "yyyy-MM-dd") & "'," & _
    '                                         "tot_debe='" & IIf(RowAsi.Item("debe") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," & _
    '                                         "tot_haber='" & IIf(RowAsi.Item("haber") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," & _
    '                                         "tot_tipdes='ENT'" & _
    '                                         "tot_nrodes='" & nPubNroCli & "'" & _
    '                                         "tot_nroasi='" & nNroAsiento & "'," & _
    '                                         "tot_caja='" & nCaja & "'," & _
    '                                         "tot_zeta='" & nZeta & "'," & _
    '                                         "tot_concepto='" & TxtConcepto.Text & "'")
    '                        End If
    '                    Next
    '                    If Not ltieneCheques Then
    '                        nItem += 1
    '                        cnn.ReplaceBD("insert into totales set tot_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                                      "tot_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                                      "tot_nrocli='" & nPubNroCli & "'," & _
    '                                                      "tot_nrocom='" & nNrocomprobante & "'," & _
    '                                                      "tot_item='" & nItem & "'," & _
    '                                                      "tot_nropla='" & RowAsi.Item("cuenta") & "'," & _
    '                                                      "tot_subpla='" & RowAsi.Item("subcuenta") & "'," & _
    '                                                      "tot_titulo='" & cTitMtr & "'," & _
    '                                                      "tot_matricula='" & nMatMtr & "'," & _
    '                                                      "tot_tipdoc='" & cTipDoc & "'," & _
    '                                                      "tot_nrodoc='" & nNroDoc & "'," & _
    '                                                      "tot_nrocuo='" & 0 & "'," & _
    '                                                      "tot_fecha='" & FechaHora & "'," & _
    '                                                      "tot_debe='" & RowAsi.Item("debe").ToString.Replace(",", ".") & "'," & _
    '                                                      "tot_haber='" & RowAsi.Item("haber").ToString.Replace(",", ".") & "'," & _
    '                                                      "tot_nroasi='" & nNroAsiento & "'," & _
    '                                                      "tot_caja='" & nCaja & "'," & _
    '                                                      "tot_zeta='" & nZeta & "'," & _
    '                                                      "tot_concepto='" & TxtConcepto.Text & "'")
    '                    End If
    '                End If
    '            Next
    '            cnn.ReplaceBD("insert into comproba set com_unegos='" & UltraComboEditor1.Value & "'," & _
    '                                              "com_proceso='" & UltraTextEditor1.Text & "'," & _
    '                                              "com_nrocli='" & nPubNroCli & "'," & _
    '                                              "com_nrocom='" & nNrocomprobante & "'," & _
    '                                              "com_nroasi='" & nNroAsiento & "'," & _
    '                                              "com_fecha='" & FechaHora & "'," & _
    '                                              "com_total='" & CDbl(TxtDebe.Text.Replace("$", "")) & "'," & _
    '                                              "com_tipdoc='" & cTipDocDestinatario & "'," & _
    '                                              "com_nrodoc='" & nNroDocDestinatario & "'," & _
    '                                              "com_titulo='" & cTitMtrDestinatario & "'," & _
    '                                              "com_matricula='" & nMatMtrDestinataria & "'," & _
    '                                              "com_caja='" & nCaja & "'," & _
    '                                              "com_zeta='" & nZeta & "'," & _
    '                                              "com_concepto1='" & TxtConcepto.Text & "'")
    '            ' cancelos las cuotas de los servicio
    '            If DTCuotasServicio.IsInitialized Then
    '                Dim nMontoCuota As Double
    '                For Each rowCuo As DataRow In DTCuotasServicio.Rows
    '                    If Not IsDBNull(rowCuo.Item("select")) AndAlso rowCuo.Item("select") Then
    '                        nMontoCuota = rowCuo.Item("debe") + rowCuo.Item("haber")
    '                        cnn.ReplaceBD("update totales set tot_imppag='" & nMontoCuota.ToString.Replace(",", ".") & "'," & _
    '                                                 "tot_asicancel='" & nNroAsiento & "'" & _
    '                                                 " where tot_nroasi=" & rowCuo.Item("tot_nroasi") & " and tot_item=" & rowCuo.Item("tot_item") & " and tot_nrocuo=" & rowCuo.Item("tot_nrocuo"))
    '                    End If
    '                Next
    '            End If
    '            miTrans.Commit()
    '        Catch ex As Exception
    '            miTrans.Rollback()
    '            MessageBox.Show(ex.Message)
    '        End Try
    '        cnn.CerrarConexion()
    '    End If
    'End Sub
    Private Sub ActualizoFechaInstitucion()
        If Not DSDetalles Is Nothing Then
            For Each RowDet As DataRow In DSDetalles.Tables("detalles").Rows
                RowDet.Item("fecha") = Format(UltraDateTimeEditor1.Value, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
                If Not UltraComboEditor1.Value Is Nothing Then
                    RowDet.Item("institucion") = UltraComboEditor1.Value
                End If
                If Not UCEdelegacion.Value Is Nothing Then
                    RowDet.Item("delegacion") = UCEdelegacion.SelectedIndex + 1
                End If
            Next
        End If
    End Sub
    Private Sub UltraDateTimeEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraDateTimeEditor1.KeyPress
        Tabular(e)
    End Sub
    Private Sub UltraDateTimeEditor1_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UltraDateTimeEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub
    Private Sub UltraComboEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraComboEditor1.KeyPress
        Tabular(e)
    End Sub
    Private Sub UltraComboEditor1_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UltraComboEditor1.ValueChanged
        ActualizoFechaInstitucion()
    End Sub
    Private Sub UltraTextEditor1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UltraTextEditor1.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscaProcesoIngresado()
        End If
    End Sub
    Private Sub BuscaProcesoIngresado()
        MuestraProcesos()
        If UltraLabel1.Visible Then
            UltraComboEditor1.SelectedIndex = DSProcesos.Tables("procesos").Rows(BSProcesos.Position).Item("pro_instit") - 1 ' Convert.ToInt16(Mid(Me.UltraTextEditor1.Text, 1, 2)) - 1
            UCEdelegacion.SelectedIndex = nPubNroCli - 1
            MuestraDetalles()
            SumoTotales()
        End If
    End Sub
    Private Sub FrmAsiento_Activated(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Activated
        UltraTextEditor1.Focus()
    End Sub
    Private Sub UltraTextEditor1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UltraTextEditor1.KeyPress
        Tabular(e)
    End Sub
    Private Sub UltraTextEditor1_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UltraTextEditor1.ValueChanged

    End Sub
    Private Sub TxtConcepto1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto1.KeyPress
        Tabular(e)
    End Sub
    Private Sub TxtConcepto2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto2.KeyPress
        Tabular(e)
    End Sub
    Private Sub TxtConcepto3_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TxtConcepto3.KeyPress
        Tabular(e)
    End Sub
    Private Sub UCEdelegacion_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles UCEdelegacion.ValueChanged
        ActualizoFechaInstitucion()
    End Sub
End Class