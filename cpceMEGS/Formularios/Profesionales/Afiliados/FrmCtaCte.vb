﻿Imports MySql.Data.MySqlClient
Imports System.Windows.Forms
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Public Class FrmCtaCte
    Private c_cnn As ConsultaBD
    Private r_rowAfi As DataRow
    Private DAServicios As MySqlDataAdapter
    Private DSServicios As DataSet
    Private DASubCuenta As MySqlDataAdapter
    Private DSSubCuenta As DataSet

    Public Sub New(ByRef conexion As ConsultaBD, ByVal rowProf As DataRow)
        InitializeComponent()
        c_cnn = conexion
        r_rowAfi = rowProf
    End Sub
    Private Sub FrmCtaCte_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CargaSubCuentas()
        CargaServicios()
    End Sub

    Private Sub CargaServicios()
        Try
            DAServicios = c_cnn.consultaBDadapter(
                "totales INNER JOIN plancuen ON pla_nropla=tot_nropla",
                "pla_nropla AS Cuenta,pla_nombre AS Servicio,SUM(tot_debe-tot_haber) AS saldo,tot_titulo,tot_matricula,tot_tipdoc,tot_nrodoc",
                "tot_tipdoc='" & r_rowAfi.Item("afi_tipdoc") & "' AND tot_nrodoc=" & r_rowAfi.Item("afi_nrodoc") & " AND pla_servicio='Si' AND tot_estado<>'9' GROUP BY pla_nropla"
            )
            'ESTO ES PARA EL DEBITO
            'Dim tabla As String = "totales inner join plancuen on pla_nropla=tot_nropla left join debito on deb_nropla=pla_nropla"
            'Dim campos As String = "pla_nropla as Cuenta,pla_nombre as Servicio,sum(tot_debe-tot_haber) as saldo,tot_titulo,tot_matricula,tot_tipdoc,tot_nrodoc,if (Id, 'Si', 'No') AS Debito"
            'Dim condiciones As String = "tot_tipdoc='" & r_rowAfi.Item("afi_tipdoc") & "' and tot_nrodoc=" & r_rowAfi.Item("afi_nrodoc") & " and pla_servicio='Si' and tot_estado<>'9' group by pla_nropla,deb_nropla"
            'DAServicios = c_cnn.consultaBDadapter(tabla, campos, condiciones)
            DSServicios = New DataSet
            DAServicios.Fill(DSServicios, "servicios")
            DSServicios.Tables(0).Columns.Add("Marcar", GetType(Boolean))

            With dgvServicios
                .DataSource = DSServicios.Tables(0)
                .Columns(0).Width = 65
                .Columns(0).ReadOnly = True
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Width = 300
                .Columns(1).ReadOnly = True
                .Columns(2).Width = 70
                .Columns(2).ReadOnly = True
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns(5).ReadOnly = False
                .Columns(7).Width = 50
                .Columns(3).Visible = False
                .Columns(4).Visible = False
                .Columns(5).Visible = False
                .Columns(6).Visible = False
            End With
            'ESTO ES PARA DEBITO
            'For i = 0 To dgvServicios.RowCount - 1
            'If dgvServicios.Rows(i).Cells("Debito").Value = "Si" Then
            'dgvServicios.Rows(i).Cells("Marcar").ReadOnly = True
            'End If
            'Next i
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CargaSubCuentas()
        Try
            DASubCuenta = c_cnn.consultaBDadapter(
                "subcuenta left join categorias on cat_codigo=scu_categoria",
                "scu_catant,scu_categoria,cat_descrip,scu_vigencia,scu_nota1",
                "scu_tipdoc='" & r_rowAfi.Item("afi_tipdoc") & "' and scu_nrodoc=" & r_rowAfi.Item("afi_nrodoc") & " order by scu_fecha DESC"
            )
            DSSubCuenta = New DataSet
            DASubCuenta.Fill(DSSubCuenta, "subcuenta")
            'DSSubCuenta.Tables(0).Columns.Add("Marcar", GetType(Boolean))
            With DgvCategorias
                .DataSource = DSSubCuenta.Tables(0)
                .Columns(0).Width = 80
                '.Columns(0).ReadOnly = True
                '.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Width = 80
                .Columns(2).Width = 250
                .Columns(3).Width = 80
                .Columns(4).Width = 250
                '.Columns(1).ReadOnly = True
                '.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                '.Columns(2).ReadOnly = False
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim clsExportarExcel As New ExportarExcel
        clsExportarExcel.ExportarDatosExcelView(DgvCategorias, "Historia Categoria - Profesional:" & r_rowAfi.Item("afi_matricula") & "-" & r_rowAfi.Item("afi_nombre"))
    End Sub

    Private Sub CButton4_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton4.Click
        Dim objWord As New Word.Application
        Dim Documento As New Word.Document
        Dim nombreDoc As String
        Dim row As DataGridViewRow = dgvServicios.CurrentRow
        Try
            Select Case row.Cells("cuenta").Value
                Case "13040100"
                    nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Aportes " & r_rowAfi.Item("afi_nombre").ToString.Trim & ".doc"
                    FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Aportes Sipres.doc", nombreDoc)
                Case "13051000"
                    nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Convenio Especial " & r_rowAfi.Item("afi_nombre").ToString.Trim & ".doc"
                    FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Ayuda Personal.doc", nombreDoc)
                Case "13051600"
                    nombreDoc = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & "Nota de Reclamo Convenio Especial " & r_rowAfi.Item("afi_nombre").ToString.Trim & ".doc"
                    FileSystem.FileCopy("W:\cpceMEGS\Notas Reclamos\Nota Reclamo Convenio Especial.doc", nombreDoc)
                Case Else
                    MessageBox.Show("No existe nota de reclamo para esta servicio")
                    Exit Sub
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try


        Documento = objWord.Documents.Open(nombreDoc)

        Documento.Bookmarks.Item("NombreProfesional").Range.Text = r_rowAfi.Item("afi_nombre")
        Documento.Bookmarks.Item("CalleProfesional").Range.Text = r_rowAfi.Item("afi_direccion")
        Documento.Bookmarks.Item("CiudadProfesional").Range.Text = r_rowAfi.Item("afi_localidad")
        Documento.Bookmarks.Item("Dia").Range.Text = Now.Day
        Documento.Bookmarks.Item("Mes").Range.Text = MonthName(Now.Month)
        Documento.Bookmarks.Item("Ano").Range.Text = Now.Year
        ' objWord.ActiveDocument.PrintOut()
        objWord.Visible = True
    End Sub

    Private Sub cbtImprimir_Click(sender As Object, e As EventArgs) Handles cbtImprimir.Click
        Try
            Dim cServiciosSelec As String = ""
            Dim DAMovimientos As MySqlDataAdapter
            Dim DTMovimientos As New DataTable
            Dim DSSaldo As New DataSet
            '  cServiciosSelec += "'"
            For Each sRow As DataRow In DSServicios.Tables(0).Rows
                If Not IsDBNull(sRow.Item("Marcar")) AndAlso sRow.Item("Marcar") Then
                    If cServiciosSelec = "" Then
                        cServiciosSelec += "'" & sRow.Item("cuenta").ToString & "'"
                    Else
                        cServiciosSelec += ",'" & sRow.Item("cuenta").ToString & "'"
                    End If
                End If
            Next
            ' SI NO SELECCIONO CARGO TODOS
            If cServiciosSelec = "" Then
                For Each sRow As DataRow In DSServicios.Tables(0).Rows
                    If cServiciosSelec = "" Then
                        cServiciosSelec += "'" & sRow.Item("cuenta").ToString & "'"
                    Else
                        cServiciosSelec += ",'" & sRow.Item("cuenta").ToString & "'"
                    End If
                Next
            End If
            DAMovimientos = c_cnn.consultaBDadapter(
                "totales",
                "sum(tot_debe-tot_haber) as saldo",
                "tot_tipdoc='" & r_rowAfi.Item("afi_tipdoc") & "' AND tot_nrodoc=" & r_rowAfi.Item("afi_nrodoc") & " AND tot_fecven<'" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & " 00:00:00' AND tot_nropla IN (" & cServiciosSelec & ") AND tot_estado<>'9'"
            )
            DAMovimientos.Fill(DSSaldo, "saldo")

            DAMovimientos = c_cnn.consultaBDadapter(
                "(totales INNER JOIN plancuen ON pla_nropla=tot_nropla) INNER JOIN afiliado ON afi_tipdoc=tot_tipdoc AND afi_nrodoc=tot_nrodoc",
                "concat(tot_proceso,'-',CAST(tot_nrocli as char),'-',CAST(tot_nrocom as char)) as tot_proceso,tot_nrocli,tot_nrocom,tot_nrocuo,CAST(tot_fecha as char) as tot_fecha,CAST(tot_fecven as char) as tot_fecven,tot_debe,tot_haber,tot_nropla,tot_titulo,CONCAT(CAST(tot_matricula as char),'-',afi_nombre) as tot_matricula,pla_nombre,tot_nroasi,tot_concepto AS concepto",
                "tot_tipdoc='" & r_rowAfi.Item("afi_tipdoc") & "' AND tot_nrodoc=" & r_rowAfi.Item("afi_nrodoc") & " AND tot_fecven BETWEEN '" & Format(Convert.ToDateTime(UDTdesde.Value), "yyyy-MM-dd") & " 00:00:00' AND '" & Format(Convert.ToDateTime(UDThasta.Value), "yyyy-MM-dd") & " 23:59:59' AND tot_nropla in (" & cServiciosSelec & ") AND tot_estado<>'9' ORDER BY tot_fecven,tot_nroasi,tot_nrocuo"
            )
            DAMovimientos.Fill(DTMovimientos)
            DTMovimientos.Columns.Add("Saldo_anterior", GetType(Double))
            DTMovimientos.Columns.Add("Periodo", GetType(String))

            For Each row As DataRow In DTMovimientos.Rows
                If IsDBNull(DSSaldo.Tables(0).Rows(0).Item("saldo")) Then
                    row.Item("Saldo_anterior") = 0
                Else
                    row.Item("Saldo_anterior") = DSSaldo.Tables(0).Rows(0).Item("saldo")
                End If
                row.Item("Periodo") = Format(Convert.ToDateTime(UDTdesde.Value), "dd/MM/yyyy") & "-" & Format(Convert.ToDateTime(UDThasta.Value), "dd/MM/yyyy")
            Next
            Dim frmResumen As New FrmReportes(DTMovimientos, , "CRCtaCte.rpt", "Resumen de cuenta")
            frmResumen.ShowDialog()

        Catch ex As Exception
            MessageBox.Show("excepcion: " & ex.Message, "Mostrando Reporte")
        End Try
    End Sub
End Class