﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports MSScriptControl

Public Class FrmOrdenPago
    Private cnn As ConsultaBD
    Private BSOrdenPago As BindingSource
    'Private d_DSServicios As DataSet
    Private DTServicios As DataTable
    Private BSFormaPago As BindingSource
    Private DSFormaPago As DataSet
    Private DAOrdenPago As MySqlDataAdapter
    Private cmdOrdenPago As MySqlCommandBuilder
    Private DTCuotasImpagas As New DataTable
    Private DTCuotasMoratoria As New DataTable
    Private DTPagAsi As DataTable
    Private lMultiAsiento As Boolean
    Private c_TipDoc As String
    Private c_NroDoc As Integer
    Private c_Titulo As String
    Private c_Matricula As Integer
    Private c_Correo As String
    Private c_ProcesoPaga As String
    Private n_ImportePaga As Double
    Private n_ItemAsiHonor1 As Integer
    Private n_ItemAsiHonor2 As Integer
    Private n_ItemAsiHonor3 As Integer
    'Trabajos.
    Private globalTrabajoIds() As Integer = Nothing 'globalTrabajoId En CargaPagos()
    Private globalTrabajoEstado As Integer = 8

    Public Sub New(ByRef conexion As ConsultaBD, ByRef cTipdoc As String, ByRef nNroDoc As Integer, ByRef cTitulo As String, ByRef nMatricula As Integer, ByRef cCorreo As String)
        InitializeComponent()
        cnn = conexion
        c_TipDoc = cTipdoc
        c_NroDoc = nNroDoc
        c_Titulo = cTitulo
        c_Matricula = nMatricula
        c_Correo = cCorreo
    End Sub
    'Public Sub New(ByVal cConexion As ConsultaBD, ByVal DSServicios As DataSet)
    '    InitializeComponent()
    '    cnn = cConexion
    '    d_DSServicios = DSServicios
    'End Sub
    Private Sub FrmOrdenPago_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Show()
        CargaOrdenPago()
    End Sub
    Private Sub CargaOrdenPago()
        Dim cuotas As New Calculos
        Dim DTMovimiento As DataTable
        'DAOrdenPago = cnn.consultaBDadapter("totales inner join plancuen on pla_nropla=tot_nropla", "pla_nropla as Cuenta,pla_nombre as Servicio,sum(tot_debe-tot_haber) as saldo,tot_titulo,tot_matricula,tot_tipdoc,tot_nrodoc", "tot_tipdoc='" & c_TipDoc & "' and tot_nrodoc=" & c_NroDoc & " and pla_servicio='Si' and tot_estado<>9 group by pla_nropla")
        'ESTA CONSULTA ES PARA QUE NO MUESTRE LA CUENTA SI LA TIENE COMO DEBITO
        Dim tabla As String = "totales INNER JOIN plancuen ON pla_nropla=tot_nropla LEFT JOIN debito ON deb_nropla=pla_nropla AND deb_tipdoc=tot_tipdoc AND deb_nrodoc=tot_nrodoc"
        Dim campos As String = "pla_nropla as Cuenta,pla_nombre as Servicio,sum(tot_debe-tot_haber) as saldo,tot_titulo,tot_matricula,tot_tipdoc,tot_nrodoc"
        Dim condiciones As String = "tot_tipdoc='" & c_TipDoc & "' AND tot_nrodoc=" & c_NroDoc & " AND pla_servicio = 'Si' AND tot_estado <> '9' AND Id IS NULL GROUP BY pla_nropla,deb_nropla"
        DAOrdenPago = cnn.consultaBDadapter(tabla, campos, condiciones)
        DTServicios = New DataTable
        DAOrdenPago.Fill(DTServicios)
        DTServicios.Columns.Add("Marcar", GetType(Boolean))
        Dim columns(0) As DataColumn
        columns(0) = DTCuotasImpagas.Columns("fecha")
        DTCuotasImpagas.PrimaryKey = columns

        Try
            For Each rowServ As DataRow In DTServicios.Rows
                '  DTMovimiento = cuotas.CuotasImpagas(rowServ.Item("Cuenta"), rowServ.Item("tot_tipdoc"), rowServ.Item("tot_nrodoc")) 'New DataTable("Movimiento")
                ' busco los reintegros pendientes que no se pagaron
                If rowServ.Item("Cuenta") = "21010300" Then
                    DTMovimiento = cuotas.CuotasImpagas(
                        rowServ.Item("Cuenta"), rowServ.Item("tot_tipdoc"), rowServ.Item("tot_nrodoc"), "2014-01-01"
                    )
                Else
                    DTMovimiento = cuotas.CuotasServicioImpagas(
                        rowServ.Item("Cuenta"), rowServ.Item("tot_tipdoc"), rowServ.Item("tot_nrodoc"), False, Now.Date, True
                    ) 'New DataTable("Movimiento")
                End If
                If DTCuotasImpagas.Rows.Count = 0 Then
                    DTCuotasImpagas = DTMovimiento.Clone
                End If

                For Each RowMov As DataRow In DTMovimiento.Rows
                    'Try
                    If RowMov.Item("procancel").ToString.Length > 0 And RowMov.Item("debe") + RowMov.Item("haber") - RowMov.Item("imppagado") > 0 Then
                        DTCuotasImpagas.ImportRow(RowMov)
                    End If
                    'Catch ex As Exception
                    '    MessageBox.Show(ex.Message)
                    'End Try
                Next
            Next
            If DTCuotasImpagas.Rows.Count = 0 Then
                MessageBox.Show("No se encontraron saldos en los servicios", "Orden de Pago", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            DTCuotasImpagas.Columns.Add("trabajoID", GetType(Integer))
            For Each RowCuota As DataRow In DTCuotasImpagas.Rows
                If RowCuota.Item("Cuenta") = "21010300" Then
                    RowCuota.Item("trabajoID") = cnn.ExisteTrabajoByNroAsientoLiquidacionGetId(RowCuota.Item("tot_nroasi"))
                End If
            Next
            BSOrdenPago = New BindingSource
            BSOrdenPago.DataSource = DTCuotasImpagas
            cmdOrdenPago = New MySqlCommandBuilder()

            UGOrdenPago.DataSource = BSOrdenPago

            With UGOrdenPago.DisplayLayout.Bands(0)
                .Columns(0).Width = 70
                .Columns(1).Width = 250
                .Columns(2).Width = 60
                .Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .Columns(3).Width = 70
                .Columns(4).Width = 80
                .Columns(5).CellAppearance.TextHAlign = HAlign.Right
                .Columns(5).Format = "c"
                .Columns(5).Width = 80
                .Columns(6).CellAppearance.TextHAlign = HAlign.Right
                .Columns(6).Format = "c"
                .Columns(6).Width = 80
                .Columns(7).CellAppearance.TextHAlign = HAlign.Right
                .Columns(7).Format = "c"
                .Columns(7).Width = 80
                .Columns(8).CellAppearance.TextHAlign = HAlign.Right
                .Columns(8).Format = "c"
                .Columns(8).Width = 80
                .Columns(9).CellAppearance.TextHAlign = HAlign.Right
                .Columns(9).Format = "c"
                .Columns(9).Width = 75
                '  .Columns(9).Hidden = True
                .Columns(10).Hidden = True
                .Columns(11).Hidden = True
                '.Columns(12).Hidden = True
                .Columns(12).CellAppearance.TextHAlign = HAlign.Right
                '.Columns(12).Format = "c"
                .Columns(12).Width = 20
                .Columns(13).Hidden = True
                .Columns(14).CellAppearance.TextHAlign = HAlign.Right
                .Columns(14).Format = "c"
                .Columns(14).Width = 65
                .Columns(15).Width = 40
            End With
            ' para ordenar por fecha
            Dim dv As DataView
            dv = New DataView(DTCuotasImpagas, "", "fecha DESC", DataViewRowState.CurrentRows)
            UGOrdenPago.DataSource = dv
            dv = Nothing
            'busco las cuentas de pago en el proceso 02H001 (Orden de pago reintegro honorarios)
            DSFormaPago = New DataSet
            DAOrdenPago = cnn.consultaBDadapter(
                "procetote inner join plancuen on pla_nropla=pto_nropla",
                "pto_codpro,pto_item,pto_nropla,pla_subcta,pla_nombre as Cuenta,pto_haber as Importe",
                "pto_codpro='02H001' and pto_tipmov='H'"
            )
            DAOrdenPago.Fill(DSFormaPago, "cuentas")
            BSFormaPago = New BindingSource
            BSFormaPago.DataSource = DSFormaPago.Tables(0)

            UGFormaPagos.DataSource = BSFormaPago
            With UGFormaPagos.DisplayLayout
                With .Bands(0)
                    .Columns(0).Hidden = True
                    .Columns(1).Hidden = True
                    .Columns(2).Hidden = True
                    .Columns(3).Hidden = True
                    .Columns(4).Width = 350
                    .Columns(4).CellActivation = Activation.NoEdit
                    .Columns(5).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(5).Format = "c"
                    .Columns(5).CellActivation = Activation.NoEdit
                End With
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub UGOrdenPago_CellChange(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGOrdenPago.CellChange
        Dim rowOP As UltraGridRow = UGOrdenPago.ActiveRow
        If e.Cell.Column.ToString = "Select" Then

            If rowOP.Cells("procancel").Text = "" Then
                rowOP.Cells("Select").Value = False
                MessageBox.Show("No tiene cargado el proceso que cancela")
                Exit Sub
            End If

            Dim ntotReintegro As Double = Convert.ToDouble(TxtTotReintegro.Text.Replace("$", ""))
            Dim ntotPagoServ As Double = Convert.ToDouble(TxPagoServicios.Text.Replace("$", ""))

            ' reintegro pendientes por un lado
            If rowOP.Cells("cuenta").Value = "21010300" Then

                If e.Cell.Text Then
                    If rowOP.Cells("Debe").Value > 0 Then
                        ntotReintegro -= (rowOP.Cells("Debe").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    Else
                        ntotReintegro += (rowOP.Cells("haber").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    End If
                Else
                    If rowOP.Cells("Debe").Value > 0 Then
                        ntotReintegro += (rowOP.Cells("Debe").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    Else
                        ntotReintegro -= (rowOP.Cells("haber").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    End If
                End If
            Else
                If e.Cell.Text Then
                    If rowOP.Cells("Debe").Value > 0 Then
                        ntotPagoServ += (rowOP.Cells("Debe").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    Else
                        ntotPagoServ -= (rowOP.Cells("haber").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    End If
                Else
                    If rowOP.Cells("Debe").Value > 0 Then
                        ntotPagoServ -= (rowOP.Cells("Debe").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    Else
                        ntotPagoServ += (rowOP.Cells("Haber").Value + rowOP.Cells("interes").Value) - rowOP.Cells("imppagado").Value
                    End If

                End If
            End If
            TxtTotReintegro.Text = FormatCurrency(ntotReintegro, 2, TriState.False, TriState.False)
            TxPagoServicios.Text = FormatCurrency(ntotPagoServ, 2, TriState.False, TriState.False)
            TxNetoReintegro.Text = FormatCurrency(ntotReintegro - ntotPagoServ, 2, TriState.False, TriState.False)
            DSFormaPago.Tables(0).Rows(BSFormaPago.Position).Item("importe") = ntotReintegro - ntotPagoServ
            If ntotReintegro - ntotPagoServ > 0 Then
                VBtConfirmar.Enabled = True
            Else
                VBtConfirmar.Enabled = False
            End If
        End If

    End Sub

    Private Sub UGOrdenPago_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGOrdenPago.InitializeRow
        UGOrdenPago.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
    End Sub
    Private Sub VBtConfirmar_Click(sender As Object, e As EventArgs) Handles VBtConfirmar.Click
        If MessageBox.Show("Confirma la Orden de Pago", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            UGOrdenPago.Height = 430
            VBtConfirmar.Visible = False
            UGFormaPagos.Visible = False
            CBtGenerarOrdPag.Visible = True
            CBtAgregarPagoServicios.Visible = True
            UltraGroupBox1.Visible = False
            CargoPagos()
        End If
    End Sub
    Private Sub UGFormaPagos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGFormaPagos.InitializeRow
        UGFormaPagos.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
        If e.Row.Band.Index = 0 Then
            e.Row.ExpandAll()
        End If
    End Sub

    Private Sub CargoPagos()
        Dim nImporteCtaPuenta As Double = 0
        Dim nTotalCtaPuenta As Double = 0
        Dim controlTotalDebe As Double = 0
        Dim controlTotalHaber As Double = 0
        Dim nInteres As Double = 0
        Dim nItemGlobal As Integer = 0
        Dim contadorGlobalTrabajoIds As Integer = 0
        Dim dtPagos As New DataTable
        Dim rowPagos As DataRow
        Dim DTProceso As DataTable
        dtPagos = DTCuotasImpagas.Clone
        Dim columns(0) As DataColumn
        columns(0) = dtPagos.Columns("procancel")
        dtPagos.PrimaryKey = columns

        DTPagAsi = New DataTable

        lMultiAsiento = False
        globalTrabajoIds = Nothing 'Reinicio los id de los trabajos

        ' agrupo por proceso que cancela
        For Each RowCuota As DataRow In DTCuotasImpagas.Rows
            If RowCuota.Item("select") AndAlso Not IsDBNull(RowCuota.Item("procancel")) Then
                Dim RowMod As DataRow = dtPagos.Rows.Find(RowCuota.Item("procancel"))
                If Not RowMod Is Nothing Then
                    RowMod.Item("debe") += RowCuota.Item("haber")
                    RowMod.Item("haber") += RowCuota.Item("debe") - RowCuota.Item("imppagado")
                    RowMod.Item("imppagado") += RowCuota.Item("imppagado")
                    RowMod.Item("interes") += RowCuota.Item("interes")
                Else
                    rowPagos = dtPagos.NewRow
                    rowPagos.Item("procancel") = RowCuota.Item("procancel")
                    rowPagos.Item("cuenta") = RowCuota.Item("cuenta")
                    rowPagos.Item("descripcion") = RowCuota.Item("descripcion")
                    rowPagos.Item("debe") = RowCuota.Item("haber")
                    rowPagos.Item("haber") = RowCuota.Item("debe") - RowCuota.Item("imppagado")
                    rowPagos.Item("imppagado") = RowCuota.Item("imppagado")
                    rowPagos.Item("interes") = RowCuota.Item("interes")
                    dtPagos.Rows.Add(rowPagos)
                End If

                If Not IsDBNull(RowCuota.Item("trabajoID")) Then
                    'Cargo el trabajoID
                    ReDim Preserve globalTrabajoIds(contadorGlobalTrabajoIds)
                    globalTrabajoIds(contadorGlobalTrabajoIds) = RowCuota.Item("trabajoID")
                    contadorGlobalTrabajoIds += 1
                End If
            End If
        Next
        'si tiene mas de un registro es multiproceso
        If dtPagos.Rows.Count > 1 Then
            lMultiAsiento = True
        End If
        'si tiene mas de un trabajo seleccionado
        If globalTrabajoIds.Count > 1 Then
            cnn.trabajoIds = globalTrabajoIds 'Guardo varios IDs
            cnn.trabajoEstado = globalTrabajoEstado
            cnn.trabajoNroAsientoActualizar = False 'No actualizo el numero de asiento
            cnn.trabajoDestinatario = c_Correo
        ElseIf globalTrabajoIds.Count = 1 Then
            cnn.trabajoId = globalTrabajoIds(0) 'Guardo un solo ID
            cnn.trabajoEstado = globalTrabajoEstado
            cnn.trabajoNroAsientoActualizar = False 'No actualizo el numero de asiento
            cnn.trabajoDestinatario = c_Correo
        End If
        'Si entra al Else es porque desmarco la casilla de Enviar Correo
        If CBEnviarCorreo.Checked() Then
            cnn.trabajosEnviarCorreo = Nothing
        Else
            cnn.trabajosEnviarCorreo = "NOENVIAR"
        End If

        For Each RowAsiCan As DataRow In dtPagos.Rows
            If RowAsiCan.Item("procancel") = "02H001" Then ' si es el proceso de pago honorarios dejo en blanco el datatable
                DAOrdenPago = cnn.consultaBDadapter(
                    "(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)",
                    "pto_codpro,pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio",
                    "pto_codpro='NOEXISTEELPROCETOTE' order by pto_item"
                )
            Else
                DAOrdenPago = cnn.consultaBDadapter(
                    "(procetote inner join plancuen on pla_nropla=pto_nropla) left join afiliado on afi_tipdoc=mid(pla_subcta,1,3) and afi_nrodoc=mid(pla_subcta,4,8)",
                    "pto_codpro,pto_nropla as Cuenta,pla_nombre as Descripcion,pto_debe as Debe,pto_haber as Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta as SubCuenta,concat(afi_titulo,CAST(afi_matricula AS CHAR)) as Matricula,pla_servicio",
                    "pto_codpro='" & RowAsiCan.Item("procancel") & "' order by pto_item"
                )
            End If
            DTProceso = New DataTable
            DAOrdenPago.Fill(DTProceso)
            Dim nItem As Integer = 0
            nInteres = 0
            nImporteCtaPuenta = 0
            For Each rowPro As DataRow In DTProceso.Rows
                If nItem = 0 Then
                    If RowAsiCan.Item("procancel").ToString.Substring(0, 2) = "01" Then ' SI ES SIPRES CARGO LA CUENTA PUENTA
                        rowPro.Item("cuenta") = "13070100"
                        rowPro.Item("descripcion") = "SIPRES -3314-(Deudor Consejo)"
                        nImporteCtaPuenta = Math.Round(RowAsiCan.Item("haber") + RowAsiCan.Item("interes"), 2)
                    Else
                        rowPro.Item("cuenta") = "21010300"
                        rowPro.Item("descripcion") = "Honorarios a Reintegrar"
                    End If

                    rowPro.Item("debe") = Math.Round(RowAsiCan.Item("haber") + RowAsiCan.Item("interes"), 2)
                    rowPro.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
                    rowPro.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")
                End If

                For Each rowCue As DataRow In dtPagos.Rows
                    If rowPro.Item("cuenta") = rowCue.Item("cuenta") Then
                        'nInteres += rowCue.Item("interes")
                        rowPro.Item("haber") = Math.Round(rowCue.Item("haber"), 2)
                        rowPro.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
                        rowPro.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")
                    ElseIf rowPro.Item("cuenta") = "22050200" And rowPro.Item("pto_codpro") = rowCue.Item("ProCancel") Then
                        rowPro.Item("haber") = Math.Round(rowCue.Item("interes"), 2)
                    End If
                Next
                nItem += 1
            Next

            Dim nTotHaber As Double = 0
            Dim nTotDeber As Double = 0
            Dim nMontoFormula As Double = 0
            Dim Exprecion As String

            Dim item(DTProceso.Rows.Count + 1) As Double
            Dim oSC As New ScriptControl
            oSC.Language = "VBScript"

            nItem = 0

            For Each RowTot As DataRow In DTProceso.Rows
                nItem += 1
                'item(nItem) = Math.Round(RowTot.Item("debe") + RowTot.Item("haber"), 2)
                item(nItem) = RowTot.Item("debe") + RowTot.Item("haber")
            Next

            nItem = 0

            For Each RowTot As DataRow In DTProceso.Rows
                nItem += 1
                If RowTot.Item("pto_valor") <> 0 Then
                    Exprecion = RowTot.Item("pto_valor")
                    nMontoFormula = oSC.Eval(Exprecion)
                    If RowTot.Item("pto_tipmov") = "D" Then
                        RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                    Else
                        RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                    End If
                    item(nItem) = nMontoFormula
                Else
                    If RowTot.Item("pto_formula").ToString.Trim <> "" Then
                        Exprecion = RowTot.Item("pto_formula")
                        For xItem = 1 To item.Length - 1
                            Exprecion = Exprecion.ToString.Replace("item" & Convert.ToString(Format(xItem, "00")), item(xItem))
                        Next
                        Exprecion = Exprecion.ToString.Replace(",", ".")
                        nMontoFormula = oSC.Eval(Exprecion)
                        If RowTot.Item("pto_tipmov") = "D" Then
                            RowTot.Item("debe") = Math.Round(nMontoFormula, 2)
                        Else
                            RowTot.Item("haber") = Math.Round(nMontoFormula, 2)
                        End If
                        item(nItem) = nMontoFormula
                    End If
                End If

                RowTot.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
                RowTot.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")

                nTotDeber += Math.Round(RowTot.Item("debe"), 2)
                nTotHaber += Math.Round(RowTot.Item("haber"), 2)
            Next

            Dim nDifeRedondeo As Double = Math.Round(nTotDeber - nTotHaber, 2)
            ' repaso para colocar la diferencia de redondeo
            If Math.Abs(nDifeRedondeo) < 0.09 Then
                nTotDeber = 0
                nTotHaber = 0

                'Aca controlo si es de sipres
                For Each RowTot As DataRow In DTProceso.Rows
                    If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 And
                   RowTot.Item("Cuenta") = "31021300" Then
                        'If nDifeRedondeo < 0 Then
                        RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                        'ElseIf nDifeRedondeo > 0 Then
                        'RowTot.Item("haber") = RowTot.Item("haber") - nDifeRedondeo
                        'End If
                        nDifeRedondeo = 0
                    End If
                    nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                    nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
                Next
                'Fin controlo sipres

                If nDifeRedondeo <> 0 Then
                    nTotDeber = 0
                    nTotHaber = 0
                    For Each RowTot As DataRow In DTProceso.Rows
                        If RowTot.Item("pto_formula").ToString.IndexOf("*") > 0 Or
                           RowTot.Item("pto_formula").ToString.IndexOf("(") > 0 Then
                            'RowTot.Item("pto_formula").ToString.IndexOf("/") > 0 Or
                            'RowTot.Item("pto_formula").ToString.IndexOf("+") > 0 Or
                            'RowTot.Item("pto_formula").ToString.IndexOf("-") > 0 Then
                            If nDifeRedondeo <> 0 Then
                                'If nDifeRedondeo < 0 Then
                                If RowTot.Item("pto_tipmov") = "H" Then
                                    'RowTot.Item("haber") = RowTot.Item("haber") + nDifeRedondeo
                                    'nDifeRedondeo = 0
                                End If
                                'Else
                                '    If RowTot.Item("pto_tipmov") = "D" Then
                                '            RowTot.Item("debe") = RowTot.Item("debe") - nDifeRedondeo
                                '            nDifeRedondeo = 0
                                '        End If
                                '    End If
                            End If
                        End If
                        nTotDeber = Math.Round((nTotDeber + RowTot.Item("debe")), 2)
                        nTotHaber = Math.Round((nTotHaber + RowTot.Item("haber")), 2)
                    Next
                End If
            End If
            ' BORRO LAS CUENTAS QUE NO SE USAN
            For X As Integer = DTProceso.Rows.Count - 1 To 1 Step -1
                If DTProceso.Rows(X).Item("debe") = 0 And DTProceso.Rows(X).Item("haber") = 0 Then
                    DTProceso.Rows.RemoveAt(X)
                End If
            Next

            If DTPagAsi.Rows.Count = 0 Then
                DTPagAsi = DTProceso.Clone
            End If

            For Each rowP As DataRow In DTProceso.Rows
                nItemGlobal += 1
                rowP.Item("pto_item") = nItemGlobal
                DTPagAsi.ImportRow(rowP)
            Next

            nTotalCtaPuenta += nImporteCtaPuenta
        Next

        If DTPagAsi.Rows.Count = 0 Then ' SI ES 0 NO SE SELECCIONO NINGUN SERVICIO PARA CANCELAR
            DAOrdenPago = cnn.consultaBDadapter(
                "(procetote inner join plancuen On pla_nropla=pto_nropla) left join afiliado On afi_tipdoc=mid(pla_subcta,1,3) And afi_nrodoc=mid(pla_subcta,4,8)",
                "pto_codpro,pto_nropla As Cuenta,pla_nombre As Descripcion,pto_debe As Debe,pto_haber As Haber,pto_tipmov,pto_item,pto_valor,pto_formula,pla_subcta As SubCuenta,concat(afi_titulo,CAST(afi_matricula As Char)) As Matricula,pla_servicio",
                "pto_codpro='02H001' order by pto_item"
            )
            DTProceso = New DataTable
            DAOrdenPago.Fill(DTProceso)
            DTPagAsi = DTProceso.Clone
        End If
        nItemGlobal += 1
        ' AGREGOS COLUMNAS DE PROCESO, INSTITUCION,delegacion,FECHA,VENCIMIENTO,CUOTAS,DESTINATARIO Y CARGO LOS VALORES PARA GRABAR EL ASIENTO
        Dim newCol As DataColumn
        newCol = New DataColumn("imppagado", Type.GetType("System.Double"))
        newCol.DefaultValue = 0
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("proceso", Type.GetType("System.String"))
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("institucion", Type.GetType("System.Int32"))
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("delegacion", Type.GetType("System.Int32"))
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("fecha", Type.GetType("System.String"))
        newCol.DefaultValue = Format(Now.Date, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("fecven", Type.GetType("System.String"))
        newCol.DefaultValue = Format(Now.Date, "yyyy-MM-dd")
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("cuotas", Type.GetType("System.Int32"))
        newCol.DefaultValue = 1
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("destinatario", Type.GetType("System.Boolean"))
        newCol.DefaultValue = True
        DTPagAsi.Columns.Add(newCol)
        newCol = New DataColumn("concepto2", Type.GetType("System.String"))
        newCol.DefaultValue = ""
        DTPagAsi.Columns.Add(newCol)
        ' creo el asiento de reintegro de honorarios
        ' SI ES CERO NO CREO
        ' If Convert.ToDouble(TxNetoReintegro.Text.Replace("$", "")) > 0 Then
        rowPagos = DTPagAsi.NewRow
        rowPagos.Item("pto_codpro") = "02H001"
        rowPagos.Item("cuenta") = "21010300"
        rowPagos.Item("descripcion") = "Honorarios a Reintegrar"
        rowPagos.Item("debe") = Math.Round(Convert.ToDouble(TxNetoReintegro.Text.Replace("$", "")) + nTotalCtaPuenta, 2)
        rowPagos.Item("haber") = 0
        rowPagos.Item("pto_tipmov") = "D"
        rowPagos.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
        rowPagos.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")
        rowPagos.Item("destinatario") = True
        rowPagos.Item("pto_item") = nItemGlobal
        DTPagAsi.Rows.Add(rowPagos)
        n_ItemAsiHonor1 = nItemGlobal - 1
        ' End If
        ' si tiene cuanta puente
        ' If nTotalCtaPuenta > 0 Then
        nItemGlobal += 1
        rowPagos = DTPagAsi.NewRow
        rowPagos.Item("pto_codpro") = "02H001"
        rowPagos.Item("cuenta") = "13070200"
        rowPagos.Item("descripcion") = "CONSEJO (Acreedor SIPRES)"
        rowPagos.Item("haber") = Math.Round(nTotalCtaPuenta, 2)
        rowPagos.Item("debe") = 0
        rowPagos.Item("pto_tipmov") = "H"
        rowPagos.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
        rowPagos.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")
        rowPagos.Item("destinatario") = True
        rowPagos.Item("pto_item") = nItemGlobal
        DTPagAsi.Rows.Add(rowPagos)
        n_ItemAsiHonor2 = nItemGlobal - 1
        'End If
        For Each rowForPag As DataRow In DSFormaPago.Tables(0).Rows
            If rowForPag.Item("importe") > 0 Then
                nItemGlobal += 1
                rowPagos = DTPagAsi.NewRow
                rowPagos.Item("pto_codpro") = "02H001"
                rowPagos.Item("cuenta") = rowForPag.Item("pto_nropla")
                rowPagos.Item("descripcion") = rowForPag.Item("cuenta")
                rowPagos.Item("debe") = 0
                rowPagos.Item("haber") = Math.Round(rowForPag.Item("importe"), 2) ' - nTotalCtaPuenta
                rowPagos.Item("pto_tipmov") = "H"
                rowPagos.Item("subcuenta") = DTServicios.Rows(0).Item("tot_tipdoc") & DTServicios.Rows(0).Item("tot_nrodoc")
                rowPagos.Item("matricula") = DTServicios.Rows(0).Item("tot_titulo") & DTServicios.Rows(0).Item("tot_matricula")
                rowPagos.Item("pto_item") = nItemGlobal
                DTPagAsi.Rows.Add(rowPagos)
                n_ItemAsiHonor3 = nItemGlobal - 1
            End If
        Next
        ' agrego columna de item dtcuotaimpagas
        newCol = New DataColumn("item", Type.GetType("System.Int32"))
        newCol.DefaultValue = 0
        DTCuotasImpagas.Columns.Add(newCol)
        ' agrego a cada item el proceso, institucion, fecha y a cada cuota impaga le asigno el item para la cancelacion
        For Each RowAdcional As DataRow In DTPagAsi.Rows
            RowAdcional.Item("proceso") = RowAdcional.Item("pto_codpro")
            RowAdcional.Item("institucion") = CInt(Mid(RowAdcional.Item("pto_codpro"), 2, 1))
            RowAdcional.Item("delegacion") = nPubNroCli
            '    RowAdcional.Item("fecha") = Format(Now.Date, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
            RowAdcional.Item("imppagado") = Math.Round(RowAdcional.Item("debe") + RowAdcional.Item("haber"), 2)
            ' cargo el item del asiento a las cuotas impagas para cancelarlas
            For Each RowCuoImp As DataRow In DTCuotasImpagas.Rows
                If Not IsDBNull(RowCuoImp.Item("procancel")) AndAlso RowCuoImp.Item("procancel") = RowAdcional.Item("pto_codpro") And RowCuoImp.Item("cuenta") = RowAdcional.Item("cuenta") Then
                    RowCuoImp.Item("item") = RowAdcional.Item("pto_item")
                End If
            Next
            'sumo los totales
            controlTotalDebe += RowAdcional.Item("debe")
            controlTotalHaber += RowAdcional.Item("haber")
        Next

        UGOrdenPago.DataSource = DTPagAsi
        UGOrdenPago.Text = "Asientos que se generaran"
        With UGOrdenPago.DisplayLayout.Bands(0)
            .Columns(6).Hidden = True
            .Columns(5).Hidden = True
            .Columns(7).Hidden = True
            .Columns(8).Hidden = True
            .Columns(11).Hidden = True
        End With

        'Seteo totales Debe y Haber
        LabelMontoDebe.Text = controlTotalDebe
        LabelMontoHaber.Text = controlTotalHaber
    End Sub
    Private Sub CBtGenerarOrdPag_Click(sender As Object, e As EventArgs) Handles CBtGenerarOrdPag.Click
        Dim nZeta As Integer
        Dim nCaja As Integer
        If cnn.CajaActiva Then
            nZeta = cnn.Zeta
            nCaja = cnn.Caja
        Else
            nZeta = 0
            nCaja = 0
        End If
        If MessageBox.Show("Confirma la Orden de Pago", "Orden de Pago", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
            cnn.GraboAsiento(DTPagAsi, Nothing, DTCuotasImpagas, DTCuotasMoratoria, "", "", "", "", nCaja, nZeta, lMultiAsiento)
            Close()
        End If
    End Sub
    Private Sub CBtAgregarPagoServicios_Click(sender As Object, e As EventArgs) Handles CBtAgregarPagoServicios.Click
        Dim FrmPagServ As New FrmPagoServicios(c_TipDoc, c_NroDoc, c_Titulo, c_Matricula, DTPagAsi.Rows(n_ItemAsiHonor1).Item("debe"))
        If FrmPagServ.ShowDialog() = DialogResult.OK Then
            Dim DTPagServ As DataTable = FrmPagServ.GetDetalles
            Dim rowPagos As DataRow
            Dim nItemglobal As Integer = DTPagAsi.Rows.Count
            Dim nTotalPago As Double = DTPagServ.Rows(0).Item("debe")
            Dim nCtaPuenta As Double = 0
            ' si tiene cuanta puente
            If nTotalPago > 0 Then
                If DTPagServ.Rows(0).Item("proceso").ToString.Substring(0, 2) = "01" Then
                    '   nCtaPuenta = nTotalPago
                    DTPagAsi.Rows(n_ItemAsiHonor2).Item("haber") += nTotalPago
                Else
                    DTPagAsi.Rows(n_ItemAsiHonor1).Item("debe") -= nTotalPago

                End If
                DTPagAsi.Rows(n_ItemAsiHonor3).Item("haber") -= nTotalPago

                If DTPagServ.Rows(0).Item("proceso").ToString = "01RCMO" Or DTPagServ.Rows(0).Item("proceso").ToString = "01DCMO" Then
                    DTCuotasMoratoria = FrmPagServ.GetCuotasServicios
                Else
                    If FrmPagServ.GetCuotasServicios.Rows.Count > 0 Then
                        If DTCuotasImpagas.Rows.Count = 0 Then
                            DTCuotasImpagas = FrmPagServ.GetCuotasServicios.Clone
                        End If
                        For Each rowCI As DataRow In FrmPagServ.GetCuotasServicios.Rows
                            If rowCI.Item("Select") Then
                                DTCuotasImpagas.ImportRow(rowCI)
                            End If
                        Next
                    End If
                End If
            End If
            For Each rowdet As DataRow In FrmPagServ.GetDetalles.Rows
                If rowdet.Item("haber") > 0 Or rowdet.Item("debe") > 0 Then
                    lMultiAsiento = True
                    nItemglobal += 1
                    rowPagos = DTPagAsi.NewRow
                    rowPagos.Item("pto_codpro") = rowdet.Item("proceso")
                    rowPagos.Item("cuenta") = rowdet.Item("cuenta")
                    rowPagos.Item("descripcion") = rowdet.Item("descripcion")
                    rowPagos.Item("haber") = rowdet.Item("haber")
                    rowPagos.Item("debe") = rowdet.Item("debe")
                    rowPagos.Item("pto_tipmov") = rowdet.Item("pto_tipmov")
                    rowPagos.Item("subcuenta") = c_TipDoc & c_NroDoc
                    rowPagos.Item("matricula") = c_Titulo & c_Matricula
                    rowPagos.Item("destinatario") = True
                    rowPagos.Item("pto_item") = nItemglobal
                    rowPagos.Item("imppagado") = rowdet.Item("debe") + rowdet.Item("haber")
                    rowPagos.Item("proceso") = rowdet.Item("proceso")
                    rowPagos.Item("institucion") = rowdet.Item("proceso").ToString.Substring(0, 2)
                    rowPagos.Item("delegacion") = nPubNroCli
                    rowPagos.Item("concepto2") = rowdet.Item("concepto2")
                    DTPagAsi.Rows.Add(rowPagos)
                End If
            Next
        End If
    End Sub
End Class