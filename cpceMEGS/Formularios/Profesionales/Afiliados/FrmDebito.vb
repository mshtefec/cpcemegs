﻿Imports MySql.Data.MySqlClient
Public Class FrmDebito
    Private c_cnn As ConsultaBD
    Private r_rowAfi As DataRow
    Private DAServicios As MySqlDataAdapter
    Private DSServicios As DataSet
    Private DASubCuenta As MySqlDataAdapter
    Private DSSubCuenta As DataSet

    Public Sub New(ByRef conexion As ConsultaBD, ByVal rowProf As DataRow)
        InitializeComponent()
        c_cnn = conexion
        r_rowAfi = rowProf
    End Sub

    Private Sub FrmCtaCte_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'TODO: esta línea de código carga datos en la tabla 'DsDebito.plancuen' Puede moverla o quitarla según sea necesario.
        PlancuenTableAdapter.Fill(DsDebito.plancuen)
        'TODO: esta línea de código carga datos en la tabla 'DsDebito.debito' Puede moverla o quitarla según sea necesario.
        DebitoTableAdapter.FillByAfiliado(DsDebito.debito, r_rowAfi.Item("afi_tipdoc"), r_rowAfi.Item("afi_nrodoc"))
    End Sub

    Private Sub DGVServicios_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles ServiciosDataGridView.CellValueChanged
        'Si cambio select de Cuenta entra
        If ServiciosDataGridView.Columns(e.ColumnIndex).Name = "Cuenta" Then
            updateDescripcionCell()
        End If
    End Sub

    Private Sub updateDescripcionCell()
        Dim DTDescripcion As New DataTable()
        Dim Celda As DataGridViewTextBoxCell = DirectCast(ServiciosDataGridView.CurrentRow.Cells("Servicio"), DataGridViewTextBoxCell)
        DTDescripcion = PlancuenTableAdapter.GetDataByNropla(ServiciosDataGridView.CurrentRow.Cells("Cuenta").Value)
        Celda.Value = DTDescripcion.Rows(0).Item("Servicio").ToString
        ServiciosDataGridView.CurrentRow.Cells("Tipo").Value = r_rowAfi.Item("afi_tipdoc")
        ServiciosDataGridView.CurrentRow.Cells("Nro").Value = r_rowAfi.Item("afi_nrodoc")
        Dim cbox As DataGridViewComboBoxCell = DirectCast(ServiciosDataGridView.CurrentRow.Cells("Debito"), DataGridViewComboBoxCell)
        cbox.Selected = 0
    End Sub

    Private Sub BindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorSaveItem.Click
        'Controlo permiso Debito automatico Create
        If controlAcceso(1, 6, "C", True) = True Then
            Validate()
            DebitoBindingSource.EndEdit()
            Try
                If MessageBox.Show("Confirmar la actualizacion", "Debito", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    DebitoTableAdapter.Adapter.Update(DsDebito)
                End If
            Catch ex As MySqlException
                If ex.Message.StartsWith("Duplicate entry") Then
                    MessageBox.Show("Cuenta para debito duplicada en el profesional")
                Else
                    MessageBox.Show(ex.Message)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class