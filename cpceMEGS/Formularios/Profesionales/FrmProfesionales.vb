﻿Imports MySql.Data.MySqlClient

Public Class FrmProfesionales
    Private cnn As New ConsultaBD(True)
    Private DSProfesional As DataSet
    Private DAProfesional As MySqlDataAdapter
    Private CMDProfesional As MySqlCommandBuilder
    Private dvProfesional As DataView
    Private bsProfesional As BindingSource
    Private bmbProfesional As BindingManagerBase
    Private cFiltroBusqueda As String = ""
    Private n_NroTab As Integer = 0
    Private l_BtnSeleccionar As Boolean = False
    Private l_nombreLike As String = ""
    Private d_DTPadron As DataTable

    Public Sub New(Optional ByVal NroTab As Integer = 0, Optional ByVal lBotonSeleccionar As Boolean = False, Optional ByVal nombreLike As String = "")
        InitializeComponent()

        n_NroTab = NroTab
        l_BtnSeleccionar = lBotonSeleccionar
        l_nombreLike = nombreLike
    End Sub

    Public ReadOnly Property GetValues() As DataTable
        Get
            Return d_DTPadron
        End Get
    End Property
    Private Sub FrmProfesionales_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        '1 Profesionales 2 Comitentes 3 Proveedores 4 Bancos 5 Empleados
        'Controlo permisos
        If controlAcceso(1, 1, , False) = False Then
            UltraTabPageControlProfesionales.Tab.Enabled = False
            UltraTabPageControlProfesionales.Tab.Visible = False
        End If
        If controlAcceso(1, 2, , False) = False Then
            UltraTabPageControlComitentes.Tab.Enabled = False
            UltraTabPageControlComitentes.Tab.Visible = False
        End If
        If controlAcceso(1, 3, , False) = False Then
            UltraTabPageControlProveedores.Tab.Enabled = False
            UltraTabPageControlProveedores.Tab.Visible = False
        End If
        If controlAcceso(1, 4, , False) = False Then
            UltraTabPageControlBancos.Tab.Enabled = False
            UltraTabPageControlBancos.Tab.Visible = False
        End If
        If controlAcceso(1, 5, , False) = False Then
            UltraTabPageControlEmpleados.Tab.Enabled = False
            UltraTabPageControlEmpleados.Tab.Visible = False
        End If
        'Fin controlo permisos
        Show()
        Select Case n_NroTab
            Case 0 ' profesionales
                UltraTabControl1.SelectedTab = UltraTabPageControlProfesionales.Tab
            Case 1 ' comitentes
                UltraTabControl1.SelectedTab = UltraTabPageControlComitentes.Tab
            Case 2 ' proveedores
                UltraTabControl1.SelectedTab = UltraTabPageControlProveedores.Tab
            Case 3 ' Bancos
                UltraTabControl1.SelectedTab = UltraTabPageControlBancos.Tab
            Case 4 ' Empleados
                UltraTabControl1.SelectedTab = UltraTabPageControlEmpleados.Tab()
            Case Else
                'UltraTabControl1.SelectedTab = UltraTabPageControlProfesionales.Tab
        End Select

        If l_BtnSeleccionar Then
            CBtSeleccionar.Visible = True
        End If
    End Sub
    Private Sub MuestraProfesional(ByVal cCondicion As String)
        Try
            DAProfesional = New MySqlDataAdapter
            DAProfesional = cnn.consultaBDadapter("afiliado", "afi_nombre,afi_titulo,afi_matricula,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia", cCondicion)
            CMDProfesional = New MySqlCommandBuilder(DAProfesional)
            DSProfesional = New DataSet
            dvProfesional = New DataView

            DAProfesional.Fill(DSProfesional, "profesional")
            bsProfesional = New BindingSource
            bsProfesional.DataSource = DSProfesional.Tables(0)
            bmbProfesional = BindingContext(DSProfesional, "profesional")

            UGprofesionales.DataSource = bsProfesional.DataSource
            With UGprofesionales.DisplayLayout.Bands(0)
                .Columns(0).Header.Caption = "Apellido y Nombre"
                .Columns(0).Width = 220
                .Columns(1).Header.Caption = "TMtr"
                .Columns(1).Width = 40
                .Columns(2).Header.Caption = "Matricula"
                .Columns(2).Width = 100
                .Columns(2).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(3).Header.Caption = "FecNac"
                .Columns(3).Width = 85
                .Columns(3).Hidden = False
                .Columns(4).Header.Caption = "TDoc"
                .Columns(4).Width = 40
                .Columns(5).Header.Caption = "NroDoc"
                .Columns(5).Width = 70
                .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(6).Header.Caption = "Dirección"
                .Columns(6).Width = 190
                .Columns(7).Header.Caption = "Localidad"
                .Columns(8).Header.Caption = "Provincia"
            End With
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MuestraEmpleado(ByVal cCondicion As String)
        Try
            DAProfesional = New MySqlDataAdapter
            DAProfesional = cnn.consultaBDadapter("afiliado", "afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_garante_tipdoc,afi_garante_nrodoc,afi_garantede_tipdoc,afi_garantede_nrodoc,afi_tipo,afi_ganancias,afi_dgiexcep,afi_dgrexcep,afi_ivaexcep,afi_doc1,afi_aut1,afi_nac1,afi_sex1,afi_fil1,afi_doc2,afi_aut2,afi_nac2,afi_sex2,afi_fil2,afi_doc3,afi_aut3,afi_nac3,afi_sex3,afi_fil3,afi_doc4,afi_aut4,afi_nac4,afi_sex4,afi_fil4,afi_doc5,afi_aut5,afi_nac5,afi_sex5,afi_fil5,afi_doc6,afi_aut6,afi_nac6,afi_sex6,afi_fil6,afi_cbu,afi_cbu_credito", cCondicion)
            CMDProfesional = New MySqlCommandBuilder(DAProfesional)
            DSProfesional = New DataSet
            dvProfesional = New DataView

            DAProfesional.Fill(DSProfesional, "profesional")
            bsProfesional = New BindingSource
            bsProfesional.DataSource = DSProfesional.Tables(0)
            bmbProfesional = BindingContext(DSProfesional, "profesional")
            UGEmpleados.DataSource = bsProfesional.DataSource
            With UGEmpleados.DisplayLayout.Bands(0)
                .Columns(0).Header.Caption = "TCod"
                .Columns(0).Width = 40
                .Columns(0).Hidden = False
                .Columns(1).Header.Caption = "Código"
                .Columns(1).Width = 55
                .Columns(1).Hidden = False
                .Columns(2).Header.Caption = "Apellido y Nombre"
                .Columns(2).Width = 220
                .Columns(3).Header.Caption = "FecNac"
                .Columns(3).Width = 85
                .Columns(3).Hidden = False
                .Columns(4).Header.Caption = "IVA"
                .Columns(4).Width = 40
                .Columns(5).Header.Caption = "DNI"
                .Columns(5).Width = 95
                .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(6).Header.Caption = "Dirección"
                .Columns(6).Width = 190
                .Columns(7).Header.Caption = "Localidad"
                .Columns(8).Header.Caption = "Provincia"
            End With
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MuestraComitente(ByVal cCondicion As String)
        Try
            If l_nombreLike <> "" Then
                If IsNumeric(l_nombreLike) Then 'Si es solo numeros ingreso el CUIT
                    cCondicion = cCondicion & " AND afi_cuit = '" & l_nombreLike & "'"
                Else 'Sino busca por nombre
                    cCondicion = cCondicion & " AND afi_nombre LIKE '" & l_nombreLike & "%'"
                End If
            End If

            DAProfesional = New MySqlDataAdapter
            DAProfesional = cnn.consultaBDadapter("comitente", "afi_cuit,afi_tipdoc,afi_nrodoc,afi_nombre", cCondicion)
            DAProfesional.SelectCommand.CommandTimeout = 600000
            CMDProfesional = New MySqlCommandBuilder(DAProfesional)
            DSProfesional = New DataSet
            dvProfesional = New DataView

            DAProfesional.Fill(DSProfesional, "profesional")
            bsProfesional = New BindingSource
            bsProfesional.DataSource = DSProfesional.Tables(0)
            bmbProfesional = BindingContext(DSProfesional, "profesional")
            UGcomitentes.DataSource = bsProfesional.DataSource
            With UGcomitentes.DisplayLayout.Bands(0)
                .Columns(0).Header.Caption = "CUIT"
                .Columns(0).Width = 125
                .Columns(1).Header.Caption = "TCod"
                .Columns(1).Width = 75
                .Columns(2).Header.Caption = "Código"
                .Columns(2).Width = 125
                .Columns(2).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(3).Header.Caption = "Descripción"
                .Columns(3).Width = 320
            End With
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MuestraProveedor(ByVal cCondicion As String)
        Dim tabla As String = ""
        Dim campos As String = "*"
        Try
            tabla = "afiliado"
            campos = " 
                afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,
                afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,
                afi_garante,afi_tipo,afi_ganancias,afi_dgiexcep,afi_dgrexcep,afi_ivaexcep,afi_cbu,afi_cbu_credito,afi_generarrenta,
                afi_fecha_jubilacion,afi_jubi_nro_resolucion,afi_jubi_edad_resolucion,afi_jubi_periodo_opto,afi_jubi_coeficiente,
                afi_jubi_capitalizacion,afi_jubi_fondo_compensador,afi_jubi_abona_cuota,afi_jubi_observacion,afi_obrasocial,
                afi_fecha_vencimiento_et
            "
            DAProfesional = New MySqlDataAdapter
            DAProfesional = cnn.consultaBDadapter(tabla, campos, cCondicion)
            CMDProfesional = New MySqlCommandBuilder(DAProfesional)
            DSProfesional = New DataSet
            dvProfesional = New DataView

            DAProfesional.Fill(DSProfesional, "profesional")
            '  dvProfesional.Table = DSProfesional.Tables(0)
            bsProfesional = New BindingSource
            bsProfesional.DataSource = DSProfesional.Tables(0)
            bmbProfesional = BindingContext(DSProfesional, "profesional")
            UGproveedores.DataSource = bsProfesional.DataSource

            With UGproveedores.DisplayLayout.Bands(0)
                .Columns(0).Header.Caption = "TCod"
                .Columns(0).Width = 40
                .Columns(0).Hidden = False
                .Columns(1).Header.Caption = "Código"
                .Columns(1).Width = 55
                .Columns(1).Hidden = False
                .Columns(2).Header.Caption = "Descripción"
                .Columns(2).Width = 220
                .Columns(3).Header.Caption = "FecNac"
                .Columns(3).Width = 85
                .Columns(3).Hidden = False
                .Columns(4).Header.Caption = "TCod"
                .Columns(4).Width = 40
                .Columns(5).Header.Caption = "Codigo"
                .Columns(5).Width = 70
                .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(6).Header.Caption = "Dirección"
                .Columns(6).Width = 190
                .Columns(7).Header.Caption = "Localidad"
                .Columns(8).Header.Caption = "Provincia"
            End With
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub MuestraBanco(ByVal cCondicion As String)
        Try
            DAProfesional = New MySqlDataAdapter
            DAProfesional = cnn.consultaBDadapter("afiliado", "afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_tipo", cCondicion)
            CMDProfesional = New MySqlCommandBuilder(DAProfesional)
            DSProfesional = New DataSet
            dvProfesional = New DataView

            DAProfesional.Fill(DSProfesional, "profesional")
            '  dvProfesional.Table = DSProfesional.Tables(0)
            bsProfesional = New BindingSource
            bsProfesional.DataSource = DSProfesional.Tables(0)
            bmbProfesional = BindingContext(DSProfesional, "profesional")
            UGbancos.DataSource = bsProfesional.DataSource

            With UGbancos.DisplayLayout.Bands(0)
                .Columns(0).Header.Caption = "TCod"
                .Columns(0).Width = 40
                .Columns(0).Hidden = False
                .Columns(1).Header.Caption = "Codigo"
                .Columns(1).Width = 55
                .Columns(1).Hidden = False
                .Columns(2).Header.Caption = ""
                .Columns(2).Width = 220
                .Columns(3).Header.Caption = "FecNac"
                .Columns(3).Width = 85
                .Columns(3).Hidden = False
                .Columns(4).Header.Caption = "TCod"
                .Columns(4).Width = 40
                .Columns(5).Header.Caption = "Codigo"
                .Columns(5).Width = 70
                .Columns(5).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                .Columns(6).Header.Caption = "Dirección"
                .Columns(6).Width = 190
                .Columns(7).Header.Caption = "Localidad"
                .Columns(8).Header.Caption = "Provincia"
            End With
        Catch ex As Exception
            MessageBox.Show("Error en el criterio de busqueda" & Chr(10) & Chr(13) & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
    Private Sub MuestraSegunTabIndex(ByVal index As Integer)
        If cnn.AbrirConexion Then
            Select Case index
                Case 0
                    MuestraProfesional("afi_tipo='A'")
                    Exit Select
                Case 1
                    MuestraComitente("afi_tipo='C'")
                    Exit Select
                Case 2
                    MuestraProveedor("afi_tipo='P'")
                    Exit Select
                Case 3
                    MuestraBanco("afi_tipo='B'")
                    Exit Select
                Case 4
                    MuestraEmpleado("afi_tipo='E'")
                    Exit Select
            End Select
            cnn.CerrarConexion()
        End If
    End Sub

    Private Sub BuscarPorTipoNroDocumento(ByVal tipo As String, ByVal documento As String)
        Dim tabla As String = ""
        Dim campos As String = "*"
        Dim condiciones As String = "1"
        If (tipo = "COM") Then
            tabla = "comitente"
            campos = "afi_cuit,afi_tipdoc,afi_nrodoc,afi_nombre,afi_tipoiva,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_dgr,afi_tipo"
            condiciones = "afi_tipdoc='" & tipo & "' AND afi_nrodoc='" & documento & "'"
        Else
            tabla = "afiliado"
            campos = "
                afi_nombre,
                afi_titulo,
                afi_matricula,
                afi_fecnac,
                afi_tipdoc,
                afi_nrodoc,
                afi_direccion,
                afi_localidad,
                afi_provincia,
                afi_zona,
                afi_codpos,
                afi_telefono1,
                afi_telefono2,
                afi_mail,
                afi_mail_alternativo,
                afi_civil,
                afi_cuit,
                afi_dgr,
                afi_sexo,
                afi_garante,
                afi_garante_tipdoc,
                afi_garante_nrodoc,
                afi_garantede_tipdoc,
                afi_garantede_nrodoc,
                afi_tipo,
                afi_ganancias,
                afi_categoria,
                afi_dgiexcep,
                afi_dgrexcep,
                afi_ivaexcep,
                afi_doc1,afi_aut1,afi_nac1,afi_sex1,afi_fil1,afi_beneficiario1,
                afi_doc2,afi_aut2,afi_nac2,afi_sex2,afi_fil2,afi_beneficiario2,
                afi_doc3,afi_aut3,afi_nac3,afi_sex3,afi_fil3,afi_beneficiario3,
                afi_doc4,afi_aut4,afi_nac4,afi_sex4,afi_fil4,afi_beneficiario4,
                afi_doc5,afi_aut5,afi_nac5,afi_sex5,afi_fil5,afi_beneficiario5,
                afi_doc6,afi_aut6,afi_nac6,afi_sex6,afi_fil6,afi_beneficiario6,
                afi_cbu,
                afi_cbu_credito,
                afi_deb_tuya,
                afi_deb_visa,
                afi_deb_mastercard,
                afi_deb_naranja,
                afi_generarrenta,
                afi_fecha_jubilacion,
                afi_jubi_nro_resolucion,
                afi_jubi_edad_resolucion,
                afi_jubi_periodo_opto,
                afi_jubi_coeficiente,
                afi_jubi_capitalizacion,
                afi_jubi_fondo_compensador,
                afi_jubi_abona_cuota,
                afi_jubi_observacion,
                afi_jubi_importe_cuota,
                afi_jubi_periodos_cobrados,
                afi_jubi_periodos_restantes,
                afi_obrasocial,
                afi_fecha_vencimiento_et"
            condiciones = "afi_tipdoc='" & tipo & "' AND afi_nrodoc='" & documento & "'"
        End If
        DAProfesional = cnn.consultaBDadapter(tabla, campos, condiciones)
        CMDProfesional = New MySqlCommandBuilder(DAProfesional)
        DSProfesional.Clear()
        DAProfesional.Fill(DSProfesional, "profesional")
    End Sub
    'COMENTO EL METODO PORQUE NO SE ESTA UTILIZANDO
    'Private Sub BuscarPorTipoNroMatricula(ByVal titulo As String, ByVal matricula As String)
    '    Dim campos As String = "*"
    '    Dim condiciones As String = "1"
    '    If (titulo = "CUIT") Then
    '        campos = "afi_cuit,afi_tipdoc,afi_nrodoc,afi_nombre"
    '        condiciones = ("afi_tipo='C' AND afi_cuit='" & matricula & "'")
    '    ElseIf (titulo = "PR") Then
    '        campos = "afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_tipo,afi_ganancias,afi_dgiexcep,afi_dgrexcep,afi_ivaexcep,afi_cbu,afi_cbu_credito"
    '        condiciones = "afi_titulo='" & titulo & "' AND afi_matricula='" & matricula & "'"
    '    ElseIf (titulo = "BC") Then
    '        campos = "afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_tipo"
    '        condiciones = "afi_titulo='" & titulo & "' AND afi_matricula='" & matricula & "'"
    '    ElseIf (titulo = "PE") Then
    '        campos = "afi_titulo,afi_matricula,afi_nombre,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_garante_tipdoc,afi_garante_nrodoc,afi_garantede_tipdoc,afi_garantede_nrodoc,afi_tipo,afi_ganancias,afi_dgiexcep,afi_dgrexcep,afi_ivaexcep,afi_doc1,afi_aut1,afi_nac1,afi_sex1,afi_fil1,afi_doc2,afi_aut2,afi_nac2,afi_sex2,afi_fil2,afi_doc3,afi_aut3,afi_nac3,afi_sex3,afi_fil3,afi_doc4,afi_aut4,afi_nac4,afi_sex4,afi_fil4,afi_doc5,afi_aut5,afi_nac5,afi_sex5,afi_fil5,afi_doc6,afi_aut6,afi_nac6,afi_sex6,afi_fil6,afi_cbu,afi_cbu_credito"
    '        condiciones = "afi_titulo='" & titulo & "' AND afi_matricula='" & matricula & "'"
    '    Else
    '        campos = "afi_nombre,afi_titulo,afi_matricula,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia"
    '        condiciones = "afi_titulo='" & titulo & "' AND afi_matricula='" & matricula & "'"
    '    End If
    '    DAProfesional = cnn.consultaBDadapter("afiliado", campos, condiciones)
    '    CMDProfesional = New MySqlCommandBuilder(DAProfesional)
    '    DSProfesional.Clear()
    '    DAProfesional.Fill(DSProfesional, "profesional")
    'End Sub
    Private Sub CButton1_Click(sender As Object, e As EventArgs) Handles CButton1.Click
        If Not IsNothing(UGprofesionales.ActiveRow) Then
            Dim fila As Integer = UGprofesionales.ActiveRow.VisibleIndex
            If fila > 0 Then
                BuscarPorTipoNroDocumento(UGprofesionales.ActiveRow.Cells.Item("afi_tipdoc").Value, UGprofesionales.ActiveRow.Cells.Item("afi_nrodoc").Value)
                Dim dr As DataRow = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGprofesionales.Rows.Item(0).Cells.Item("afi_tipdoc").Value & "' AND afi_nrodoc='" & UGprofesionales.Rows.Item(0).Cells.Item("afi_nrodoc").Value & "'")(0)
                fila = DSProfesional.Tables(0).Rows.IndexOf(dr)

                Dim frmFicha As New FrmFichaProfesionales("A", DSProfesional, DAProfesional, fila, False, cnn)
                frmFicha.MdiParent = MdiParent
                frmFicha.Show()
            Else
                MessageBox.Show("Debe seleccionar un profesional", "Profesional", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show("Debe seleccionar un profesional", "Profesional", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub UltraTabControl1_SelectedTabChanged(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs) Handles UltraTabControl1.SelectedTabChanged
        MuestraSegunTabIndex(e.Tab.Index)
    End Sub
    Private Sub CBAltaProfesional_Click(sender As Object, e As EventArgs) Handles CBAltaProfesional.Click
        'Controlo permiso Create
        If controlAcceso(1, 1, "C", True) = True Then
            Dim filaficha As Integer
            If bsProfesional.Current Is Nothing Then
                filaficha = 0
            Else
                filaficha = UGprofesionales.ActiveRow.Index
            End If
            BuscarPorTipoNroDocumento("", 0)
            Dim frmFicha As New FrmFichaProfesionales("A", DSProfesional, DAProfesional, filaficha, True, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CButtonActualizar.Enabled = True
            frmFicha.Show()
        End If
    End Sub
    Private Sub CBtFichaCom_Click(sender As Object, e As EventArgs) Handles CBtFichaCom.Click
        Dim filaficha As Integer = UGcomitentes.ActiveRow.VisibleIndex
        'Dim filaficha As Integer
        If filaficha > 0 Then
            BuscarPorTipoNroDocumento(UGcomitentes.ActiveRow.Cells.Item("afi_tipdoc").Value, UGcomitentes.ActiveRow.Cells.Item("afi_nrodoc").Value)
            Dim dr As DataRow = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGcomitentes.Rows.Item(0).Cells.Item("afi_tipdoc").Value & "' and afi_nrodoc='" & UGcomitentes.Rows.Item(0).Cells.Item("afi_nrodoc").Value & "'")(0)
            filaficha = DSProfesional.Tables(0).Rows.IndexOf(dr)
            Dim frmFicha As New FrmFichaComitentes(DSProfesional, DAProfesional, filaficha, False, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CButtonActualizar.Enabled = True
            frmFicha.Show()
            frmFicha.MdiParent = MdiParent
            frmFicha.Label2.Text = "Codigo:"
            frmFicha.CboTipdoc.Text = "COM"
            frmFicha.CboTipdoc.Enabled = False
            frmFicha.UltraGroupBox2.Text = "Datos Comitente"
            frmFicha.Show()
        Else
            MessageBox.Show("Debe seleccionar un comitente", "Comitente", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub CBtAltaCom_Click(sender As Object, e As EventArgs) Handles CBtAltaCom.Click
        'Controlo permiso Create
        If controlAcceso(1, 2, "C", True) = True Then
            Dim filaficha As Integer
            If bsProfesional.Current Is Nothing Then
                filaficha = 0
            Else
                filaficha = UGcomitentes.ActiveRow.Index
            End If
            BuscarPorTipoNroDocumento("COM", 0)
            Dim frmFicha As New FrmFichaComitentes(DSProfesional, DAProfesional, filaficha, True, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CButtonActualizar.Enabled = True
            frmFicha.CboTipdoc.Text = "COM"
            frmFicha.CboTipdoc.Enabled = False
            frmFicha.UltraGroupBox2.Text = "Datos Comitente"
            frmFicha.Show()
        End If
    End Sub
    Private Sub CBtFichaPrv_Click(sender As Object, e As EventArgs) Handles CBtFichaPrv.Click
        If Not IsNothing(UGproveedores.ActiveRow) Then
            Dim fila As Integer = UGproveedores.ActiveRow.VisibleIndex
            If fila > 0 Then
                BuscarPorTipoNroDocumento(UGproveedores.ActiveRow.Cells.Item("afi_tipdoc").Value, UGproveedores.ActiveRow.Cells.Item("afi_nrodoc").Value)
                Dim dr As DataRow = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGproveedores.Rows.Item(0).Cells.Item("afi_tipdoc").Value & "' AND afi_nrodoc='" & UGproveedores.Rows.Item(0).Cells.Item("afi_nrodoc").Value & "'")(0)
                fila = DSProfesional.Tables(0).Rows.IndexOf(dr)

                Dim frmFicha As New FrmFichaProfesionales("P", DSProfesional, DAProfesional, fila, False, cnn)
                frmFicha.MdiParent = MdiParent
                frmFicha.Label2.Text = "Codigo:"
                'frmFicha.CboTipdoc.Text = "PRV"
                frmFicha.CboTipdoc.Enabled = False
                frmFicha.UGBTitulos.Visible = False
                frmFicha.UGBDatos.Text = "Datos Proveedor"
                'Oculto prestamo
                frmFicha.UGBPrestamo.Enabled = False
                frmFicha.UGBPrestamo.Visible = False
                frmFicha.Show()
            Else
                MessageBox.Show("Debe seleccionar un proveedor", "Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show("Debe seleccionar un proveedor", "Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub CBtAltaPrv_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtAltaPrv.Click
        'Controlo permiso Create
        If controlAcceso(1, 3, "C", True) = True Then
            Dim filaficha As Integer
            If bsProfesional.Current Is Nothing Then
                filaficha = 0
            Else
                filaficha = UGprofesionales.ActiveRow.Index
            End If

            Dim frmFicha As New FrmFichaProfesionales("P", DSProfesional, DAProfesional, filaficha, True, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CButtonActualizar.Enabled = True
            'frmFicha.CboTipdoc.Text = "PRV"
            frmFicha.CboTipdoc.Items.Clear()
            frmFicha.CboTipdoc.Items.Add("PRV")
            frmFicha.CboTipdoc.SelectedIndex = 0
            frmFicha.CboTipdoc.Enabled = False
            frmFicha.UGBTitulos.Visible = False
            frmFicha.UGBDatos.Text = "Datos Proveedor"
            frmFicha.Show()
        End If
    End Sub

    Private Sub CBtAltaBco_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtAltaBco.Click
        'Controlo permiso Create
        If controlAcceso(1, 4, "C", True) = True Then
            Dim filaficha As Integer
            If bsProfesional.Current Is Nothing Then
                filaficha = 0
            Else
                filaficha = UGprofesionales.ActiveRow.Index
            End If

            Dim frmFicha As New FrmFichaBancos(DSProfesional, DAProfesional, filaficha, True, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CboTipdoc.Text = "BCO"
            frmFicha.Show()
        End If
    End Sub
    Private Sub cBtFichaBco_Click(sender As Object, e As EventArgs) Handles cBtFichaBco.Click
        Dim filaficha As Integer
        If bsProfesional.Current Is Nothing Then
            filaficha = 0
        Else
            filaficha = UGbancos.ActiveRow.Index
        End If

        Dim frmFicha As New FrmFichaBancos(DSProfesional, DAProfesional, filaficha, False, cnn)
        frmFicha.MdiParent = MdiParent
        frmFicha.Show()
    End Sub
    Private Sub CBtSeleccionar_Click(sender As Object, e As EventArgs) Handles CBtSeleccionar.Click
        Dim filaficha As Integer
        If bsProfesional.Current Is Nothing Then
            filaficha = 0
        Else
            filaficha = UGcomitentes.ActiveRow.Index
        End If
        Try
            Dim rowComitente As DataRow = DSProfesional.Tables(0).Rows(filaficha)
            d_DTPadron = DSProfesional.Tables(0).Clone
            d_DTPadron.ImportRow(rowComitente)
            DialogResult = DialogResult.OK
        Catch ex As Exception
            MessageBox.Show("Debe seleccionar un comitente del listado.")
        End Try
    End Sub
    Private Sub CBtFicEmp_Click(sender As Object, e As EventArgs) Handles CBtFicEmp.Click
        If Not IsNothing(UGEmpleados.ActiveRow) Then
            Dim fila As Integer = UGEmpleados.ActiveRow.VisibleIndex
            If fila > 0 Then
                BuscarPorTipoNroDocumento(UGEmpleados.ActiveRow.Cells.Item("afi_tipdoc").Value, UGEmpleados.ActiveRow.Cells.Item("afi_nrodoc").Value)
                Dim dr As DataRow = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGEmpleados.Rows.Item(0).Cells.Item("afi_tipdoc").Value & "' AND afi_nrodoc='" & UGEmpleados.Rows.Item(0).Cells.Item("afi_nrodoc").Value & "'")(0)
                fila = DSProfesional.Tables(0).Rows.IndexOf(dr)

                Dim frmFicha As New FrmFichaProfesionales("E", DSProfesional, DAProfesional, fila, False, cnn)
                frmFicha.MdiParent = MdiParent
                frmFicha.Show()
            Else
                MessageBox.Show("Debe seleccionar un empleado", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show("Debe seleccionar un empleado", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub CButton5_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton5.Click
        Dim daSubCuenta As New MySqlDataAdapter
        Dim DTsubcuenta As DataTable
        For Each rowAfi As DataRow In DSProfesional.Tables("profesional").Rows
            daSubCuenta = cnn.consultaBDadapter("subcuenta left join categorias on cat_codigo=scu_categoria", "scu_catant,scu_categoria,cat_descrip,scu_vigencia,scu_nota1", "scu_tipdoc='" & rowAfi.Item("afi_tipdoc") & "' and scu_nrodoc=" & rowAfi.Item("afi_nrodoc") & " order by scu_fecha DESC")
            DTsubcuenta = New DataTable
            daSubCuenta.Fill(DTsubcuenta)
            If DTsubcuenta.Rows.Count > 0 Then
                If DTsubcuenta.Rows(0).Item("scu_categoria") <> rowAfi.Item("afi_categoria") Then
                    MessageBox.Show(rowAfi.Item("afi_mAtricula"))
                End If
            End If
        Next
    End Sub

    Private Sub CBtAltEmp_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtAltEmp.Click
        'Controlo permiso Create
        If controlAcceso(1, 5, "C", True) = True Then
            Dim filaficha As Integer
            If bsProfesional.Current Is Nothing Then
                filaficha = 0
            Else
                filaficha = UGEmpleados.ActiveRow.Index
            End If

            Dim frmFicha As New FrmFichaProfesionales("E", DSProfesional, DAProfesional, filaficha, True, cnn)
            frmFicha.MdiParent = MdiParent
            frmFicha.CButtonActualizar.Enabled = True
            frmFicha.CboTipdoc.Text = ""
            frmFicha.CboTipdoc.Enabled = True
            frmFicha.UGBTitulos.Visible = False
            frmFicha.UGBDatos.Text = "Datos Empleado"
            frmFicha.Show()
        End If
    End Sub
    Private Sub MostrarDebitos(ByVal index As Integer)
        'Controlo permiso Debito automatico
        If controlAcceso(1, 6, , True) = True Then
            Dim fila As Integer
            Select Case index
                Case 0
                    fila = UGprofesionales.ActiveRow.VisibleIndex
                    Exit Select
                Case 4
                    fila = UGEmpleados.ActiveRow.VisibleIndex
                    Exit Select
            End Select

            If fila > 0 Then
                Dim dr As DataRow = Nothing

                Select Case index
                    Case 0
                        dr = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGprofesionales.ActiveRow.Cells("afi_tipdoc").Value & "' and afi_nrodoc='" & UGprofesionales.ActiveRow.Cells("afi_nrodoc").Value & "'")(0)
                        Exit Select
                    Case 4
                        dr = DSProfesional.Tables(0).Select("afi_tipdoc='" & UGEmpleados.ActiveRow.Cells("afi_tipdoc").Value & "' and afi_nrodoc='" & UGEmpleados.ActiveRow.Cells("afi_nrodoc").Value & "'")(0)
                        Exit Select
                End Select

                fila = DSProfesional.Tables(0).Rows.IndexOf(dr)

                Dim FrmDebito As New FrmDebito(cnn, dr)
                FrmDebito.Show()
            Else
                MessageBox.Show("Debe seleccionar un afiliado", "Debitos afiliados", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Sub CBDebito_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBDebito.Click
        MostrarDebitos(0)
    End Sub
    Private Sub CBDebitoEmpleados_Click(sender As Object, e As EventArgs) Handles CBDebitoEmpleados.Click
        MostrarDebitos(4)
    End Sub

    Private Sub BListarTodos_Click(sender As Object, e As EventArgs) Handles BListarTodos.Click
        MuestraSegunTabIndex(UltraTabControl1.SelectedTab.Index)
    End Sub
End Class