﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConciliarBanco
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConciliarBanco))
        Me.UGMovimientos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CBtBuscar = New System.Windows.Forms.Button()
        Me.CBtActualizar = New System.Windows.Forms.Button()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.PanelBotones = New System.Windows.Forms.Panel()
        CType(Me.UGMovimientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'UGMovimientos
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGMovimientos.DisplayLayout.Appearance = Appearance1
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGMovimientos.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGMovimientos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGMovimientos.DisplayLayout.Override.HeaderAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMovimientos.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGMovimientos.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGMovimientos.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UGMovimientos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGMovimientos.Location = New System.Drawing.Point(0, 32)
        Me.UGMovimientos.Name = "UGMovimientos"
        Me.UGMovimientos.Size = New System.Drawing.Size(1024, 470)
        Me.UGMovimientos.TabIndex = 0
        Me.UGMovimientos.Text = "Movimientos"
        '
        'UDTdesde
        '
        Me.UDTdesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTdesde.Location = New System.Drawing.Point(59, 3)
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(101, 24)
        Me.UDTdesde.TabIndex = 1
        '
        'UDThasta
        '
        Me.UDThasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDThasta.Location = New System.Drawing.Point(229, 3)
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(100, 24)
        Me.UDThasta.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Desde:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(182, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Hasta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CBtBuscar
        '
        Me.CBtBuscar.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CBtBuscar.Location = New System.Drawing.Point(344, 3)
        Me.CBtBuscar.Name = "CBtBuscar"
        Me.CBtBuscar.Size = New System.Drawing.Size(65, 28)
        Me.CBtBuscar.TabIndex = 9
        '
        'CBtActualizar
        '
        Me.CBtActualizar.BackColor = System.Drawing.Color.Transparent
        Me.CBtActualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtActualizar.ImageIndex = 0
        Me.CBtActualizar.Location = New System.Drawing.Point(477, 3)
        Me.CBtActualizar.Name = "CBtActualizar"
        Me.CBtActualizar.Size = New System.Drawing.Size(170, 28)
        Me.CBtActualizar.TabIndex = 116
        Me.CBtActualizar.Text = "Actualizar conciliación"
        Me.CBtActualizar.UseVisualStyleBackColor = False
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.ImageIndex = 0
        Me.CBtExportar.Location = New System.Drawing.Point(674, 3)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(183, 28)
        Me.CBtExportar.TabIndex = 124
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'PanelBotones
        '
        Me.PanelBotones.Controls.Add(Me.Label1)
        Me.PanelBotones.Controls.Add(Me.CBtExportar)
        Me.PanelBotones.Controls.Add(Me.UDTdesde)
        Me.PanelBotones.Controls.Add(Me.CBtActualizar)
        Me.PanelBotones.Controls.Add(Me.UDThasta)
        Me.PanelBotones.Controls.Add(Me.CBtBuscar)
        Me.PanelBotones.Controls.Add(Me.Label2)
        Me.PanelBotones.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelBotones.Location = New System.Drawing.Point(0, 0)
        Me.PanelBotones.Name = "PanelBotones"
        Me.PanelBotones.Size = New System.Drawing.Size(1024, 32)
        Me.PanelBotones.TabIndex = 125
        '
        'FrmConciliarBanco
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1024, 502)
        Me.Controls.Add(Me.PanelBotones)
        Me.Controls.Add(Me.UGMovimientos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmConciliarBanco"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conciliación bancaria"
        CType(Me.UGMovimientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelBotones.ResumeLayout(False)
        Me.PanelBotones.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UGMovimientos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CBtBuscar As System.Windows.Forms.Button
    Friend WithEvents CBtActualizar As System.Windows.Forms.Button
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents PanelBotones As Panel
End Class
