﻿Imports MySql.Data.MySqlClient
Public Class FrmAltaChequeras
    Private conexion As New ConsultaBD(True)
    Private c_cuentaBanco As String
    Private n_numeroBanco As Integer

    Public WriteOnly Property CuentaBanco() As String
        Set(ByVal value As String)
            c_cuentaBanco = value
        End Set
    End Property

    Public WriteOnly Property NumeroBanco() As Integer
        Set(ByVal value As Integer)
            n_numeroBanco = value
        End Set
    End Property

    Private Sub TxSerie_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxSerie.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub UltraNumericEditor1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraNumericEditor1.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub UltraNumericEditor2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UltraNumericEditor2.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub cboTipoChequera_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTipoChequera.KeyPress
        If e.KeyChar = ChrW(13) Then
            Tabular(e)
        End If
    End Sub

    Private Sub CButton2_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton2.Click
        ConfirmaChequeras()
    End Sub

    Private Sub CButton2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CButton2.KeyPress
        If e.KeyChar = ChrW(13) Then
            ConfirmaChequeras()
        End If
    End Sub

    Private Sub ConfirmaChequeras()
        If UltraNumericEditor2.Value - UltraNumericEditor1.Value > 0 AndAlso UltraNumericEditor2.Value - UltraNumericEditor1.Value < 100 Then
            If MessageBox.Show("Confirma el alta de " & UltraNumericEditor2.Value - UltraNumericEditor1.Value & " cheques", "Alta chequeras", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                GrabaChequera()
                DialogResult = DialogResult.OK
            End If
        End If
    End Sub

    Private Sub GrabaChequera()
        Dim DTchequera As New DataTable
        Dim DAChequera As New MySqlDataAdapter
        Dim MiTrans As MySqlTransaction
        DAChequera = conexion.consultaBDadapter("chequera", "max(cha_nrotal) as chequera")
        DAChequera.Fill(DTchequera)
        If conexion.AbrirConexion Then
            MiTrans = conexion.InicioTransaccion()
            Try
                conexion.ReplaceBD("INSERT INTO CHEQUERA SET cha_nrocli='" & nPubNroCli & "'," & _
                                                             "cha_tipbco='BC'," & _
                                                             "cha_nrobco='" & n_numeroBanco & "'," & _
                                                             "cha_cuebco='" & c_cuentaBanco & "'," & _
                                                             "cha_nrotal='" & DTchequera.Rows(0).Item("chequera") + 1 & "'," & _
                                                             "cha_tipo='" & Mid(cboTipoChequera.Text, 1, 1) & "'," & _
                                                             "cha_fecha='" & Format(Now, "yyy-MM-dd") & "'," & _
                                                             "cha_serie='" & TxSerie.Text & "'," & _
                                                             "cha_cheini='" & UltraNumericEditor1.Value & "'," & _
                                                             "cha_chefin='" & UltraNumericEditor2.Value & "'")

                For nChe As Integer = UltraNumericEditor1.Value To UltraNumericEditor2.Value
                    conexion.ReplaceBD("INSERT INTO CHEQUES SET che_nrocli='" & nPubNroCli & "'," & _
                                                               "che_tipbco='BC'," & _
                                                               "che_nrobco='" & n_numeroBanco & "'," & _
                                                               "che_cuebco='" & c_cuentaBanco & "'," & _
                                                               "che_nrochequera='" & DTchequera.Rows(0).Item("chequera") + 1 & "'," & _
                                                               "che_letser='" & TxSerie.Text & "'," & _
                                                               "che_nroser='" & nChe & "'")
                Next
                MiTrans.Commit()
            Catch ex As Exception
                MiTrans.Rollback()
                MessageBox.Show(ex.Message)
            End Try

            conexion.CerrarConexion()
        End If
    End Sub
End Class