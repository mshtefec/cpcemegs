﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Public Class FrmConciliarBanco
    Private cnn As New ConsultaBD(True)
    Private DAConsilia As MySqlDataAdapter
    Private DTConsilia As DataTable
    Private CmdConsilia As MySqlCommandBuilder
    Private BSConsilia As BindingSource
    Private r_Banco As DataRow
    Private clsExportarExcel As New ExportarExcel
    Public Sub New(ByRef rowBanco As DataRow)
        InitializeComponent()
        r_Banco = rowBanco
        loadValEstados()
    End Sub

    Private Sub MuestraMovimientos()
        Try
            DAConsilia = cnn.consultaBDadapter(
                "(totales INNER JOIN comproba ON com_nroasi=tot_nroasi AND com_nrocli=tot_nrocli) LEFT JOIN afiliado ON afi_tipdoc=com_tipdoc AND afi_nrodoc=com_nrodoc",
                "tot_nrocheque AS Cheque, tot_estche AS Estado, tot_fecconcilia AS conciliado, tot_fecha AS Fecha, tot_proceso AS Proceso,
                tot_nrocom AS Comproba, tot_nroasi AS Asiento, tot_debe AS Debe, tot_haber AS Haber,
                tot_fecche AS Emitido, tot_fecdif AS diferida, tot_concepto AS Concepto,
                afi_nombre AS Destinatario, afi_titulo AS Titulo, afi_matricula AS Matricula, tot_item, tot_nrocuo",
                "tot_nropla='" & r_Banco.Item("cuenta") & "'
                AND tot_fecha BETWEEN '" & Format(UDTdesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" & Format(UDThasta.Value, "yyyy-MM-dd") & " 23:59:00'
                AND tot_nrocheque > 0
                AND tot_estado <> '9'
                ORDER BY tot_estche"
            )
            DTConsilia = New DataTable
            DAConsilia.Fill(DTConsilia)
            Dim NewCol As New DataColumn
            NewCol.DefaultValue = False
            NewCol.AllowDBNull = False
            NewCol.Caption = "Select"
            NewCol.ColumnName = "Select"
            NewCol.DataType = Type.GetType("System.Boolean")

            DTConsilia.Columns.Add(NewCol)
            DTConsilia.Columns("Select").SetOrdinal(0)
            BSConsilia = New BindingSource
            BSConsilia.DataSource = DTConsilia
            CmdConsilia = New MySqlCommandBuilder(DAConsilia)
            UGMovimientos.DataSource = BSConsilia
            UGMovimientos.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
            With UGMovimientos.DisplayLayout.Bands(0)
                .Columns(2).Width = 100
                .Columns(2).Style = ColumnStyle.DropDownList
                .Columns(2).ValueList = valEstados
                .Columns(3).Width = 135
                .Columns(4).Width = 135
                .Columns(5).Width = 60
                .Columns(8).CellAppearance.TextHAlign = HAlign.Right
                .Columns(8).Width = 90
                .Columns(8).Format = "c"
                .Columns(8).PromptChar = ""
                '.Columns(8).Style = ColumnStyle.Double
                .Columns(8).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(9).CellAppearance.TextHAlign = HAlign.Right
                .Columns(9).Width = 90
                .Columns(9).Format = "c"
                .Columns(9).PromptChar = ""
                '.Columns(9).Style = ColumnStyle.Double
                .Columns(9).FormatInfo = Globalization.CultureInfo.CurrentCulture
                .Columns(11).Width = 80
                .Columns(12).Width = 80
                .Columns(13).Width = 200
                .Columns(14).Width = 200
                .Columns(15).Width = 80
                ' .Columns(13).Hidden = True
                ' .Columns(14).Hidden = True
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmConsolidarBanco_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Me.Text = Me.Text & " Cuenta: " & r_Banco.Item("Descripcion")
        UDThasta.Value = Date.Today
        UDTdesde.Value = Date.Today.AddDays(-10)
        MuestraMovimientos()
    End Sub
    Private Sub CBtBuscar_Click(sender As Object, e As EventArgs) Handles CBtBuscar.Click
        MuestraMovimientos()
    End Sub
    Private Sub GrabaConciliacion()
        Try
            If cnn.AbrirConexion Then
                For Each RowConcilia As DataRow In DTConsilia.Rows
                    If RowConcilia.Item("Select") Then
                        cnn.ReplaceBD(
                            "UPDATE totales SET tot_fecconcilia='" & cnn.FechaHoraActual & "', " &
                            "tot_estche='" & RowConcilia.Item("Estado") & "', " &
                            "tot_opeconcilia='" & nPubNroOperador & "' " &
                            "WHERE tot_nroasi=" & RowConcilia.Item("Asiento") & " AND tot_item=" & RowConcilia.Item("tot_item") & " AND tot_nrocuo=" & RowConcilia.Item("tot_nrocuo")
                        )
                    End If
                Next
                cnn.CerrarConexion()
                Dim nPos As Integer = BSConsilia.Position
                MuestraMovimientos()
                BSConsilia.Position = nPos
            End If
        Catch ex As Exception
            cnn.CerrarConexion()
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub CBtActualizar_Click(sender As Object, e As EventArgs) Handles CBtActualizar.Click
        GrabaConciliacion()
    End Sub
    Private Sub UGMovimientos_InitializeRow(ByVal sender As Object, ByVal e As InitializeRowEventArgs) Handles UGMovimientos.InitializeRow
        If e.Row.Cells("Estado").Value.ToString = "" Then
            'Ingreso
            e.Row.Appearance.BackColor = Color.Blue
            e.Row.Appearance.BackColor2 = Color.White
            e.Row.Appearance.ForeColor = Color.White
        ElseIf e.Row.Cells("Estado").Value.ToString = "A" Then
            'Acreditado
            e.Row.Appearance.BackColor = Color.Green
            e.Row.Appearance.BackColor2 = Color.White
            e.Row.Appearance.ForeColor = Color.White
        ElseIf e.Row.Cells("Estado").Value.ToString = "R" Then
            'Rechazado
            e.Row.Appearance.BackColor = Color.Red
            e.Row.Appearance.BackColor2 = Color.White
            e.Row.Appearance.ForeColor = Color.White
        End If
    End Sub

    Private Sub CBtExportar_Click(sender As Object, e As EventArgs) Handles CBtExportar.Click
        Dim cTitulo(0) As String
        cTitulo(0) = "Conciliación de Banco - Periodo desde :" & UDTdesde.Value & " hasta:" & UDThasta.Value
        clsExportarExcel.ExportarDatosExcel(UGMovimientos, cTitulo)
    End Sub
End Class