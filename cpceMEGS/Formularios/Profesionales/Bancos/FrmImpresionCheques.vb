﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid

Public Class FrmImpresionCheques
    Private cnn As New ConsultaBD(True)
    Private DTDestinatario As DataTable
    Private DSOrdenPago As DataSet
    Private BSOrdenes As BindingSource
    Private BSCheque As BindingSource
    Private DAOrdenPago As MySqlDataAdapter
    Private DROrdenPago As DataRelation
    Private DTCheques As DataTable
    Private DRCtaBanco As DataRow
    Private nUltimoNroCheque As Integer = 0
    Private nPrimerNroCheque As Integer = 0
    Private cCondicionBusquedaOrden As String
    Private DTImprimirCheques As New DataTable
    Dim xC As Integer
    Dim nPag As Integer
    Dim PosMontoX, PosMontoY, PosDiaX, PosDiaY, PosMesX, PosMesY, PosAñoX, PosAñoY, PosDestiX, PosDestiY, PosLetraX, PosLetraY As Double

    Public Sub New(ByVal c_DRCuenta As DataRow)
        InitializeComponent()
        DRCtaBanco = c_DRCuenta
    End Sub

    Private Sub CargoOrdenesPagos()
        Try
            DTDestinatario = New DataTable
            DAOrdenPago = cnn.consultaBDadapter(
                "afiliado",
                "afi_nombre as Destinatario,afi_titulo as Titulo,afi_matricula as Matricula,afi_tipdoc,afi_nrodoc",
                "1 order by afi_nombre"
            )
            DAOrdenPago.Fill(DTDestinatario)

            UltraDropDown1.DataSource = DTDestinatario
            UltraDropDown1.DisplayLayout.Bands(0).Columns(0).Width = 300
            UltraDropDown1.DisplayLayout.Bands(0).Columns(1).Width = 60
            UltraDropDown1.DisplayLayout.Bands(0).Columns(2).Width = 60
            UltraDropDown1.DisplayLayout.Bands(0).Columns(3).Hidden = True
            UltraDropDown1.DisplayLayout.Bands(0).Columns(4).Hidden = True

            DSOrdenPago = New DataSet
            DAOrdenPago = cnn.consultaBDadapter(
                "(totales left join afiliado on afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc) inner join procesos on pro_codigo=tot_proceso",
                "tot_item as item,tot_nroasi as Asiento,tot_nrocom as Orden,tot_fecha as Fecha,tot_haber as Importe,pro_nombre as Proceso,afi_nombre as Destinatario,afi_titulo,afi_matricula,afi_tipdoc,afi_nrodoc",
                cCondicionBusquedaOrden
            )
            DAOrdenPago.Fill(DSOrdenPago, "Ordenes")
            Dim columns(1) As DataColumn
            columns(0) = DSOrdenPago.Tables("Ordenes").Columns("Asiento")
            columns(1) = DSOrdenPago.Tables("Ordenes").Columns("item")

            DSOrdenPago.Tables("Ordenes").PrimaryKey = columns

            BSOrdenes = New BindingSource
            BSOrdenes.DataSource = DSOrdenPago.Tables("Ordenes")

            DTCheques = New DataTable("cheques")
            With DTCheques
                .Columns.Add("asiento", Type.GetType("System.Int64"))
                .Columns.Add("item", Type.GetType("System.Int32"))
                .Columns.Add("subitem", Type.GetType("System.Int32"))
                .Columns.Add("LetChe", Type.GetType("System.String"))
                .Columns.Add("Cheque", Type.GetType("System.String"))
                .Columns.Add("FechaEmision", Type.GetType("System.DateTime"))
                .Columns.Add("FechaDiferida", Type.GetType("System.DateTime"))
                .Columns.Add("Matricula", Type.GetType("System.String"))
                .Columns.Add("Destinatario", Type.GetType("System.String"))
                .Columns.Add("Monto", Type.GetType("System.Double"))
                .Columns.Add("Agregar", Type.GetType("System.String"))
                .Columns.Add("Transfe", Type.GetType("System.String"))
                .Columns.Add("Eliminar", Type.GetType("System.String"))
                .Columns.Add("tipdoc", Type.GetType("System.String"))
                .Columns.Add("nrodoc", Type.GetType("System.Int32"))
            End With

            For Each rowItem As DataRow In DSOrdenPago.Tables("Ordenes").Rows
                AgregarCheques(rowItem.Item("asiento"), rowItem.Item("item"), 0)
            Next
            DSOrdenPago.Tables.Add(DTCheques)

            BSCheque = New BindingSource
            BSCheque.DataSource = DSOrdenPago.Tables("cheques")

            Dim parentCols() As DataColumn = New DataColumn() _
             {DSOrdenPago.Tables("ordenes").Columns("asiento"),
             DSOrdenPago.Tables("ordenes").Columns("item")}
            Dim childCols() As DataColumn = New DataColumn() _
                {DSOrdenPago.Tables("cheques").Columns("asiento"),
                 DSOrdenPago.Tables("cheques").Columns("item")}

            'DROrdenPago = New DataRelation("RelacionCheques", DSOrdenPago.Tables("Ordenes").Columns("asiento"), DSOrdenPago.Tables("cheques").Columns("asiento"))
            DROrdenPago = New DataRelation("RelacionCheques", parentCols, childCols)

            DSOrdenPago.Relations.Add(DROrdenPago)

            UGOrdPagos.DataSource = DSOrdenPago
            UGOrdPagos.Text = DSOrdenPago.Tables(0).Rows.Count & " Ordenes de Pagos"
            With UGOrdPagos.DisplayLayout
                With .Bands(0)
                    .Columns(0).Hidden = True
                    .Columns(1).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(1).CellActivation = Activation.NoEdit
                    .Columns(2).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(2).CellActivation = Activation.NoEdit
                    .Columns(3).Width = 80
                    .Columns(3).CellActivation = Activation.NoEdit
                    .Columns(4).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(4).Format = "c"
                    .Columns(4).CellActivation = Activation.NoEdit
                    .Columns(5).Width = 280
                    .Columns(5).CellActivation = Activation.NoEdit
                    .Columns(6).Width = 270
                    .Columns(6).CellActivation = Activation.NoEdit
                    .Columns(7).Hidden = True
                    .Columns(8).Hidden = True
                    .Columns(9).Hidden = True
                    .Columns(10).Hidden = True
                End With
                With .Bands(1)
                    Dim column As UltraGridColumn = .Columns("Agregar")

                    column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                    column.Width = 50
                    .Columns("Agregar").Header.Caption = ""
                    .Columns("Agregar").Style = ColumnStyle.Button
                    .Columns("Agregar").CellButtonAppearance.Image = ImageList1.Images(0)
                    .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center

                    column = .Columns("Transfe")
                    column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                    column.Width = 50
                    .Columns("Transfe").Header.Caption = ""
                    .Columns("Transfe").Style = ColumnStyle.Button
                    .Columns("Transfe").CellButtonAppearance.Image = ImageList1.Images(3)
                    .Columns("Transfe").CellButtonAppearance.ImageHAlign = HAlign.Center

                    column = .Columns("Eliminar")
                    column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                    column.Width = 50
                    .Columns("Eliminar").Header.Caption = ""
                    .Columns("Eliminar").Style = ColumnStyle.Button
                    .Columns("Eliminar").CellButtonAppearance.Image = ImageList1.Images(2)
                    .Columns("Eliminar").CellButtonAppearance.ImageHAlign = HAlign.Center

                    .Columns(0).Hidden = True
                    .Columns(1).Hidden = True
                    .Columns(2).Hidden = True
                    .Columns(3).Hidden = True
                    .Columns(4).CellActivation = Activation.NoEdit
                    .Columns(7).Width = 80
                    .Columns(8).Width = 250
                    .Columns(8).Style = ColumnStyle.DropDownList
                    .Columns(8).ValueList = UltraDropDown1
                    .Columns(9).Format = "c"
                    .Columns(9).PromptChar = ""
                    .Columns(9).Style = ColumnStyle.Double
                    .Columns(9).CellAppearance.TextHAlign = HAlign.Right
                    .Columns(9).CellActivation = Activation.NoEdit
                    .Columns(10).Width = 60
                    .Columns(10).Header.Caption = "Asignar"
                    .Columns(11).Width = 60
                    .Columns(11).Header.Caption = "Transf"
                    .Columns(12).Width = 60
                    .Columns(12).Header.Caption = "Eliminar"
                    .Columns(13).Hidden = True
                    .Columns(14).Hidden = True
                    .Columns(0).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(0).Header.Appearance.ForeColor = Color.Black
                    .Columns(0).Header.Appearance.BackColor = Color.Aqua
                    .Columns(0).Header.Appearance.BackColor2 = Color.White
                    .Columns(1).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(1).Header.Appearance.ForeColor = Color.Black
                    .Columns(1).Header.Appearance.BackColor = Color.Aqua
                    .Columns(1).Header.Appearance.BackColor2 = Color.White
                    .Columns(2).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(2).Header.Appearance.ForeColor = Color.Black
                    .Columns(2).Header.Appearance.BackColor = Color.Aqua
                    .Columns(2).Header.Appearance.BackColor2 = Color.White
                    .Columns(3).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(3).Header.Appearance.ForeColor = Color.Black
                    .Columns(3).Header.Appearance.BackColor = Color.Aqua
                    .Columns(3).Header.Appearance.BackColor2 = Color.White
                    .Columns(4).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(4).Header.Appearance.ForeColor = Color.Black
                    .Columns(4).Header.Appearance.BackColor = Color.Aqua
                    .Columns(4).Header.Appearance.BackColor2 = Color.White
                    .Columns(5).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(5).Header.Appearance.ForeColor = Color.Black
                    .Columns(5).Header.Appearance.BackColor = Color.Aqua
                    .Columns(5).Header.Appearance.BackColor2 = Color.White
                    .Columns(6).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(6).Header.Appearance.ForeColor = Color.Black
                    .Columns(6).Header.Appearance.BackColor = Color.Aqua
                    .Columns(6).Header.Appearance.BackColor2 = Color.White
                    .Columns(7).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(7).Header.Appearance.ForeColor = Color.Black
                    .Columns(7).Header.Appearance.BackColor = Color.Aqua
                    .Columns(7).Header.Appearance.BackColor2 = Color.White
                    .Columns(8).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(8).Header.Appearance.ForeColor = Color.Black
                    .Columns(8).Header.Appearance.BackColor = Color.Aqua
                    .Columns(8).Header.Appearance.BackColor2 = Color.White
                    .Columns(9).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(9).Header.Appearance.ForeColor = Color.Black
                    .Columns(9).Header.Appearance.BackColor = Color.Aqua
                    .Columns(9).Header.Appearance.BackColor2 = Color.White
                    .Columns(10).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(10).Header.Appearance.ForeColor = Color.Black
                    .Columns(10).Header.Appearance.BackColor = Color.Aqua
                    .Columns(10).Header.Appearance.BackColor2 = Color.White
                    .Columns(11).Header.Appearance.BackGradientStyle = GradientStyle.Vertical
                    .Columns(11).Header.Appearance.ForeColor = Color.Black
                    .Columns(11).Header.Appearance.BackColor = Color.Yellow
                    .Columns(11).Header.Appearance.BackColor2 = Color.White
                End With
            End With
            UGOrdPagos.PerformAction(UltraGridAction.EnterEditMode)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AgregarCheques(ByRef Asiento As Long, ByVal Item As Integer, ByVal subitem As Integer)
        Dim mydr As DataRow
        mydr = DTCheques.NewRow()
        mydr("asiento") = Asiento
        mydr("item") = Item
        mydr("subitem") = subitem
        mydr("letChe") = ""
        mydr("cheque") = ""
        mydr("Agregar") = ""
        DTCheques.Rows.Add(mydr)
    End Sub

    Private Sub EliminarCheques(ByVal Asiento As Long, ByVal Item As Integer, ByVal subItem As Integer, ByVal LetChe As String, ByVal Cheque As String)
        Dim nFilaDel As Integer = -1
        Dim lDelChe As Boolean = False
        If Cheque = "" Then
            lDelChe = True
        Else
            Dim rowChBlanco As DataRow
            If LetChe = "T" Then
                lDelChe = True
            Else
                For nCh As Integer = 0 To DSOrdenPago.Tables("cheques").Rows.Count - 1
                    rowChBlanco = DSOrdenPago.Tables("cheques").Rows(nCh)
                    If rowChBlanco.Item("cheque").ToString = Cheque Then
                        If nUltimoNroCheque = CInt(Cheque) Then
                            nFilaDel = nCh
                            lDelChe = True
                            Exit For
                        End If
                    Else
                        If nFilaDel > -1 Then
                            If Not IsDBNull(rowChBlanco.Item("monto")) AndAlso rowChBlanco.Item("monto") > 0 Then
                                nFilaDel = -1
                                lDelChe = False
                            End If
                            Exit For
                        End If
                    End If
                Next
            End If
        End If
        If lDelChe Then
            If nFilaDel > -1 Then
                DSOrdenPago.Tables("cheques").Rows(nFilaDel).Item("monto") = 0
                If nUltimoNroCheque <> nPrimerNroCheque Then
                    nUltimoNroCheque -= 1
                Else
                    nUltimoNroCheque = 0
                End If
            End If
            With UGOrdPagos.ActiveRow
                .Delete()

                Dim rowUltChe() As DataRow = DTCheques.Select("asiento=" & Asiento)
                If rowUltChe.Length = 0 Then
                    AgregarCheques(Asiento, Item, subItem)
                End If
            End With
        Else
            MessageBox.Show("Para eliminar un cheque debe seguir la secuencia de la numeración", "Eliminar cheque", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub AsignarTransferencia(ByRef rowSubItem As UltraGridRow)
        Try
            cnn.AbrirConexion()
            Dim Keys(1) As String
            Keys(0) = rowSubItem.Cells("asiento").Value
            Keys(1) = rowSubItem.Cells("item").Value
            Dim RowOrdenPago As DataRow = DSOrdenPago.Tables("Ordenes").Rows.Find(Keys)

            '      Dim RowOrdenPago As DataRow = DSOrdenPago.Tables("Ordenes").Rows.Find(rowSubItem.Cells("asiento").Value)
            Dim nNroTransf As Integer = cnn.TomaCobte(nPubNroIns, nPubNroCli, "TRANSF")
            cnn.CerrarConexion()
            If rowSubItem.Cells("cheque").Value = "" Then
                rowSubItem.Cells("letChe").Value = "T"
                rowSubItem.Cells("cheque").Value = nNroTransf
                rowSubItem.Cells("fechaemision").Value = Mid(Convert.ToString(Now.Date), 1, 10)
                rowSubItem.Cells("fechadiferida").Value = Mid(Convert.ToString(Now.Date), 1, 10)
                rowSubItem.Cells("Monto").Value = RowOrdenPago.Item("importe")
                rowSubItem.Cells("Destinatario").Value = RowOrdenPago.Item("Destinatario")
                rowSubItem.Cells("Matricula").Value = RowOrdenPago.Item("afi_titulo") & RowOrdenPago.Item("afi_matricula")
                rowSubItem.Cells("tipdoc").Value = RowOrdenPago.Item("afi_tipdoc")
                rowSubItem.Cells("nrodoc").Value = RowOrdenPago.Item("afi_nrodoc")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub AsignarCheque(ByRef rowSubItem As UltraGridRow)
        Try
            Dim Keys(1) As String
            Keys(0) = rowSubItem.Cells("asiento").Value
            Keys(1) = rowSubItem.Cells("item").Value
            Dim RowOrdenPago As DataRow = DSOrdenPago.Tables("Ordenes").Rows.Find(Keys)
            If nUltimoNroCheque = 0 Then
                Dim nVarCheque As String
                nVarCheque = InputBox("Cheque N°", "Ingrese Numero de Cheque")
                If nVarCheque.Trim = "" Then
                    Exit Sub
                ElseIf Not IsNumeric(nVarCheque) Then
                    MessageBox.Show("Numero de cheque invalido", "Error")
                    Exit Sub
                End If
                nPrimerNroCheque = nVarCheque
                nUltimoNroCheque = nVarCheque
            Else
                nUltimoNroCheque += 1
            End If
            If rowSubItem.Cells("cheque").Value = "" Then
                rowSubItem.Cells("cheque").Value = nUltimoNroCheque
                rowSubItem.Cells("fechaemision").Value = Mid(Convert.ToString(Now.Date), 1, 10)
                rowSubItem.Cells("fechadiferida").Value = Mid(Convert.ToString(Now.Date), 1, 10)
                rowSubItem.Cells("Monto").Value = RowOrdenPago.Item("importe")
                rowSubItem.Cells("Destinatario").Value = RowOrdenPago.Item("Destinatario")
                rowSubItem.Cells("Matricula").Value = RowOrdenPago.Item("afi_titulo") & RowOrdenPago.Item("afi_matricula")
                rowSubItem.Cells("tipdoc").Value = RowOrdenPago.Item("afi_tipdoc")
                rowSubItem.Cells("nrodoc").Value = RowOrdenPago.Item("afi_nrodoc")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub CtrlMontoCheque(ByRef rowSubItem As UltraGridRow)
        Dim nMontoAsiento As Double = 0
        Dim Keys(1) As String
        Keys(0) = rowSubItem.Cells("asiento").Value
        Keys(1) = rowSubItem.Cells("item").Value
        Dim RowOrdenPago As DataRow = DSOrdenPago.Tables("Ordenes").Rows.Find(Keys)
        For Each rowChblanco As DataRow In DSOrdenPago.Tables("cheques").Rows
            ' busco primer cheque en blanco
            If rowChblanco.Item("asiento") = RowOrdenPago.Item("asiento") Then

                nMontoAsiento += rowChblanco.Item("monto")
            End If
        Next
        If FormatNumber(nMontoAsiento) <> RowOrdenPago.Item("importe") Then
            MessageBox.Show("El importe de los cheques no es igual a la orden de pago", "Orden de Pago N°" & RowOrdenPago.Item("Orden"), MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub CBtAsignarAuto_Click(sender As Object, e As EventArgs) Handles CBtAsignarAuto.Click
        AsignacionAutomatica()
    End Sub

    Private Sub FrmImpresionCheques_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' SOLO ORDENES DE PAGO DEL CONSEJO, PEDIDO VANINA 28/02/2014
        cCondicionBusquedaOrden = "tot_nropla='" & DRCtaBanco.Item("cuenta") & "' AND (MID(tot_proceso,1,3) ='02H' OR MID(tot_proceso,1,3) ='02P') AND tot_nrocheque=0 AND tot_fecha>='2013-07-11' AND tot_estado<>'9' AND tot_haber>0"
        CargoOrdenesPagos()
    End Sub


    Private Sub UGOrdPagos_BeforeRowsDeleted(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs) Handles UGOrdPagos.BeforeRowsDeleted
        ' para que no pregunte cuando borro una fila
        e.DisplayPromptMsg = False
    End Sub

    Private Sub UGOrdPagos_CellChange(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGOrdPagos.CellChange
        If e.Cell.Band.Index = 1 Then
            If e.Cell.Column.Index = 8 Then
                e.Cell.Row.Cells("matricula").Value = UltraDropDown1.ActiveRow.Cells("titulo").Value & UltraDropDown1.ActiveRow.Cells("matricula").Value
                e.Cell.Row.Cells("tipdoc").Value = UltraDropDown1.ActiveRow.Cells("afi_tipdoc").Value
                e.Cell.Row.Cells("nrodoc").Value = UltraDropDown1.ActiveRow.Cells("afi_nrodoc").Value
            End If
        End If
    End Sub


    Private Sub UGOrdPagos_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGOrdPagos.ClickCellButton
        If e.Cell.Band.Index = 1 Then ' carga cheques
            Select Case e.Cell.Column.Index
                Case 10  'Agregar cheque
                    If e.Cell.Row.Cells("Cheque").Value <> "" Then
                        AgregarCheques(e.Cell.Row.Cells("asiento").Value, e.Cell.Row.Cells("item").Value, e.Cell.Row.Cells("subitem").Value + 1)
                    Else
                        AsignarCheque(e.Cell.Row)
                    End If
                Case 11 ' agregar transferencia
                    AsignarTransferencia(e.Cell.Row)
                Case 12 ' eliminar cheque
                    If MessageBox.Show("    Confirmar ", "Eliminar Cheque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        EliminarCheques(e.Cell.Row.Cells("asiento").Value, e.Cell.Row.Cells("item").Value, e.Cell.Row.Cells("subitem").Value, e.Cell.Row.Cells("letche").Value, e.Cell.Row.Cells("cheque").Value)
                    End If

            End Select
        End If
    End Sub


    Private Sub UGOrdPagos_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGOrdPagos.InitializeRow
        UGOrdPagos.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free
        If e.Row.Band.Index = 0 Then
            e.Row.ExpandAll()
        End If
        If e.Row.Band.Index = 1 Then
            If e.Row.Cells("LetChe").Value = "T" Then
                e.Row.Appearance.BackColor = Color.Yellow
                e.Row.Appearance.ForeColor = Color.Black
                e.Row.Appearance.BackColor2 = Color.Yellow
            End If
        End If
    End Sub

    Private Sub AsignacionAutomatica()

        For Each rowOP As UltraGridRow In UGOrdPagos.Rows
            Dim RowChild As UltraGridRow = rowOP.ChildBands.FirstRow
            AsignarCheque(RowChild)
        Next
    End Sub

    Private Sub UGOrdPagos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles UGOrdPagos.KeyPress
        Try
            If e.KeyChar = ChrW(Keys.Enter) Then
                If UGOrdPagos.ActiveCell.IsInEditMode Then
                    UGOrdPagos.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.NoEdit
                    UGOrdPagos.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode)

                    CtrlMontoCheque(UGOrdPagos.ActiveRow)
                Else
                    UGOrdPagos.DisplayLayout.Bands(1).Columns("Monto").CellActivation = Activation.AllowEdit
                    UGOrdPagos.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode)

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub CBtImprimir_Click(sender As Object, e As EventArgs) Handles CBtImprimir.Click
        If DSOrdenPago.Tables("Ordenes").Rows.Count > 0 Then
            GeneroImpresionCheques()
            CargoOrdenesPagos()
        End If
    End Sub
    Private Sub GeneroImpresionCheques()
        Dim nMontoChequesOrden As Double
        Dim nCantChequeOrden As Integer
        Dim NL As New NumLetra
        Dim cMsjError As String

        DTImprimirCheques = New DataTable

        DTImprimirCheques.Columns.Add("asiento", GetType(Long))
        DTImprimirCheques.Columns.Add("item", GetType(Integer))
        DTImprimirCheques.Columns.Add("subitem", GetType(Integer))
        DTImprimirCheques.Columns.Add("cheque", GetType(String))
        DTImprimirCheques.Columns.Add("dia", GetType(String))
        DTImprimirCheques.Columns.Add("mes", GetType(String))
        DTImprimirCheques.Columns.Add("año", GetType(String))
        DTImprimirCheques.Columns.Add("destinatario", GetType(String))
        DTImprimirCheques.Columns.Add("monto", GetType(String))
        DTImprimirCheques.Columns.Add("pesos", GetType(String))

        For Each rowOP As DataRow In DSOrdenPago.Tables("Ordenes").Rows
            nMontoChequesOrden = 0
            nCantChequeOrden = 0
            For Each orderRow In rowOP.GetChildRows(DROrdenPago)
                cMsjError = ""
                ' si letche tiene T es una transferencia no se imprime
                If orderRow("cheque").ToString.Length > 0 And orderRow.Item("letChe") = "" Then
                    '   cMsjError = "No tiene asignado el cheque" & vbCrLf & vbCrLf
                    nMontoChequesOrden += orderRow("monto")
                    nCantChequeOrden += 1
                    If orderRow("fechadiferida").ToString = "" Then
                        cMsjError = cMsjError & "No tiene cargado de fecha diferida" & vbCrLf & vbCrLf
                    End If
                    If orderRow("matricula").ToString = "" Or orderRow("destinatario").ToString = "" Then
                        cMsjError = cMsjError & "No tiene asignado el destinatario" & vbCrLf & vbCrLf
                    End If
                    If orderRow("monto").ToString = "" Then
                        cMsjError = cMsjError & "No tiene cargado el monto"
                    End If
                    If cMsjError <> "" Then
                        MessageBox.Show(cMsjError, "Orden de Pago N° " & rowOP.Item("orden"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    Else
                        DTImprimirCheques.Rows.Add()
                        Dim rowImpche As DataRow = DTImprimirCheques.Rows(DTImprimirCheques.Rows.Count - 1)
                        rowImpche.Item("asiento") = orderRow.Item("asiento")
                        rowImpche.Item("item") = orderRow.Item("item")
                        rowImpche.Item("subitem") = orderRow.Item("subitem")
                        rowImpche.Item("cheque") = orderRow.Item("cheque")
                        rowImpche.Item("dia") = Mid(orderRow.Item("fechaemision").ToString, 1, 2)
                        rowImpche.Item("mes") = MonthName(CInt(Mid(orderRow.Item("fechaemision").ToString, 4, 2)))
                        rowImpche.Item("año") = Mid(orderRow.Item("fechaemision").ToString, 7, 4)
                        rowImpche.Item("destinatario") = orderRow.Item("destinatario")
                        rowImpche.Item("monto") = ceros(FormatNumber(orderRow.Item("monto")), 20)
                        rowImpche.Item("pesos") = Atrasceros(NL.Convertir(FormatNumber(orderRow.Item("monto")).ToString, True), 400)
                    End If
                Else
                    If Not IsDBNull(orderRow.Item("monto")) Then
                        nMontoChequesOrden += orderRow("monto")
                    End If
                End If
            Next
            If nCantChequeOrden > 0 AndAlso FormatNumber(nMontoChequesOrden) <> rowOP.Item("importe") Then
                MessageBox.Show("La suma de los montos de los cheques es diferente el importe de la orden de pago", "Orden de Pago N° " & rowOP.Item("orden"), MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        Dim lImprimioCheques As Boolean = False

        If DTImprimirCheques.Rows.Count > 0 Then
            'Dim frmImprimir As New FrmReportes(DTImprimirCheques, , "CRCheques.rpt", "Cheques")
            'frmImprimir.ShowDialog()
            'Imresion directa del reporte de cheques
            Dim impresionDirecta As New ImpresionDirecta(DTImprimirCheques, , "CRCheques.rpt", pubImpresoraNombreCheques, pubImpresoraPaginaCheques)
            impresionDirecta.Imprimir()
        End If
        '   Exit Sub
        If MessageBox.Show("Los cheques se imprimieron correctamente", "Cheques", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            If cnn.AbrirConexion Then
                Dim DTtotales As New DataTable
                Dim DATotales As MySqlDataAdapter
                Dim cmdTotales As MySqlCommandBuilder
                Dim dFechaCheque As MySql.Data.Types.MySqlDateTime
                Dim miTrans As MySqlTransaction
                miTrans = cnn.InicioTransaccion()
                Try
                    For Each rowCheImp As DataRow In DTCheques.Rows
                        If rowCheImp.Item("cheque") <> "" Then
                            DATotales = cnn.consultaBDadapter("totales", , "tot_nroasi=" & rowCheImp.Item("asiento") & " and tot_item=" & rowCheImp.Item("item"))
                            cmdTotales = New MySqlCommandBuilder(DATotales)
                            DTtotales = New DataTable
                            DATotales.Fill(DTtotales)
                            Dim RowTotales As DataRow
                            ' si es subitem mayor a cero agrego el item
                            If rowCheImp.Item("subitem") > 0 Then
                                Dim rowClone As DataRow = DTtotales.NewRow
                                rowClone.ItemArray = DTtotales.Rows(0).ItemArray
                                rowClone.Item("tot_nrocuo") = rowCheImp.Item("subitem") + 1
                                DTtotales.Rows.Add(rowClone)
                            End If
                            RowTotales = DTtotales.Rows(rowCheImp.Item("subitem"))
                            RowTotales.Item("tot_letcheque") = rowCheImp.Item("letche")
                            RowTotales.Item("tot_nrocheque") = rowCheImp.Item("cheque")
                            dFechaCheque.Day = rowCheImp.Item("fechaemision").ToString.Substring(0, 2)
                            dFechaCheque.Month = rowCheImp.Item("fechaemision").ToString.Substring(3, 2)
                            dFechaCheque.Year = rowCheImp.Item("fechaemision").ToString.Substring(6, 4)

                            RowTotales.Item("tot_fecche") = dFechaCheque

                            dFechaCheque.Day = rowCheImp.Item("fechadiferida").ToString.Substring(0, 2)
                            dFechaCheque.Month = rowCheImp.Item("fechadiferida").ToString.Substring(3, 2)
                            dFechaCheque.Year = rowCheImp.Item("fechadiferida").ToString.Substring(6, 4)

                            RowTotales.Item("tot_fecdif") = dFechaCheque
                            RowTotales.Item("tot_tipdes") = rowCheImp.Item("tipdoc")
                            RowTotales.Item("tot_nrodes") = rowCheImp.Item("nrodoc")
                            RowTotales.Item("tot_estche") = "E"
                            RowTotales.Item("tot_haber") = rowCheImp.Item("monto")

                            DATotales.Update(DTtotales)
                        End If
                    Next
                    miTrans.Commit()
                Catch ex As Exception
                    miTrans.Rollback()
                    MessageBox.Show("Se produjo un ERROR: " & ex.Message)
                End Try
                cnn.CerrarConexion()
            End If
        End If
    End Sub

    Private Sub ImpresionPrint_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles HojaImpresion.PrintPage
        Dim TamañoCheque As Decimal = 7.5

        Dim Monto, Letra As String

        'Inicializo las Variables
        'a ojo saque estos valores

        'PosMontoX = 500
        'PosMontoY = 7
        'PosDiaX = 140
        'PosDiaY = 62
        'PosMesX = 230
        'PosMesY = 62
        'PosAñoX = 380
        'PosAñoY = 62
        'PosDestiX = 130
        'PosDestiY = 82
        'PosLetraX = 190
        'PosLetraY = 122

        ''PARA LASER
        'PosMontoX = 520
        'PosMontoY = 12
        'PosDiaX = 230
        'PosDiaY = 70
        'PosMesX = 330
        'PosMesY = 70
        'PosAñoX = 480
        'PosAñoY = 70
        'PosDestiX = 200
        'PosDestiY = 90
        'PosLetraX = 250
        'PosLetraY = 130
        Try
            ' La fuente a usar
            Dim prFont As New Font("Microsoft Sans Serif", 10, FontStyle.Regular)
            Dim cCopias As Integer = DTImprimirCheques.Rows.Count - 1
            Dim row As DataRow
            For xP = xC To xC + 3
                '  For Each row In DTImprimirCheques.Rows
                If xP > cCopias Then
                    Exit For
                End If
                row = DTImprimirCheques.Rows(xP)
                'imprimimo el Monto el Pesos
                'completo el monto con * al principio
                Monto = ceros(FormatNumber(row("Monto"), 2), 20)
                e.Graphics.DrawString(Monto, prFont, Brushes.Black, PosMontoX, PosMontoY)

                ''imprimimo la fecha         
                e.Graphics.DrawString(row("Dia"), prFont, Brushes.Black, PosDiaX, PosDiaY)
                e.Graphics.DrawString(MonthName(row("Mes")), prFont, Brushes.Black, PosMesX, PosMesY)
                e.Graphics.DrawString(row("Año"), prFont, Brushes.Black, PosAñoX, PosAñoY)

                ''imprimimo el nombre del Profesional     
                If row("Destinatario").ToString.Length > 50 Then
                    Dim parte1 As String = row("Destinatario").ToString.Substring(0, 50)
                    Dim parte2 As String = row("Destinatario").ToString.Substring(50)
                    'imprime en 2 partes la cadena de caracteres cuando es mas de 15 caracters
                    e.Graphics.DrawString(parte1, prFont, Brushes.Black, PosDestiX, PosDestiY)
                    e.Graphics.DrawString(parte2, prFont, Brushes.Black, PosDestiX, PosDestiY + 17.5)
                Else
                    e.Graphics.DrawString(row("Destinatario"), prFont, Brushes.Black, PosDestiX, PosDestiY)
                End If

                'imprimimo el monto en Letras   
                'completo las leras con * al final
                Letra = Atrasceros(row("Pesos"), 100)
                'Sin el tamaño es mayor a 50 caracteres
                'lo divido e imprimo en la siguiente linea
                If Letra.Length > 50 Then
                    Dim parte1 As String = Letra.Substring(0, 50)
                    Dim parte2 As String = Letra.Substring(50)
                    'imprime en 2 partes la cadena de caracteres cuando es mas de 50 caracters
                    e.Graphics.DrawString(parte1, prFont, Brushes.Black, PosLetraX, PosLetraY)
                    e.Graphics.DrawString(parte2, prFont, Brushes.Black, PosLetraX, PosLetraY + 17.5)
                Else
                    'aqui imprime la cadena completa cuando la cadena no es mas de 50 caracteres
                    e.Graphics.DrawString(Letra, prFont, Brushes.Black, PosLetraX, PosLetraY)
                End If

                PosMontoY += 300
                PosDiaY += 300
                PosMesY += 300
                PosAñoY += 300
                PosDestiY += 300
                PosLetraY += 300
                xC += 1
            Next

            nPag -= 1

            If nPag > 0 Then
                e.HasMorePages = False
            Else
                e.HasMorePages = True

            End If
            'indicamos que hemos llegado al final de la pagina
            '  e.HasMorePages = False

        Catch ex As Exception
            e.Cancel = True
            MessageBox.Show("ERROR : " & ex.Message, "IMPRESION DE CHEQUES ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub CBtReasignacion_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtReasignacion.Click
        'cCondicionBusquedaOrden = "tot_nropla='" & DRCtaBanco.Item("cuenta") & "' and (MID(tot_proceso,1,3) ='02H' OR MID(tot_proceso,1,3) ='02P') and tot_nrocheque>0 and tot_fecha>='" & Format(Now.Date.AddDays(-10), "yyyy-MM-dd") & "'"
        'CargoOrdenesPagos()

        Dim sw1 As System.IO.StreamWriter = System.IO.File.CreateText("F:\temp.txt")
        'sw1.Write(Chr(27) + Chr(64) + Chr(13) + Chr(18))
        sw1.Write(Chr(13))
        sw1.WriteLine("")
        sw1.WriteLine("")
        sw1.WriteLine("")
        sw1.WriteLine("Esta es una prueba para el salto de linea.")
        sw1.Write("Esta no salta linea. ")
        sw1.Write("Continua con la linea anterior")
        sw1.WriteLine("Linea nueva")
        '  sw1.Write(Chr(12))
        sw1.Close()
        Try
            Shell("net use LPT1 \\terminalvanina\epson")
            Shell("print /d:LPT1 F:\temp.txt") 'si quieres en FUNCIONA ESTA LINEA CORRECTAMENTE 
            'un puerto COM : "print/d:COM1 C:/temp.txt" 
        Catch X As System.IO.FileNotFoundException
            MsgBox(X.Message)
        End Try
    End Sub
End Class