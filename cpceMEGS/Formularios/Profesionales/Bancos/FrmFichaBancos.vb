﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports Infragistics.Shared
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Public Class FrmFichaBancos
    Private c_drProf As DataRow
    Private c_dsProf As DataSet
    Private c_daProf As MySqlDataAdapter
    Private c_cnn As ConsultaBD
    Private l_alta As Boolean
    Private DAMatricula As MySqlDataAdapter
    Private DSMatricula As DataSet
    Private DASubcuenta As MySqlDataAdapter
    Private DSsubCuenta As DataSet
    Private cmdMatricula As MySqlCommandBuilder
    Private bindingCuenta As New BindingSource
    Private bindingBase As BindingManagerBase
    Private dFecMysql As MySqlDateTime
    Private n_nRow As Integer
    Public Sub New(ByRef dsProf As DataSet, ByRef daProf As MySqlDataAdapter, ByVal nRow As Integer, ByVal lAlta As Boolean, ByVal conexion As ConsultaBD)
        InitializeComponent()
        c_dsProf = dsProf
        c_daProf = daProf
        n_nRow = nRow
        l_alta = lAlta
        c_cnn = conexion
    End Sub

    Private Sub cargoDatosBanco()

        Try

            CboTipdoc.Text = c_drProf.Item("afi_tipdoc")

            TxDocumento.Text = Format(c_drProf.Item("afi_nrodoc"), "00000000")

            TxNombre.Text = c_drProf.Item("afi_nombre")

            MtxCuit.Text = c_drProf.Item("afi_cuit")

            MtxDGR.Text = c_drProf.Item("afi_dgr")

            cboProvincia.Text = c_drProf.Item("afi_provincia")

            TxDireccion.Text = c_drProf.Item("afi_direccion")

            TxLocalidad.Text = c_drProf.Item("afi_localidad")

            TxCodPos.Text = c_drProf.Item("afi_codpos")

            MtxTelefono.Text = c_drProf.Item("afi_telefono1")

            MtxCelu.Text = c_drProf.Item("afi_telefono2")

            TxMail.Text = c_drProf.Item("afi_mail")

            TxGarante.Text = c_drProf.Item("afi_garante")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EditarBanco(ByVal lEditar As Boolean)

        Try
            If l_alta Then

                CboTipdoc.Enabled = lEditar

                TxDocumento.Enabled = lEditar

            End If
            cboProvincia.Enabled = lEditar

            TxNombre.Enabled = lEditar

            MtxCuit.Enabled = lEditar

            MtxDGR.Enabled = lEditar

            TxDireccion.Enabled = lEditar

            TxLocalidad.Enabled = lEditar

            TxCodPos.Enabled = lEditar

            MtxTelefono.Enabled = lEditar

            MtxCelu.Enabled = lEditar

            TxMail.Enabled = lEditar

            TxGarante.Enabled = lEditar

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ActualizoDatosBanco()

        Try
            If l_alta Then

                c_drProf.Item("afi_tipdoc") = CboTipdoc.Text

                c_drProf.Item("afi_nrodoc") = TxDocumento.Text

                c_drProf.Item("afi_titulo") = "BC"

                c_drProf.Item("afi_matricula") = TxDocumento.Text

            End If

            c_drProf.Item("afi_tipo") = "B"

            c_drProf.Item("afi_nombre") = Trim(TxNombre.Text)

            c_drProf.Item("afi_fecnac") = dFecMysql

            c_drProf.Item("afi_cuit") = MtxCuit.Text

            c_drProf.Item("afi_dgr") = MtxDGR.Text

            c_drProf.Item("afi_provincia") = cboProvincia.Text

            c_drProf.Item("afi_direccion") = TxDireccion.Text

            c_drProf.Item("afi_localidad") = TxLocalidad.Text

            If IsNumeric(TxCodPos.Text) Then
                c_drProf.Item("afi_codpos") = Convert.ToInt16(TxCodPos.Text)
            Else
                c_drProf.Item("afi_codpos") = 0
            End If
            c_drProf.Item("afi_telefono1") = MtxTelefono.Text

            c_drProf.Item("afi_telefono2") = MtxCelu.Text

            c_drProf.Item("afi_mail") = TxMail.Text

            c_drProf.Item("afi_garante") = TxGarante.Text
            If l_alta Then
                c_dsProf.Tables(0).Rows.Add(c_drProf)
            End If
            c_daProf.Update(c_dsProf, "profesional")

        Catch ex As Exception
            c_dsProf.RejectChanges()
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    'Private Sub CuentaBanco()
    '    Try
    '        DSsubCuenta = New DataSet

    '        DASubcuenta = c_cnn.consultaBDadapter("plancuen", "pla_nropla as Cuenta,pla_nombre as Descripcion", "pla_subcta='" & c_drProf.Item("afi_tipdoc") & Format(c_drProf.Item("afi_nrodoc"), "00000000") & "'")
    '        DASubcuenta.Fill(DSsubCuenta, "cuentas")
    '        DSsubCuenta.Tables("cuentas").Columns.Add("Agregar", Type.GetType("System.String"))
    '        bindingCuenta = New BindingSource
    '        bindingCuenta.DataSource = DSsubCuenta.Tables("cuentas")

    '        DASubcuenta = c_cnn.consultaBDadapter("chequera", "cha_nrotal as chequera,if(cha_tipo='C','Comun','Diferido') as Tipo,cha_fecha as Fecha,cha_serie as Serie,cha_cheini as Desde,cha_chefin as Hasta,cha_cuebco", "1 order by cha_nrotal")

    '        DASubcuenta.Fill(DSsubCuenta, "chequeras")
    '        DSsubCuenta.Tables("chequeras").Columns.Add("Asignar", Type.GetType("System.String"))


    '        DASubcuenta = c_cnn.consultaBDadapter("cheques  left join afiliado on afi_tipdoc=che_tipdoc and afi_nrodoc=che_nrodoc", "che_nrochequera,che_letser as Serie,che_nroser as Numero,che_fecche as FecChe,che_fecdif as FecDif,che_impche as Importe,afi_nombre as Destinatario,che_ordpag as OrdPago,che_nroasi as Asiento,che_cuebco")

    '        DASubcuenta.Fill(DSsubCuenta, "cheques")
    '        DSsubCuenta.Tables("cheques").Columns.Add("Asignar", Type.GetType("System.String"))


    '        Dim DRCuentas As New DataRelation("MaestroDetalle", DSsubCuenta.Tables("cuentas").Columns("Cuenta"), DSsubCuenta.Tables("chequeras").Columns("cha_cuebco"), False)
    '        DSsubCuenta.Relations.Add(DRCuentas)

    '        DRCuentas = New DataRelation("Cheques", DSsubCuenta.Tables("chequeras").Columns("chequera"), DSsubCuenta.Tables("cheques").Columns("che_nrochequera"), False)
    '        DSsubCuenta.Relations.Add(DRCuentas)

    '        UGCtaBco.DataSource = DSsubCuenta.Tables(0)
    '        With UGCtaBco.DisplayLayout.Bands(0)
    '            .Columns(1).Width = 300
    '            Dim column As UltraGridColumn = Me.UGCtaBco.DisplayLayout.Bands(0).Columns("Agregar")

    '            column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
    '            column.Width = 50
    '            .Columns("Agregar").Header.Caption = ""
    '            .Columns("Agregar").Style = UltraWinGrid.ColumnStyle.Button
    '            .Columns("Agregar").CellButtonAppearance.Image = Me.ImageList1.Images(0)
    '            .Columns("Agregar").CellButtonAppearance.ImageHAlign = HAlign.Center


    '        End With
    '        With UGCtaBco.DisplayLayout.Bands(1)
    '            .Columns(1).Width = 70
    '            .Columns(2).Width = 80
    '            .Columns(3).Width = 40
    '            .Columns(4).Width = 100
    '            .Columns(5).Width = 100
    '            .Columns(6).Hidden = True
    '            Dim column As UltraGridColumn = Me.UGCtaBco.DisplayLayout.Bands(1).Columns("Asignar")

    '            column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
    '            column.Width = 50
    '            .Columns("Asignar").Header.Caption = ""
    '            .Columns("Asignar").Style = UltraWinGrid.ColumnStyle.Button
    '            .Columns("Asignar").CellButtonAppearance.Image = Me.ImageList1.Images(1)
    '            .Columns("Asignar").CellButtonAppearance.ImageHAlign = HAlign.Center

    '        End With
    '        With UGCtaBco.DisplayLayout.Bands(2)
    '            .Columns(0).Hidden = True
    '            .Columns(1).Width = 25
    '            .Columns(2).Width = 90
    '            .Columns(3).Width = 80
    '            .Columns(4).Width = 80
    '            .Columns(5).CellAppearance.TextHAlign = HAlign.Right
    '            .Columns(6).Width = 200
    '            .Columns(9).Hidden = True
    '            Dim column As UltraGridColumn = Me.UGCtaBco.DisplayLayout.Bands(2).Columns("Asignar")

    '            column.ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always
    '            column.Width = 50
    '            .Columns("Asignar").Header.Caption = ""
    '            .Columns("Asignar").Style = UltraWinGrid.ColumnStyle.Button
    '            .Columns("Asignar").CellButtonAppearance.Image = Me.ImageList1.Images(1)
    '            .Columns("Asignar").CellButtonAppearance.ImageHAlign = HAlign.Center

    '        End With
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Private Sub CuentaBanco()
        Try
            DSsubCuenta = New DataSet

            DASubcuenta = c_cnn.consultaBDadapter("plancuen", "pla_nropla as Cuenta,pla_nombre as Descripcion", "pla_subcta='" & c_drProf.Item("afi_tipdoc") & Format(c_drProf.Item("afi_nrodoc"), "00000000") & "'")
            DASubcuenta.Fill(DSsubCuenta, "cuentas")
            DSsubCuenta.Tables("cuentas").Columns.Add("cheques", Type.GetType("System.String"))
            DSsubCuenta.Tables("cuentas").Columns.Add("imprimir", Type.GetType("System.String"))
            DSsubCuenta.Tables("cuentas").Columns.Add("conciliar", Type.GetType("System.String"))

            bindingCuenta = New BindingSource
            bindingCuenta.DataSource = DSsubCuenta.Tables("cuentas")

            UGCtaBco.DataSource = DSsubCuenta.Tables(0)
            With UGCtaBco.DisplayLayout.Bands(0)
                .Columns(0).Width = 70
                .Columns(1).Width = 300
                Dim column As UltraGridColumn = UGCtaBco.DisplayLayout.Bands(0).Columns("cheques")

                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 65
                ' .Columns("cheques").Header.Caption = ""
                .Columns("cheques").Style = ColumnStyle.Button
                .Columns("cheques").CellButtonAppearance.Image = ImageList1.Images(3)
                .Columns("cheques").CellButtonAppearance.ImageHAlign = HAlign.Center

                column = UGCtaBco.DisplayLayout.Bands(0).Columns("imprimir")

                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 65
                ' .Columns("imprimir").Header.Caption = ""
                .Columns("imprimir").Style = ColumnStyle.Button
                .Columns("imprimir").CellButtonAppearance.Image = ImageList1.Images(4)
                .Columns("imprimir").CellButtonAppearance.ImageHAlign = HAlign.Center

                column = UGCtaBco.DisplayLayout.Bands(0).Columns("conciliar")

                column.ButtonDisplayStyle = UltraWinGrid.ButtonDisplayStyle.Always
                column.Width = 65
                .Columns("conciliar").Style = ColumnStyle.Button
                .Columns("conciliar").CellButtonAppearance.Image = ImageList1.Images(5)
                .Columns("conciliar").CellButtonAppearance.ImageHAlign = HAlign.Center
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmFichaBancos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If l_alta Then
            If c_dsProf.Tables(0).Rows.Count = 0 Then
                TxDocumento.Text = 1
            Else
                TxDocumento.Text = c_dsProf.Tables(0).Rows(c_dsProf.Tables(0).Rows.Count - 1).Item("afi_nrodoc") + 1
            End If
            c_drProf = c_dsProf.Tables(0).NewRow
            EditarBanco(True)
        Else
            c_drProf = c_dsProf.Tables(0).Rows(n_nRow)
            cargoDatosBanco()
            CuentaBanco()
            EditarBanco(False)
        End If
    End Sub

    Private Sub CButton1_Click(ByVal Sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton1.Click
        'Controlo permiso Update
        If controlAcceso(1, 4, "U", True) = True Then
            CButton2.Enabled = True
            EditarBanco(True)
        End If
    End Sub

    Private Sub CButton2_Click(ByVal Sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CButton2.Click
        'Controlo permiso Create o Update
        If controlAcceso(1, 4, "CU", True) = True Then
            CButton2.Enabled = False
            ActualizoDatosBanco()
            EditarBanco(False)
        End If
    End Sub

    Private Sub UGCtaBco_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.CellEventArgs) Handles UGCtaBco.ClickCellButton
        Select Case e.Cell.Column.Index
            Case 2 ' agrega cheques
                Dim frmChequeras As New FrmAltaChequeras
                frmChequeras.NumeroBanco = TxDocumento.Text
                frmChequeras.CuentaBanco = DSsubCuenta.Tables(0).Rows(UGCtaBco.ActiveRow.Index).Item("cuenta")
                frmChequeras.ShowDialog()
                If frmChequeras.DialogResult = DialogResult.OK Then
                    CuentaBanco()
                End If
            Case 3  'Asignar Orden de pagos e imprimir cheques
                Dim frmImpChequeras As New FrmImpresionCheques(DSsubCuenta.Tables(0).Rows(UGCtaBco.ActiveRow.Index))
                frmImpChequeras.ShowDialog()
            Case 4
                Dim frmConciliar As New FrmConciliarBanco(DSsubCuenta.Tables(0).Rows(UGCtaBco.ActiveRow.Index))
                frmConciliar.MdiParent = MdiParent
                frmConciliar.Show()
        End Select
     
    End Sub

    Private Sub UGCtaBco_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeRowEventArgs) Handles UGCtaBco.InitializeRow
        UGCtaBco.DisplayLayout.Override.AllowColSizing = AllowColSizing.Free

    End Sub
End Class