﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmImpresionCheques
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmImpresionCheques))
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGOrdPagos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.CBtAsignarAuto = New System.Windows.Forms.Button()
        Me.CBtImprimir = New System.Windows.Forms.Button()
        Me.UltraDropDown1 = New Infragistics.Win.UltraWinGrid.UltraDropDown()
        Me.HojaImpresion = New System.Drawing.Printing.PrintDocument()
        Me.HojaImpDialog = New System.Windows.Forms.PrintDialog()
        Me.CBtReasignacion = New System.Windows.Forms.Button()
        CType(Me.UGOrdPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraDropDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGOrdPagos
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGOrdPagos.DisplayLayout.Appearance = Appearance1
        Me.UGOrdPagos.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGOrdPagos.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGOrdPagos.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[True]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGOrdPagos.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGOrdPagos.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGOrdPagos.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGOrdPagos.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGOrdPagos.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGOrdPagos.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGOrdPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGOrdPagos.Location = New System.Drawing.Point(0, 0)
        Me.UGOrdPagos.Name = "UGOrdPagos"
        Me.UGOrdPagos.Size = New System.Drawing.Size(1034, 419)
        Me.UGOrdPagos.TabIndex = 149
        Me.UGOrdPagos.Text = "Ordenes de Pagos"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "add.ico")
        Me.ImageList1.Images.SetKeyName(1, "Search (2).ico")
        Me.ImageList1.Images.SetKeyName(2, "delete.ico")
        Me.ImageList1.Images.SetKeyName(3, "application_go.png")
        '
        'CBtAsignarAuto
        '
        Me.CBtAsignarAuto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAsignarAuto.ForeColor = System.Drawing.Color.Black
        Me.CBtAsignarAuto.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Play_32xMD_color
        Me.CBtAsignarAuto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAsignarAuto.Location = New System.Drawing.Point(254, 425)
        Me.CBtAsignarAuto.Name = "CBtAsignarAuto"
        Me.CBtAsignarAuto.Size = New System.Drawing.Size(200, 44)
        Me.CBtAsignarAuto.TabIndex = 150
        Me.CBtAsignarAuto.Text = "Asignación Automatica"
        Me.CBtAsignarAuto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CBtImprimir
        '
        Me.CBtImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtImprimir.ForeColor = System.Drawing.Color.Black
        Me.CBtImprimir.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.CBtImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtImprimir.Location = New System.Drawing.Point(480, 425)
        Me.CBtImprimir.Name = "CBtImprimir"
        Me.CBtImprimir.Size = New System.Drawing.Size(161, 44)
        Me.CBtImprimir.TabIndex = 151
        Me.CBtImprimir.Text = "Imprimir"
        '
        'UltraDropDown1
        '
        Appearance6.BackColor = System.Drawing.Color.White
        Me.UltraDropDown1.DisplayLayout.Appearance = Appearance6
        Appearance7.BackColor = System.Drawing.Color.Transparent
        Me.UltraDropDown1.DisplayLayout.Override.CardAreaAppearance = Appearance7
        Appearance8.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance8.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance8.FontData.BoldAsString = "True"
        Appearance8.FontData.Name = "Arial"
        Appearance8.FontData.SizeInPoints = 10.0!
        Appearance8.ForeColor = System.Drawing.Color.White
        Appearance8.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UltraDropDown1.DisplayLayout.Override.HeaderAppearance = Appearance8
        Me.UltraDropDown1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Appearance9.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UltraDropDown1.DisplayLayout.Override.RowSelectorAppearance = Appearance9
        Appearance10.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance10.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UltraDropDown1.DisplayLayout.Override.SelectedRowAppearance = Appearance10
        Me.UltraDropDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UltraDropDown1.Location = New System.Drawing.Point(323, 148)
        Me.UltraDropDown1.Name = "UltraDropDown1"
        Me.UltraDropDown1.Size = New System.Drawing.Size(486, 80)
        Me.UltraDropDown1.TabIndex = 152
        Me.UltraDropDown1.Visible = False
        '
        'HojaImpresion
        '
        '
        'HojaImpDialog
        '
        Me.HojaImpDialog.UseEXDialog = True
        '
        'CBtReasignacion
        '
        Me.CBtReasignacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtReasignacion.ForeColor = System.Drawing.Color.Black
        Me.CBtReasignacion.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Warning_32xMD_color
        Me.CBtReasignacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtReasignacion.Location = New System.Drawing.Point(662, 425)
        Me.CBtReasignacion.Name = "CBtReasignacion"
        Me.CBtReasignacion.Size = New System.Drawing.Size(173, 44)
        Me.CBtReasignacion.TabIndex = 153
        Me.CBtReasignacion.Text = "Reasignación"
        '
        'FrmImpresionCheques
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1034, 472)
        Me.Controls.Add(Me.CBtReasignacion)
        Me.Controls.Add(Me.UltraDropDown1)
        Me.Controls.Add(Me.CBtImprimir)
        Me.Controls.Add(Me.CBtAsignarAuto)
        Me.Controls.Add(Me.UGOrdPagos)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmImpresionCheques"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión de Cheques"
        CType(Me.UGOrdPagos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraDropDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UGOrdPagos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CBtAsignarAuto As System.Windows.Forms.Button
    Friend WithEvents CBtImprimir As System.Windows.Forms.Button
    Friend WithEvents UltraDropDown1 As Infragistics.Win.UltraWinGrid.UltraDropDown
    Friend WithEvents HojaImpresion As System.Drawing.Printing.PrintDocument
    Friend WithEvents HojaImpDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents CBtReasignacion As System.Windows.Forms.Button
End Class
