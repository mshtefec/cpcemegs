﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmProfesionales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraTab1 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraTab2 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Dim Appearance29 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraTab3 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Dim Appearance30 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraTab4 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Dim Appearance31 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraTab5 As Infragistics.Win.UltraWinTabControl.UltraTab = New Infragistics.Win.UltraWinTabControl.UltraTab()
        Dim Appearance32 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmProfesionales))
        Me.UltraTabPageControlProfesionales = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.CBDebito = New System.Windows.Forms.Button()
        Me.CButton5 = New System.Windows.Forms.Button()
        Me.UGprofesionales = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBAltaProfesional = New System.Windows.Forms.Button()
        Me.CButton1 = New System.Windows.Forms.Button()
        Me.UltraTabPageControlComitentes = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.CBtSeleccionar = New System.Windows.Forms.Button()
        Me.UGcomitentes = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtAltaCom = New System.Windows.Forms.Button()
        Me.CBtFichaCom = New System.Windows.Forms.Button()
        Me.UltraTabPageControlProveedores = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.UGproveedores = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtAltaPrv = New System.Windows.Forms.Button()
        Me.CBtFichaPrv = New System.Windows.Forms.Button()
        Me.UltraTabPageControlBancos = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.UGbancos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtAltaBco = New System.Windows.Forms.Button()
        Me.cBtFichaBco = New System.Windows.Forms.Button()
        Me.BListarTodos = New System.Windows.Forms.Button()
        Me.UltraTabPageControlEmpleados = New Infragistics.Win.UltraWinTabControl.UltraTabPageControl()
        Me.CBtAltEmp = New System.Windows.Forms.Button()
        Me.CBtFicEmp = New System.Windows.Forms.Button()
        Me.UGEmpleados = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraTabControl1 = New Infragistics.Win.UltraWinTabControl.UltraTabControl()
        Me.UltraTabSharedControlsPage1 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage()
        Me.UltraTabSharedControlsPage2 = New Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage()
        Me.CBDebitoEmpleados = New System.Windows.Forms.Button()
        Me.UltraTabPageControlProfesionales.SuspendLayout()
        CType(Me.UGprofesionales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControlComitentes.SuspendLayout()
        CType(Me.UGcomitentes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControlProveedores.SuspendLayout()
        CType(Me.UGproveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControlBancos.SuspendLayout()
        CType(Me.UGbancos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabPageControlEmpleados.SuspendLayout()
        CType(Me.UGEmpleados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraTabControl1.SuspendLayout()
        Me.UltraTabSharedControlsPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UltraTabPageControlProfesionales
        '
        Me.UltraTabPageControlProfesionales.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.UltraTabPageControlProfesionales.Controls.Add(Me.CBDebito)
        Me.UltraTabPageControlProfesionales.Controls.Add(Me.CButton5)
        Me.UltraTabPageControlProfesionales.Controls.Add(Me.UGprofesionales)
        Me.UltraTabPageControlProfesionales.Controls.Add(Me.CBAltaProfesional)
        Me.UltraTabPageControlProfesionales.Controls.Add(Me.CButton1)
        Me.UltraTabPageControlProfesionales.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControlProfesionales.Name = "UltraTabPageControlProfesionales"
        Me.UltraTabPageControlProfesionales.Size = New System.Drawing.Size(982, 554)
        '
        'CBDebito
        '
        Me.CBDebito.BackColor = System.Drawing.Color.Transparent
        Me.CBDebito.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBDebito.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Alert_32xMD_color
        Me.CBDebito.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBDebito.Location = New System.Drawing.Point(768, 513)
        Me.CBDebito.Name = "CBDebito"
        Me.CBDebito.Size = New System.Drawing.Size(120, 38)
        Me.CBDebito.TabIndex = 15
        Me.CBDebito.Text = "Debito"
        Me.CBDebito.UseVisualStyleBackColor = False
        '
        'CButton5
        '
        Me.CButton5.BackColor = System.Drawing.Color.Transparent
        Me.CButton5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButton5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton5.ImageIndex = 0
        Me.CButton5.Location = New System.Drawing.Point(309, 513)
        Me.CButton5.Name = "CButton5"
        Me.CButton5.Size = New System.Drawing.Size(120, 38)
        Me.CButton5.TabIndex = 14
        Me.CButton5.Text = "Categorias"
        Me.CButton5.UseVisualStyleBackColor = False
        Me.CButton5.Visible = False
        '
        'UGprofesionales
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGprofesionales.DisplayLayout.Appearance = Appearance1
        Me.UGprofesionales.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGprofesionales.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGprofesionales.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGprofesionales.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Me.UGprofesionales.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGprofesionales.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGprofesionales.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGprofesionales.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGprofesionales.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGprofesionales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGprofesionales.Location = New System.Drawing.Point(0, 0)
        Me.UGprofesionales.Name = "UGprofesionales"
        Me.UGprofesionales.Size = New System.Drawing.Size(979, 510)
        Me.UGprofesionales.TabIndex = 13
        Me.UGprofesionales.Text = "Profesionales"
        '
        'CBAltaProfesional
        '
        Me.CBAltaProfesional.BackColor = System.Drawing.Color.Transparent
        Me.CBAltaProfesional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBAltaProfesional.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBAltaProfesional.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBAltaProfesional.Location = New System.Drawing.Point(101, 513)
        Me.CBAltaProfesional.Name = "CBAltaProfesional"
        Me.CBAltaProfesional.Size = New System.Drawing.Size(94, 38)
        Me.CBAltaProfesional.TabIndex = 12
        Me.CBAltaProfesional.Text = "Alta"
        Me.CBAltaProfesional.UseVisualStyleBackColor = False
        '
        'CButton1
        '
        Me.CButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CButton1.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CButton1.Location = New System.Drawing.Point(1, 513)
        Me.CButton1.Name = "CButton1"
        Me.CButton1.Size = New System.Drawing.Size(94, 38)
        Me.CButton1.TabIndex = 11
        Me.CButton1.Text = "Ficha"
        Me.CButton1.UseVisualStyleBackColor = True
        '
        'UltraTabPageControlComitentes
        '
        Me.UltraTabPageControlComitentes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.UltraTabPageControlComitentes.Controls.Add(Me.CBtSeleccionar)
        Me.UltraTabPageControlComitentes.Controls.Add(Me.UGcomitentes)
        Me.UltraTabPageControlComitentes.Controls.Add(Me.CBtAltaCom)
        Me.UltraTabPageControlComitentes.Controls.Add(Me.CBtFichaCom)
        Me.UltraTabPageControlComitentes.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControlComitentes.Name = "UltraTabPageControlComitentes"
        Me.UltraTabPageControlComitentes.Size = New System.Drawing.Size(982, 554)
        '
        'CBtSeleccionar
        '
        Me.CBtSeleccionar.BackColor = System.Drawing.Color.Transparent
        Me.CBtSeleccionar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtSeleccionar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.CBtSeleccionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtSeleccionar.Location = New System.Drawing.Point(720, 513)
        Me.CBtSeleccionar.Name = "CBtSeleccionar"
        Me.CBtSeleccionar.Size = New System.Drawing.Size(165, 38)
        Me.CBtSeleccionar.TabIndex = 16
        Me.CBtSeleccionar.Text = "Seleccionar"
        Me.CBtSeleccionar.UseVisualStyleBackColor = False
        Me.CBtSeleccionar.Visible = False
        '
        'UGcomitentes
        '
        Appearance6.BackColor = System.Drawing.Color.White
        Me.UGcomitentes.DisplayLayout.Appearance = Appearance6
        Me.UGcomitentes.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGcomitentes.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGcomitentes.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance7.BackColor = System.Drawing.Color.Transparent
        Me.UGcomitentes.DisplayLayout.Override.CardAreaAppearance = Appearance7
        Me.UGcomitentes.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance8.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance8.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance8.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance8.FontData.BoldAsString = "True"
        Appearance8.FontData.Name = "Arial"
        Appearance8.FontData.SizeInPoints = 10.0!
        Appearance8.ForeColor = System.Drawing.Color.White
        Appearance8.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGcomitentes.DisplayLayout.Override.HeaderAppearance = Appearance8
        Me.UGcomitentes.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance9.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGcomitentes.DisplayLayout.Override.RowSelectorAppearance = Appearance9
        Appearance10.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance10.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGcomitentes.DisplayLayout.Override.SelectedRowAppearance = Appearance10
        Me.UGcomitentes.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGcomitentes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGcomitentes.Location = New System.Drawing.Point(0, 0)
        Me.UGcomitentes.Name = "UGcomitentes"
        Me.UGcomitentes.Size = New System.Drawing.Size(978, 510)
        Me.UGcomitentes.TabIndex = 15
        Me.UGcomitentes.Text = "Comitentes"
        '
        'CBtAltaCom
        '
        Me.CBtAltaCom.BackColor = System.Drawing.Color.Transparent
        Me.CBtAltaCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAltaCom.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBtAltaCom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAltaCom.Location = New System.Drawing.Point(101, 513)
        Me.CBtAltaCom.Name = "CBtAltaCom"
        Me.CBtAltaCom.Size = New System.Drawing.Size(94, 38)
        Me.CBtAltaCom.TabIndex = 14
        Me.CBtAltaCom.Text = "Alta"
        Me.CBtAltaCom.UseVisualStyleBackColor = False
        '
        'CBtFichaCom
        '
        Me.CBtFichaCom.BackColor = System.Drawing.Color.Transparent
        Me.CBtFichaCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtFichaCom.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CBtFichaCom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtFichaCom.Location = New System.Drawing.Point(1, 513)
        Me.CBtFichaCom.Name = "CBtFichaCom"
        Me.CBtFichaCom.Size = New System.Drawing.Size(94, 38)
        Me.CBtFichaCom.TabIndex = 13
        Me.CBtFichaCom.Text = "Ficha"
        Me.CBtFichaCom.UseVisualStyleBackColor = False
        '
        'UltraTabPageControlProveedores
        '
        Me.UltraTabPageControlProveedores.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.UltraTabPageControlProveedores.Controls.Add(Me.UGproveedores)
        Me.UltraTabPageControlProveedores.Controls.Add(Me.CBtAltaPrv)
        Me.UltraTabPageControlProveedores.Controls.Add(Me.CBtFichaPrv)
        Me.UltraTabPageControlProveedores.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControlProveedores.Name = "UltraTabPageControlProveedores"
        Me.UltraTabPageControlProveedores.Size = New System.Drawing.Size(982, 554)
        '
        'UGproveedores
        '
        Appearance11.BackColor = System.Drawing.Color.White
        Me.UGproveedores.DisplayLayout.Appearance = Appearance11
        Me.UGproveedores.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGproveedores.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGproveedores.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance12.BackColor = System.Drawing.Color.Transparent
        Me.UGproveedores.DisplayLayout.Override.CardAreaAppearance = Appearance12
        Me.UGproveedores.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance13.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance13.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance13.FontData.BoldAsString = "True"
        Appearance13.FontData.Name = "Arial"
        Appearance13.FontData.SizeInPoints = 10.0!
        Appearance13.ForeColor = System.Drawing.Color.White
        Appearance13.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGproveedores.DisplayLayout.Override.HeaderAppearance = Appearance13
        Me.UGproveedores.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance14.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance14.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGproveedores.DisplayLayout.Override.RowSelectorAppearance = Appearance14
        Appearance15.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance15.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGproveedores.DisplayLayout.Override.SelectedRowAppearance = Appearance15
        Me.UGproveedores.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGproveedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGproveedores.Location = New System.Drawing.Point(0, 0)
        Me.UGproveedores.Name = "UGproveedores"
        Me.UGproveedores.Size = New System.Drawing.Size(978, 510)
        Me.UGproveedores.TabIndex = 20
        Me.UGproveedores.Text = "Proveedores"
        '
        'CBtAltaPrv
        '
        Me.CBtAltaPrv.BackColor = System.Drawing.Color.Transparent
        Me.CBtAltaPrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAltaPrv.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBtAltaPrv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAltaPrv.Location = New System.Drawing.Point(101, 513)
        Me.CBtAltaPrv.Name = "CBtAltaPrv"
        Me.CBtAltaPrv.Size = New System.Drawing.Size(94, 38)
        Me.CBtAltaPrv.TabIndex = 19
        Me.CBtAltaPrv.Text = "Alta"
        Me.CBtAltaPrv.UseVisualStyleBackColor = False
        '
        'CBtFichaPrv
        '
        Me.CBtFichaPrv.BackColor = System.Drawing.Color.Transparent
        Me.CBtFichaPrv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtFichaPrv.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CBtFichaPrv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtFichaPrv.Location = New System.Drawing.Point(1, 513)
        Me.CBtFichaPrv.Name = "CBtFichaPrv"
        Me.CBtFichaPrv.Size = New System.Drawing.Size(94, 38)
        Me.CBtFichaPrv.TabIndex = 18
        Me.CBtFichaPrv.Text = "Ficha"
        Me.CBtFichaPrv.UseVisualStyleBackColor = False
        '
        'UltraTabPageControlBancos
        '
        Me.UltraTabPageControlBancos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.UltraTabPageControlBancos.Controls.Add(Me.UGbancos)
        Me.UltraTabPageControlBancos.Controls.Add(Me.CBtAltaBco)
        Me.UltraTabPageControlBancos.Controls.Add(Me.cBtFichaBco)
        Me.UltraTabPageControlBancos.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabPageControlBancos.Name = "UltraTabPageControlBancos"
        Me.UltraTabPageControlBancos.Size = New System.Drawing.Size(982, 554)
        '
        'UGbancos
        '
        Appearance16.BackColor = System.Drawing.Color.White
        Me.UGbancos.DisplayLayout.Appearance = Appearance16
        Me.UGbancos.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGbancos.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGbancos.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance17.BackColor = System.Drawing.Color.Transparent
        Me.UGbancos.DisplayLayout.Override.CardAreaAppearance = Appearance17
        Me.UGbancos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance18.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance18.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance18.FontData.BoldAsString = "True"
        Appearance18.FontData.Name = "Arial"
        Appearance18.FontData.SizeInPoints = 10.0!
        Appearance18.ForeColor = System.Drawing.Color.White
        Appearance18.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGbancos.DisplayLayout.Override.HeaderAppearance = Appearance18
        Me.UGbancos.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance19.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance19.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance19.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGbancos.DisplayLayout.Override.RowSelectorAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance20.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGbancos.DisplayLayout.Override.SelectedRowAppearance = Appearance20
        Me.UGbancos.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGbancos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGbancos.Location = New System.Drawing.Point(0, 0)
        Me.UGbancos.Name = "UGbancos"
        Me.UGbancos.Size = New System.Drawing.Size(978, 510)
        Me.UGbancos.TabIndex = 25
        Me.UGbancos.Text = "Bancos"
        '
        'CBtAltaBco
        '
        Me.CBtAltaBco.BackColor = System.Drawing.Color.Transparent
        Me.CBtAltaBco.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAltaBco.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBtAltaBco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAltaBco.Location = New System.Drawing.Point(101, 513)
        Me.CBtAltaBco.Name = "CBtAltaBco"
        Me.CBtAltaBco.Size = New System.Drawing.Size(94, 38)
        Me.CBtAltaBco.TabIndex = 24
        Me.CBtAltaBco.Text = "Alta"
        Me.CBtAltaBco.UseVisualStyleBackColor = False
        '
        'cBtFichaBco
        '
        Me.cBtFichaBco.BackColor = System.Drawing.Color.Transparent
        Me.cBtFichaBco.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cBtFichaBco.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.cBtFichaBco.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cBtFichaBco.Location = New System.Drawing.Point(1, 513)
        Me.cBtFichaBco.Name = "cBtFichaBco"
        Me.cBtFichaBco.Size = New System.Drawing.Size(94, 38)
        Me.cBtFichaBco.TabIndex = 23
        Me.cBtFichaBco.Text = "Ficha"
        Me.cBtFichaBco.UseVisualStyleBackColor = False
        '
        'BListarTodos
        '
        Me.BListarTodos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BListarTodos.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Play_32xMD_color
        Me.BListarTodos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BListarTodos.Location = New System.Drawing.Point(519, 513)
        Me.BListarTodos.Name = "BListarTodos"
        Me.BListarTodos.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BListarTodos.Size = New System.Drawing.Size(152, 38)
        Me.BListarTodos.TabIndex = 0
        Me.BListarTodos.Text = "Listar Todos"
        Me.BListarTodos.UseVisualStyleBackColor = True
        '
        'UltraTabPageControlEmpleados
        '
        Me.UltraTabPageControlEmpleados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.UltraTabPageControlEmpleados.Controls.Add(Me.CBDebitoEmpleados)
        Me.UltraTabPageControlEmpleados.Controls.Add(Me.CBtAltEmp)
        Me.UltraTabPageControlEmpleados.Controls.Add(Me.CBtFicEmp)
        Me.UltraTabPageControlEmpleados.Controls.Add(Me.UGEmpleados)
        Me.UltraTabPageControlEmpleados.Controls.Add(Me.BListarTodos)
        Me.UltraTabPageControlEmpleados.Location = New System.Drawing.Point(96, 2)
        Me.UltraTabPageControlEmpleados.Name = "UltraTabPageControlEmpleados"
        Me.UltraTabPageControlEmpleados.Size = New System.Drawing.Size(982, 554)
        '
        'CBtAltEmp
        '
        Me.CBtAltEmp.BackColor = System.Drawing.Color.Transparent
        Me.CBtAltEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtAltEmp.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CBtAltEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtAltEmp.Location = New System.Drawing.Point(101, 513)
        Me.CBtAltEmp.Name = "CBtAltEmp"
        Me.CBtAltEmp.Size = New System.Drawing.Size(94, 38)
        Me.CBtAltEmp.TabIndex = 18
        Me.CBtAltEmp.Text = "Alta"
        Me.CBtAltEmp.UseVisualStyleBackColor = False
        '
        'CBtFicEmp
        '
        Me.CBtFicEmp.BackColor = System.Drawing.Color.Transparent
        Me.CBtFicEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtFicEmp.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.CBtFicEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtFicEmp.Location = New System.Drawing.Point(1, 513)
        Me.CBtFicEmp.Name = "CBtFicEmp"
        Me.CBtFicEmp.Size = New System.Drawing.Size(94, 38)
        Me.CBtFicEmp.TabIndex = 17
        Me.CBtFicEmp.Text = "Ficha"
        Me.CBtFicEmp.UseVisualStyleBackColor = False
        '
        'UGEmpleados
        '
        Appearance21.BackColor = System.Drawing.Color.White
        Me.UGEmpleados.DisplayLayout.Appearance = Appearance21
        Me.UGEmpleados.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGEmpleados.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGEmpleados.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance22.BackColor = System.Drawing.Color.Transparent
        Me.UGEmpleados.DisplayLayout.Override.CardAreaAppearance = Appearance22
        Me.UGEmpleados.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance23.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance23.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance23.FontData.BoldAsString = "True"
        Appearance23.FontData.Name = "Arial"
        Appearance23.FontData.SizeInPoints = 10.0!
        Appearance23.ForeColor = System.Drawing.Color.White
        Appearance23.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGEmpleados.DisplayLayout.Override.HeaderAppearance = Appearance23
        Me.UGEmpleados.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance24.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance24.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGEmpleados.DisplayLayout.Override.RowSelectorAppearance = Appearance24
        Appearance25.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance25.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGEmpleados.DisplayLayout.Override.SelectedRowAppearance = Appearance25
        Me.UGEmpleados.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGEmpleados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGEmpleados.Location = New System.Drawing.Point(0, 0)
        Me.UGEmpleados.Name = "UGEmpleados"
        Me.UGEmpleados.Size = New System.Drawing.Size(978, 510)
        Me.UGEmpleados.TabIndex = 16
        Me.UGEmpleados.Text = "Empleados"
        '
        'UltraTabControl1
        '
        Appearance26.BackColor2 = System.Drawing.Color.DarkBlue
        Appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UltraTabControl1.ActiveTabAppearance = Appearance26
        Appearance27.BackColor2 = System.Drawing.Color.Blue
        Appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.GlassBottom20Bright
        Me.UltraTabControl1.ClientAreaAppearance = Appearance27
        Me.UltraTabControl1.Controls.Add(Me.UltraTabSharedControlsPage1)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControlComitentes)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControlProveedores)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControlProfesionales)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControlBancos)
        Me.UltraTabControl1.Controls.Add(Me.UltraTabPageControlEmpleados)
        Me.UltraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraTabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.UltraTabControl1.Name = "UltraTabControl1"
        Me.UltraTabControl1.SharedControls.AddRange(New System.Windows.Forms.Control() {Me.BListarTodos})
        Me.UltraTabControl1.SharedControlsPage = Me.UltraTabSharedControlsPage1
        Me.UltraTabControl1.Size = New System.Drawing.Size(1080, 558)
        Me.UltraTabControl1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage
        Me.UltraTabControl1.TabButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        Me.UltraTabControl1.TabIndex = 8
        Me.UltraTabControl1.TabOrientation = Infragistics.Win.UltraWinTabs.TabOrientation.LeftTop
        Appearance28.BackColor2 = System.Drawing.Color.Blue
        UltraTab1.ActiveAppearance = Appearance28
        UltraTab1.TabPage = Me.UltraTabPageControlProfesionales
        UltraTab1.Text = "Profesionales"
        Appearance29.BackColor2 = System.Drawing.Color.Blue
        UltraTab2.ActiveAppearance = Appearance29
        UltraTab2.TabPage = Me.UltraTabPageControlComitentes
        UltraTab2.Text = "Comitentes"
        Appearance30.BackColor2 = System.Drawing.Color.Blue
        UltraTab3.ActiveAppearance = Appearance30
        UltraTab3.TabPage = Me.UltraTabPageControlProveedores
        UltraTab3.Text = "Proveedores"
        Appearance31.BackColor2 = System.Drawing.Color.Blue
        UltraTab4.ActiveAppearance = Appearance31
        UltraTab4.TabPage = Me.UltraTabPageControlBancos
        UltraTab4.Text = "Bancos"
        Appearance32.BackColor2 = System.Drawing.Color.Blue
        UltraTab5.ActiveAppearance = Appearance32
        UltraTab5.TabPage = Me.UltraTabPageControlEmpleados
        UltraTab5.Text = "Empleados"
        Me.UltraTabControl1.Tabs.AddRange(New Infragistics.Win.UltraWinTabControl.UltraTab() {UltraTab1, UltraTab2, UltraTab3, UltraTab4, UltraTab5})
        Me.UltraTabControl1.TextOrientation = Infragistics.Win.UltraWinTabs.TextOrientation.Horizontal
        Me.UltraTabControl1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2003
        '
        'UltraTabSharedControlsPage1
        '
        Me.UltraTabSharedControlsPage1.Controls.Add(Me.BListarTodos)
        Me.UltraTabSharedControlsPage1.Location = New System.Drawing.Point(-10000, -10000)
        Me.UltraTabSharedControlsPage1.Name = "UltraTabSharedControlsPage1"
        Me.UltraTabSharedControlsPage1.Size = New System.Drawing.Size(982, 554)
        '
        'UltraTabSharedControlsPage2
        '
        Me.UltraTabSharedControlsPage2.Location = New System.Drawing.Point(96, 2)
        Me.UltraTabSharedControlsPage2.Name = "UltraTabSharedControlsPage2"
        Me.UltraTabSharedControlsPage2.Size = New System.Drawing.Size(686, 444)
        '
        'CBDebitoEmpleados
        '
        Me.CBDebitoEmpleados.BackColor = System.Drawing.Color.Transparent
        Me.CBDebitoEmpleados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBDebitoEmpleados.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Alert_32xMD_color
        Me.CBDebitoEmpleados.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBDebitoEmpleados.Location = New System.Drawing.Point(768, 513)
        Me.CBDebitoEmpleados.Name = "CBDebitoEmpleados"
        Me.CBDebitoEmpleados.Size = New System.Drawing.Size(120, 38)
        Me.CBDebitoEmpleados.TabIndex = 19
        Me.CBDebitoEmpleados.Text = "Debito"
        Me.CBDebitoEmpleados.UseVisualStyleBackColor = False
        '
        'FrmProfesionales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1080, 558)
        Me.Controls.Add(Me.UltraTabControl1)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmProfesionales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Padrones"
        Me.UltraTabPageControlProfesionales.ResumeLayout(False)
        CType(Me.UGprofesionales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControlComitentes.ResumeLayout(False)
        CType(Me.UGcomitentes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControlProveedores.ResumeLayout(False)
        CType(Me.UGproveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControlBancos.ResumeLayout(False)
        CType(Me.UGbancos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabPageControlEmpleados.ResumeLayout(False)
        CType(Me.UGEmpleados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraTabControl1.ResumeLayout(False)
        Me.UltraTabSharedControlsPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CBAltaProfesional As System.Windows.Forms.Button
    Friend WithEvents CButton1 As System.Windows.Forms.Button
    Friend WithEvents CBtAltaCom As System.Windows.Forms.Button
    Friend WithEvents CBtFichaCom As System.Windows.Forms.Button
    Friend WithEvents CBtAltaPrv As System.Windows.Forms.Button
    Friend WithEvents CBtFichaPrv As System.Windows.Forms.Button
    Friend WithEvents CBtAltaBco As System.Windows.Forms.Button
    Friend WithEvents cBtFichaBco As System.Windows.Forms.Button
    Friend WithEvents CBtSeleccionar As System.Windows.Forms.Button
    Friend WithEvents CBtAltEmp As System.Windows.Forms.Button
    Friend WithEvents CBtFicEmp As System.Windows.Forms.Button
    Friend WithEvents CButton5 As System.Windows.Forms.Button
    Friend WithEvents CBDebito As System.Windows.Forms.Button
    Private WithEvents UltraTabControl1 As Infragistics.Win.UltraWinTabControl.UltraTabControl
    Private WithEvents UltraTabSharedControlsPage1 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Private WithEvents UltraTabPageControlComitentes As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Private WithEvents UltraTabPageControlProveedores As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Private WithEvents UltraTabPageControlProfesionales As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Private WithEvents UltraTabPageControlBancos As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Private WithEvents UltraTabSharedControlsPage2 As Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage
    Private WithEvents UGprofesionales As Infragistics.Win.UltraWinGrid.UltraGrid
    Private WithEvents UGcomitentes As Infragistics.Win.UltraWinGrid.UltraGrid
    Private WithEvents UGproveedores As Infragistics.Win.UltraWinGrid.UltraGrid
    Private WithEvents UGbancos As Infragistics.Win.UltraWinGrid.UltraGrid
    Private WithEvents UltraTabPageControlEmpleados As Infragistics.Win.UltraWinTabControl.UltraTabPageControl
    Private WithEvents UGEmpleados As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents BListarTodos As System.Windows.Forms.Button
    Friend WithEvents CBDebitoEmpleados As Button
End Class
