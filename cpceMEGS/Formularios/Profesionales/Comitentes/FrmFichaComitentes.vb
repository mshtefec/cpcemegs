﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Public Class FrmFichaComitentes
    Private c_drProf As DataRow
    Private c_dsProf As DataSet
    Private c_daProf As MySqlDataAdapter
    Private c_cnn As ConsultaBD
    Private l_alta As Boolean
    Private DATrabajo As MySqlDataAdapter
    Private DSTrabajo As DataSet
    Private dFecMysql As MySqlDateTime
    Private n_nRow As Integer
    Public Sub New(ByRef dsProf As DataSet, ByRef daProf As MySqlDataAdapter, ByVal nRow As Integer, ByVal lAlta As Boolean, ByVal conexion As ConsultaBD)
        InitializeComponent()
        c_dsProf = dsProf
        c_daProf = daProf
        n_nRow = nRow
        l_alta = lAlta
        c_cnn = conexion
    End Sub
    Private Sub FrmFichaComitentes_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If l_alta Then
            'If c_dsProf.Tables(0).Rows.Count = 0 Then
            '    TxDocumento.Text = 1
            'Else
            '    c_dsProf.Tables(0).DefaultView.Sort = "afi_nrodoc ASC"
            '    TxDocumento.Text = c_dsProf.Tables(0).Rows(c_dsProf.Tables(0).Rows.Count - 1).Item("afi_nrodoc") + 1
            'End If
            c_drProf = c_dsProf.Tables(0).NewRow
            EditarComitente(True)
        Else
            c_drProf = c_dsProf.Tables(0).Rows(n_nRow)
            cargoDatosComitente()
            EditarComitente(False)
            MuestraTrabajos()
        End If
    End Sub

    Private Sub cargoDatosComitente()
        Try
            CboTipdoc.Text = c_drProf.Item("afi_tipdoc")
            TxDocumento.Text = c_drProf.Item("afi_nrodoc")

            If InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") = 0 Then ' SI NO TIENE COMA
                TxApellido.Text = c_drProf.Item("afi_nombre")
            Else
                TxApellido.Text = Strings.Left((Trim(c_drProf.Item("afi_nombre"))), InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") - 1)
                TxNombre.Text = Strings.Mid(Trim(c_drProf.Item("afi_nombre")), InStr(1, Trim(c_drProf.Item("afi_nombre")), ",") + 2)
            End If

            cboTipoIva.Text = c_drProf.Item("afi_tipoiva")
            MtxCuit.Text = c_drProf.Item("afi_cuit")
            MtxDGR.Text = c_drProf.Item("afi_dgr")
            cboProvincia.Text = c_drProf.Item("afi_provincia")
            TxDireccion.Text = c_drProf.Item("afi_direccion")
            TxLocalidad.Text = c_drProf.Item("afi_localidad")
            TxCodPos.Text = c_drProf.Item("afi_codpos")
            MtxTelefono.Text = c_drProf.Item("afi_telefono1")
            MtxCelu.Text = c_drProf.Item("afi_telefono2")
            TxMail.Text = c_drProf.Item("afi_mail")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub EditarComitente(ByVal lEditar As Boolean)
        Try
            If l_alta Then
                CboTipdoc.Enabled = lEditar
                'Me.TxDocumento.Enabled = lEditar
                SelectEmpresa()
            Else
                If String.IsNullOrWhiteSpace(TxNombre.Text) Then
                    SelectEmpresa()
                Else
                    SelectPersona()
                End If
            End If

            CBTipo.Enabled = lEditar
            cboTipoIva.Enabled = lEditar
            cboProvincia.Enabled = lEditar
            TxApellido.Enabled = lEditar
            TxNombre.Enabled = lEditar
            MtxCuit.Enabled = lEditar
            MtxDGR.Enabled = lEditar
            TxDireccion.Enabled = lEditar
            TxLocalidad.Enabled = lEditar
            TxCodPos.Enabled = lEditar
            MtxTelefono.Enabled = lEditar
            MtxCelu.Enabled = lEditar
            TxMail.Enabled = lEditar
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ActualizoDatosProfesional()
        Try
            c_drProf.Item("afi_tipo") = "C"
            'afi_nombre
            If Trim(TxNombre.Text) = "" Then
                c_drProf.Item("afi_nombre") = Trim(TxApellido.Text)
            Else
                c_drProf.Item("afi_nombre") = Trim(TxApellido.Text) & ", " & Trim(TxNombre.Text)
            End If

            c_drProf.Item("afi_tipoiva") = cboTipoIva.Text
            'CUIT
            If MtxCuit.Text = "  -        -" Then
                c_drProf.Item("afi_cuit") = ""
            Else
                c_drProf.Item("afi_cuit") = MtxCuit.Text.Replace("-", "")
            End If

            c_drProf.Item("afi_dgr") = MtxDGR.Text
            c_drProf.Item("afi_provincia") = cboProvincia.Text
            c_drProf.Item("afi_direccion") = TxDireccion.Text
            c_drProf.Item("afi_localidad") = TxLocalidad.Text

            If IsNumeric(TxCodPos.Text) Then
                c_drProf.Item("afi_codpos") = Convert.ToInt16(TxCodPos.Text)
            Else
                c_drProf.Item("afi_codpos") = 0
            End If

            c_drProf.Item("afi_telefono1") = MtxTelefono.Text
            c_drProf.Item("afi_telefono2") = MtxCelu.Text
            c_drProf.Item("afi_mail") = TxMail.Text

            If ControlDatosIngresados() Then
                'Si es l_alta true tengo que hacer el Add es new
                If l_alta Then
                    c_drProf.Item("afi_tipdoc") = CboTipdoc.Text
                    c_cnn.AbrirConexion()
                    c_drProf.Item("afi_nrodoc") = c_cnn.TomaCobte(nPubNroIns, nPubNroCli, "COMITE") & Format(nPubNroCli, "00")
                    c_cnn.CerrarConexion()
                    c_dsProf.Tables(0).Rows.Add(c_drProf)
                End If
                'Es Edit
                c_daProf.Update(c_dsProf, "profesional")
                EditarComitente(False)
            Else
                c_dsProf.RejectChanges()
                MessageBox.Show("Compruebe los datos ingresados.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        Catch ex As Exception
            c_dsProf.RejectChanges()
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CButton1_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CButton1.Click
        'Controlo permiso Update
        If controlAcceso(1, 2, "U", True) = True Then
            EditarComitente(True)
        End If
    End Sub
    Private Sub CButtonActualizar_Click(sender As Object, e As EventArgs) Handles CButtonActualizar.Click
        'Controlo permiso Create o Update
        If controlAcceso(1, 2, "CU", True) = True Then
            ActualizoDatosProfesional()
        End If
    End Sub
    Private Sub MuestraTrabajos()
        Try
            DSTrabajo = New DataSet
            Dim tabla As String = "((((obleas a LEFT JOIN comitente ON comitente.afi_tipdoc=a.obl_tipcte AND comitente.afi_nrodoc=a.obl_nrocte) INNER JOIN afiliado c ON c.afi_tipdoc=a.obl_tippro AND c.afi_nrodoc=a.obl_nropro) INNER JOIN afiliado e ON e.afi_tipdoc=a.obl_tipimp AND e.afi_nrodoc=a.obl_nroimp) LEFT JOIN trabajo ON a.obl_nrolegali = trabajo.tra_nrolegali) LEFT JOIN tareas d ON d.tar_codigo=a.obl_tarea"
            Dim campos As String = "a.obl_nrolegali as NroLegalizacion, trabajo.id AS CodBarra, a.obl_fecha as Fecha, a.obl_fectrab as Cierre, a.obl_feccert as Certificado, a.obl_fecotor as Otorgado, c.afi_nombre as Profesional, e.afi_nombre as Impreso, d.tar_descrip as Trabajo, a.obl_estado as Estado, trabajo.tra_estado AS TrabajoEstado, trabajo.tra_certificado AS TrabajoCertificado"
            Dim condiciones As String = "comitente.afi_nrodoc = " & c_drProf.Item("afi_nrodoc") & " GROUP BY a.obl_nrolegali ORDER BY a.obl_nrolegali DESC, a.obl_item ASC"
            DATrabajo = c_cnn.consultaBDadapter(tabla, campos, condiciones)
            DATrabajo.Fill(DSTrabajo, "trabajo")
            UGtrabajos.DataSource = DSTrabajo.Tables(0)

            With UGtrabajos.DisplayLayout.Bands(0)
                '.Columns(0).Width = 70
                '.Columns(1).Width = 70
                .Columns(2).Width = 70
                .Columns(3).Width = 70
                .Columns(4).Width = 70
                .Columns(5).Width = 70
                '    .Columns(1).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center
                '    .Columns(2).Width = 100
                '    .Columns(2).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                '    .Columns(3).Width = 90
                '    .Columns(3).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right
                '    .Columns(4).Width = 225
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CBtCtaCte_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles CBtCtaCte.Click
        Dim FrmCuenta As New FrmCtaCte(c_cnn, c_drProf)
        '  formularios.Add(FrmCuenta)
        FrmCuenta.Show()
    End Sub

    Private Sub TxApellido_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxApellido.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxNombre_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxNombre.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxDireccion_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxDireccion.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxLocalidad_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TxLocalidad.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Function ControlDatosIngresados() As Boolean
        ControlDatosIngresados = True
        If String.IsNullOrWhiteSpace(c_drProf.Item("afi_cuit")) Or c_drProf.Item("afi_cuit").ToString.Count() <> 11 Then
            ControlDatosIngresados = False
        End If
        If String.IsNullOrWhiteSpace(TxApellido.Text) Or String.IsNullOrWhiteSpace(c_drProf.Item("afi_nombre")) Then
            ControlDatosIngresados = False
        End If
    End Function

    Private Sub SelectEmpresa()
        CBTipo.SelectedIndex = 0
        LApellido.Text = "Empresa:"
        LNombre.Hide()
        TxNombre.Text = ""
        TxNombre.Hide()
    End Sub

    Private Sub SelectPersona()
        CBTipo.SelectedIndex = 1
        LApellido.Text = "Apellido:"
        LNombre.Show()
        TxNombre.Show()
    End Sub

    Private Sub CBTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBTipo.SelectedIndexChanged
        If CBTipo.SelectedIndex = 0 Then
            SelectEmpresa()
        Else
            SelectPersona()
        End If
    End Sub
End Class