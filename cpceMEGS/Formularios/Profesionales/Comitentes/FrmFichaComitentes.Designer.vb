﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFichaComitentes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFichaComitentes))
        Me.UltraGroupBox2 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.CBTipo = New System.Windows.Forms.ComboBox()
        Me.LTipoComitente = New System.Windows.Forms.Label()
        Me.CBtCtaCte = New System.Windows.Forms.Button()
        Me.cboTipoIva = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CButtonActualizar = New System.Windows.Forms.Button()
        Me.CButton1 = New System.Windows.Forms.Button()
        Me.CboTipdoc = New System.Windows.Forms.ComboBox()
        Me.TxDocumento = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboProvincia = New System.Windows.Forms.ComboBox()
        Me.MtxDGR = New System.Windows.Forms.MaskedTextBox()
        Me.MtxCuit = New System.Windows.Forms.MaskedTextBox()
        Me.MtxCelu = New System.Windows.Forms.MaskedTextBox()
        Me.MtxTelefono = New System.Windows.Forms.MaskedTextBox()
        Me.TxMail = New System.Windows.Forms.TextBox()
        Me.TxLocalidad = New System.Windows.Forms.TextBox()
        Me.TxDireccion = New System.Windows.Forms.TextBox()
        Me.TxNombre = New System.Windows.Forms.TextBox()
        Me.TxApellido = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LNombre = New System.Windows.Forms.Label()
        Me.LApellido = New System.Windows.Forms.Label()
        Me.TxCodPos = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.UGtrabajos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox2.SuspendLayout()
        CType(Me.UGtrabajos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraGroupBox2
        '
        Me.UltraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularDoubleSolid
        Me.UltraGroupBox2.Controls.Add(Me.CBTipo)
        Me.UltraGroupBox2.Controls.Add(Me.LTipoComitente)
        Me.UltraGroupBox2.Controls.Add(Me.CBtCtaCte)
        Me.UltraGroupBox2.Controls.Add(Me.cboTipoIva)
        Me.UltraGroupBox2.Controls.Add(Me.Label1)
        Me.UltraGroupBox2.Controls.Add(Me.CButtonActualizar)
        Me.UltraGroupBox2.Controls.Add(Me.CButton1)
        Me.UltraGroupBox2.Controls.Add(Me.CboTipdoc)
        Me.UltraGroupBox2.Controls.Add(Me.TxDocumento)
        Me.UltraGroupBox2.Controls.Add(Me.Label15)
        Me.UltraGroupBox2.Controls.Add(Me.Label2)
        Me.UltraGroupBox2.Controls.Add(Me.cboProvincia)
        Me.UltraGroupBox2.Controls.Add(Me.MtxDGR)
        Me.UltraGroupBox2.Controls.Add(Me.MtxCuit)
        Me.UltraGroupBox2.Controls.Add(Me.MtxCelu)
        Me.UltraGroupBox2.Controls.Add(Me.MtxTelefono)
        Me.UltraGroupBox2.Controls.Add(Me.TxMail)
        Me.UltraGroupBox2.Controls.Add(Me.TxLocalidad)
        Me.UltraGroupBox2.Controls.Add(Me.TxDireccion)
        Me.UltraGroupBox2.Controls.Add(Me.TxNombre)
        Me.UltraGroupBox2.Controls.Add(Me.TxApellido)
        Me.UltraGroupBox2.Controls.Add(Me.Label24)
        Me.UltraGroupBox2.Controls.Add(Me.Label23)
        Me.UltraGroupBox2.Controls.Add(Me.Label22)
        Me.UltraGroupBox2.Controls.Add(Me.Label14)
        Me.UltraGroupBox2.Controls.Add(Me.Label10)
        Me.UltraGroupBox2.Controls.Add(Me.Label9)
        Me.UltraGroupBox2.Controls.Add(Me.Label8)
        Me.UltraGroupBox2.Controls.Add(Me.LNombre)
        Me.UltraGroupBox2.Controls.Add(Me.LApellido)
        Me.UltraGroupBox2.Controls.Add(Me.TxCodPos)
        Me.UltraGroupBox2.Controls.Add(Me.Label11)
        Me.UltraGroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraGroupBox2.ForeColor = System.Drawing.Color.Black
        Appearance1.BackColor = System.Drawing.Color.Blue
        Appearance1.FontData.BoldAsString = "True"
        Appearance1.ForeColor = System.Drawing.Color.White
        Me.UltraGroupBox2.HeaderAppearance = Appearance1
        Me.UltraGroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.UltraGroupBox2.Name = "UltraGroupBox2"
        Me.UltraGroupBox2.Size = New System.Drawing.Size(964, 353)
        Me.UltraGroupBox2.TabIndex = 147
        Me.UltraGroupBox2.Text = "Datos del Comitente"
        Me.UltraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'CBTipo
        '
        Me.CBTipo.Enabled = False
        Me.CBTipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBTipo.FormattingEnabled = True
        Me.CBTipo.Items.AddRange(New Object() {"Empresa", "Persona"})
        Me.CBTipo.Location = New System.Drawing.Point(106, 52)
        Me.CBTipo.Name = "CBTipo"
        Me.CBTipo.Size = New System.Drawing.Size(106, 24)
        Me.CBTipo.TabIndex = 150
        '
        'LTipoComitente
        '
        Me.LTipoComitente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LTipoComitente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTipoComitente.Location = New System.Drawing.Point(6, 54)
        Me.LTipoComitente.Name = "LTipoComitente"
        Me.LTipoComitente.Size = New System.Drawing.Size(93, 21)
        Me.LTipoComitente.TabIndex = 149
        Me.LTipoComitente.Text = "Tipo:"
        Me.LTipoComitente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CBtCtaCte
        '
        Me.CBtCtaCte.BackColor = System.Drawing.Color.Transparent
        Me.CBtCtaCte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtCtaCte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtCtaCte.ImageIndex = 0
        Me.CBtCtaCte.Location = New System.Drawing.Point(293, 312)
        Me.CBtCtaCte.Name = "CBtCtaCte"
        Me.CBtCtaCte.Size = New System.Drawing.Size(135, 34)
        Me.CBtCtaCte.TabIndex = 148
        Me.CBtCtaCte.Text = "Cuenta Corriente"
        Me.CBtCtaCte.UseVisualStyleBackColor = False
        '
        'cboTipoIva
        '
        Me.cboTipoIva.Enabled = False
        Me.cboTipoIva.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboTipoIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoIva.FormattingEnabled = True
        Me.cboTipoIva.Items.AddRange(New Object() {"CF", "MO", "NO", "RI", "EX"})
        Me.cboTipoIva.Location = New System.Drawing.Point(317, 53)
        Me.cboTipoIva.Name = "cboTipoIva"
        Me.cboTipoIva.Size = New System.Drawing.Size(53, 24)
        Me.cboTipoIva.TabIndex = 117
        '
        'Label1
        '
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(218, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 21)
        Me.Label1.TabIndex = 116
        Me.Label1.Text = "IVA:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CButtonActualizar
        '
        Me.CButtonActualizar.BackColor = System.Drawing.Color.Transparent
        Me.CButtonActualizar.Enabled = False
        Me.CButtonActualizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButtonActualizar.ImageIndex = 0
        Me.CButtonActualizar.Location = New System.Drawing.Point(149, 312)
        Me.CButtonActualizar.Name = "CButtonActualizar"
        Me.CButtonActualizar.Size = New System.Drawing.Size(135, 34)
        Me.CButtonActualizar.TabIndex = 115
        Me.CButtonActualizar.Text = "Actualizar"
        Me.CButtonActualizar.UseVisualStyleBackColor = False
        '
        'CButton1
        '
        Me.CButton1.BackColor = System.Drawing.Color.Transparent
        Me.CButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CButton1.ImageIndex = 0
        Me.CButton1.Location = New System.Drawing.Point(7, 312)
        Me.CButton1.Name = "CButton1"
        Me.CButton1.Size = New System.Drawing.Size(135, 34)
        Me.CButton1.TabIndex = 114
        Me.CButton1.Text = "Editar"
        Me.CButton1.UseVisualStyleBackColor = False
        '
        'CboTipdoc
        '
        Me.CboTipdoc.Enabled = False
        Me.CboTipdoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboTipdoc.FormattingEnabled = True
        Me.CboTipdoc.Items.AddRange(New Object() {"COM"})
        Me.CboTipdoc.Location = New System.Drawing.Point(371, 26)
        Me.CboTipdoc.Name = "CboTipdoc"
        Me.CboTipdoc.Size = New System.Drawing.Size(57, 24)
        Me.CboTipdoc.TabIndex = 79
        Me.CboTipdoc.Visible = False
        '
        'TxDocumento
        '
        Me.TxDocumento.Enabled = False
        Me.TxDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxDocumento.Location = New System.Drawing.Point(104, 28)
        Me.TxDocumento.Name = "TxDocumento"
        Me.TxDocumento.Size = New System.Drawing.Size(108, 22)
        Me.TxDocumento.TabIndex = 80
        '
        'Label15
        '
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(223, 226)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(93, 21)
        Me.Label15.TabIndex = 82
        Me.Label15.Text = "Telefono 2:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.CausesValidation = False
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 21)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "Codigo:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboProvincia
        '
        Me.cboProvincia.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboProvincia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvincia.FormattingEnabled = True
        Me.cboProvincia.Items.AddRange(New Object() {"Chaco" & Global.Microsoft.VisualBasic.ChrW(9), "Corrientes", "Formosa", "Misiones", "Santa Fe", "Entre Rios", "Santiago del Estero", "Salta", "Jujuy", "Tucuman", "Catamarca", "La Rioja", "San Juan", "Cordoba", "Mendoza", "Buenos Aires", "La Pampa", "Neuquen", "Chubut", "Rio Negro", "Santa Cruz", "Tierra del Fuego"})
        Me.cboProvincia.Location = New System.Drawing.Point(104, 201)
        Me.cboProvincia.Name = "cboProvincia"
        Me.cboProvincia.Size = New System.Drawing.Size(223, 24)
        Me.cboProvincia.TabIndex = 92
        '
        'MtxDGR
        '
        Me.MtxDGR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MtxDGR.Location = New System.Drawing.Point(317, 78)
        Me.MtxDGR.Mask = "00-0000000-0"
        Me.MtxDGR.Name = "MtxDGR"
        Me.MtxDGR.Size = New System.Drawing.Size(92, 22)
        Me.MtxDGR.TabIndex = 83
        '
        'MtxCuit
        '
        Me.MtxCuit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MtxCuit.Location = New System.Drawing.Point(106, 77)
        Me.MtxCuit.Mask = "00-00000000-0"
        Me.MtxCuit.Name = "MtxCuit"
        Me.MtxCuit.Size = New System.Drawing.Size(106, 22)
        Me.MtxCuit.TabIndex = 81
        '
        'MtxCelu
        '
        Me.MtxCelu.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MtxCelu.Location = New System.Drawing.Point(322, 226)
        Me.MtxCelu.Mask = "000-000-0000"
        Me.MtxCelu.Name = "MtxCelu"
        Me.MtxCelu.Size = New System.Drawing.Size(106, 22)
        Me.MtxCelu.TabIndex = 96
        '
        'MtxTelefono
        '
        Me.MtxTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MtxTelefono.Location = New System.Drawing.Point(105, 226)
        Me.MtxTelefono.Mask = "(999)000-0000"
        Me.MtxTelefono.Name = "MtxTelefono"
        Me.MtxTelefono.Size = New System.Drawing.Size(95, 22)
        Me.MtxTelefono.TabIndex = 95
        '
        'TxMail
        '
        Me.TxMail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxMail.Location = New System.Drawing.Point(105, 251)
        Me.TxMail.Name = "TxMail"
        Me.TxMail.Size = New System.Drawing.Size(272, 22)
        Me.TxMail.TabIndex = 97
        '
        'TxLocalidad
        '
        Me.TxLocalidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxLocalidad.Location = New System.Drawing.Point(104, 178)
        Me.TxLocalidad.Name = "TxLocalidad"
        Me.TxLocalidad.Size = New System.Drawing.Size(223, 22)
        Me.TxLocalidad.TabIndex = 90
        '
        'TxDireccion
        '
        Me.TxDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxDireccion.Location = New System.Drawing.Point(105, 153)
        Me.TxDireccion.Name = "TxDireccion"
        Me.TxDireccion.Size = New System.Drawing.Size(323, 22)
        Me.TxDireccion.TabIndex = 89
        '
        'TxNombre
        '
        Me.TxNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxNombre.Location = New System.Drawing.Point(105, 128)
        Me.TxNombre.Name = "TxNombre"
        Me.TxNombre.Size = New System.Drawing.Size(323, 22)
        Me.TxNombre.TabIndex = 85
        '
        'TxApellido
        '
        Me.TxApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxApellido.Location = New System.Drawing.Point(104, 103)
        Me.TxApellido.Name = "TxApellido"
        Me.TxApellido.Size = New System.Drawing.Size(324, 22)
        Me.TxApellido.TabIndex = 84
        '
        'Label24
        '
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(6, 251)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(93, 20)
        Me.Label24.TabIndex = 112
        Me.Label24.Text = "Email:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(218, 78)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(93, 21)
        Me.Label23.TabIndex = 111
        Me.Label23.Text = "DGR:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(6, 78)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(93, 21)
        Me.Label22.TabIndex = 110
        Me.Label22.Text = "CUIT:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(6, 226)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 20)
        Me.Label14.TabIndex = 108
        Me.Label14.Text = "Telefono 1:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 201)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 21)
        Me.Label10.TabIndex = 105
        Me.Label10.Text = "Provincia:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 178)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(93, 21)
        Me.Label9.TabIndex = 104
        Me.Label9.Text = "Localidad:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 153)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(93, 21)
        Me.Label8.TabIndex = 103
        Me.Label8.Text = "Direccion:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LNombre
        '
        Me.LNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNombre.Location = New System.Drawing.Point(6, 128)
        Me.LNombre.Name = "LNombre"
        Me.LNombre.Size = New System.Drawing.Size(93, 21)
        Me.LNombre.TabIndex = 100
        Me.LNombre.Text = "Nombre:"
        Me.LNombre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LApellido
        '
        Me.LApellido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LApellido.Location = New System.Drawing.Point(6, 103)
        Me.LApellido.Name = "LApellido"
        Me.LApellido.Size = New System.Drawing.Size(93, 21)
        Me.LApellido.TabIndex = 99
        Me.LApellido.Text = "Apellido:"
        Me.LApellido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxCodPos
        '
        Me.TxCodPos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxCodPos.Location = New System.Drawing.Point(376, 201)
        Me.TxCodPos.Name = "TxCodPos"
        Me.TxCodPos.Size = New System.Drawing.Size(52, 22)
        Me.TxCodPos.TabIndex = 93
        '
        'Label11
        '
        Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(338, 201)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(32, 23)
        Me.Label11.TabIndex = 91
        Me.Label11.Text = "C.P.:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UGtrabajos
        '
        Appearance2.BackColor = System.Drawing.Color.White
        Me.UGtrabajos.DisplayLayout.Appearance = Appearance2
        Me.UGtrabajos.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGtrabajos.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.UGtrabajos.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Appearance3.BackColor = System.Drawing.Color.Transparent
        Me.UGtrabajos.DisplayLayout.Override.CardAreaAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance4.FontData.BoldAsString = "True"
        Appearance4.FontData.Name = "Arial"
        Appearance4.FontData.SizeInPoints = 10.0!
        Appearance4.ForeColor = System.Drawing.Color.White
        Appearance4.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGtrabajos.DisplayLayout.Override.HeaderAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGtrabajos.DisplayLayout.Override.RowSelectorAppearance = Appearance5
        Appearance6.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance6.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance6.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGtrabajos.DisplayLayout.Override.SelectedRowAppearance = Appearance6
        Me.UGtrabajos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGtrabajos.Location = New System.Drawing.Point(437, 30)
        Me.UGtrabajos.Name = "UGtrabajos"
        Me.UGtrabajos.Size = New System.Drawing.Size(523, 319)
        Me.UGtrabajos.TabIndex = 148
        Me.UGtrabajos.Text = "Trabajos"
        '
        'FrmFichaComitentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(970, 354)
        Me.Controls.Add(Me.UGtrabajos)
        Me.Controls.Add(Me.UltraGroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmFichaComitentes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comitentes"
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox2.ResumeLayout(False)
        Me.UltraGroupBox2.PerformLayout()
        CType(Me.UGtrabajos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents UltraGroupBox2 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents CButtonActualizar As System.Windows.Forms.Button
    Friend WithEvents CButton1 As System.Windows.Forms.Button
    Friend WithEvents CboTipdoc As System.Windows.Forms.ComboBox
    Friend WithEvents TxDocumento As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboProvincia As System.Windows.Forms.ComboBox
    Friend WithEvents MtxDGR As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxCuit As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxCelu As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MtxTelefono As System.Windows.Forms.MaskedTextBox
    Friend WithEvents TxMail As System.Windows.Forms.TextBox
    Friend WithEvents TxLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents TxDireccion As System.Windows.Forms.TextBox
    Friend WithEvents TxNombre As System.Windows.Forms.TextBox
    Friend WithEvents TxApellido As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LNombre As System.Windows.Forms.Label
    Friend WithEvents LApellido As System.Windows.Forms.Label
    Friend WithEvents TxCodPos As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboTipoIva As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UGtrabajos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtCtaCte As System.Windows.Forms.Button
    Friend WithEvents CBTipo As System.Windows.Forms.ComboBox
    Friend WithEvents LTipoComitente As System.Windows.Forms.Label
End Class
