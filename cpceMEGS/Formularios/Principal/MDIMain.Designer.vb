﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MDIMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIMain))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.OpcionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroMayorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroDiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSumaYSaldoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajaGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PadronesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosDeChequeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculoNumeralesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GerencialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeudoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeudoresPrestamosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldosProfesionalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeudoresDEPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MorososCuotasAportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarContraseñaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SipresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1control = New System.Windows.Forms.Label()
        Me.Label2control = New System.Windows.Forms.Label()
        Me.Label3control = New System.Windows.Forms.Label()
        Me.Label4control = New System.Windows.Forms.Label()
        Me.Label5control = New System.Windows.Forms.Label()
        Me.Label6control = New System.Windows.Forms.Label()
        Me.Label7control = New System.Windows.Forms.Label()
        Me.UGBNovedades = New Infragistics.Win.Misc.UltraGroupBox()
        Me.DGVAfiCumpleanios = New System.Windows.Forms.DataGridView()
        Me.PBRegalo = New System.Windows.Forms.PictureBox()
        Me.PBTrabajo = New System.Windows.Forms.PictureBox()
        Me.PBLiquidacion = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.UGBNovedades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBNovedades.SuspendLayout()
        CType(Me.DGVAfiCumpleanios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBRegalo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBLiquidacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpcionesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(726, 24)
        Me.MenuStrip1.TabIndex = 43
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'OpcionesToolStripMenuItem
        '
        Me.OpcionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InformesToolStripMenuItem, Me.CambiarContraseñaToolStripMenuItem, Me.SipresToolStripMenuItem})
        Me.OpcionesToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpcionesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.OpcionesToolStripMenuItem.Name = "OpcionesToolStripMenuItem"
        Me.OpcionesToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.OpcionesToolStripMenuItem.Text = "Opciones"
        '
        'InformesToolStripMenuItem
        '
        Me.InformesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LibroMayorToolStripMenuItem, Me.LibroDiarioToolStripMenuItem, Me.BalanceSumaYSaldoToolStripMenuItem, Me.CajaGeneralToolStripMenuItem, Me.ExportarToolStripMenuItem, Me.PadronesToolStripMenuItem, Me.MovimientosDeChequeToolStripMenuItem, Me.CalculoNumeralesToolStripMenuItem, Me.GerencialesToolStripMenuItem})
        Me.InformesToolStripMenuItem.Name = "InformesToolStripMenuItem"
        Me.InformesToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.InformesToolStripMenuItem.Text = "Informes"
        '
        'LibroMayorToolStripMenuItem
        '
        Me.LibroMayorToolStripMenuItem.Name = "LibroMayorToolStripMenuItem"
        Me.LibroMayorToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.LibroMayorToolStripMenuItem.Text = "Libro Mayor"
        '
        'LibroDiarioToolStripMenuItem
        '
        Me.LibroDiarioToolStripMenuItem.Name = "LibroDiarioToolStripMenuItem"
        Me.LibroDiarioToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.LibroDiarioToolStripMenuItem.Text = "Libro Diario"
        '
        'BalanceSumaYSaldoToolStripMenuItem
        '
        Me.BalanceSumaYSaldoToolStripMenuItem.Name = "BalanceSumaYSaldoToolStripMenuItem"
        Me.BalanceSumaYSaldoToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.BalanceSumaYSaldoToolStripMenuItem.Text = "Balance de Sumas y Saldos"
        '
        'CajaGeneralToolStripMenuItem
        '
        Me.CajaGeneralToolStripMenuItem.Name = "CajaGeneralToolStripMenuItem"
        Me.CajaGeneralToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.CajaGeneralToolStripMenuItem.Text = "Caja General"
        '
        'ExportarToolStripMenuItem
        '
        Me.ExportarToolStripMenuItem.Name = "ExportarToolStripMenuItem"
        Me.ExportarToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.ExportarToolStripMenuItem.Text = "Exportar Retenciones"
        '
        'PadronesToolStripMenuItem
        '
        Me.PadronesToolStripMenuItem.Name = "PadronesToolStripMenuItem"
        Me.PadronesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.PadronesToolStripMenuItem.Text = "Padrones"
        '
        'MovimientosDeChequeToolStripMenuItem
        '
        Me.MovimientosDeChequeToolStripMenuItem.Name = "MovimientosDeChequeToolStripMenuItem"
        Me.MovimientosDeChequeToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.MovimientosDeChequeToolStripMenuItem.Text = "Movimientos de Cheque"
        '
        'CalculoNumeralesToolStripMenuItem
        '
        Me.CalculoNumeralesToolStripMenuItem.Name = "CalculoNumeralesToolStripMenuItem"
        Me.CalculoNumeralesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.CalculoNumeralesToolStripMenuItem.Text = "Acreditación de la Renta"
        '
        'GerencialesToolStripMenuItem
        '
        Me.GerencialesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeudoresToolStripMenuItem, Me.DeudoresPrestamosToolStripMenuItem, Me.SaldosProfesionalesToolStripMenuItem, Me.DeudoresDEPToolStripMenuItem, Me.MorososCuotasAportesToolStripMenuItem})
        Me.GerencialesToolStripMenuItem.Name = "GerencialesToolStripMenuItem"
        Me.GerencialesToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.GerencialesToolStripMenuItem.Text = "Gerenciales"
        '
        'DeudoresToolStripMenuItem
        '
        Me.DeudoresToolStripMenuItem.Name = "DeudoresToolStripMenuItem"
        Me.DeudoresToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.DeudoresToolStripMenuItem.Text = "Deudores"
        '
        'DeudoresPrestamosToolStripMenuItem
        '
        Me.DeudoresPrestamosToolStripMenuItem.Name = "DeudoresPrestamosToolStripMenuItem"
        Me.DeudoresPrestamosToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.DeudoresPrestamosToolStripMenuItem.Text = "Deudores Prestamos"
        '
        'SaldosProfesionalesToolStripMenuItem
        '
        Me.SaldosProfesionalesToolStripMenuItem.Name = "SaldosProfesionalesToolStripMenuItem"
        Me.SaldosProfesionalesToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.SaldosProfesionalesToolStripMenuItem.Text = "Conciliacion Deudores Aportes"
        '
        'DeudoresDEPToolStripMenuItem
        '
        Me.DeudoresDEPToolStripMenuItem.Name = "DeudoresDEPToolStripMenuItem"
        Me.DeudoresDEPToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.DeudoresDEPToolStripMenuItem.Text = "Cuenta Corriente - Seguro"
        '
        'MorososCuotasAportesToolStripMenuItem
        '
        Me.MorososCuotasAportesToolStripMenuItem.Name = "MorososCuotasAportesToolStripMenuItem"
        Me.MorososCuotasAportesToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.MorososCuotasAportesToolStripMenuItem.Text = "Morosos Cuotas Aportes"
        '
        'CambiarContraseñaToolStripMenuItem
        '
        Me.CambiarContraseñaToolStripMenuItem.Name = "CambiarContraseñaToolStripMenuItem"
        Me.CambiarContraseñaToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.CambiarContraseñaToolStripMenuItem.Text = "Cambiar Contraseña"
        '
        'SipresToolStripMenuItem
        '
        Me.SipresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebitosToolStripMenuItem})
        Me.SipresToolStripMenuItem.Name = "SipresToolStripMenuItem"
        Me.SipresToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.SipresToolStripMenuItem.Text = "Sipres"
        '
        'DebitosToolStripMenuItem
        '
        Me.DebitosToolStripMenuItem.Name = "DebitosToolStripMenuItem"
        Me.DebitosToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.DebitosToolStripMenuItem.Text = "Debitos"
        '
        'Label1control
        '
        Me.Label1control.AutoSize = True
        Me.Label1control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1control.Location = New System.Drawing.Point(656, 352)
        Me.Label1control.Name = "Label1control"
        Me.Label1control.Size = New System.Drawing.Size(59, 13)
        Me.Label1control.TabIndex = 45
        Me.Label1control.Text = "Control + P"
        '
        'Label2control
        '
        Me.Label2control.AutoSize = True
        Me.Label2control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2control.Location = New System.Drawing.Point(656, 180)
        Me.Label2control.Name = "Label2control"
        Me.Label2control.Size = New System.Drawing.Size(59, 13)
        Me.Label2control.TabIndex = 46
        Me.Label2control.Text = "Control + C"
        '
        'Label3control
        '
        Me.Label3control.AutoSize = True
        Me.Label3control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3control.Location = New System.Drawing.Point(571, 95)
        Me.Label3control.Name = "Label3control"
        Me.Label3control.Size = New System.Drawing.Size(59, 13)
        Me.Label3control.TabIndex = 47
        Me.Label3control.Text = "Control + K"
        '
        'Label4control
        '
        Me.Label4control.AutoSize = True
        Me.Label4control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4control.Location = New System.Drawing.Point(656, 94)
        Me.Label4control.Name = "Label4control"
        Me.Label4control.Size = New System.Drawing.Size(59, 13)
        Me.Label4control.TabIndex = 48
        Me.Label4control.Text = "Control + A"
        '
        'Label5control
        '
        Me.Label5control.AutoSize = True
        Me.Label5control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5control.Location = New System.Drawing.Point(655, 266)
        Me.Label5control.Name = "Label5control"
        Me.Label5control.Size = New System.Drawing.Size(60, 13)
        Me.Label5control.TabIndex = 49
        Me.Label5control.Text = "Control + O"
        '
        'Label6control
        '
        Me.Label6control.AutoSize = True
        Me.Label6control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6control.Location = New System.Drawing.Point(571, 267)
        Me.Label6control.Name = "Label6control"
        Me.Label6control.Size = New System.Drawing.Size(61, 13)
        Me.Label6control.TabIndex = 50
        Me.Label6control.Text = "Control + M"
        '
        'Label7control
        '
        Me.Label7control.AutoSize = True
        Me.Label7control.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7control.Location = New System.Drawing.Point(656, 438)
        Me.Label7control.Name = "Label7control"
        Me.Label7control.Size = New System.Drawing.Size(60, 13)
        Me.Label7control.TabIndex = 52
        Me.Label7control.Text = "Control + D"
        Me.Label7control.Visible = False
        '
        'UGBNovedades
        '
        Me.UGBNovedades.Controls.Add(Me.DGVAfiCumpleanios)
        Me.UGBNovedades.Location = New System.Drawing.Point(7, 26)
        Me.UGBNovedades.Name = "UGBNovedades"
        Me.UGBNovedades.Size = New System.Drawing.Size(549, 424)
        Me.UGBNovedades.TabIndex = 55
        Me.UGBNovedades.Text = "Novedades"
        Me.UGBNovedades.Visible = False
        '
        'DGVAfiCumpleanios
        '
        Me.DGVAfiCumpleanios.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVAfiCumpleanios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVAfiCumpleanios.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DGVAfiCumpleanios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVAfiCumpleanios.Location = New System.Drawing.Point(3, 19)
        Me.DGVAfiCumpleanios.Name = "DGVAfiCumpleanios"
        Me.DGVAfiCumpleanios.Size = New System.Drawing.Size(537, 397)
        Me.DGVAfiCumpleanios.TabIndex = 0
        '
        'PBRegalo
        '
        Me.PBRegalo.BackColor = System.Drawing.Color.Transparent
        Me.PBRegalo.Image = Global.cpceMEGS.My.Resources.Resources.icono_regalo
        Me.PBRegalo.Location = New System.Drawing.Point(562, 370)
        Me.PBRegalo.Name = "PBRegalo"
        Me.PBRegalo.Size = New System.Drawing.Size(79, 80)
        Me.PBRegalo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PBRegalo.TabIndex = 56
        Me.PBRegalo.TabStop = False
        '
        'PBTrabajo
        '
        Me.PBTrabajo.BackColor = System.Drawing.Color.Transparent
        Me.PBTrabajo.Image = Global.cpceMEGS.My.Resources.Resources.icon_trabajo
        Me.PBTrabajo.Location = New System.Drawing.Point(562, 285)
        Me.PBTrabajo.Name = "PBTrabajo"
        Me.PBTrabajo.Size = New System.Drawing.Size(79, 80)
        Me.PBTrabajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PBTrabajo.TabIndex = 54
        Me.PBTrabajo.TabStop = False
        '
        'PBLiquidacion
        '
        Me.PBLiquidacion.BackColor = System.Drawing.Color.Transparent
        Me.PBLiquidacion.Image = CType(resources.GetObject("PBLiquidacion.Image"), System.Drawing.Image)
        Me.PBLiquidacion.Location = New System.Drawing.Point(562, 114)
        Me.PBLiquidacion.Name = "PBLiquidacion"
        Me.PBLiquidacion.Size = New System.Drawing.Size(79, 80)
        Me.PBLiquidacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PBLiquidacion.TabIndex = 53
        Me.PBLiquidacion.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(645, 370)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox7.TabIndex = 51
        Me.PictureBox7.TabStop = False
        Me.PictureBox7.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(645, 199)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 44
        Me.PictureBox5.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.cpceMEGS.My.Resources.Resources.icon_herramienta
        Me.PictureBox3.Location = New System.Drawing.Point(562, 199)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 42
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(645, 27)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 40
        Me.PictureBox4.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(645, 114)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 39
        Me.PictureBox2.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(562, 27)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 41
        Me.PictureBox6.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(645, 285)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(79, 80)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 38
        Me.PictureBox1.TabStop = False
        '
        'MDIMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(726, 454)
        Me.Controls.Add(Me.PBRegalo)
        Me.Controls.Add(Me.UGBNovedades)
        Me.Controls.Add(Me.PBTrabajo)
        Me.Controls.Add(Me.PBLiquidacion)
        Me.Controls.Add(Me.Label7control)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.Label6control)
        Me.Controls.Add(Me.Label5control)
        Me.Controls.Add(Me.Label4control)
        Me.Controls.Add(Me.Label3control)
        Me.Controls.Add(Me.Label2control)
        Me.Controls.Add(Me.Label1control)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MDIMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CPCE v1.4.5"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.UGBNovedades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBNovedades.ResumeLayout(False)
        CType(Me.DGVAfiCumpleanios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBRegalo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBLiquidacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents OpcionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroMayorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceSumaYSaldoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajaGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PadronesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosDeChequeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculoNumeralesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroDiarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GerencialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeudoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaldosProfesionalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeudoresDEPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarContraseñaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MorososCuotasAportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1control As System.Windows.Forms.Label
    Friend WithEvents Label2control As System.Windows.Forms.Label
    Friend WithEvents Label3control As System.Windows.Forms.Label
    Friend WithEvents Label4control As System.Windows.Forms.Label
    Friend WithEvents Label5control As System.Windows.Forms.Label
    Friend WithEvents Label6control As System.Windows.Forms.Label
    Friend WithEvents DeudoresPrestamosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label7control As System.Windows.Forms.Label
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PBLiquidacion As System.Windows.Forms.PictureBox
    Friend WithEvents PBTrabajo As PictureBox
    Friend WithEvents UGBNovedades As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents DGVAfiCumpleanios As DataGridView
    Friend WithEvents PBRegalo As PictureBox
    Friend WithEvents SipresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DebitosToolStripMenuItem As ToolStripMenuItem
End Class
