﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambiarPassword
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCambiarPassword))
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.LabelContraAnterior = New System.Windows.Forms.Label()
        Me.LabelContraNueva = New System.Windows.Forms.Label()
        Me.LabelContraRepetir = New System.Windows.Forms.Label()
        Me.TextContraAnterior = New System.Windows.Forms.TextBox()
        Me.TextContraNueva = New System.Windows.Forms.TextBox()
        Me.TextContraRepetir = New System.Windows.Forms.TextBox()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.ButtonCancelar = New System.Windows.Forms.Button()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(179, 176)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.LogoPictureBox.TabIndex = 1
        Me.LogoPictureBox.TabStop = False
        '
        'LabelContraAnterior
        '
        Me.LabelContraAnterior.AutoSize = True
        Me.LabelContraAnterior.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelContraAnterior.Location = New System.Drawing.Point(185, 6)
        Me.LabelContraAnterior.Name = "LabelContraAnterior"
        Me.LabelContraAnterior.Size = New System.Drawing.Size(121, 13)
        Me.LabelContraAnterior.TabIndex = 0
        Me.LabelContraAnterior.Text = "Contraseña anterior"
        '
        'LabelContraNueva
        '
        Me.LabelContraNueva.AutoSize = True
        Me.LabelContraNueva.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelContraNueva.Location = New System.Drawing.Point(185, 50)
        Me.LabelContraNueva.Name = "LabelContraNueva"
        Me.LabelContraNueva.Size = New System.Drawing.Size(109, 13)
        Me.LabelContraNueva.TabIndex = 0
        Me.LabelContraNueva.Text = "Nueva contraseña"
        '
        'LabelContraRepetir
        '
        Me.LabelContraRepetir.AutoSize = True
        Me.LabelContraRepetir.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelContraRepetir.Location = New System.Drawing.Point(185, 95)
        Me.LabelContraRepetir.Name = "LabelContraRepetir"
        Me.LabelContraRepetir.Size = New System.Drawing.Size(116, 13)
        Me.LabelContraRepetir.TabIndex = 0
        Me.LabelContraRepetir.Text = "Repetir contraseña"
        '
        'TextContraAnterior
        '
        Me.TextContraAnterior.Location = New System.Drawing.Point(185, 22)
        Me.TextContraAnterior.Name = "TextContraAnterior"
        Me.TextContraAnterior.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextContraAnterior.Size = New System.Drawing.Size(208, 20)
        Me.TextContraAnterior.TabIndex = 0
        '
        'TextContraNueva
        '
        Me.TextContraNueva.Location = New System.Drawing.Point(185, 66)
        Me.TextContraNueva.Name = "TextContraNueva"
        Me.TextContraNueva.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextContraNueva.Size = New System.Drawing.Size(208, 20)
        Me.TextContraNueva.TabIndex = 1
        '
        'TextContraRepetir
        '
        Me.TextContraRepetir.Location = New System.Drawing.Point(185, 111)
        Me.TextContraRepetir.Name = "TextContraRepetir"
        Me.TextContraRepetir.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextContraRepetir.Size = New System.Drawing.Size(208, 20)
        Me.TextContraRepetir.TabIndex = 2
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonAceptar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.ButtonAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButtonAceptar.Location = New System.Drawing.Point(185, 137)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(100, 35)
        Me.ButtonAceptar.TabIndex = 3
        Me.ButtonAceptar.Text = "&Aceptar"
        Me.ButtonAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ButtonCancelar
        '
        Me.ButtonCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonCancelar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Blocked_32xMD_color
        Me.ButtonCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButtonCancelar.Location = New System.Drawing.Point(294, 137)
        Me.ButtonCancelar.Name = "ButtonCancelar"
        Me.ButtonCancelar.Size = New System.Drawing.Size(100, 35)
        Me.ButtonCancelar.TabIndex = 4
        Me.ButtonCancelar.Text = "&Cancelar"
        Me.ButtonCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FrmCambiarPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(401, 177)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.ButtonCancelar)
        Me.Controls.Add(Me.TextContraRepetir)
        Me.Controls.Add(Me.TextContraNueva)
        Me.Controls.Add(Me.TextContraAnterior)
        Me.Controls.Add(Me.LabelContraRepetir)
        Me.Controls.Add(Me.LabelContraNueva)
        Me.Controls.Add(Me.LabelContraAnterior)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCambiarPassword"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CAMBIAR CONTRASEÑA"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents LabelContraAnterior As System.Windows.Forms.Label
    Friend WithEvents LabelContraNueva As System.Windows.Forms.Label
    Friend WithEvents LabelContraRepetir As System.Windows.Forms.Label
    Friend WithEvents TextContraAnterior As System.Windows.Forms.TextBox
    Friend WithEvents TextContraNueva As System.Windows.Forms.TextBox
    Friend WithEvents TextContraRepetir As System.Windows.Forms.TextBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents ButtonCancelar As System.Windows.Forms.Button
End Class
