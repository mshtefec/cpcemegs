﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmReportes
    Private m_Reporte As String
    Private m_SubReporte As String
    Private m_DTreport As DataTable
    Public m_DTSubreport As DataSet
    Public myReporte As New ReportDocument

    Public Sub New(ByVal DTReport As DataTable, Optional ByVal DSReport As DataSet = Nothing, Optional ByVal cReporte As String = "", Optional ByVal cTituloFrm As String = "")
        InitializeComponent()
        m_Reporte = cReporte
        m_DTreport = DTReport
        m_DTSubreport = DSReport
        Text = cTituloFrm
    End Sub

    Private Sub frmReportes_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        cargaReporte()
    End Sub

    Public Sub cargaReporte()
        Dim cArchivo As String = Application.StartupPath & "\Reportes\" & m_Reporte
        'If FileSystem.Dir("w:\CpceMEGS\reportes\" & m_Reporte & "") <> "" Then
        '    cArchivo = "w:\CpceMEGS\reportes\" & m_Reporte
        'Else
        'cArchivo = Application.StartupPath & "\Reportes\" & m_Reporte
        'End If
        Try
            myReporte.Load(cArchivo)
            If Not IsNothing(m_DTSubreport) Then
                myReporte.SetDataSource(m_DTSubreport)
            Else
                myReporte.SetDataSource(m_DTreport)
            End If

            CrystalReportViewer1.ReportSource = myReporte
        Catch ex As Exception
            MessageBox.Show("Se Produjo un error - " & ex.Message.ToString & " " & cArchivo, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class