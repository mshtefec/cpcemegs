﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class ImpresionDirecta
    Private m_Reporte As String
    Private m_SubReporte As String
    Private m_DTreport As DataTable
    Public m_DTSubreport As DataSet
    Private print_name As String
    Private print_paper As String

    Public Sub New(ByVal DTReport As DataTable, Optional ByVal DSReport As DataSet = Nothing, Optional ByVal cReporte As String = "", Optional ByVal cNombreImpresora As String = "", Optional ByVal cNombrePagina As String = "")
        m_Reporte = cReporte
        m_DTreport = DTReport
        m_DTSubreport = DSReport
        'Nombre de impresora a setear
        print_name = cNombreImpresora
        'Nombre de papel a setear
        print_paper = cNombrePagina
    End Sub

    Public Function Imprimir() As Boolean
        Dim myReporte As New ReportDocument
        'Guardo los valores de la primer fila de la tabla
        Dim values As Object()
        'Genero string para el mensaje con los values
        Dim message As String
        Dim cArchivo As String = Application.StartupPath & "\Reportes\" & m_Reporte
        If FileSystem.Dir("w:\CpceMEGS\reportes\" & m_Reporte & "") <> "" Then
            cArchivo = "w:\CpceMEGS\reportes\" & m_Reporte
        Else
            cArchivo = Application.StartupPath & "\Reportes\" & m_Reporte
        End If
        Try
            myReporte.Load(cArchivo)
            myReporte = ConfigurarHojaCrystalReport(myReporte)
            If Not IsNothing(m_DTSubreport) Then
                values = m_DTSubreport.Tables(0).Rows(0).ItemArray()
                myReporte.SetDataSource(m_DTSubreport)
            Else
                values = m_DTreport.Rows(0).ItemArray()
                myReporte.SetDataSource(m_DTreport)
            End If

            Select Case m_Reporte
                Case "CRObleas.rpt"
                    message = "OBLEAS" & vbCrLf &
                        "Nro. de Legalización: " & values(2) & vbCrLf &
                        "Profesional: " & values(1) & vbCrLf &
                        "Comitente: " & values(3) & vbCrLf &
                        "Fecha: " & values(5)
                Case "CRCheques.rpt"
                    message = "CHEQUES" & vbCrLf &
                        "Descripción de cuenta: " & values(7) & vbCrLf &
                        "Fecha: " & values(4) & "-" & values(5) & "-" & values(6)
                Case Else
                    message = "Imprimir?"
            End Select

            myReporte.Refresh()
            'Confirmar la impresion
            If MessageBox.Show(message, "Confirmar impresión directa:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                'Yes continua
                'Metodo para impresion directa
                myReporte.PrintToPrinter(1, False, 0, 0)
                Imprimir = True
            Else
                Imprimir = False
            End If
        Catch ex As Exception
            MessageBox.Show("Se Produjo un error - " & ex.Message.ToString & " " & cArchivo, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Imprimir = False
        End Try
    End Function
    'Setea impresora y papel
    Private Function ConfigurarHojaCrystalReport(ByVal myReporte As ReportDocument) As ReportDocument
        'If nombre de impresora
        If print_name <> "" Then
            'If nombre de hoja personalizada
            If print_paper <> "" Then
                myReporte.PrintOptions.PrinterName = print_name
                myReporte.PrintOptions.PaperSize = GetPapersizeID(print_name, print_paper)
            End If
        End If
        Return myReporte
    End Function
    'Metodo para obtener el id del papel por su nombre
    Public Function GetPapersizeID(ByVal PrinterName As String, ByVal PaperSizeName As String) As Integer
        Dim pdprint As New System.Drawing.Printing.PrintDocument()
        Dim PaperSizeID As Integer = 0
        Dim ppname As String = ""
        pdprint.PrinterSettings.PrinterName = PrinterName
        For i As Integer = 0 To pdprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            ppname = PaperSizeName
            If pdprint.PrinterSettings.PaperSizes(i).PaperName = ppname Then
                rawKind = CInt(pdprint.PrinterSettings.PaperSizes(i).RawKind)
                PaperSizeID = rawKind
                Exit For
            End If
        Next
        Return PaperSizeID
    End Function
    'Metodo para saber que impresora esta por default
    Private Function DefaultPrinterName() As String
        Dim oPS As New System.Drawing.Printing.PrinterSettings
        Try
            DefaultPrinterName = oPS.PrinterName
        Catch ex As System.Exception
            DefaultPrinterName = ""
        Finally
            oPS = Nothing
        End Try
    End Function
End Class
