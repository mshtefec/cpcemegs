﻿Imports MySql.Data.MySqlClient

Public Class MDIMain
    Shadows menu As ToolStripMenuItem
    Dim ctlMDI As MdiClient
    Dim enlargenum As Integer = 5
    Dim nNroInstVentas As Integer
    Dim cnn As ConsultaBD

    Private consultaBD As ConsultaBD

    Private da_afiliados_cumpleanios As MySqlDataAdapter
    Private dt_afiliados_cumpleanios As DataTable = New DataTable()

#Region "Degradado de fondo para un formulario MDI"

    ' <summary>
    '  Contendrá el control que representa al área cliente MDI
    ' </summary>
    ' <remarks>Se emplea por el evento Form Resize
    '  Si se declara [WithEvents] hay que escirbir 
    '  el evento [_ctlMdiClient.Paint]
    '  si no hay que asignar el manejador 
    '  [AddHandler _ctlMdiClient.Paint, AddressOf PintarFondo]
    '</remarks>
    Private WithEvents _ctlMdiClient As MdiClient = Nothing

    ' <summary>
    '   Esta función tiene que ser llamada 
    '   por el evento FormLoad del Form
    ' </summary>
    ' <remarks></remarks>
    Private Sub EstablecerFondoDegradadoParaFormLoad()

        '-------------------------------------------------
        ' ATENCION
        ' Para que todo esto funcione, esta función tiene que 
        ' ser llamada desde el evento Load del formulario
        '-------------------------------------------------

        '-------------------------------------------------
        ' Estamos buscando en control que representa
        ' el área cliente MDI 
        '  La función [GetMdiContainer] comprobará si existe 
        '  un control MdiClient en el formulario indicado, 
        '  devolviendo la referencia al citado control.
        '  o el valor Nothing si no existe
        _ctlMdiClient = GetMdiContainer(Me)

    End Sub

    ' <summary>
    '   Esta función tiene que ser llamada 
    '   por el evento FormLoad del Form
    ' </summary>
    Private Sub EstablecerImagenFondoParaFormLoad()
        Try
            ' Asignamos una imagen al fondo del formulario.
            BackgroundImage =
            Image.FromFile("C:\CpceMEGS\Imagenes\favicon.ico")

            ' Ajustamos la imagen dentro del rectángulo
            ' cliente del control.
            ' Por ejemplo.: 
            ' Si la imagen es muy 'grande' y ponemos *center* 
            ' la imagen no se ve entera, solo se ve la parte 
            ' superior izquierda que *entra* en el formulario
            BackgroundImageLayout = ImageLayout.Stretch

        Finally
            ' No hago nada
            ' Si no se carga la imagen se muestra 
            ' el color de fondo por defecto
        End Try
    End Sub

    ' <summary>
    '  La función comprobará si existe un control MdiClient
    '  en el formulario indicado, devolviendo la referencia
    '  al citado control. o el valor Nothing si no existe
    ' </summary>
    ' <param name="frm">El formulario en el que se busca</param>
    ' <returns> 
    '    La referncia al objeto 
    '    <see cref="MdiClient">[MdiClient]</see> si existe 
    '    o un valor Nothing si no existe en el formulario 
    ' </returns>
    Private Shared Function GetMdiContainer(
                     ByVal frm As Form) _
                 As MdiClient

        '--------------------------------------------------------
        ' La función comprobará si existe un control MdiClient
        ' en el formulario indicado, devolviendo la referencia
        ' al citado control. O el valor Nothing si no existe
        '--------------------------------------------------------
        ' Estamos buscando el control que representa 
        ' el área cliente MDI 
        Dim resultado As MdiClient = Nothing
        Dim ctl As Control
        For Each ctl In frm.Controls
            If ctl.GetType.Name = GetType(MdiClient).Name Then
                ' ¿Encontrado!
                resultado = CType(ctl, MdiClient)
                ' no hace falta seguir buscando mas
                Exit For
            End If
        Next
        '
        Return resultado
    End Function

    ' <summary>
    ' Función que pinta el fondo del área cliente
    ' </summary>
    Private Shared Sub PintarFondo(
                ByVal sender As Object,
                ByVal e As System.Windows.Forms.PaintEventArgs)

        Try
            ' Colores para el degradado
            Dim colorSuperior As Color = Color.Blue 'Color.Blue
            Dim colorInferior As Color = Color.OldLace ' Color.Black
            ' el control MdiClient
            Dim panelMdiForm As MdiClient = CType(sender, MdiClient)
            ' el degradado
            Dim GradientePanel As New Drawing2D.LinearGradientBrush(
               New RectangleF(0, 0, panelMdiForm.Width, panelMdiForm.Height),
                   colorSuperior,
                   colorInferior,
                   Drawing2D.LinearGradientMode.Vertical)
            ' dibujarlo
            e.Graphics.FillRectangle(
                GradientePanel,
                New RectangleF(0, 0, panelMdiForm.Width, panelMdiForm.Height))

        Catch ex As Exception
            '--------------------------------------------------
            ' Ignoro la regla FxCop
            ' CA1031: No capturar los tipos de excepción general
            ' http://msdn.microsoft.com/es-es/library/ms182137.aspx
            ' Info: You should not catch Exception or SystemException. 
            '       Catching generic exception types can hide run-time 
            '       problems from the library user, and can complicate 
            '       debugging. You should catch only those exceptions 
            '       that you can handle gracefully.
            '--------------------------------------------------
            ' Registro el problema en el log pero 
            ' dejo que continue el proceso
            ' no se cambiara el color de fondo del formulario
            My.Application.Log.WriteEntry(
             "Problemas en la funcion *PintarFondo* " & ex.Message,
              System.Diagnostics.TraceEventType.Error)
        End Try
    End Sub

    ' <summary>
    '  Capturar el evento Paint del control [MdiClient].
    '  Evento Paint de la variable de clase que tiene la instancia
    '  del control MdiClient y que está definida como [WithEvents]
    ' </summary>
    Private Sub _ctlMdiClient_Paint(
            ByVal sender As Object,
            ByVal e As System.Windows.Forms.PaintEventArgs) _
        Handles _ctlMdiClient.Paint
        '---------
        ' Llamar a la función que hace el 
        ' trabajo de  dibujar el degradado
        Call PintarFondo(sender, e)
    End Sub

    ' <summary>
    '  Capturar el evento Resize del control [MdiClient].
    '  Evento Paint de la variable de clase que tiene la instancia
    '  del control MdiClient y que esta definida como [WithEvents]
    ' </summary>
    ' <remarks>  
    '  <para>
    '    Resuelve el problema de redibujar la imagen 
    '    cuando se cambia el tamaño del formulario
    ' </para> 
    '  <para>
    '    Cuando se cambia el tamaño del formulario, la imagen 
    '    tiene problemas para dibujarse y se queda como a "capas".
    '    Con este evento forzamos a que se dibuje totalmente 
    '    y se resuelve el problema
    ' </para> 
    ' </remarks>
    Private Sub _ctlMdiClient_Resize(
                ByVal sender As Object, ByVal e As EventArgs) _
            Handles _ctlMdiClient.Resize

        ' Llamar a la función que hace el 
        ' trabajo de  dibujar el degradado
        Call PintarFondo(sender, New PaintEventArgs(
                    _ctlMdiClient.CreateGraphics,
                    New Rectangle(_ctlMdiClient.Location, _ctlMdiClient.Size)))
    End Sub

    ' ' <summary>
    ' '   Capturar el evento Resize del formulario.
    ' ' </summary>
    ' ' <remarks>  
    ' '  <para>
    ' '    Resuelve el problema de redibujar la imagen 
    ' '    cuando se cambia el tamaño del formulario
    ' ' </para> 
    ' '  <para>
    ' '    Cuando se cambia el tamaño del formulario, la imagen 
    ' '    tiene problemas para dibujarse y se queda como a "capas".
    ' '    Con este evento forzamos a que se dibuje totalmente 
    ' '    y se resuelve el problema
    ' ' </para> 
    ' ' </remarks>
    'Private Sub Evento_Resize( _
    '            ByVal sender As Object, _
    '            ByVal e As System.EventArgs) _
    '        Handles MyBase.Resize
    '
    '    '--------------------------------------------------------
    '    ' Esta pregunta evita errores en la función [PintarFondo]
    '    ' cuando _ctlMdiClient Is Nothing, hecho que ocurre
    '    ' cuando no se llama la función[TrabajoParaFormLoad] 
    '    ' desde el evento "Load del formulario
    '    '--------------------------------------------------------
    '
    '    If Not (Me._ctlMdiClient Is Nothing) Then
    '        ' llamar a la función que pinta el 
    '        ' degradado del fondo del form
    '        '------------------
    '        ' Observa la *trampa* al llamar
    '        ' a la función [PintarFondo]  
    '        ' El objeto [Sender] no es el formulario sino que se  
    '        ' cambia al control [MdiClient].  
    '        ' De la misma forma el evento [EventArgs] se cambia  
    '        ' al evento [PaintEventArgs] y se vuelve a usar para  
    '        ' ello al control [MdiClient] en lugar del formulario.
    '        '------------------
    '        Call PintarFondo( _
    '            Me._ctlMdiClient, _
    '            New PaintEventArgs( _
    '                Me._ctlMdiClient.CreateGraphics, _
    '                New Rectangle(Me._ctlMdiClient.Location, _
    '                              Me._ctlMdiClient.Size)))
    '    End If
    'End Sub

#End Region

    Private Sub PictureBox1_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox1.MouseEnter
        PictureBox1.BringToFront()
        PictureBox1.BackColor = Color.Blue
        PictureBox1.Width = (PictureBox1.Width + enlargenum)
        PictureBox1.Height = (PictureBox1.Height + enlargenum)
        PictureBox1.Location = New Point(PictureBox1.Location.X - enlargenum / 2, PictureBox1.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PictureBox1_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox1.MouseLeave
        PictureBox1.BackColor = Color.GhostWhite
        PictureBox1.Width = (PictureBox1.Width - enlargenum)
        PictureBox1.Height = (PictureBox1.Height - enlargenum)
        PictureBox1.Location = New Point(PictureBox1.Location.X + enlargenum / 2, PictureBox1.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub PictureBox2_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox2.MouseEnter
        '  Label3.Visible = True
        PictureBox2.BringToFront()
        PictureBox2.BackColor = Color.Blue
        PictureBox2.Width = (PictureBox2.Width + enlargenum)
        PictureBox2.Height = (PictureBox2.Height + enlargenum)
        PictureBox2.Location = New Point(PictureBox2.Location.X - enlargenum / 2, PictureBox2.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PictureBox2_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox2.MouseLeave
        '  Label3.Visible = False
        PictureBox2.BackColor = Color.GhostWhite
        PictureBox2.Width = (PictureBox2.Width - enlargenum)
        PictureBox2.Height = (PictureBox2.Height - enlargenum)
        PictureBox2.Location = New Point(PictureBox2.Location.X + enlargenum / 2, PictureBox2.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub PictureBox3_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox3.MouseEnter
        PictureBox3.BringToFront()
        PictureBox3.BackColor = Color.Blue
        PictureBox3.Width = (PictureBox3.Width + enlargenum)
        PictureBox3.Height = (PictureBox3.Height + enlargenum)
        PictureBox3.Location = New Point(PictureBox3.Location.X - enlargenum / 2, PictureBox3.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PictureBox3_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox3.MouseLeave
        '  Label7.Visible = False
        PictureBox3.BackColor = Color.GhostWhite
        PictureBox3.Width = (PictureBox3.Width - enlargenum)
        PictureBox3.Height = (PictureBox3.Height - enlargenum)
        PictureBox3.Location = New Point(PictureBox3.Location.X + enlargenum / 2, PictureBox3.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub PictureBox4_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox4.MouseEnter
        PictureBox4.BringToFront()
        PictureBox4.BackColor = Color.Blue
        PictureBox4.Width = (PictureBox4.Width + enlargenum)
        PictureBox4.Height = (PictureBox4.Height + enlargenum)
        PictureBox4.Location = New Point(PictureBox4.Location.X - enlargenum / 2, PictureBox4.Location.Y - enlargenum / 2)
    End Sub
    Private Sub PictureBox4_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox4.MouseLeave
        PictureBox4.BackColor = Color.GhostWhite
        PictureBox4.Width = (PictureBox4.Width - enlargenum)
        PictureBox4.Height = (PictureBox4.Height - enlargenum)
        PictureBox4.Location = New Point(PictureBox4.Location.X + enlargenum / 2, PictureBox4.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub PictureBox5_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox5.MouseEnter
        PictureBox5.BringToFront()
        PictureBox5.BackColor = Color.Blue
        PictureBox5.Width = (PictureBox5.Width + enlargenum)
        PictureBox5.Height = (PictureBox5.Height + enlargenum)
        PictureBox5.Location = New Point(PictureBox5.Location.X - enlargenum / 2, PictureBox5.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PictureBox5_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox5.MouseLeave
        '  Label4.Visible = False
        PictureBox5.BackColor = Color.GhostWhite
        PictureBox5.Width = (PictureBox5.Width - enlargenum)
        PictureBox5.Height = (PictureBox5.Height - enlargenum)
        PictureBox5.Location = New Point(PictureBox5.Location.X + enlargenum / 2, PictureBox5.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub PictureBox6_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.BringToFront()
        PictureBox6.BackColor = Color.Blue
        PictureBox6.Width = (PictureBox6.Width + enlargenum)
        PictureBox6.Height = (PictureBox6.Height + enlargenum)
        PictureBox6.Location = New Point(PictureBox6.Location.X - enlargenum / 2, PictureBox6.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PictureBox6_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox6.MouseLeave
        '  Label4.Visible = False
        PictureBox6.BackColor = Color.GhostWhite
        PictureBox6.Width = (PictureBox6.Width - enlargenum)
        PictureBox6.Height = (PictureBox6.Height - enlargenum)
        PictureBox6.Location = New Point(PictureBox6.Location.X + enlargenum / 2, PictureBox6.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub
    Private Sub PBLiquidacion_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PBLiquidacion.MouseEnter
        PBLiquidacion.BringToFront()
        PBLiquidacion.BackColor = Color.Blue
        PBLiquidacion.Width = (PBLiquidacion.Width + enlargenum)
        PBLiquidacion.Height = (PBLiquidacion.Height + enlargenum)
        PBLiquidacion.Location = New Point(PBLiquidacion.Location.X - enlargenum / 2, PBLiquidacion.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PBLiquidacion_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PBLiquidacion.MouseLeave
        PBLiquidacion.BackColor = Color.GhostWhite
        PBLiquidacion.Width = (PBLiquidacion.Width - enlargenum)
        PBLiquidacion.Height = (PBLiquidacion.Height - enlargenum)
        PBLiquidacion.Location = New Point(PBLiquidacion.Location.X + enlargenum / 2, PBLiquidacion.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub
    Private Sub PBTrabajo_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PBTrabajo.MouseEnter
        PBTrabajo.BringToFront()
        PBTrabajo.BackColor = Color.Blue
        PBTrabajo.Width = (PBTrabajo.Width + enlargenum)
        PBTrabajo.Height = (PBTrabajo.Height + enlargenum)
        PBTrabajo.Location = New Point(PBTrabajo.Location.X - enlargenum / 2, PBTrabajo.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PBTrabajo_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PBTrabajo.MouseLeave
        '  Label4.Visible = False
        PBTrabajo.BackColor = Color.GhostWhite
        PBTrabajo.Width = (PBTrabajo.Width - enlargenum)
        PBTrabajo.Height = (PBTrabajo.Height - enlargenum)
        PBTrabajo.Location = New Point(PBTrabajo.Location.X + enlargenum / 2, PBTrabajo.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub
    Private Sub PBRegalo_MouseEnter(ByVal sender As Object, ByVal e As EventArgs) Handles PBRegalo.MouseEnter
        PBRegalo.BringToFront()
        PBRegalo.BackColor = Color.Blue
        PBRegalo.Width = (PBRegalo.Width + enlargenum)
        PBRegalo.Height = (PBRegalo.Height + enlargenum)
        PBRegalo.Location = New Point(PBRegalo.Location.X - enlargenum / 2, PBRegalo.Location.Y - enlargenum / 2)
    End Sub

    Private Sub PBRegalo_MouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PBRegalo.MouseLeave
        '  Label4.Visible = False
        PBRegalo.BackColor = Color.GhostWhite
        PBRegalo.Width = (PBRegalo.Width - enlargenum)
        PBRegalo.Height = (PBRegalo.Height - enlargenum)
        PBRegalo.Location = New Point(PBRegalo.Location.X + enlargenum / 2, PBRegalo.Location.Y + enlargenum / 2)
        textControlFront()
    End Sub

    Private Sub textControlFront()
        Label1control.BringToFront()
        Label2control.BringToFront()
        Label3control.BringToFront()
        Label4control.BringToFront()
        Label5control.BringToFront()
        Label6control.BringToFront()
    End Sub

    Private Sub MDIMain_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control And e.KeyCode = Keys.P Then
            PictureBox1_Click(sender, e)
        End If
        If e.Control And e.KeyCode = Keys.C Then
            PictureBox2_Click(sender, e)
        End If
        If e.Control And e.KeyCode = Keys.K Then
            PictureBox6_Click(sender, e)
        End If
        If e.Control And e.KeyCode = Keys.A Then
            PictureBox4_Click(sender, e)
        End If
        If e.Control And e.KeyCode = Keys.O Then
            PictureBox5_Click(sender, e)
        End If
        If e.Control And e.KeyCode = Keys.M Then
            PictureBox3_Click(sender, e)
        End If
    End Sub

    Private Sub MDIMain_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim cDirVersion As String = "C:\CpceMEGS"
        Dim cDirTestVersion As String = "C:\CpceMEGStest"
        Dim cFileVersion As String = "actualizar.ini"
        Dim cOldVersion As String = Nothing
        Dim cNewVersion As String = Nothing
        Dim cServidorIP As String = "actualizar.cpcechaco.org.ar"
        Dim cUrlVersion As String = "http://" & cServidorIP & "/cpcesistemas/actualizacion"
        ' FORMULARIO TRANSPARENTE
        '  Me.ControlBox = False
        '  Me.BackColor = Color.White
        '  Me.TransparencyKey = Color.White
        ' POSICIONAR EN LA PANTALLA
        Top = 5
        Left = (Screen.PrimaryScreen.Bounds.Width - Width - 5)
        MenuStrip1.DefaultDropDownDirection = ToolStripDropDownDirection.BelowLeft
        InformesToolStripMenuItem.Owner.DefaultDropDownDirection = ToolStripDropDownDirection.BelowLeft
        Try
            If Application.StartupPath = cDirTestVersion Then
                'C:\CpceMEGStest version test. Reemplazo Dir por DirTest
                cDirVersion = cDirTestVersion
                cOldVersion = cDirVersion & "\" & cFileVersion
                cNewVersion = cDirVersion & "\Temp\" & cFileVersion
                cUrlVersion = cUrlVersion & "/test/" & cFileVersion
            ElseIf Application.StartupPath = cDirVersion Then
                'C:\CpceMEGS version prod
                cOldVersion = cDirVersion & "\" & cFileVersion
                cNewVersion = cDirVersion & "\Temp\" & cFileVersion
                cUrlVersion = cUrlVersion & "/" & cFileVersion
            Else
                'version dev
                'siActualizar = False
                'C:\CpceMEGS version prod
                cOldVersion = cDirVersion & "\" & cFileVersion
                cNewVersion = cDirVersion & "\Temp\" & cFileVersion
                cUrlVersion = cUrlVersion & "/" & cFileVersion
                cPubCorreoDestinatario = "cpcechaco.org.ar@gmail.com" 'Seteo correo global para debug Correos.vb
            End If
            If IO.Directory.Exists(cDirVersion & "\Temp") = False Then ' si no existe la carpeta se crea
                IO.Directory.CreateDirectory(cDirVersion & "\Temp")
            End If
            'ping a dns google
            If ComprobarConexion() Then
                'ping al servidor
                If ComprobarConexion(cServidorIP) Then
                    My.Computer.Network.DownloadFile(cUrlVersion, cNewVersion, "", "", False, 10000, True)
                    Dim fileReaderOld As Int16 = My.Computer.FileSystem.ReadAllText(cOldVersion)
                    Dim fileReaderNew As Int16 = My.Computer.FileSystem.ReadAllText(cNewVersion)
                    If fileReaderNew > fileReaderOld Then
                        MessageBox.Show("Se ha encontrado una nueva versión disponible, ejecutando actualizador.")
                        ' + Application.StartupPath & "\VersionMEGS.exe"
                        Process.Start(Application.StartupPath & "\VersionMEGS.exe")
                        End
                    End If
                End If
            End If
        Catch ex As Net.WebException
            MessageBox.Show("Posible error del servidor de actualización." & vbCrLf & "Presione enter o aceptar para continuar.", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Catch ex As Exception
            MessageBox.Show("Error de directorios." & vbCrLf & ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        ' FORMULARIO TRANSPARENTE
        'Me.ControlBox = False
        'Me.BackColor = Color.White
        'Me.TransparencyKey = Color.White
        ' POSICIONAR EL EL CENTRO DE LA PANTALLA
        If PrevInstance() = True Then
            MessageBox.Show("El sistema ya esta en ejecución", "CpceMEGS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '   ControlClose = True
            Application.Exit()
        End If

        cnn = New ConsultaBD()
        cPubNombrePC = My.Computer.Name
        cPubIpLocal = TraeIpHost(cPubNombrePC)
        'cnn = New ConsultaBD(cPubServidor, "cpce", cPubUsuario, cPubClave)
        'MessageBox.Show(MenuStrip1.Items("OpcionesToolStripMenuItem").ToString)
        If My.Forms.frmLogin.ShowDialog() = DialogResult.Cancel Then
            End
        End If
        'LOGUEADO OK
        'Controlo permisos 7 Informes
        Dim subMenu As ToolStripMenuItem = MenuStrip1.Items("OpcionesToolStripMenuItem")
        If controlAcceso(7, , , False) = False Then
            subMenu.DropDownItems("InformesToolStripMenuItem").Enabled = False
            subMenu.DropDownItems("InformesToolStripMenuItem").Visible = False
        Else
            'Libro Mayor 1 Libro Diario 2 Balance de Sumas y Saldos 3 Caja General 4
            'Exportar Retenciones 5 Padrones 6 Movimientos de Cheque 7 Acreditación de la Renta 8 Gerenciales 9
            Dim subMenuItems As ToolStripMenuItem = subMenu.DropDownItems("InformesToolStripMenuItem")
            If controlAcceso(7, 1, , False) = False Then
                subMenuItems.DropDownItems("LibroMayorToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("LibroMayorToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 2, , False) = False Then
                subMenuItems.DropDownItems("LibroDiarioToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("LibroDiarioToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 3, , False) = False Then
                subMenuItems.DropDownItems("BalanceSumaYSaldoToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("BalanceSumaYSaldoToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 4, , False) = False Then
                subMenuItems.DropDownItems("CajaGeneralToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("CajaGeneralToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 5, , False) = False Then
                subMenuItems.DropDownItems("ExportarToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("ExportarToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 6, , False) = False Then
                subMenuItems.DropDownItems("PadronesToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("PadronesToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 7, , False) = False Then
                subMenuItems.DropDownItems("MovimientosDeChequeToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("MovimientosDeChequeToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 8, , False) = False Then
                subMenuItems.DropDownItems("CalculoNumeralesToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("CalculoNumeralesToolStripMenuItem").Visible = False
            End If
            If controlAcceso(7, 9, , False) = False Then
                subMenuItems.DropDownItems("GerencialesToolStripMenuItem").Enabled = False
                subMenuItems.DropDownItems("GerencialesToolStripMenuItem").Visible = False
            End If
            'Fin controlo permisos
        End If
        Try
            consultaBD = New ConsultaBD(True)
            consultaBD.AbrirConexion()

            da_afiliados_cumpleanios = consultaBD.GetAfiliadosCumpleanios()
            da_afiliados_cumpleanios.Fill(dt_afiliados_cumpleanios)

            dt_afiliados_cumpleanios.Columns.Add("Hoy Cumplen Años", GetType(String))
            dt_afiliados_cumpleanios.Columns("Hoy Cumplen Años").ReadOnly = False

            For Each row As DataRow In dt_afiliados_cumpleanios.Rows
                row.Item("Hoy Cumplen Años") = row.Item("Afiliado").ToString() + ", " + row.Item("Fecha").ToString() + ", Cumple " + row.Item("Edad").ToString() + " Años"
            Next

            dt_afiliados_cumpleanios.Columns.Remove("Afiliado")
            dt_afiliados_cumpleanios.Columns.Remove("Fecha")
            dt_afiliados_cumpleanios.Columns.Remove("Edad")

            dt_afiliados_cumpleanios.AcceptChanges()

            DGVAfiCumpleanios.DataSource = dt_afiliados_cumpleanios

            DGVAfiCumpleanios.AutoResizeColumns()
            DGVAfiCumpleanios.Dock = DockStyle.Fill
            DGVAfiCumpleanios.ColumnHeadersDefaultCellStyle.ForeColor = Color.White 'Letras
            DGVAfiCumpleanios.ColumnHeadersDefaultCellStyle.Font = New Font("Roboto", 9, FontStyle.Bold)
            DGVAfiCumpleanios.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#323232") 'Fondo de Header
            DGVAfiCumpleanios.RowHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#7f7f7f") 'Selector
            DGVAfiCumpleanios.RowsDefaultCellStyle.Font = New Font("Roboto", 9)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            consultaBD.CerrarConexion()
        Finally
            consultaBD.CerrarConexion()
        End Try
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox1.Click
        If controlAcceso(1) = False Then
            Exit Sub
        End If
        Dim frmProf As New FrmProfesionales
        frmProf.MdiParent = MdiParent
        frmProf.Show()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox2.Click
        If controlAcceso(2) = False Then
            Exit Sub
        End If
        Dim frmComproba As New FrmComprobantes
        frmComproba.MdiParent = MdiParent
        frmComproba.Show()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox3.Click
        If controlAcceso(6) = False Then
            Exit Sub
        End If
        Dim frmMante As New FrmMantenimiento
        frmMante.MdiParent = MdiParent
        frmMante.Show()
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox4.Click
        If controlAcceso(4) = False Then
            Exit Sub
        End If
        Dim frmConta As New FrmAsiento
        frmConta.MdiParent = MdiParent
        frmConta.Show()
    End Sub

    Private Sub PictureBox5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox5.Click
        If controlAcceso(5) = False Then
            Exit Sub
        End If
        Dim frmObl As New FrmObleas
        frmObl.MdiParent = MdiParent
        frmObl.Show()
    End Sub

    Private Sub PictureBox6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox6.Click
        If controlAcceso(3) = False Then
            Exit Sub
        End If
        Dim frmCaj As New FrmCaja
        frmCaj.MdiParent = MdiParent
        frmCaj.Show()
    End Sub
    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        If controlAcceso(4) = False Then
            Exit Sub
        End If
        Dim frmContaMatricula As New FrmAsientoMatricula
        frmContaMatricula.MdiParent = MdiParent
        frmContaMatricula.Show()
    End Sub

    Private Sub PBLiquidacion_Click(sender As Object, e As EventArgs) Handles PBLiquidacion.Click
        If controlAcceso(8) = False Then
            Exit Sub
        End If
        Dim frmObl As New FrmObleas(True) 'True es liquidacion
        frmObl.MdiParent = MdiParent
        frmObl.Show()
    End Sub
    Private Sub PBTrabajo_Click(sender As Object, e As EventArgs) Handles PBTrabajo.Click
        If controlAcceso(9) = False Then
            Exit Sub
        End If
        Dim frmTrabajo As New FrmTrabajos()
        frmTrabajo.MdiParent = MdiParent
        frmTrabajo.Show()
    End Sub
    Private Sub PBRegalo_Click(sender As Object, e As EventArgs) Handles PBRegalo.Click
        If UGBNovedades.Visible = True Then
            UGBNovedades.Visible = False
        Else
            UGBNovedades.Visible = True
        End If
    End Sub

    Private Sub BalanceSumaYSaldoToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BalanceSumaYSaldoToolStripMenuItem.Click
        Dim frmBalance As New FrmBalanceSumaSaldo
        frmBalance.Show()
    End Sub

    Private Sub LibroMayorToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LibroMayorToolStripMenuItem.Click
        Dim frmLibro As New FrmLibroMayor
        frmLibro.Show()
    End Sub

    Private Sub CajaGeneralToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CajaGeneralToolStripMenuItem.Click
        Dim frmCajaGen As New frmCajaGeneral
        frmCajaGen.Show()
    End Sub

    Private Sub ExportarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExportarToolStripMenuItem.Click
        Dim frmExportar As New frmExportarArchivo
        frmExportar.Show()
    End Sub

    Private Sub PadronesToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PadronesToolStripMenuItem.Click
        Dim frmPadrones As New FrmListadosGerencialesDeudores
        frmPadrones.MdiParent = MdiParent
        frmPadrones.Show()
    End Sub

    Private Sub MovimientosDeChequeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MovimientosDeChequeToolStripMenuItem.Click
        Dim frmMovChe As New FrmMoviCheques
        frmMovChe.MdiParent = MdiParent
        frmMovChe.Show()
    End Sub

    Private Sub CalculoNumeralesToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CalculoNumeralesToolStripMenuItem.Click
        Dim frmNumeral As New FrmCalculosNumerales
        frmNumeral.MdiParent = MdiParent
        frmNumeral.Show()
    End Sub

    Private Sub LibroDiarioToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LibroDiarioToolStripMenuItem.Click
        Dim frmDiario As New FrmLibroDiario
        frmDiario.MdiParent = MdiParent
        frmDiario.Show()
    End Sub

    Private Sub DeudoresToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DeudoresToolStripMenuItem.Click
        Dim frmGeren As New FrmListadoDeudores
        frmGeren.MdiParent = MdiParent
        frmGeren.Show()
    End Sub

    Private Sub SaldosProfesionalesToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SaldosProfesionalesToolStripMenuItem.Click
        Dim frmGeren As New FrmListadoSaldosProfesionales
        frmGeren.MdiParent = MdiParent
        frmGeren.Show()
    End Sub

    Private Sub DeudoresDEPToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DeudoresDEPToolStripMenuItem.Click
        Dim FrmDeuDEP As New FrmDeudoresDEPActivosSeguro
        FrmDeuDEP.MdiParent = MdiParent
        FrmDeuDEP.Show()
    End Sub

    Private Sub MorososCuotasAportesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MorososCuotasAportesToolStripMenuItem.Click
        Dim FrmMorosoCuotaAporte As New FrmMorososCuotasAportes
        FrmMorosoCuotaAporte.MdiParent = MdiParent
        FrmMorosoCuotaAporte.Show()
    End Sub

    Private Sub CambiarContraseñaToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CambiarContraseñaToolStripMenuItem.Click
        Dim frmCambiarPassword As New FrmCambiarPassword
        frmCambiarPassword.Show()
    End Sub

    Private Sub DeudoresPrestamosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeudoresPrestamosToolStripMenuItem.Click
        Dim FrmDeudoresPrestamos As New FrmDeudoresPrestamos
        FrmDeudoresPrestamos.MdiParent = MdiParent
        FrmDeudoresPrestamos.Show()
    End Sub

    Private Sub DebitosToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub JubilacionesToolStripMenuItem_Click(sender As Object, e As EventArgs)
        If controlAcceso(10, 2) = False Then
            Exit Sub
        End If
        Dim FrmJubilaciones As New FrmJubilaciones
        FrmJubilaciones.MdiParent = MdiParent
        FrmJubilaciones.Show()
    End Sub

    Private Sub Debitos2ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DebitosToolStripMenuItem.Click
        If controlAcceso(10, 1) = False Then
            Exit Sub
        End If
        Dim FrmDebitosAutomaticos As New FrmDebitosAutomaticos
        FrmDebitosAutomaticos.MdiParent = MdiParent
        FrmDebitosAutomaticos.Show()
    End Sub

    Private Sub SipresMainToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim FrmSipresMain As New FrmSipresMain
        FrmSipresMain.MdiParent = MdiParent
        FrmSipresMain.Show()
    End Sub

    Private Sub SipresMenuToolStripMenuItem_Click(sender As Object, e As EventArgs) 
        Dim FrmSipresMenu As New FrmSipres
        FrmSipresMenu.MdiParent = MdiParent
        FrmSipresMenu.Show()
    End Sub
End Class
