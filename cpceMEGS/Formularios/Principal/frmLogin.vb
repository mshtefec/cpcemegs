﻿Imports MySql.Data.MySqlClient
Public Class frmLogin
    Dim cnn As ConsultaBD
    Private DAglobalMysql As MySqlDataAdapter
    Dim DSOperador As DataSet
    Dim DSPermisos As DataSet
    Dim CResult As DialogResult = DialogResult.Cancel

    Private Sub UsernameTextBox_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles UsernameTextBox.KeyPress, PasswordTextBox.KeyPress
        Tabular(e)
    End Sub

    Private Sub ValidaOperador()
        If PassOperador() Then
            nPubNroOperador = DSOperador.Tables("operador").Rows(0).Item("ope_nroope").ToString
            cPubNomOperador = DSOperador.Tables("operador").Rows(0).Item("ope_nombre").ToString
            CResult = DialogResult.OK
            Close()
        End If
    End Sub
    Private Function PassOperador() As Boolean
        If ControlCampos() Then
            'Controla el usuario
            If ModPrincipal.loginCpce(UsernameTextBox.Text, PasswordTextBox.Text) Then
                ModPrincipal.ComprobarFechaServidor()
                cnn = New ConsultaBD(True)
                If cnn.AbrirConexion Then
                    'Busco operador
                    DAglobalMysql = cnn.consultaBDadapter("operador", , "ope_nrocli=" & Trim(nPubNroCli) & " AND ope_nombre='" & Trim(UsernameTextBox.Text) & "'")
                    DSOperador = New DataSet
                    'Guardo operador
                    DAglobalMysql.Fill(DSOperador, "operador")
                    If DSOperador.Tables("operador").Rows.Count = 1 Then
                        If DSOperador.Tables("operador").Rows(0).Item("ope_activo") <> 1 Then
                            MessageBox.Show("Usuario inactivo. (03001)", "Acceso", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Application.Exit()
                        End If
                        If DSOperador.Tables("operador").Rows(0).Item("ope_clave").ToString = MD5pass(PasswordTextBox.Text) Then
                            PassOperador = True
                            pubOperador = DSOperador.Tables("operador").Rows(0).Item("ope_nombre")
                            Try
                                Dim selectSQL As String =
                                    "permiso as p " +
                                    "INNER JOIN modulo as m ON p.per_modulo = m.Id " +
                                    "INNER JOIN accion as a ON p.per_accion = a.Id " +
                                    "INNER JOIN permiso_tipo as pt ON p.per_permisos = pt.Id"
                                Dim camposSQL As String = "mod_numero as modulo, acc_numero as accion, pet_tipo as permisos"
                                Dim whereSQL As String = "per_operador='" & DSOperador.Tables("operador").Rows(0).Item("ope_nroope") & "'"
                                DAglobalMysql = cnn.consultaBDadapter(selectSQL, camposSQL, whereSQL)
                                DSPermisos = New DataSet
                                DAglobalMysql.Fill(DSPermisos, "permiso")
                                pubPermisos = DSPermisos.Tables("permiso")
                                'pubPermisos = (From row In DSPermisos.Tables("permiso") Select colModulo = row("mod_nombre").ToString, colAccion = row("acc_nombre").ToString, colPermisos = row("pet_tipo").ToString).ToArray
                            Catch ex As Exception
                                MessageBox.Show("Error de permisos. (03002)", "Acceso", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Try
                            'Seteo valores por default segun sucursal nPubNroCli
                            Dim DTFarcat As DataTable = New DataTable
                            DAglobalMysql = cnn.consultaBDadapter("farcat", "far_locali, far_siglas, far_impres", " far_activo = " & nPubNroCli)
                            DAglobalMysql.Fill(DTFarcat)
                            cPubSucursal = DTFarcat.Rows(0).Item("far_locali")
                            cPubSucursalSiglas = DTFarcat.Rows(0).Item("far_siglas")
                            cPubSucursalImpresion = DTFarcat.Rows(0).Item("far_impres")
                            'Fin Seteo valores por default
                        End If
                    Else
                        PassOperador = False
                    End If
                    cnn.CerrarConexion()
                Else
                    PassOperador = False
                End If
            Else
                PassOperador = False
            End If
        Else
            PassOperador = False
        End If
    End Function

    Private Sub frmLogin_Activated(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Activated
        UsernameTextBox.Focus()
    End Sub

    Private Sub frmLogin_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
        DialogResult = CResult
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Text = "INICIO SESIÓN | CONEXION: " + cPubTipoConexion
        CResult = DialogResult.Cancel
        UsernameTextBox.Text = ""
        PasswordTextBox.Text = ""
        UsernameTextBox.Focus()
    End Sub

    Private Sub CButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CButton1.Click
        CResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub CButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CButton2.Click
        ValidaOperador()
    End Sub

    Private Sub CButton1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles CButton1.KeyDown
        CResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub CButton2_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles CButton2.KeyDown
        ValidaOperador()
    End Sub

    Public Sub New()
        'Llamada necesaria para el diseñador.
        InitializeComponent()
        'Agregue cualquier inicialización después de la llamada a InitializeComponent().
        AllowTransparency = False
    End Sub
    'Controlo campos no sean vacios o nulos
    Private Function ControlCampos() As Boolean
        If String.IsNullOrEmpty(UsernameTextBox.Text) Then
            MessageBox.Show("El campo no debe estar vacio.")
            UsernameTextBox.Focus()
            Return False
        End If
        If String.IsNullOrEmpty(PasswordTextBox.Text) Then
            MessageBox.Show("El campo no debe estar vacio.")
            PasswordTextBox.Focus()
            Return False
        End If

        Return True
    End Function
End Class
