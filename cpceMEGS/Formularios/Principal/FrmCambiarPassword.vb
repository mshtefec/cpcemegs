﻿Imports MySql.Data.MySqlClient
Public Class FrmCambiarPassword
    Dim c_cnn As ConsultaBD
    Dim DAOperador As MySqlDataAdapter
    Dim DSOperador As DataSet
    Dim CResult As DialogResult = DialogResult.Cancel

    Private Sub FrmCambiarPassword_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        TextContraAnterior.Focus()
    End Sub

    Private Sub frmCambiarPassword_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        CResult = DialogResult.Cancel
        c_cnn = New ConsultaBD(True)
        TextContraAnterior.Focus()
    End Sub

    Private Sub ButtonCancelar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles ButtonCancelar.Click
        CResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ButtonAceptar_Click(ByVal Sender As Object, ByVal e As MouseEventArgs) Handles ButtonAceptar.Click
        If ControlCampos() Then
            PassOperador()
        End If
    End Sub

    Private Function PassOperador() As Boolean
        If c_cnn.AbrirConexion Then
            DAOperador = New MySqlDataAdapter
            DAOperador = c_cnn.consultaBDadapter("operador", , "ope_nombre='" & pubOperador & "'")
            DSOperador = New DataSet
            DAOperador.Fill(DSOperador, "operador")

            If DSOperador.Tables(0).Rows.Count = 1 Then
                Dim contraAnterior As String = TextContraAnterior.Text
                Dim contraNueva As String = TextContraNueva.Text
                Dim contraRepetir As String = TextContraRepetir.Text
                If DSOperador.Tables(0).Rows(0).Item("ope_clave").ToString = MD5pass(contraAnterior) Then
                    If contraNueva = contraRepetir Then
                        If savePassword(contraNueva) Then
                            MessageBox.Show("Se guardo correctamente su nueva contraseña.")
                            Me.Close()
                        End If
                    Else
                        MessageBox.Show("La contraseña nueva debe coincidir.")
                    End If
                Else
                    MessageBox.Show("La contraseña no es válida.")
                End If
            End If
            c_cnn.CerrarConexion()
        Else
            MessageBox.Show("Error de conexión.")
        End If
    End Function

    Private Function savePassword(ByVal password As String) As Boolean
        Try
            password = MD5pass(password)
            c_cnn.actualizarBD("update operador set ope_clave='" & password & "' WHERE ope_nombre='" & pubOperador & "'")
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try
    End Function
    'Controlo campos no sean vacios o nulos
    Private Function ControlCampos() As Boolean
        If String.IsNullOrEmpty(TextContraAnterior.Text) Then
            MessageBox.Show("El campo no debe estar vacio.")
            TextContraAnterior.Focus()
            Return False
        End If
        If String.IsNullOrEmpty(TextContraNueva.Text) Then
            MessageBox.Show("El campo no debe estar vacio.")
            TextContraNueva.Focus()
            Return False
        End If
        If String.IsNullOrEmpty(TextContraRepetir.Text) Then
            MessageBox.Show("El campo no debe estar vacio.")
            TextContraRepetir.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub TextContraAnterior_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextContraAnterior.KeyPress
        Tabular(e)
    End Sub

    Private Sub TextContraNueva_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextContraNueva.KeyPress
        Tabular(e)
    End Sub

    Private Sub TextContraRepetir_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextContraRepetir.KeyPress
        Tabular(e)
    End Sub

    Public Sub New()
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        'Sin Transparencia
        Me.AllowTransparency = False
    End Sub
End Class