﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExportarArchivo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportarArchivo))
        Me.UltraTree1 = New Infragistics.Win.UltraWinTree.UltraTree()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UDThasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDTdesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UltraProgressBar1 = New Infragistics.Win.UltraWinProgressBar.UltraProgressBar()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.TxOrden = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UltraTree1
        '
        Me.UltraTree1.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.UltraTree1.Location = New System.Drawing.Point(87, 12)
        Me.UltraTree1.Name = "UltraTree1"
        Me.UltraTree1.NodeConnectorColor = System.Drawing.SystemColors.ControlDark
        Me.UltraTree1.Size = New System.Drawing.Size(235, 92)
        Me.UltraTree1.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(8, 234)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Hasta"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(8, 207)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Desde"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(7, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 21)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "Entidad"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDThasta
        '
        Me.UDThasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDThasta.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDThasta.FormatString = ""
        Me.UDThasta.Location = New System.Drawing.Point(87, 234)
        Me.UDThasta.MaskInput = "{date}"
        Me.UDThasta.Name = "UDThasta"
        Me.UDThasta.Size = New System.Drawing.Size(236, 21)
        Me.UDThasta.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDThasta.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDThasta.TabIndex = 3
        '
        'UDTdesde
        '
        Me.UDTdesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
        Me.UDTdesde.FormatProvider = New System.Globalization.CultureInfo("es-AR")
        Me.UDTdesde.FormatString = ""
        Me.UDTdesde.Location = New System.Drawing.Point(87, 207)
        Me.UDTdesde.MaskInput = "{date}"
        Me.UDTdesde.Name = "UDTdesde"
        Me.UDTdesde.Size = New System.Drawing.Size(236, 21)
        Me.UDTdesde.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left
        Me.UDTdesde.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always
        Me.UDTdesde.TabIndex = 2
        '
        'UltraProgressBar1
        '
        Me.UltraProgressBar1.Location = New System.Drawing.Point(8, 261)
        Me.UltraProgressBar1.Name = "UltraProgressBar1"
        Me.UltraProgressBar1.Size = New System.Drawing.Size(316, 28)
        Me.UltraProgressBar1.TabIndex = 64
        Me.UltraProgressBar1.Text = "[Formatted]"
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.Image = Global.cpceMEGS.My.Resources.Resources.PrintSetup_11011
        Me.CBtExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtExportar.Location = New System.Drawing.Point(66, 295)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(192, 50)
        Me.CBtExportar.TabIndex = 4
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'TxOrden
        '
        Me.TxOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxOrden.Location = New System.Drawing.Point(87, 179)
        Me.TxOrden.Name = "TxOrden"
        Me.TxOrden.Size = New System.Drawing.Size(235, 22)
        Me.TxOrden.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(7, 180)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 21)
        Me.Label6.TabIndex = 69
        Me.Label6.Text = "Nro Orden:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.Location = New System.Drawing.Point(33, 122)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(135, 20)
        Me.RadioButton1.TabIndex = 70
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Retenciones DGR"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.Location = New System.Drawing.Point(33, 148)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(195, 20)
        Me.RadioButton2.TabIndex = 71
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Retenciones Ganancias/IVA"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'frmExportarArchivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(337, 346)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.TxOrden)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.UltraTree1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.UDThasta)
        Me.Controls.Add(Me.UDTdesde)
        Me.Controls.Add(Me.UltraProgressBar1)
        Me.Controls.Add(Me.CBtExportar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarArchivo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Exportar Retenciones "
        CType(Me.UltraTree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDThasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTdesde, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UltraTree1 As Infragistics.Win.UltraWinTree.UltraTree
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UDThasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTdesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UltraProgressBar1 As Infragistics.Win.UltraWinProgressBar.UltraProgressBar
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents TxOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
End Class
