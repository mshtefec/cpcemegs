﻿Imports MySql.Data.MySqlClient
Imports System.Data.OleDb
Imports System.Drawing.Drawing2D
Imports System.Net
Imports System.Net.Dns
Imports System.Reflection
'Encrypt conection string
Imports System.IO
Imports System.Text

Module ModPrincipal
    Public nPubNroIns As Integer
    Public nPubNroCli As Integer
    Public nPubNroOperador As Integer
    Public cPubNomOperador As String
    Public cPubServidor As String
    Public ReadOnly cPubPuerto As String = "5306"
    Public cPubUsuario As String
    Public cPubClave As String
    Public cPubMotorBD As String
    Public cPubIpLocal As String
    Public cPubNombrePC As String
    Public cPubConexion As String
    Public cPubTipoConexion As String
    Public cPubBD As String = "cpce"
    Public DRCliente As DataRow
    Public delegacion As Integer
    'Resumen de Cta
    '----------------------------------
    Dim DAResumen As MySqlDataAdapter
    Dim DSResumen As DataSet
    Dim Comm As MySqlCommand
    Dim DAAcc As OleDbDataAdapter
    Dim DSAcc As DataSet
    Dim CommAcc As OleDbCommand
    Public Reporte As String
    '----------------------------------
    Public DSClientes As DataSet
    'SEGURIDAD
    Public pubOperador As String
    Public pubPermisos As DataTable
    'IMPRESORAS
    Public ReadOnly pubImpresoraNombreObleas As String = "epson"
    'Public pubImpresoraNombreObleas As String = "\\MESAENTRADA-PC\epson"
    Public ReadOnly pubImpresoraNombreCheques As String = "epson"
    'Public pubImpresoraNombreCheques As String = "\\TERMINALVANINA\epson"
    Public ReadOnly pubImpresoraPaginaObleas As String = "Obleas"
    Public ReadOnly pubImpresoraPaginaCheques As String = "Cheques"
    Public ReadOnly pubCaracteresPermiteNumero As String = "0123456789.,"
    Public ReadOnly pubCaracteresPermiteComitente As String = "abcdefghijklmnñopqrstuvwxyz0123456789º°.-'/()& "
    Public ReadOnly pubCaracteresPermiteProfesional As String = "abcdefghijklmnñopqrstuvwxyz0123456789°.-' "
    'Se utiliza en el listado de padrones para seleccionar solo estos campos
    Public camposListadoSelect As String() = {
        "afi_nombre", "afi_fecnac", "afi_tipo", "afi_tipdoc", "afi_nrodoc", "afi_titulo", "afi_matricula",
        "afi_direccion", "afi_localidad", "afi_provincia", "afi_zona", "afi_civil", "afi_tipoiva", "afi_cuit",
        "afi_categoria", "afi_sexo", "afi_telefono1", "afi_telefono2", "afi_mail", "afi_mail_alternativo"
    }
    'Se utiliza en el listado de padrones para seleccionar solo estos campos
    'D1-0, D2-1, D3-2
    'C1-3, C2-4, C3-5, C4-6, C5-7, C6-8, C7-9, C8-10
    Public montosPorcentajesMoratoria As Double() = {
        "0,7823", "0,166666667", "0,051033333",
        "0,475966667", "0", "0,122966667", "0", "0,0919", "0,091466667", "0,051033333", "0,166666667"
    }
    Public cPubSucursal As String 'GUARDO LA SUCURSAL
    Public cPubSucursalSiglas As String 'GUARDO LA ABREVIATURA USO EN OBLEA
    Public ReadOnly cPubSucursalSiglasTodas As String() = {"0", "SC", "SP", "VA", "SO"} 'Guardo las abreviaturas que existen
    Public cPubSucursalImpresion As Integer 'SI ES 1 UTILIZA ImpresionDirecta EN OBLEA SINO MUESTRA FrmReportes
    Public ReadOnly cPubTrabajoAnticipo As String = "200" 'GUARDO LA ABREVIATURA USO EN OBLEA
    Public cPubCorreoDestinatario As String = Nothing 'Correos.vb Este valor seteo en MDIMain_Load si es Debug
    'Intereses Sipres
    Public ReadOnly pubCuentasGeneranInteres As String() = {"13040100", "13051000", "13051100", "13051600", "13050700", "13050900"}
    Public ReadOnly pubCuentasGeneranInteresOtros As String() = {"13051000", "13051100", "13051600", "13050700", "13050900"}
    Public ReadOnly pubMontosInteresSipresAporte As Double = 0.05 'Se usa en Calculos y FrmDeudoresPrestamos
    Public ReadOnly pubMontosInteresSipresOtros As Double = 0.13 'Se usa en Calculos y FrmDeudoresPrestamos
    Public ReadOnly valEstados As New Infragistics.Win.ValueList 'Se usa en FrmConciliarBanco y FrmMoviCheques
    Public Sub loadValEstados()
        valEstados.ValueListItems.Add("", "Ingreso")
        valEstados.ValueListItems.Add("E", "Impreso")
        valEstados.ValueListItems.Add("A", "Acreditado")
        valEstados.ValueListItems.Add("R", "Rechazado")
    End Sub
    'Public Function excOR(ByVal Cadena As String, ByVal semilla As String) As String
    '    Dim I As Integer
    '    Dim ptr As Integer
    '    Dim encrip As String
    '    ptr = 1 : encrip = ""
    '    For I = 1 To Len(Cadena)
    '        If ptr > Len(semilla) Then ptr = 1
    '        encrip = encrip + Chr(Asc(Mid(Cadena, I, 1)) Xor Asc(Mid(semilla, ptr, 1)))
    '        ptr = ptr + 1
    '    Next I
    '    excOR = encrip
    'End Function
    'Public ReadOnly cPubProcesosSipres As String() = {'FrmMantenimiento Balanceo uso en ConsultaBD BalanceoAsiento
    '    "01R126", "01R127", "01R128", "01R129", "01R131", "01R132", "01R133", "01R134", "01R135", "01R136", "01R137", "01R138"
    '}
    Public Function MD5pass(ByVal Cadena As String, Optional ByVal Separator As String = Nothing) As String
        Dim Hash As Byte()
        Dim Encoder As New UTF8Encoding()
        Dim Hasher As New System.Security.Cryptography.MD5CryptoServiceProvider()
        Hash = Hasher.ComputeHash(Encoder.GetBytes(Cadena))
        MD5pass = Replace(BitConverter.ToString(Hash), "-", Separator)
    End Function

    Public Function controlAcceso(ByVal modulo As Integer, Optional ByVal accion As Integer = 0, Optional ByVal permisos As String = "R", Optional ByVal mostrarMensaje As Boolean = True) As Boolean
        Dim permiso As String
        Try
            controlAcceso = False
            'permiso super admin
            Dim permisosArray = (From row In pubPermisos Where (row("modulo") = 0)).ToArray
            If permisosArray.Count() > 0 Then
                'si contiene permiso super admin entra
                controlAcceso = True
            Else
                If accion = 0 Then
                    'si accion = 0 busca por modulo y permiso
                    'Recorro y consulto por cada permiso individual C R U D
                    For Each p As Char In permisos
                        permiso = "*" & p & "*" 'para poder usar LIKE **
                        permisosArray = (From row In pubPermisos Where (row("modulo") = modulo And row("permisos").ToString Like permiso)).ToArray
                        If permisosArray.Count() > 0 Then
                            'si encontro el permiso = True
                            controlAcceso = True
                            Exit For
                        End If
                    Next
                Else
                    'si accion viene como parametro busca modulo, accion y permiso
                    'O si accion = 100. Acceso Total
                    'Recorro y consulto por cada permiso individual C R U D
                    For Each p As Char In permisos
                        permiso = "*" & p & "*" 'para poder usar LIKE **
                        permisosArray = (From row In pubPermisos Where (row("modulo") = modulo And (row("accion") = accion Or row("accion") = 100) And row("permisos").ToString Like permiso)).ToArray
                        If permisosArray.Count() > 0 Then
                            'si encontro el permiso = True
                            controlAcceso = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If controlAcceso = False And mostrarMensaje = True Then
                MessageBox.Show("Acceso Denegado. Contacte al Administrador.")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    'Public Sub ToggleConfigEncryption(ByVal exeConfigName As String)
    '    ' Takes the executable file name without the
    '    ' .config extension.
    '    Try
    '        ' Open the configuration file and retrieve 
    '        ' the connectionStrings section.
    '        Dim config As Configuration = ConfigurationManager. _
    '            OpenExeConfiguration(exeConfigName)
    '        Dim section As ConnectionStringsSection = DirectCast( _
    '            config.GetSection("connectionStrings"),  _
    '            ConnectionStringsSection)
    '        If section.SectionInformation.IsProtected Then
    '            ' Remove encryption.
    '            section.SectionInformation.UnprotectSection()
    '        Else
    '            ' Encrypt the section.
    '            section.SectionInformation.ProtectSection( _
    '              "DataProtectionConfigurationProvider")
    '        End If
    '        ' Save the current configuration.
    '        config.Save()
    '        Console.WriteLine("Protected={0}", _
    '        section.SectionInformation.IsProtected)
    '    Catch ex As Exception
    '        Console.WriteLine(ex.Message)
    '    End Try
    'End Sub
    Public Sub Tabular(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Public Sub CargoComboBox(ByVal conexion As ConsultaBD, ByVal cTabla As String, ByVal cCondicion As String, ByRef comboSuc As ComboBox, ByVal Dmember As String, ByVal Vmember As String)
        Dim DASucursal As MySqlDataAdapter
        Dim DSSucursal As DataSet
        DASucursal = New MySqlDataAdapter
        DASucursal = conexion.consultaBDadapter(cTabla, , cCondicion)
        DSSucursal = New DataSet
        DASucursal.Fill(DSSucursal, cTabla)
        comboSuc.DataSource = DSSucursal.Tables(0)
        comboSuc.DisplayMember = Dmember ' DSSucursal.Tables(0).Columns(Dmember).ToString
        comboSuc.ValueMember = Vmember ' DSSucursal.Tables(0).Columns(Vmember).ToString
    End Sub

    Public Sub FondoDegrade(ByVal color1 As Color, ByVal color2 As Color, ByVal ancho As Integer, ByVal alto As Integer, ByVal e As System.Windows.Forms.PaintEventArgs)
        Dim x As Integer = 0
        Dim y As Integer = 0

        Dim colorear As New LinearGradientBrush(New Point(x, y), New Point(x, y + alto + 100), color1, color2)
        'Dim colorear1 As New LinearGradientBrush(,,,,
        e.Graphics.FillRectangle(colorear, x, y, ancho, alto)
        y += alto + 10
    End Sub

    Public Function TraeIpHost(ByVal mStrHost As String) As String
        '   Dim mIpHostentry As IPHostEntry = GetHostEntry(mStrHost) 'GetHostByName(mStrHost)
        'Dim mIpAddList As IPAddress() = mIpHostentry.AddressList()
        Dim mIpLocal As String = "127.0.0.1"
        For Each ip In GetHostEntry(GetHostName).AddressList
            If ip.AddressFamily = Sockets.AddressFamily.InterNetwork Then
                'Añadir solo las correspoondientes a IP4
                mIpLocal = ip.ToString
                Exit For
            End If
        Next

        Return mIpLocal
    End Function
    '-----------------------------------------------------------------------------------------------------------
    'Para Completar con * los campos de las impresiones
    '-----------------------------------------------------------------------------------------------------------
    Public Function ceros(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "*"
        For i = 1 To Cantidad
            cuantos = cuantos & "*"
        Next i
        ceros = Mid(cuantos, 1, Cantidad - Len(numero)) & numero
    End Function

    Public Function Atrasceros(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "*"
        For i = 1 To Cantidad
            cuantos = "*" & cuantos
        Next i
        Atrasceros = numero & " " & Mid(cuantos, 1, Cantidad - Len(numero))
    End Function

    Public Function AtrasEspacios(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = " "
        For i = 1 To Cantidad
            cuantos = " " & cuantos
        Next i
        AtrasEspacios = numero & Mid(cuantos, 1, Cantidad - Len(numero))
    End Function

    Public Function ceros1(ByVal Nro As String, ByVal Cantidad As Integer) As String
        Dim numero As String, cuantos As String, i As Integer
        numero = Trim(Nro) 'Trim quita los espacion en blanco
        cuantos = "0"
        For i = 1 To Cantidad
            cuantos = cuantos & "0"
        Next i
        ceros1 = Mid(cuantos, 1, Cantidad - Len(numero)) & numero
    End Function

    Public Sub ThreadCargarMotorCrystal()
        Try
            Dim ensamblado As [Assembly]

            ensamblado = [Assembly].Load("CrystalDecisions.CrystalReports.Engine.dll")

            ensamblado = [Assembly].Load("CrystalDecisions.ReportSource.dll")

            ensamblado = [Assembly].Load("CrystalDecisions.Shared.dll")

            ensamblado = [Assembly].Load("CrystalDecisions.Windows.Forms.dll")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Function PrevInstance() As Boolean
        If UBound(Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub mostrarErrorConexion(Optional ByVal message As String = "", Optional ByVal excepcion As String = "")
        MessageBox.Show("Intente nuevamente." & Chr(13) & Chr(13) & "Ha ocurrido un error al " & message & Chr(13) & excepcion, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Public Sub mostrarMensajeExitoso(Optional ByVal message As String = "")
        MessageBox.Show("Se realizó exitosamente " & message, "EXITO", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
    End Sub
    ' La función retorna true si se pudo acceder a la dirección indicada  
    '********************************************************************
    Function ComprobarConexion(Optional url As String = "8.8.8.8") As Boolean
        ComprobarConexion = False
        If My.Computer.Network.IsAvailable() Then
            Try
                If My.Computer.Network.Ping(url, 1000) Then
                    ComprobarConexion = True
                ElseIf url = "8.8.8.8" Then
                    MessageBox.Show("No conectado a internet.", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Else
                    MessageBox.Show("El servidor de actualización no responde.", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Catch ex As NetworkInformation.PingException
                MessageBox.Show("No conectado a internet.", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        Else
            MessageBox.Show("Error de conexión.", "ALERTA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Function
    'Return true detener la ejecucion. Return false todo correcto.
    Public Function ComprobarFechaServidor(Optional ByVal cerrarAplicacion As Boolean = True) As Boolean
        ComprobarFechaServidor = False
        Dim c_cnn As New ConsultaBD(True)
        If c_cnn.AbrirConexion Then
            Dim dataAdapter As MySqlDataAdapter
            Dim dataSet As DataSet
            dataAdapter = c_cnn.consultaHoraServidor()
            dataSet = New DataSet
            'Guardo la hora en dataSet
            dataAdapter.Fill(dataSet, "hora")
            Dim diferenciaDeHora As Long = DateDiff(DateInterval.Hour, CDate(dataSet.Tables("hora").Rows(0).Item("hora").ToString), Date.Now)
            If diferenciaDeHora < 0 Then
                MessageBox.Show("La fecha y hora de su PC no son correctas. Corríjalas.   Error (04001)", "Acceso", MessageBoxButtons.OK, MessageBoxIcon.Error)
                If cerrarAplicacion Then
                    Application.Exit()
                Else
                    ComprobarFechaServidor = True
                End If
            End If
        End If
        c_cnn.CerrarConexion()
    End Function

    Public Function loginCpce(ByVal username As String, ByVal pass As String) As Boolean
        loginCpce = False
        If loginServerCpce(username, pass, "obtener.cpcechaco.org.ar") Then
            loginCpce = True
            'ElseIf loginServerCpce(username, pass, "obtener.cpcechaco.com.ar") Then
            'loginCpce = True
        End If
    End Function
    Private Function loginServerCpce(ByVal username As String, ByVal pass As String, ByVal url As String) As Boolean
        Try
            Dim params As String = "_usuario=" & Trim(username) & "&_clave=" & pass
            If ComprobarConexion(url) Then
                url = "http://" & url & "/retorno"
            Else
                MessageBox.Show("Error con servidores de acceso (loginServerCpce)", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                loginServerCpce = False
            End If
            If url <> "" Then
                'Controlo directorio prod o dev
                'Dim cDirVersion As String = "C:\CpceMEGS"
                'If Application.StartupPath <> cDirVersion Then
                '    url = "http://login.cpce.dev/retorno"
                'End If
                'Fin prod o dev
                Dim request As HttpWebRequest = WebRequest.Create(url)
                request.Accept = "*/*"
                request.Method = WebRequestMethods.Http.Post
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
                ' Create a new string object to POST data to the Url. 
                Dim encoding As New ASCIIEncoding()
                Dim byte1 As Byte() = encoding.GetBytes(params)
                ' Set the content type of the data being posted.
                request.ContentType = "application/x-www-form-urlencoded"
                ' Set the content length of the string being posted.
                request.ContentLength = byte1.Length
                Dim newStream As Stream = request.GetRequestStream()
                newStream.Write(byte1, 0, byte1.Length)
                'MessageBox.Show("The value of 'ContentLength' property after sending the data is " & request.ContentLength)
                newStream.Close()
                Dim oResponse As HttpWebResponse = request.GetResponse()
                Dim reader As New StreamReader(oResponse.GetResponseStream())
                Dim xmlString As String = reader.ReadToEnd()
                oResponse.Close()

                Dim xmlres As XElement = XElement.Parse(xmlString)

                If xmlres.<continuar>.Value() = "SI" Then
                    cPubClave = "Sistema_" & Text.Encoding.UTF8.GetString(Convert.FromBase64String(xmlres.<res>.Value()))
                    loginServerCpce = True
                Else
                    MessageBox.Show("Datos inválidos, ingrese nuevamente", "Inicio de sesión", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    loginServerCpce = False
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            loginServerCpce = False
        End Try
    End Function
    'OBLEAS
    'Retorna el numero de legalizacion en forma de texto
    Public Function NroLegalizacionLongToString(ByVal nNroLegalizacion As Long, Optional esAnticipo As Boolean = False) As String
        If esAnticipo = False Then
            NroLegalizacionLongToString = cPubSucursalSiglas & nNroLegalizacion & "/" & Now.Year
        Else
            'Para los trabajos Anticipos
            NroLegalizacionLongToString = cPubTrabajoAnticipo & nNroLegalizacion & "/" & Now.Year
        End If
    End Function
    'Retorna el numero de legalizacion para almacenar en la base de datos
    Public Function NroLegalizacionLongToSave(ByVal nNroLegalizacion As Long, Optional ByVal anio As String = Nothing, Optional esAnticipo As Boolean = False) As Long
        If IsNothing(anio) Then
            If esAnticipo = False Then
                NroLegalizacionLongToSave = nPubNroCli & nNroLegalizacion & Now.Year
            Else
                NroLegalizacionLongToSave = cPubTrabajoAnticipo & nNroLegalizacion & Now.Year
            End If
        ElseIf Integer.Parse(anio) < 2016 Then
            'If esAnticipo = False Then
            NroLegalizacionLongToSave = nNroLegalizacion & anio
            'Else
            '    NroLegalizacionLongToSave = nNroLegalizacion & anio
            'End If
        Else
            If esAnticipo = False Then
                NroLegalizacionLongToSave = nPubNroCli & nNroLegalizacion & anio
            Else
                NroLegalizacionLongToSave = cPubTrabajoAnticipo & nNroLegalizacion & anio
            End If
        End If
    End Function
    'Retorna el numero de legalizacion de la BD como String
    Public Function NroLegalizacionBDToString(ByVal nNroLegalizacion As String, Optional esAnticipo As Boolean = False) As String
        If esAnticipo = False Then
            Dim nNroLegalizacionSimple As String = Left(nNroLegalizacion, Len(nNroLegalizacion) - 4)
            nNroLegalizacionSimple = Right(nNroLegalizacionSimple, Len(nNroLegalizacionSimple) - 1)
            Dim nNroDelegacion As Integer = Left(nNroLegalizacion, 1)
            'NroLegalizacionBDToString = cPubSucursalSiglas & nNroLegalizacionSimple & "/" & GetAnioNroLegalizacion(nNroLegalizacion)
            NroLegalizacionBDToString = cPubSucursalSiglasTodas(nNroDelegacion) & nNroLegalizacionSimple & "/" & GetAnioNroLegalizacion(nNroLegalizacion)
        Else
            Dim nNroLegalizacionSimple As String = Left(nNroLegalizacion, Len(nNroLegalizacion) - 4)
            NroLegalizacionBDToString = nNroLegalizacionSimple & "/" & GetAnioNroLegalizacion(nNroLegalizacion)
        End If
    End Function
    'Retorna el numero ed legalizacion de string para ser guardado en BD
    Public Function NroLegalizacionStringToSave(ByVal nNroLegalizacion As String, Optional esAnticipo As Boolean = False) As String
        If esAnticipo = False Then
            Dim nNroLegalizacionSimple As String = Left(nNroLegalizacion, Len(nNroLegalizacion) - 5)
            nNroLegalizacionSimple = Right(nNroLegalizacionSimple, Len(nNroLegalizacionSimple) - 2)
            Dim nNroDelegacion As Integer = NroLegalizacionDelegacionStringToInteger(nNroLegalizacion)
            delegacion = nNroDelegacion
            NroLegalizacionStringToSave = nNroDelegacion & nNroLegalizacionSimple & GetAnioNroLegalizacion(nNroLegalizacion)
        Else
            Dim nNroLegalizacionSimple As String = Left(nNroLegalizacion, Len(nNroLegalizacion) - 5)
            NroLegalizacionStringToSave = nNroLegalizacionSimple & GetAnioNroLegalizacion(nNroLegalizacion)
        End If
    End Function
    Public Function NroLegalizacionDelegacionStringToInteger(ByVal nNroLegalizacion As String) As Integer
        NroLegalizacionDelegacionStringToInteger = 0
        Dim siglaDelegacion As String = Left(nNroLegalizacion, 2)
        Select Case siglaDelegacion
            Case "SC"
                NroLegalizacionDelegacionStringToInteger = 1
            Case "SP"
                NroLegalizacionDelegacionStringToInteger = 2
            Case "VA"
                NroLegalizacionDelegacionStringToInteger = 3
            Case "SO"
                NroLegalizacionDelegacionStringToInteger = 4
        End Select
    End Function
    Public Function GetAnioNroLegalizacion(ByVal nNroLegalizacion As String, Optional ByVal caracteres As Integer = 4) As String
        GetAnioNroLegalizacion = Right(nNroLegalizacion, caracteres)
    End Function
    Public Function GetEstadoStringSegunNro(ByVal numeroEstadoTrabajo As Integer) As String
        Select Case numeroEstadoTrabajo
            Case 1
                GetEstadoStringSegunNro = "GENERADO"
            Case 3
                GetEstadoStringSegunNro = "PAGADO"
            Case 5
                GetEstadoStringSegunNro = "PRESENTADO"
            Case 7
                GetEstadoStringSegunNro = "LIQUIDADO"
            Case 8
                GetEstadoStringSegunNro = "REINTEGRO"
            Case 15
                GetEstadoStringSegunNro = "COBRADO"
            Case 20
                GetEstadoStringSegunNro = "ANULADO"
            Case Else
                GetEstadoStringSegunNro = ""
        End Select
    End Function
    Public Function GetCertificadoStringSegunNro(ByVal numeroCertificadoTrabajo As Integer) As String
        Select Case numeroCertificadoTrabajo
            Case 1
                GetCertificadoStringSegunNro = "FINALIZADO"
            Case 2
                GetCertificadoStringSegunNro = "ENTREGADO"
            Case Else
                GetCertificadoStringSegunNro = ""
        End Select
    End Function
    'FIN OBLEAS
End Module
