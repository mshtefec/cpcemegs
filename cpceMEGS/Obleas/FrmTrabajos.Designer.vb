﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTrabajos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTrabajos))
        Me.UGBAccion = New Infragistics.Win.Misc.UltraGroupBox()
        Me.BindingNavigatorTrabajo = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.TrabajoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSTrabajo = New cpceMEGS.DSTrabajo()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSDDButtonBuscar = New System.Windows.Forms.ToolStripDropDownButton()
        Me.TodosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneradosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PresentadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfeccionandoReintegroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReintegrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobradosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSLCodigo = New System.Windows.Forms.ToolStripLabel()
        Me.TSTBCodigo = New System.Windows.Forms.ToolStripTextBox()
        Me.TSButtonCodigo = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSButtonCobrado = New System.Windows.Forms.ToolStripButton()
        Me.LabelLiquidar = New System.Windows.Forms.ToolStripLabel()
        Me.TSBLiquidar = New System.Windows.Forms.ToolStripButton()
        Me.TSLMatricula = New System.Windows.Forms.ToolStripLabel()
        Me.TSTBMatricula = New System.Windows.Forms.ToolStripTextBox()
        Me.TSButtonMatricula = New System.Windows.Forms.ToolStripButton()
        Me.UGBTrabajo = New Infragistics.Win.Misc.UltraGroupBox()
        Me.DGVTrabajo = New System.Windows.Forms.DataGridView()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.TrabajoestadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MatriculaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProfesionalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tra_tarea_id = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.TareasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IngresosDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrabajoTableAdapter = New cpceMEGS.DSTrabajoTableAdapters.trabajoTableAdapter()
        Me.TareasTableAdapter = New cpceMEGS.DSTrabajoTableAdapters.tareasTableAdapter()
        Me.Trabajo_estadoTableAdapter = New cpceMEGS.DSTrabajoTableAdapters.trabajo_estadoTableAdapter()
        Me.LabelCobrar = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        CType(Me.UGBAccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBAccion.SuspendLayout()
        CType(Me.BindingNavigatorTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigatorTrabajo.SuspendLayout()
        CType(Me.TrabajoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGBTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UGBTrabajo.SuspendLayout()
        CType(Me.DGVTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrabajoestadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TareasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGBAccion
        '
        Me.UGBAccion.Controls.Add(Me.BindingNavigatorTrabajo)
        Me.UGBAccion.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGBAccion.Location = New System.Drawing.Point(0, 0)
        Me.UGBAccion.Name = "UGBAccion"
        Me.UGBAccion.Size = New System.Drawing.Size(957, 64)
        Me.UGBAccion.TabIndex = 4
        Me.UGBAccion.Text = "Acciones"
        '
        'BindingNavigatorTrabajo
        '
        Me.BindingNavigatorTrabajo.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.BindingNavigatorTrabajo.BindingSource = Me.TrabajoBindingSource
        Me.BindingNavigatorTrabajo.CountItem = Me.BindingNavigatorCountItem
        Me.BindingNavigatorTrabajo.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.BindingNavigatorTrabajo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.BindingNavigatorSaveItem, Me.ToolStripSeparator1, Me.TSDDButtonBuscar, Me.ToolStripSeparator2, Me.TSLCodigo, Me.TSTBCodigo, Me.TSButtonCodigo, Me.ToolStripSeparator3, Me.LabelCobrar, Me.TSButtonCobrado, Me.ToolStripSeparator4, Me.LabelLiquidar, Me.TSBLiquidar, Me.ToolStripSeparator5, Me.TSLMatricula, Me.TSTBMatricula, Me.TSButtonMatricula})
        Me.BindingNavigatorTrabajo.Location = New System.Drawing.Point(3, 16)
        Me.BindingNavigatorTrabajo.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.BindingNavigatorTrabajo.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.BindingNavigatorTrabajo.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.BindingNavigatorTrabajo.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.BindingNavigatorTrabajo.Name = "BindingNavigatorTrabajo"
        Me.BindingNavigatorTrabajo.PositionItem = Me.BindingNavigatorPositionItem
        Me.BindingNavigatorTrabajo.Size = New System.Drawing.Size(951, 25)
        Me.BindingNavigatorTrabajo.TabIndex = 0
        Me.BindingNavigatorTrabajo.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'TrabajoBindingSource
        '
        Me.TrabajoBindingSource.DataMember = "trabajo"
        Me.TrabajoBindingSource.DataSource = Me.DSTrabajo
        '
        'DSTrabajo
        '
        Me.DSTrabajo.DataSetName = "DSTrabajo"
        Me.DSTrabajo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(37, 22)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorSaveItem.Image = CType(resources.GetObject("BindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TSDDButtonBuscar
        '
        Me.TSDDButtonBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSDDButtonBuscar.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TodosToolStripMenuItem, Me.GeneradosToolStripMenuItem, Me.PagadosToolStripMenuItem, Me.PresentadosToolStripMenuItem, Me.ConfeccionandoReintegroToolStripMenuItem, Me.ReintegrosToolStripMenuItem, Me.CobradosToolStripMenuItem})
        Me.TSDDButtonBuscar.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.TSDDButtonBuscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSDDButtonBuscar.Name = "TSDDButtonBuscar"
        Me.TSDDButtonBuscar.Size = New System.Drawing.Size(29, 22)
        Me.TSDDButtonBuscar.Text = "ToolStripDropDownButton1"
        Me.TSDDButtonBuscar.ToolTipText = "Buscar por Estados"
        '
        'TodosToolStripMenuItem
        '
        Me.TodosToolStripMenuItem.Name = "TodosToolStripMenuItem"
        Me.TodosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.TodosToolStripMenuItem.Text = "Todos"
        '
        'GeneradosToolStripMenuItem
        '
        Me.GeneradosToolStripMenuItem.Name = "GeneradosToolStripMenuItem"
        Me.GeneradosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.GeneradosToolStripMenuItem.Text = "Generados"
        '
        'PagadosToolStripMenuItem
        '
        Me.PagadosToolStripMenuItem.Name = "PagadosToolStripMenuItem"
        Me.PagadosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.PagadosToolStripMenuItem.Text = "Pagados"
        '
        'PresentadosToolStripMenuItem
        '
        Me.PresentadosToolStripMenuItem.Name = "PresentadosToolStripMenuItem"
        Me.PresentadosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.PresentadosToolStripMenuItem.Text = "Presentados"
        '
        'ConfeccionandoReintegroToolStripMenuItem
        '
        Me.ConfeccionandoReintegroToolStripMenuItem.Name = "ConfeccionandoReintegroToolStripMenuItem"
        Me.ConfeccionandoReintegroToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ConfeccionandoReintegroToolStripMenuItem.Text = "Confeccionando Reintegro"
        '
        'ReintegrosToolStripMenuItem
        '
        Me.ReintegrosToolStripMenuItem.Name = "ReintegrosToolStripMenuItem"
        Me.ReintegrosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ReintegrosToolStripMenuItem.Text = "Reintegros"
        '
        'CobradosToolStripMenuItem
        '
        Me.CobradosToolStripMenuItem.Name = "CobradosToolStripMenuItem"
        Me.CobradosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.CobradosToolStripMenuItem.Text = "Cobrados"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'TSLCodigo
        '
        Me.TSLCodigo.Name = "TSLCodigo"
        Me.TSLCodigo.Size = New System.Drawing.Size(49, 22)
        Me.TSLCodigo.Text = "Código:"
        '
        'TSTBCodigo
        '
        Me.TSTBCodigo.Name = "TSTBCodigo"
        Me.TSTBCodigo.Size = New System.Drawing.Size(70, 25)
        '
        'TSButtonCodigo
        '
        Me.TSButtonCodigo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSButtonCodigo.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.TSButtonCodigo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSButtonCodigo.Name = "TSButtonCodigo"
        Me.TSButtonCodigo.Size = New System.Drawing.Size(23, 22)
        Me.TSButtonCodigo.Text = "Buscar por Código"
        Me.TSButtonCodigo.ToolTipText = "Buscar por Código"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'TSButtonCobrado
        '
        Me.TSButtonCobrado.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSButtonCobrado.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.TSButtonCobrado.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSButtonCobrado.Name = "TSButtonCobrado"
        Me.TSButtonCobrado.Size = New System.Drawing.Size(23, 22)
        Me.TSButtonCobrado.Text = "EstadoCobrado"
        Me.TSButtonCobrado.ToolTipText = "Cambiar estado a Cobrado"
        '
        'LabelLiquidar
        '
        Me.LabelLiquidar.Name = "LabelLiquidar"
        Me.LabelLiquidar.Size = New System.Drawing.Size(53, 22)
        Me.LabelLiquidar.Text = "Liquidar:"
        '
        'TSBLiquidar
        '
        Me.TSBLiquidar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSBLiquidar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Play_32xMD_color
        Me.TSBLiquidar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSBLiquidar.Name = "TSBLiquidar"
        Me.TSBLiquidar.Size = New System.Drawing.Size(23, 22)
        Me.TSBLiquidar.Text = "Liquidar"
        Me.TSBLiquidar.ToolTipText = "Liquidar"
        '
        'TSLMatricula
        '
        Me.TSLMatricula.Name = "TSLMatricula"
        Me.TSLMatricula.Size = New System.Drawing.Size(60, 22)
        Me.TSLMatricula.Text = "Matrícula:"
        '
        'TSTBMatricula
        '
        Me.TSTBMatricula.Name = "TSTBMatricula"
        Me.TSTBMatricula.Size = New System.Drawing.Size(100, 25)
        '
        'TSButtonMatricula
        '
        Me.TSButtonMatricula.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSButtonMatricula.Image = Global.cpceMEGS.My.Resources.Resources.Find_5650
        Me.TSButtonMatricula.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSButtonMatricula.Name = "TSButtonMatricula"
        Me.TSButtonMatricula.Size = New System.Drawing.Size(23, 22)
        Me.TSButtonMatricula.Text = "Buscar por Matricula"
        Me.TSButtonMatricula.ToolTipText = "Buscar por Matricula"
        '
        'UGBTrabajo
        '
        Me.UGBTrabajo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UGBTrabajo.Controls.Add(Me.DGVTrabajo)
        Me.UGBTrabajo.Location = New System.Drawing.Point(0, 64)
        Me.UGBTrabajo.Name = "UGBTrabajo"
        Me.UGBTrabajo.Size = New System.Drawing.Size(957, 306)
        Me.UGBTrabajo.TabIndex = 3
        Me.UGBTrabajo.Text = "Listado de Trabajos"
        '
        'DGVTrabajo
        '
        Me.DGVTrabajo.AllowUserToOrderColumns = True
        Me.DGVTrabajo.AutoGenerateColumns = False
        Me.DGVTrabajo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVTrabajo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.DataGridViewTextBoxColumn8, Me.Estado, Me.MatriculaDataGridViewTextBoxColumn, Me.ProfesionalDataGridViewTextBoxColumn, Me.tra_tarea_id, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn3, Me.IngresosDataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn7})
        Me.DGVTrabajo.DataSource = Me.TrabajoBindingSource
        Me.DGVTrabajo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGVTrabajo.Location = New System.Drawing.Point(3, 16)
        Me.DGVTrabajo.Name = "DGVTrabajo"
        Me.DGVTrabajo.Size = New System.Drawing.Size(951, 287)
        Me.DGVTrabajo.TabIndex = 0
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "tra_nrolegali"
        Me.DataGridViewTextBoxColumn8.HeaderText = "tra_nrolegali"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.DataSource = Me.TrabajoestadoBindingSource
        Me.Estado.DisplayMember = "nombre"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Estado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Estado.ValueMember = "id"
        '
        'TrabajoestadoBindingSource
        '
        Me.TrabajoestadoBindingSource.DataMember = "trabajo_estado"
        Me.TrabajoestadoBindingSource.DataSource = Me.DSTrabajo
        '
        'MatriculaDataGridViewTextBoxColumn
        '
        Me.MatriculaDataGridViewTextBoxColumn.DataPropertyName = "Matricula"
        Me.MatriculaDataGridViewTextBoxColumn.HeaderText = "Matricula"
        Me.MatriculaDataGridViewTextBoxColumn.Name = "MatriculaDataGridViewTextBoxColumn"
        '
        'ProfesionalDataGridViewTextBoxColumn
        '
        Me.ProfesionalDataGridViewTextBoxColumn.DataPropertyName = "Profesional"
        Me.ProfesionalDataGridViewTextBoxColumn.HeaderText = "Profesional"
        Me.ProfesionalDataGridViewTextBoxColumn.Name = "ProfesionalDataGridViewTextBoxColumn"
        '
        'tra_tarea_id
        '
        Me.tra_tarea_id.DataPropertyName = "tra_tarea_id"
        Me.tra_tarea_id.DataSource = Me.TareasBindingSource
        Me.tra_tarea_id.DisplayMember = "Descripcion"
        Me.tra_tarea_id.HeaderText = "Tarea"
        Me.tra_tarea_id.Name = "tra_tarea_id"
        Me.tra_tarea_id.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.tra_tarea_id.ValueMember = "tar_codigo"
        '
        'TareasBindingSource
        '
        Me.TareasBindingSource.DataMember = "tareas"
        Me.TareasBindingSource.DataSource = Me.DSTrabajo
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "tra_comitentecuit"
        Me.DataGridViewTextBoxColumn2.HeaderText = "tra_comitentecuit"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "tra_clientecomitente"
        Me.DataGridViewTextBoxColumn1.HeaderText = "tra_clientecomitente"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "tra_importe1"
        Me.DataGridViewTextBoxColumn3.HeaderText = "tra_importe1"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'IngresosDataGridViewTextBoxColumn
        '
        Me.IngresosDataGridViewTextBoxColumn.DataPropertyName = "Ingresos"
        Me.IngresosDataGridViewTextBoxColumn.HeaderText = "Ingresos"
        Me.IngresosDataGridViewTextBoxColumn.Name = "IngresosDataGridViewTextBoxColumn"
        '
        'MontoDataGridViewTextBoxColumn
        '
        Me.MontoDataGridViewTextBoxColumn.DataPropertyName = "Monto"
        Me.MontoDataGridViewTextBoxColumn.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn.Name = "MontoDataGridViewTextBoxColumn"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "tra_monto_aporte"
        Me.DataGridViewTextBoxColumn6.HeaderText = "tra_monto_aporte"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "tra_monto_iva"
        Me.DataGridViewTextBoxColumn5.HeaderText = "tra_monto_iva"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "tra_honorariominimo"
        Me.DataGridViewTextBoxColumn4.HeaderText = "tra_honorariominimo"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "tra_monto_deposito"
        Me.DataGridViewTextBoxColumn7.HeaderText = "tra_monto_deposito"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        '
        'TrabajoTableAdapter
        '
        Me.TrabajoTableAdapter.ClearBeforeFill = True
        '
        'TareasTableAdapter
        '
        Me.TareasTableAdapter.ClearBeforeFill = True
        '
        'Trabajo_estadoTableAdapter
        '
        Me.Trabajo_estadoTableAdapter.ClearBeforeFill = True
        '
        'LabelCobrar
        '
        Me.LabelCobrar.Name = "LabelCobrar"
        Me.LabelCobrar.Size = New System.Drawing.Size(94, 22)
        Me.LabelCobrar.Text = "Estado Cobrado:"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'FrmTrabajos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(957, 370)
        Me.Controls.Add(Me.UGBAccion)
        Me.Controls.Add(Me.UGBTrabajo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmTrabajos"
        Me.Text = "Trabajos"
        CType(Me.UGBAccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBAccion.ResumeLayout(False)
        Me.UGBAccion.PerformLayout()
        CType(Me.BindingNavigatorTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigatorTrabajo.ResumeLayout(False)
        Me.BindingNavigatorTrabajo.PerformLayout()
        CType(Me.TrabajoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGBTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UGBTrabajo.ResumeLayout(False)
        CType(Me.DGVTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrabajoestadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TareasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TracomitentecuitDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TraclientecomitenteDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TramatriculaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TraprofesionalDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Traimporte1DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Traimporte2DataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TrahonorariominimoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TramontoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TracondicionivaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TracuitDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TracantidadDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TraestadoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TramontoivaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TramontoaporteDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TramontodepositoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TranrolegaliDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UGBAccion As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UGBTrabajo As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents DGVTrabajo As DataGridView
    Friend WithEvents DSTrabajo As DSTrabajo
    Friend WithEvents TrabajoTableAdapter As DSTrabajoTableAdapters.trabajoTableAdapter
    Friend WithEvents TrabajoBindingSource As BindingSource
    Friend WithEvents TareasBindingSource As BindingSource
    Friend WithEvents TareasTableAdapter As DSTrabajoTableAdapters.tareasTableAdapter
    Friend WithEvents TardescripDataGridViewTextBoxColumn As DataGridViewComboBoxColumn
    Friend WithEvents BindingNavigatorTrabajo As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As ToolStripSeparator
    Friend WithEvents BindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents TSDDButtonBuscar As ToolStripDropDownButton
    Friend WithEvents TodosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PagadosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PresentadosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents TSLCodigo As ToolStripLabel
    Friend WithEvents TSTBCodigo As ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents TSButtonCodigo As ToolStripButton
    Friend WithEvents TrabajoestadoBindingSource As BindingSource
    Friend WithEvents Trabajo_estadoTableAdapter As DSTrabajoTableAdapters.trabajo_estadoTableAdapter
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As DataGridViewTextBoxColumn
    Friend WithEvents Estado As DataGridViewComboBoxColumn
    Friend WithEvents MatriculaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProfesionalDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents tra_tarea_id As DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents IngresosDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As DataGridViewTextBoxColumn
    Friend WithEvents TSButtonCobrado As ToolStripButton
    Friend WithEvents TSLMatricula As ToolStripLabel
    Friend WithEvents TSTBMatricula As ToolStripTextBox
    Friend WithEvents TSButtonMatricula As ToolStripButton
    Friend WithEvents CobradosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GeneradosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConfeccionandoReintegroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReintegrosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TSBLiquidar As ToolStripButton
    Friend WithEvents LabelLiquidar As ToolStripLabel
    Friend WithEvents LabelCobrar As ToolStripLabel
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As ToolStripSeparator
End Class
