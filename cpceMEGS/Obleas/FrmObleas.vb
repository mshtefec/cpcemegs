﻿Imports MySql.Data.MySqlClient
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Public Class FrmObleas
    Private cnnMante As New ConsultaBD(True)
    Private DAglobalMysql As MySqlDataAdapter
    Private globalEsAnticipo As Boolean
    Private DTTrabajo As DataTable
    Private BSTrabajo As BindingSource
    Private DTComitente As DataTable = New DataTable
    Private DTProfesionales As DataTable
    Private BSProfesionales As BindingSource
    Private DTProfImpreso As DataTable
    Private BSProfImpreso As BindingSource
    Private RowComitente As DataRow
    Private RowProfesional As DataRow
    Private RowProfImpreso As DataRow
    'Si es Alta true. Si es Editar false
    Private lAltaObleas As Boolean
    'Si es Editar *no esta siendo usado al parecer
    Private lEditarObleas As Boolean
    'Marco recibos en false para controlar si selecciono alguno
    Dim lMarcoRecibos As Boolean = False
    'Marco recibos en false para controlar si selecciono alguno
    Dim lMarcoEjemplar As Boolean = False
    'Si es True es porque es un ejemplar adicional
    Private lObleasEjemplarAdicional As Boolean = False
    Private DTObleas As DataTable
    Private DAObleas As MySqlDataAdapter
    Private cmdObleas As MySqlCommandBuilder
    Private WithEvents BSObleas As BindingSource
    Private DTRecibos As DataTable
    'Private DARecibos As MySqlDataAdapter
    Private BSRecibos As BindingSource
    'Private cmdRecibos As MySqlCommandBuilder
    Private DT As DataTable
    Private nLegaliOriginal As Integer
    Dim xC As Integer
    Private c_condicion As String
    Private CCopias As Integer = 0
    'Trabajos.
    'globalTrabajoId En GrabaObleas() EditarObleas() MuestraDatosObleas() lo seteo en 0 y BuscaTrabajoIngresado()
    Private globalTrabajoId As Integer = 0
    Private globalTrabajoEstado As Integer = 5
    'Si tiene permiso Total en oblea le permito guardar obleas sin seleccionar recibo
    Private lPermiteGuardarSinRecibo As Boolean = False
    Public Sub New(Optional ByVal esLiquidacion As Boolean = False)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        globalEsAnticipo = esLiquidacion
    End Sub

    Private Sub FrmObleas_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Show()
        UDTHasta.Value = Date.Today
        UDTDesde.Value = Date.Today.AddDays(-3)
        'Control Permite editar el texto de las siglas para buscar obleas
        If controlAcceso(5, 5, "R", False) Then
            UTESucursalSiglas.ReadOnly = False
        End If
        If globalEsAnticipo = False Then
            UTESucursalSiglas.Text = cPubSucursalSiglas
        Else
            UTESucursalSiglas.Text = cPubTrabajoAnticipo
        End If
        Try
            UNLegaliAnio.Value = Now.Year
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        MuestraObleas("obl_fecha BETWEEN '" & Format(UDTDesde.Value, "yyyy-MM-dd") & " 00:00:00' AND '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:59'")
        'Si NO encuentra el permiso para: Listar categoria 22 inclusive. Retorna False, Entra y filtra.
        'Lo utilizo para la busqueda de los matriculados
        If controlAcceso(5, 3, , False) = False Then
            c_condicion = "AND MID(afi_categoria,1,2) NOT IN (20,21,22)"
        Else
            c_condicion = ""
        End If
        'Si es admin le permito guardar el trabajo sin marcar el recibo
        If controlAcceso(5, 100, , False) = True Then
            lPermiteGuardarSinRecibo = True
        End If
    End Sub

    Private Function GrabaObleas() As Boolean
        GrabaObleas = False
        Dim nNroLegalizacion As Integer
        'Dim myFecha As MySql.Data.Types.MySqlDateTime
        Dim nNroOblea As Integer = UNEoblea.Value
        Dim cFecCertif As String
        Dim cFecOtorga As String
        Dim cFecTrab As String
        Dim comitente_tipdoc As String = If(RowComitente.IsNull("afi_tipdoc"), "", RowComitente.Item("afi_tipdoc"))
        Dim comitente_nrodoc As Integer = If(RowComitente.IsNull("afi_nrodoc"), 0, RowComitente.Item("afi_nrodoc"))
        'Dim caratula As String = If(IsNothing(TBCaratula.Text), TBCaratula.Text, "")
        If cnnMante.AbrirConexion Then
            Dim miTrans As MySqlTransaction
            miTrans = cnnMante.InicioTransaccion()
            Try
                If UDTfecha.Value Is Nothing Then
                    cFecTrab = "0000-00-00"
                Else
                    cFecTrab = Format(UDTfecha.Value, "yyyy-MM-dd")
                End If
                If UDTCertif.Value Is Nothing Then
                    cFecCertif = "0000-00-00"
                Else
                    cFecCertif = Format(UDTCertif.Value, "yyyy-MM-dd")
                End If
                If UDTotorga.Value Is Nothing Then
                    cFecOtorga = "0000-00-00"
                Else
                    cFecOtorga = Format(UDTotorga.Value, "yyyy-MM-dd")
                End If
                If lAltaObleas Then
                    If globalEsAnticipo Then
                        nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "ANTICI")
                    Else
                        nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "LEGALI")
                    End If
                    nNroLegalizacion = NroLegalizacionLongToSave(nLegaliOriginal, Nothing, globalEsAnticipo)
                    UNElegali.Value = ModPrincipal.NroLegalizacionLongToString(nLegaliOriginal, globalEsAnticipo)
                    'If nLegaliOriginal = UNElegali.Value Then
                    '    nNroLegalizacion = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "LEGALI") & Mid(Now.Year, 3, 2)
                    '    UNElegali.Value = nNroLegalizacion
                    'Else
                    '    nNroLegalizacion = UNElegali.Value
                    'End If
                    UTEbuscaLegali.Value = nLegaliOriginal
                    For nObl As Integer = 0 To CInt(UNCopias.Value) - 1
                        cnnMante.ReplaceBD("INSERT INTO obleas (obl_nrocli,obl_nrolegali,obl_item,obl_nrooblea,obl_fecha,obl_fectrab,obl_tippro,obl_nropro,obl_tipimp,obl_nroimp,obl_tipcte,obl_nrocte,obl_feccert,obl_fecotor,obl_tarea,obl_copias,obl_estado,obl_nroope,obl_ejemplaradicional,obl_caratula) " &
                            "VALUES ('" &
                            nPubNroCli & "','" &
                            nNroLegalizacion & "','" &
                            nObl + 1 & "','" &
                            nNroOblea & "','" &
                            Format(Now.Date, "yyyy-MM-dd HH:mm:ss") & "','" &
                            Format(UDTfecha.Value, "yyyy-MM-dd HH:mm:ss") & "','" &
                            RowProfesional.Item("afi_tipdoc") & "','" &
                            RowProfesional.Item("afi_nrodoc") & "','" &
                            RowProfImpreso.Item("afi_tipdoc") & "','" &
                            RowProfImpreso.Item("afi_nrodoc") & "','" &
                            comitente_tipdoc & "','" &
                            comitente_nrodoc & "','" &
                            cFecCertif & "','" &
                            cFecOtorga & "','" &
                            DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") & "','" &
                            UNCopias.Value & "','" &
                            "" & "','" &
                            nPubNroOperador & "'," &
                            lObleasEjemplarAdicional & ",'" &
                            TBCaratula.Text &
                        "')")
                        nNroOblea += 1
                    Next
                    'BSObleas.Position = DTObleas.Rows.Count
                Else
                    'BuscaProfesional()
                    'CargaRecibos()
                    nNroOblea = 0
                    'nNroLegalizacion = ModPrincipal.NroLegalizacionStringToSave(UNElegali.Value)
                    If GetAnioNroLegalizacion(UNElegali.Value, 2) > 15 Then
                        nNroLegalizacion = NroLegalizacionStringToSave(UNElegali.Value, globalEsAnticipo)
                    Else
                        nNroLegalizacion = UNElegali.Value
                    End If
                    nLegaliOriginal = nNroLegalizacion
                    nNroOblea += UNEoblea.Value
                    'Dim cCondicion = "obl_nrolegali = " & nNroLegalizacion & " AND obl_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:00'"
                    Dim cCondicion = "obl_nrolegali = " & nNroLegalizacion
                    DAObleas = cnnMante.consultaBDadapter(
                        "(((obleas a inner join comitente b on b.afi_tipdoc=a.obl_tipcte and b.afi_nrodoc=a.obl_nrocte) inner join afiliado c on c.afi_tipdoc=a.obl_tippro and c.afi_nrodoc=a.obl_nropro) inner join afiliado e on e.afi_tipdoc=a.obl_tipimp and e.afi_nrodoc=a.obl_nroimp) left join tareas d on d.tar_codigo=a.obl_tarea",
                        "a.obl_fecha as Fecha, a.obl_nrolegali as NroLegalizacion, a.obl_nrooblea as NroOblea,b.afi_nombre as comitente,c.afi_nombre as profesional,e.afi_nombre as Nombre_impreso,a.obl_fectrab as Fectrab,a.obl_feccert as Certificado,a.obl_fecotor as Otorgado,d.tar_descrip as Trabajo,a.obl_copias as copias,a.obl_estado as Est,concat(c.afi_titulo,CAST(c.afi_matricula as char)) as Mtr,concat(e.afi_titulo,CAST(e.afi_matricula as char)) as MtrImp,obl_nrocli,obl_item as item,obl_tipcte,obl_nrocte",
                        cCondicion & " ORDER BY obl_nrolegali desc, obl_item asc"
                    )
                    Dim DTObleasUpdate = New DataTable
                    DAObleas.Fill(DTObleasUpdate)
                    For i As Integer = 0 To DTObleasUpdate.Rows.Count - 1
                        'For i As Integer = BSObleas.Position To DTObleas.Rows.Count - 1
                        'If nNroLegalizacion = DTObleas.Rows(i).Item("NroLegalizacion") Then
                        cnnMante.ReplaceBD("UPDATE obleas SET obl_nrolegali='" & nNroLegalizacion &
                            "',obl_nrooblea='" & UNEoblea.Value &
                            "',obl_tippro='" & RowProfesional.Item("afi_tipdoc") &
                            "',obl_nropro='" & RowProfesional.Item("afi_nrodoc") &
                            "',obl_tipcte='" & comitente_tipdoc &
                            "',obl_nrocte='" & comitente_nrodoc &
                            "',obl_tipimp='" & RowProfImpreso.Item("afi_tipdoc") &
                            "',obl_nroimp='" & RowProfImpreso.Item("afi_nrodoc") &
                            "',obl_tarea='" & DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") &
                            "',obl_fecha='" & Format(Now.Date, "yyyy-MM-dd HH:mm:ss") &
                            "',obl_feccert='" & cFecCertif &
                            "',obl_fecotor='" & cFecOtorga &
                            "',obl_fectrab='" & cFecTrab &
                            "',obl_nrooblea= '" & nNroOblea &
                            "',obl_copias='" & UNCopias.Value &
                            "',obl_caratula='" & TBCaratula.Text &
                            "' WHERE obl_nrocli=" & DTObleasUpdate.Rows(i).Item("obl_nrocli") &
                            " and obl_nrolegali=" & nNroLegalizacion &
                            " and obl_item= " & DTObleasUpdate.Rows(i).Item("item") &
                        "")
                        nNroOblea += 1
                        'Else
                        'Exit For
                        'End If
                    Next
                End If
                'resto 1 al numero de oblea porque no se utiliza la ultima suma al salir del for
                nNroOblea -= 1
                nNroOblea = cnnMante.GetUpdateUltimoNumero("NUMOBLEA", nPubNroIns, nPubNroCli, True, nNroOblea)
                'fin set nuevo numero oblea
                Dim DAComprobaEnRecibo As MySqlDataAdapter
                For Each rowRecibos As DataRow In DTRecibos.Rows
                    Dim DTComprobaEnRecibo = New DataTable()
                    DAComprobaEnRecibo = cnnMante.consultaBDadapter(
                        "comproba inner join totales on tot_nroasi=com_nroasi",
                        "com_fecha as Fecha,com_proceso as Proceso,com_nrocom as Recibo,tot_debe as Importe,tot_nroasi,tot_item,tot_nrocuo,com_tipdoc,com_nrodoc,tot_nrolegali",
                        "com_unegos=2 and com_proceso='02R120' and tot_imppag=0 and tot_debe > 0 and com_tipdoc='" & rowRecibos.Item("com_tipdoc") & "' and com_nrodoc=" & rowRecibos.Item("com_nrodoc") & " and com_estado <> 9 and com_nrocom =" & rowRecibos.Item("Recibo") & " order by com_fecha"
                    )
                    DAComprobaEnRecibo.Fill(DTComprobaEnRecibo)

                    If rowRecibos.Item("Select") Then
                        For Each rowRec As DataRow In DTComprobaEnRecibo.Rows
                            cnnMante.ReplaceBD(
                                "UPDATE totales SET tot_nrolegali='" & nNroLegalizacion & "'" &
                                " WHERE tot_nroasi=" & rowRec.Item("tot_nroasi") & " and tot_item=" & rowRec.Item("tot_item") & " and tot_nrocuo=" & rowRec.Item("tot_nrocuo")
                            )
                            'Si tiene el id del trabajo seteo el nro asiento para actualizar
                            If globalTrabajoId > 0 Then
                                cnnMante.trabajoNroAsiento = rowRec.Item("tot_nroasi")
                            End If
                        Next
                    Else
                        For Each rowRec As DataRow In DTComprobaEnRecibo.Rows
                            cnnMante.ReplaceBD(
                                "UPDATE totales SET tot_nrolegali='" & 0 & "'" &
                                " WHERE tot_nroasi=" & rowRec.Item("tot_nroasi") & " and tot_item=" & rowRec.Item("tot_item") & " and tot_nrocuo=" & rowRec.Item("tot_nrocuo")
                            )
                        Next
                    End If
                Next
                'Si tiene el id del trabajo seteo los valores para actualizar el estado
                If globalTrabajoId > 0 Then
                    cnnMante.trabajoId = globalTrabajoId
                    cnnMante.trabajoEstado = globalTrabajoEstado
                    cnnMante.trabajoNroLegalizacion = nNroLegalizacion
                    cnnMante.CambiarEstadoTrabajo()
                End If
                miTrans.Commit()
                globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
                GrabaObleas = True 'Seteo en true porque guardo correctamente la oblea
            Catch ex As Exception
                miTrans.Rollback()
                cnnMante.CerrarConexion()
                MessageBox.Show(ex.Message)
                CbtAltaOlbeas_Click(CbtAltaOlbeas, Nothing)
            End Try
            cnnMante.CerrarConexion()
        End If
    End Function

    Private Sub BuscaComitente()
        DAObleas = cnnMante.consultaBDadapter(
            "comitente",
            "afi_nombre,afi_tipdoc,afi_nrodoc",
            "afi_tipdoc='" & DTObleas.Rows(BSObleas.Position).Item("obl_tipcte") & "' AND afi_nrodoc=" & DTObleas.Rows(BSObleas.Position).Item("obl_nrocte") & " ORDER BY afi_nombre"
        )
        DTComitente = New DataTable
        DAObleas.Fill(DTComitente)
        '  BSProfesionales.DataSource = DTComitente
        If DTComitente.Rows.Count() > 0 Then
            RowComitente = DTComitente.Rows(0)
        End If
    End Sub

    Private Sub MuestraObleas(ByVal cCondicion As String)
        Try
            'If control true lista las obleas de todas las delegaciones
            If Not controlAcceso(5, 2, "R", False) Then
                'lista obleas de la delegacion logueado
                cCondicion = "obl_nrocli = " & nPubNroCli & " AND " & cCondicion
            End If
            If globalEsAnticipo = True Then
                cCondicion = "obl_tarea = 5 AND " & cCondicion
            End If
            lAltaObleas = False
            EditarObleas(False)

            BSProfesionales = New BindingSource
            BSProfesionales.DataSource = DTProfesionales

            DAObleas = cnnMante.consultaBDadapter("tareas", "tar_descrip, tar_codigo", "tar_activo = 1 ORDER BY tar_orden")
            DTTrabajo = New DataTable
            DAObleas.Fill(DTTrabajo)
            BSTrabajo = New BindingSource
            BSTrabajo.DataSource = DTTrabajo
            UcboTrabajo.DataSource = BSTrabajo
            With UcboTrabajo.DisplayLayout.Bands(0)
                '.Override.FilterUIType = FilterUIType.FilterRow
                .Columns(0).Header.Caption = "Descripcion"
                .Columns(0).Width = 400
                .Columns(1).Header.Caption = "Cod"
                .Columns(1).Width = 40
            End With

            Dim tabla As String = "((((obleas a LEFT JOIN comitente b ON b.afi_tipdoc=a.obl_tipcte AND b.afi_nrodoc=a.obl_nrocte) INNER JOIN afiliado c ON c.afi_tipdoc=a.obl_tippro AND c.afi_nrodoc=a.obl_nropro) INNER JOIN afiliado e ON e.afi_tipdoc=a.obl_tipimp AND e.afi_nrodoc=a.obl_nroimp) LEFT JOIN trabajo ON a.obl_nrolegali = trabajo.tra_nrolegali) LEFT JOIN tareas d ON d.tar_codigo=a.obl_tarea"
            Dim campos As String = "a.obl_fecha as Fecha, a.obl_nrolegali as NroLegalizacion, a.obl_nrooblea as NroOblea,c.afi_nombre as profesional,e.afi_nombre as Nombre_impreso,b.afi_nombre as comitente,obl_caratula AS Caratula,a.obl_fectrab as Fectrab,a.obl_feccert as Certificado,a.obl_fecotor as Otorgado,d.tar_descrip as Trabajo,a.obl_copias as copias,a.obl_estado as Est,concat(c.afi_titulo,CAST(c.afi_matricula as char)) as Mtr,concat(e.afi_titulo,CAST(e.afi_matricula as char)) as MtrImp,obl_nrocli,obl_item as item,obl_tipcte,obl_nrocte,obl_ejemplaradicional AS EjemplarAdicional, trabajo.id AS TrabajoID, trabajo.tra_estado AS TrabajoEstado, trabajo.tra_certificado AS TrabajoCertificado, c.afi_mail AS ProfesionalEmail"
            Dim condiciones As String = cCondicion & " GROUP BY obl_nrolegali ORDER BY obl_nrolegali DESC, obl_item ASC"
            DAObleas = cnnMante.consultaBDadapter(tabla, campos, condiciones)
            cmdObleas = New MySqlCommandBuilder(DAObleas)
            DTObleas = New DataTable
            DAObleas.Fill(DTObleas)
            If DTObleas.Rows.Count > 0 Then
                'Actualizo variable de ejemplares segun el campo de la oblea
                If DTObleas.Rows(0).Item("EjemplarAdicional").ToString() = "1" Then
                    lObleasEjemplarAdicional = True
                End If
                'fin actualizo si o no ejemplar
                BSObleas = New BindingSource
                BSObleas.DataSource = DTObleas
                UGobleas.DataSource = BSObleas
                UGobleas.DisplayLayout.Bands(0).Override.FilterUIType = FilterUIType.FilterRow
                With UGobleas.DisplayLayout.Bands(0)
                    .Columns(0).Width = 80
                    '.Columns(0).CellAppearance.TextHAlign = Infragistics.Win.HAlign.Center
                    .Columns(1).Width = 80
                    .Columns(2).Width = 60
                    .Columns(3).Width = 190
                    .Columns(4).Width = 190
                    .Columns(5).Width = 190
                    .Columns(6).Width = 190
                    .Columns(7).Width = 75
                    .Columns(8).Width = 180
                    .Columns(9).Width = 35
                    .Columns(10).Width = 35
                    .Columns(11).Width = 35
                End With
            Else
                BSObleas = New BindingSource
                BSObleas.DataSource = DTObleas
                UGobleas.DataSource = BSObleas
                MessageBox.Show("Oblea/s no encontrada/s.", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MuestraDatosObleas()
        'If IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoID")) Then
        globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
        'Else
        '    globalTrabajoId = DTObleas.Rows(BSObleas.Position).Item("TrabajoID") 'Seteo el ID del trabajo seleccionado si lo tiene
        'End If
        'Si es mayor a 15 es 2016
        If GetAnioNroLegalizacion(DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion"), 2) > 15 Then
            UNElegali.Value = NroLegalizacionBDToString(DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion"), globalEsAnticipo)
        Else
            UNElegali.Value = DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion")
        End If
        UNEoblea.Value = DTObleas.Rows(BSObleas.Position).Item("NroOblea")
        UDTfecha.Value = DTObleas.Rows(BSObleas.Position).Item("fectrab")
        UDTCertif.Value = DTObleas.Rows(BSObleas.Position).Item("Certificado")
        UDTotorga.Value = DTObleas.Rows(BSObleas.Position).Item("Otorgado")
        UTEComitente.Value = DTObleas.Rows(BSObleas.Position).Item("comitente")
        UTEMtra.Value = DTObleas.Rows(BSObleas.Position).Item("Mtr")
        UTEprofesional.Value = DTObleas.Rows(BSObleas.Position).Item("profesional")
        UTEMtrImp.Value = DTObleas.Rows(BSObleas.Position).Item("MtrImp")
        UTEImpreso.Value = DTObleas.Rows(BSObleas.Position).Item("Nombre_impreso")
        UcboTrabajo.Value = DTObleas.Rows(BSObleas.Position).Item("Trabajo")
        UNCopias.Value = DTObleas.Rows(BSObleas.Position).Item("copias")
        TBCaratula.Text = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("Caratula")), "", DTObleas.Rows(BSObleas.Position).Item("Caratula"))
        If IsDBNull(DTObleas.Rows(BSObleas.Position).Item("EjemplarAdicional")) Then
            UTEjemplarAdicional.Value = "No"
        ElseIf DTObleas.Rows(BSObleas.Position).Item("EjemplarAdicional") <> 1 Then
            UTEjemplarAdicional.Value = "No"
        Else
            UTEjemplarAdicional.Value = "Si"
        End If
        UTEEstado.Text = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoEstado")), "", GetEstadoStringSegunNro(DTObleas.Rows(BSObleas.Position).Item("TrabajoEstado")))
        If IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoCertificado")) Then
            ButtonFinalizar.Enabled = True
            ButtonEntregar.Enabled = False
        ElseIf DTObleas.Rows(BSObleas.Position).Item("TrabajoCertificado") = 1 Then
            ButtonFinalizar.Enabled = False
            ButtonEntregar.Enabled = True
        Else
            ButtonFinalizar.Enabled = False
            ButtonEntregar.Enabled = True
            ButtonEntregar.Text = "Imprimir"
        End If
        UTECertificado.Text = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoCertificado")), "", GetCertificadoStringSegunNro(DTObleas.Rows(BSObleas.Position).Item("TrabajoCertificado")))

        EditarObleas(False)
        BuscaProfesional()
        BuscaProfImpreso()
        BuscaComitente()
        CargaRecibos()
    End Sub

    Private Sub EditarObleas(ByVal lEditar As Boolean)
        globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
        CbtActualizarObleas.Enabled = lEditar
        If lEditar Then
            UNEoblea.Enabled = True
            UDTfecha.Enabled = True
            UDTCertif.Enabled = True
            UDTotorga.Enabled = True
            UTEprofesional.Enabled = True
            UTEImpreso.Enabled = True
            UTEMtrImp.Enabled = True
            UTEComitente.Enabled = True
            UcboTrabajo.Enabled = True
            UTEMtra.Enabled = True
            UNCopias.Enabled = True
            TBCaratula.Enabled = True
        Else
            UNEoblea.Enabled = False
            UDTfecha.Enabled = False
            UDTCertif.Enabled = False
            UDTotorga.Enabled = False
            UTEprofesional.Enabled = False
            UTEMtrImp.Enabled = False
            UTEImpreso.Enabled = False
            UTEComitente.Enabled = False
            UcboTrabajo.Enabled = False
            UTEMtra.Enabled = False
            UNCopias.Enabled = False
            TBCaratula.Enabled = False
        End If
    End Sub

    Private Sub UGobleas_InitializeRow(ByVal sender As Object, ByVal e As InitializeRowEventArgs) Handles UGobleas.InitializeRow
        If e.Row.Cells("Est").Text = "L" Then
            e.Row.Appearance.BackColor = Color.White
            e.Row.Appearance.BackColor2 = Color.Cyan
            e.Row.Appearance.BackGradientStyle = GradientStyle.Vertical
        End If
    End Sub

    Private Sub UTEComitente_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles UTEComitente.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            If Not pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub UTEComitente_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UTEComitente.EditorButtonClick
        BuscoComitente()
    End Sub

    Private Sub UTEComitente_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UTEComitente.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscoComitente()
        End If
    End Sub
    Private Sub BuscoComitente()
        Dim frmComitente As New FrmProfesionales(1, True, UTEComitente.Text)
        If frmComitente.ShowDialog() = DialogResult.OK Then
            DTComitente = frmComitente.GetValues
            RowComitente = DTComitente.Rows(0)
            UTEComitente.Text = RowComitente.Item("afi_nombre")
        End If
    End Sub

    Private Sub UDTHasta_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UDTHasta.EditorButtonClick
        MuestraObleas("obl_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:00'")
    End Sub

    Private Sub UTEbuscaLegali_EditorButtonClick(ByVal sender As Object, ByVal e As UltraWinEditors.EditorButtonEventArgs) Handles UTEbuscaLegali.EditorButtonClick
        MuestraObleas("obl_nrolegali=" & NroLegalizacionLongToSave(UTEbuscaLegali.Value, UNLegaliAnio.Value, globalEsAnticipo))
    End Sub

    Private Sub CbtImprimirObleas_Click(sender As Object, e As EventArgs) Handles CbtImprimirObleas.Click
        'ImprimirObleMatricial()
        ImprimeObleasCR()
    End Sub

    Private Sub ImprimirObleMatricial()
        Dim DAOblea2 As MySqlDataAdapter
        Dim fila As Integer = UGobleas.ActiveRow.VisibleIndex
        If fila >= 0 Then
            'Busco las obleas relacionadas a la legalizacion seleccionada
            DAOblea2 = cnnMante.consultaBDadapter(
                "((obleas a inner join comitente b on b.afi_tipdoc=a.obl_tipcte and b.afi_nrodoc=a.obl_nrocte) inner join afiliado c on c.afi_tipdoc=a.obl_tippro and c.afi_nrodoc=a.obl_nropro) left join tareas d on d.tar_codigo=a.obl_tarea",
                "a.obl_fecha as Fecha, a.obl_nrolegali as NroLegalizacion, a.obl_nrooblea as NroOblea,b.afi_nombre as comitente,c.afi_nombre as profesional,a.obl_fectrab as Fectrabajo,a.obl_feccert as Certificado,a.obl_fecotor as Otorgado,d.tar_descrip as Trabajo,a.obl_copias as copias,a.obl_estado as Est,CAST(c.afi_matricula as char) as Mtr, obl_ejemplaradicional AS Ejemplar",
                "obl_nrolegali = " & DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion") & " "
            )
            cmdObleas = New MySqlCommandBuilder(DAOblea2)
            DT = New DataTable
            DAOblea2.Fill(DT)
        Else
            MessageBox.Show("Debe seleccione una Legalizacion ", "IMPRIMIR OBLEAS", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If DT.Rows.Count > 0 Then
            CCopias = UNCopias.Value ' UNCopias.SelectedItem
            If ppdDialog.ShowDialog = DialogResult.OK Then
                ppd.PrinterSettings.PrinterName = ppdDialog.PrinterSettings.PrinterName
                ppd.PrinterSettings.Copies = ppdDialog.PrinterSettings.Copies

                Dim prtPrev As New PrintPreviewDialog
                'printDoc.PrinterSettings.PrinterName = RowFarmacia.Item("far_controlador1")
                prtPrev.Document = ppd

                prtPrev.Text = "Previsualizar documento"
                prtPrev.ShowIcon = False
                prtPrev.PrintPreviewControl.Zoom = 0.9
                prtPrev.Height = 600
                prtPrev.StartPosition = FormStartPosition.CenterScreen
                prtPrev.ShowDialog()
                'Inicializo las Variables
                'a ojo saque estos valores
                xC = 0
                ' ppd.Print()
            End If
        End If
    End Sub

    Private Sub ppd_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles ppd.PrintPage
        Dim PosProfX, PosProfY, PosMtrX, PosMtrY, PosLegX, PosLegY, PosTraX, PosTraY, PosComX, PosComY, PosLugarX, PosLugarY As Double
        'Inicializo las Variables
        'a ojo saque estos valores
        PosProfX = 140
        PosProfY = 138
        PosMtrX = 140
        PosMtrY = 153
        PosLegX = 190
        PosLegY = 170
        PosTraX = 140
        PosTraY = 187
        PosComX = 180
        PosComY = 207
        PosLugarX = 140
        PosLugarY = 235
        ' PARA LASER -------
        'PosProfX = 165
        'PosProfY = 145
        'PosMtrX = 170
        'PosMtrY = 163
        'PosLegX = 215
        'PosLegY = 180
        'PosTraX = 165
        'PosTraY = 192
        'PosComX = 205
        'PosComY = 212
        'PosLugarX = 165
        'PosLugarY = 240
        '---------------------------------------------------------------------------------------------------
        'OBTENGO LA LOCALIDAD GUARDADA EN FARCAT PARA IMPRIMIR EN LA OBLEA
        '---------------------------------------------------------------------------------------------------
        'Dim DAFarcat As MySqlDataAdapter
        'Dim DTFarcat As DataTable
        Dim Lugar As String = ""

        'DAFarcat = cnnMante.consultaBDadapter("farcat", , " far_activo = " & nPubNroCli)
        'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
        'DTFarcat = New DataTable
        'DAFarcat.Fill(DTFarcat)
        Try
            'La fuente a usar
            Dim prFont As Font
            'Guardo la cantidad de copias a hacer
            Dim I As Integer = 1
            ' For Each row In DT.Rows
            Dim row As DataRow = DT.Rows(0)
            Dim cLegalizacion As String = Mid(row("NroLegalizacion"), 1, row("NroLegalizacion").ToString.Length - 2) & "/" & Mid(row("NroLegalizacion"), row("NroLegalizacion").ToString.Length - 1, 2)
            For xP As Integer = 0 To CCopias - 1
                Lugar = cPubSucursal & "   " & Mid(row("Fecha").ToString, 1, 10)
                prFont = New Font("Arial", 12, FontStyle.Regular)
                'Lugar = String.Empty
                e.Graphics.DrawString(row("profesional"), prFont, Brushes.Black, PosProfX, PosProfY)
                prFont = New Font("Arial", 10, FontStyle.Regular)
                e.Graphics.DrawString(row("Mtr"), prFont, Brushes.Black, PosMtrX, PosMtrY)
                e.Graphics.DrawString(cLegalizacion, prFont, Brushes.Black, PosLegX, PosLegY)
                'prFont = New Font("Microsoft Sans Serif", 10, FontStyle.Regular)
                e.Graphics.DrawString(row("Trabajo") & " al " & row("fectrabajo").ToString, prFont, Brushes.Black, PosTraX, PosTraY)
                e.Graphics.DrawString(row("comitente"), prFont, Brushes.Black, PosComX, PosComY)
                'e.Graphics.DrawString(row("Lugar"), prFont, Brushes.Black, PosLugarX, PosLugarY)
                e.Graphics.DrawString(Lugar, prFont, Brushes.Black, PosLugarX, PosLugarY)
                PosProfY += 300 ' * I
                PosMtrY += 300 '* I
                PosLegY += 300 '* I
                PosTraY += 300 '* I
                PosComY += 300 '* I
                PosLugarY += 300 '* I
            Next
            xC += 1

            Dim nPag As Integer
            If CCopias > 4 Then
                nPag = CCopias Mod 4 + 1
            Else
                nPag = 0
            End If

            If xC > nPag Then
                e.HasMorePages = False
            Else
                CCopias -= 4
                e.HasMorePages = True
            End If
            'indicamos que hemos llegado al final de la pagina
            'e.HasMorePages = False
        Catch ex As Exception
            e.Cancel = True
            MessageBox.Show("ERROR : " & ex.Message, "IMPRESION DE OBLEAS ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub generarNumeroLegalizacion()

    End Sub
    Private Sub CbtAltaOlbeas_Click(sender As Object, e As EventArgs) Handles CbtAltaOlbeas.Click
        globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
        Dim myFecha As MySql.Data.Types.MySqlDateTime = getMySqlDateNow()
        'Dim rowOblAlta As DataRow = DTObleas.NewRow
        lAltaObleas = True
        'Set en false obleas ejemplar adicional porque es un alta nuevo
        lObleasEjemplarAdicional = False
        'Si no se pasa lActualizaCorrelatividad en False, por defecto actualiza el numero de legalizacion
        'Dim nowYear As String = Mid(Now.Year, 3, 2)
        If globalEsAnticipo Then
            nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "ANTICI", False)
        Else
            nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "LEGALI", False)
        End If
        UNElegali.Value = ModPrincipal.NroLegalizacionLongToString(nLegaliOriginal, globalEsAnticipo)
        nLegaliOriginal = NroLegalizacionLongToSave(nLegaliOriginal, Nothing, globalEsAnticipo)
        If globalEsAnticipo Then
            UNEoblea.Value = 0
        Else
            UNEoblea.Value = cnnMante.GetUpdateUltimoNumero("NUMOBLEA", nPubNroIns, nPubNroCli)
        End If
        UTEComitente.Text = ""
        UTEprofesional.Text = ""
        UTEImpreso.Text = ""
        UTEMtrImp.Text = ""
        UcboTrabajo.Text = ""
        TBCaratula.Text = "" 'Esto no funciona
        UDTfecha.Value = ""
        UDTCertif.Value = ""
        UDTotorga.Value = myFecha
        UTEMtra.Value = ""
        UNCopias.Value = 1
        UTEjemplarAdicional.Value = 0
        EditarObleas(True)
        If DTRecibos IsNot Nothing Then
            DTRecibos.Clear()
        End If
        If DTProfesionales IsNot Nothing Then
            DTProfesionales.Clear()
        End If
        If DTProfImpreso IsNot Nothing Then
            DTProfImpreso.Clear()
        End If
        If DTComitente IsNot Nothing Then
            DTComitente.Clear()
        End If
        'CargaRecibos()
        UNEoblea.Focus()
    End Sub

    Private Function getMySqlDateNow() As MySql.Data.Types.MySqlDateTime
        getMySqlDateNow.Day = Date.Now.Day
        getMySqlDateNow.Month = Date.Now.Month
        getMySqlDateNow.Year = Date.Now.Year
        getMySqlDateNow.Hour = Date.Now.Hour
        getMySqlDateNow.Minute = Date.Now.Minute
        getMySqlDateNow.Second = Date.Now.Second
    End Function
    Private Sub CbtActualizarObleas_Click(sender As Object, e As EventArgs) Handles CbtActualizarObleas.Click
        lMarcoRecibos = False 'Marco recibo lo seteo en False siempre al entrar al metodo Actualizar.
        If UcboTrabajo.ActiveRow Is Nothing Then
            MessageBox.Show("Falta cargar el Trabajo", "Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        Select Case DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo")
            'Controlo los 2 campos. Comitente y Caratula segun trabajo.
            Case 15, 16
                'Consultoria Tecnica requiere caratula
                If TBCaratula.Text Is Nothing Or TBCaratula.Text = "" Then
                    MessageBox.Show("Falta cargar la caratula", "Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
                UTEComitente.Text = ""
            Case 25
                'Certificacion firma solo requiere profesional
                UTEComitente.Text = ""
                TBCaratula.Text = ""
            Case Else
                'Otros trabajos requieren comitente
                If DTComitente Is Nothing Or DTComitente.Rows.Count = 0 Then
                    MessageBox.Show("Falta cargar el comitente", "Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
        End Select
        If IsNothing(DTProfesionales) OrElse DTProfesionales.Rows.Count = 0 Then
            MessageBox.Show("Falta cargar el Profesional", "Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If IsNothing(DTProfImpreso) OrElse DTProfImpreso.Rows.Count = 0 Then
            MessageBox.Show("Falta cargar el Profesional que sale impreso en la oblea", "Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        ' PERICIAS NO NECESITA RECIBOS
        If DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") = 16 Or
            DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") = 18 Or
            DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") = 25 Or
            DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo") = 32 Then
            For Each rowHon As DataRow In DTRecibos.Rows
                If rowHon.Item("Select") Then
                    MessageBox.Show("No debe Marcar el o los recibos de honorarios a reintegrar", "Alta Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            Next
            lMarcoRecibos = True 'Si no marco recibo pongo en True
        Else
            'Si es True permite guardar sin recibo
            If lPermiteGuardarSinRecibo Then
                lMarcoRecibos = True 'Si marco recibo pongo en True
            Else
                For Each rowHon As DataRow In DTRecibos.Rows
                    If rowHon.Item("Select") Then
                        lMarcoRecibos = True 'Si marco recibo pongo en True
                    End If
                Next
            End If
            If (BSObleas.Count() > 0) AndAlso DTObleas.Rows(BSObleas.Position).Item("est").ToString() = "L" And controlAcceso(5, 4, "U", False) Then
                lMarcoRecibos = True 'Si esta editando un trabajo ya liquidado y tine permisos, marco recibo pongo en True
            End If
        End If
        If lMarcoRecibos Or lMarcoEjemplar Then
            If GrabaObleas() Then
                If lAltaObleas Then
                    MuestraObleas("obl_nrolegali=" & NroLegalizacionLongToSave(nLegaliOriginal, Nothing, globalEsAnticipo))
                Else
                    MuestraObleas("obl_nrolegali=" & nLegaliOriginal)
                End If
                'Muestro mensaje para confirmar la impresion de las obleas
                If MessageBox.Show("Desea imprimir las obleas???", "Impresión de Obleas:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    'Yes continua. Imprime directamente las obleas.
                    ImprimeObleasCR()
                End If
            Else
                MessageBox.Show("Ocurrió un error al Actualizar. Intente nuevamente.", "Alta Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Debe Marcar el o los recibos de honorarios a reintegrar", "Alta Obleas", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        'MuestraObleas("obl_fecha between '" & Format(UDTDesde.Value, "yyyy-MM-dd") & "' and '" & Format(UDTHasta.Value, "yyyy-MM-dd") & " 23:59:00'")
    End Sub
    'Private Sub UGobleas_ClickCell(ByVal sender As Object, ByVal e As Infragistics.Win.UltraWinGrid.ClickCellEventArgs) Handles UGobleas.ClickCell
    '    lAltaObleas = False
    '    MuestraDatosObleas()
    'End Sub
    Private Sub BSObleas_PositionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles BSObleas.PositionChanged
        lAltaObleas = False
        MuestraDatosObleas()
    End Sub

    Private Sub CargaRecibos(Optional ByVal trabajoNroAsiento As Long = 0)
        'Dim lmuestraRecibos As Boolean = False
        DTRecibos = New DataTable
        'si no fue liquidado muestro todos los recibos pendientes
        'If Not IsNothing(BSObleas) AndAlso BSObleas.Position > -1 Then
        'Filtro tambien por tot_fecha > '2015-07-01'
        If lAltaObleas Then
            Dim condiciones As String = "com_unegos=2 AND com_proceso='02R120' AND tot_imppag=0 AND tot_debe > 0 AND com_tipdoc='" & RowProfesional.Item("afi_tipdoc") & "' AND com_nrodoc=" & RowProfesional.Item("afi_nrodoc") & " AND com_estado <> 9 AND tot_nrolegali=0 AND com_fecha > '2015-07-01'"
            'Si no esta vacio el numero de asiento del trabajo lo busco directamente al recibo
            If trabajoNroAsiento > 0 Then
                condiciones &= " AND com_nroasi = '" & trabajoNroAsiento & "'"
            End If

            DAglobalMysql = cnnMante.consultaBDadapter(
                "comproba inner join totales on tot_nroasi=com_nroasi and tot_unegos=com_unegos and tot_nrocli=com_nrocli",
                "com_fecha as Fecha,com_proceso as Proceso,com_nrocom as Recibo,SUM(tot_debe) as Importe,com_titulo AS Ti,com_matricula AS Matr,tot_nroasi,tot_item,tot_nrocuo,com_tipdoc,com_nrodoc,tot_nrolegali",
                condiciones & " GROUP BY Recibo ORDER BY com_fecha DESC"
            )
        ElseIf Not IsNothing(BSObleas) AndAlso BSObleas.Position > -1 Then
            'Es Show o Edit
            DAglobalMysql = cnnMante.consultaBDadapter(
                "comproba inner join totales on tot_nroasi=com_nroasi and tot_unegos=com_unegos and tot_nrocli=com_nrocli",
                "com_fecha as Fecha,com_proceso as Proceso,com_nrocom as Recibo,SUM(tot_debe) as Importe,com_titulo AS Ti,com_matricula AS Matr,tot_nroasi,tot_item,tot_nrocuo,com_tipdoc,com_nrodoc,tot_nrolegali",
                "com_unegos=2 and com_proceso='02R120' AND tot_debe > 0 AND com_tipdoc='" & RowProfesional.Item("afi_tipdoc") & "' AND com_nrodoc=" & RowProfesional.Item("afi_nrodoc") & " AND com_estado <> 9 AND tot_fecha > '2015-07-01' AND tot_nrolegali in (0," & DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion") & ") group by Recibo order by com_fecha desc"
            )
        End If
        'lmuestraRecibos = True
        'Else 'creo que no entra aca.
        '    If lAltaObleas Then
        '        'no muestra los anulados
        '        DAglobalMysql = cnnMante.consultaBDadapter(
        '            "comproba inner join totales on tot_nroasi=com_nroasi and tot_unegos=com_unegos and tot_nrocli=com_nrocli",
        '            "com_fecha as Fecha,com_proceso as Proceso,com_nrocom as Recibo,SUM(tot_debe) as Importe,com_titulo AS Ti,com_matricula AS Matr,tot_nroasi,tot_item,tot_nrocuo,com_tipdoc,com_nrodoc,tot_nrolegali",
        '            "com_unegos=2 and com_proceso='02R120' and tot_imppag=0 and tot_debe > 0 and com_tipdoc='" & RowProfesional.Item("afi_tipdoc") & "' and com_nrodoc=" & RowProfesional.Item("afi_nrodoc") & " and com_estado <> 9 group by Recibo order by com_fecha desc"
        '        )
        '        lmuestraRecibos = True
        '    End If
        'End If
        DAglobalMysql.Fill(DTRecibos)

        If DTRecibos.Rows.Count > 0 Then
            DTRecibos.Columns.Add("Select", GetType(Boolean))
            'cmdRecibos = New MySqlCommandBuilder(DAglobalMysql)
            BSRecibos = New BindingSource
            BSRecibos.DataSource = DTRecibos

            If trabajoNroAsiento = 0 Then 'Si esta vacio el numero de asiento en el trabajo entra
                For Each rowHon As DataRow In DTRecibos.Rows
                    If rowHon.Item("tot_nrolegali") = 0 Then
                        rowHon.Item("Select") = False
                    Else
                        rowHon.Item("Select") = True
                    End If
                Next
            Else 'Si el trabajo tiene un numero de asiento entra y lo selecciona
                DTRecibos.Rows(0).Item("Select") = True
            End If

            With UGRecibos
                .DataSource = BSRecibos
                '.DisplayLayout.Bands(0).Columns(0).Header.Caption = "Comitente"
                .DisplayLayout.Bands(0).Columns(0).Width = 80
                .DisplayLayout.Bands(0).Columns(1).Width = 60
                '.DisplayLayout.Bands(0).Columns(1).Header.Caption = "Numero"
                .DisplayLayout.Bands(0).Columns(2).Width = 55
                '.DisplayLayout.Bands(0).Columns(2).Hidden = True
                .DisplayLayout.Bands(0).Columns(3).Format = "c"
                .DisplayLayout.Bands(0).Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .DisplayLayout.Bands(0).Columns(3).Width = 80
                .DisplayLayout.Bands(0).Columns(4).Width = 30
                .DisplayLayout.Bands(0).Columns(5).Width = 45
                .DisplayLayout.Bands(0).Columns(6).Hidden = True
                .DisplayLayout.Bands(0).Columns(7).Hidden = True
                .DisplayLayout.Bands(0).Columns(8).Hidden = True
                .DisplayLayout.Bands(0).Columns(9).Hidden = True
                .DisplayLayout.Bands(0).Columns(10).Hidden = True
                .DisplayLayout.Bands(0).Columns(11).Hidden = True
            End With
        Else
            BSRecibos = New BindingSource
            BSRecibos.DataSource = DTRecibos
            With UGRecibos
                .DataSource = BSRecibos
                '.DisplayLayout.Bands(0).Columns(0).Header.Caption = "Comitente"
                .DisplayLayout.Bands(0).Columns(0).Width = 80
                .DisplayLayout.Bands(0).Columns(1).Width = 60
                '.DisplayLayout.Bands(0).Columns(1).Header.Caption = "Numero"
                .DisplayLayout.Bands(0).Columns(2).Width = 55
                '.DisplayLayout.Bands(0).Columns(2).Hidden = True
                .DisplayLayout.Bands(0).Columns(3).Format = "c"
                .DisplayLayout.Bands(0).Columns(3).CellAppearance.TextHAlign = HAlign.Right
                .DisplayLayout.Bands(0).Columns(3).Width = 80
                .DisplayLayout.Bands(0).Columns(4).Width = 30
                .DisplayLayout.Bands(0).Columns(5).Width = 45
                .DisplayLayout.Bands(0).Columns(6).Hidden = True
                .DisplayLayout.Bands(0).Columns(7).Hidden = True
                .DisplayLayout.Bands(0).Columns(8).Hidden = True
                .DisplayLayout.Bands(0).Columns(9).Hidden = True
                .DisplayLayout.Bands(0).Columns(10).Hidden = True
                .DisplayLayout.Bands(0).Columns(11).Hidden = True
            End With
        End If
    End Sub

    Private Sub UTEMtra_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UTEMtra.KeyDown
        If e.KeyCode = Keys.Enter Then
            UTEMtraPressEnter()
        End If
    End Sub
    Private Sub UTEMtraPressEnter(Optional ByVal trabajoNroAsiento As Long = 0)
        If UTEMtra.Text.Length > 2 Then
            BuscaProfesional()
            If DTProfesionales.Rows.Count = 1 Then
                UTEprofesional.Text = DTProfesionales.Rows(BSProfesionales.Position).Item("afi_nombre")
                CargaRecibos(trabajoNroAsiento)
            End If
        End If
    End Sub
    Private Sub UTEMtrImp_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles UTEMtrImp.KeyDown
        If e.KeyCode = Keys.Enter Then
            UTEMtrImpPressEnter()
        End If
    End Sub
    Private Sub UTEMtrImpPressEnter()
        If UTEMtrImp.Text.Length > 2 Then
            BuscaProfImpreso()
            If DTProfImpreso.Rows.Count = 1 Then
                UTEImpreso.Text = DTProfImpreso.Rows(BSProfImpreso.Position).Item("afi_nombre")
                'CargaRecibos()
            End If
        End If
    End Sub
    Private Sub BuscaProfesional()
        DAObleas = cnnMante.consultaBDadapter(
            "afiliado LEFT JOIN categorias ON cat_codigo=afi_categoria",
            "afi_nombre,afi_tipdoc,afi_nrodoc,afi_titulo,afi_matricula",
            "afi_titulo='" & UTEMtra.Text.Substring(0, 2) & "' AND afi_matricula=" & UTEMtra.Text.Substring(2) & " " & c_condicion
        )
        DTProfesionales = New DataTable
        DAObleas.Fill(DTProfesionales)
        If DTProfesionales.Rows.Count > 0 Then
            BSProfesionales.DataSource = DTProfesionales
            RowProfesional = DTProfesionales.Rows(0)
        Else
            MessageBox.Show("No se encuentra matricula o esta deshabilitado", "PROFESIONAL OBLEAS", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub BuscaProfImpreso()
        Try
            DAObleas = cnnMante.consultaBDadapter(
                "afiliado LEFT JOIN categorias ON cat_codigo=afi_categoria",
                "afi_nombre,afi_tipdoc,afi_nrodoc,afi_titulo,afi_matricula",
                "afi_titulo='" & UTEMtrImp.Text.Substring(0, 2) & "' AND afi_matricula=" & UTEMtrImp.Text.Substring(2) & " " & c_condicion
            )
            DTProfImpreso = New DataTable
            DAObleas.Fill(DTProfImpreso)
            If DTProfImpreso.Rows.Count > 0 Then
                BSProfImpreso = New BindingSource
                BSProfImpreso.DataSource = DTProfImpreso
                RowProfImpreso = DTProfImpreso.Rows(0)
            Else
                MessageBox.Show("No se encuentra matricula o esta deshabilitado", "PROFESIONAL IMPRESION OBLEAS", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message())
        End Try
    End Sub
    Private Sub CBtEditar_Click(sender As Object, e As EventArgs) Handles CBtEditar.Click
        If controlAcceso(5, 0, "U", True) Then
            If Not IsNothing(BSObleas) AndAlso DTObleas.Rows(BSObleas.Position).Item("est").ToString() <> "L" Or controlAcceso(5, 4, "U", False) Then
                EditarObleas(True)
            End If
        End If
    End Sub
    Private Sub ImprimeObleasCR(Optional ByVal imprimirCRObleasRecibo As Boolean = False)
        Dim DAOblea2 As MySqlDataAdapter
        Dim fila As Integer = UGobleas.ActiveRow.VisibleIndex
        If fila >= 0 Then
            'Busco las obleas relacionadas a la legalizacion seleccionada
            DAOblea2 = cnnMante.consultaBDadapter(
                "((obleas a LEFT JOIN comitente b ON b.afi_tipdoc=a.obl_tipcte AND b.afi_nrodoc=a.obl_nrocte) INNER JOIN afiliado c ON c.afi_tipdoc=a.obl_tipimp AND c.afi_nrodoc=a.obl_nroimp) LEFT JOIN tareas d ON d.tar_codigo=a.obl_tarea LEFT JOIN trabajo t ON a.obl_nrolegali=t.tra_nrolegali",
                "a.obl_fecha as Fecha, a.obl_nrolegali as NroLegalizacion, a.obl_nrooblea as NroOblea,b.afi_nombre as comitente,c.afi_nombre as profesional,a.obl_fectrab as Fectrabajo,a.obl_feccert as Certificado,a.obl_fecotor as Otorgado,d.tar_descrip as Trabajo,a.obl_copias as copias,a.obl_estado as Est,CAST(c.afi_matricula as char) as Mtr, obl_ejemplaradicional AS Ejemplar, obl_caratula AS Caratula, t.id AS idCodigoBarra",
                "obl_nrolegali = " & DTObleas.Rows(BSObleas.Position).Item("NroLegalizacion") & " "
            )
            cmdObleas = New MySqlCommandBuilder(DAOblea2)
            DT = New DataTable
            DAOblea2.Fill(DT)
        Else
            MessageBox.Show("Debe seleccionar una Legalizacion ", "IMPRIMIR OBLEAS", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        '---------------------------------------------------------------------------------------------------
        'OBTENGO LA LOCALIDAD GUARDADA EN FARCAT PARA IMPRIMIR EN LA OBLEA
        '---------------------------------------------------------------------------------------------------
        Dim DTObleasImp As DataTable
        DTObleasImp = New DataTable("obleas")
        With DTObleasImp
            .Columns.Add("matricula", Type.GetType("System.String"))
            .Columns.Add("profesional", Type.GetType("System.String"))
            .Columns.Add("legalizacion", Type.GetType("System.String"))
            .Columns.Add("comitente", Type.GetType("System.String"))
            .Columns.Add("trabajo", Type.GetType("System.String"))
            .Columns.Add("lugar", Type.GetType("System.String"))
            .Columns.Add("codigo", Type.GetType("System.String"))
        End With
        'Dim DAFarcat As MySqlDataAdapter
        'Dim DTFarcat As DataTable
        Dim Lugar As String = ""

        'DAFarcat = cnnMante.consultaBDadapter("farcat", , " far_activo = " & nPubNroCli)
        'Dim cmbFarcat As New MySqlCommandBuilder(DAFarcat)
        'DTFarcat = New DataTable
        'DAFarcat.Fill(DTFarcat)
        Try
            Dim I As Integer = 1
            Dim row As DataRow = DT.Rows(0)
            'Si es mayor a 15 es 2016
            Dim cLegalizacion As String
            If GetAnioNroLegalizacion(row("NroLegalizacion"), 2) > 15 Then
                cLegalizacion = NroLegalizacionBDToString(row("NroLegalizacion"), globalEsAnticipo)
            Else
                cLegalizacion = row("NroLegalizacion")
            End If
            Dim mydr As DataRow
            For xP As Integer = 0 To UNCopias.Value - 1
                If imprimirCRObleasRecibo Then
                    Lugar = cPubSucursal
                Else
                    Lugar = cPubSucursal & "   " & Mid(row("Otorgado").ToString, 1, 10)
                End If
                mydr = DTObleasImp.NewRow()
                mydr("matricula") = row("Mtr")
                mydr("profesional") = row("Profesional")
                mydr("legalizacion") = cLegalizacion
                mydr("lugar") = Lugar
                If (row("comitente").ToString() = "") Or (row("comitente").ToString() = "SIN DEFINIR") Then
                    'Muestro solo Caratula
                    mydr("comitente") = row("Caratula")
                Else
                    'Si caratula esta vacio muestro solo Comitente sino muestro Comitente (Caratula)
                    mydr("comitente") = If(((row("Caratula").ToString() = "")), row("comitente"), row("comitente") & " (" & row("Caratula") & ")")
                    'mydr("comitente") = If(((row("comitente").ToString() = "") Or (row("comitente").ToString() = "SIN DEFINIR")), row("Caratula"), row("comitente") & " (" & row("Caratula") & ")")
                End If
                'si no es null entra
                If Not IsDBNull(row("Ejemplar")) Then
                    'si es true entra
                    If row("Ejemplar") Then
                        'agrego leyenda (ejemplar adicional)
                        mydr("trabajo") = row("Trabajo") & " al " & row("fectrabajo").ToString & " (ejemplar adicional)"
                    Else
                        mydr("trabajo") = row("Trabajo") & " al " & row("fectrabajo").ToString
                    End If
                Else
                    mydr("trabajo") = row("Trabajo") & " al " & row("fectrabajo").ToString
                End If
                'si es null entra
                If IsDBNull(row("idCodigoBarra")) Then
                    mydr("codigo") = ""
                Else
                    mydr("codigo") = row("idCodigoBarra")
                End If
                DTObleasImp.Rows.Add(mydr)
                If imprimirCRObleasRecibo Then
                    Exit For
                End If
            Next
            If imprimirCRObleasRecibo Then
                'Si imprimirCRObleasRecibo entra porque se presiono el boton Entregado
                'Muestra formulario para la impresion (utilizo CRObleasReciboA5.rpt)
                Dim FrmImprimir As New FrmReportes(DTObleasImp, , "CRObleasReciboA5.rpt", "Impresión Entrega Trabajo")
                'Muestra formulario para la impresion (utilizo el numero delegacion para el nombre del reporte)
                'Dim FrmImprimir As New FrmReportes(DTObleasImp, , "CRObleas" & nPubNroCli & ".rpt", "Impresión Obleas")
                FrmImprimir.ShowDialog()
            Else
                'Sino imprime la oblea normalmente porque presiono boton Imprimir Obleas
                If cPubSucursalImpresion = 1 Then
                    'Imresion directa del reporte de las obleas
                    Dim impresionDirecta As New ImpresionDirecta(DTObleasImp, , "CRObleas.rpt", pubImpresoraNombreObleas, pubImpresoraPaginaObleas)
                    impresionDirecta.Imprimir()
                Else
                    'Muestra formulario para la impresion (utilizo CRObleas2.rpt)
                    Dim FrmImprimir As New FrmReportes(DTObleasImp, , "CRObleas2.rpt", "Impresión Obleas")
                    'Muestra formulario para la impresion (utilizo el numero delegacion para el nombre del reporte)
                    'Dim FrmImprimir As New FrmReportes(DTObleasImp, , "CRObleas" & nPubNroCli & ".rpt", "Impresión Obleas")
                    FrmImprimir.ShowDialog()
                End If
                CbtAltaOlbeas_Click(CbtAltaOlbeas, Nothing)
            End If
        Catch ex As Exception
            MessageBox.Show("ERROR :  " & ex.Message, "IMPRESION DE OBLEAS ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            CbtAltaOlbeas_Click(CbtAltaOlbeas, Nothing)
        End Try
    End Sub

    Private Sub CBtExportar_Click(ByVal Sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CBtExportar.Click
        Dim clsExportarExcel As New ExportarExcel
        Dim cListado(0) As String
        cListado(0) = "Listado de Obleas"
        clsExportarExcel.ExportarDatosExcel(UGobleas, cListado)
    End Sub
    Private Sub CBEjemplar_Click(sender As Object, e As EventArgs) Handles CBEjemplar.Click
        Dim numeroLegalizacionMessage As String = UNElegali.Value
        'Confirmar la generacion del ejemplar
        Dim message = "EJEMPLAR ADICIONAL" & vbCrLf &
            "Nro. de Legalización: " & numeroLegalizacionMessage & vbCrLf &
            "Esta seguro?"
        If MessageBox.Show(message, "Confirmar la generación:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim myFecha As MySql.Data.Types.MySqlDateTime = getMySqlDateNow()
            'Si no se pasa lActualizaCorrelatividad en False, por defecto actualiza el numero de legalizacion
            If globalEsAnticipo Then
                nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "ANTICI", False)
                UNEoblea.Value = 0
            Else
                nLegaliOriginal = cnnMante.TomaCobte(nPubNroIns, nPubNroCli, "LEGALI", False)
                UNEoblea.Value = cnnMante.GetUpdateUltimoNumero("NUMOBLEA", nPubNroIns, nPubNroCli)
            End If

            UNElegali.Value = ModPrincipal.NroLegalizacionLongToString(nLegaliOriginal, globalEsAnticipo)
            nLegaliOriginal = NroLegalizacionLongToSave(nLegaliOriginal, Nothing, globalEsAnticipo)
            'If globalEsAnticipo Then
            '    UNEoblea.Value = 0
            'Else
            '    UNEoblea.Value = cnnMante.GetUpdateUltimoNumero("NUMOBLEA", nPubNroIns, nPubNroCli)
            'End If
            UDTotorga.Value = myFecha
            EditarObleas(True)
            UNEoblea.Focus()
            DTRecibos.Clear()

            'Yes continua. Seteo true para que entre al insert en generar oblea
            lAltaObleas = True
            lObleasEjemplarAdicional = True
        Else
            Exit Sub
        End If
        'Para que no controle si selecciono algun recibo seteo true
        lMarcoEjemplar = True
        CbtActualizarObleas_Click(CbtActualizarObleas, Nothing)
        lMarcoEjemplar = False
    End Sub

    Private Sub UcboTrabajo_ValueChanged(sender As Object, e As EventArgs) Handles UcboTrabajo.ValueChanged
        If UcboTrabajo.Text <> "" And Not IsNothing(UcboTrabajo.SelectedRow) Then
            LabelCaratula.Text = "Cliente:"
            LabelComitente.Show()
            LabelCaratula.Show()
            UTEComitente.Show()
            TBCaratula.Show()
            Select Case DTTrabajo.Rows(UcboTrabajo.SelectedRow.Index).Item("tar_codigo")
                Case 15, 16
                    LabelCaratula.Text = "Caratula:"
                    LabelComitente.Hide()
                    UTEComitente.Hide()
                    RowComitente = DTComitente.NewRow()
                Case 25
                    LabelComitente.Hide()
                    LabelCaratula.Hide()
                    UTEComitente.Hide()
                    TBCaratula.Hide()
                    RowComitente = DTComitente.NewRow()
            End Select
        End If
    End Sub

    Private Sub TBCaratula_KeyPress(sender As Object, e As KeyPressEventArgs)
        If ((Strings.Asc(e.KeyChar) <> 8) AndAlso Not ModPrincipal.pubCaracteresPermiteComitente.Contains(e.KeyChar.ToString.ToLower)) Then
            e.KeyChar = ChrW(0)
            e.Handled = True
        End If
    End Sub
    'TRABAJOS
    Private Sub UTECodigoBarra_EditorButtonClick(sender As Object, e As UltraWinEditors.EditorButtonEventArgs) Handles UTECodigoBarra.EditorButtonClick
        If e.Button.Key = "Right" Then
            BuscaTrabajoIngresado(UTECodigoBarra.Text)
        End If
    End Sub
    Private Sub UTECodigoBarra_KeyDown(sender As Object, e As KeyEventArgs) Handles UTECodigoBarra.KeyDown
        If e.KeyCode = Keys.Enter Then
            BuscaTrabajoIngresado(UTECodigoBarra.Text)
        End If
    End Sub
    Private Sub BuscaTrabajoIngresado(ByVal valorCodigoBarra As Integer)
        DAglobalMysql = cnnMante.QueryTrabajoById(valorCodigoBarra)

        Dim DSTrabajo As DataSet = New DataSet()
        DAglobalMysql.Fill(DSTrabajo, "trabajo")
        If DSTrabajo.Tables("trabajo").Rows.Count() > 0 Then
            Dim rowTrabajo As DataRow = DSTrabajo.Tables("trabajo").Rows(0)
            'Control si el estado del trabajo es PAGADO entra
            If rowTrabajo.Item("tra_estado") = 3 Then
                CbtAltaOlbeas_Click(CbtAltaOlbeas, Nothing)
                'Completo campos
                UNCopias.Value = rowTrabajo.Item("tra_cantidad")
                UcboTrabajo.Text = rowTrabajo.Item("tar_descrip")

                UTEMtra.Text = rowTrabajo.Item("tra_matricula")
                UTEMtraPressEnter(rowTrabajo.Item("tra_nroasi"))

                UTEMtrImp.Text = rowTrabajo.Item("tra_matricula")
                UTEMtrImpPressEnter()

                'UTEComitente.Text = rowTrabajo.Item("tra_monto_deposito")
                UTEComitente.Text = rowTrabajo.Item("tra_comitentecuit").Replace("-", "")
                BuscoComitente()
                UDTfecha.Value = CDate(rowTrabajo.Item("tra_fecha_cierre").ToString())
                UDTCertif.Value = CDate(rowTrabajo.Item("tra_fecha_informe").ToString())
                'TxtConcepto2.Text = "Comitente: " & rowTrabajo.Item("tra_comitentecuit") & " " & rowTrabajo.Item("tra_clientecomitente")
                'TxtConcepto3.Text = "Trabajo: " & rowTrabajo.Item("tar_descrip") & ". " & Format(CDate(rowTrabajo.Item("tra_fecha_cierre").ToString()), "dd-MM-yyyy")
                'LabelMontoTrabajo.Text = rowTrabajo.Item("tra_monto_deposito")
                UTEEstado.Text = If(IsDBNull(rowTrabajo.Item("tra_estado")), "", GetEstadoStringSegunNro(rowTrabajo.Item("tra_estado")))
                UTECertificado.Text = If(IsDBNull(rowTrabajo.Item("tra_certificado")), "", GetCertificadoStringSegunNro(rowTrabajo.Item("tra_certificado")))
                'Guardo el ID del trabajo para utilizarlo luego de guardar el asiento y actualizar el estado del mismo
                globalTrabajoId = rowTrabajo.Item("id")
            ElseIf rowTrabajo.Item("tra_estado") = 20 Then
                globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", SE ENCUENTRA ANULADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf rowTrabajo.Item("tra_estado") >= 5 Then
                globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
                'MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", YA SE ENCUENTRA PRESENTADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                MuestraObleas("obl_nrolegali=" & rowTrabajo.Item("tra_nrolegali").ToString())
            Else
                'LabelMontoTrabajo.Text = "0000,00"
                globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
                MessageBox.Show("EL TRABAJO NUMERO: " & valorCodigoBarra & ", NO SE ENCUENTRA PAGADO", "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        Else
            'LabelMontoTrabajo.Text = "0000,00"
            globalTrabajoId = 0 'Seteo en 0 el ID del trabajo
            MessageBox.Show("NO EXISTE EL TRABAJO NUMERO: " & valorCodigoBarra, "Trabajos", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub
    Private Sub ButtonFinalizar_Click(sender As Object, e As EventArgs) Handles ButtonFinalizar.Click
        If cnnMante.AbrirConexion Then
            Dim miTrans As MySqlTransaction
            miTrans = cnnMante.InicioTransaccion()
            Try
                Dim id As Integer = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoId")), 0, DTObleas.Rows(BSObleas.Position).Item("TrabajoId"))
                If id <> 0 Then
                    Dim email As String = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("ProfesionalEmail")), Nothing, DTObleas.Rows(BSObleas.Position).Item("ProfesionalEmail"))
                    cnnMante.CambiarEstadoCertificadoTrabajo(id, 1) '1 = Finalizado
                    miTrans.Commit()
                    enviarCorreo(id, 1, email) '1 = Finalizado
                    If GetAnioNroLegalizacion(UNElegali.Value, 2) > 15 Then
                        UTEbuscaLegali.Value = NroLegalizacionStringToSave(UNElegali.Value, globalEsAnticipo)
                    Else
                        UTEbuscaLegali.Value = UNElegali.Value
                    End If
                    MuestraObleas("obl_nrolegali=" & UTEbuscaLegali.Value)
                Else
                    miTrans.Rollback()
                End If
            Catch ex As Exception
                miTrans.Rollback()
                cnnMante.CerrarConexion()
                MessageBox.Show(ex.Message)
            End Try
            cnnMante.CerrarConexion()
        End If
    End Sub
    Private Sub ButtonEntregar_Click(sender As Object, e As EventArgs) Handles ButtonEntregar.Click
        If cnnMante.AbrirConexion Then
            Dim miTrans As MySqlTransaction
            miTrans = cnnMante.InicioTransaccion()
            Try
                Dim id As Integer = If(IsDBNull(DTObleas.Rows(BSObleas.Position).Item("TrabajoId")), 0, DTObleas.Rows(BSObleas.Position).Item("TrabajoId"))
                If id <> 0 Then
                    cnnMante.CambiarEstadoCertificadoTrabajo(id, 2) '2 = Entregado
                    miTrans.Commit()
                    If GetAnioNroLegalizacion(UNElegali.Value, 2) > 15 Then
                        UTEbuscaLegali.Value = NroLegalizacionStringToSave(UNElegali.Value, globalEsAnticipo)
                    Else
                        UTEbuscaLegali.Value = UNElegali.Value
                    End If
                    ImprimeObleasCR(True)
                    MuestraObleas("obl_nrolegali=" & UTEbuscaLegali.Value)
                Else
                    miTrans.Rollback()
                End If
            Catch ex As Exception
                miTrans.Rollback()
                cnnMante.CerrarConexion()
                MessageBox.Show(ex.Message)
            End Try
            cnnMante.CerrarConexion()
        End If
    End Sub
    Private Sub enviarCorreo(ByVal id As Integer, ByVal certificado As Integer, ByVal destinatario As String)
        If certificado = 1 And Not IsNothing(destinatario) Then
            Dim contenido As String = "
                <p>Estimado profesional</p>
                <p>Su trabajo con código nro: " & id & "</p>
                <p>se encuentra a disposición para ser retirado, en Sede Central.</p>
                <p>Otras Delegaciones, Consultar.</p>
                <p>Atentamente,</p>
                <p>CPCE Chaco.</p>
            "
            Dim correos As Correos = New Correos()
            correos.enviarCorreo(
                destinatario,
                "CPCE Chaco, Trabajo",
                contenido
            )
        End If
    End Sub
End Class