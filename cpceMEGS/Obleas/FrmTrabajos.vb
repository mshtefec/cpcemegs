﻿Public Class FrmTrabajos
    Private cnn As New ConsultaBD(True)
    Private DAProfesional As MySql.Data.MySqlClient.MySqlDataAdapter
    Private DTProfesionales As DataTable
    Private RowProfesional As DataRow
    Private Sub FrmTrabajos_Load(sender As Object, e As EventArgs) Handles Me.Load
        'TODO: esta línea de código carga datos en la tabla 'DSTrabajo.trabajo_estado' Puede moverla o quitarla según sea necesario.
        Trabajo_estadoTableAdapter.Fill(DSTrabajo.trabajo_estado)
        'TODO: esta línea de código carga datos en la tabla 'DSTrabajo.tareas' Puede moverla o quitarla según sea necesario.
        TareasTableAdapter.Fill(DSTrabajo.tareas)
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 8)

        With DGVTrabajo
            .Columns(0).HeaderText = "Código"
            .Columns(0).Width = 60
            .Columns(1).HeaderText = "Nro Legalización"
            .Columns(1).Width = 80
            '.Columns(2).HeaderText = "Estado"
            '.Columns(3).HeaderText = "Matricula"
            .Columns(3).Width = 60
            '.Columns(4).HeaderText = "Profesional"
            .Columns(4).Width = 120
            .Columns(5).HeaderText = "Tarea"
            .Columns(5).Width = 320
            .Columns(6).HeaderText = "Comitente Cuit"
            .Columns(6).Width = 80
            .Columns(7).HeaderText = "Comitente"
            .Columns(7).Width = 120
            .Columns(8).HeaderText = "Activo+Pasivo"
            .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(8).DefaultCellStyle.Format = "c"
            .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(9).DefaultCellStyle.Format = "c"
            .Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(10).DefaultCellStyle.Format = "c"
            .Columns(11).HeaderText = "Monto Aporte"
            .Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(11).DefaultCellStyle.Format = "c"
            .Columns(12).HeaderText = "Monto IVA"
            .Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(12).DefaultCellStyle.Format = "c"
            .Columns(13).HeaderText = "Honorario Minimo"
            .Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(13).DefaultCellStyle.Format = "c"
            .Columns(14).HeaderText = "Monto Deposito"
            .Columns(14).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(14).DefaultCellStyle.Format = "c"
        End With
    End Sub
    'METODOS PARA LOS SELECT QUERY
    Private Sub TodosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TodosToolStripMenuItem.Click
        TrabajoTableAdapter.FillAll(DSTrabajo.trabajo)
    End Sub
    Private Sub GeneradosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GeneradosToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 1)
    End Sub
    Private Sub PagadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PagadosToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 3)
    End Sub
    Private Sub PresentadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PresentadosToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 5)
    End Sub
    Private Sub ConfeccionandoReintegroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfeccionandoReintegroToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 7)
    End Sub
    Private Sub ReintegrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReintegrosToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 8)
    End Sub
    Private Sub CobradosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CobradosToolStripMenuItem.Click
        TrabajoTableAdapter.Fill(DSTrabajo.trabajo, 15)
    End Sub
    'METODOS PARA BUSCAR POR CODIGO
    Private Sub TSButtonCodigo_Click(sender As Object, e As EventArgs) Handles TSButtonCodigo.Click
        If IsNumeric(TSTBCodigo.Text) Then
            TrabajoTableAdapter.FillById(DSTrabajo.trabajo, TSTBCodigo.Text())
        End If
    End Sub

    Private Sub TSTBCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TSTBCodigo.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) And IsNumeric(TSTBCodigo.Text) Then
            TrabajoTableAdapter.FillById(DSTrabajo.trabajo, TSTBCodigo.Text())
        End If
    End Sub
    'METODOS PARA BUSCAR POR MATRICULA
    Private Sub TSButtonMatricula_Click(sender As Object, e As EventArgs) Handles TSButtonMatricula.Click
        If TSTBMatricula.Text <> "" Then
            TrabajoTableAdapter.FillByMatricula(DSTrabajo.trabajo, TSTBMatricula.Text())
        End If
    End Sub

    Private Sub TSTBMatricula_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TSTBMatricula.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) And TSTBMatricula.Text <> "" Then
            TrabajoTableAdapter.FillByMatricula(DSTrabajo.trabajo, TSTBMatricula.Text())
        End If
    End Sub

    Private Sub ActualizarTrabajoTableAdapter()
        Validate()
        TrabajoBindingSource.EndEdit()
        Try
            If MessageBox.Show("Confirmar la actualizacion", "Permiso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                TrabajoTableAdapter.Adapter.Update(DSTrabajo)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorSaveItem.Click
        'Controlo permiso Trabajo Honorario CRUD
        If controlAcceso(9, 1010, , True) = True Then
            ActualizarTrabajoTableAdapter()
        End If
    End Sub

    Private Sub TSButtonCobrado_Click(sender As Object, e As EventArgs) Handles TSButtonCobrado.Click
        'Controlo permiso Trabajo Honorario
        If controlAcceso(9, 1, , True) = True Then
            Dim mensaje As String = "Trabajo actualizado correctamente"
            Dim icon As MessageBoxIcon = MessageBoxIcon.Information
            Try
                If DGVTrabajo.CurrentRow.Cells.Item(0).Value() > 0 Then
                    If DGVTrabajo.CurrentRow.Cells.Item(2).Value = 8 Then
                        DGVTrabajo.CurrentRow.Cells.Item(2).Value = 15
                        ActualizarTrabajoTableAdapter()
                    Else
                        mensaje = "No se genero el reintegro para el trabajo"
                        icon = MessageBoxIcon.Warning
                    End If
                Else
                    mensaje = "Seleccionar correctamente el trabajo en el listado"
                    icon = MessageBoxIcon.Warning
                End If
            Catch ex As Exception
                mensaje = "Seleccionar correctamente el trabajo en el listado"
                icon = MessageBoxIcon.Warning
            End Try
            MessageBox.Show(mensaje, "Trabajos", MessageBoxButtons.OK, icon)
        End If
    End Sub

    Private Sub TSBLiquidar_Click(sender As Object, e As EventArgs) Handles TSBLiquidar.Click
        'Controlo permiso Liquidacion Honorario
        If controlAcceso(9, 2, , True) = True Then
            BuscaProfesional()
            Dim FrmLiquidacion As New FrmLiquidacionHonorarios(RowProfesional)
            'controlo lo que tiene cargado en ganacias
            If RowProfesional.Item("afi_ganancias") = "S" Then
                FrmLiquidacion.UCEganancias.Text = "SI"
            ElseIf RowProfesional.Item("afi_ganancias") = "N" Then
                FrmLiquidacion.UCEganancias.Text = "NO"
            ElseIf RowProfesional.Item("afi_ganancias") = "" Then
                FrmLiquidacion.UCEganancias.Text = "NO"
            Else
                FrmLiquidacion.UCEganancias.Text = RowProfesional.Item("afi_ganancias")
            End If
            FrmLiquidacion.UDTdgi.Value = RowProfesional.Item("afi_dgiexcep").ToString
            FrmLiquidacion.UDTdgr.Value = RowProfesional.Item("afi_dgrexcep").ToString
            FrmLiquidacion.UDTiva.Value = RowProfesional.Item("afi_ivaexcep").ToString
            FrmLiquidacion.UTECodigoBarra.Value = DGVTrabajo.CurrentRow.Cells.Item(0).Value()
            FrmLiquidacion.Show()
        End If
    End Sub
    Private Sub BuscaProfesional()
        DAProfesional = cnn.consultaBDadapter(
            "afiliado LEFT JOIN categorias ON cat_codigo=afi_categoria",
            "afi_nombre,afi_titulo,afi_matricula,afi_fecnac,afi_tipdoc,afi_nrodoc,afi_direccion,afi_localidad,afi_provincia,afi_zona,afi_codpos,afi_telefono1,afi_telefono2,afi_mail,afi_mail_alternativo,afi_civil,afi_cuit,afi_dgr,afi_sexo,afi_garante,afi_garante_tipdoc,afi_garante_nrodoc,afi_garantede_tipdoc,afi_garantede_nrodoc,afi_tipo,afi_ganancias,afi_categoria,afi_dgiexcep,afi_dgrexcep,afi_ivaexcep,afi_doc1,afi_aut1,afi_nac1,afi_sex1,afi_fil1,afi_doc2,afi_aut2,afi_nac2,afi_sex2,afi_fil2,afi_doc3,afi_aut3,afi_nac3,afi_sex3,afi_fil3,afi_doc4,afi_aut4,afi_nac4,afi_sex4,afi_fil4,afi_doc5,afi_aut5,afi_nac5,afi_sex5,afi_fil5,afi_doc6,afi_aut6,afi_nac6,afi_sex6,afi_fil6,afi_cbu,afi_cbu_credito,afi_generarrenta",
            "afi_titulo='" & DGVTrabajo.CurrentRow.Cells.Item(3).Value().ToString.Substring(0, 2) & "' AND afi_matricula=" & DGVTrabajo.CurrentRow.Cells.Item(3).Value().ToString.Substring(2)
        )
        DTProfesionales = New DataTable
        DAProfesional.Fill(DTProfesionales)
        If DTProfesionales.Rows.Count > 0 Then
            RowProfesional = DTProfesionales.Rows(0)
        Else
            MessageBox.Show("No se encuentra matricula o esta deshabilitado", "PROFESIONAL OBLEAS", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class