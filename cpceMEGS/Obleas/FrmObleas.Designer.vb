﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmObleas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance13 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton1 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance14 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmObleas))
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton2 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton3 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance21 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim EditorButton4 As Infragistics.Win.UltraWinEditors.EditorButton = New Infragistics.Win.UltraWinEditors.EditorButton("Right")
        Dim Appearance25 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.UGobleas = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UTEprofesional = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UTEMtra = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UNEoblea = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.UcboTrabajo = New Infragistics.Win.UltraWinGrid.UltraCombo()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.UDTotorga = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.UDTCertif = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.LabelComitente = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.UDTfecha = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.UNCopias = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.CbtImprimirObleas = New System.Windows.Forms.Button()
        Me.CbtActualizarObleas = New System.Windows.Forms.Button()
        Me.CbtAltaOlbeas = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UTEComitente = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UNLegaliAnio = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.UTESucursalSiglas = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CBtExportar = New System.Windows.Forms.Button()
        Me.UTEbuscaLegali = New Infragistics.Win.UltraWinEditors.UltraNumericEditor()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UDTHasta = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.UDTDesde = New Infragistics.Win.UltraWinEditors.UltraDateTimeEditor()
        Me.ppdDialog = New System.Windows.Forms.PrintDialog()
        Me.ppd = New System.Drawing.Printing.PrintDocument()
        Me.UGRecibos = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.CBtEditar = New System.Windows.Forms.Button()
        Me.UTEImpreso = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UTEMtrImp = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.CBEjemplar = New System.Windows.Forms.Button()
        Me.LabelEjemplarAdicional = New System.Windows.Forms.Label()
        Me.UTEjemplarAdicional = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.LabelCaratula = New System.Windows.Forms.Label()
        Me.TBCaratula = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.GBTrabajo = New System.Windows.Forms.GroupBox()
        Me.UTECodigoBarra = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UNElegali = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.UTEEstado = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.ButtonFinalizar = New System.Windows.Forms.Button()
        Me.GBEstados = New System.Windows.Forms.GroupBox()
        Me.UTECertificado = New Infragistics.Win.UltraWinEditors.UltraTextEditor()
        Me.ButtonEntregar = New System.Windows.Forms.Button()
        CType(Me.UGobleas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEprofesional, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEMtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNEoblea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UcboTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTotorga, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTCertif, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTfecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNCopias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEComitente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UNLegaliAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTESucursalSiglas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEbuscaLegali, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UGRecibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEImpreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEMtrImp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEjemplarAdicional, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TBCaratula, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBTrabajo.SuspendLayout()
        CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UNElegali, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UTEEstado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GBEstados.SuspendLayout()
        CType(Me.UTECertificado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UGobleas
        '
        Appearance1.BackColor = System.Drawing.Color.White
        Me.UGobleas.DisplayLayout.Appearance = Appearance1
        Me.UGobleas.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.UGobleas.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Appearance2.BackColor = System.Drawing.Color.Transparent
        Me.UGobleas.DisplayLayout.Override.CardAreaAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance3.FontData.BoldAsString = "True"
        Appearance3.FontData.Name = "Arial"
        Appearance3.FontData.SizeInPoints = 10.0!
        Appearance3.ForeColor = System.Drawing.Color.White
        Appearance3.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGobleas.DisplayLayout.Override.HeaderAppearance = Appearance3
        Me.UGobleas.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle
        Appearance4.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGobleas.DisplayLayout.Override.RowSelectorAppearance = Appearance4
        Appearance5.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance5.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGobleas.DisplayLayout.Override.SelectedRowAppearance = Appearance5
        Me.UGobleas.Dock = System.Windows.Forms.DockStyle.Top
        Me.UGobleas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGobleas.Location = New System.Drawing.Point(0, 0)
        Me.UGobleas.Name = "UGobleas"
        Me.UGobleas.Size = New System.Drawing.Size(934, 281)
        Me.UGobleas.TabIndex = 35
        '
        'UTEprofesional
        '
        Me.UTEprofesional.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEprofesional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEprofesional.Location = New System.Drawing.Point(208, 426)
        Me.UTEprofesional.Name = "UTEprofesional"
        Me.UTEprofesional.ReadOnly = True
        Me.UTEprofesional.Size = New System.Drawing.Size(274, 24)
        Me.UTEprofesional.TabIndex = 93
        '
        'UTEMtra
        '
        Me.UTEMtra.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEMtra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEMtra.Location = New System.Drawing.Point(120, 426)
        Me.UTEMtra.Name = "UTEMtra"
        Me.UTEMtra.Size = New System.Drawing.Size(82, 24)
        Me.UTEMtra.TabIndex = 4
        '
        'UNEoblea
        '
        Appearance6.TextHAlignAsString = "Center"
        Me.UNEoblea.Appearance = Appearance6
        Me.UNEoblea.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UNEoblea.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNEoblea.Location = New System.Drawing.Point(384, 344)
        Me.UNEoblea.Name = "UNEoblea"
        Me.UNEoblea.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNEoblea.Size = New System.Drawing.Size(98, 24)
        Me.UNEoblea.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label25.Location = New System.Drawing.Point(321, 371)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(57, 21)
        Me.Label25.TabIndex = 83
        Me.Label25.Text = "Copias:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UcboTrabajo
        '
        Appearance7.BackColor = System.Drawing.Color.White
        Appearance7.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.ForwardDiagonal
        Me.UcboTrabajo.DisplayLayout.Appearance = Appearance7
        Me.UcboTrabajo.DisplayLayout.InterBandSpacing = 10
        Appearance8.BackColor = System.Drawing.Color.Transparent
        Me.UcboTrabajo.DisplayLayout.Override.CardAreaAppearance = Appearance8
        Appearance9.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance9.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance9.ForeColor = System.Drawing.Color.White
        Appearance9.TextHAlignAsString = "Left"
        Appearance9.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UcboTrabajo.DisplayLayout.Override.HeaderAppearance = Appearance9
        Appearance10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UcboTrabajo.DisplayLayout.Override.RowAppearance = Appearance10
        Appearance11.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance11.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Appearance11.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UcboTrabajo.DisplayLayout.Override.RowSelectorAppearance = Appearance11
        Me.UcboTrabajo.DisplayLayout.Override.RowSelectorWidth = 12
        Me.UcboTrabajo.DisplayLayout.Override.RowSpacingBefore = 2
        Appearance12.BackColor = System.Drawing.Color.FromArgb(CType(CType(129, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(226, Byte), Integer))
        Appearance12.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(254, Byte), Integer))
        Appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance12.ForeColor = System.Drawing.Color.Black
        Me.UcboTrabajo.DisplayLayout.Override.SelectedRowAppearance = Appearance12
        Me.UcboTrabajo.DisplayLayout.RowConnectorColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(208, Byte), Integer))
        Me.UcboTrabajo.DisplayLayout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.Solid
        Me.UcboTrabajo.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UcboTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcboTrabajo.Location = New System.Drawing.Point(6, 397)
        Me.UcboTrabajo.Name = "UcboTrabajo"
        Me.UcboTrabajo.Size = New System.Drawing.Size(476, 25)
        Me.UcboTrabajo.TabIndex = 3
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label24.Location = New System.Drawing.Point(6, 371)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(108, 21)
        Me.Label24.TabIndex = 82
        Me.Label24.Text = "Trabajo:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTotorga
        '
        Me.UDTotorga.DateTime = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.UDTotorga.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTotorga.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTotorga.Location = New System.Drawing.Point(231, 598)
        Me.UDTotorga.Name = "UDTotorga"
        Me.UDTotorga.Size = New System.Drawing.Size(103, 24)
        Me.UDTotorga.TabIndex = 10
        Me.UDTotorga.Value = Nothing
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(6, 601)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(210, 21)
        Me.Label23.TabIndex = 81
        Me.Label23.Text = "Fecha de otorgamiento:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTCertif
        '
        Me.UDTCertif.DateTime = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.UDTCertif.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTCertif.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTCertif.Location = New System.Drawing.Point(231, 568)
        Me.UDTCertif.MaskInput = "{date}"
        Me.UDTCertif.Name = "UDTCertif"
        Me.UDTCertif.Size = New System.Drawing.Size(103, 24)
        Me.UDTCertif.TabIndex = 9
        Me.UDTCertif.Value = Nothing
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(6, 571)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(210, 21)
        Me.Label22.TabIndex = 80
        Me.Label22.Text = "Fecha del informe o certificación:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelComitente
        '
        Me.LabelComitente.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelComitente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelComitente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelComitente.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelComitente.Location = New System.Drawing.Point(6, 482)
        Me.LabelComitente.Name = "LabelComitente"
        Me.LabelComitente.Size = New System.Drawing.Size(108, 21)
        Me.LabelComitente.TabIndex = 79
        Me.LabelComitente.Text = "Comitente:"
        Me.LabelComitente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label20.Location = New System.Drawing.Point(6, 426)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(108, 21)
        Me.Label20.TabIndex = 78
        Me.Label20.Text = "Profesional:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTfecha
        '
        Me.UDTfecha.DateTime = New Date(1753, 1, 1, 0, 0, 0, 0)
        Me.UDTfecha.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTfecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTfecha.Location = New System.Drawing.Point(231, 538)
        Me.UDTfecha.Name = "UDTfecha"
        Me.UDTfecha.Size = New System.Drawing.Size(103, 24)
        Me.UDTfecha.TabIndex = 8
        Me.UDTfecha.Value = Nothing
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(6, 541)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(210, 21)
        Me.Label19.TabIndex = 77
        Me.Label19.Text = "Fecha de cierre:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(311, 345)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 21)
        Me.Label17.TabIndex = 76
        Me.Label17.Text = "N° Oblea:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(6, 345)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(108, 21)
        Me.Label18.TabIndex = 75
        Me.Label18.Text = "N°Legalización:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UNCopias
        '
        Appearance13.TextHAlignAsString = "Left"
        Me.UNCopias.Appearance = Appearance13
        Me.UNCopias.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UNCopias.Location = New System.Drawing.Point(384, 371)
        Me.UNCopias.MaskInput = "nnnn"
        Me.UNCopias.MaxValue = 100
        Me.UNCopias.MinValue = 1
        Me.UNCopias.Name = "UNCopias"
        Me.UNCopias.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNCopias.Size = New System.Drawing.Size(55, 21)
        Me.UNCopias.TabIndex = 2
        '
        'CbtImprimirObleas
        '
        Me.CbtImprimirObleas.BackColor = System.Drawing.Color.Transparent
        Me.CbtImprimirObleas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CbtImprimirObleas.Image = Global.cpceMEGS.My.Resources.Resources.Print_11009
        Me.CbtImprimirObleas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CbtImprimirObleas.Location = New System.Drawing.Point(488, 626)
        Me.CbtImprimirObleas.Margin = New System.Windows.Forms.Padding(0)
        Me.CbtImprimirObleas.Name = "CbtImprimirObleas"
        Me.CbtImprimirObleas.Size = New System.Drawing.Size(131, 44)
        Me.CbtImprimirObleas.TabIndex = 12
        Me.CbtImprimirObleas.Text = "Imprimir Obleas"
        Me.CbtImprimirObleas.UseVisualStyleBackColor = False
        '
        'CbtActualizarObleas
        '
        Me.CbtActualizarObleas.BackColor = System.Drawing.Color.Transparent
        Me.CbtActualizarObleas.Enabled = False
        Me.CbtActualizarObleas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CbtActualizarObleas.Image = Global.cpceMEGS.My.Resources.Resources.Save_6530
        Me.CbtActualizarObleas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CbtActualizarObleas.Location = New System.Drawing.Point(268, 630)
        Me.CbtActualizarObleas.Name = "CbtActualizarObleas"
        Me.CbtActualizarObleas.Size = New System.Drawing.Size(126, 39)
        Me.CbtActualizarObleas.TabIndex = 11
        Me.CbtActualizarObleas.Text = "Actualizar"
        Me.CbtActualizarObleas.UseVisualStyleBackColor = False
        '
        'CbtAltaOlbeas
        '
        Me.CbtAltaOlbeas.BackColor = System.Drawing.Color.Transparent
        Me.CbtAltaOlbeas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CbtAltaOlbeas.Image = Global.cpceMEGS.My.Resources.Resources.AddMark_10580
        Me.CbtAltaOlbeas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CbtAltaOlbeas.Location = New System.Drawing.Point(4, 630)
        Me.CbtAltaOlbeas.Name = "CbtAltaOlbeas"
        Me.CbtAltaOlbeas.Size = New System.Drawing.Size(126, 39)
        Me.CbtAltaOlbeas.TabIndex = 50
        Me.CbtAltaOlbeas.Text = "Alta"
        Me.CbtAltaOlbeas.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(6, 455)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 21)
        Me.Label1.TabIndex = 102
        Me.Label1.Text = "Impreso:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UTEComitente
        '
        Appearance14.Image = CType(resources.GetObject("Appearance14.Image"), Object)
        Appearance14.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance14.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton1.Appearance = Appearance14
        EditorButton1.Width = 30
        Me.UTEComitente.ButtonsRight.Add(EditorButton1)
        Me.UTEComitente.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEComitente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEComitente.Location = New System.Drawing.Point(120, 479)
        Me.UTEComitente.Name = "UTEComitente"
        Me.UTEComitente.Size = New System.Drawing.Size(362, 24)
        Me.UTEComitente.TabIndex = 6
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.Controls.Add(Me.UNLegaliAnio)
        Me.UltraGroupBox1.Controls.Add(Me.UTESucursalSiglas)
        Me.UltraGroupBox1.Controls.Add(Me.CBtExportar)
        Me.UltraGroupBox1.Controls.Add(Me.UTEbuscaLegali)
        Me.UltraGroupBox1.Controls.Add(Me.Label2)
        Me.UltraGroupBox1.Controls.Add(Me.UDTHasta)
        Me.UltraGroupBox1.Controls.Add(Me.UDTDesde)
        Me.UltraGroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraGroupBox1.Location = New System.Drawing.Point(7, 284)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(687, 55)
        Me.UltraGroupBox1.TabIndex = 103
        Me.UltraGroupBox1.Text = "Criterios de Búsqueda"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2000
        '
        'UNLegaliAnio
        '
        Appearance15.TextHAlignAsString = "Left"
        Me.UNLegaliAnio.Appearance = Appearance15
        Me.UNLegaliAnio.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UNLegaliAnio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNLegaliAnio.Location = New System.Drawing.Point(247, 18)
        Me.UNLegaliAnio.MaskInput = "nnnn"
        Me.UNLegaliAnio.MaxValue = 3000
        Me.UNLegaliAnio.MinValue = 0
        Me.UNLegaliAnio.Name = "UNLegaliAnio"
        Me.UNLegaliAnio.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UNLegaliAnio.Size = New System.Drawing.Size(70, 21)
        Me.UNLegaliAnio.TabIndex = 114
        '
        'UTESucursalSiglas
        '
        Appearance16.TextHAlignAsString = "Center"
        Me.UTESucursalSiglas.Appearance = Appearance16
        Me.UTESucursalSiglas.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTESucursalSiglas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTESucursalSiglas.Location = New System.Drawing.Point(113, 18)
        Me.UTESucursalSiglas.Name = "UTESucursalSiglas"
        Me.UTESucursalSiglas.ReadOnly = True
        Me.UTESucursalSiglas.Size = New System.Drawing.Size(34, 24)
        Me.UTESucursalSiglas.TabIndex = 114
        '
        'CBtExportar
        '
        Me.CBtExportar.BackColor = System.Drawing.Color.Transparent
        Me.CBtExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBtExportar.Image = Global.cpceMEGS.My.Resources.Resources.PrintSetup_11011
        Me.CBtExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtExportar.Location = New System.Drawing.Point(579, 11)
        Me.CBtExportar.Name = "CBtExportar"
        Me.CBtExportar.Size = New System.Drawing.Size(102, 38)
        Me.CBtExportar.TabIndex = 52
        Me.CBtExportar.Text = "Exportar"
        Me.CBtExportar.UseVisualStyleBackColor = False
        '
        'UTEbuscaLegali
        '
        Appearance17.TextHAlignAsString = "Center"
        Me.UTEbuscaLegali.Appearance = Appearance17
        Appearance18.Image = CType(resources.GetObject("Appearance18.Image"), Object)
        Appearance18.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance18.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton2.Appearance = Appearance18
        EditorButton2.Width = 30
        Me.UTEbuscaLegali.ButtonsRight.Add(EditorButton2)
        Me.UTEbuscaLegali.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEbuscaLegali.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEbuscaLegali.Location = New System.Drawing.Point(147, 18)
        Me.UTEbuscaLegali.Name = "UTEbuscaLegali"
        Me.UTEbuscaLegali.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UTEbuscaLegali.Size = New System.Drawing.Size(94, 24)
        Me.UTEbuscaLegali.TabIndex = 100
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(7, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 21)
        Me.Label2.TabIndex = 104
        Me.Label2.Text = "N°Legalización:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UDTHasta
        '
        Appearance19.Image = CType(resources.GetObject("Appearance19.Image"), Object)
        Appearance19.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance19.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton3.Appearance = Appearance19
        EditorButton3.Width = 30
        Me.UDTHasta.ButtonsRight.Add(EditorButton3)
        Me.UDTHasta.DateTime = New Date(2012, 11, 5, 0, 0, 0, 0)
        Me.UDTHasta.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTHasta.Location = New System.Drawing.Point(438, 18)
        Me.UDTHasta.Name = "UDTHasta"
        Me.UDTHasta.Size = New System.Drawing.Size(135, 24)
        Me.UDTHasta.TabIndex = 102
        Me.UDTHasta.Value = New Date(2012, 11, 5, 0, 0, 0, 0)
        '
        'UDTDesde
        '
        Me.UDTDesde.DateTime = New Date(2012, 11, 5, 0, 0, 0, 0)
        Me.UDTDesde.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UDTDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UDTDesde.Location = New System.Drawing.Point(322, 18)
        Me.UDTDesde.Name = "UDTDesde"
        Me.UDTDesde.Size = New System.Drawing.Size(110, 24)
        Me.UDTDesde.TabIndex = 101
        Me.UDTDesde.Value = New Date(2012, 11, 5, 0, 0, 0, 0)
        '
        'ppdDialog
        '
        Me.ppdDialog.UseEXDialog = True
        '
        'ppd
        '
        '
        'UGRecibos
        '
        Appearance20.BackColor = System.Drawing.Color.White
        Me.UGRecibos.DisplayLayout.Appearance = Appearance20
        Appearance21.BackColor = System.Drawing.Color.Transparent
        Me.UGRecibos.DisplayLayout.Override.CardAreaAppearance = Appearance21
        Me.UGRecibos.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow
        Appearance22.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance22.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance22.FontData.BoldAsString = "True"
        Appearance22.FontData.Name = "Arial"
        Appearance22.FontData.SizeInPoints = 10.0!
        Appearance22.ForeColor = System.Drawing.Color.White
        Appearance22.ThemedElementAlpha = Infragistics.Win.Alpha.Transparent
        Me.UGRecibos.DisplayLayout.Override.HeaderAppearance = Appearance22
        Appearance23.BackColor = System.Drawing.Color.FromArgb(CType(CType(89, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(214, Byte), Integer))
        Appearance23.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(7, Byte), Integer), CType(CType(59, Byte), Integer), CType(CType(150, Byte), Integer))
        Appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGRecibos.DisplayLayout.Override.RowSelectorAppearance = Appearance23
        Appearance24.BackColor = System.Drawing.Color.FromArgb(CType(CType(251, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(148, Byte), Integer))
        Appearance24.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(149, Byte), Integer), CType(CType(21, Byte), Integer))
        Appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Me.UGRecibos.DisplayLayout.Override.SelectedRowAppearance = Appearance24
        Me.UGRecibos.DisplayLayout.Override.WrapHeaderText = Infragistics.Win.DefaultableBoolean.[True]
        Me.UGRecibos.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand
        Me.UGRecibos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.UGRecibos.Location = New System.Drawing.Point(488, 343)
        Me.UGRecibos.Name = "UGRecibos"
        Me.UGRecibos.Size = New System.Drawing.Size(446, 187)
        Me.UGRecibos.TabIndex = 104
        Me.UGRecibos.Text = "Recibos"
        '
        'CBtEditar
        '
        Me.CBtEditar.BackColor = System.Drawing.Color.Transparent
        Me.CBtEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBtEditar.Image = Global.cpceMEGS.My.Resources.Resources.PencilAngled_32xSM_color
        Me.CBtEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBtEditar.Location = New System.Drawing.Point(136, 630)
        Me.CBtEditar.Name = "CBtEditar"
        Me.CBtEditar.Size = New System.Drawing.Size(126, 39)
        Me.CBtEditar.TabIndex = 51
        Me.CBtEditar.Text = "Editar"
        Me.CBtEditar.UseVisualStyleBackColor = False
        '
        'UTEImpreso
        '
        Me.UTEImpreso.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEImpreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEImpreso.Location = New System.Drawing.Point(208, 453)
        Me.UTEImpreso.Name = "UTEImpreso"
        Me.UTEImpreso.ReadOnly = True
        Me.UTEImpreso.Size = New System.Drawing.Size(274, 24)
        Me.UTEImpreso.TabIndex = 106
        '
        'UTEMtrImp
        '
        Me.UTEMtrImp.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEMtrImp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEMtrImp.Location = New System.Drawing.Point(120, 453)
        Me.UTEMtrImp.Name = "UTEMtrImp"
        Me.UTEMtrImp.Size = New System.Drawing.Size(82, 24)
        Me.UTEMtrImp.TabIndex = 5
        '
        'CBEjemplar
        '
        Me.CBEjemplar.BackColor = System.Drawing.Color.Transparent
        Me.CBEjemplar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CBEjemplar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Warning_32xMD_color
        Me.CBEjemplar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBEjemplar.Location = New System.Drawing.Point(794, 626)
        Me.CBEjemplar.Margin = New System.Windows.Forms.Padding(0)
        Me.CBEjemplar.Name = "CBEjemplar"
        Me.CBEjemplar.Size = New System.Drawing.Size(140, 44)
        Me.CBEjemplar.TabIndex = 13
        Me.CBEjemplar.Text = "Ejemplar Adicional"
        Me.CBEjemplar.UseVisualStyleBackColor = False
        '
        'LabelEjemplarAdicional
        '
        Me.LabelEjemplarAdicional.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelEjemplarAdicional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelEjemplarAdicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelEjemplarAdicional.Location = New System.Drawing.Point(361, 582)
        Me.LabelEjemplarAdicional.Name = "LabelEjemplarAdicional"
        Me.LabelEjemplarAdicional.Size = New System.Drawing.Size(78, 40)
        Me.LabelEjemplarAdicional.TabIndex = 109
        Me.LabelEjemplarAdicional.Text = "Ejemplar adicional:"
        Me.LabelEjemplarAdicional.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UTEjemplarAdicional
        '
        Me.UTEjemplarAdicional.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEjemplarAdicional.Enabled = False
        Me.UTEjemplarAdicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEjemplarAdicional.Location = New System.Drawing.Point(448, 598)
        Me.UTEjemplarAdicional.Name = "UTEjemplarAdicional"
        Me.UTEjemplarAdicional.Size = New System.Drawing.Size(34, 24)
        Me.UTEjemplarAdicional.TabIndex = 10
        '
        'LabelCaratula
        '
        Me.LabelCaratula.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelCaratula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelCaratula.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelCaratula.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelCaratula.Location = New System.Drawing.Point(6, 509)
        Me.LabelCaratula.Name = "LabelCaratula"
        Me.LabelCaratula.Size = New System.Drawing.Size(108, 21)
        Me.LabelCaratula.TabIndex = 110
        Me.LabelCaratula.Text = "Cliente:"
        Me.LabelCaratula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TBCaratula
        '
        Me.TBCaratula.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.TBCaratula.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBCaratula.Location = New System.Drawing.Point(120, 506)
        Me.TBCaratula.Name = "TBCaratula"
        Me.TBCaratula.Size = New System.Drawing.Size(362, 24)
        Me.TBCaratula.TabIndex = 111
        '
        'GBTrabajo
        '
        Me.GBTrabajo.Controls.Add(Me.UTECodigoBarra)
        Me.GBTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GBTrabajo.Location = New System.Drawing.Point(700, 287)
        Me.GBTrabajo.Name = "GBTrabajo"
        Me.GBTrabajo.Size = New System.Drawing.Size(234, 50)
        Me.GBTrabajo.TabIndex = 112
        Me.GBTrabajo.TabStop = False
        Me.GBTrabajo.Text = "Código de Barras (Buscar Trabajo)"
        '
        'UTECodigoBarra
        '
        Me.UTECodigoBarra.AutoSize = False
        Appearance25.Image = CType(resources.GetObject("Appearance25.Image"), Object)
        Appearance25.ImageHAlign = Infragistics.Win.HAlign.Center
        Appearance25.ImageVAlign = Infragistics.Win.VAlign.Middle
        EditorButton4.Appearance = Appearance25
        EditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button3D
        EditorButton4.Key = "Right"
        Appearance26.Image = CType(resources.GetObject("Appearance26.Image"), Object)
        Appearance26.ImageBackgroundDisabled = CType(resources.GetObject("Appearance26.ImageBackgroundDisabled"), System.Drawing.Image)
        EditorButton4.PressedAppearance = Appearance26
        EditorButton4.Width = 30
        Me.UTECodigoBarra.ButtonsRight.Add(EditorButton4)
        Me.UTECodigoBarra.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTECodigoBarra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTECodigoBarra.Location = New System.Drawing.Point(6, 17)
        Me.UTECodigoBarra.Name = "UTECodigoBarra"
        Me.UTECodigoBarra.Size = New System.Drawing.Size(222, 29)
        Me.UTECodigoBarra.TabIndex = 33
        '
        'UNElegali
        '
        Appearance27.TextHAlignAsString = "Center"
        Me.UNElegali.Appearance = Appearance27
        Me.UNElegali.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UNElegali.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UNElegali.Location = New System.Drawing.Point(120, 344)
        Me.UNElegali.Name = "UNElegali"
        Me.UNElegali.ReadOnly = True
        Me.UNElegali.Size = New System.Drawing.Size(185, 24)
        Me.UNElegali.TabIndex = 113
        '
        'UTEEstado
        '
        Me.UTEEstado.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTEEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTEEstado.Location = New System.Drawing.Point(6, 16)
        Me.UTEEstado.Name = "UTEEstado"
        Me.UTEEstado.ReadOnly = True
        Me.UTEEstado.Size = New System.Drawing.Size(305, 24)
        Me.UTEEstado.TabIndex = 115
        '
        'ButtonFinalizar
        '
        Me.ButtonFinalizar.BackColor = System.Drawing.Color.Transparent
        Me.ButtonFinalizar.Enabled = False
        Me.ButtonFinalizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonFinalizar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Complete_and_ok_32xMD_color
        Me.ButtonFinalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButtonFinalizar.Location = New System.Drawing.Point(314, 10)
        Me.ButtonFinalizar.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonFinalizar.Name = "ButtonFinalizar"
        Me.ButtonFinalizar.Size = New System.Drawing.Size(123, 34)
        Me.ButtonFinalizar.TabIndex = 116
        Me.ButtonFinalizar.Text = "Finalizado"
        Me.ButtonFinalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ButtonFinalizar.UseVisualStyleBackColor = False
        '
        'GBEstados
        '
        Me.GBEstados.Controls.Add(Me.UTECertificado)
        Me.GBEstados.Controls.Add(Me.ButtonEntregar)
        Me.GBEstados.Controls.Add(Me.ButtonFinalizar)
        Me.GBEstados.Controls.Add(Me.UTEEstado)
        Me.GBEstados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GBEstados.Location = New System.Drawing.Point(488, 534)
        Me.GBEstados.Name = "GBEstados"
        Me.GBEstados.Size = New System.Drawing.Size(446, 87)
        Me.GBEstados.TabIndex = 117
        Me.GBEstados.TabStop = False
        Me.GBEstados.Text = "Estados del trabajo"
        '
        'UTECertificado
        '
        Me.UTECertificado.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.UTECertificado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UTECertificado.Location = New System.Drawing.Point(6, 52)
        Me.UTECertificado.Name = "UTECertificado"
        Me.UTECertificado.ReadOnly = True
        Me.UTECertificado.Size = New System.Drawing.Size(305, 24)
        Me.UTECertificado.TabIndex = 118
        '
        'ButtonEntregar
        '
        Me.ButtonEntregar.BackColor = System.Drawing.Color.Transparent
        Me.ButtonEntregar.Enabled = False
        Me.ButtonEntregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ButtonEntregar.Image = Global.cpceMEGS.My.Resources.Resources.StatusAnnotations_Alert_32xMD_color
        Me.ButtonEntregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ButtonEntregar.Location = New System.Drawing.Point(314, 44)
        Me.ButtonEntregar.Margin = New System.Windows.Forms.Padding(0)
        Me.ButtonEntregar.Name = "ButtonEntregar"
        Me.ButtonEntregar.Size = New System.Drawing.Size(123, 34)
        Me.ButtonEntregar.TabIndex = 117
        Me.ButtonEntregar.Text = "Entregado"
        Me.ButtonEntregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ButtonEntregar.UseVisualStyleBackColor = False
        '
        'FrmObleas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(934, 670)
        Me.Controls.Add(Me.GBEstados)
        Me.Controls.Add(Me.UNElegali)
        Me.Controls.Add(Me.GBTrabajo)
        Me.Controls.Add(Me.TBCaratula)
        Me.Controls.Add(Me.LabelCaratula)
        Me.Controls.Add(Me.UTEjemplarAdicional)
        Me.Controls.Add(Me.LabelEjemplarAdicional)
        Me.Controls.Add(Me.CBEjemplar)
        Me.Controls.Add(Me.UTEMtrImp)
        Me.Controls.Add(Me.UTEImpreso)
        Me.Controls.Add(Me.CBtEditar)
        Me.Controls.Add(Me.UGRecibos)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.UTEComitente)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CbtImprimirObleas)
        Me.Controls.Add(Me.CbtActualizarObleas)
        Me.Controls.Add(Me.CbtAltaOlbeas)
        Me.Controls.Add(Me.UTEprofesional)
        Me.Controls.Add(Me.UTEMtra)
        Me.Controls.Add(Me.UNEoblea)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.UcboTrabajo)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.UDTotorga)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.UDTCertif)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.LabelComitente)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.UDTfecha)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.UNCopias)
        Me.Controls.Add(Me.UGobleas)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmObleas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mantenimiento de Obleas"
        CType(Me.UGobleas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEprofesional, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEMtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNEoblea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UcboTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTotorga, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTCertif, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTfecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNCopias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEComitente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.UNLegaliAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTESucursalSiglas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEbuscaLegali, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTHasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UDTDesde, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UGRecibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEImpreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEMtrImp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEjemplarAdicional, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TBCaratula, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBTrabajo.ResumeLayout(False)
        CType(Me.UTECodigoBarra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UNElegali, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UTEEstado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GBEstados.ResumeLayout(False)
        Me.GBEstados.PerformLayout()
        CType(Me.UTECertificado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UGobleas As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UTEprofesional As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UTEMtra As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UNEoblea As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents UcboTrabajo As Infragistics.Win.UltraWinGrid.UltraCombo
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents UDTotorga As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents UDTCertif As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents LabelComitente As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents UDTfecha As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents UNCopias As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents CbtImprimirObleas As System.Windows.Forms.Button
    Friend WithEvents CbtActualizarObleas As System.Windows.Forms.Button
    Friend WithEvents CbtAltaOlbeas As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UTEComitente As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UTEbuscaLegali As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UDTHasta As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents UDTDesde As Infragistics.Win.UltraWinEditors.UltraDateTimeEditor
    Friend WithEvents ppdDialog As System.Windows.Forms.PrintDialog
    Friend WithEvents ppd As System.Drawing.Printing.PrintDocument
    Friend WithEvents UGRecibos As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents CBtEditar As System.Windows.Forms.Button
    Friend WithEvents UTEImpreso As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UTEMtrImp As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents CBtExportar As System.Windows.Forms.Button
    Friend WithEvents CBEjemplar As System.Windows.Forms.Button
    Friend WithEvents LabelEjemplarAdicional As System.Windows.Forms.Label
    Friend WithEvents UTEjemplarAdicional As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents LabelCaratula As System.Windows.Forms.Label
    Friend WithEvents TBCaratula As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents GBTrabajo As System.Windows.Forms.GroupBox
    Friend WithEvents UTECodigoBarra As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UNElegali As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UTESucursalSiglas As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents UNLegaliAnio As Infragistics.Win.UltraWinEditors.UltraNumericEditor
    Friend WithEvents UTEEstado As Infragistics.Win.UltraWinEditors.UltraTextEditor
    Friend WithEvents ButtonFinalizar As Button
    Friend WithEvents GBEstados As GroupBox
    Friend WithEvents ButtonEntregar As Button
    Friend WithEvents UTECertificado As Infragistics.Win.UltraWinEditors.UltraTextEditor
End Class
