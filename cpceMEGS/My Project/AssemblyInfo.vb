﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("cpceMEGS")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("CPCE")> 
<Assembly: AssemblyProduct("cpceSistema")> 
<Assembly: AssemblyCopyright("Copyright © CPCE 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("c533afc2-31b4-4b55-ac46-a2ec8f33f66c")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("es-AR")> 