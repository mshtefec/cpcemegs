﻿Imports MySql.Data.MySqlClient
Imports System.Drawing.Printing

Public Class Impresiones
    Private cnn As New ConsultaBD(True)
    Private DTImprimir As DataTable
    Private DTImpDeta As DataTable
    Private DTAsiento As DataTable
    Private DAImprimir As MySqlDataAdapter
    Private n_NroAsiento As Long
    Private DTCheque As DataTable
    Private DACheque As MySqlDataAdapter
    'tipoPagina por defecto es A4
    Public Sub ImprimirAsiento(ByVal nNroAsiento As Long, ByVal cProceso As String, Optional ByVal tipoPagina As String = "", Optional ByVal imprimirDirecto As Boolean = False)
        n_NroAsiento = nNroAsiento
        Select Case cProceso
            Case "02LIHN"
                ImprimeLiquidacion(tipoPagina)
            Case "02H001", "02P035"
                ImprimeOrdenPago(tipoPagina)
            Case "CIERRE"
                ImprimeCierre(tipoPagina)
            Case "RESULT"
                ImprimeCierre(tipoPagina)
            Case "APERT"
                ImprimeCierre(tipoPagina)
            Case Else
                ImprimeAsiento(tipoPagina, imprimirDirecto)
        End Select
    End Sub

    Private Sub ImprimeLiquidacion(Optional ByVal tipoPagina As String = "")
        Dim frmImprimir As New FrmReportes(AltaDatosLiquidacion(), , "CRLiquidacionHonorarios" & tipoPagina & ".rpt", "Liquidación de Honorarios")
        frmImprimir.ShowDialog()
    End Sub

    Private Sub ImprimeAsiento(Optional ByVal tipoPagina As String = "", Optional ByVal imprimirDirecto As Boolean = False)
        Try
            DAImprimir = cnn.consultaBDadapter(
                "(((((comproba INNER JOIN procesos ON pro_codigo=com_proceso) INNER JOIN farcat ON far_unegos=com_unegos AND far_nrocli=com_nrocli ) INNER JOIN operador ON ope_nroope=com_nroope) INNER JOIN totales ON tot_nroasi=com_nroasi AND tot_unegos=com_unegos AND tot_nrocli=com_nrocli) INNER JOIN plancuen ON pla_nropla=tot_nropla) LEFT JOIN afiliado ON afi_tipdoc=tot_tipdoc AND afi_nrodoc=tot_nrodoc",
                "far_nombre AS institucion,far_sucursal AS delegacion,com_proceso AS proceso,CAST(com_nrocom as char) as comprobante,com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip,DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha,DATE_FORMAT(CAST(tot_fecven AS char),'%d/%m/%Y') as vencimiento,IF(com_concepto1='',pro_cpto1, com_concepto1) AS conceptos1,com_concepto2 AS conceptos2,com_concepto3 AS conceptos3,CAST(tot_item AS char) as item,tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip,tot_nrocuo as itemsubcta,if(com_destinatario<>'','',CONCAT(afi_titulo,CAST(afi_matricula AS char))) as subcta,if(com_destinatario<>'',com_destinatario,afi_nombre) as subctadescrip,tot_debe as debe,tot_haber as haber,CAST(tot_nrocheque AS CHAR) as cheque,tot_debe-tot_haber as impche,com_concepto3 as literal,pro_reporte" & nPubNroCli & ",tot_concepto AS desticheque, tot_estado AS estado",
                "com_nroasi=" & n_NroAsiento
            )

            DTAsiento = New DataTable
            DAImprimir.Fill(DTAsiento)
            '     DTAsiento.Rows(0).Item("pro_reporte") = "CRreciboNuevo"
            Dim frmImprimir As FrmReportes
            If DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim = "" Then
                If imprimirDirecto Then
                    tipoPagina = "A5"
                    frmImprimir = New FrmReportes(DTAsiento, , "CRAsiento" & tipoPagina & ".rpt", "Asiento")
                    frmImprimir.cargaReporte()
                    frmImprimir.myReporte.PrintToPrinter(1, False, 0, 0)
                Else
                    frmImprimir = New FrmReportes(DTAsiento, , "CRAsiento" & tipoPagina & ".rpt", "Asiento")
                    frmImprimir.ShowDialog()
                End If
            Else
                If DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim = "CRreciboNuevo" Or _
                   DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim = "CRreciboSinLogo" Then
                    RedefinoRecibo(DTAsiento)
                Else
                    RedefinoAsiento(DTAsiento)
                End If
                If imprimirDirecto Then
                    tipoPagina = "A5"
                    frmImprimir = New FrmReportes(DTAsiento, , DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim & tipoPagina & ".rpt", "Asiento")
                    frmImprimir.cargaReporte()
                    frmImprimir.myReporte.PrintToPrinter(1, False, 0, 0)
                Else
                    frmImprimir = New FrmReportes(DTAsiento, , DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim & tipoPagina & ".rpt", "Asiento")
                    frmImprimir.ShowDialog()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("No se puede imprimir,ERROR: " & ex.Message)
        End Try
    End Sub

    Private Sub ImprimeCierre(Optional ByVal tipoPagina As String = "")
        Try
            DAImprimir = cnn.consultaBDadapter(
                "(((((comproba inner join procesos on pro_codigo=com_proceso) inner join farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) inner join plancuen on pla_nropla=tot_nropla) left join afiliado on afi_tipdoc=tot_tipdoc and afi_nrodoc=tot_nrodoc",
                "far_nombre as institucion,far_sucursal as delegacion,com_proceso as proceso,com_nrocom as comprobante,com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip,DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha,DATE_FORMAT(CAST(tot_fecven AS char),'%d/%m/%Y') as vencimiento,com_concepto1 as conceptos1,com_concepto2 as conceptos2,com_concepto3 as conceptos3,CAST(tot_item AS char) as item,tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip,tot_nrocuo as itemsubcta,CONCAT(tot_subpla,' ',afi_titulo,CAST(afi_matricula AS char)) as subcta,afi_nombre as subctadescrip,sum(tot_debe) as debe,sum(tot_haber) as haber,CAST(tot_nrocheque AS CHAR) as cheque,SUM(tot_debe-tot_haber) as impche,com_concepto3 as literal,pro_reporte" & nPubNroCli & ",tot_concepto as desticheque",
                "com_nroasi=" & n_NroAsiento & " GROUP BY tot_nropla"
            )

            DTAsiento = New DataTable
            DAImprimir.Fill(DTAsiento)

            For x As Integer = DTAsiento.Rows.Count - 1 To 0 Step -1
                If DTAsiento.Rows(x).Item("impche") = 0 Then
                    DTAsiento.Rows.RemoveAt(x)
                ElseIf DTAsiento.Rows(x).Item("impche") > 0 Then
                    DTAsiento.Rows(x).Item("debe") = DTAsiento.Rows(x).Item("impche")
                    DTAsiento.Rows(x).Item("haber") = 0
                Else
                    DTAsiento.Rows(x).Item("debe") = 0
                    DTAsiento.Rows(x).Item("haber") = DTAsiento.Rows(x).Item("impche") * -1
                End If
            Next

            If DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim = "" Then
                Dim frmImprimir As New FrmReportes(DTAsiento, , "CRAsiento" & tipoPagina & ".rpt", "Asiento")
                frmImprimir.ShowDialog()
            Else
                RedefinoAsiento(DTAsiento)

                Dim frmImprimir As New FrmReportes(DTAsiento, , DTAsiento.Rows(0).Item("pro_reporte" & nPubNroCli).ToString.Trim & tipoPagina & ".rpt", "Asiento")
                frmImprimir.ShowDialog()
            End If
        Catch ex As Exception
            MessageBox.Show("No se puede imprimir,ERROR: " & ex.Message)
        End Try
    End Sub

    Private Sub RedefinoAsiento(ByRef DTAsiento As DataTable)
        Dim literal As New NumLetra
        Dim nSumaPagos As Double = 0
        For Each rOP As DataRow In DTAsiento.Rows

            If rOP.Item("debe") > 0 Then
                rOP.Item("proceso") = "FORMA DE PAGOS:"
                rOP.Item("item") = 1
                nSumaPagos += rOP.Item("debe")
            Else
                rOP.Item("proceso") = "EN CONCEPTO:"
                rOP.Item("item") = 0
            End If
            Select Case rOP("cuenta")
                Case "11010101"
                    rOP.Item("cuentadescrip") = "Efectivo : ............................................................................  " ' & FormatCurrency(Monto, 2)
                Case "11010102"
                    rOP.Item("cuentadescrip") = "Cheque        Banco                          " ' , prFont, Brushes.Black, LeyendaX, LeyendaY)
                Case "13032400", "11010103"
                    rOP.Item("cuentadescrip") = "Tarjeta  : ............................................................................  " ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                Case "11031700"
                    rOP.Item("cuentadescrip") = "Deposito : ............................................................................  " '& FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                Case "11030900"
                    rOP.Item("cuentadescrip") = "Deposito : ............................................................................  " '& FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                Case "21010300"
                    rOP.Item("cuentadescrip") = "Reintegro Honorarios : ................................................................  "  ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
            End Select
        Next
        For Each rOP As DataRow In DTAsiento.Rows
            rOP.Item("literal") = FormatCurrency(nSumaPagos, 2) & " (SON PESOS:" & literal.Convertir(FormatNumber(nSumaPagos) & ")", True)
        Next
    End Sub

    Private Sub RedefinoRecibo(ByRef DTAsiento As DataTable)
        Dim literal As New NumLetra
        Dim nSumaPagos As Double = 0
        Dim cCuoCancel As String = ""

        If DTAsiento.Rows(0).Item("conceptos3").ToString.Trim = "" Then
            Dim DTCuoCancel As New DataTable
            Dim DTProceso As New DataTable
            DAImprimir = cnn.consultaBDadapter("totales", , "tot_asicancel=" & n_NroAsiento)
            DAImprimir.Fill(DTCuoCancel)
            For Each rowCuCan As DataRow In DTCuoCancel.Rows
                DAImprimir = cnn.consultaBDadapter("procesos", , "pro_codigo='" & rowCuCan.Item("tot_proceso") & "'")
                DTProceso = New DataTable
                DAImprimir.Fill(DTProceso)
                If DTAsiento.Rows(0).Item("proceso") = DTProceso.Rows(0).Item("pro_procancela") Then
                    If cCuoCancel.Trim = "" Then
                        cCuoCancel = "Cuota/s N°: "
                    Else
                        cCuoCancel = cCuoCancel & " - "
                    End If
                    cCuoCancel = cCuoCancel & rowCuCan.Item("tot_nrocuo").ToString & rowCuCan.Item("tot_fecven").ToString.Substring(5, 5)
                End If
            Next
        End If

        ' busco cual es el matriculado
        Dim cMtrImp As String = ""
        Dim cMtrDestinatario As String = ""
        For Each rOP As DataRow In DTAsiento.Rows
            If rOP.Item("subcta").ToString() <> "" And rOP.Item("subcta").ToString() <> "0" Then
                cMtrImp = rOP.Item("subcta")
            End If
            If rOP.Item("subctadescrip").ToString() <> "" And rOP.Item("subctadescrip").ToString() <> "0" Then
                cMtrDestinatario = rOP.Item("subctadescrip")
            End If
        Next

        For Each rOP As DataRow In DTAsiento.Rows
            'If cConcepto1.Trim <> "" Then
            '    rOP.Item("conceptos1") = cConcepto1
            'End If
            rOP.Item("subcta") = cMtrImp
            rOP.Item("subctadescrip") = cMtrDestinatario

            If cCuoCancel.Trim <> "" Then
                rOP.Item("conceptos3") = cCuoCancel
            End If

            If rOP.Item("debe") > 0 Then
                Select Case rOP("cuenta")
                    Case "11010101"
                        rOP.Item("cuentadescrip") = "Efectivo : ............................................................................  " ' & FormatCurrency(Monto, 2)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "11010102"
                        rOP.Item("cuentadescrip") = "Cheque        Banco                          " ' , prFont, Brushes.Black, LeyendaX, LeyendaY)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "13032400", "11010103"
                        rOP.Item("cuentadescrip") = "Tarjeta  : ............................................................................  " ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "11031700"
                        rOP.Item("cuentadescrip") = "Deposito : ............................................................................  " '& FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "11030801", "11031001"
                        rOP.Item("cuentadescrip") = "Debito   : ............................................................................  " '& FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "11030900"
                        rOP.Item("cuentadescrip") = "Deposito : ............................................................................  " '& FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "21010300"
                        rOP.Item("cuentadescrip") = "Reintegro Honorarios : ................................................................  "  ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "13070100"
                        rOP.Item("cuentadescrip") = "Reintegro Honorarios : ................................................................  "  ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case "11031000", "11030800"
                        rOP.Item("cuentadescrip") = "Transferencia : .......................................................................  "  ' & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        rOP.Item("proceso") = "FORMA DE PAGOS:"
                        rOP.Item("item") = 1
                        nSumaPagos += rOP.Item("debe")
                    Case Else
                        rOP.Item("item") = 0
                End Select
            Else
                rOP.Item("proceso") = "EN CONCEPTO:"
                rOP.Item("item") = 0
            End If
        Next
        For Each rOP As DataRow In DTAsiento.Rows
            rOP.Item("literal") = FormatCurrency(nSumaPagos, 2) & " (SON PESOS:" & literal.Convertir(FormatNumber(nSumaPagos) & ")", True)
        Next
    End Sub

    Private Sub ImprimeOrdenPago(Optional ByVal tipoPagina As String = "")
        Dim DTInst As New DataTable
        Dim DTAsiGrupal As New DataTable
        DAImprimir = cnn.consultaBDadapter(
            "(comproba inner join procesos on pro_codigo=com_proceso) INNER JOIN totales ON tot_unegos=com_unegos and tot_nrocli=com_nrocli and tot_nroasi=com_nroasi", ,
            "com_nroasi=" & n_NroAsiento
        )
        DAImprimir.Fill(DTAsiGrupal)
        DAImprimir = cnn.consultaBDadapter("farcat", , "far_unegos=" & DTAsiGrupal.Rows(0).Item("com_unegos") & " and far_nrocli=" & DTAsiGrupal.Rows(0).Item("com_nrocli"))
        DAImprimir.Fill(DTInst)
        If DTAsiGrupal.Rows(0).Item("com_asigrupal") = 0 Then
            DAImprimir = cnn.consultaBDadapter(
                "((((((comproba inner join procesos on pro_codigo=com_proceso) inner join farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) inner join plancuen on pla_nropla=tot_nropla) left join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc) left join afiliado a on a.afi_tipdoc=tot_tipdes and a.afi_nrodoc=tot_nrodes",
                "far_nombre as institucion,far_sucursal as delegacion,com_proceso as proceso,com_nrocom as comprobante,com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip,DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha,com_concepto1 as conceptos,tot_item as item,tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip,tot_item as itemsubcta,tot_subpla as subcta,afiliado.afi_nombre as subctadescrip,concat(tot_titulo,tot_matricula) as Matricula,if(tot_debe>0,tot_imppag,0) as debe,if(tot_haber>0,tot_imppag,0) as haber,CONCAT(tot_letcheque,CAST(tot_nrocheque AS CHAR)) as cheque,tot_debe+tot_haber as impche,CONCAT(a.afi_titulo,CAST(a.afi_matricula AS char),' ',a.afi_nombre) as desticheque,com_concepto1 as literal,com_total as total,a.afi_cbu_credito AS cbucredito, tot_estado AS estado",
                "com_nroasi=" & n_NroAsiento
            )
        Else
            DAImprimir = cnn.consultaBDadapter(
                "((((((comproba inner join procesos on pro_codigo=com_proceso) inner join farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) inner join plancuen on pla_nropla=tot_nropla) left join afiliado on afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc) left join afiliado a on a.afi_tipdoc=tot_tipdes and a.afi_nrodoc=tot_nrodes",
                "far_nombre as institucion,far_sucursal as delegacion,com_proceso as proceso,com_nrocom as comprobante,tot_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip,DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha,com_concepto1 as conceptos,tot_item as item,tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip,tot_item as itemsubcta,tot_subpla as subcta,afiliado.afi_nombre as subctadescrip,concat(tot_titulo,tot_matricula) as Matricula,if(tot_debe>0,tot_imppag,0) as debe,if(tot_haber>0,tot_imppag,0) as haber,CONCAT(tot_letcheque,CAST(tot_nrocheque AS CHAR)) as cheque,tot_debe+tot_haber as impche,CONCAT(a.afi_titulo,CAST(a.afi_matricula AS char),' ',a.afi_nombre) as desticheque,com_concepto1 as literal,com_total as total,a.afi_cbu_credito AS cbucredito, tot_estado AS estado",
                "com_asigrupal=" & DTAsiGrupal.Rows(0).Item("com_asigrupal")
            )
        End If
        DTAsiento = New DataTable
        DAImprimir.Fill(DTAsiento)

        Dim literal As New NumLetra
        Dim nItem As Integer = 0
        Dim lchequeEmitido As Boolean = False
        For Each rOP As DataRow In DTAsiento.Rows
            nItem += 1
            rOP.Item("item") = nItem
            If rOP.Item("cheque").ToString.Length > 1 Then
                lchequeEmitido = True
            End If
            rOP.Item("institucion") = DTInst.Rows(0).Item("far_nombre")
            rOP.Item("comprobante") = DTAsiGrupal.Rows(0).Item("com_nrocom")
            rOP.Item("procedescrip") = DTAsiGrupal.Rows(0).Item("pro_nombre")
            rOP.Item("conceptos") = ""
            rOP.Item("literal") = literal.Convertir(FormatNumber(DTAsiGrupal.Rows(DTAsiGrupal.Rows.Count - 1).Item("tot_haber")), True)
        Next
        If lchequeEmitido Then
        Else
            For Each rOP As DataRow In DTAsiento.Rows
                rOP.Item("conceptos") = "Orden de Pago en proceso de confirmacion. NO se ha emitido el cheque"
            Next
        End If
        Dim frmImprimir As New FrmReportes(DTAsiento, , "CROrdenPago" & tipoPagina & ".rpt", "Orden de Pago")
        frmImprimir.ShowDialog()
    End Sub

    Private Function AltaDatosLiquidacion() As DataTable
        DTImprimir = New DataTable
        DAImprimir = cnn.consultaBDadapter(
            "comproba
            INNER JOIN afiliado ON afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc
            LEFT JOIN trabajo ON com_nroasi=tra_nroasi_liquidacion", ,
            "com_nroasi=" & n_NroAsiento
        )
        DAImprimir.Fill(DTImprimir)
        Dim DTTarea As New DataTable
        DAImprimir = cnn.consultaBDadapter(
            "tareas INNER JOIN plancuen on pla_nropla=tar_ctaing", ,
            "tar_codigo=" & DTImprimir.Rows(0).Item("com_tarea")
        )
        DAImprimir.Fill(DTTarea)
        Dim DTComitente As New DataTable
        DAImprimir.Dispose()
        DAImprimir = cnn.consultaBDadapter(
            "comitente", ,
            "afi_tipdoc='" & DTImprimir.Rows(0).Item("com_tipcmt") & "' and afi_nrodoc=" & DTImprimir.Rows(0).Item("com_nrocmt")
        )
        DAImprimir.Fill(DTComitente)
        DTImpDeta = New DataTable
        DAImprimir.Dispose()
        DAImprimir = cnn.consultaBDadapter(
            "(totales LEFT JOIN hconcept on hco_concepto=tot_codretencion) INNER JOIN plancuen on pla_nropla=tot_nropla", ,
            "tot_nroasi=" & DTImprimir.Rows(0).Item("com_nroasi")
        )
        DAImprimir.Fill(DTImpDeta)
        Dim DTliquida As New DataTable
        With DTliquida
            .Columns.Add("liquidacion")
            .Columns.Add("profesional")
            .Columns.Add("cuit")
            .Columns.Add("direccion")
            .Columns.Add("trabajo")
            .Columns.Add("comitente")
            .Columns.Add("fecha")
            .Columns.Add("codigo")
            .Columns.Add("concepto")
            .Columns.Add("porcentaje")
            .Columns.Add("sobre")
            .Columns.Add("importe")
            .Columns.Add("reintegro")
            .Columns.Add("descriptrabajo")
            .Columns.Add("honortrabajo")
            .Columns.Add("iva")
            .Columns.Add("estado")
            .Columns.Add("id_codigo_trabajo")
        End With
        Dim rowLiq As DataRow
        For Each rowRet As DataRow In DTImpDeta.Rows
            rowLiq = DTliquida.NewRow
            rowLiq.Item("liquidacion") = DTImprimir.Rows(0).Item("com_nrocom")
            rowLiq.Item("profesional") = DTImprimir.Rows(0).Item("afi_matricula") & "-" & DTImprimir.Rows(0).Item("afi_nombre").ToString.Trim
            rowLiq.Item("cuit") = DTImprimir.Rows(0).Item("afi_cuit")
            rowLiq.Item("direccion") = DTImprimir.Rows(0).Item("afi_direccion").ToString.Trim & "-" & DTImprimir.Rows(0).Item("afi_localidad").ToString.Trim
            rowLiq.Item("trabajo") = DTImprimir.Rows(0).Item("com_nrotrabajo")
            rowLiq.Item("id_codigo_trabajo") = DTImprimir.Rows(0).Item("id")
            If DTComitente.Rows.Count = 1 Then
                rowLiq.Item("comitente") = DTComitente.Rows(0).Item("afi_nombre")
            End If

            rowLiq.Item("fecha") = DTImprimir.Rows(0).Item("com_fecha")
            If IsDBNull(rowRet.Item("hco_concepto")) Then
                rowLiq.Item("codigo") = rowRet.Item("tot_nropla")
                rowLiq.Item("concepto") = rowRet.Item("pla_nombre")
            Else
                rowLiq.Item("codigo") = rowRet.Item("tot_nropla")
                rowLiq.Item("concepto") = rowRet.Item("hco_descripcion")
            End If
            If rowRet.Item("tot_sobre") > 0 Then
                rowLiq.Item("porcentaje") = FormatNumber(rowRet.Item("tot_porcentaje"), 2, TriState.True, TriState.True) & "%"
                rowLiq.Item("sobre") = FormatNumber(rowRet.Item("tot_sobre"), 2, TriState.True, TriState.True)
            End If
            rowLiq.Item("importe") = FormatNumber(rowRet.Item("tot_debe") + rowRet.Item("tot_haber"), 2, TriState.True, TriState.True)
            rowLiq.Item("reintegro") = FormatCurrency(DTImpDeta.Rows(DTImpDeta.Rows.Count - 1).Item("tot_haber"), 2, TriState.True, TriState.True) ' FormatCurrency(DTImprimir.Rows(0).Item("com_total"), , TriState.True, TriState.True)
            rowLiq.Item("descriptrabajo") = DTTarea.Rows(0).Item("tar_descrip")
            rowLiq.Item("honortrabajo") = FormatCurrency(DTImpDeta.Rows(DTImpDeta.Rows.Count - 1).Item("tot_haber"), 2, TriState.True, TriState.True) ' DTImprimir.Rows(0).Item("com_imppag")
            rowLiq.Item("iva") = FormatCurrency(rowRet.Item("tot_iva"))
            rowLiq.Item("estado") = rowRet.Item("tot_estado")
            DTliquida.Rows.Add(rowLiq)
         Next
        Return DTliquida
    End Function
    Public Sub ImprimirRecibo(ByVal nAsiento As Long)
        ' Usamos una clase del tipo PrintDocument
        Dim printDoc As New PrintDocument

        n_NroAsiento = nAsiento

        AddHandler printDoc.PrintPage, AddressOf ImpresionPrint_PrintPage
        ' la configuración a usar en la impresión
        printDoc.Print()
    End Sub

    Private Sub ImpresionPrint_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
        Dim NL As New NumLetra
        Dim NroReciboX, NroReciboY, FechaX, FechaY, AfiliadoX, AfiliadoY, MatriculaX, MatriculaY As Double
        Dim NroCliX, NroCliY, MontoX, MontoY, MontoLetrasX, MontoLetrasY As Double
        Dim Conceptos1X, Conceptos2X, Conceptos3X, Conceptos1Y, Conceptos2Y, Conceptos3Y As Double
        Dim Monto1X, Monto1Y As Double
        Dim LeyendaX, LeyendaY As Double
        Dim ProcesoX, ProcesoY As Double
        Dim ChequeX, ChequeY, BancoX, BancoY, SucX, SucY, MontoCHX, MontoCHY As Double
        Dim OperadorX, OperadorY As Double
        Dim InstitucionX, InstitucionY As Double
        Dim PiePaginaX, PiePaginaY As Double
        Dim PiePagina1X, PiePagina2Y As Double
        Dim Banco, Cheque, Suc As String
        Dim Monto, nTotal As Double
        Dim Letra As String
        Banco = ""
        'Inicializo las Variables
        'a ojo saque estos valores
        NroReciboX = 500
        NroReciboY = 9
        InstitucionX = 580
        InstitucionY = 9
        FechaX = 580
        FechaY = 45
        AfiliadoX = 55
        AfiliadoY = 130
        MatriculaX = 300
        MatriculaY = 130
        NroCliX = 50
        NroCliY = 158
        MontoX = 70
        MontoY = 210
        MontoLetrasX = 180
        MontoLetrasY = 210
        Conceptos1X = 70
        Conceptos1Y = 255
        Conceptos2X = 70
        Conceptos2Y = 270
        Conceptos3X = 70
        Conceptos3Y = 285
        Monto1X = 20
        Monto1Y = 309
        LeyendaX = 20
        LeyendaY = 330
        ProcesoX = 70
        ProcesoY = 236

        ChequeX = 20
        ChequeY = 350

        BancoX = 120
        BancoY = 350
        SucX = 305
        SucY = 350
        MontoCHX = 465
        MontoCHY = 350

        PiePaginaX = 20
        PiePaginaY = 500
        PiePaginaX = 20
        PiePaginaY = 500
        PiePagina1X = 20
        PiePagina2Y = 510
        OperadorX = 500
        OperadorY = 99
        ' La fuente a usar
        Dim prFont As New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

        Try
            DAImprimir = cnn.consultaBDadapter(
                "(((((comproba inner join procesos on pro_codigo=com_proceso) inner join " &
                "farcat on far_unegos=com_unegos and far_nrocli=com_nrocli ) inner join  " &
                "operador on ope_nroope=com_nroope) inner join totales on tot_nroasi=com_nroasi) " &
                "inner join plancuen on pla_nropla=tot_nropla) left join afiliado on " &
                "afi_tipdoc=com_tipdoc and afi_nrodoc=com_nrodoc", "far_nombre as institucion, " &
                "far_nrocli as delegacion, com_proceso as proceso,com_nrocom as comprobante, " &
                "com_nroasi as asiento,ope_nombre as operador,pro_nombre as procedescrip, " &
                "DATE_FORMAT(CAST(com_fecha AS char),'%d/%m/%Y %T') as fecha, " &
                "DATE_FORMAT(CAST(tot_fecven AS char),'%d/%m/%Y') as vencimiento, " &
                "com_concepto1 as conceptos1, com_concepto2 as conceptos2,com_concepto3 as conceptos3, " &
                "tot_item as item, tot_nrocuo as subitem,tot_nropla as cuenta,pla_nombre as cuentadescrip, " &
                "tot_nrocuo as itemsubcta,CONCAT(tot_subpla,' ',afi_titulo, CAST(afi_matricula AS char)) as subcta, " &
                "afi_nombre as subctadescrip, tot_debe as debe,tot_haber as haber,pro_reporte" & nPubNroCli & ", " &
                "afi_matricula as matricula,tot_tipdoc,tot_nrodoc,tot_titulo, " &
                "tot_matricula,com_total as total", "tot_debe > 0  and com_nroasi=" & n_NroAsiento
            )
            DTImprimir = New DataTable
            DAImprimir.Fill(DTImprimir)

            If DTImprimir.Rows.Count > 0 Then
                DACheque = cnn.consultaBDadapter(
                    "totales",
                    "tot_nrocheque as cheque, tot_debe-tot_haber as impche,tot_concepto as desticheque, tot_estche,tot_concepto as Banco",
                    "tot_nrocheque > 0 and tot_nroasi = " & n_NroAsiento
                )
                DTCheque = New DataTable
                DACheque.Fill(DTCheque)

                If DTCheque.Rows.Count > 0 Then
                    For Each rowCheque In DTCheque.Rows
                        If rowCheque("cheque") > 0 Then
                            Banco = rowCheque("banco")
                            Cheque = rowCheque("cheque")
                            Suc = "RCIA"
                            'imprimimo el nro del cheque
                            e.Graphics.DrawString(rowCheque("cheque"), prFont, Brushes.Black, ChequeX, ChequeY)
                            'imprimimo el banco del cheque
                            e.Graphics.DrawString(Banco, prFont, Brushes.Black, BancoX, BancoY)
                            'imprimimo la sucursal cheque
                            e.Graphics.DrawString(Suc, prFont, Brushes.Black, SucX, SucY)
                            'imprimimo el monto del cheque
                            e.Graphics.DrawString(FormatCurrency(rowCheque("impche"), 2), prFont, Brushes.Black, MontoCHX, MontoCHY)

                            ChequeY += 15
                            BancoY += 15
                            SucY += 15
                            MontoCHY += 15
                        End If
                    Next
                End If
                Dim row As DataRow = DTImprimir.Rows(0)
                'imprimimo el comprobante
                e.Graphics.DrawString(row("comprobante"), prFont, Brushes.Black, NroReciboX, NroReciboY)
                'imprimo la institucion
                If row("institucion") = "SIPRES" Then
                    e.Graphics.DrawString("(SIPRES)", prFont, Brushes.Black, InstitucionX, InstitucionY)
                Else
                    e.Graphics.DrawString(" ", prFont, Brushes.Black, InstitucionX, InstitucionY)
                End If
                'imprimimo la fecha         
                e.Graphics.DrawString(row("fecha"), prFont, Brushes.Black, FechaX, FechaY)
                'imprimimo el OEPRADOR
                e.Graphics.DrawString("Operador: " & row("operador"), prFont, Brushes.Black, OperadorX, OperadorY)
                'imprimimo el Afiliado 
                e.Graphics.DrawString(row("subctadescrip"), prFont, Brushes.Black, AfiliadoX, AfiliadoY)
                'imprimimo la matricula
                e.Graphics.DrawString("Matricula N°: " & row("matricula"), prFont, Brushes.Black, MatriculaX, MatriculaY)
                'imprimimo la delegacion
                'e.Graphics.DrawString(row("delegacion"), prFont, Brushes.Black, NroCliX, NroCliY)
                e.Graphics.DrawString(ceros1(row("delegacion"), 5), prFont, Brushes.Black, NroCliX, NroCliY)
                'imprimo la descripcion
                prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
                e.Graphics.DrawString(row("conceptos1"), prFont, Brushes.Black, Conceptos1X, Conceptos1Y)
                e.Graphics.DrawString(row("conceptos2"), prFont, Brushes.Black, Conceptos2X, Conceptos2Y)
                e.Graphics.DrawString(row("conceptos3"), prFont, Brushes.Black, Conceptos3X, Conceptos3Y)

                prFont = New Font("Microsoft Sans Serif", 10, FontStyle.Regular)

                For Each row In DTImprimir.Rows
                    Monto = row("debe")
                    Select Case row("cuenta")
                        Case "11010101"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Efectivo : ............................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        Case "11010102"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Cheque               Banco                                       Sucursal                            Importe ", prFont, Brushes.Black, LeyendaX, LeyendaY)
                        Case "13032400", "11010103"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Tarjeta  : ............................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        Case "11031700"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Deposito : ............................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        Case "11030900"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Deposito : ............................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        Case "21010300"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Reintegro Honorarios : ................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                        Case "13070100"
                            nTotal += row("debe")
                            e.Graphics.DrawString("Reintegro Honorarios : ................................................................  " & FormatCurrency(Monto, 2), prFont, Brushes.Black, Monto1X, Monto1Y)
                    End Select
                Next
                prFont = New Font("Microsoft Sans Serif", 9, FontStyle.Regular)
                'imprimimo el pie de pagina
                e.Graphics.DrawString("LOS CHEQUES RECIBIDOS SERAN ACREDITADOS A V/CUENTA PERSONAL UNA VEZ HECHOS EFECTIVOS", prFont, Brushes.Black, PiePaginaX, PiePaginaY)
                'imprimimo el monto
                e.Graphics.DrawString(FormatNumber(nTotal, 2), prFont, Brushes.Black, MontoX, MontoY)
                'imprimimo el monto en letras
                Letra = NL.Convertir(FormatNumber(nTotal), True)
                MontoLetrasX = (MontoX + Len(nTotal) + 60)
                e.Graphics.DrawString(" (SON PESOS: " & Letra & " )", prFont, Brushes.Black, MontoLetrasX, MontoLetrasY)
                'imprimimo el detalle DEL PROCESO
                e.Graphics.DrawString(row("procedescrip") & " (" & row("proceso") & ")", prFont, Brushes.Black, ProcesoX, ProcesoY)
                'indicamos que hemos llegado al final de la pagina
                e.HasMorePages = False
                n_NroAsiento = 0
            End If
        Catch ex As Exception
            e.Cancel = True
            MessageBox.Show("ERROR : " & ex.Message, "IMPRESION DE RECIBOS ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class
