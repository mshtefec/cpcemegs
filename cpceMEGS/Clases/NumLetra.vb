﻿
Imports System.Text.RegularExpressions

Public Class NumLetra

    Dim UNIDADES As String() = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "}
    Dim DECENAS As String() = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ", "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ", "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "}
    Dim CENTENAS As String() = {"", "ciento ", "doscientos ", "trescientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos "}

    Dim r As Regex

    Sub NumLetra()
    End Sub

    Public Function Convertir(ByVal numero As String, ByVal mayusculas As Boolean) As String
        Dim literal As String = ""
        Dim parte_decimal As String = ""
        'si el numero utiliza (.) en lugar de (,) -> se reemplaza
        'numero = Replace(Right(numero, 3), ".", ",")
        'si el numero no tiene parte decimal, se le agrega ,00        
        If numero.IndexOf(",") = -1 Then
            numero = numero & ",00"
        End If
        'se valida formato de entrada -> 0,00 y 999 999 999,00
        'if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {

        r = New Regex("\d{1,9},\d{1,2}")
        Dim mc As MatchCollection = r.Matches(numero)
        If mc.Count > 0 Then
            'se divide el numero 0000000,00 -> entero y decimal
            Dim Num As String() = numero.Split(",")
            'de da formato al numero decimal
            parte_decimal = Num(1) & "/100"
            'se convierte el numero a literal            
            If Num(0) = 0 Then
                literal = "cero "
            ElseIf Num(0) > 999999 Then
                literal = getMillones(Num(0))
            ElseIf Num(0) > 999 Then
                literal = getMiles(Num(0))
            ElseIf Num(0) > 99 Then
                literal = getCentenas(Num(0))
            ElseIf Num(0) > 9 Then
                literal = getDecenas(Num(0))
            Else
                literal = getUnidades(Num(0))
            End If
            'devuelve el resultado en mayusculas o minusculas
            If mayusculas Then
                Return (literal & "CON " & parte_decimal).ToUpper
            Else
                Return literal & "con " & parte_decimal
            End If
        Else
            Return ""
        End If

    End Function

    ' funciones para convertir los numeros a literales

    Private Function getUnidades(ByVal numero As String) As String '1 - 9
        'si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
        Dim num As String = numero.Substring(numero.Length - 1)
        Return UNIDADES(num)
    End Function

    Private Function getDecenas(ByVal numero As String) As String '99
        If numero < 10 Then 'para casos como -> 01 - 09
            Return getUnidades(numero)
        ElseIf numero > 29 Then 'para 30...99
            Dim u As String = getUnidades(numero)
            If u.Equals("") Then 'para 30,40,50,60,70,80,90
                Return DECENAS(numero.Substring(0, 1) + 8)
            Else
                Return DECENAS(numero.Substring(0, 1) + 8) & "y " & u
            End If
        ElseIf numero > 19 Then 'para 20...29
            Dim u As String = getUnidades(numero)
            If u.Equals("") Then 'para 20
                Return DECENAS(numero.Substring(0, 1) + 8)
            Else
                Return "veinti" & u
            End If
        Else
            Return DECENAS(numero - 10)
        End If
    End Function

    Private Function getCentenas(ByVal numero As String) As String
        ' si tiene cero a la izquierda los sacos
        numero = Convert.ToString(CInt(numero))
        If numero > 99 Then 'es centena
            If numero = 100 Then 'caso especial
                Return "cien "
            Else
                Return CENTENAS(numero.Substring(0, 1)) & getDecenas(numero.Substring(1))
            End If
        Else 'se quita el 0 antes de convertir a decenas
            Return getDecenas(numero)
        End If
    End Function


    Private Function getMiles(ByVal numero As String) As String
        'obtiene las centenas'
        Dim c As String = numero.Substring(numero.Length - 3)
        'obtiene los miles
        Dim m As String = numero.Substring(0, numero.Length - 3)
        Dim n As String = ""
        'se comprueba que miles tenga valor entero
        If m > 0 Then
            n = getCentenas(m)
            Return n & " mil " & getCentenas(c)
        Else
            Return "" & getCentenas(c)
        End If
    End Function

    Private Function getMillones(ByVal numero As String) As String
        'se obtiene los miles
        Dim miles As String = numero.Substring(numero.Length - 6)
        'millones
        Dim millon As String = numero.Substring(0, numero.Length - 6)
        Dim n As String = ""
        If millon > 9 Then
            n = getCentenas(millon) & " millones "
        Else
            n = getUnidades(millon) & " millon "
        End If
        Return n & getMiles(miles)
    End Function
End Class
