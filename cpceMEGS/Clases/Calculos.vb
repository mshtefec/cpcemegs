﻿Imports MySql.Data.MySqlClient
Public Class Calculos
    Private cnn As New ConsultaBD(True)
    Private DSCalculo As DataSet
    Private DACalculo As MySqlDataAdapter
    Private BSCalculo As BindingSource
    Private AportesCoincide As Boolean = True

    Public Function CalculoHonorarios(
        ByVal nTarea As Integer, ByVal nMonto As String, ByVal nMeses As Double,
        ByVal nPorcentajeSindico As Double, ByVal nImportePeriodo As Double, ByVal nTablaCalculosResolucion As String
    ) As Double
        Dim nCalculoHonorarios As Double = 0
        Dim nExcedente As Double = 0

        Select Case nTarea
            Case 4, 5, 17, 19, 20, 21, 23, 24, 26, 27, 28, 29, 30, 31 ' digitados
                nMonto = nMonto.ToString.Replace(".", ",")
                nCalculoHonorarios = Convert.ToDouble(nMonto)
            Case Else
                DSCalculo = New DataSet
                DACalculo = cnn.consultaBDadapter(nTablaCalculosResolucion, , nMonto & " between thc_mondes and thc_monhas")
                DACalculo.Fill(DSCalculo, "calculo")
                BSCalculo = New BindingSource
                BSCalculo.DataSource = DSCalculo.Tables(0)

                If DSCalculo.Tables(0).Rows.Count = 1 Then
                    nExcedente = IIf(Convert.ToDouble(nMonto.Replace(".", ",")) > DSCalculo.Tables(0).Rows(0).Item("thc_excedente"), Convert.ToDouble(nMonto.Replace(".", ",")) - DSCalculo.Tables(0).Rows(0).Item("thc_excedente"), 0)

                    nCalculoHonorarios = DSCalculo.Tables(0).Rows(0).Item("thc_fijo") + nExcedente * DSCalculo.Tables(0).Rows(0).Item("thc_adicporc") / 100
                    If nMeses > 0 Then
                        'If nTarea = 22 Then
                        '    nCalculoHonorarios = nCalculoHonorarios * (100 - nMeses) / 100
                        'Else
                        nCalculoHonorarios = nCalculoHonorarios / 12 * nMeses
                        'End If
                    End If
                    If nPorcentajeSindico <> 100 And nPorcentajeSindico <> 0 Then
                        nCalculoHonorarios = nCalculoHonorarios / 100 * nPorcentajeSindico
                    End If
                    If nCalculoHonorarios > nImportePeriodo Then
                        nCalculoHonorarios = nCalculoHonorarios - nImportePeriodo
                    Else
                        nCalculoHonorarios = 0
                    End If
                End If
        End Select
        Return nCalculoHonorarios
    End Function

    Public Function CalculoRetencion(ByRef dsRetenciones As DataSet, ByVal RowProf As DataRow, ByVal nMonto As Double, ByVal nMontoConIva As Double, ByVal nSoloMonto As Double, ByVal nMontoRecibo As Double, ByVal lSinfinesDeLucro As Boolean, ByVal lGanancia As Boolean, ByVal cFechaExpDGI As Date, ByVal cFechaExpDGR As Date, ByVal cFechaExpIVA As Date) As Double
        Dim DsGanac As New DataSet
        Dim DAGanac As MySqlDataAdapter
        Dim nCalculoRentecion As Double
        Dim sobre As Double
        Dim cPrimerDiaDelMes As String = Now.Year & "-" & Format(Now.Month, "00") & "-01 00:00:00"
        Dim cUltimoDiaDelMes As String = Now.Year & "-" & Format(Now.Month, "00") & "-" & Date.DaysInMonth(Now.Year, Now.Month) & " 23:59:59"
        'Obtengo la sumatoria de los Importes - Iva = Monto para calculo del 8%
        Dim nHonAcuMensual As Double = CalculoHonrariosMensuales("'21010100'", "02LIHN", RowProf, cPrimerDiaDelMes, cUltimoDiaDelMes) + nMontoRecibo
		'Obtengo la sumatoria del 8% ya calculado
		Dim nHonAcuMensualOchoPorciento As Double = CalculoHonrariosMensuales("'31010400','31010401','31010402','31010403','31010404','31010405','31010406','31010407','13071200'", "02LIHN", RowProf, cPrimerDiaDelMes, cUltimoDiaDelMes)
		'A la sumatoria del Acumulado Mensual le resto el Acumulado del 8%
		nHonAcuMensual -= nHonAcuMensualOchoPorciento
        Dim nRetAcuMensual As Double = CalculoRet143Mensuales("'21030100'", "02LIHN", RowProf, cPrimerDiaDelMes, cUltimoDiaDelMes)
        Dim n141 As Double
        Dim n142 As Double
        Dim n144 As Double
        Dim n145 As Double
		Dim n146 As Double
		Dim n147 As Double
		CalculoRetencion = 0
        Dim nMontoParaCertificacion As Double

        For Each rowRet As DataRow In dsRetenciones.Tables(0).Rows
            nCalculoRentecion = 0
            ' el rowRet.Item("porcentaje") saca de tabla hconcept
            Select Case rowRet.Item("thc_concepto")
                Case 0 ' bonificacion
                Case 141
                    sobre = nMonto ' por defecto

                    Select Case rowRet.Item("thc_tarea")
                        Case 1, 3, 6, 7, 9, 10, 11
                            If nMonto > nMontoConIva Then
                                nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            Else
                                nCalculoRentecion = nMontoConIva * rowRet.Item("porcentaje") / 100
                                sobre = nMontoConIva ' si el nMonto es menor al nMontoConIva
                            End If

                            If lSinfinesDeLucro Then
                                nCalculoRentecion = IIf(nCalculoRentecion >= rowRet.Item("thc_retsinlucro"), nCalculoRentecion, rowRet.Item("thc_retsinlucro"))
                            Else
                                nCalculoRentecion = IIf(nCalculoRentecion >= rowRet.Item("thc_retconlucro"), nCalculoRentecion, rowRet.Item("thc_retconlucro"))
                            End If
                        Case 4
                        Case 5, 24
                            nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                        Case 2, 8
                            If nMonto > rowRet.Item("thc_montomayor") Then
                                nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            Else
                                nCalculoRentecion = rowRet.Item("thc_retimporte")
                            End If
                        'Case 19
                            'nCalculoRentecion = rowRet.Item("thc_retimporte")
                        Case 19, 20, 21, 26, 27, 28, 29, 30, 31
                            If nSoloMonto > 0 Then
                                'Si entra es porque discrimino el aporte
                                sobre = nSoloMonto ' el monto sobre para el calculo es unicamente el monto solo.
                                nMontoParaCertificacion = nSoloMonto
                            Else
                                nMontoParaCertificacion = nMonto
                            End If

                            'YA NO APLICA EL MONTO MINIMO FIJO DEBAJO ESTA EL RESGUARDO
                            nCalculoRentecion = nMontoParaCertificacion * rowRet.Item("porcentaje") / 100
                            If nSoloMonto > 0 Then
                                If nSoloMonto <> nMontoParaCertificacion Then
                                    AportesCoincide = False
                                End If
                            End If

                            'RESGUARDO DE CALCULOS EN EL CASO QUE EL MONTO SEA MENOR A 100
                            'If nMontoParaCertificacion > rowRet.Item("thc_montomayor") Then
                            '	nCalculoRentecion = nMontoParaCertificacion * rowRet.Item("porcentaje") / 100
                            '	If nSoloMonto > 0 Then
                            '		If nSoloMonto <> nMontoParaCertificacion Then
                            '			AportesCoincide = False
                            '		End If
                            '	End If
                            'Else
                            '	nCalculoRentecion = rowRet.Item("thc_retimporte")
                            'End If

                            'Case 22 ' 8% sobre en neto depositado
                            'nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            'Case 17, 23
                            'nCalculoRentecion = rowRet.Item("thc_retimporte")
                    End Select
                    n141 = nCalculoRentecion
                    rowRet.Item("sobre") = sobre
                    rowRet.Item("importe") = nCalculoRentecion
                Case 142
                    'RENTAS
                    If cFechaExpDGR < Today Then
                        If nMontoRecibo > rowRet.Item("thc_retimporte") Then
                            nCalculoRentecion = (nMontoRecibo - (n141 + n147)) * rowRet.Item("porcentaje") / 100
                        End If
                    End If
                    rowRet.Item("sobre") = nMontoRecibo - (n141 + n147)
                    n142 = nCalculoRentecion
                    rowRet.Item("importe") = nCalculoRentecion
                Case 143
                    'GANANCIAS
                    ' Resto el 8% calculado anteriormente al acumulado
                    nHonAcuMensual -= n141 + n147
                    If RowProf.Item("afi_tipdoc") = "ECO" Then
                        If nHonAcuMensual > rowRet.Item("thc_retimporte") Then
                            nHonAcuMensual -= rowRet.Item("thc_retimporte")
                        End If
                    Else
                        If nHonAcuMensual > rowRet.Item("thc_montomayor") Then
                            nHonAcuMensual -= rowRet.Item("thc_montomayor")
                        End If
                    End If
                    If cFechaExpDGI < Today Then
                        If lGanancia Then
                            DAGanac = cnn.consultaBDadapter("thganac", , "thg_hasta >'" & nHonAcuMensual & "' and thg_desde<'" & nHonAcuMensual & "'")
                            DAGanac.Fill(DsGanac, "ganacia")
                            If DsGanac.Tables(0).Rows.Count > 0 Then
                                ' si es un estudio contable tiene que superar los 30000 se le hace 2%
                                If RowProf.Item("afi_tipdoc") = "ECO" Then
                                    If nHonAcuMensual > rowRet.Item("thc_retimporte") Then
                                        nHonAcuMensual -= (rowRet.Item("thc_retimporte") + n141)
                                        nCalculoRentecion = (nHonAcuMensual * rowRet.Item("thc_porcentaje") / 100) - nRetAcuMensual
                                    Else
                                        nHonAcuMensual = 0
                                        nCalculoRentecion = 0
                                    End If
                                    rowRet.Item("porcentaje") = rowRet.Item("thc_porcentaje")
                                Else
                                    If nHonAcuMensual > rowRet.Item("thc_montomayor") Then
                                        DAGanac = cnn.consultaBDadapter("thganac", , "thg_hasta >'" & nHonAcuMensual & "' and thg_desde<'" & nHonAcuMensual & "'")
                                        DAGanac.Fill(DsGanac, "ganacia")
                                        nCalculoRentecion = DsGanac.Tables(0).Rows(0).Item("thg_fijo") + ((nHonAcuMensual - DsGanac.Tables(0).Rows(0).Item("thg_excedente")) * DsGanac.Tables(0).Rows(0).Item("thg_porcentaje") / 100)
                                        nCalculoRentecion -= nRetAcuMensual
                                    Else
                                        nHonAcuMensual = 0
                                        nCalculoRentecion = 0
                                    End If
                                    rowRet.Item("porcentaje") = DsGanac.Tables(0).Rows(0).Item("thg_porcentaje")
                                End If
                                If nCalculoRentecion < 90 Then
                                    nHonAcuMensual = 0
                                    nCalculoRentecion = 0
                                End If
                            Else
                                nCalculoRentecion = 0
                            End If
                        End If
                    End If
                    rowRet.Item("sobre") = nHonAcuMensual
                    rowRet.Item("importe") = nCalculoRentecion
                Case 144
                    '10 % DE RENTAS
                    nCalculoRentecion = n142 * rowRet.Item("porcentaje") / 100
                    rowRet.Item("sobre") = n142
                    n144 = nCalculoRentecion
                    rowRet.Item("importe") = nCalculoRentecion
                Case 145
                    'IVA 14% solo cuando paga ganancia y supera 160 pesos
                    If lGanancia And cFechaExpIVA < Today Then
                        nCalculoRentecion = nMontoRecibo * rowRet.Item("porcentaje") / 100
                        rowRet.Item("sobre") = nMontoRecibo
                        If nCalculoRentecion > rowRet.Item("thc_retimporte") Then
                            n145 = nCalculoRentecion
                            rowRet.Item("importe") = n145
                        Else
                            nCalculoRentecion = 0
                            n145 = nCalculoRentecion
                            rowRet.Item("importe") = 0
                        End If
                    End If
                Case 146
                    'GANANCIAS
                    If Not lGanancia Then
                        If CalculoGanancias(rowRet.Item("tipdoc"), rowRet.Item("nrodoc")) >= rowRet.Item("hco_montoacum") Then
                            n146 = nMontoRecibo * rowRet.Item("porcentaje") / 100
                            nCalculoRentecion = n146
                        Else
                            nCalculoRentecion = 0
                            n146 = 0
                        End If
                        rowRet.Item("sobre") = nMontoRecibo
                        rowRet.Item("importe") = n146
                    End If
                Case 147
                    sobre = nMonto ' por defecto

                    Select Case rowRet.Item("thc_tarea")
                        Case 1, 3, 6, 7, 9, 10, 11
                            If nMonto > nMontoConIva Then
                                nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            Else
                                nCalculoRentecion = nMontoConIva * rowRet.Item("porcentaje") / 100
                                sobre = nMontoConIva ' si el nMonto es menor al nMontoConIva
                            End If

                            If lSinfinesDeLucro Then
                                nCalculoRentecion = IIf(nCalculoRentecion >= rowRet.Item("thc_retsinlucro"), nCalculoRentecion, rowRet.Item("thc_retsinlucro"))
                            Else
                                nCalculoRentecion = IIf(nCalculoRentecion >= rowRet.Item("thc_retconlucro"), nCalculoRentecion, rowRet.Item("thc_retconlucro"))
                            End If
                        Case 4
                        Case 5, 24
                            nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                        Case 2, 8
                            If nMonto > rowRet.Item("thc_montomayor") Then
                                nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            Else
                                nCalculoRentecion = rowRet.Item("thc_retimporte")
                            End If
                        Case 19, 20, 21, 26, 27, 28, 29, 30, 31
                            If nSoloMonto > 0 Then
                                'Si entra es porque discrimino el aporte
                                sobre = nSoloMonto ' el monto sobre para el calculo es unicamente el monto solo.
                                nMontoParaCertificacion = nSoloMonto
                            Else
                                nMontoParaCertificacion = nMonto
                            End If
                            If nMontoParaCertificacion > rowRet.Item("thc_montomayor") Then
                                nCalculoRentecion = nMontoParaCertificacion * rowRet.Item("porcentaje") / 100
                                If nSoloMonto > 0 Then
                                    If nSoloMonto <> nMontoParaCertificacion Then
                                        AportesCoincide = False
                                    End If
                                End If
                            Else
                                nCalculoRentecion = rowRet.Item("thc_retimporte")
                            End If
                            'Case 22 ' 8% sobre en neto depositado
                            'nCalculoRentecion = nMonto * rowRet.Item("porcentaje") / 100
                            'Case 17, 23
                            'nCalculoRentecion = rowRet.Item("thc_retimporte")
                    End Select
                    n147 = nCalculoRentecion
                    rowRet.Item("sobre") = sobre
                    rowRet.Item("importe") = nCalculoRentecion
            End Select

            CalculoRetencion += Math.Round(nCalculoRentecion, 2)
        Next
    End Function
    Public Function GetAporteCoincide() As Boolean
        GetAporteCoincide = AportesCoincide
    End Function
    Private Function CalculoHonrariosMensuales(ByVal cNroPla As String, ByVal cProceso As String, ByVal RowAfi As DataRow, ByVal cPrimerDia As String, ByVal cUltimoDia As String) As Double
        Dim DTCtaMensual As New DataTable
        Dim DACtaMensual As MySqlDataAdapter
        DACtaMensual = cnn.consultaBDadapter(
            "totales",
            "tot_nropla, SUM(tot_haber+tot_debe-tot_iva-tot_bonifica) AS total",
            "tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' AND tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " AND tot_proceso='" & cProceso & "' AND tot_nropla IN (" & cNroPla & ") AND tot_fecha BETWEEN '" & cPrimerDia & "' AND '" & cUltimoDia & "' AND tot_estado<>'9'"
        )
        DACtaMensual.Fill(DTCtaMensual)
        If DTCtaMensual.Rows.Count > 0 Then
            If Not IsDBNull(DTCtaMensual.Rows(0).Item("total")) Then
                CalculoHonrariosMensuales = DTCtaMensual.Rows(0).Item("total")
            Else
                CalculoHonrariosMensuales = 0
            End If
        Else
            CalculoHonrariosMensuales = 0
        End If
    End Function

    Private Function CalculoRet143Mensuales(ByVal cNroPla As String, ByVal cProceso As String, ByVal RowAfi As DataRow, ByVal cPrimerDia As String, ByVal cUltimoDia As String) As Double
        Dim DTCtaMensual As New DataTable
        Dim DACtaMensual As MySqlDataAdapter
        DACtaMensual = cnn.consultaBDadapter(
            "totales",
            "tot_nropla, SUM(tot_haber+tot_debe) AS total",
            "tot_tipdoc='" & RowAfi.Item("afi_tipdoc") & "' AND tot_nrodoc=" & RowAfi.Item("afi_nrodoc") & " AND tot_nropla IN (" & cNroPla & ") AND tot_fecha BETWEEN '" & cPrimerDia & "' AND '" & cUltimoDia & "' AND tot_estado<>'9' GROUP BY tot_nropla"
        )
        DACtaMensual.Fill(DTCtaMensual)
        If DTCtaMensual.Rows.Count > 0 Then
            CalculoRet143Mensuales = DTCtaMensual.Rows(0).Item("total")
        Else
            CalculoRet143Mensuales = 0
        End If
    End Function

    Public Function CuotasImpagas(ByVal Cuenta As String, ByVal cTipoDoc As String, ByVal nNroDoc As Integer, Optional ByVal fecha As String = Nothing) As DataTable
        Dim DTPagos As New DataTable
        Dim DTcuotas As New DataTable
        Dim DAcuotas As MySqlDataAdapter
        Dim nTotalPagos As Double = 0
        Dim queryFecha As String = ""

        If Not IsNothing(fecha) Then
            queryFecha = " AND tot_fecha > '" & fecha & "'"
        End If

        DAcuotas = cnn.consultaBDadapter(
            "(totales INNER JOIN plancuen on pla_nropla=tot_nropla) INNER JOIN procesos ON pro_codigo=tot_proceso",
            "tot_nropla AS Cuenta, pla_nombre AS Descripcion,tot_proceso AS Proceso,tot_nrocom AS NroCompr,CAST(tot_fecha AS date) AS Fecha,tot_fecven AS Vencimento,tot_debe AS Debe,tot_haber AS Haber,tot_imppag AS ImpPagado,0 AS interes,tot_nroasi,tot_item,tot_nrocuo AS nroCuota,tot_asicancel,pro_procancela AS ProCancel",
            "tot_nropla='" & Cuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND (tot_debe + tot_haber) <> tot_imppag AND tot_estado <> '9'" & queryFecha & " ORDER BY tot_fecha"
        )
        DAcuotas.Fill(DTcuotas)
        DTcuotas.Columns.Add("Select", GetType(Boolean))

        For Each rowCuo As DataRow In DTcuotas.Rows
            rowCuo.Item("Select") = False
        Next
        CuotasImpagas = DTcuotas
    End Function

    Public Function CuotasServicioProximo(ByVal Cuenta As String, ByVal cTipoDoc As String, ByVal nNroDoc As Integer, ByVal lBorraCuotaCancelada As Boolean, ByVal dFecPagoHasta As Date, Optional ByVal ocultarCamposTotalReal As Boolean = False, Optional ByVal dFecPagoHastaVencimiento As Boolean = False) As DataTable

        Dim tablas As String
        Dim filtros As String
        Dim campos As String

        Dim Dscuotas As New DataSet
        Dim DAcuotas As MySqlDataAdapter

        tablas = "(totales inner join plancuen on pla_nropla=tot_nropla) left join procesos on pro_codigo=tot_proceso"
        campos = "tot_nropla as Cuenta, pla_nombre as Descripcion,CAST(tot_fecha as date) as Fecha, tot_nrocuo as nroCuota,pro_procancela as ProCancel,tot_debe as debeReal"
        filtros = "tot_nropla='" & Cuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_debe > 0 AND tot_estado <> '9' ORDER BY tot_fecven DESC limit 3"

        DAcuotas = cnn.consultaBDadapter(tablas, campos, filtros)
        DAcuotas.Fill(Dscuotas, "pagos")
        CuotasServicioProximo = Dscuotas.Tables(0)

    End Function

    Public Function CuotasServicioImpagas(ByVal Cuenta As String, ByVal cTipoDoc As String, ByVal nNroDoc As Integer, ByVal lBorraCuotaCancelada As Boolean, ByVal dFecPagoHasta As Date, Optional ByVal ocultarCamposTotalReal As Boolean = False, Optional ByVal dFecPagoHastaVencimiento As Boolean = False) As DataTable
        Dim DsPagos As New DataSet
        Dim Dscuotas As New DataSet
        Dim DAcuotas As MySqlDataAdapter
        Dim nMesMoroso As Integer = 0
        Dim noSeBorroLaCuotaCancelada As Boolean = True
        Dim nTotalPagosTieneDiferencia As Boolean = False
        Dim tablas As String
        Dim filtros As String
        Dim campos As String
        Dim dFecPagoTipo As String
        If dFecPagoHastaVencimiento Then
            dFecPagoTipo = "tot_fecven"
        Else
            dFecPagoTipo = "tot_fecha"
        End If
        'INTERESES SOLO PARA DEUDORES APORTE es 1,5% mensual
        Dim porcentajeInteres As Double = 0
        If pubCuentasGeneranInteres.Contains(Cuenta) Then
            'INTERESES DE 4% mensual
            If pubCuentasGeneranInteresOtros.Contains(Cuenta) Then
                porcentajeInteres = pubMontosInteresSipresOtros
            Else
                porcentajeInteres = pubMontosInteresSipresAporte
            End If
        End If
        DAcuotas = cnn.consultaBDadapter(
            "totales",
            "tot_nropla, SUM(tot_haber) AS pagos",
            "tot_nropla='" & Cuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_haber > 0 AND tot_estado <> '9' > 0 AND " & dFecPagoTipo & "<='" & Format(dFecPagoHasta, "yyyy-MM-dd") & " 23:59:59' GROUP BY tot_nropla"
        )
        DAcuotas.Fill(DsPagos, "pagos")
        Dim nTotalPagos As Double = 0
        If DsPagos.Tables(0).Rows.Count > 0 Then
            nTotalPagos = DsPagos.Tables(0).Rows(0).Item("pagos")
        End If
        tablas = "(totales inner join plancuen on pla_nropla=tot_nropla) left join procesos on pro_codigo=tot_proceso"
        campos = "tot_nropla as Cuenta, pla_nombre as Descripcion,tot_proceso as Proceso,tot_nrocom as NroCompr,CAST(tot_fecha as date) as Fecha,tot_fecven as Vencimiento,tot_debe as Debe,tot_haber as Haber,tot_imppag as ImpPagado,0 as interes,tot_nroasi,tot_item,tot_nrocuo as nroCuota,tot_asicancel,pro_procancela as ProCancel,tot_debe as debeReal,0 as interesReal"
        If dFecPagoHastaVencimiento Then
            filtros = "tot_nropla='" & Cuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_debe > 0 AND tot_estado <> '9' AND tot_fecven <='" & Format(dFecPagoHasta, "yyyy-MM-dd") & "' ORDER BY tot_fecven DESC"
        Else
            filtros = "tot_nropla='" & Cuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_debe > 0 AND tot_estado <> '9' ORDER BY tot_fecven DESC"
        End If

        DAcuotas = cnn.consultaBDadapter(tablas, campos, filtros)

        DAcuotas.Fill(Dscuotas, "cuotas")
        Dscuotas.Tables(0).Columns.Add("total", GetType(Double))
        Dscuotas.Tables(0).Columns.Add("totalReal", GetType(Double))
        'Dscuotas.Tables(0).Columns.Add("nroCuota", GetType(Integer))
        Dscuotas.Tables(0).Columns.Add("Select", GetType(Boolean))

        Dim rowCuo As DataRow
        'Dim nroCuota As Integer = 1
        'Dim fechaCuota As Date = Nothing
        For nCuo As Integer = Dscuotas.Tables(0).Rows.Count - 1 To 0 Step -1
            rowCuo = Dscuotas.Tables(0).Rows(nCuo)
            ''seteo la fecha para hacer un corte de control
            'If fechaCuota = Nothing Then
            '    fechaCuota = rowCuo.Item("Fecha").GetDateTime()
            'ElseIf fechaCuota <> rowCuo.Item("Fecha").GetDateTime() Then
            '    'si cambia la fecha reinicio el numero de cuota a 1
            '    fechaCuota = rowCuo.Item("Fecha").GetDateTime()
            '    'nroCuota = 1
            'End If
            'seteo el numero de cuota
            'rowCuo.Item("nroCuota") = nroCuota
            If nTotalPagos > 0 Then
                If nTotalPagos >= rowCuo.Item("debe") Then
                    nTotalPagos -= rowCuo.Item("debe")
                    If lBorraCuotaCancelada Then
                        Dscuotas.Tables(0).Rows.RemoveAt(nCuo)
                        noSeBorroLaCuotaCancelada = False
                    Else
                        rowCuo.Item("imppagado") = rowCuo.Item("debe")
                        noSeBorroLaCuotaCancelada = True
                    End If

                    nMesMoroso = 0
                Else
                    nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                    rowCuo.Item("imppagado") = nTotalPagos
                    If nTotalPagos > 0 Then
                        nTotalPagosTieneDiferencia = True
                    End If
                    nTotalPagos = 0
                End If
            Else
                rowCuo.Item("imppagado") = 0
                nMesMoroso = DateDiff(DateInterval.Day, CDate(rowCuo.Item("Vencimiento").ToString), Now.Date)
                noSeBorroLaCuotaCancelada = True
            End If
            'Si no se borro la cuota cancelada entra porque sino da error y dice que no existe
            If noSeBorroLaCuotaCancelada Or nTotalPagosTieneDiferencia Then
                nTotalPagosTieneDiferencia = False
                If nMesMoroso > 0 And porcentajeInteres > 0 Then ' INTERESES SOLO PARA LAS CUENTAS EN cuentasGeneranInteres
                    rowCuo.Item("interes") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) * (porcentajeInteres * nMesMoroso / 100), 2)
                    rowCuo.Item("interesReal") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) * (porcentajeInteres * nMesMoroso / 100), 2)
                    rowCuo.Item("total") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) + rowCuo.Item("interes"), 2)
                    rowCuo.Item("totalReal") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) + rowCuo.Item("interes"), 2)
                Else
                    rowCuo.Item("total") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) + rowCuo.Item("interes"), 2)
                    rowCuo.Item("totalReal") = Math.Round((rowCuo.Item("debe") - rowCuo.Item("imppagado")) + rowCuo.Item("interes"), 2)
                End If
                rowCuo.Item("Select") = False
            End If
            'nroCuota += 1
        Next
        If ocultarCamposTotalReal = True Then
            Dscuotas.Tables(0).Columns.Remove("interesReal")
            Dscuotas.Tables(0).Columns.Remove("debeReal")
            Dscuotas.Tables(0).Columns.Remove("total")
            Dscuotas.Tables(0).Columns.Remove("totalReal")
            'Dscuotas.Tables(0).Columns.Remove("nroCuota")
        End If
        CuotasServicioImpagas = Dscuotas.Tables(0)
    End Function

    Public Function CuotasMoratoriasImpagas(ByVal Cuenta As String, ByVal cTipoDoc As String, ByVal nNroDoc As Integer) As DataTable
        Dim nroPlanCuenta As Integer = "13040400"
        Dim DsPagos As New DataSet
        Dim Dscuotas As New DataSet
        Dim DSCuentaCte As New DataSet
        Dim DAcuotas As MySqlDataAdapter
        Dim nTotalPagos As Double = 0

        DAcuotas = cnn.consultaBDadapter("afiliado", , "afi_tipdoc='" & cTipoDoc & "' and afi_nrodoc=" & nNroDoc)
        DAcuotas.Fill(DsPagos, "afiliado")

        DAcuotas = cnn.consultaBDadapter("cuo_det", , "matricula=" & DsPagos.Tables("afiliado").Rows(0).Item("afi_matricula") & " AND cancelada<>1")
        DAcuotas.Fill(Dscuotas, "cuotas")

        DAcuotas = cnn.consultaBDadapter("totales", "tot_nropla, SUM(tot_haber) AS pagos", "tot_nropla='" & nroPlanCuenta & "' AND tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_haber>0 AND tot_estado<>'9' > 0 AND tot_fecven<='" & Format(Now.Date, "yyyy-MM-dd") & " 23:59:59' GROUP BY tot_nropla")
        DAcuotas.Fill(DsPagos, "pagos")

        If DsPagos.Tables(0).Rows.Count > 0 Then
            If DsPagos.Tables("pagos").Rows.Count > 0 Then
                nTotalPagos = DsPagos.Tables("pagos").Rows(0).Item("pagos")
            Else
                nTotalPagos = 0
            End If
        End If

        Dscuotas.Tables(0).Columns.Add("Select", GetType(Boolean))
        Dscuotas.Tables(0).Columns.Add("totalReal", GetType(Double))
        Dscuotas.Tables(0).Columns.Add("color", GetType(String))

        Dim tabla As String = "totales"
        Dim campos As String = "SUM(tot_debe-tot_haber) AS saldo, tot_titulo, tot_matricula, tot_tipdoc, tot_nrodoc"
        Dim condiciones As String = "tot_tipdoc='" & cTipoDoc & "' AND tot_nrodoc=" & nNroDoc & " AND tot_nropla = '" & nroPlanCuenta & "' AND tot_estado <> '9' > 0"
        DAcuotas = cnn.consultaBDadapter(tabla, campos, condiciones)
        DSCuentaCte = New DataSet
        DAcuotas.Fill(DSCuentaCte, "servicios")
        Dim saldo As Double = DSCuentaCte.Tables("servicios").Rows(0).Item("saldo")

        For i As Integer = (Dscuotas.Tables(0).Rows.Count - 1) To 0 Step -1
            'For Each rowCuo As DataRow In Dscuotas.Tables(0).Rows
            Dscuotas.Tables(0).Rows(i).Item("Select") = False
            Dim dt_Cuotas As DataTable = Dscuotas.Tables(0)
            If saldo <= 0 Then
                Dscuotas.Tables(0).Rows(i).Item("color") = "green"
            ElseIf saldo > Dscuotas.Tables(0).Rows(i).Item("total") Then
                Dscuotas.Tables(0).Rows(i).Item("color") = "red"
                saldo = Math.Round(saldo - Dscuotas.Tables(0).Rows(i).Item("total"), 2)
                Dscuotas.Tables(0).Rows(i).Item("totalReal") = Dscuotas.Tables(0).Rows(i).Item("total")
            ElseIf saldo < Dscuotas.Tables(0).Rows(i).Item("total") Then
                Dscuotas.Tables(0).Rows(i).Item("color") = "yellow"
                'Seteo el totalReal con el total para poder calcular el porcentaje
                Dscuotas.Tables(0).Rows(i).Item("totalReal") = Dscuotas.Tables(0).Rows(i).Item("total")
                Dscuotas.Tables(0).Rows(i).Item("total") = saldo
                'Calculo el porcentaje antes de modificar el total real
                calculoPorcentajeMoratoria(Dscuotas.Tables(0).Rows(i))
                'Seteo el totalReal
                Dscuotas.Tables(0).Rows(i).Item("totalReal") = Dscuotas.Tables(0).Rows(i).Item("total")
                saldo = 0
            Else
                Dscuotas.Tables(0).Rows(i).Item("color") = "red"
                saldo = 0
                Dscuotas.Tables(0).Rows(i).Item("totalReal") = Dscuotas.Tables(0).Rows(i).Item("total")
            End If
            'rowCuo.Item("Select") = False
            'rowCuo.Item("totalReal") = rowCuo.Item("total")
            'resto al total pagado el monto de la cuota para ver si pago todo o no
            nTotalPagos -= Dscuotas.Tables(0).Rows(i).Item("total")
        Next
        If nTotalPagos < 0 Then
            CuotasMoratoriasImpagas = Dscuotas.Tables("cuotas")
        Else
            Dscuotas.Tables("cuotas").Clear()
            CuotasMoratoriasImpagas = Dscuotas.Tables("cuotas")
        End If
    End Function

    Private Function CalculoGanancias(ByVal cTipDoc As String, ByVal nNroDoc As Integer) As Double
        Dim nMontoGanac As Double = 0
        Dim DTGanac As New DataTable
        Dim DAGanac As MySqlDataAdapter
        DAGanac = cnn.consultaBDadapter("totales", "sum(tot_debe) AS ganancias", "tot_nropla='21010300' AND year(tot_fecha)= " & Now.Date.Year & " AND tot_tipdoc='" & cTipDoc & "' AND tot_nrodoc=" & nNroDoc)
        DAGanac.Fill(DTGanac)
        If Not IsDBNull(DTGanac.Rows(0).Item("ganancias")) Then
            nMontoGanac = DTGanac.Rows(0).Item("ganancias")
        End If
        Return nMontoGanac
    End Function

    Public Sub calculoPorcentajeMoratoria(ByVal RowCuo_det As DataRow)
        Try
            Dim porcentaje As Double = 1
            If RowCuo_det.Item("total") <> RowCuo_det.Item("totalReal") Then
                porcentaje = (RowCuo_det.Item("total") / RowCuo_det.Item("totalReal"))
            End If

            RowCuo_det.Item("D1") = Math.Round((RowCuo_det.Item("D1") * porcentaje), 2)
            RowCuo_det.Item("D2") = Math.Round((RowCuo_det.Item("D2") * porcentaje), 2)
            RowCuo_det.Item("D3") = Math.Round((RowCuo_det.Item("D3") * porcentaje), 2)
            RowCuo_det.Item("C1") = Math.Round((RowCuo_det.Item("C1") * porcentaje), 2)
            RowCuo_det.Item("C2") = Math.Round((RowCuo_det.Item("C2") * porcentaje), 2)
            RowCuo_det.Item("C3") = Math.Round((RowCuo_det.Item("C3") * porcentaje), 2)
            RowCuo_det.Item("C4") = Math.Round((RowCuo_det.Item("C4") * porcentaje), 2)
            RowCuo_det.Item("C5") = Math.Round((RowCuo_det.Item("C5") * porcentaje), 2)
            RowCuo_det.Item("C6") = Math.Round((RowCuo_det.Item("C6") * porcentaje), 2)
            RowCuo_det.Item("C7") = Math.Round((RowCuo_det.Item("C7") * porcentaje), 2)
            RowCuo_det.Item("C8") = Math.Round((RowCuo_det.Item("C8") * porcentaje), 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
