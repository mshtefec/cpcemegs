﻿Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports System.Data.OleDb
Imports System.Data

Public Class clsCuentas

    Dim Cnn As New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
    Dim DSCue As DataSet
    Dim DACue As MySqlDataAdapter
    Dim cmb As MySqlCommandBuilder

    Private _NroDoc As Integer
    Private _TipDoc As String
    Private _Cuenta As String
    Private _Tipo As Char
    Private _Saldo As Double
    Private _Limite As Double
    Private _Estado As Char
    Private _Banco As String
    Private _Cajaho As String
    Private _NroCtaCte As String
    Private _cbu As String

    Public Property NroDoc() As Integer
        Get
            Return Me._NroDoc
        End Get
        Set(ByVal value As Integer)
            Me._NroDoc = value
        End Set
    End Property

    Public Property TipDoc() As String
        Get
            Return Me._TipDoc
        End Get
        Set(ByVal value As String)
            Me._TipDoc = value
        End Set
    End Property

    Public Property Cuenta() As String
        Get
            Return Me._Cuenta
        End Get
        Set(ByVal value As String)
            Me._Cuenta = value
        End Set
    End Property

    Public Property Tipo() As Char
        Get
            Return Me._Tipo
        End Get
        Set(ByVal value As Char)
            Me._Tipo = value
        End Set
    End Property


    Public Property Saldo() As Double
        Get
            Return Me._Saldo
        End Get
        Set(ByVal value As Double)
            Me._Saldo = value
        End Set
    End Property

    Public Property Limite() As Double
        Get
            Return Me._Limite
        End Get
        Set(ByVal value As Double)
            Me._Limite = value
        End Set
    End Property

    Public Property Estado() As Char
        Get
            Return Me._Estado
        End Get
        Set(ByVal value As Char)
            Me._Estado = value
        End Set
    End Property

    Public Property Banco() As String
        Get
            Return Me._Banco
        End Get
        Set(ByVal value As String)
            Me._Banco = value
        End Set
    End Property

    Public Property Cajaahorro() As String
        Get
            Return Me._Cajaho
        End Get
        Set(ByVal value As String)
            Me._Cajaho = value
        End Set
    End Property

    Public Property NroCtaCte() As String
        Get
            Return Me._NroCtaCte
        End Get
        Set(ByVal value As String)
            Me._NroCtaCte = value
        End Set
    End Property

    Public Property cbu() As String
        Get
            Return Me._cbu
        End Get
        Set(ByVal value As String)
            Me._cbu = value
        End Set
    End Property

    Public Sub Ceros()
        Me._Saldo = 0
        Me._Cuenta = ""
        Me._NroDoc = 0
        Me._TipDoc = ""
        Me._Estado = ""
        Me._Tipo = ""
        Me._Limite = 0
        Me._Banco = ""
        Me._Cajaho = ""
        Me._cbu = ""
        Me._NroCtaCte = ""

        'Me._Autoriza = Nothing
    End Sub

    Public Function BuscaCuentas(ByVal TDoc As String, ByVal NDoc As Integer, Optional ByVal cCuenta As String = "") As DataSet
        DACue = New MySqlDataAdapter
        DSCue = New DataSet
        Try
            If Cnn.AbrirConexion Then
                If cCuenta = "" Then
                    If nPubNroCli = 0 Then
                        DACue = Cnn.consultaBDadapter("cuentas INNER JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo", "cue_cuenta as Cuenta,prt_nombre as Convenio,cue_tipdoc as TDoc,cue_nrodoc as NDoc ,cue_suspen as Susp,cue_ultsal as Saldo,cue_limite as Tope,cue_tipo as Tipo,cue_banco,cue_nrocue,cue_nroaho,cue_cbu", "cue_tipdoc = '" & TDoc & "' AND cue_nrodoc = " & NDoc & " and prt_nrocli=1")
                    Else
                        DACue = Cnn.consultaBDadapter("cuentas LEFT JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo and cue_nrocli = prt_nrocli", "cue_cuenta as Cuenta,prt_nombre as Convenio,cue_tipdoc as TDoc,cue_nrodoc as NDoc ,cue_suspen as Susp,cue_ultsal as Saldo,cue_limite as Tope,cue_tipo as Tipo,cue_banco,cue_nrocue,cue_nroaho,cue_cbu", "cue_tipdoc = '" & TDoc & "' AND cue_nrodoc = " & NDoc & " and cue_suspen IN('N','S',' ')")
                    End If
                Else
                    If nPubNroCli = 0 Then
                        DACue = Cnn.consultaBDadapter("cuentas INNER JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo", "cue_cuenta as Cuenta,prt_nombre as Convenio,cue_tipdoc as TDoc,cue_nrodoc as NDoc ,cue_suspen as Susp,cue_ultsal as Saldo,cue_limite as Tope,cue_tipo as Tipo,cue_banco,cue_nrocue,cue_nroaho,cue_cbu", "cue_tipdoc = '" & TDoc & "' AND cue_nrodoc = " & NDoc & " and cue_cuenta='" & cCuenta & "' and prt_nrocli=1")
                    Else
                        DACue = Cnn.consultaBDadapter("cuentas LEFT JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo and cue_nrocli = prt_nrocli", "cue_cuenta as Cuenta,prt_nombre as Convenio,cue_tipdoc as TDoc,cue_nrodoc as NDoc ,cue_suspen as Susp,cue_ultsal as Saldo,cue_limite as Tope,cue_tipo as Tipo,cue_banco,cue_nrocue,cue_nroaho,cue_cbu", "cue_tipdoc = '" & TDoc & "' AND cue_nrodoc = " & NDoc & " and cue_cuenta='" & cCuenta & "' and cue_suspen IN('N','S',' ')")
                    End If
                End If
                'DACue = Cnn.consultaBDadapter("Cuentas", "cue_tipdoc,cue_nrodoc,cue_cuenta as Cuenta,cue_limite as Tope,cue_suspen as Suspendida", "cue_tipdoc='" & TDoc & "' and cue_nrodoc=" & NDoc & "")
                Dim Cmb = New MySqlCommandBuilder(DACue)
                DACue.Fill(DSCue, "Cuentas")
                ' If DSCue.Tables("cuentas").Rows.Count > 0 Then
                Return DSCue
                'Else
                '   Return Nothing
                'End If
            Else
            Return Nothing
            MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            Return Nothing
            MessageBox.Show("Se Producjo un Error " & ex.Message.ToString, "POR FAVOR VERFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try
    End Function

    Public Function SaldoCta(ByVal Tdoc As String, ByVal Nro As Integer, ByVal Cuenta As String, Optional ByVal Fe1 As String = "", Optional ByVal Fe2 As String = "", Optional ByVal Servicio As Integer = 0) As Double
        Dim Comando As MySqlCommand
        Comando = New MySqlCommand
        Try
            If Cnn.AbrirConexion Then
                Comando.Connection = Cnn.conexion
                Comando.CommandText = "select sum((mov_coeficiente*mov_impcom)) as Saldo from movcompr WHERE mov_fecha <='" & Format(Date.Now, "yyyy-MM-dd") & "' and mov_tipcta = '" & Tdoc & "' AND mov_nrocta = " & Nro & " AND mov_cuenta = '" & Cuenta & "' AND mov_servicio = " & Servicio & ""
                Comando.CommandType = CommandType.Text
                If Not IsDBNull(Comando.ExecuteScalar) Then
                    Return CType(Comando.ExecuteScalar, Double)
                Else
                    Return 0
                End If                
            End If
        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try
    End Function

    Public Function Autorizados(ByVal TipDoc As String, ByVal NroDoc As Integer) As DataSet
        DACue = New MySqlDataAdapter
        DSCue = New DataSet
        Try
            If Cnn.AbrirConexion Then                
                DACue = Cnn.consultaBDadapter("(autoriza inner join clientes on aut_tiptitular = cli_tipdoc and aut_nrotitular = cli_nrodoc) left join cuentas on aut_tiptitular = cue_tipdoc and aut_nrotitular = cue_nrodoc", "cli_nombre as titular,aut_tiptitular as TipTit,aut_nrotitular as NroTit,cli_obrsoc as Obrsoc,cue_cuenta as Cuenta,cue_limite,cue_suspen,cue_tipdoc,cue_nrodoc", "aut_tipautoriza='" & TipDoc & "' and aut_nroautoriza=" & NroDoc & " and aut_suspen <> 'S'")
                Dim Cmb = New MySqlCommandBuilder(DACue)
                DACue.Fill(DSCue, "Autoriza")
                If DSCue.Tables("Autoriza").Rows.Count > 0 Then
                    Autorizados = DSCue
                Else
                    Autorizados = Nothing
                End If
            Else
                Autorizados = Nothing
            End If
        Catch ex As Exception
            Autorizados = Nothing
            MessageBox.Show("Se Produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try
        Return Autorizados
    End Function

    Public Function ActualizaCta() As Boolean
        Dim fecAlta As New MySqlDateTime
        DACue = New MySqlDataAdapter
        DSCue = New DataSet
        Dim DRCue As DataRow
        fecAlta.Month = Date.Today.Month
        fecAlta.Day = Date.Today.Day
        fecAlta.Year = Date.Today.Year
        Try
            '  If Cnn.AbrirConexion Then
            'DACue = Cnn.consultaBDadapter("cuentas", "*", "cue_tipdoc='" & TDoc & "' and cue_nrocta=" & Nro & " and cue_cuenta='" & Cuenta & "' and cue_tipo ='" & Tipo & "'")
            DACue = Cnn.consultaBDadapter("cuentas", , "cue_tipdoc='" & TipDoc & "' and cue_nrodoc=" & NroDoc & " and cue_cuenta='" & Cuenta & "'")

            DACue.Fill(DSCue, "Cuentas")

            DACue = Cnn.consultaBDadapter("cuentas", , "cue_tipdoc='ENT' and cue_tipo='E' and cue_cuenta='" & Cuenta & "'")
            DACue.Fill(DSCue, "Convenio")

            cmb = New MySqlCommandBuilder(DACue)

            If DSCue.Tables("Cuentas").Rows.Count > 0 Then      'Actualizo                    
                DRCue = DSCue.Tables("Cuentas").Rows(0)
                If Limite <> -1 Then
                    DRCue("cue_limite") = Limite
                End If
                DRCue("cue_suspen") = Estado
                DRCue("cue_lote") = 0
                DRCue("cue_operador") = nPubNroOperador
                DRCue("cue_banco") = Banco
                DRCue("cue_nrocue") = NroCtaCte
                DRCue("cue_nroaho") = Cajaahorro
                DRCue("cue_cbu") = cbu
                DACue.Update(DSCue, "Cuentas")
                ActualizaCta = True
                MessageBox.Show("Cuenta Acutalizada Correctamente", "EDICION DE CTA CTE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                'Alta
                If DSCue.Tables("Convenio").Rows.Count = 0 Then
                    MessageBox.Show("El convenio " & Cuenta & " no existe", "ALTA DE CTA CTE", MessageBoxButtons.OK)
                Else
                    DRCue = DSCue.Tables("Cuentas").NewRow
                    DRCue("cue_nrocli") = nPubNroCli
                    DRCue("cue_tipdoc") = TipDoc
                    DRCue("cue_nrodoc") = NroDoc
                    DRCue("cue_cuenta") = Cuenta
                    DRCue("cue_tipo") = "E"
                    DRCue("cue_limite") = Limite
                    DRCue("cue_suspen") = Estado
                    DRCue("cue_fecalt") = fecAlta
                    DRCue("cue_operador") = 0
                    DRCue("cue_lote") = 0
                    DSCue.Tables("Cuentas").Rows.Add(DRCue)
                    DACue.Update(DSCue, "Cuentas")
                    MessageBox.Show("Cuenta Dada de Alta Correctamente", "NUEVA CTA CTE", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    ActualizaCta = True
                End If
            End If
            '  Else
            ''  MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            ' End If
        Catch ex As Exception
            MessageBox.Show("Se produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ActualizaCta = True
        Finally
            Cnn.CerrarConexion()
        End Try
        Return ActualizaCta
    End Function

    Public Function PagoCta(ByVal TDoc As String, ByVal Nro As Integer, ByVal Cuenta As String, ByVal Monto As Decimal) As Boolean
        Dim nSaldo As Decimal
        DACue = New MySqlDataAdapter
        DSCue = New DataSet

        Try
            If Cnn.AbrirConexion Then
                'Calculo el Saldo
                nSaldo = SaldoCta(TDoc, Nro, Cuenta, Now.Date)

                DACue = Cnn.consultaBDadapter("", "", "")
                cmb = New MySqlCommandBuilder(DACue)
                DACue.Fill(DSCue, "Saldo")




            Else
                MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception

        End Try

        'Calculo el Saldo a la Fecha




        Return PagoCta
    End Function

    Public Sub CargarCta(ByVal DR As DataRow)
        '   If DR("susp") = "N" Or Trim(DR("susp")) = "" Then
        Me._TipDoc = DR("tdoc")
        Me._NroDoc = DR("ndoc")
        Me._Cuenta = DR("cuenta")
        Me._Estado = DR("susp")
        Me._Limite = DR("tope")
        Me._Tipo = DR("tipo")
        Me._Banco = DR("cue_banco")
        Me._NroCtaCte = DR("cue_nrocue")
        Me._Cajaho = DR("cue_nroaho")
        Me._cbu = DR("cue_cbu")
        Me.Saldo = SaldoCta(TipDoc, NroDoc, Cuenta)
        '   End If
        ''
    End Sub

    Public Sub ImprimirCta(ByVal TipDoc As String, ByVal NroDoc As Integer, ByVal Cuenta As String, ByVal FDesde As Date, ByVal FHasta As Date, Optional ByVal Detallado As Boolean = False, Optional ByVal Nombre As String = "")
        Dim nSaldoMoroso, nSaldo, nSaldoAnterior As Double
        Dim CnnAcc As New OleDbConnection
        Dim DAacc As OleDbDataAdapter
        Dim DSAcc As DataSet
        Dim ComnadoAcc As OleDbCommand
        Dim Comando As MySqlCommand
        Dim Result As Double = 0
        Dim D As DateTime

        Dim DSCuenta As DataSet
        Dim DACuenta As MySqlDataAdapter
        Dim DRCuenta, oDataRow As DataRow

        '--------------------------------------------------------------------------
        'Borra la tabla en el Access
        '--------------------------------------------------------------------------
        CnnAcc.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\farmaMEGS\temp\resumendecuenta.mdb;Persist Security Info=False"
        ComnadoAcc = New OleDbCommand("Delete from resumendecuenta", CnnAcc)
        CnnAcc.Open()
        ComnadoAcc.ExecuteNonQuery()
        CnnAcc.Close()
        ComnadoAcc.Dispose()
        '--------------------------------------------------------------------------

        '--------------------------------------------------------------------------
        'ABRO EL ACCESS PARA ESCRIBIRLO
        '--------------------------------------------------------------------------
        DAacc = New OleDbDataAdapter("select * from resumendecuenta", CnnAcc)
        CnnAcc.Open()

        DSAcc = New DataSet
        Dim CBAcc As OleDbCommandBuilder
        CBAcc = New OleDbCommandBuilder(DAacc)
        DAacc.Fill(DSAcc, "Resumen")
        CnnAcc.Close()
        '--------------------------------------------------------------------------

        Try
            If Cnn.AbrirConexion Then

                Comando = New MySqlCommand()
                Comando.CommandText = "select sum(mov_impcom) as Pagos from movcompr where mov_tipcta='" & TipDoc & "' and mov_nrocta=" & NroDoc & " and mov_cuenta='" & Cuenta & "' and mov_fecha >='" & Format(FHasta, "yyyy-MM-dd") & "' and mov_tipcom IN ('PG','AP','NC') GROUP BY mov_tipcta,mov_nrocta,mov_cuenta"
                Comando.CommandType = CommandType.Text
                Comando.Connection = Cnn.conexion
                Result = Comando.ExecuteScalar()

                D = FHasta.AddDays(50)

                If Result <= 0 Then

                    Comando = New MySqlCommand()
                    Comando.CommandText = "select sum(mov_coeficiente * mov_impcom) as Saldo,mov_fecha from movcompr WHERE " & _
                                           "mov_tipcta = '" & TipDoc & "' AND mov_nrocta = '" & NroDoc & "' AND mov_cuenta = '" & Cuenta & "' AND " & _
                                           "mov_fecha < '" & Format(D, "yyyy-MM-dd") & "' AND mov_servicio =0"
                    Comando.CommandType = CommandType.Text
                    Comando.Connection = Cnn.conexion
                    nSaldoMoroso = Comando.ExecuteScalar()

                Else

                    Comando = New MySqlCommand()
                    Comando.CommandText = "select sum(mov_coeficiente * mov_impcom) as Saldo ,mov_fecha from movcompr WHERE " & _
                                           "mov_tipcta = '" & TipDoc & "' AND mov_nrocta = '" & NroDoc & "' AND mov_cuenta = '" & Cuenta & "' AND " & _
                                           "mov_fecha < '" & Format(D, "yyyy-MM-dd") & "' AND mov_servicio =0"
                    Comando.CommandType = CommandType.Text
                    Comando.Connection = Cnn.conexion
                    nSaldoMoroso = Comando.ExecuteScalar()

                    nSaldoMoroso += Result
                End If


                Comando = New MySqlCommand()
                Comando.CommandText = "select sum(mov_coeficiente * mov_impcom) as Saldo from movcompr WHERE " & _
                                       "mov_tipcta = '" & TipDoc & "' AND mov_nrocta = '" & NroDoc & "' AND mov_cuenta = '" & Cuenta & "' AND " & _
                                       "mov_fecha < '" & Format(FDesde, "yyyy-MM-dd") & "' AND mov_servicio =0"
                Comando.CommandType = CommandType.Text
                Comando.Connection = Cnn.conexion

                If Not IsDBNull(Comando.ExecuteScalar()) Then
                    nSaldo = Comando.ExecuteScalar()
                Else
                    nSaldo = 0
                End If


                nSaldoAnterior = nSaldo

                DSCuenta = New DataSet

                DACuenta = Cnn.consultaBDadapter("movcompr", "mov_nrocli as Far,mov_fecha as Fecha,mov_hora as hora,mov_nrocuo as cuota ,mov_tipcom as TC ,mov_puncom as Punto,mov_nrocom As Factura,mov_nrocuo as Cuo,mov_tipmov as TM,mov_impcom as Importe, mov_imppag as Pago,mov_saldo as Saldo,mov_estado as Est,mov_fecpag as fecpago,mov_coeficiente,mov_tipcan,mov_puncan,mov_comcan,mov_nropre,mov_servicio", "mov_tipcta = '" & TipDoc & "' AND mov_nrocta = " & NroDoc & " AND mov_cuenta = '" & Cuenta & "' AND mov_fecha >= '" & Format(FDesde, "yyyy-MM-dd") & "' AND mov_fecha <= '" & Format(FHasta, "yyyy-MM-dd") & "' AND mov_servicio = 0 ORDER BY mov_fecha,mov_hora")
                DACuenta.Fill(DSCuenta, "Cuentas")

                If DSCuenta.Tables("Cuentas").Rows.Count > 0 Then
                    For x As Integer = 0 To DSCuenta.Tables("Cuentas").Rows.Count - 1
                        DRCuenta = DSCuenta.Tables("Cuentas").Rows(x)

                        Comando = New MySqlCommand("select ser_descripcion from servicios where ser_nroint =" & DRCuenta("mov_servicio") & "", Cnn.conexion)
                        Dim R As String = Comando.ExecuteScalar()

                        oDataRow = DSAcc.Tables("Resumen").NewRow()

                        oDataRow("sucursal") = DRCuenta("far")
                        oDataRow("descrisucursal") = "ACA"
                        oDataRow("cuenta") = Cuenta
                        oDataRow("nrocta") = NroDoc
                        oDataRow("tipcta") = TipDoc
                        oDataRow("cliente") = Nombre
                        oDataRow("tipcom") = DRCuenta("tc")
                        oDataRow("puncom") = 0
                        oDataRow("nrocom") = DRCuenta("factura")
                        oDataRow("fecha") = DRCuenta("fecha").ToString
                        oDataRow("hora") = DRCuenta("hora")
                        oDataRow("cuota") = DRCuenta("cuota")
                        oDataRow("desde") = Format(FDesde, "yyyy/MM/dd")
                        oDataRow("hasta") = Format(FHasta, "yyyy/MM/dd")

                        If DRCuenta("tm") = "+" Then
                            nSaldo = nSaldo + DRCuenta("importe")
                            oDataRow("haber") = Format(0, "####0.00")
                            oDataRow("deber") = Format(DRCuenta("importe"), "####0.00")
                            oDataRow("saldo") = Format(nSaldo, "####0.00")
                        Else
                            nSaldo = nSaldo - DRCuenta("importe")
                            oDataRow("deber") = Format(0, "####0.00")
                            oDataRow("haber") = Format(DRCuenta("importe"), "####0.00")
                            oDataRow("saldo") = Format(nSaldo, "####0.00")
                        End If

                        oDataRow("saldoanterior") = Format(nSaldoAnterior, "####0.00")
                        oDataRow("lineas") = 1
                        oDataRow("descritipcom") = R

                        Try
                            DSAcc.Tables("Resumen").Rows.Add(oDataRow)
                            DAacc.Update(DSAcc, "Resumen") 'ACTUALIZA LA BASE DE DATOS                            
                        Catch ex As Exception
                            MessageBox.Show(ex.Message & "/" & ex.Source)
                        End Try

                        'CON DETALLES
                        If Detallado Then
                            Dim DALcomprob As MySqlDataAdapter
                            Dim DSLcomprob As DataSet

                            Dim DAPrd As MySqlDataAdapter
                            Dim DSPrd As DataSet

                            Dim nspc As Integer = 1

                            DALcomprob = Cnn.consultaBDadapter("lcomprob", "lco_nroint,lco_nrocli,lco_tipcom,lco_nrocom,lco_cantid,lco_impafi", "lco_unegos = 0 and lco_nrocli ='" & DRCuenta("far") & "' AND lco_tipcom ='" & DRCuenta("tc") & "' AND " & _
                                                       "lco_puncom=0 and lco_nrocom ='" & DRCuenta("factura") & "'")
                            DSLcomprob = New DataSet
                            DALcomprob.Fill(DSLcomprob, "Lcomprob")

                            If DSLcomprob.Tables("Lcomprob").Rows.Count > 0 Then
                                For i As Integer = 0 To DSLcomprob.Tables("Lcomprob").Rows.Count - 1

                                    nspc = nspc + 1
                                    oDataRow = DSAcc.Tables("Resumen").NewRow()

                                    oDataRow("sucursal") = DRCuenta("far")
                                    oDataRow("descrisucursal") = "ACA"
                                    oDataRow("cuenta") = Cuenta
                                    oDataRow("nrocta") = NroDoc
                                    oDataRow("tipcta") = TipDoc
                                    oDataRow("cliente") = Nombre
                                    oDataRow("tipcom") = DRCuenta("tc")
                                    oDataRow("puncom") = 0
                                    oDataRow("nrocom") = DRCuenta("factura")
                                    oDataRow("fecha") = DRCuenta("fecha").ToString
                                    oDataRow("hora") = DRCuenta("hora")
                                    oDataRow("cuota") = DRCuenta("cuota")
                                    oDataRow("desde") = Format(FDesde, "yyyy/MM/dd")
                                    oDataRow("hasta") = Format(FHasta, "yyyy/MM/dd")


                                    oDataRow("lineas") = nspc
                                    oDataRow("cantidad") = DSLcomprob.Tables("Lcomprob").Rows(i).Item("lco_cantid")


                                    DSPrd = New DataSet
                                    DAPrd = Cnn.consultaBDadapter("producto", "*", "prd_nroint =" & DSLcomprob.Tables("Lcomprob").Rows(i).Item("lco_nroint") & "")
                                    DAPrd.Fill(DSPrd, "Producto")

                                    If DSPrd.Tables("Producto").Rows.Count > 0 Then
                                        oDataRow("Producto") = Left(Left(DSPrd.Tables("Producto").Rows(0).Item("prd_descri"), 15) & " " & LTrim(DSPrd.Tables("Producto").Rows(0).Item("prd_presen")), 45)
                                    End If

                                    oDataRow("importe") = Format(DSLcomprob.Tables("Lcomprob").Rows(i).Item("lco_impafi"), "####0.00")

                                    Try
                                        DSAcc.Tables("Resumen").Rows.Add(oDataRow)
                                        DAacc.Update(DSAcc, "Resumen") 'ACTUALIZA LA BASE DE DATOS                            
                                    Catch ex As Exception
                                        MessageBox.Show(ex.Message & "/" & ex.Source)
                                    End Try

                                Next

                            End If
                        End If
                    Next
                End If

                'Reporte = "resumen de cuenta S.rpt"
                'My.Forms.frmReportes.ShowDialog()

            Else
                MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            MessageBox.Show("Se produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try
    End Sub

    Public Function BuscaCtaServicios(ByVal TipoDoc, ByVal NroDoc) As DataSet

        Try
            If Cnn.AbrirConexion Then
                DACue = New MySqlDataAdapter
                DSCue = New DataSet

                DACue = Cnn.consultaBDadapter("planclie INNER JOIN servicios ON pcl_servicio = ser_nroint", "pcl_servicio ,ser_descripcion ,pcl_impcuo ,pcl_suspen ,pcl_fecalt as alta,pcl_fecbaj,pcl_tipdoc,pcl_nrodoc,ser_nrocta,ser_debengar,ser_tipcom,pcl_servicio,pcl_debito", "pcl_tipdoc='" & TipoDoc & "' and pcl_nrodoc=" & NroDoc & " and pcl_servicio  > 0")
                cmb = New MySqlCommandBuilder(DACue)
                DACue.Fill(DSCue, "PlanClie")

                Return DSCue
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return Nothing
        Finally
            Cnn.CerrarConexion()
        End Try
    End Function

End Class
