﻿Imports MySql.Data.MySqlClient
'SqlConnectionStringBuilder
Imports System.Configuration

Public Class ConsultaBD
    Public conexion As MySqlConnection 'gestiona la conexion a la Base de Datos
    Private comandos As MySqlCommand 'gestiona las consultas
    Private lect As MySqlDataReader 'gestiona los resultados de las consultas, es el equivalente al DataSet de Acces
    Private lectA As MySqlDataAdapter
    Private MiBuilder As MySqlCommandBuilder
    Private MiData As DataSet
    Private MiTrans As MySqlTransaction
    Private NroRegistradora As Integer
    Private NroCaja As Long
    Private NroZeta As Long
    Private NroPlanProceso As String
    Private NumeroPlan As String
    Private DescripcionPlan As String
    Private cCuentaOS As String
    'TRABAJOS
    Public trabajoId As Integer = 0
    Public trabajoIds() As Integer = Nothing
    Public trabajoEstado As Integer = 0
    Public trabajoNroAsiento As Long = 0
    Public trabajoNroAsientoLiquidacion As Long = 0
    Public trabajoNroLegalizacion As Integer = 0
    Public trabajoNroAsientoActualizar As Boolean = True
    Public trabajoDestinatario As String = Nothing
    Public trabajosEnviarCorreo As String = Nothing
    'Esta es la constructora de la clase, a la cual le paso los parametros de conexion
    'a la base de datos, presta atencion a la linea de ConnectionString, ya que en donde pone database=NOMBRE
    'en vez de NOMBRE va el nombre de la Base de datos a llamar
    Private Sub SetTrabajoDefaultValues()
        trabajoId = 0
        trabajoIds = Nothing
        trabajoEstado = 0
        trabajoNroAsiento = 0
        trabajoNroAsientoLiquidacion = 0
        trabajoNroLegalizacion = 0
        trabajoNroAsientoActualizar = True
        trabajoDestinatario = Nothing
        trabajosEnviarCorreo = Nothing
    End Sub
    Public Sub New(ByVal server As String, ByVal BD As String, ByVal user As String, ByVal pass As String, Optional ByVal puerto As Integer = 0)
        'Si no se pasa un puerto se utiliza el definido
        If puerto = 0 Then
            puerto = cPubPuerto
        End If
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=" & server & ";port=" & puerto & ";" & "user id=" & user & ";" & "password=" & pass & ";" & "database=" & BD & ";Allow Zero Datetime=True;Convert Zero Datetime=False;Connect Timeout=600000;"
        'Try
        'conexion.Open()
        'MessageBox.Show("Conexión Abierta Con Éxito")
        'conexion.Close()
        'Catch mierror As MySqlException
        'MessageBox.Show("Error de Conexión a la Base de Datos: " & mierror.Message)
        'Finally
        'conexion.Dispose()
        'End Try
    End Sub
    'Conexion Utilizando Singleton
    Public Sub New(ByVal useConexionUnica As Boolean)
        Dim miConexion As ConexionUnica
        'Se crea la instancia de ConexionUnica
        miConexion = ConexionUnica.getInstancia()
        conexion = miConexion.GetConexion()
    End Sub

    Public Sub New()
        CargaConexion()
    End Sub

    Public Function GetConexion() As MySqlConnection
        Return conexion
    End Function

    Public Function AbrirConexion() As Boolean
        Try
            conexion.Open()
            AbrirConexion = True
        Catch ex As Exception
            mostrarErrorConexion("abrir conexion.", ex.Message)
            AbrirConexion = False
        End Try
    End Function

    Public Sub CerrarConexion()
        Try
            conexion.Close()
        Catch ex As Exception
            mostrarErrorConexion("cerrar conexion.", ex.Message)
        End Try
    End Sub

    Public Function EstadoConexion() As Boolean
        If conexion.State = ConnectionState.Open Then
            EstadoConexion = True
        Else
            EstadoConexion = False
        End If
    End Function

    Public Function InicioTransaccion() As MySqlTransaction
        InicioTransaccion = conexion.BeginTransaction()
    End Function

    'aqui hice otro metodo que es la de consulta, a ella le paso la tabla a consultar, asi como datos opcionales, 
    ' como pueden ser los campos a elegir y las condiciones a usar.
    'Si no se les pasa los valores Optional la consulta es la tipica:
    '"select * from TABLA where 1"
    'esto lo hago para que sea lo mas generica posible y se pueda manejar mejor con cualquier consulta.
    Public Function consultaBDreader(ByVal tabla As String, Optional ByVal campos As String = "*", Optional ByVal condiciones As String = "1") As MySqlDataReader
        comandos = New MySqlCommand()
        'los campos, tablas y condiciones hay que pasarlas para que esta concatenacion quede como una consulta
        'valida hacia la Base de Datos
        comandos.CommandText = "select " & campos & " from " & tabla & " where " & condiciones & ""
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion 'le digo cual es la variable de conexion
        lect = comandos.ExecuteReader() ' le digo a la variable lectora que lo que tiene que recojer es la ejecucion de la variable comandos
        Return lect 'retorno una variable del tipo MysqlDataReader
    End Function

    Public Function consultaBDadapter(ByVal tabla As String, Optional ByVal campos As String = "*", Optional ByVal condiciones As String = "1") As MySqlDataAdapter
        'los campos, tablas y condiciones hay que pasarlas para que esta concatenacion quede como una consulta
        'valida hacia la Base de Datos
        lectA = New MySqlDataAdapter("select " & campos & " from " & tabla & " where " & condiciones & ";", conexion)
        Return lectA 'retorno una variable del tipo MysqlDataAdapter
    End Function

    Public Function consultaBDscalar(ByVal tabla As String, Optional ByVal campos As String = "*", Optional ByVal condiciones As String = "1") As String
        comandos = New MySqlCommand()
        'los campos, tablas y condiciones hay que pasarlas para que esta concatenacion quede como una consulta
        'valida hacia la Base de Datos
        comandos.CommandText = "select " & campos & " from " & tabla & " where " & condiciones & ""
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion 'le digo cual es la variable de conexion
        Return comandos.ExecuteScalar
    End Function

    Public Function consultaHoraServidor() As MySqlDataAdapter
        'Obtengo hora del servidor MySQL
        Return New MySqlDataAdapter("SELECT NOW() AS hora;", conexion)
    End Function

    'este metodo es parecido al anterior, pero realiza acciones de INSERT sobre la Base de Datos
    'se le tiene que pasar a que tabla se agregan los datos y los datos en el formato campos = 'var1','var2','var3'...
    Public Function insertarBD(ByVal tabla As String, ByVal campos As String) As Boolean
        Try
            comandos = New MySqlCommand()
            'comandos.CommandText = "insert into tabla values(a,b,c,d,e,f,g);"
            comandos.CommandText = "insert into " & tabla & " values(" & campos & ");"
            comandos.CommandType = CommandType.Text
            comandos.Connection = conexion
            lect = comandos.ExecuteReader()
            lect.Close()
            insertarBD = True
        Catch ex As Exception
            MessageBox.Show("Se produjo un error en el Alta, Por Favor Verifique los valores " & ex.Message, "ERROR EN EL ALTA", MessageBoxButtons.OK, MessageBoxIcon.Error)
            insertarBD = False
        End Try
    End Function

    'lo mismo que los anteriores, pero para Borrar
    Public Sub borrarBD(ByVal tabla As String, ByVal condicion As String)
        'Dim xInicio As String = TimeOfDay
        Try
            comandos = New MySqlCommand()
            'comandos.CommandText = "DELETE FROM Contactos WHERE nombre = 'a' " 'la consulta debe quedar con esta estructura
            comandos.CommandText = "delete from " & tabla & " where " & condicion & ";"
            comandos.CommandType = CommandType.Text
            comandos.Connection = conexion
            lect = comandos.ExecuteReader()
            lect.Close()
        Catch ex As Exception
        End Try
        'MessageBox.Show("empezo " + xInicio + " Termino:" + CStr(TimeOfDay))
    End Sub

    Public Sub Bloqueo(ByVal lBloquea As Boolean)
        Dim dsBLoqueo As New DataSet
        Dim rowBloqueo As DataRow
        Dim daBloqueo As New MySqlDataAdapter
        Dim cmdBloqueo As New MySqlCommandBuilder
        Dim nEspera As Single
        Dim lSaliobloqueado As Boolean = False
        'Busco tabla segun delegacion 1 2 3 4
        daBloqueo = consultaBDadapter("bloqueos", , "nrocli = " & nPubNroCli)
        cmdBloqueo = New MySqlCommandBuilder(daBloqueo)
        daBloqueo.Fill(dsBLoqueo, "bloqueos")
        rowBloqueo = dsBLoqueo.Tables(0).Rows(0)
        nEspera = Microsoft.VisualBasic.Timer + 20
        If lBloquea Then
            Do While rowBloqueo.Item("bloqueo") = "True"
                If nEspera < Microsoft.VisualBasic.Timer Then
                    lSaliobloqueado = True
                    Exit Do
                End If
                dsBLoqueo = New DataSet
                daBloqueo.Fill(dsBLoqueo, "bloqueos")
                rowBloqueo = dsBLoqueo.Tables(0).Rows(0)
            Loop
        End If
        If lSaliobloqueado Then
            MessageBox.Show("Una terminal esta bloqueando")
        Else
            rowBloqueo.Item("bloqueo") = IIf(lBloquea, "True", "False")
            rowBloqueo.Item("terminal") = cPubIpLocal
            rowBloqueo.Item("nombre") = cPubNombrePC
            daBloqueo.Update(dsBLoqueo.Tables(0))
        End If
    End Sub

    Public Sub DesbloqueoCatalogo()
        'Dim xInicio As String = TimeOfDay
        Try
            comandos = New MySqlCommand()
            'comandos.CommandText = "DELETE FROM Contactos WHERE nombre = 'a' " 'la consulta debe quedar con esta estructura
            comandos.CommandText = "update catalogo set cat_bloqueo='False';"
            comandos.CommandType = CommandType.Text
            comandos.Connection = conexion
            lect = comandos.ExecuteReader()
            lect.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'MessageBox.Show("empezo " + xInicio + " Termino:" + CStr(TimeOfDay))
    End Sub

    Public Function ReplaceBD(ByVal StringReplace As String) As Boolean
        ' Dim archEnvio As StreamReader = File.OpenText(StringReplace)
        If EstadoConexion() Then
            comandos = New MySqlCommand(StringReplace, conexion)
            comandos.ExecuteNonQuery()
            ReplaceBD = True
        Else
            ReplaceBD = False
        End If
    End Function

    Public Function actualizarBD(ByVal StringReplace As String) As Boolean
        Try
            comandos = New MySqlCommand()

            comandos.CommandText = StringReplace
            comandos.CommandType = CommandType.Text
            comandos.Connection = conexion
            lect = comandos.ExecuteReader
            lect.Close()
            actualizarBD = True
        Catch ex As Exception
            MessageBox.Show("Error en Actualizar en archivo:" + StringReplace + " " + ex.Message)
            actualizarBD = False
        End Try

    End Function

    'Devuelve 1 Valor que indica si existe o no el dato concreto buscado
    Public Function Existe(ByVal tabla As String, Optional ByVal campos As String = "*", Optional ByVal condiciones As String = "1") As Boolean
        'Dim I As Integer = 1
        comandos = New MySqlCommand()
        comandos.CommandText = "select " & campos & " from " & tabla & " where " & condiciones & ""
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion
        'iExiste = CInt(comandos.ExecuteScalar)
        If CInt(comandos.ExecuteScalar) = 0 Then
            Existe = False
        Else
            Existe = True
        End If
    End Function

    'Trae los Nro de comprobante
    Public Function TomaNro(ByVal condiciones) As Integer
        comandos = New MySqlCommand()
        comandos.CommandText = "select num_nrocom from Numeros where " & condiciones & ""
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion
        TomaNro = CInt(comandos.ExecuteScalar)
        If TomaNro > 0 Then
            TomaNro += 1
        Else
            TomaNro = 1
        End If
        Return TomaNro
    End Function

    Public Function TomaCobte(ByVal nInstitucion As Integer, ByVal nDelegacion As Integer, ByVal cProceso As String, Optional ByVal lActualizaCorrelatividad As Boolean = True) As Long
        Dim nNroCom As Long = 0
        Dim DACobte As MySqlDataAdapter = New MySqlDataAdapter
        Dim DRCobte As MySqlDataReader
        Dim DTProceso As DataTable = New DataTable
        Dim cmdCobte As MySqlCommand = New MySqlCommand
        Try
            DACobte = consultaBDadapter(
                "numeros JOIN procesos ON num_numerador = pro_numerador",
                "pro_numerador, num_nrocom",
                "num_unegos=" & nInstitucion & " AND num_nrocli=" & nDelegacion & " AND pro_codigo='" & cProceso & "'"
            )
            DACobte.Fill(DTProceso)
            If DTProceso.Rows.Count = 1 Then
                nNroCom = DTProceso.Rows.Item(0).Item("num_nrocom").ToString + 1
                If lActualizaCorrelatividad Then
                    cmdCobte.CommandText = "UPDATE numeros " &
                        "SET num_nrocom = '" & nNroCom & "'," &
                        "num_fecha = '" & Format(Date.Today, "yyyy-MM-dd") & "' " &
                        " WHERE num_numerador = '" & DTProceso.Rows(0).Item("pro_numerador") & "' AND num_unegos=" & nInstitucion & " AND num_nrocli=" & nDelegacion & ";"
                End If
            Else
                DACobte = consultaBDadapter("procesos", "pro_numerador", "pro_codigo='" & cProceso & "'")
                DACobte.Fill(DTProceso)
                If DTProceso.Rows.Count = 0 Then
                    MessageBox.Show("No se encontro numerador del proceso " & cProceso, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Try
                End If
                nNroCom = 1
                If lActualizaCorrelatividad Then
                    cmdCobte.CommandText = "INSERT INTO numeros (num_unegos, num_nrocli, num_numerador , num_puncom , num_nrocom, num_fecha ) " &
                        "VALUES ('" & nInstitucion & "','" & nDelegacion & "','" & DTProceso.Rows(0).Item("pro_numerador") & "','" & 0 & "','" & nNroCom & "','" & Format(Date.Today, "yyyy-MM-dd") & "');"
                End If
            End If
            If lActualizaCorrelatividad Then
                cmdCobte.CommandType = CommandType.Text
                cmdCobte.Connection = conexion
                DRCobte = cmdCobte.ExecuteReader()
                DRCobte.Close()
            End If
        Catch ex As Exception
            nNroCom = 0
            MessageBox.Show("Error en TomaCobte " & ex.Message)
        End Try

        TomaCobte = nNroCom
    End Function
    Public Function TomaCobteAsiento(ByVal cProceso As String) As Long
        Dim nNroCom As Long = 0
        Dim DACobte As MySqlDataAdapter = New MySqlDataAdapter
        Dim DRCobte As MySqlDataReader
        Dim DTProceso As DataTable = New DataTable
        Dim cmdCobte As MySqlCommand = New MySqlCommand
        Try
            DACobte = consultaBDadapter(
                "numeros JOIN procesos ON num_numerador = pro_numerador",
                "pro_numerador, num_nrocom",
                "num_unegos=" & nPubNroIns & " AND num_nrocli=" & nPubNroCli & " AND pro_codigo='" & cProceso & "'"
            )
            DACobte.Fill(DTProceso)
            If DTProceso.Rows.Count = 1 Then
                nNroCom = DTProceso.Rows.Item(0).Item("num_nrocom").ToString + 1
                cmdCobte.CommandText = "UPDATE numeros " &
                    "SET num_nrocom = '" & nNroCom & "'," &
                    "num_fecha = '" & Format(Date.Today, "yyyy-MM-dd") & "' " &
                    " WHERE num_unegos=" & nPubNroIns & " AND num_nrocli=" & nPubNroCli & " AND num_numerador = '" & DTProceso.Rows(0).Item("pro_numerador") & "';"
            Else
                DACobte = consultaBDadapter("procesos", "pro_numerador", "pro_codigo='" & cProceso & "'")
                DACobte.Fill(DTProceso)
                If DTProceso.Rows.Count = 0 Then
                    MessageBox.Show("No se encontro numerador del proceso " & cProceso, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Try
                End If
                nNroCom = 1
                cmdCobte.CommandText = "INSERT INTO numeros (num_unegos, num_nrocli, num_numerador , num_puncom , num_nrocom, num_fecha ) " &
                    "VALUES ('" & nPubNroIns & "','" & nPubNroCli & "','" & DTProceso.Rows(0).Item("pro_numerador") & "','" & 0 & "','" & nNroCom & "','" & Format(Date.Today, "yyyy-MM-dd") & "');"
            End If

            cmdCobte.CommandType = CommandType.Text
            cmdCobte.Connection = conexion
            DRCobte = cmdCobte.ExecuteReader()
            DRCobte.Close()
        Catch ex As Exception
            nNroCom = 0
            MessageBox.Show("Error en TomaCobteAsiento " & ex.Message)
        End Try

        TomaCobteAsiento = nNroCom & Format(nPubNroIns, "00") & Format(nPubNroCli, "00")
    End Function
    'Public Function TomaCobteLiquidacion(ByVal nInstitucion As Integer, ByVal nDelegacion As Integer, ByVal cProceso As String, Optional ByVal lActualizaCorrelatividad As Boolean = True) As Long
    '    Dim nNroCom As Long = 0
    '    Dim DACobte As MySqlDataAdapter = New MySqlDataAdapter
    '    Dim DRCobte As MySqlDataReader
    '    Dim DTProceso As DataTable = New DataTable
    '    Dim cmdCobte As MySqlCommand = New MySqlCommand
    '    Try
    '        DACobte = consultaBDadapter("numeros JOIN procesos ON num_numerador = pro_numerador", "pro_numerador, num_nrocom", "num_unegos=" & nInstitucion & " AND num_nrocli=" & nDelegacion & " AND pro_codigo='" & cProceso & "'")
    '        DACobte.Fill(DTProceso)
    '        If DTProceso.Rows.Count = 1 Then
    '            nNroCom = DTProceso.Rows.Item(0).Item("num_nrocom").ToString + 1
    '            If lActualizaCorrelatividad Then
    '                cmdCobte.CommandText = "UPDATE numeros " & _
    '                    "SET num_nrocom = '" & nNroCom & "'," & _
    '                    "num_fecha = '" & Format(Date.Today, "yyyy-MM-dd") & "' " & _
    '                    " WHERE num_numerador = '" & DTProceso.Rows(0).Item("pro_numerador") & "' AND num_unegos=" & nInstitucion & " AND num_nrocli=" & nDelegacion & ";"
    '            End If
    '        Else
    '            DACobte = consultaBDadapter("procesos", "pro_numerador", "pro_codigo='" & cProceso & "'")
    '            DACobte.Fill(DTProceso)
    '            If DTProceso.Rows.Count = 0 Then
    '                MessageBox.Show("No se encontro numerador del proceso " & cProceso, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                Exit Try
    '            End If
    '            nNroCom = 1
    '            If lActualizaCorrelatividad Then
    '                cmdCobte.CommandText = "INSERT INTO numeros (num_unegos, num_nrocli, num_numerador , num_puncom , num_nrocom, num_fecha ) " & _
    '                    "VALUES ('" & nInstitucion & "','" & nDelegacion & "','" & DTProceso.Rows(0).Item("pro_numerador") & "','" & 0 & "','" & nNroCom & "','" & Format(Date.Today, "yyyy-MM-dd") & "');"
    '            End If
    '        End If
    '        If lActualizaCorrelatividad Then
    '            cmdCobte.CommandType = CommandType.Text
    '            cmdCobte.Connection = conexion
    '            DRCobte = cmdCobte.ExecuteReader()
    '            DRCobte.Close()
    '        End If
    '    Catch ex As Exception
    '        nNroCom = 0
    '        MessageBox.Show("Error en TomaCobte " & ex.Message)
    '    End Try

    '    TomaCobteLiquidacion = nNroCom
    'End Function
    'Por defecto es solo Get... Para actualizar pasar lActualizaCorrelatividad = True y nroDeOblea
    Public Function GetUpdateUltimoNumero(ByVal nNumerador As String, ByVal nInstitucion As Integer, ByVal nDelegacion As Integer, Optional ByVal lActualizaCorrelatividad As Boolean = False, Optional ByVal nNroComActualiza As Long = 1) As Long
        Dim nNroCom As Long = 0
        Dim DACobte As MySqlDataAdapter
        Dim DRCobte As MySqlDataReader
        Dim DTCobte As DataTable
        Dim cmdCobte As MySqlCommand
        Try
            DACobte = New MySqlDataAdapter
            DTCobte = New DataTable
            cmdCobte = New MySqlCommand

            DACobte = consultaBDadapter("numeros", , "num_unegos=" & nInstitucion & " and num_nrocli=" & nDelegacion & " AND num_numerador = '" & nNumerador & "'")
            DACobte.Fill(DTCobte)

            If DTCobte.Rows.Count = 1 Then
                nNroCom = DTCobte.Rows(0).Item("num_nrocom").ToString + 1
                'nNroCom = rstOrigen!num_nrocom + 1
                If lActualizaCorrelatividad Then
                    cmdCobte.CommandText = "UPDATE numeros " &
                                      "SET num_nrocom  = '" & nNroComActualiza & "'," &
                                      "num_fecha   = '" & Format(Date.Today, "yyyy-MM-dd") & "' " &
                                      " WHERE num_numerador = '" & nNumerador & "' AND num_unegos=" & nInstitucion & " AND num_nrocli=" & nDelegacion & ";"
                End If
            Else
                nNroCom = 1
                If lActualizaCorrelatividad Then
                    cmdCobte.CommandText = "INSERT INTO numeros (num_unegos, num_nrocli, num_numerador , num_puncom , num_nrocom, num_fecha ) " &
                                         "VALUES ('" & nInstitucion & "','" & nDelegacion & "','" & nNumerador & "','" & 0 & "','" & nNroComActualiza & "','" & Format(Date.Today, "yyyy-MM-dd") & "');"
                End If
            End If
            If lActualizaCorrelatividad Then
                cmdCobte.CommandType = CommandType.Text
                cmdCobte.Connection = conexion
                DRCobte = cmdCobte.ExecuteReader()
                DRCobte.Close()
            End If
        Catch ex As Exception
            MessageBox.Show("Error en Tomacobte " & ex.Message)
        End Try

        GetUpdateUltimoNumero = nNroCom
    End Function

    Public Function TomaNroMatricula(ByVal cTitulo As String) As Long
        Dim nNroMtr As Long = 0
        Dim DAMatr As MySqlDataAdapter
        Dim DTMatr As DataTable
        Dim BuilderMtr As MySqlCommandBuilder
        Try
            DAMatr = New MySqlDataAdapter
            DTMatr = New DataTable

            DAMatr = consultaBDadapter("titulos", , "tit_idtitulo='" & cTitulo & "'")
            BuilderMtr = New MySqlCommandBuilder(DAMatr)
            DAMatr.Fill(DTMatr)

            If DTMatr.Rows.Count = 0 Then
                MessageBox.Show("No se encontro numerador del titulo " & cTitulo, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Try
            End If

            nNroMtr = DTMatr.Rows(0).Item("tit_matricula").ToString + 1
            DTMatr.Rows(0).Item("tit_matricula") = nNroMtr
            DAMatr.Update(DTMatr)
        Catch ex As Exception
            MessageBox.Show("Error en TomaNroMatricula " & ex.Message)
        End Try
        TomaNroMatricula = nNroMtr
    End Function

    Public Function CajaActiva() As Boolean
        Dim dsCaja As New DataSet
        Dim daCaja As New MySqlDataAdapter
        daCaja = consultaBDadapter("cajas", , "caj_cajact = '*' AND caj_nrocli=" & nPubNroCli & " and caj_caja=1")
        daCaja.Fill(dsCaja, "caja")
        NroRegistradora = 0
        NroCaja = 0
        NroZeta = 0
        If dsCaja.Tables(0).Rows.Count = 1 Then
			If dsCaja.Tables(0).Rows(0).Item("caj_fecven").ToString = Format(Date.Today, "dd/MM/yyyy") Then
				' Me.NroRegistradora = dsCaja.Tables(0).Rows(0).Item("caj_nroreg")
				NroCaja = dsCaja.Tables(0).Rows(0).Item("caj_caja")
				NroZeta = dsCaja.Tables(0).Rows(0).Item("caj_zeta")
				Return True
			Else
				MessageBox.Show("No coincide la fecha actual con la de la caja activa, por favor CONTROLE...", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return False
            End If
        Else
            MessageBox.Show("NO HAY CAJA ACTIVA", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
    End Function

    Public ReadOnly Property FechaHoraActual() As String
        Get
            Return Format(Now.Date, "yyyy-MM-dd") & " " & Mid(Now.TimeOfDay.ToString, 1, 8)
        End Get
    End Property

    Public Property Registradora() As Double
        Get
            Return NroRegistradora
        End Get
        Set(ByVal value As Double)
            NroRegistradora = value
        End Set
    End Property

    Public Property Caja() As Double
        Get
            Return NroCaja
        End Get
        Set(ByVal value As Double)
            NroCaja = value
        End Set
    End Property

    Public Property Zeta() As Double
        Get
            Return NroZeta
        End Get
        Set(ByVal value As Double)
            NroZeta = value
        End Set
    End Property

    'Public Function ControlAcceso(ByVal nNroAcc As Integer) As Boolean
    '    Dim lAcceso As Boolean
    '    Dim DSAcceso As New DataSet
    '    Dim DAAcceso As New MySqlDataAdapter

    '    If nPubNroCli = 55 Then
    '        DAAcceso = consultaBDadapter("permisos", , "per_operador=" & nPubNroOperador & " AND per_acceso=" & nNroAcc & " and per_nrocli=" & nPubNroCli)
    '    Else
    '        DAAcceso = consultaBDadapter("permisos", , "per_operador=" & nPubNroOperador & " AND per_acceso=" & nNroAcc)
    '    End If

    '    DAAcceso.Fill(DSAcceso, "Acceso")

    '    If DSAcceso.Tables(0).Rows.Count = 1 Then
    '        If DSAcceso.Tables(0).Rows(0).Item("per_permitido") = "SI" Then
    '            lAcceso = True
    '        Else
    '            lAcceso = False
    '        End If
    '    Else
    '        lAcceso = False
    '    End If
    '    If Not lAcceso Then
    '        MessageBox.Show("USTED NO ESTA HABILITADO PARA ESTA OPERACION", "Accesos", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Return lAcceso
    '    Else
    '        Return lAcceso
    '    End If
    'End Function

    Public Sub ProcesoTotal(ByVal cCodPro As String, ByVal nItem As Integer)
        Dim dsPTot As New DataSet
        Dim daPtot As New MySqlDataAdapter
        daPtot = consultaBDadapter("procetote", , "pto_codpro = '" & cCodPro & "' AND pto_item =" & nItem)
        daPtot.Fill(dsPTot, "proceso")

        If dsPTot.Tables(0).Rows.Count = 1 Then
            NroPlanProceso = dsPTot.Tables(0).Rows(0).Item("pto_nropla")
        Else
            NroPlanProceso = ""
        End If
    End Sub

    Public Property NroPlan() As String
        Get
            Return NroPlanProceso
        End Get
        Set(ByVal value As String)
            NroPlanProceso = value
        End Set
    End Property

    Public ReadOnly Property NumeroCuenta() As String
        Get
            Return NumeroPlan
        End Get
    End Property

    Public ReadOnly Property DescripcionCuenta() As String
        Get
            Return DescripcionPlan
        End Get
    End Property

    Public Function TraeCuenta(ByVal cCuenta As String) As Boolean
        Dim dtCuenta As New DataTable
        Dim daCuenta As New MySqlDataAdapter
        daCuenta = consultaBDadapter("plancuen", , "pla_nropla = '" & cCuenta & "'")
        daCuenta.Fill(dtCuenta)

        If dtCuenta.Rows.Count = 1 Then
            NumeroPlan = dtCuenta.Rows(0).Item("pla_nropla")
            DescripcionPlan = dtCuenta.Rows(0).Item("pla_nombre")
            TraeCuenta = True
        Else
            NumeroPlan = ""
            DescripcionPlan = ""
            TraeCuenta = False
        End If
    End Function

    Public Function GraboAsiento(ByVal DTDetalles As DataTable, ByVal DTCheques As DataTable, ByVal DTCuotas As DataTable, ByVal DTMoratoria As DataTable, ByVal cConcepto1 As String, ByVal cConcepto2 As String, ByVal cConcepto3 As String, ByVal destinatarioExterno As String, ByVal nCaja As Integer, ByVal nZeta As Integer, ByVal lMultiProceso As Boolean) As Long
        Dim nNroAsiento As Long = 0
        ' PERMITE GRABAR MULTIPLES ASIENTOS, DEPENDIENDO DE LA CANTIDAD DE PROCESOS QUE SE ENVIA EN DTDETALLES
        If AbrirConexion() Then
            Bloqueo(True)
            Dim miTrans As MySqlTransaction
            miTrans = InicioTransaccion()
            Try
                Dim nInstitucion As Integer = DTDetalles.Rows(0).Item("institucion")
                Dim nDelegacion As Integer = DTDetalles.Rows(0).Item("delegacion")
                Dim cProceso As String = ""
                Dim FechaHora As String = ""
                Dim nNroAsiGrupal As Long
                Dim nPrimerNroAsi As Long = 0
                Dim nNrocomprobante As Long
                Dim cTipDocDestinatario As String = ""
                Dim nNroDocDestinatario As Integer = 0
                Dim cTitMtrDestinatario As String = ""
                Dim nMatMtrDestinataria As Integer = 0
                Dim cTipDoc As String = ""
                Dim nNroDoc As Integer = 0
                Dim cTitMtr As String = ""
                Dim nMatMtr As Integer = 0
                Dim nItem As Integer = 0
                Dim nTotal As Double = 0
                Dim ltieneCheques As Boolean = False
                Dim numeroDeCuota As Integer = 1

                ' si es multiproceso cargo numero de asiento grupal
                If lMultiProceso Then
                    nNroAsiGrupal = TomaCobteAsiento("ASIENT")
                Else
                    nNroAsiGrupal = 0
                End If

                For Each RowAsi As DataRow In DTDetalles.Rows
                    If cProceso <> RowAsi.Item("proceso") Then
                        ' si cProceso no es blanco inserto el registro del asiento en comproba
                        If cProceso <> "" Then
                            ReplaceBD("insert into comproba set com_unegos='" & nInstitucion & "'," &
                                      "com_nrodeleg='" & nDelegacion & "'," &
                                      "com_proceso='" & cProceso & "'," &
                                      "com_nrocli='" & nDelegacion & "'," &
                                      "com_nrocom='" & nNrocomprobante & "'," &
                                      "com_nroasi='" & nNroAsiento & "'," &
                                      "com_asigrupal='" & nNroAsiGrupal & "'," &
                                      "com_fecha='" & FechaHora & "'," &
                                      "com_total='" & nTotal.ToString.Replace(",", ".") & "'," &
                                      "com_tipdoc='" & cTipDocDestinatario & "'," &
                                      "com_nrodoc='" & nNroDocDestinatario & "'," &
                                      "com_titulo='" & cTitMtrDestinatario & "'," &
                                      "com_matricula='" & nMatMtrDestinataria & "'," &
                                      "com_nroope='" & nPubNroOperador & "'," &
                                      "com_caja='" & nCaja & "'," &
                                      "com_zeta='" & nZeta & "'," &
                                      "com_concepto1='" & cConcepto1 & "'," &
                                      "com_concepto2='" & cConcepto2 & "'," &
                                      "com_concepto3='" & cConcepto3 & "'," &
                                      "com_destinatario='" & destinatarioExterno & "'," &
                                      "com_fecalt='" & FechaHoraActual & "'")
                        End If
                        nInstitucion = RowAsi.Item("institucion")
                        nDelegacion = RowAsi.Item("delegacion")
                        cProceso = RowAsi.Item("proceso")
                        FechaHora = RowAsi.Item("fecha")
                        nNroAsiento = TomaCobteAsiento("ASIENT")
                        ' guardo el primer numero de asiento para cargarlo en las cuotas que cancela
                        If nPrimerNroAsi = 0 Then
                            nPrimerNroAsi = nNroAsiento
                        End If
                        ' nNrocomprobante = TomaCobte(nInstitucion, nDelegacion, cProceso)
                        ' apartir del 19/02/14 toma el numero de la delegacion donde hace el asiento para que el numero de comprobante sea correlativo
                        nNrocomprobante = TomaCobte(nInstitucion, nPubNroCli, cProceso)
                        nItem = 0
                        nTotal = 0
                    End If
                    'Si el asiento tiene el campo concepto2 entra
                    If DTDetalles.Columns.Contains("concepto2") Then
                        'Si no esta vacio guarda el valor en cConcepto2 cambiando el detalle del comprobante
                        If Not IsDBNull(RowAsi.Item("concepto2")) And Not String.IsNullOrEmpty(RowAsi.Item("concepto2").ToString()) Then
                            cConcepto2 = RowAsi.Item("concepto2").ToString()
                        End If
                    End If
                    If RowAsi.Item("Debe") > 0 Or RowAsi.Item("Haber") > 0 Then
                        If RowAsi.Item("Subcuenta").ToString.Trim <> "" Then
                            ' si es el destinatario a nombre de quien sale el asiento
                            If RowAsi.Item("destinatario") Then
                                cTipDocDestinatario = Mid(RowAsi.Item("subcuenta"), 1, 3)
                                nNroDocDestinatario = CInt(Mid(RowAsi.Item("subcuenta"), 4))
                                cTitMtrDestinatario = Mid(RowAsi.Item("Matricula"), 1, 2)
                                nMatMtrDestinataria = CInt(Mid(RowAsi.Item("Matricula"), 3))
                            End If
                            cTipDoc = Mid(RowAsi.Item("subcuenta"), 1, 3)
                            nNroDoc = CInt(Mid(RowAsi.Item("subcuenta"), 4))
                            cTitMtr = Mid(RowAsi.Item("Matricula"), 1, 2)
                            nMatMtr = CInt(Mid(RowAsi.Item("Matricula"), 3))
                        Else
                            cTipDoc = ""
                            nNroDoc = 0
                            cTitMtr = ""
                            nMatMtr = 0
                        End If

                        If DTCheques Is Nothing Then
                            ltieneCheques = False
                        Else
                            ltieneCheques = False

                            For Each RowChe As DataRow In DTCheques.Rows
                                If RowAsi.Item("pto_item") = RowChe.Item("item") Then
                                    If Not IsDBNull(RowChe.Item("monto")) Then
                                        ltieneCheques = True
                                        nItem += 1
                                        Dim cFechasCheque As String = ""
                                        If Not IsDBNull(RowChe.Item("fechaEmision")) Then
                                            cFechasCheque = "tot_fecche='" & Format(CDate(RowChe.Item("fechaEmision")), "yyyy-MM-dd") & "',"
                                        End If

                                        If Not IsDBNull(RowChe.Item("fechaDiferida")) Then
                                            cFechasCheque += "tot_fecdif='" & Format(CDate(RowChe.Item("fechaDiferida")), "yyyy-MM-dd") & "',"
                                        End If

                                        ReplaceBD("insert into totales set tot_unegos='" & nInstitucion & "'," &
                                                     "tot_proceso='" & cProceso & "'," &
                                                     "tot_nrocli='" & nDelegacion & "'," &
                                                     "tot_nrocom='" & nNrocomprobante & "'," &
                                                     "tot_item='" & nItem & "'," &
                                                     "tot_nropla='" & RowAsi.Item("cuenta") & "'," &
                                                     "tot_subpla='" & RowAsi.Item("subcuenta") & "'," &
                                                     "tot_titulo='" & cTitMtr & "'," &
                                                     "tot_matricula='" & nMatMtr & "'," &
                                                     "tot_tipdoc='" & cTipDoc & "'," &
                                                     "tot_nrodoc='" & nNroDoc & "'," &
                                                     "tot_nroope='" & nPubNroOperador & "'," &
                                                     "tot_nrocuo='" & 1 & "'," &
                                                     "tot_fecha='" & FechaHora & "'," &
                                                     "tot_fecven='" & Format(CDate(RowAsi.Item("fecven").ToString), "yyyy-MM-dd") & "'," &
                                                     "tot_fecalt='" & FechaHoraActual & "'," &
                                                     "tot_nrocheque='" & RowChe.Item("cheque") & "'," &
                                                      cFechasCheque &
                                                     "tot_debe='" & IIf(RowAsi.Item("debe") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," &
                                                     "tot_haber='" & IIf(RowAsi.Item("haber") > 0, RowChe.Item("monto").ToString.Replace(",", "."), 0) & "'," &
                                                     "tot_imppag='" & RowAsi.Item("imppagado").ToString.Replace(",", ".") & "'," &
                                                     "tot_tipdes='" & cTipDoc & "'," &
                                                     "tot_nrodes='" & nNroDoc & "'," &
                                                     "tot_nroasi='" & nNroAsiento & "'," &
                                                     "tot_asigrupal='" & nNroAsiGrupal & "'," &
                                                     "tot_caja='" & nCaja & "'," &
                                                     "tot_zeta='" & nZeta & "'," &
                                                     "tot_concepto='" & RowChe.Item("Banco") & "'")
                                    End If
                                End If
                            Next
                        End If
                        If Not ltieneCheques Then
                            nItem += 1
                            Dim dFechaVencimento As Date
                            dFechaVencimento = CDate(RowAsi.Item("fecven"))
                            For nCuo As Integer = 1 To RowAsi.Item("cuotas")
                                If RowAsi.Table.Columns.Contains("numerodecuota") Then
                                    If RowAsi.Item("numerodecuota") Then
                                        numeroDeCuota = RowAsi.Item("numerodecuota")
                                    Else
                                        numeroDeCuota = nCuo
                                    End If
                                Else
                                    numeroDeCuota = nCuo
                                End If
                                ReplaceBD("insert into totales set tot_unegos='" & nInstitucion & "'," &
                                          "tot_proceso='" & cProceso & "'," &
                                          "tot_nrocli='" & nDelegacion & "'," &
                                          "tot_nrocom='" & nNrocomprobante & "'," &
                                          "tot_item='" & nItem & "'," &
                                          "tot_nropla='" & RowAsi.Item("cuenta") & "'," &
                                          "tot_subpla='" & RowAsi.Item("subcuenta") & "'," &
                                          "tot_titulo='" & cTitMtr & "'," &
                                          "tot_matricula='" & nMatMtr & "'," &
                                          "tot_tipdoc='" & cTipDoc & "'," &
                                          "tot_nrodoc='" & nNroDoc & "'," &
                                          "tot_nroope='" & nPubNroOperador & "'," &
                                          "tot_nrocuo='" & numeroDeCuota & "'," &
                                          "tot_fecha='" & FechaHora & "'," &
                                          "tot_fecven='" & Format(dFechaVencimento, "yyyy-MM-dd") & "'," &
                                          "tot_fecalt='" & FechaHoraActual & "'," &
                                          "tot_debe='" & (RowAsi.Item("debe") / RowAsi.Item("cuotas")).ToString.Replace(",", ".") & "'," &
                                          "tot_haber='" & (RowAsi.Item("haber") / RowAsi.Item("cuotas")).ToString.Replace(",", ".") & "'," &
                                          "tot_imppag='" & (RowAsi.Item("imppagado") / RowAsi.Item("cuotas")).ToString.Replace(",", ".") & "'," &
                                          "tot_nroasi='" & nNroAsiento & "'," &
                                          "tot_asigrupal='" & nNroAsiGrupal & "'," &
                                          "tot_caja='" & nCaja & "'," &
                                          "tot_zeta='" & nZeta & "'," &
                                          "tot_concepto='" & Mid(cConcepto1, 1, 50) & "'")
                                dFechaVencimento = dFechaVencimento.AddMonths(1)
                            Next
                        End If
                        ' cancelos las cuotas de los servicio
                        If Not DTCuotas Is Nothing Then
                            If DTCuotas.Rows.Count > 0 Then
                                Dim nMontoCuota As Double
                                For Each rowCuo As DataRow In DTCuotas.Rows
                                    If cProceso = rowCuo.Item("procancel") Then
                                        If Not IsDBNull(rowCuo.Item("select")) AndAlso rowCuo.Item("select") Then
                                            nMontoCuota = rowCuo.Item("debe") + rowCuo.Item("haber")
                                            ReplaceBD("update totales set tot_imppag='" & nMontoCuota.ToString.Replace(",", ".") & "'," &
                                                      "tot_asicancel='" & nNroAsiento & "'" &
                                                      " where tot_nroasi=" & rowCuo.Item("tot_nroasi") & " and tot_item=" & rowCuo.Item("tot_item") & " and tot_nrocuo=" & rowCuo.Item("nroCuota"))
                                            '" where tot_nroasi=" & rowCuo.Item("tot_nroasi") & " and tot_item=" & rowCuo.Item("tot_item") & " and tot_nrocuo=" & numeroDeCuota)
                                        End If
                                    End If
                                    ' End If
                                Next
                            End If
                        End If
                        ' si cancela cuota moratoria
                        If Not DTMoratoria Is Nothing Then
                            If cProceso = "01RCMO" Or cProceso = "01DCMO" Then
                                For Each rowMora As DataRow In DTMoratoria.Rows
                                    If rowMora.Item("Select") Then
                                        ReplaceBD("update cuo_det set f_pago='" & FechaHoraActual & "'," &
                                          "asiento='" & nNroAsiento & "',recibo='" & nNrocomprobante & "'" &
                                          " where matricula=" & rowMora.Item("matricula") & " and numero=" & rowMora.Item("numero") & " and ncuota=" & rowMora.Item("ncuota"))
                                    End If
                                Next
                            End If
                        End If
                    End If
                    ' voy sumando en nTotal el valor de haber para que me de el total
                    nTotal += RowAsi.Item("haber")
                Next
                ' cancelos las cuotas de los servicio
                'If Not DTCuotas Is Nothing Then
                '    If DTCuotas.Rows.Count > 0 Then
                '        Dim nMontoCuota As Double
                '        For Each rowCuo As DataRow In DTCuotas.Rows
                '            ' If RowAsi.Item("pto_item") = rowCuo.Item("item") Then
                '            If Not IsDBNull(rowCuo.Item("select")) AndAlso rowCuo.Item("select") Then
                '                nMontoCuota = rowCuo.Item("debe") + rowCuo.Item("haber")
                '                ReplaceBD("update totales set tot_imppag='" & nMontoCuota.ToString.Replace(",", ".") & "'," & _
                '                                         "tot_asicancel='" & nPrimerNroAsi & "'" & _
                '                                         " where tot_nroasi=" & rowCuo.Item("tot_nroasi") & " and tot_item=" & rowCuo.Item("tot_item") & " and tot_nrocuo=" & rowCuo.Item("tot_nrocuo"))
                '            End If
                '            '  End If
                '            ' End If
                '        Next
                '    End If
                'End If
                '' si cancela cuota moratoria
                'For Each rowMora As DataRow In DTMoratoria.Rows
                '    If rowMora.Item("Select") Then
                '        ReplaceBD("update cuo_det set f_pago='" & FechaHoraActual & "'," & _
                '          "asiento='" & nPrimerNroAsi & "',recibo='" & nNrocomprobante & "'" & _
                '          " where matricula=" & rowMora.Item("matricula") & " and numero=" & rowMora.Item("numero") & " and ncuota=" & rowMora.Item("ncuota"))
                '    End If
                'Next

                If cProceso <> "" Then
                    ReplaceBD("insert into comproba set com_unegos='" & nInstitucion & "'," &
                              "com_nrodeleg='" & nDelegacion & "'," &
                              "com_proceso='" & cProceso & "'," &
                              "com_nrocli='" & nDelegacion & "'," &
                              "com_nrocom='" & nNrocomprobante & "'," &
                              "com_nroasi='" & nNroAsiento & "'," &
                              "com_asigrupal='" & nNroAsiGrupal & "'," &
                              "com_fecha='" & FechaHora & "'," &
                              "com_fecalt='" & FechaHoraActual & "'," &
                              "com_total='" & nTotal.ToString.Replace(",", ".") & "'," &
                              "com_tipdoc='" & cTipDocDestinatario & "'," &
                              "com_nrodoc='" & nNroDocDestinatario & "'," &
                              "com_titulo='" & cTitMtrDestinatario & "'," &
                              "com_matricula='" & nMatMtrDestinataria & "'," &
                              "com_nroope='" & nPubNroOperador & "'," &
                              "com_caja='" & nCaja & "'," &
                              "com_zeta='" & nZeta & "'," &
                              "com_concepto1='" & cConcepto1 & "'," &
                              "com_concepto2='" & cConcepto2 & "'," &
                              "com_concepto3='" & cConcepto3 & "'," &
                              "com_destinatario='" & destinatarioExterno &
                              "'")
                End If
                trabajoNroAsiento = nNroAsiento 'Seteo el numero de asiento antes de cambiar el estado del trabajo
                CambiarEstadoTrabajo() 'Cambio el estado del trabajo si es que se seteo el ID
                miTrans.Commit()
                Bloqueo(False)
            Catch ex As Exception
                miTrans.Rollback()
                Bloqueo(False)
                SetTrabajoDefaultValues()
                MessageBox.Show(ex.Message)
            End Try
            'Try
            enviarCorreo()
            'Catch ex As Exception
            'End Try
            SetTrabajoDefaultValues()
            CerrarConexion()
        End If
        Return nNroAsiento
    End Function

    Public Sub CargaConexion()
        Dim CnnCnx As New OleDb.OleDbConnection
        Dim dsCnx As DataSet
        Dim daCnx As OleDb.OleDbDataAdapter
        Dim source As String

        If Application.StartupPath = "D:\TODO\dev\cpcemegs\cpceMEGS\bin\Debug" Then
            'Si codigo en desarrollo entra y utiliza conexion Desarrollo
            source = "conexionNETdev.mdb"
        ElseIf Application.StartupPath = "C:\CpceMEGStest" Then
            'Si codigo de prueba entra y utiliza conexion Pruebas
            source = "conexionNETtest.mdb"
        Else
            'Sino codigo en produccion utiliza conexion Produccion segun sucursal
            source = "conexionNET.mdb"
        End If

        CnnCnx.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + source + ";Persist Security Info=False"
        '--------------------------------------------------------------------------
        'ABRO EL ACCESS PARA ESCRIBIRLO
        '--------------------------------------------------------------------------
        Try
            CnnCnx.Open()
        Catch ex As Exception
            MessageBox.Show("El sistema no puede iniciarse. Error de conexión (01001). Contacte con el administrador.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Application.Exit()
        End Try
        dsCnx = New DataSet
        daCnx = New OleDb.OleDbDataAdapter("select * from conexion", CnnCnx)
        daCnx.Fill(dsCnx, "sucursal")
        CnnCnx.Close()
        For Each drCnx As DataRow In dsCnx.Tables(0).Rows
            If drCnx.Item("Activo") = "*" Then
                If Application.StartupPath = "D:\DEV\cpceMEGS\cpceMEGS\bin\Debug" Then
                    'Si codigo en desarrollo entra y muestra a que servidor conecta
                    MessageBox.Show("Servidor BD: " + drCnx.Item("servidor"))
                ElseIf Application.StartupPath = "C:\CpceMEGStest" Then
                    'Si codigo de pruebas entra y muestra a que servidor conecta
                    MessageBox.Show("Servidor BD: " + drCnx.Item("servidor"))
                End If
                cPubServidor = drCnx.Item("servidor")
                cPubBD = drCnx.Item("BD")
                cPubUsuario = drCnx.Item("usuario")
                nPubNroIns = drCnx.Item("institucion")
                nPubNroCli = drCnx.Item("sucursal")
                cPubConexion = drCnx.Item("conexion")
                cPubTipoConexion = drCnx.Item("tipo_conexion")
                'actualizo origen de datos para los dataset
                Dim connString As String = "server=" + cPubServidor + ";port=" + cPubPuerto + ";user id=" + cPubUsuario + ";Password=" + cPubClave + ";persist security info=False;database=" + cPubBD
                'DisplayConnectionStrings()
                UpdateConnectionStrings(connString)
                'fin
                Exit For
            End If
        Next
        dsCnx.Dispose()
        daCnx.Dispose()
        CnnCnx.Dispose()
    End Sub

    ' Show how to use OpenExeConfiguration(ConfigurationUserLevel) 
    ' and RefreshSection. 
    ' This function creates a configuration file for the application, if one
    ' does not exist.
    Shared Sub UpdateConnectionStrings(ByVal connString As String)
        Try
            'Se abre el app.config para recuperar la seccion ConnectionString
            Dim config As Configuration =
            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            'Se recupera la seccion ConnectionStrings
            Dim MiSeccion As ConnectionStringsSection =
            DirectCast(config.GetSection("connectionStrings"), ConnectionStringsSection)
            'Se establece el nuevo valor de la cadena de conexion
            MiSeccion.ConnectionStrings("cpceMEGS.My.MySettings.cpceConnectionString").ConnectionString = connString
            ' Encrypt the section.
            MiSeccion.SectionInformation.ProtectSection("DataProtectionConfigurationProvider")
            ' Save the configuration file.
            config.Save(ConfigurationSaveMode.Modified)
            ' Force a reload of the changed section.
            ConfigurationManager.RefreshSection("connectionStrings")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub 'UpdateAppSettings

    Public Function Edad(ByVal FechaNacimiento As Date) As Integer
        ' Se calcula la edad con respecto a la fecha del día en curso
        Edad = Int(DateDiff("d", FechaNacimiento, Now) / 365)
    End Function

    Function ReplaceTabla(ByRef tabla As String, ByVal registro As DataRow, ByRef campos As DataColumnCollection, ByRef nombreCampoLote As String, ByRef valorCampoLote As Integer) As String
        Dim r As Integer = 0
        Dim cCampos As String = ""
        Dim posicionCampoLote As Integer
        Dim colTipo As DataColumn
        Try
            cCampos = "REPLACE INTO `" + tabla + "` (`"
            For r = 0 To campos.Count - 2
                cCampos += campos.Item(r).ColumnName + "`,`"
                If campos.Item(r).ColumnName = nombreCampoLote Then
                    posicionCampoLote = r
                End If
            Next
            If campos.Item(r).ColumnName = nombreCampoLote Then
                posicionCampoLote = r
            End If
            cCampos += campos.Item(r).ColumnName + "`) VALUES ("
            For r = 0 To campos.Count - 1
                If r = posicionCampoLote Then
                    cCampos += "" + CStr(valorCampoLote)
                Else
                    colTipo = campos.Item(r)
                    Select Case colTipo.DataType.Name
                        Case "String"
                            ' If registro.Item(r).Equals(DBNull.Value) Then
                            'cCampos += "NULL"
                            'Else
                            Dim miString As String = registro.Item(r).ToString
                            Dim miPosi As Integer = miString.IndexOf("'")
                            If miPosi = -1 Then
                                cCampos += "'" + registro.Item(r) + "'"
                            Else
                                cCampos += "'" + Mid(registro.Item(r), 1, miPosi) + "\" + Mid(registro.Item(r), miPosi + 1) + "'"
                            End If
                        Case "Int32", "Int64"
                            If registro.Item(r).Equals(DBNull.Value) Then
                                cCampos += "NULL"
                            Else
                                cCampos += "" + CStr(registro.Item(r))
                            End If
                        Case "Decimal", "Double"
                            If registro.Item(r).Equals(DBNull.Value) Then
                                cCampos += "NULL"
                            Else
                                cCampos += "" + CStr(registro.Item(r)).Replace(",", ".")
                            End If
                        Case "MySqlDateTime"
                            If registro.Item(r).Equals(DBNull.Value) Then
                                cCampos += "NULL"
                            Else
                                ' MessageBox.Show(registro.Item(r).ToString)
                                If registro.Item(r).ToString = "00/00/0000" Then
                                    cCampos += "'0000-00-00'"
                                ElseIf registro.Item(r).ToString = "00/00/0000 00:00:00" Then
                                    cCampos += "'0000-00-00 00:00:00'"
                                Else
                                    If registro.Item(r).ToString.Length > 10 Then ' SI TIENE LA HORA SUPERA LOS 10 CHAR
                                        cCampos += "'" + Convert.ToString(Format(CDate(registro.Item(r).ToString), "yyyy-MM-dd HH:mm:ss")) + "'"
                                    Else
                                        cCampos += "'" + Convert.ToString(Format(CDate(registro.Item(r).ToString), "yyyy-MM-dd")) + "'"
                                    End If
                                End If
                            End If
                        Case "TimeSpan"
                            If registro.Item(r).Equals(DBNull.Value) Then
                                cCampos += "NULL"
                            Else
                                cCampos += "'" + Convert.ToString(registro.Item(r)) + "'"
                            End If
                        Case "Byte[]"
                            If registro.Item(r).Equals(DBNull.Value) Then
                                cCampos += "NULL"
                            Else
                                cCampos += "'" + Convert.ToString(registro.Item(r)) + "'"
                            End If
                        Case Else
                            MessageBox.Show("Falta tipo : " + colTipo.DataType.Name)
                    End Select
                End If
                If r < campos.Count - 1 Then
                    cCampos += ","
                End If
            Next
            cCampos += ");"
            ReplaceTabla = cCampos
        Finally
        End Try
        '  MessageBox.Show(cCampos)
    End Function

    Public Function ActualizoLote(ByRef tabla As String) As Integer
        ' Dim lConcurrencia As Boolean = True
        lectA = New MySqlDataAdapter("select * from catalogo where cat_tabla='" + tabla + "';", conexion)
        MiBuilder = New MySqlCommandBuilder(lectA)

        MiData = New DataSet
        lectA.Fill(MiData, "catalogo")
        While True
            Try
                MiData.Tables("catalogo").Rows(0).Item("cat_ultlote") += 1
                lectA.Update(MiData, "catalogo")
                Exit While
            Catch ex As DBConcurrencyException
                MiData = New DataSet
                lectA.Fill(MiData, "catalogo")
                'MessageBox.Show("No pudo actualizar lote " + tabla + " " + ex.Message)
                'lConcurrencia = False
            End Try
        End While
        ActualizoLote = MiData.Tables("catalogo").Rows(0).Item("cat_ultlote")
    End Function

    Public Sub BalanceoAsiento(ByVal nroAsiento As Long, ByVal diferencia As Double)
        lectA = New MySqlDataAdapter("select * from totales where tot_nroasi = '" & nroAsiento & "';", conexion)
        MiBuilder = New MySqlCommandBuilder(lectA)

        MiData = New DataSet
        lectA.Fill(MiData, "totales")
        Dim contador As Integer = 0
        If MiData.Tables("totales").Rows(0).Item("tot_proceso") = "02LIHN" Then
            While contador < MiData.Tables("totales").Rows().Count()
                If MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "31010402" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                ElseIf MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "31010403" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                ElseIf MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "21030200" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                End If
                contador += 1
            End While
        ElseIf MiData.Tables("totales").Rows(0).Item("tot_proceso") = "02P035" Or
               MiData.Tables("totales").Rows(0).Item("tot_proceso") = "02H001" Then
            While contador < MiData.Tables("totales").Rows().Count()
                If MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "13070200" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                End If
                contador += 1
            End While
            'ElseIf cPubProcesosSipres.Contains(MiData.Tables("totales").Rows(0).Item("tot_proceso")) Then
        ElseIf Mid(MiData.Tables("totales").Rows(0).Item("tot_proceso"), 1, 4) = "01R1" Or
               Mid(MiData.Tables("totales").Rows(0).Item("tot_proceso"), 1, 4) = "01D1" Or
               Mid(MiData.Tables("totales").Rows(0).Item("tot_proceso"), 1, 4) = "01L1" Then
            While contador < MiData.Tables("totales").Rows().Count()
                If MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "31021300" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                End If
                contador += 1
            End While
        ElseIf MiData.Tables("totales").Rows(0).Item("tot_proceso") = "01R022" Then
            While contador < MiData.Tables("totales").Rows().Count()
                If MiData.Tables("totales").Rows(contador).Item("tot_nropla") = "22050300" Then
                    MiData.Tables("totales").Rows(contador).Item("tot_haber") -= diferencia
                    lectA.Update(MiData, "totales")
                    Exit While
                End If
                contador += 1
            End While
        End If
    End Sub
    'TRABAJOS
    Public Function QueryTrabajoById(ByVal valorCodigoBarra As Integer) As MySqlDataAdapter
        QueryTrabajoById = consultaBDadapter(
            "trabajo JOIN tareas ON trabajo.tra_tarea_id = tareas.tar_codigo", , "id = " & valorCodigoBarra
        )
    End Function
	'OLIMPIADAS
	Public Function QueryFichaById(ByVal id As Integer) As MySqlDataAdapter
        QueryFichaById = consultaBDadapter(
            "insc_ficha JOIN afiliado ON insc_ficha.afi_tipdoc = afiliado.afi_tipdoc AND insc_ficha.afi_nrodoc = afiliado.afi_nrodoc", , "id = " & id
        )
    End Function

	Public Function ExisteTrabajoByNroAsientoGetEstado(ByVal nroAsiento As Long) As Integer
        ExisteTrabajoByNroAsientoGetEstado = 0
        lectA = consultaBDadapter("trabajo", , "tra_nroasi = " & nroAsiento)
        MiData = New DataSet()
        lectA.Fill(MiData, "trabajo")
        If MiData.Tables("trabajo").Rows.Count() > 0 Then
            ExisteTrabajoByNroAsientoGetEstado = MiData.Tables("trabajo").Rows(0).Item("tra_estado")
        End If
    End Function
    Public Function ExisteTrabajoByNroAsientoLiquidacionGetId(ByVal nroAsientoLiquidacion As Long) As Integer
        ExisteTrabajoByNroAsientoLiquidacionGetId = 0
        lectA = consultaBDadapter("trabajo", , "tra_nroasi_liquidacion = " & nroAsientoLiquidacion)
        MiData = New DataSet()
        lectA.Fill(MiData, "trabajo")
        If MiData.Tables("trabajo").Rows.Count() > 0 Then
            ExisteTrabajoByNroAsientoLiquidacionGetId = MiData.Tables("trabajo").Rows(0).Item("id")
        End If
    End Function
    Public Sub CambiarEstadoTrabajo()
        'Public Sub CambiarEstadoTrabajo(Optional ByVal soloEstado As Boolean = False)
        If trabajoId <> 0 And trabajoEstado <> 0 Then
            CambiarEstadoTrabajoQuery(trabajoId)
            If trabajoEstado = 8 Then
                If trabajosEnviarCorreo <> "NOENVIAR" Then
                    trabajosEnviarCorreo = trabajoId
                End If
            Else
                trabajosEnviarCorreo = Nothing
            End If
        Else
            If Not IsNothing(trabajoIds) Then
                For Each traId As Integer In trabajoIds
                    CambiarEstadoTrabajoQuery(traId)
                    If trabajoEstado = 8 Then
                        If trabajosEnviarCorreo <> "NOENVIAR" Then
                            trabajosEnviarCorreo = trabajosEnviarCorreo & traId & ", "
                        End If
                    Else
                        trabajosEnviarCorreo = Nothing
                    End If
                Next
            End If
        End If
        'SetTrabajoDefaultValues()
    End Sub
    Private Sub CambiarEstadoTrabajoQuery(ByVal traId As Integer)
        Dim query As String = "UPDATE trabajo SET tra_estado=" & trabajoEstado

        If trabajoNroAsiento <> 0 And trabajoNroAsientoActualizar Then 'Caja
            query &= ", tra_nroasi='" & trabajoNroAsiento & "'"
        End If
        If trabajoNroLegalizacion <> 0 Then 'Oblea
            query &= ", tra_nrolegali='" & trabajoNroLegalizacion & "'"
        End If
        If trabajoNroAsientoLiquidacion <> 0 Then 'Liquidacion
            query &= ", tra_nroasi_liquidacion='" & trabajoNroAsientoLiquidacion & "'"
        End If
        'If soloEstado Then
        'Solo actualiza el estado del trabajo
        'Else
        'Throw New System.Exception("Los datos para actualizar el trabajo no son correctos. CambiarEstadoTrabajo()")
        'End If

        query &= " WHERE id=" & traId
        ReplaceBD(query)
    End Sub
    Public Sub CambiarEstadoCertificadoTrabajo(ByVal id As Integer, ByVal certificado As Integer)
        'If id <> 0 Then
        Dim query As String = "UPDATE trabajo SET tra_certificado=" & certificado & " WHERE id=" & id
        ReplaceBD(query)
        'End If
    End Sub
    'ENVIO CORREO
    Private Sub enviarCorreo()
        If Not IsNothing(trabajosEnviarCorreo) And trabajosEnviarCorreo <> "NOENVIAR" Then
            Dim contenido As String
            If trabajosEnviarCorreo.Contains(",") = True Then
                contenido = "
                    <p>Estimado profesional</p>
                    <p>Su Reintegro por los trabajos con códigos nro: " & trabajosEnviarCorreo & "</p>
                    <p>se encontrarán a disposición en 48hs hábiles, según el medio de pago que usted haya elegido en Sede Central.</p>
                    <p>Otras Delegaciones, Consultar.</p>
                    <p>Atentamente,</p>
                    <p>CPCE Chaco.</p>
                "
            Else
                contenido = "
                    <p>Estimado profesional</p>
                    <p>Su Reintegro por el trabajo con código nro: " & trabajosEnviarCorreo & ",</p>
                    <p>se encontrará a disposición en 48hs hábiles, según el medio de pago que usted haya elegido en Sede Central.</p>
                    <p>Otras Delegaciones, Consultar.</p>
                    <p>Atentamente,</p>
                    <p>CPCE Chaco.</p>
                "
            End If
            Dim correos As Correos = New Correos()
            correos.enviarCorreo(
                trabajoDestinatario,
                "CPCE Chaco, Reintegro",
                contenido
            )
        End If
    End Sub

    Public Function DeterminaProceso(ByVal afi_tipdoc As String, afi_nrodoc As Integer) As String
        comandos = New MySqlCommand()
        comandos.CommandText = "select cat_recibo1 
                                from categorias 
                                join afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & "
                                and afi_categoria = cat_codigo;"
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion
        Return comandos.ExecuteScalar
    End Function

	Public Function DeterminaProcesoAportes(ByVal afi_tipdoc As String, afi_nrodoc As Integer) As String
		comandos = New MySqlCommand()
		comandos.CommandText = "select cat_recibo3
                                from categorias 
                                join afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & "
                                and afi_categoria = cat_codigo;"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		Return comandos.ExecuteScalar
	End Function

	Public Function DeterminaProcesoDeudoresAportes(ByVal afi_tipdoc As String, afi_nrodoc As Integer) As String
		Me.AbrirConexion()
		Dim scalar
		comandos = New MySqlCommand()
		comandos.CommandText = "select cat_recibo3
                                from categorias 
                                join afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & "
                                and afi_categoria = cat_codigo;"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		scalar = comandos.ExecuteScalar
		Me.CerrarConexion()
		Return scalar
	End Function

	Public Function buscaProcesoDescripcion(ByVal proCodigo As String) As String
		comandos = New MySqlCommand()
		comandos.CommandText = "SELECT pro_nombre 
                                FROM procesos 
                                WHERE pro_codigo = '" & proCodigo & "';"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		Return comandos.ExecuteScalar
	End Function

	Public Function buscaProcesoDescripcionScalar(ByVal proCodigo As String) As String
		Me.AbrirConexion()
		Dim scalar
		comandos = New MySqlCommand()
		comandos.CommandText = "SELECT pro_nombre 
                                FROM procesos 
                                WHERE pro_codigo = '" & proCodigo & "';"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		scalar = comandos.ExecuteScalar
		Me.CerrarConexion()
		Return scalar
	End Function

	Public Function buscaProcetotePorcentaje(ByVal proCodigo As String) As MySqlDataAdapter
		buscaProcetotePorcentaje = New MySqlDataAdapter(
			"select * 
            from procetote 
            where pto_codpro = '" & proCodigo & "';",
			conexion
		)
	End Function

	Public Function buscaCategoria(ByVal afi_tipdoc As String, afi_nrodoc As Integer) As String
		comandos = New MySqlCommand()
		comandos.CommandText = "select cat_codigo 
                                from categorias 
                                join afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & "
                                and afi_categoria = cat_codigo;"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		Return comandos.ExecuteScalar
	End Function

	Public Function buscaCategoriaScalar(ByVal afi_tipdoc As String, afi_nrodoc As Integer) As String
		Me.AbrirConexion()
		Dim scalar
		comandos = New MySqlCommand()
		comandos.CommandText = "select cat_codigo 
                                from categorias 
                                join afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & "
                                and afi_categoria = cat_codigo;"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		scalar = comandos.ExecuteScalar
		Me.CerrarConexion()
		Return scalar
	End Function

	Public Function buscaMatricula(afi_nrodoc As Integer) As String
		comandos = New MySqlCommand()
		comandos.CommandText = "select afi_matricula 
                                from afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & ";"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		Return comandos.ExecuteScalar
	End Function

	Public Function buscaMatriculaScalar(afi_nrodoc As Integer) As String
		Me.AbrirConexion()
		Dim scalar
		comandos = New MySqlCommand()
		comandos.CommandText = "select afi_matricula 
                                from afiliado 
                                where afi_tipdoc= 'DNI'
                                and afi_nrodoc=" & afi_nrodoc & ";"
		comandos.CommandType = CommandType.Text
		comandos.Connection = conexion
		scalar = comandos.ExecuteScalar
		Me.CerrarConexion()
		Return scalar
	End Function

    ''' <summary>
    ''' Obtiene todos los afiliados de la tabla de tipo Afiliado con Matricula X y sin Estudios Contables ni En Tramite
    ''' </summary>
    Public Function GetAfiliados() As MySqlDataAdapter
        'Dim fechaNow As DateTime = Now
        GetAfiliados = New MySqlDataAdapter(
            "SELECT afi_nombre AS ApellidoyNombre,
                    afi_nrodoc AS DNI,
                    CONCAT(afi_titulo, afi_matricula) AS Matricula,
                    afi_fecnac AS FechadeNacimiento,
                    YEAR(Now()) - YEAR(afi_fecnac) AS Edad,
                    afi_fecha_jubilacion AS FechadeJubilacion,
                    afi_categoria AS Categoria,
                    afi_telefono1 AS Telefono,
                    afi_mail AS Email
             FROM afiliado
             WHERE afi_tipo = 'A' AND
                    afi_matricula <> 0 AND
                    afi_titulo <> 'EC' AND
                    afi_titulo <> 'ET'     
             ORDER BY afi_nombre;",
          conexion
        )
    End Function

    ''' <summary>
    ''' Obtiene todos los afiliados de la tabla de tipo Afiliado con Matricula X y sin Estudios Contables ni En Tramite
    ''' </summary>
    Public Function GetAfiliadosJubilados() As MySqlDataAdapter
        'Dim fechaNow As DateTime = Now
        GetAfiliadosJubilados = New MySqlDataAdapter(
            "SELECT afi_nombre AS nombre,
                    afi_nrodoc AS dni,
                    CONCAT(afi_titulo, afi_matricula) AS matricula,
                    afi_fecnac AS FechadeNacimiento,
                    YEAR(Now()) - YEAR(afi_fecnac) AS edad,
                    afi_fecha_jubilacion AS fecha_jubilacion,
                    afi_jubi_nro_resolucion AS nro_resolucion,        
                    afi_jubi_edad_resolucion AS edad_resolucion,
                    afi_jubi_periodo_opto AS periodos,
                    afi_jubi_capitalizacion AS capitalizacion,
                    afi_categoria AS Categoria
             FROM afiliado
             WHERE afi_tipo = 'A' AND
                    afi_matricula <> 0 AND
                    afi_titulo <> 'EC' AND
                    afi_titulo <> 'ET' AND
                    afi_categoria = '170103'
             ORDER BY afi_nombre;",
          conexion
        )
    End Function

    'select pla_nropla, pla_nombre, sum(tot_debe - tot_haber) as saldo, tot_titulo, tot_matricula, tot_tipdoc, tot_nrodoc
    ''' <summary>
    ''' Busca saldo de una cuenta en la matricula x a fecha x.
    ''' </summary>
    Public Function GetSaldo(afi_nrodoc As Integer, cuenta As Integer, Optional fecha As Date = Nothing) As String
        comandos = New MySqlCommand()
        comandos.CommandText = "SELECT sum(tot_debe - tot_haber) AS saldo
            FROM totales 
            INNER JOIN plancuen on pla_nropla = tot_nropla
            WHERE tot_tipdoc = 'DNI' AND
                tot_nrodoc = " & afi_nrodoc & " AND
                pla_nropla = " & cuenta & " AND
                pla_servicio = 'Si' AND
                tot_estado <> '9' AND
                tot_fecven < '" & Format(Convert.ToDateTime(fecha), "yyyy-MM-dd 23:59:59") & "'
            ;"
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion
        Return comandos.ExecuteScalar
    End Function

    ''' <summary>
    ''' Devuelve una tabla de la resolucion x.
    ''' </summary>
    Public Function GetTablaResolucionSipres(resolucion As String) As MySqlDataAdapter
		GetTablaResolucionSipres = New MySqlDataAdapter(
			"SELECT *
             FROM   sipres_jub_res 
             WHERE  sipres_reso     = '" & resolucion & "';",
			conexion
		)
	End Function

    ''' <summary>
    ''' Devuelve nombres x de las resoluciones cargadas en bd.
    ''' </summary>
    Public Function GetNombresResolucionSipres() As MySqlDataAdapter
        GetNombresResolucionSipres = New MySqlDataAdapter(
            "SELECT * FROM sipres_jub_res GROUP BY sipres_reso;",
            conexion
        )
    End Function
	''' <summary>
	''' Obtiene todos los afiliados que cumplen años
	''' </summary>
	Public Function GetAfiliadosCumpleanios() As MySqlDataAdapter
		Dim fechaHoy = Format(Date.Today, "MM-dd")
		GetAfiliadosCumpleanios = New MySqlDataAdapter(
			"SELECT CONCAT(afi_nombre, ' ', afi_titulo, ' ', afi_matricula) AS Afiliado,
                    afi_fecnac                      AS Fecha, 
                    YEAR(Now()) - YEAR(afi_fecnac)  AS Edad
             FROM   afiliado
             WHERE  afi_tipo        = 'A'       AND 
                    afi_matricula   <> 0        AND 
                    afi_titulo      <> 'EC'     AND" &
			" DATE_FORMAT(afi_fecnac, '%m-%d') = '" & fechaHoy & "'" &
			" ORDER BY afi_nombre;",
		  conexion
		)
	End Function

    ''' <summary>
    ''' Obtiene de la tabla ec_table el estudio contable segun la matricula
    ''' </summary>
    Public Function GetEstudioContable(titulo As String, nroMatricula As String) As MySqlDataAdapter
        GetEstudioContable = New MySqlDataAdapter(
            "SELECT *
             FROM   ec_table
             WHERE  ec_titulo		= '" & titulo & "' AND
					ec_matricula	= '" & nroMatricula & "'
			 ORDER BY ec_prof_matricula;
			",
          conexion
        )
    End Function

    ''' <summary>
    ''' Obtiene todos los afiliados que cumplen años
    ''' </summary>
    Public Function GetFirmante(nroLegalizacion As String) As MySqlDataAdapter
        GetFirmante = New MySqlDataAdapter(
            "SELECT afi_titulo, afi_matricula, afi_tipdoc, afi_nrodoc
             FROM   afiliado
             LEFT JOIN obleas
             ON (afi_tipdoc = obl_tipimp and afi_nrodoc = obl_nroimp) 
             WHERE  obl_nrolegali=' " & nroLegalizacion & "';",
          conexion
        )
    End Function
End Class