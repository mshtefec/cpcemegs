﻿Imports MySql.Data.MySqlClient
Imports System.Configuration

Public Class ConexionUnica
    Private Shared Instancia As ConexionUnica = Nothing
    Private cnx As MySqlConnection
    'Se inicia el constructor por unica vez
    Private Sub New()
        Dim connString As String = "server=" & cPubServidor & ";port=" & cPubPuerto & ";" & "user id=" & cPubUsuario & ";" & "password=" & cPubClave & ";" & "database=" & cPubBD & ";Allow Zero Datetime=True;Convert Zero Datetime=False;Connect Timeout=600000;SslMode=none;"
        cnx = New MySqlConnection(connString)
        'actualizo origen de datos para los dataset
        UpdateConnectionStrings(connString)
    End Sub
    Public Shared Function getInstancia() As ConexionUnica
        If Instancia Is Nothing Then
            Instancia = New ConexionUnica()
        End If
        Return Instancia
    End Function
    Public Function GetConexion() As MySqlConnection
        Return cnx
    End Function
    ' Show how to use ConnectionStrings.
    ' This sub assumes that at least one connection string
    ' has been defined.
    Shared Sub DisplayConnectionStrings()
        ' Get the ConnectionStrings collection.
        Dim connections As ConnectionStringSettingsCollection = _
        ConfigurationManager.ConnectionStrings
        If connections.Count <> 0 Then
            Console.WriteLine("Connection strings:")
            ' Loop to get the collection elements.
            For Each connection As ConnectionStringSettings In connections
                Dim name As String = connection.Name
                Dim provider As String = connection.ProviderName
                Dim connectionString As String = connection.ConnectionString
                MessageBox.Show("Name: " + name + " - Provider: " + provider + " - Connection string: " + connectionString)
            Next
        Else
            MessageBox.Show("No connection string is defined.")
        End If
    End Sub 'DisplayConnectionStrings
    ' Show how to use OpenExeConfiguration(ConfigurationUserLevel) 
    ' and RefreshSection. 
    ' This function creates a configuration file for the application, if one
    ' does not exist.
    Shared Sub UpdateConnectionStrings(ByVal connString As String)
        Try
            'Se abre el app.config para recuperar la seccion ConnectionString
            Dim config As Configuration = _
            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            'Se recupera la seccion ConnectionStrings
            Dim MiSeccion As ConnectionStringsSection = _
            DirectCast(config.GetSection("connectionStrings"), ConnectionStringsSection)
            'Se establece el nuevo valor de la cadena de conexion
            MiSeccion.ConnectionStrings("cpceMEGS.My.MySettings.cpceConnectionString").ConnectionString = connString
            ' Encrypt the section.
            MiSeccion.SectionInformation.ProtectSection("DataProtectionConfigurationProvider")
            ' Save the configuration file.
            config.Save(ConfigurationSaveMode.Modified)
            ' Force a reload of the changed section.
            ConfigurationManager.RefreshSection("connectionStrings")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub 'UpdateAppSettings
End Class
