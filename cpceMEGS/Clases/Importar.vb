﻿Imports System.Data.OleDb
Imports Newtonsoft.Json
Imports MySql.Data.MySqlClient
Public Class Importar
    Private cnn As New ConsultaBD(True)
    'Para el interes se usan pubCuentasGeneranInteres y pubCuentasGeneranInteresOtros son valores globales
    ''' <summary>
    ''' Carga un excel a un datagrid.
    ''' </summary>
    Public Sub cargarGrid(ByRef dg As DataGridView, ByVal ruta As String, Optional ByVal hoja As String = "")
        'proveedor: oledb, datasource: ruta donde se encuentra el archivo, propiedades: tipo de archivo, hdr: 
        Dim provider As String = "Provider=Microsoft.ACE.OLEDB.12.0;"
        'data: url de la ubicacion del archivo
        Dim dataSource As String = "data source=" & ruta & ";"
        'propiedad: tipo de archivo y hdr: yes (establece que la primera fila son nombres de columna)
        Dim properties As String = "Extended Properties='Excel 12.0 Xml;HDR=Yes'"
        'genero la conexion para oledb
        Dim conexion As String = provider & dataSource & properties
        'MsgBox(conexion, MsgBoxStyle.Information, "VALOR:")
        Dim conn As OleDbConnection = New OleDbConnection(conexion)

        Try
            'genera una consulta que devuelve todos los campos de la hoja
            Dim da As OleDbDataAdapter = New OleDbDataAdapter("SELECT * FROM  [" & hoja & "$]", conn)

            conn.Open()
            Dim dt As DataTable = New DataTable()

            'creo un datatable con todos los valores de la hoja importada
            da.Fill(dt)

            'Elimino columnas que no me sirven
            dt.Columns.Remove("CUIT")
            dt.Columns.Remove("EMPRESA")
            dt.Columns.Remove("CODIGO")
            'dt.Columns.Remove("SERVICIO")
            dt.Columns.Remove("TIPO_DNI")
            dt.Columns.Remove("CUIT_DNI")
            dt.Columns.Remove("CBU")
            dt.Columns.Remove("IDCLIENTE")
            dt.Columns.Remove("RF_DEBIT")
            dt.Columns.Remove("FE_PRES")
            dt.Columns.Remove("FE_COMP")
            dt.Columns.Remove("FE_RECH")
            dt.Columns.Remove("FE_REVE")
            dt.Columns.Remove("IMP_REVE")
            dt.Columns.Remove("TRCNUMBER")

            'agrego la tabla al datagridview
            dg.DataSource = dt

            dg.Columns("DENOMINAC").HeaderText = "APELLIDO_NOMBRE"
            dg.Columns("NUM_DNI").HeaderText = "DNI"

            dt.Columns.Add("TOTAL", GetType(Double))
            dt.Columns("TOTAL").ReadOnly = False

            dt.Columns.Add("MATRICULA", GetType(String))
            dt.Columns("MATRICULA").ReadOnly = False

            dt.Columns.Add("PROCESO", GetType(String))
            dt.Columns("PROCESO").ReadOnly = False

            dt.Columns.Add("CATEGORIA", GetType(String))
            dt.Columns("CATEGORIA").ReadOnly = False

            dt.Columns.Add("DESCRIP_1", GetType(String))
            dt.Columns("DESCRIP_1").ReadOnly = False

            dt.Columns.Add("DESCRIP_2", GetType(String))
            dt.Columns("DESCRIP_2").ReadOnly = False

            dt.Columns.Add("DESCRIP_3", GetType(String))
            dt.Columns("DESCRIP_3").ReadOnly = False

            dt.Columns.Add("INTERES", GetType(Double))
            dt.Columns("INTERES").ReadOnly = False

            dt.Columns.Add("C1", GetType(String))
            dt.Columns("C1").ReadOnly = False

            dt.Columns.Add("C2", GetType(String))
            dt.Columns("C2").ReadOnly = False

            dt.Columns.Add("C3", GetType(String))
            dt.Columns("C3").ReadOnly = False

            dt.Columns.Add("C4", GetType(String))
            dt.Columns("C4").ReadOnly = False

            dt.Columns.Add("C5", GetType(String))
            dt.Columns("C5").ReadOnly = False

            dt.Columns.Add("C6", GetType(String))
            dt.Columns("C6").ReadOnly = False

            dt.Columns.Add("C7", GetType(String))
            dt.Columns("C7").ReadOnly = False

            dt.Columns.Add("C8", GetType(String))
            dt.Columns("C8").ReadOnly = False

            dt.Columns.Add("C9", GetType(String))
            dt.Columns("C9").ReadOnly = False

            dt.Columns.Add("C10", GetType(String))
            dt.Columns("C10").ReadOnly = False

            dt.Columns.Add("C11", GetType(String))
            dt.Columns("C11").ReadOnly = False

            MsgBox("Se ha cargado la importacion correctamente", MsgBoxStyle.Information, "Importado con exito")
        Catch
            MsgBox("Inserte un nombre válido de la Hoja que desea importar", MsgBoxStyle.Critical, "Error")
        Finally
            conn.Close()
        End Try
    End Sub

	''' <summary>
	''' Recibe un dataTable y lo parsea a json, (devuelve un string).
	''' </summary>
	Public Function generaJson(dt As DataTable, tipo As String, nombre As String, fechaComp As String) As String

		Dim key As Integer = 0
		Dim key_cuenta As Integer = 0

		Dim jsonDebito As New JsonDebito

		dt.TableName = tipo

		jsonDebito.tipo = tipo
		jsonDebito.servicio = nombre
		jsonDebito.operador = nPubNroOperador

		'controlo que la caja este activa
		If cnn.CajaActiva Then
			jsonDebito.caja = cnn.Caja
			jsonDebito.zeta = cnn.Zeta
		Else
			jsonDebito.caja = 0
			jsonDebito.zeta = 0
		End If

		jsonDebito.fecha_comp = fechaComp

		'recorro las filas de la tabla
		For Each row As DataRow In dt.Rows

			Dim jsonCuota As New JsonCuota

			jsonCuota.servicio = row.Item("SERVICIO")
			jsonCuota.num_dni = row.Item("NUM_DNI")
			jsonCuota.denominac = row.Item("DENOMINAC")
			jsonCuota.imp_comp = row.Item("IMP_COMP")
			jsonCuota.matricula = row.Item("MATRICULA")
			jsonCuota.description1 = row.Item("DESCRIP_1")
			jsonCuota.description2 = row.Item("DESCRIP_2")
			jsonCuota.description3 = row.Item("DESCRIP_3")
			jsonCuota.proceso = row.Item("PROCESO")
			jsonCuota.cuota = row.Item("CUOTA")
			jsonCuota.total = row.Item("TOTAL")

			'recorro las columnas de la tabla por fila
			For Each item As DataColumn In row.Table.Columns
				'item caption = nombre de la columna, item ordinal = posicion de la columna para acceder al valor
				If item.Ordinal > 10 Then
					jsonCuota.cuentas.Add(key_cuenta, row.Item(item.Ordinal))
					key_cuenta += 1
				End If
			Next
			key_cuenta = 0

			jsonDebito.cuotas.Add(key, jsonCuota)

			key += 1
		Next

		Return JsonConvert.SerializeObject(jsonDebito)
	End Function

	''' <summary>
	''' Recibe un dataTable y lo parsea a json, (devuelve un string).
	''' </summary>
	Public Function generaJson_resg(dt As DataTable, nombre As String, tipo As String, fechaComp As String) As String

		Dim jsonDict As New Dictionary(Of String, Object)

		dt.TableName = tipo
		dt.Columns.Remove("IMP_RECH")
		dt.Columns.Remove("MOTIVO_RE")

		jsonDict.Add("tipo", nombre)

		Dim key As Integer = 0
		Dim dictItem As New Dictionary(Of String, String)

		'recorro las filas de la tabla
		For Each row As DataRow In dt.Rows
			'recorro las columnas de la tabla por fila
			For Each item As DataColumn In row.Table.Columns
				'item caption = nombre de la columna, item ordinal = posicion de la columna para acceder al valor
				dictItem.Add(item.Caption, row.Item(item.Ordinal))
			Next

			'aca viene un problema al hacer dictItem clear se borra todo incluso lo que contenia antes
			'entonces debo crear una intancia nueva que contenga la copia de dictItem
			Dim resg As New Dictionary(Of String, String)(dictItem)
			'voy agregando el diccionario segun tantas filas tenga la tabla
			jsonDict.Add(key, resg)
			dictItem.Clear()

			key += 1
		Next

		jsonDict.Add("serv", tipo)

		'voy a cargar los datos del usuario para mandarlos por json
		Dim datos(2) As String
		'nroOperador
		datos(0) = nPubNroOperador
		'datos(0) = OPERADOR ID
		'datos(1) = CAJA ID
		'datos(2) = ZETA ID
		'controlo que la caja este activa
		If cnn.CajaActiva Then
			datos(1) = cnn.Caja
			datos(2) = cnn.Zeta
		Else
			datos(1) = 0
			datos(2) = 0
		End If
		jsonDict.Add("operador", datos)
		jsonDict.Add("fecha_comp", fechaComp)

		Return JsonConvert.SerializeObject(jsonDict)
	End Function

End Class
