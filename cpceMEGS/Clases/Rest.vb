﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json
Public Class Rest
    Dim status As Boolean = False

    Private Sub sendJson(ByVal urlParameter As String, ByVal params As String)
        Try
            ' Controlo si es prod o test para utilizar el rest
            Dim url = ""
            If Application.StartupPath = "C:\CpceMEGS" Then
                url = "http://cpcerest.prod/"
            Else
                url = "http://cpcerest.test/"
            End If
            url = url & urlParameter

            Dim request As HttpWebRequest = WebRequest.Create(url)
            request.Accept = "*/*"
            request.Method = WebRequestMethods.Http.Post
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
            ' Create a new string object to POST data to the Url. 
            Dim encoding As New ASCIIEncoding()
            Dim byte1 As Byte() = encoding.GetBytes(params)
            'Set the content type of the data being posted.
            request.ContentType = "application/json"
            ' Set the content length of the string being posted.
            request.ContentLength = byte1.Length
            Dim newStream As Stream = request.GetRequestStream()

            newStream.Write(byte1, 0, byte1.Length)
            newStream.Close()

            Dim oResponse As HttpWebResponse = request.GetResponse()
            Dim reader As New StreamReader(oResponse.GetResponseStream())
            Dim xmlString As String = reader.ReadToEnd()

            If HttpStatusCode.OK = oResponse.StatusCode Then
                'Enviados DatosJson Correctamente
                status = True
                MessageBox.Show("Datos Enviados Correctamente", oResponse.StatusDescription)
            Else
                'ERROR al Enviar DatosJson
                MessageBox.Show("Error al procesar el archivo", oResponse.StatusDescription)
            End If

            oResponse.Close()

            'Dim xmlres As XElement = XElement.Parse(xmlString)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Envia un json a SymfonyRest para transacciones
    ''' </summary>
    Public Sub sendTransaccion(ByVal json As String)
        Dim params As String = "{'valor_json': " & json & "}"
        sendJson("transaccion/", params)
    End Sub

    ''' <summary>
    ''' Envia un json a SymfonyRest para debitos
    ''' </summary>
    Public Sub sendDebito(ByVal json As String)
        sendJson("debito/", json)
    End Sub

    Public Function getCalendarioJson(ByVal urlParameter As String, ByVal id_calendario As Integer) As Dictionary(Of String, String)

        Dim json As Dictionary(Of String, String)

        Try
            ' Controlo si es prod o test para utilizar el rest
            Dim url = ""
            If Application.StartupPath = "C:\CpceMEGS" Then
                url = "http://cpcechaco.org.ar/apirest/"
            Else
                url = "http://cpcechaco.localhost/app_dev.php/apirest/"
            End If

            url = url & urlParameter & id_calendario

            Dim request As HttpWebRequest = WebRequest.Create(url)
            Dim response As WebResponse

            request.Credentials = CredentialCache.DefaultCredentials

            response = request.GetResponse()
            Dim oResponse As HttpWebResponse = request.GetResponse()
            Dim reader As New StreamReader(oResponse.GetResponseStream())
            Dim xmlString As String = reader.ReadToEnd()

            response.Close()
            If HttpStatusCode.OK = oResponse.StatusCode Then
                'Si acepta peticion obtiene datos de agenda Correctamente
                status = True
                'MessageBox.Show("Peticion Aceptada Correctamente", oResponse.StatusDescription)
            Else
                'ERROR al Enviar DatosJson
                MessageBox.Show("Error al procesar el archivo", oResponse.StatusDescription)
            End If

            oResponse.Close()

            json = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(xmlString)

        Catch ex As Exception

            json = New Dictionary(Of String, String)

            MessageBox.Show(ex.Message)
        End Try

        Return json

    End Function

End Class
