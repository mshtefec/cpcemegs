﻿Public Class clsMensajes
    Private Mensaje As String

    Public Property Resultado() As String
        Get
            Return Me.Mensaje
        End Get
        Set(ByVal value As String)
            Me.Mensaje = value
        End Set
    End Property

    Public Sub msgInformacion(ByVal msg As String)
        MessageBox.Show(msg, "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Sub msgPregunta(ByVal msg As String)
        If (MessageBox.Show(msg, "PREGUNTA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes) Then
            Me.Resultado = "SI"
        End If
    End Sub

    Public Sub msgError(ByVal msg As String)
        MessageBox.Show(msg, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

End Class
