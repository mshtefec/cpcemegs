﻿Imports MySql.Data.MySqlClient
Imports System.Data
Public Class clsClientes
    Dim Cnn As New ConsultaBD(cPubServidor, cPubUsuario, cPubClave)
    Dim DSCli As DataSet
    Dim DACli As MySqlDataAdapter

    Private _NroCli As Integer
    Private _NroDoc As Integer
    Private _TipDoc As String
    Private _Nombre As String
    Private _Tipo As Char
    Private _Categoria As String
    Private _Suspen As Char
    Private _Calle As String
    Private _NroDom As Integer
    Private _Barrio As String
    Private _Zona As String
    Private _CP As Integer
    Private _Tel As String
    Private _Cel As String
    Private _ObrSoc As String
    Private _Cuit As String
    Private _Iva As String
    'Private _FecNac As  Date
    'Private _FecNac As Nullable(Of Date)
    Private _FecNac As String
    Private _Sexo As Char
    Private _Civil As Char
    Private _Nacion As String
    Private _Ocupa As String
    Private _NroAfi As String
    Private _Legajo As String
    Private _Debita As Char
    Private _NroAhorro As String
    Private _NroCue As String
    Private _Contacto As String
    Private _TelCont As String
    Private _Refe As String
    Private _Lugar As String
    Private _Disco As Char
    Private _Copias As Integer
    Private _CanLot As Integer
    Private _Comentario As String
    Private _TipList As Integer
    Private _TipFact As Integer
    Private _Titulo As String
    Private _Bonific As Decimal
    Private _Sucursal As String
    Private _Presen As Char
    'Private _FecAlt As Date
    Private _FecAlt As Nullable(Of Date)
    Private _Operador As Integer
    Private _Mensaje As String
    Private _Mail As String
    Private _SocioAmeb As String
    Private _Plan As String
    Private _EstSocio As Char
    'Private _FecBaja As Date
    Private _FecBaja As Nullable(Of Date)
    Private _Parentesco As Char
    Private _TipTit As String
    Private _NroTit As Integer
    Private _Lote As Double

    Private _TipCta As String
    Private _NroCta As Integer



    '---------------------------
    Private _SaldoAmeb As Double
    Private _SaldoCta As Double
    '---------------------------
    Private _Cuenta As String       'Guarda el Nombre de la Cta Cte
    Private _TopeCta As Double      'Limite de la cta cte
    Private _TipTitular As String   'Si es autorizado doc del titular
    Private _NroTitular As Integer  'Si es autorizado doc del titular
    Private _EstadoCta As Char

    Private _Ameb As Boolean        'Si es socio ameb y esta activo

    Dim mTrans As MySqlTransaction


    Public Property SaldoAmeb() As Double
        Get
            Return Me._SaldoAmeb
        End Get
        Set(ByVal value As Double)
            Me._SaldoAmeb = value
        End Set
    End Property

    Public Property SaldoCta() As Double
        Get
            Return Me._SaldoCta
        End Get
        Set(ByVal value As Double)
            Me._SaldoCta = value
        End Set
    End Property

    Public ReadOnly Property Ameb() As Boolean
        Get
            Return Me._Ameb
        End Get
    End Property

    Public Property Cuenta() As String
        Get
            Return Me._Cuenta
        End Get
        Set(ByVal value As String)
            Me._Cuenta = value
        End Set
    End Property

    Public Property NroCli() As Integer
        Get
            Return Me._NroCli
        End Get
        Set(ByVal value As Integer)
            Me._NroCli = value
        End Set
    End Property

    Public Property NroDoc() As Integer
        Get
            Return Me._NroDoc
        End Get
        Set(ByVal value As Integer)
            Me._NroDoc = value
        End Set
    End Property

    Public Property TipDoc() As String
        Get
            Return Me._TipDoc
        End Get
        Set(ByVal value As String)
            Me._TipDoc = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return Me._Nombre
        End Get
        Set(ByVal value As String)
            Me._Nombre = value
        End Set
    End Property

    Public Property Tipo() As Char
        Get
            Return Me._Tipo
        End Get
        Set(ByVal value As Char)
            Me._Tipo = value
        End Set
    End Property

    Public Property Categoria() As String
        Get
            Return Me._Categoria
        End Get
        Set(ByVal value As String)
            Me._Categoria = value
        End Set
    End Property

    Public Property Suspendido() As Char
        Get
            Return Me._Suspen
        End Get
        Set(ByVal value As Char)
            Me._Suspen = value
        End Set
    End Property

    Public Property Calle() As String
        Get
            Return Me._Calle
        End Get
        Set(ByVal value As String)
            Me._Calle = value
        End Set
    End Property

    Public Property NroDom() As Integer
        Get
            Return Me._NroDom
        End Get
        Set(ByVal value As Integer)
            Me._NroDom = value
        End Set
    End Property

    Public Property Barrio() As String
        Get
            Return Me._Barrio
        End Get
        Set(ByVal value As String)
            Me._Barrio = value
        End Set
    End Property

    Public Property Zona() As String
        Get
            Return Me._Zona
        End Get
        Set(ByVal value As String)
            Me._Zona = value
        End Set
    End Property

    Public Property CP() As Integer
        Get
            Return Me._CP
        End Get
        Set(ByVal value As Integer)
            Me._CP = value
        End Set
    End Property

    Public Property Telefono() As String
        Get
            Return Me._Tel
        End Get
        Set(ByVal value As String)
            Me._Tel = value
        End Set
    End Property

    Public Property Celular() As String
        Get
            Return Me._Cel
        End Get
        Set(ByVal value As String)
            Me._Cel = value
        End Set
    End Property

    Public Property ObraSocial() As String
        Get
            Return Me._ObrSoc
        End Get
        Set(ByVal value As String)
            Me._ObrSoc = value
        End Set
    End Property

    Public Property Cuit() As String
        Get
            Return Me._Cuit
        End Get
        Set(ByVal value As String)
            Me._Cuit = value
        End Set
    End Property

    Public Property Iva() As String
        Get
            Return Me._Iva
        End Get
        Set(ByVal value As String)
            Me._Iva = value
        End Set
    End Property

    Public Property FecNac() As String
        Get
            Return Me._FecNac
        End Get
        Set(ByVal value As String)
            Me._FecNac = value
        End Set
    End Property

    Public Property Sexo() As Char
        Get
            Return Me._Sexo
        End Get
        Set(ByVal value As Char)
            Me._Sexo = value
        End Set
    End Property

    Public Property Civil() As Char
        Get
            Return Me._Civil
        End Get
        Set(ByVal value As Char)
            Me._Civil = value
        End Set
    End Property

    Public Property Nacion() As String
        Get
            Return Me._Nacion
        End Get
        Set(ByVal value As String)
            Me._Nacion = value
        End Set
    End Property

    Public Property Ocupacion() As String
        Get
            Return Me._Ocupa
        End Get
        Set(ByVal value As String)
            Me._Ocupa = value
        End Set
    End Property

    Public Property NroAfi() As String
        Get
            Return Me._NroAfi
        End Get
        Set(ByVal value As String)
            Me._NroAfi = value
        End Set
    End Property

    Public Property Legajo() As String
        Get
            Return Me._Legajo
        End Get
        Set(ByVal value As String)
            Me._Legajo = value
        End Set
    End Property

    Public Property Debita() As Char
        Get
            Return Me._Debita
        End Get
        Set(ByVal value As Char)
            Me._Debita = value
        End Set
    End Property

    Public Property CajAhorro() As String
        Get
            Return Me._NroAhorro
        End Get
        Set(ByVal value As String)
            Me._NroAhorro = value
        End Set
    End Property

    Public Property NroCue() As String
        Get
            Return Me._NroCue
        End Get
        Set(ByVal value As String)
            Me._NroCue = value
        End Set
    End Property

    Public Property Contacto() As String
        Get
            Return Me._Contacto
        End Get
        Set(ByVal value As String)
            Me._Contacto = value
        End Set
    End Property

    Public Property TelCont() As String
        Get
            Return Me._TelCont
        End Get
        Set(ByVal value As String)
            Me._TelCont = value
        End Set
    End Property

    Public Property Referencia() As String
        Get
            Return Me._Refe
        End Get
        Set(ByVal value As String)
            Me._Refe = value
        End Set
    End Property

    Public Property Lugar() As String
        Get
            Return Me._Lugar
        End Get
        Set(ByVal value As String)
            Me._Lugar = value
        End Set
    End Property

    Public Property Disco() As Char
        Get
            Return Me._Disco
        End Get
        Set(ByVal value As Char)
            Me._Disco = value
        End Set
    End Property

    Public Property Copias() As Integer
        Get
            Return Me._Copias
        End Get
        Set(ByVal value As Integer)
            Me._Copias = value
        End Set
    End Property

    Public Property CanLot() As Integer
        Get
            Return Me._CanLot
        End Get
        Set(ByVal value As Integer)
            Me._CanLot = value
        End Set
    End Property

    Public Property Comentario() As String
        Get
            Return Me._Comentario
        End Get
        Set(ByVal value As String)
            Me._Comentario = value
        End Set
    End Property

    Public Property TipList() As Integer
        Get
            Return Me._TipList
        End Get
        Set(ByVal value As Integer)
            Me._TipList = value
        End Set
    End Property

    Public Property TipFact() As Integer
        Get
            Return Me._TipFact
        End Get
        Set(ByVal value As Integer)
            Me._TipFact = value
        End Set
    End Property

    Public Property Titulo() As String
        Get
            Return Me._Titulo
        End Get
        Set(ByVal value As String)
            Me._Titulo = value
        End Set
    End Property

    Public Property Bonificacion() As Decimal
        Get
            Return Me._Bonific
        End Get
        Set(ByVal value As Decimal)
            Me._Bonific = value
        End Set
    End Property

    Public Property Sucursal() As String
        Get
            Return Me._Sucursal
        End Get
        Set(ByVal value As String)
            Me._Sucursal = value
        End Set
    End Property

    Public Property Presen() As Char
        Get
            Return Me._Presen
        End Get
        Set(ByVal value As Char)
            Me._Presen = value
        End Set
    End Property

    Public Property FechaAlta() As Date
        Get
            Return Me._FecAlt
        End Get
        Set(ByVal value As Date)
            Me._FecAlt = value
        End Set
    End Property

    Public Property Operador() As Integer
        Get
            Return Me._Operador
        End Get
        Set(ByVal value As Integer)
            Me._Operador = value
        End Set
    End Property

    Public Property Mesaje() As String
        Get
            Return Me._Mensaje
        End Get
        Set(ByVal value As String)
            Me._Mensaje = value
        End Set
    End Property

    Public Property Mail() As String
        Get
            Return Me._Mail 
        End Get
        Set(ByVal value As String)
            Me._Mail = value
        End Set
    End Property

    Public Property SocioAmeb() As String
        Get
            Return Me._SocioAmeb
        End Get
        Set(ByVal value As String)
            Me._SocioAmeb = value
        End Set
    End Property

    Public Property Plan() As String
        Get
            Return Me._Plan
        End Get
        Set(ByVal value As String)
            Me._Plan = value
        End Set
    End Property

    Public Property EstSocio() As Char
        Get
            Return Me._EstSocio
        End Get
        Set(ByVal value As Char)
            Me._EstSocio = value
        End Set
    End Property

    Public Property FechaBaja() As Date
        Get
            If Me._FecBaja.HasValue Then
                Return Me._FecBaja
            End If
        End Get
        Set(ByVal value As Date)
            Me._FecBaja = value
        End Set
    End Property

    Public Property Parentesco() As Char
        Get
            Return Me._Parentesco
        End Get
        Set(ByVal value As Char)
            Me._Parentesco = value
        End Set
    End Property

    Public Property TipoTit() As String
        Get
            Return Me._TipTit
        End Get
        Set(ByVal value As String)
            Me._TipTit = value
        End Set
    End Property

    Public Property NroTit() As Integer
        Get
            Return Me._NroTit
        End Get
        Set(ByVal value As Integer)
            Me._NroTit = value
        End Set
    End Property

    Public Property Lote() As Double
        Get
            Return Me._Lote
        End Get
        Set(ByVal value As Double)
            Me._Lote = value
        End Set
    End Property

    Public Property TipTitular() As String
        Get
            Return Me._TipTitular
        End Get
        Set(ByVal value As String)
            Me._TipTitular = value
        End Set
    End Property

    Public Property NroTitular() As Integer
        Get
            Return Me._NroTitular
        End Get
        Set(ByVal value As Integer)
            Me._NroTitular = value
        End Set
    End Property

    Public Property TopeCta() As Double
        Get
            Return Me._TopeCta
        End Get
        Set(ByVal value As Double)
            Me._TopeCta = value
        End Set
    End Property

    Public Property EstadoCta() As Char
        Get
            Return Me._EstadoCta
        End Get
        Set(ByVal value As Char)
            Me._EstadoCta = value
        End Set
    End Property

    Public Property TipCta() As String
        Get
            Return Me._TipCta
        End Get
        Set(ByVal value As String)
            Me._TipCta = value
        End Set
    End Property

    Public Property NroCta() As Integer
        Get
            Return Me._NroCta
        End Get
        Set(ByVal value As Integer)
            Me._NroCta = value
        End Set
    End Property


    '----------------------------------------------------------------------------------
    '                                   METODOS                                       '
    '----------------------------------------------------------------------------------

    Public Function DatosCliente(Optional ByVal TDoc As String = "", Optional ByVal Nro As Integer = 0, Optional ByVal Nomb As String = "") As DataSet
        DSCli = New DataSet

        'Me._TipDoc = TDoc
        'Me._NroDoc = Nro
        'Me._Nombre = Nomb

        Try
            If Cnn.AbrirConexion Then
                If Nro = 0 Then
                    If Nomb <> "" Then
                        DACli = Cnn.consultaBDadapter("Clientes", "*", "cli_nombre like '" & Nomb & "%'")
                    End If
                Else
                    If TDoc <> "" Then
                        DACli = Cnn.consultaBDadapter("Clientes", "*", "cli_tipdoc='" & TDoc & "' and cli_nrodoc=" & Nro & "")
                    End If
                End If

                Dim CB As New MySqlCommandBuilder(DACli)
                DACli.Fill(DSCli, "Clientes")

                If DSCli.Tables("Clientes").Rows.Count > 0 Then
                    DatosCliente = DSCli
                Else
                    DatosCliente = Nothing
                End If
            Else
                DatosCliente = Nothing
                MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            DatosCliente = Nothing
            MessageBox.Show("Se produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try

        Return DatosCliente

    End Function

    Public Function ClienteVenta(Optional ByVal TDoc As String = "", Optional ByVal Nro As Integer = 0, Optional ByVal Nomb As String = "") As DataSet
        DSCli = New DataSet

        Try
            If Cnn.AbrirConexion Then
                If Nro = 0 Then
                    If Nomb <> "" Then
                        DACli = Cnn.consultaBDadapter("clientes ", "*", "cli_nombre like '" & Nomb & "%'")
                    Else
                        Cnn.CerrarConexion()
                        Return Nothing
                        Exit Function
                    End If
                Else
                    If TDoc <> "" Then
                        DACli = Cnn.consultaBDadapter("clientes", "*", "cli_tipdoc='" & TDoc & "' and cli_nrodoc=" & Nro & "")
                    Else
                        Cnn.CerrarConexion()
                        Return Nothing
                        Exit Function
                    End If
                End If

                Dim CB As New MySqlCommandBuilder(DACli)
                DACli.Fill(DSCli, "Clientes")

                If DSCli.Tables("Clientes").Rows.Count > 0 Then
                    Return DSCli
                Else
                    Return Nothing
                End If

            Else
                Return Nothing
                MessageBox.Show("No Se Pudo Establecer la conexion con el Servidor", "INTENTELO DE NUEVO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Cnn.CerrarConexion()
            End If
        Catch ex As Exception
            Return Nothing
            MessageBox.Show("Se Produjo un Error, Por Favor Verifique" & ex.Message.ToString, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try
    End Function

    Public Sub CargarDatos(ByVal DR As DataRow)
        Ceros()
        Me._NroCli = DR("cli_nrocli")
        Me._TipDoc = DR("cli_tipdoc")
        Me._NroDoc = DR("cli_nrodoc")
        Me._Nombre = DR("cli_nombre")
        Me._Tipo = DR("cli_tipo")
        Me._Categoria = DR("cli_categoria")
        Me._Suspen = DR("cli_suspen")
        Me._Calle = DR("cli_calle")
        Me._NroDom = DR("cli_nrodom")
        Me._Barrio = DR("cli_barrio")
        Me._CP = DR("cli_codpos")
        Me._Tel = DR("cli_telefono")
        Me._Cel = DR("cli_telcon")
        Me._ObrSoc = DR("cli_obrsoc")
        If Not IsDBNull(DR("cli_obrsoc")) Then
            Me._Cuit = DR("cli_cuit")
        Else
            Me._Cuit = 0
        End If
        If Not IsDBNull(DR("cli_tipoiva")) Then
            Me._Iva = DR("cli_tipoiva")
        Else
            Me._Iva = 0
        End If        
        If Not DR("cli_fecnac").ToString = "00/00/0000" Then
            If Not IsDBNull(DR("cli_fecnac")) Then
                'Me._FecNac = Format(Convert.ToDateTime(DR("cli_fecnac")), "yyyy-MM-dd")
                Me._FecNac = DR("cli_fecnac").ToString
            Else
                Me._FecNac = Nothing
            End If
        End If
        Me._Sexo = DR("cli_sexo")
        Me._Civil = DR("cli_civil")
        Me._Nacion = DR("cli_nacion")
        Me._Ocupa = DR("cli_ocupa")
        Me._NroAfi = DR("cli_nroafi")
        Me._Legajo = DR("cli_legajo")
        Me._Debita = DR("cli_debito")
        Me._NroAhorro = DR("cli_nroaho")
        Me._NroCue = DR("cli_nrocue")
        Me._Contacto = DR("cli_contac")
        Me._TelCont = DR("cli_telcon")
        Me._Refe = DR("cli_refere")
        Me._Lugar = DR("cli_lugprt")
        Me._Disco = DR("cli_disco")
        Me._Copias = DR("cli_copias")
        Me._CanLot = DR("cli_canlot")
        Me._Comentario = DR("cli_coment")
        Me._TipList = DR("cli_tiplis")
        Me._TipFact = DR("cli_tipfac")
        Me._Titulo = DR("cli_titulo")
        Me._Bonific = DR("cli_bonif")
        Me._Sucursal = DR("cli_sucursal")
        Me._Presen = DR("cli_presen")

        If Not DR("cli_fecalt").ToString = "00/00/0000" Then
            If Not IsDBNull(DR("cli_fecalt")) Then               
                Me._FecAlt = DR("cli_fecalt").ToString
            Else
                Me._FecAlt = Nothing
            End If
        End If

        Me._Operador = DR("cli_operador")
        Me._Mensaje = DR("cli_mensaje")
        Me._Mail = DR("cli_email")
        Me._SocioAmeb = DR("cli_socioameb")
        Me._Plan = DR("cli_plan")
        Me._EstSocio = DR("cli_estsocio")

        If Not DR("cli_fecbaja").ToString = "00/00/0000" Then
            If Not IsDBNull(DR("cli_fecbaja")) Then
                Me._FecBaja = DR("cli_fecbaja").ToString
            Else
                Me._FecBaja = Nothing
            End If
        End If

        Me._Parentesco = DR("cli_parentesco")
        Me._TipTit = DR("cli_tiptitular")
        Me._NroTit = DR("cli_nrotitular")
        Me._Lote = DR("cli_lote")
    End Sub

    Public Sub CargarCli(Optional ByVal NCli As Integer = 0, Optional ByVal Tipo As String = "", Optional ByVal Doc As Integer = 0, Optional ByVal Nomb As String = "", Optional ByVal OS As String = "", Optional ByVal NAfi As String = "", Optional ByVal Dom As String = "", Optional ByVal NDom As Integer = 0, Optional ByVal Barrio As String = "", Optional ByVal CP As Integer = 0, Optional ByVal Zona As String = "", Optional ByVal Cuit As String = "", Optional ByVal Tel As String = "", Optional ByVal Cel As String = "", Optional ByVal FNac As String = "", Optional ByVal Sexo As Char = "", Optional ByVal Civil As String = "", Optional ByVal Nacio As String = "", Optional ByVal Ocupa As String = "", Optional ByVal Mail As String = "")
        Me._NroCli = NCli
        Me._TipDoc = Tipo
        Me._NroDoc = Doc
        Me._Nombre = Nomb
        Me._Calle = Dom
        Me._NroDom = NDom
        Me._Barrio = Barrio
        Me._Zona = Zona
        Me._CP = CP
        Me._Tel = Tel
        Me._ObrSoc = OS
        Me._Cel = Cel
        If Not IsDBNull(Cuit) Then
            Me._Cuit = Cuit
        Else
            Me._Cuit = 0
        End If
        If Not IsDBNull(Iva) Then
            Me._Iva = Iva
        Else
            Me._Iva = 0
        End If

        If FNac.ToString <> "" Then
            If Not FNac.ToString = "00/00/0000" Then
                If Not IsDBNull(FNac) Then
                    Me._FecNac = Format(Convert.ToDateTime(FNac), "yyyy-MM-dd")
                Else
                    Me._FecNac = Nothing
                End If
            Else
                Me._FecNac = Nothing
            End If
        Else
            Me._FecNac = Nothing
        End If
        Me._Sexo = Sexo
        Me._Civil = Civil
        Me._Nacion = Nacio
        Me._Ocupa = Ocupa
        Me._NroAfi = NAfi
        Me._Operador = nPubNroOperador
        Me._Lote = 0
        Me._Mail = Mail
    End Sub

    Public Function Saldo(ByVal Tdoc As String, ByVal Nro As Integer, ByVal Cuenta As String, Optional ByVal Fe1 As String = "", Optional ByVal Fe2 As String = "", Optional ByVal Servicio As Integer = 0) As Double
        Dim Comando As MySqlCommand
        Comando = New MySqlCommand

        Comando.Connection = Cnn.conexion
        Comando.CommandText = "select sum((mov_coeficiente*mov_impcom)) as Saldo from movcompr WHERE mov_fecha <='" & Format(Date.Now, "yyyy-MM-dd") & "' and mov_tipcta = '" & Tdoc & "' AND mov_nrocta = " & Nro & " AND mov_cuenta = '" & Cuenta & "' AND mov_servicio = " & Servicio & ""
        Comando.CommandType = CommandType.Text
        If Cnn.EstadoConexion = False Then
            Cnn.AbrirConexion()
        End If

        Try
            If IsDBNull(Comando.ExecuteScalar) Then
                Saldo = 0
            Else
                Saldo = CDbl(Comando.ExecuteScalar)
            End If

        Catch ex As Exception
            MessageBox.Show("Se Produjo un Error " & ex.ToString, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            Cnn.CerrarConexion()
        End Try
        Return Saldo
    End Function

    'Public Function TieneCuenta(ByVal Tdoc As String, ByVal Nro As Integer, Optional ByVal NroSuc As Integer = 0) As DataSet
    '    Dim DSCta As DataSet = New DataSet
    '    DACli = New MySqlDataAdapter

    '    If NroSuc = 0 Then
    '        DACli = Cnn.consultaBDadapter("cuentas INNER JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo", "cue_cuenta,prt_nombre,cue_tipdoc,cue_nrodoc ,cue_suspen,cue_limite ", "cue_tipdoc = '" & Tdoc & "' AND cue_nrodoc = " & Nro & " and prt_nrocli=" & 1)
    '    Else
    '        DACli = Cnn.consultaBDadapter("cuentas LEFT JOIN prestado ON cue_cuenta = prt_presta AND cue_tipo = prt_tipo and cue_nrocli = prt_nrocli", "cue_cuenta,prt_nombre,cue_tipdoc,cue_nrodoc ,cue_suspen,cue_limite ", "cue_tipdoc = '" & Tdoc & "' AND cue_nrodoc = " & Nro & " and cue_suspen IN('N','S',' ')")
    '    End If
    '    Dim CB = New MySqlCommandBuilder(DACli)
    '    DACli.Fill(DSCta, "Cuentas")

    '    If DSCta.Tables("Cuentas").Rows.Count > 0 Then
    '        TieneCuenta = DSCta
    '    Else
    '        TieneCuenta = Nothing
    '    End If

    '    Return TieneCuenta
    'End Function

    Public Function Autoriza(ByVal Tdoc As String, ByVal Nro As Integer) As Boolean
        Dim DSAut As DataSet = New DataSet
        DACli = New MySqlDataAdapter

        'select cli_nombre as titular,aut_tiptitular as TipTit,aut_nrotitular as NroTit,cli_obrsoc as Obrsoc,cue_cuenta as Cuenta,cli_calle,cli_barrio,cli_mensaje from (autoriza inner join clientes on aut_tiptitular = cli_tipdoc and aut_nrotitular = cli_nrodoc) left join cuentas on aut_tiptitular = cue_tipdoc and aut_nrotitular = cue_nrodoc where aut_tipautoriza ='" & rstCli.Fields!tdoc & "' and aut_nroautoriza =" & rstCli.Fields!documento & " and aut_suspen <> 'S'", cnnConexion, adOpenStatic, adLockOptimistic

        DACli = Cnn.consultaBDadapter("(autoriza inner join clientes on aut_tiptitular = cli_tipdoc and aut_nrotitular = cli_nrodoc) left join cuentas on aut_tiptitular = cue_tipdoc and aut_nrotitular = cue_nrodoc", "cli_nombre as titular,aut_tiptitular as TipTit,aut_nrotitular as NroTit,cli_obrsoc as Obrsoc,cue_cuenta as Cuenta,cli_calle,cli_barrio,cli_mensaje", "aut_tipautoriza ='" & Tdoc & "' and aut_nroautoriza =" & Nro & " and aut_suspen <> 'S'")

        Dim CB = New MySqlCommandBuilder(DACli)
        DACli.Fill(DSAut, "Autoriza")

        If DSAut.Tables("Autoriza").Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub Ceros()     
        Me.Barrio = ""
        Me.Bonificacion = 0
        Me.CajAhorro = ""
        Me.Calle = ""
        Me.CanLot = 0
        Me.Categoria = ""
        Me.Civil = ""
        Me.Comentario = ""
        Me.Contacto = ""
        Me.Copias = 0
        Me.CP = 0
        Me.Cuenta = ""
        Me.Cuit = ""
        Me.Debita = ""
        Me.Disco = ""
        Me.EstSocio = ""
        Me.FechaAlta = "01/01/1900"
        Me.FechaBaja = "01/01/1900"
        Me.FecNac = "01/01/1900"
        Me.Iva = ""
        Me.Legajo = ""
        Me.Lote = 0
        Me.Lugar = ""
        Me.Mail = ""
        Me.Mesaje = ""
        Me.Nacion = ""
        Me.Nombre = ""
        Me.NroAfi = 0
        Me.NroCli = 0
        Me.NroCue = ""
        Me.NroDoc = 0
        Me.NroDom = 0
        Me.NroTit = 0
        Me.NroTitular = 0
        Me.ObraSocial = ""
        Me.Ocupacion = ""
        Me.Operador = 0
        Me.Parentesco = ""
        Me.Plan = ""
        Me.Presen = ""
        Me.Referencia = ""
        Me.Sexo = ""
        Me.SocioAmeb = 0
        Me.Sucursal = ""
        Me.Suspendido = ""
        Me.TelCont = ""
        Me.Telefono = ""
        Me.TipDoc = ""
        Me.TipFact = 0
        Me.TipList = 0
        Me.Tipo = ""
        Me.TipoTit = ""
        Me.TipTitular = ""
        Me.Titulo = ""
        Me.TipCta = ""
        Me.NroCta = 0
    End Sub

    Public Function DeudaAmeb(ByVal Tipo As String, ByVal Nro As Long, ByVal Tipameb As String, ByVal NroAmeb As Long) As Boolean
        Dim DSDeuda As New DataSet
        Dim DADeuda As MySqlDataAdapter

        Try
            If Cnn.AbrirConexion Then
                DADeuda = New MySqlDataAdapter
                DADeuda = Cnn.consultaBDadapter("movcompr", "*", "mov_cuenta='AMEB' and mov_tipcta='" & Tipo & "' and mov_nrocta=" & Nro & " and mov_fecha > '" & Format(Date.Now.AddMonths(-2).Date, "yyyy-MM-dd") & "'")
                DADeuda.Fill(DSDeuda, "Movimientos")

                If DSDeuda.Tables("Movimientos").Rows.Count = 0 Then
                    DeudaAmeb = False
                Else
                    'BUSCAR EL SALDO AHORA
                    If Saldo(Tipo, Nro, "AMEB", Date.Now, , 5000) > 6 Then                  
                        DADeuda = Cnn.consultaBDadapter("movcompr", "*", "mov_cuenta='AMEB' and mov_tipcta='" & Tipo & "' and mov_nrocta=" & Nro & " and mov_fecha > '" & Format(Date.Now.AddMonths(-2).Date, "yyyy-MM-dd") & "' and mov_tipcom IN ('PS','AA')")
                        DADeuda.Fill(DSDeuda, "Movimientos2")
                        If DSDeuda.Tables("Movimientos2").Rows.Count = 0 Then
                            DeudaAmeb = False
                        Else
                            DeudaAmeb = True
                        End If
                    Else
                        DeudaAmeb = True
                    End If
                End If
            Else
                MessageBox.Show("Error al Conectar con el Servidor", "NO SE PUDO ESTABLECER LA CONEXION CON EL SERVIDOR", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            MessageBox.Show("Se produjo un Error " & ex.Message.ToString, "POR FAVOR VERIFIQUE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            Cnn.CerrarConexion()
        End Try

        Return DeudaAmeb

    End Function

    Public Function ActualizaCliente() As Boolean
        DSCli = New DataSet

        Try
            If Cnn.AbrirConexion Then
                mTrans = Cnn.InicioTransaccion
                DACli = Cnn.consultaBDadapter("Clientes", "*", "cli_tipdoc='" & TipDoc & "' and cli_nrodoc = " & NroDoc & "")
                DACli.Fill(DSCli, "Clientes")

                If DSCli.Tables("Clientes").Rows.Count = 0 Then
                    If Cnn.insertarBD("clientes (cli_nrocli,cli_tipdoc,cli_nrodoc,cli_nombre,cli_tipo," & _
                            "cli_obrsoc,cli_cuit,cli_nroafi,cli_nrodom,cli_barrio, " & _
                            "cli_calle,cli_fecnac,cli_sexo,cli_civil,cli_codpos ," & _
                            "cli_nacion,cli_lote,cli_telefono,cli_ocupa,cli_telcon," & _
                            "cli_email,cli_fecalt)", "" & NroCli & ",'" & TipDoc & "'," & NroDoc & ", " & _
                            "'" & Nombre & "','" & "C" & "','" & ObraSocial & "','" & Cuit & "','" & NroAfi & "'," & _
                            "'" & NroDom & "','" & Barrio & "','" & Calle & "'," & _
                            "'" & IIf(Trim(FecNac) = "", "0000-00-00", FecNac) & "'," & _
                            "'" & Sexo & "','" & Civil & "'," & CP & ",'" & Nacion & "','" & 1 & "', " & _
                            "'" & Telefono & "','" & Ocupacion & "','" & Celular & "','" & Mail & "','" & Format(Convert.ToDateTime(Date.Now), "yyyy-MM-dd") & "'") Then
                        ActualizaCliente = True
                    End If
                ElseIf DSCli.Tables("Clientes").Rows.Count = 1 Then
                    If Cnn.actualizarBD("UPDATE clientes SET cli_nombre = '" & Nombre & "'," & _
                                        "cli_obrsoc = '" & ObraSocial & "'," & _
                                        "cli_cuit   = '" & Cuit & "'," & _
                                        "cli_nroafi = '" & NroAfi & "'," & _
                                        "cli_nrodom = '" & NroDom & "'," & _
                                        "cli_barrio = '" & Barrio & "'," & _
                                        "cli_calle =  '" & Calle & "'," & _
                                        "cli_fecnac = '" & IIf(Trim(FecNac) = "", "0000-00-00", FecNac) & "'," & _
                                        "cli_sexo ='" & Sexo & "'," & _
                                        "cli_civil='" & Civil & "'," & _
                                        "cli_codpos=" & CP & "," & _
                                        "cli_nacion='" & Nacion & "'," & _
                                        "cli_lote='" & 1 & "'," & _
                                        "cli_telefono='" & Telefono & "'," & _
                                        "cli_ocupa='" & Ocupacion & "'," & _
                                        "cli_telcon='" & Celular & "', cli_email ='" & Mail & "' " & _
                            "WHERE cli_tipdoc = '" & TipDoc & "'" & _
                            " AND cli_nrodoc = " & NroDoc & "") Then
                        ActualizaCliente = True
                    End If
                Else
                    MessageBox.Show("Se Encontraron 2 Clientes con el Mismo Documento", "DOCUMENTO DUPLICADO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    ActualizaCliente = False
                    Exit Function
                End If
                mTrans.Commit()
            Else
                MessageBox.Show("No Se Pudo Establecer la conexion con el Servidor", "INTENTELO DE NUEVO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ActualizaCliente = False
            End If
        Catch ex As Exception
            ActualizaCliente = False
            mTrans.Rollback()
            MessageBox.Show("Se Produjo un Error, Por Favor Verifique" & ex.Message.ToString, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            mTrans.Dispose()
            Cnn.CerrarConexion()
        End Try
        Return ActualizaCliente
    End Function

    Public Function ActualizaMasDatosClientes(Optional ByVal DS As DataSet = Nothing) As Boolean

        DSCli = New DataSet

        Try
            If Cnn.AbrirConexion Then
                mTrans = Cnn.InicioTransaccion
                DACli = Cnn.consultaBDadapter("Clientes", "*", "cli_tipdoc='" & TipDoc & "' and cli_nrodoc = " & NroDoc & "")
                DACli.Fill(DSCli, "Clientes")

                If DSCli.Tables("Clientes").Rows.Count = 1 Then
                    If Cnn.actualizarBD("UPDATE clientes SET SET cli_socioameb ='" & SocioAmeb & "'," & _
                        "cli_categoria ='" & Categoria & "'," & _
                        "cli_legajo ='" & Legajo & "'," & _
                        "cli_mensaje ='" & Mesaje & "'," & _
                        "cli_email ='" & Mail & "'," & _
                        "cli_canlot='" & IIf(CanLot = "", 0, CanLot) & "'," & _
                        "cli_nroaho='" & CajAhorro & "'," & _
                        "cli_nrocue='" & NroCue & "'," & _
                        "cli_sucursal='" & Sucursal & "'," & _
                        "cli_lote ='" & 1 & "'," & _
                        "cli_parentesco='" & Parentesco & "'," & _
                        "cli_tiptitular='" & TipoTit & "'," & _
                        "cli_nrotitular='" & NroTit & "'," & _
                        "cli_fecbaja= '" & IIf(Trim(FechaBaja) = "", "0000-00-00", FechaBaja) & "'," & _
                        "cli_refere='" & Referencia & "'," & _
                        "cli_lugprt='" & Lugar & "'," & _
                        "cli_titulo='" & Titulo & "'," & _
                        "cli_telcon='" & TelCont & "' " & _
                        "WHERE cli_tipdoc = '" & TipDoc & "' AND cli_nrodoc = " & NroDoc & "") Then
                        ActualizaMasDatosClientes = True
                        mTrans.Commit()
                    Else
                        ActualizaMasDatosClientes = False
                    End If
                Else
                    MessageBox.Show("Se Encontro Mas de 1 Cliente con el tipo y numero de documento elegidos", "ERROR,DOCUMENTO DUPLICADO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    ActualizaMasDatosClientes = False
                End If
            Else
                MessageBox.Show("No Se Pudo Establecer la conexion con el Servidor", "INTENTELO DE NUEVO", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                ActualizaMasDatosClientes = False
            End If
        Catch ex As Exception
            ActualizaMasDatosClientes = False
            mTrans.Rollback()
            MessageBox.Show("Se Produjo un Error, Por Favor Verifique" & ex.Message.ToString, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            mTrans.Dispose()
            Cnn.CerrarConexion()
        End Try

        Return ActualizaMasDatosClientes

    End Function


End Class
