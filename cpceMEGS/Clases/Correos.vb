﻿Imports System.Net.Mail
Public Class Correos
    Dim message As New MailMessage
    Dim smtp As New SmtpClient
    Dim addressFrom As String = "noresponder@cpcechaco.org.ar"
    Dim password As String = "SIqueres_2016"
    Dim displayName As String = "CPCE Chaco (no responder)"
    Public Sub enviarCorreo(ByVal destinatario As String, ByVal asunto As String, ByVal contenido As String)
        'Si esta en Debug utiliza el correo seteado en la variable
        If Not IsNothing(cPubCorreoDestinatario) Then
            destinatario = cPubCorreoDestinatario
        End If
        'Valida el correo antes de entrar
        If ValidateEmail(destinatario) Then
            message.From = New MailAddress(addressFrom, displayName)
            message.To.Add(destinatario)
            message.Subject = asunto
            message.IsBodyHtml = True
            message.Body = contenido
            message.Priority = MailPriority.Normal
            'smtp.EnableSsl = True
            'smtp.Port = "143"
            smtp.Port = "587"
            smtp.Host = "mail.cpcechaco.org.ar"
            enviarCorreoAhora()
        End If
    End Sub
    'Envia el correo o muestra la exception
    Private Sub enviarCorreoAhora()
        Try
            smtp.Credentials = New Net.NetworkCredential(addressFrom, password)
            smtp.Send(message)
        Catch ex As Exception
            MessageBox.Show("No se pudo enviar el correo. (2001) " & ex.Message)
        End Try
    End Sub
    'Este método comprueba que las direcciones de correo electrónico tengan el formato alguien@ejemplo.com.
    Function ValidateEmail(ByVal email As String) As Boolean
        Dim emailRegex As New Text.RegularExpressions.Regex(
            "^(?<user>[^@]+)@(?<host>.+)$")
        Dim emailMatch As Text.RegularExpressions.Match =
           emailRegex.Match(email)
        Return emailMatch.Success
    End Function
End Class
